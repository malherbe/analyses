<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN637" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="7(abab)">
				<head type="number">IV</head>
				<head type="main">Landrol</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:2">L<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>dr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2" punct="vg">o</seg>l</w>, <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg></w> <w n="1.3" punct="pe:4">d<seg phoneme="œ" type="vs" value="1" rule="406" place="4" punct="pe">eu</seg>il</w> ! <w n="1.4" punct="pe:6">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pe">eu</seg>r</w> ! <w n="1.5" punct="pe:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>xt<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="2.2" punct="pe:3">K<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>n<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="pe">in</seg>g</w> ! <w n="2.3">L<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>dr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>l</w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="2.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>st<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Qu<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg>s</w>, <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="3.4" punct="vg:5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5" punct="vg">u</seg>s</w>, <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="3.6" punct="pt:8">G<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>mn<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="4.2">n<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="4.3" punct="pe:4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="pe">in</seg></w> ! <w n="4.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt</w> <w n="4.5" punct="pe:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>st<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></w> !</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="5.2" punct="vg:3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" punct="vg">en</seg>t</w>, <w n="5.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="5.4">p<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="7">œu</seg>r</w> <w n="5.6" punct="pt:8">tr<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1" punct="vg:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="3" punct="vg">e</seg>t</w>, <w n="6.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>t</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="6.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.5" punct="vg:8">tr<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>bl<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>r</w> <w n="7.3" punct="dp:5">m<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="5" punct="dp">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> : <w n="7.4"><seg phoneme="i" type="vs" value="1" rule="468" place="6">I</seg>l</w> <w n="7.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6">s<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="8.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="8.5">m</w>’<w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="8.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2" punct="vg:3">Scr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="9.3">qu</w>’<w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="9.5">l<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="9.6" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>t</w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>rs</w> <w n="10.5">fl<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>tt<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ts</rhyme></w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="11.4" punct="dp:5">L<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>dr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="dp">o</seg>l</w> : <w n="11.5">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">L<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>dr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l</w> <w n="12.2" punct="dp:4">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="dp">on</seg>d</w> : <w n="12.3">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.5" punct="pi:8">t<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="pi">em</seg>ps</rhyme></w> ?</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="13.2">bl<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">tr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="13.6" punct="vg:8">r<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">Scr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r</w> <w n="14.5" punct="pt:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="a" type="vs" value="1" rule="194" place="7">e</seg>nn<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</rhyme></w>.</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">l</w>’<w n="15.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="15.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="15.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="15.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="15.7"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.6" punct="pt:8">c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>n<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</rhyme></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="17.2">C<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="17.3"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="17.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="17.5">Th<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>nt</w>, <w n="18.2">m<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="18.3" punct="vg:8">t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="vg">oin</seg>s</rhyme></w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="19.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="19.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="19.4" punct="pt:4">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="pt ti">ai</seg>rs</w>. — <w n="19.5">Tr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="19.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="19.7" punct="vg:8">si<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w>-<w n="20.2" punct="pt:2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pt">i</seg>l</w>. <w n="20.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">Ou</seg></w> <w n="20.4" punct="pt:5">qu<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="pt">e</seg></w>. <w n="20.5">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="20.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="20.7" punct="pt:8">m<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="pt">oin</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1">L<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>dr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l</w> <w n="21.2" punct="dp:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="dp">i</seg>t</w> : <w n="21.3">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="21.4" punct="pe:5">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="5" punct="pe">oi</seg></w> ! <w n="21.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="21.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1">Fr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="22.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="22.3">J<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ph<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t</rhyme></w></l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="23.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="23.3"><seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>l<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="24.2">f<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="24.3">Scr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="24.5" punct="dp:7">r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="dp">on</seg>d</w> : <w n="24.6" punct="pe:8">F<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>t</rhyme></w> !</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="25.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="25.3" punct="vg:5">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="25.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="25.5">m</w>’<w n="25.6" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>bst<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="26.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="26.3">l</w>’<w n="26.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="26.5">r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</rhyme></w></l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="27.2">j<seg phoneme="u" type="vs" value="1" rule="429" place="2">ou</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="27.3">M<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="27.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="27.5">Chr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="28.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="28.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt</w> <w n="28.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="28.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="28.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="28.7" punct="pe:8">Di<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg>x</rhyme></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1888"> 9 juin 1888.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>