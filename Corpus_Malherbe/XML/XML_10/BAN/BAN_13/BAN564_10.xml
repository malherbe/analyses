<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2572 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">OCCIDENTALES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-03-13" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Triolets</head><div type="poem" key="BAN564" modus="sm" lm_max="8" metProfile="8" form="triolet classique" schema="ABaAabAB">
					<head type="main">Autres Chassepots</head>
					<lg n="1" rhyme="ABaAabAB">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="1.2">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>q</w> <w n="1.3"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="1.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>x</w> <w n="1.5">f<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</w> <w n="1.6">l</w>’<w n="1.7" punct="vg:8"><rhyme label="A" id="1" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2" punct="dp:3">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="dp">i</seg>ls</w> : <w n="2.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="2.7" punct="pe:8">r<rhyme label="B" id="2" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="3.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-6" place="6">e</seg>r</w> <w n="3.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pt">an</seg></rhyme></w>.</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1" punct="vg:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="4.2">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>q</w> <w n="4.3"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="4.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>x</w> <w n="4.5">f<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</w> <w n="4.6">l</w>’<w n="4.7" punct="vg:8"><rhyme label="A" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg></rhyme></w>,</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2" punct="pe:3">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="pe">i</seg>ls</w> ! <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="5.4" punct="pe:6">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pe">i</seg>ls</w> ! <w n="5.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="5.6"><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></rhyme></w></l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="6.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.4" punct="pt:8">ch<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1" punct="vg:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="7.2">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>q</w> <w n="7.3"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="7.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>x</w> <w n="7.5">f<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</w> <w n="7.6">l</w>’<w n="7.7" punct="vg:8"><rhyme label="A" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg></rhyme></w>,</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2" punct="dp:3">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="dp">i</seg>ls</w> : <w n="8.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="8.7" punct="pe:8">r<rhyme label="B" id="2" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					</lg>
				</div></body></text></TEI>