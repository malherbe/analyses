<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="BAN62" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="11((aa))">
					<head type="main">Ô JEUNE FLORENTINE</head>
					<lg n="1" type="distiques" rhyme="aa…">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="1.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">Fl<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.6">pr<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="1.7" punct="vg:12">n<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="2.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="2.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="2.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="2.7" punct="vg:12">gl<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="P">u</seg>r</w> <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="3.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="3.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="y" type="vs" value="1" rule="251" place="7">eû</seg>t</w> <w n="3.7">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="3.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="3.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="3.10">l<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="493" place="12">y</seg>s</rhyme></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="4.3">G<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="4.4"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="4.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="P">u</seg>r</w> <w n="4.6" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">A</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="493" place="11" mp="M">y</seg>ll<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>s</rhyme></w>,</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="5.3">d</w>’<w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="5.5">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d</w> <w n="5.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M">ai</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="5.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="5.9">ch<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="6.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4">e</seg>ds</w> <w n="6.5">p<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="6.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="6.7">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg>s</w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="6.9">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="11">e</seg>ts</w> <w n="6.10" punct="vg:12">r<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">H<seg phoneme="i" type="vs" value="1" rule="d-1" place="1" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="7.4" punct="vg:6">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="7.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8" mp="M">i</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="7.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="7.8" punct="vg:12">b<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="vg">ain</seg></rhyme></w>,</l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="8.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="8.4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>c<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="8.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="8.6">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</w> <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.8">d</w>’<w n="8.9" punct="pt:12"><seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">U</seg>rb<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg></rhyme></w>.</l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="9.2">c<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="9.4" punct="pe:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pe" caesura="1">oi</seg>rs</w> !<caesura></caesura> <w n="9.5">M<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w> <w n="9.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="9.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="C">ou</seg>s</w> <w n="9.8">tr<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="9.9">t<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">j</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="10.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="10.5">br<seg phoneme="y" type="vs" value="1" rule="445" place="5" mp="M">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="10.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="10.8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.9" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rt<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="11.2">j</w>’<w n="11.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="11.4">R<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ph<seg phoneme="a" type="vs" value="1" rule="343" place="5" mp="M">a</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="6" caesura="1">ë</seg>l</w><caesura></caesura> <w n="11.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="11.6">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">A</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>ghi<seg phoneme="e" type="vs" value="1" rule="BAN62_1" place="11" mp="M">e</seg>r<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg></rhyme></w></l>
						<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2">m<seg phoneme="e" type="vs" value="1" rule="353" place="2" mp="M">e</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="12.4">cl<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="12.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="12.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="12.7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="12.8" punct="vg:12">ch<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>r<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg></rhyme></w>,</l>
						<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="13.4" punct="vg:6">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>ts</w>,<caesura></caesura> <w n="13.5">f<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="13.7">j<seg phoneme="wa" type="vs" value="1" rule="423" place="9">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="13.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="13.9">d</w>’<w n="13.10" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>vr<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1" punct="vg:3">Pl<seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="M">a</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3" punct="vg">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>, <w n="14.2" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="5" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="14.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="14.4">l</w>’<w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r</w> <w n="14.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="14.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="C">ou</seg>s</w> <w n="14.8" punct="pt:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="15" num="1.15" lm="12" met="6+6"><space quantity="2" unit="char"></space><w n="15.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="15.2" punct="vg:3">V<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg></w> <w n="15.4" punct="pe:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe" caesura="1">a</seg></w> !<caesura></caesura> <w n="15.5">M</w>’<w n="15.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>stru<seg phoneme="i" type="vs" value="1" rule="491" place="8" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="15.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="15.9" punct="vg:12">j<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="16.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ts</w> <w n="16.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="16.4">gu<seg phoneme="i" type="vs" value="1" rule="491" place="4" mp="M">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="16.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" mp="P">e</seg>rs</w> <w n="16.6">l</w>’<w n="16.7"><seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">O</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="9">ym</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="16.8">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="11" mp="M">ei</seg>g<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</rhyme></w></l>
						<l n="17" num="1.17" lm="12" met="6+6"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="17.4"><seg phoneme="i" type="vs" value="1" rule="497" place="3" mp="C">y</seg></w> <w n="17.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="17.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="17.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="17.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="17.9">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="9" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w> <w n="17.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="17.11" punct="vg:12">l<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="18" num="1.18" lm="12" met="6+6"><w n="18.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="18.4">v<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="18.5">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="18.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.7"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="18.8">v<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="18.9" punct="pt:12">br<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="19" num="1.19" lm="12" met="6+6"><w n="19.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="19.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="19.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="19.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>ts</w><caesura></caesura> <w n="19.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="19.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="19.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="19.8"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>l<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></w></l>
						<l n="20" num="1.20" lm="12" met="6+6"><w n="20.1" punct="vg:2">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="20.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="20.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="20.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="20.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>c</w><caesura></caesura> <w n="20.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="20.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></w></l>
						<l n="21" num="1.21" lm="12" met="6+6"><w n="21.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="21.2" punct="vg:3">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="21.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="21.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="21.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="21.7" punct="vg:12">fl<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ss<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="22" num="1.22" lm="12" met="6+6"><w n="22.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="22.2">gr<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="22.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" mp="M">ai</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="22.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="22.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="22.6" punct="pe:12">ch<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>ss<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1832">mai 1832</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>