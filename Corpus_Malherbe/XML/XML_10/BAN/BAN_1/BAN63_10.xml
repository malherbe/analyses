<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><head type="main_subpart">EN HABIT ZINZOLIN</head><div type="poem" key="BAN63" modus="cp" lm_max="10" metProfile="4−6, (4)" form="rondeau classique" schema="aabba aabC aabbaC">
						<head type="number">I</head>
						<head type="main">RONDEAU, À ÉGLÉ</head>
						<lg n="1" rhyme="aabba">
							<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="1.3">pl<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="1.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fc">e</seg></w> <w n="1.6">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.7">cl<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
							<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" caesura="1">oi</seg>t</w><caesura></caesura> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="M">on</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="2.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="9">ein</seg></w> <w n="2.8" punct="vg:10">r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="3.2">br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="3.3" punct="vg:4">h<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="3.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="3.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rps</w> <w n="3.7" punct="vg:10">p<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>l<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></rhyme></w>,</l>
							<l n="4" num="1.4" lm="10" met="4−6" mp4="P"><w n="4.1" punct="vg:2">S<seg phoneme="y" type="vs" value="1" rule="d-3" place="1" mp="M">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P" caesura="1">an</seg>s</w><caesura></caesura> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="4.5">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="6">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="4.6" punct="vg:10">m<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>l<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></rhyme></w>,</l>
							<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2" punct="vg:2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>t</w>, <w n="5.3" punct="pe:4">h<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe" caesura="1">a</seg>s</w> !<caesura></caesura> <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">Un</seg></w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="5.6" punct="pt:10">d<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>sp<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
						<lg n="2" rhyme="aabC">
							<l n="6" num="2.1" lm="10" met="4+6"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="6.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="6.3">sc<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>pt<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="6.6">b<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="6.7">m<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
							<l n="7" num="2.2" lm="10" met="4+6"><w n="7.1">M<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="7.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="7.5" punct="vg:7">bl<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>sph<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7" punct="vg">è</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="7.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>pp<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
							<l n="8" num="2.3" lm="10" met="4+6"><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">à</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="8.4">n<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="8.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="8.7">d</w>’<w n="8.8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt</w> <w n="8.9">s</w>’<w n="8.10"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="8.11">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9" mp="M">ê</seg>l<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></rhyme></w></l>
							<l n="9" num="2.4" lm="4"><space quantity="12" unit="char"></space><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="9.3" punct="pt:4">pl<rhyme label="C" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pt">i</seg>s</rhyme></w>.</l>
						</lg>
						<lg n="3" rhyme="aabbaC">
							<l n="10" num="3.1" lm="10" met="4+6"><w n="10.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="10.2">qu</w>’<w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="3" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="10.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="10.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" mp="M">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="10.6" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>cl<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="11" num="3.2" lm="10" met="4+6"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="11.7" punct="pt:10">m<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rph<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
							<l n="12" num="3.3" lm="10" met="4+6"><w n="12.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, <w n="12.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="12.4" punct="pv:4">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" punct="pv" caesura="1">ai</seg></w> ;<caesura></caesura> <w n="12.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="12.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>l<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></rhyme></w></l>
							<l n="13" num="3.4" lm="10" met="4+6"><w n="13.1">N</w>’<w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="13.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="13.4" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="13.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="13.7">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">É</seg>gl<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></rhyme></w>,</l>
							<l n="14" num="3.5" lm="10" met="4+6"><w n="14.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="14.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="14.3" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>rd</w>,<caesura></caesura> <w n="14.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="14.6" punct="vg:8"><seg phoneme="wa" type="vs" value="1" rule="420" place="7" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></w>, <w n="14.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="14.8">p<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
							<l n="15" num="3.6" lm="4"><space quantity="12" unit="char"></space><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="15.3" punct="pt:4">pl<rhyme label="C" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pt">i</seg>s</rhyme></w>.</l>
						</lg>
					</div></body></text></TEI>