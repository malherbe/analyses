<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><div type="poem" key="BAN38" modus="cp" lm_max="10" metProfile="5, 4+6" form="suite périodique" schema="6(abab)">
					<head type="main">IDOLÂTRIE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>Les sociétés polies, mais idolâtres, <lb></lb>
									de Rome et d’Athènes ignoraient <lb></lb>
									la céleste dignité de la femme, <lb></lb>
									révélée plus tard aux hommes par <lb></lb>
									le Dieu qui voulut naître d’une <lb></lb>
									fille d’Ève.</quote>
								<bibl>
									<name>VICTOR HUGO</name>, <hi rend="ital">Littérature <lb></lb>et Philosophie mêlées.</hi>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">M<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.2" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg" caesura="1">in</seg></w>,<caesura></caesura> <w n="1.3">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.5">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.6" punct="vg:10">r<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="2.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" caesura="1">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="2.5">p<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="7">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.6" punct="vg:10">n<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="vg">eau</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="3.3">j<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="3.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="3.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="P">ou</seg>r</w> <w n="3.6" punct="vg:10">H<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="4.1">Rh<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3" punct="pe:5">S<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pph<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="5" punct="pe">o</seg></rhyme></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" mp="Lp">ai</seg>s</w>-<w n="5.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="5.3">fl<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="5.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="5.6">n<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="8">ym</seg>ph<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>pr<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="6.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</w> <w n="6.7" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="10" punct="vg">oi</seg></rhyme></w>,</l>
						<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1">Qu<seg phoneme="wa" type="vs" value="1" rule="281" place="1">oi</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="7.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4" caesura="1">œu</seg>r</w><caesura></caesura> <w n="7.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="7.5">D<seg phoneme="i" type="vs" value="1" rule="d-1" place="6" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="7.6">m<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>pr<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">V<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="8.4" punct="pe:5">m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="pe">oi</seg></rhyme></w> !</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="9.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3" punct="vg:4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="9.5" punct="vg:7">gr<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" punct="vg" mp="F">e</seg>s</w>, <w n="9.6">tr<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.7" punct="vg:10">n<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="10" num="3.2" lm="10" met="4+6"><w n="10.1">Vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="1">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="10.2" punct="vg:4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="10.5">c<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>cc<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" punct="vg">o</seg>rd</rhyme></w>,</l>
						<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="11.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="11.3" punct="vg:4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4" punct="vg" caesura="1">ein</seg></w>,<caesura></caesura> <w n="11.4">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="11.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>n<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="12" num="3.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="12.1">L<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>d<seg phoneme="i" type="vs" value="1" rule="dc-1" place="2">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.2">s</w>’<w n="12.3" punct="pt:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" punct="pt">o</seg>rt</rhyme></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="10" met="4+6"><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="13.2">f<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">â</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="13.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="13.5" punct="vg:10">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="14" num="4.2" lm="10" met="4+6"><w n="14.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="14.2">s</w>’<w n="14.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>t</w><caesura></caesura> <w n="14.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="14.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="6" mp="C">o</seg>s</w> <w n="14.6">fl<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>ts</w> <w n="14.7" punct="vg:10">qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>ll<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="vg">eu</seg>rs</rhyme></w>,</l>
						<l n="15" num="4.3" lm="10" met="4+6"><w n="15.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="15.2">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="Fm">e</seg>s</w>-<w n="15.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="15.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="5" mp="C">o</seg>s</w> <w n="15.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="15.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="15.7" punct="vg:10">c<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="16" num="4.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="16.1">N<seg phoneme="a" type="vs" value="1" rule="343" place="1">a</seg>ï<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="16.3" punct="pe:5">pl<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="pe">eu</seg>rs</rhyme></w> !</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="10" met="4+6"><w n="17.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M/mp">In</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fm">e</seg></w>-<w n="17.2" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="17.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="17.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="17.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="17.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="17.7" punct="vg:10">l<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="493" place="10">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="18" num="5.2" lm="10" met="4+6"><w n="18.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="18.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="18.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="18.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>r</w><caesura></caesura> <w n="18.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="18.6">l</w>’<w n="18.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>qu<seg phoneme="i" type="vs" value="1" rule="487" place="9" mp="M">i</seg>l<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">on</seg></rhyme></w>,</l>
						<l n="19" num="5.3" lm="10" met="4+6"><w n="19.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="19.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ts</w> <w n="19.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4">br<seg phoneme="y" type="vs" value="1" rule="445" place="4" caesura="1">û</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="19.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="19.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="19.7" punct="vg:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>l<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="20" num="5.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="20.1">Ph<seg phoneme="e" type="vs" value="1" rule="250" place="1">œ</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="20.2" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>p<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>ll<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="pe">on</seg></rhyme></w> !</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="10" met="4+6"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="21.3" punct="vg:4">C<seg phoneme="i" type="vs" value="1" rule="493" place="3" mp="M">y</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="21.4">v<seg phoneme="ø" type="vs" value="1" rule="398" place="5" mp="Lp">eu</seg>x</w>-<w n="21.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="21.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="21.7">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg></w> <w n="21.9" punct="pi:10">pi<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
						<l n="22" num="6.2" lm="10" met="4+6"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="22.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="22.3">t</w>’<w n="22.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" caesura="1">o</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="22.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="22.7">m<seg phoneme="i" type="vs" value="1" rule="493" place="8">y</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="22.8">v<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rt</rhyme></w></l>
						<l n="23" num="6.3" lm="10" met="4+6"><w n="23.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="23.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="o" type="vs" value="1" rule="315" place="4" caesura="1">eau</seg>x</w><caesura></caesura> <w n="23.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="23.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>cs</w> <w n="23.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="23.6">n</w>’<w n="23.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="23.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="23.9">n<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="10">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="24" num="6.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="24.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">l<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>s</w> <w n="24.4" punct="pe:5"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" punct="pe">e</seg>rt</rhyme></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1842">juin 1842</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>