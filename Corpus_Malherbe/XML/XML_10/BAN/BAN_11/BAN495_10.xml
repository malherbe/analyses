<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN495" modus="sm" lm_max="8" metProfile="8" form="petite ballade" schema="3(ababbcbc) bcbc">
					<head type="number">XXI</head>
					<head type="main">Ballade pour les Parisiennes</head>
					<lg n="1" type="huitain" rhyme="ababbcbc">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="1.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="1.3" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>t</w>, <w n="1.4">ch<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="1.6">T<seg phoneme="ø" type="vs" value="1" rule="405" place="7">eu</seg>t<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">ch<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rm<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="2.5" punct="vg:8">p<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>g<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">I</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="3.5">J<seg phoneme="a" type="vs" value="1" rule="310" place="6">ea</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">F<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt</w> <w n="4.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">l</w>’<w n="4.5" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>th<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">Vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="2">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg></w>, <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="5.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.6" punct="vg:8">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rg<rhyme label="b" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="6.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="6.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="6.6" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>pr<rhyme label="c" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg>s</rhyme></w> ;</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="7.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">qu</w>’<w n="7.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="7.6" punct="dp:6">n<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="dp">e</seg></w> : <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.8" punct="vg:8">f<rhyme label="b" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">C</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6" punct="pt:8">P<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="c" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="2" type="huitain" rhyme="ababbcbc">
						<l n="9" num="2.1" lm="8" met="8"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="9.3" punct="vg:4">b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="9.5" punct="vg:6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>s</w>, <w n="9.6" punct="pt:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</rhyme></w>.</l>
						<l n="10" num="2.2" lm="8" met="8"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="10.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lg<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="11" num="2.3" lm="8" met="8"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2" punct="vg:2">l<seg phoneme="i" type="vs" value="1" rule="493" place="2" punct="vg">y</seg>s</w>, <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4" punct="vg:5">r<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6" punct="pt:8">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>st<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</rhyme></w>.</l>
						<l n="12" num="2.4" lm="8" met="8"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="12.5">qu</w>’<w n="12.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="12.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="12.8" punct="pe:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ff<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
						<l n="13" num="2.5" lm="8" met="8"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="13.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="13.5">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="13.6" punct="pv:8">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>gr<rhyme label="b" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="14" num="2.6" lm="8" met="8"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="14.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="14.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.6" punct="vg:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<rhyme label="c" id="8" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</rhyme></w>,</l>
						<l n="15" num="2.7" lm="8" met="8"><w n="15.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="15.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="15.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5">qu</w>’<w n="15.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7" punct="dp:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>t<rhyme label="b" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
						<l n="16" num="2.8" lm="8" met="8"><w n="16.1">C</w>’<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="16.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.6" punct="pt:8">P<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="c" id="8" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="3" type="huitain" rhyme="ababbcbc">
						<l n="17" num="3.1" lm="8" met="8"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="17.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ts</w> <w n="17.4">gl<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</rhyme></w></l>
						<l n="18" num="3.2" lm="8" met="8"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="18.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="18.4">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="18.6">f<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="18.7" punct="vg:8">tr<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="19" num="3.3" lm="8" met="8"><w n="19.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="19.4">f<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</rhyme></w></l>
						<l n="20" num="3.4" lm="8" met="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ct<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="20.5" punct="pt:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>dr<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="21" num="3.5" lm="8" met="8"><w n="21.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="21.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="21.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="21.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="21.5">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rg<rhyme label="b" id="11" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="22" num="3.6" lm="8" met="8"><w n="22.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301" place="2">ai</seg>t</w> <w n="22.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>s</w> <w n="22.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="22.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="c" id="12" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</rhyme></w></l>
						<l n="23" num="3.7" lm="8" met="8"><w n="23.1">Qu</w>’<w n="23.2" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="23.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="23.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="23.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="23.6" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gr<rhyme label="b" id="11" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
						<l n="24" num="3.8" lm="8" met="8"><w n="24.1">C</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="24.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="24.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.6" punct="pt:8">P<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="c" id="12" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="bcbc">
						<head type="form">Envoi</head>
						<l n="25" num="4.1" lm="8" met="8"><w n="25.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="25.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="25.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="25.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="25.6" punct="dp:8">g<rhyme label="b" id="13" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
						<l n="26" num="4.2" lm="8" met="8"><w n="26.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="26.2">f<seg phoneme="a" type="vs" value="1" rule="193" place="2">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="26.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="26.5">j<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="26.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="26.7">pr<rhyme label="c" id="14" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>x</rhyme></w></l>
						<l n="27" num="4.3" lm="8" met="8"><w n="27.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="27.2">v<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="27.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="27.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="27.5">d</w>’<w n="27.6" punct="pv:6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="pv">o</seg>r</w> ; <w n="27.7" punct="vg:7">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="vg">ai</seg>s</w>, <w n="27.8" punct="pe:8">d<rhyme label="b" id="13" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
						<l n="28" num="4.4" lm="8" met="8"><w n="28.1">C</w>’<w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="28.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="28.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="28.6" punct="pt:8">P<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="c" id="14" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>s</rhyme></w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1869">Mai 1869.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>