<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM12" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="2(abab) 3(abba)">
				<head type="main">RAYONNEZ, PLUMES DE MALGACHE</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:3">R<seg phoneme="ɛ" type="vs" value="1" rule="339" place="1">a</seg>y<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="1.2">pl<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.4" punct="vg:8">M<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lg<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1" punct="vg:3">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.4" punct="vg:8">S<seg phoneme="i" type="vs" value="1" rule="dc-1" place="7">i</seg><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>x</rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4" punct="vg:6">d<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>rds</w>, <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6" punct="vg:8">h<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">R<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="4.3" punct="vg:4">bu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg>s</w>, <w n="4.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="4.6" punct="pe:8">h<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pe">ou</seg>x</rhyme></w> !</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">m</w>’<w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="5.4">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="5.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="5.6">l</w>’<w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>f<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="6.3">n<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="6.4">M<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.5">C<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="7.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>r</w> <w n="7.6">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="7.7">b<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c</rhyme></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="8.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="8.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>pi<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="8.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="8.5" punct="pt:8">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>bl<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abba">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2" punct="vg:3">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="3" punct="vg">à</seg></w>, <w n="9.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="9.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rt</w> <w n="9.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="9.7">ch<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>ps</rhyme></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="10.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="10.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="10.6">fl<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="11.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.3">cr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">p<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w>-<w n="11.6">gr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">In</seg>di<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg>s</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.5">l</w>’<w n="12.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="12.7" punct="pt:8">c<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">am</seg>p</rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abba">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rsqu</w>’<w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="13.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="13.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></rhyme></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2">p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="14.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="14.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>t</w> <w n="15.3">s</w>’<w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257" place="4">eoi</seg>r</w> <w n="15.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="15.6">l</w>’<w n="15.7"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rc</w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="16.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="16.6" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg>t<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abba">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">m</w>’<w n="17.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.5">n<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8">en</seg>t</rhyme></w></l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="18.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="18.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="18.6" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>ni<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="19.2">l</w>’<w n="19.3"><seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="19.6" punct="vg:8">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>mi<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="20.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="20.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="20.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="20.6" punct="pt:8">bl<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>cs</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>