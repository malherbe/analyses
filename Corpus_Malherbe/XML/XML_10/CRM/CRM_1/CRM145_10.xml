<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM145" modus="sm" lm_max="6" metProfile="6" form="suite périodique avec alternance de type 1" schema="2{1(abab) 1(abba) 1(abab)}">
				<head type="main">DANS L’ODEUR DU MATIN</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="6" met="6"><w n="1.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="1.5" punct="vg:6">br<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><w n="2.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2" punct="vg:3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3" punct="vg">œu</seg>r</w>, <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="2.4" punct="pt:6">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="pt">in</seg></rhyme></w>.</l>
					<l n="3" num="1.3" lm="6" met="6"><w n="3.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="6" met="6"><w n="4.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="4.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="4.4" punct="pt:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="pt">in</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abba">
					<l n="5" num="2.1" lm="6" met="6"><w n="5.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="5.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="5.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="5.4">v<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
					<l n="6" num="2.2" lm="6" met="6"><w n="6.1">N</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5" punct="pv:6">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>hi<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pv">er</seg></rhyme></w> ;</l>
					<l n="7" num="2.3" lm="6" met="6"><w n="7.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="7.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="7.4" punct="vg:6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>s<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></rhyme></w>,</l>
					<l n="8" num="2.4" lm="6" met="6"><w n="8.1">C</w>’<w n="8.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="8.4" punct="pt:6">s<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="6" met="6"><w n="9.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="9.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>ni<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="9.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>s</rhyme></w></l>
					<l n="10" num="3.2" lm="6" met="6"><w n="10.1">D</w>’<w n="10.2" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="o" type="vs" value="1" rule="318" place="2" punct="vg">au</seg>t</w>, <w n="10.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.5" punct="vg:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" punct="vg">e</seg>il</rhyme></w>,</l>
					<l n="11" num="3.3" lm="6" met="6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="11.3">pl<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="11.4">d</w>’<w n="11.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">In</seg>di<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></rhyme></w></l>
					<l n="12" num="3.4" lm="6" met="6"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.3" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<rhyme label="b" id="12" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="6" met="6"><w n="13.1">C</w>’<w n="13.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="13.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>gt</w> <w n="13.4">m<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<rhyme label="a" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
					<l n="14" num="4.2" lm="6" met="6"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="14.3">f<seg phoneme="œ" type="vs" value="1" rule="304" place="3">ai</seg>si<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="14.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rn<rhyme label="b" id="7" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></rhyme></w></l>
					<l n="15" num="4.3" lm="6" met="6"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="15.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>p<rhyme label="a" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
					<l n="16" num="4.4" lm="6" met="6"><w n="16.1">Cl<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="16.2">d</w>’<w n="16.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="16.4" punct="pt:6">cu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>vr<rhyme label="b" id="7" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pt">é</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abba">
					<l n="17" num="5.1" lm="6" met="6"><w n="17.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="17.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="17.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.5" punct="vg:6">l<rhyme label="a" id="8" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg">in</seg></rhyme></w>,</l>
					<l n="18" num="5.2" lm="6" met="6"><w n="18.1">Cr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3" punct="vg:3">ge<seg phoneme="ɛ" type="vs" value="1" rule="301" place="3" punct="vg">ai</seg></w>, <w n="18.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="18.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="18.6" punct="vg:6">d<rhyme label="b" id="9" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="19" num="5.3" lm="6" met="6"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="19.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<rhyme label="b" id="9" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="20" num="5.4" lm="6" met="6"><w n="20.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="20.2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="20.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="20.5" punct="pt:6">m<rhyme label="a" id="8" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" punct="pt">ain</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="6" met="6"><w n="21.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="21.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="21.5" punct="vg:6">br<rhyme label="a" id="10" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="22" num="6.2" lm="6" met="6"><w n="22.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.2" punct="vg:3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3" punct="vg">œu</seg>r</w>, <w n="22.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="22.4" punct="pt:6">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<rhyme label="b" id="11" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="pt">in</seg></rhyme></w>.</l>
					<l n="23" num="6.3" lm="6" met="6"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="23.2">c</w>’<w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="23.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="23.5">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<rhyme label="a" id="10" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="24" num="6.4" lm="6" met="6"><w n="24.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="24.2">l</w>’<w n="24.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="24.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="24.5" punct="pt:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<rhyme label="b" id="11" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="pt">in</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>