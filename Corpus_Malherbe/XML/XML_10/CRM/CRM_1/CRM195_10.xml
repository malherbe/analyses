<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM195" rhyme="none" modus="sm" lm_max="6" metProfile="6">
				<head type="main">JOIE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="6" met="6"><w n="1.1">J<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="1.6" punct="vg:6">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="6" punct="vg">oi</seg></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><w n="2.1">J<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3" punct="vg:3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" punct="vg">en</seg>t</w>, <w n="2.4">j<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.6" punct="vg:6">f<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
					<l n="3" num="1.3" lm="6" met="6"><w n="3.1">J<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="3.2">fl<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">d</w>’<w n="3.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="406" place="6" punct="vg">eu</seg>il</w>,</l>
					<l n="4" num="1.4" lm="6" met="6"><w n="4.1">J<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">m<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="4.5" punct="pt:6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pt">oi</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="6" met="6"><w n="5.1">J<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="5.2">d</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="5.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.7">g<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
					<l n="6" num="2.2" lm="6" met="6"><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="6.5" punct="vg:6">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg">em</seg>ps</w>,</l>
					<l n="7" num="2.3" lm="6" met="6"><w n="7.1">J<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="7.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
					<l n="8" num="2.4" lm="6" met="6"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="8.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>s</w> <w n="8.4" punct="pt:6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pt">an</seg>ts</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="6" met="6"><w n="9.1">J<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="9.2">d</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="9.6" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>p</w>,</l>
					<l n="10" num="3.2" lm="6" met="6"><w n="10.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="10.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.4" punct="vg:6">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>r</w>,</l>
					<l n="11" num="3.3" lm="6" met="6"><w n="11.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>c<seg phoneme="u" type="vs" value="1" rule="426" place="6" punct="vg">ou</seg></w>,</l>
					<l n="12" num="3.4" lm="6" met="6"><w n="12.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4" punct="pt:6">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pt">oi</seg>r</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="6" met="6"><w n="13.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="13.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4" punct="vg:6">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>,</l>
					<l n="14" num="4.2" lm="6" met="6"><w n="14.1" punct="vg:2">Cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">er</seg></w>, <w n="14.2">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w></l>
					<l n="15" num="4.3" lm="6" met="6"><w n="15.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="15.2">m<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>ts</w> <w n="15.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="15.5">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="15.6">d</w>’<w n="15.7"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</w></l>
					<l n="16" num="4.4" lm="6" met="6"><w n="16.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="16.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="16.4" punct="pt:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="6" met="6"><w n="17.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="17.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="17.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w></l>
					<l n="18" num="5.2" lm="6" met="6"><w n="18.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="18.2">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="18.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="18.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w></l>
					<l n="19" num="5.3" lm="6" met="6"><w n="19.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="19.2">ch<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.4" punct="vg:6">b<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="vg">eau</seg></w>,</l>
					<l n="20" num="5.4" lm="6" met="6"><w n="20.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">ch<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.4" punct="vg:6">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>t</w>,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1" lm="6" met="6"><w n="21.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="21.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w></l>
					<l n="22" num="6.2" lm="6" met="6"><w n="22.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="22.3" punct="vg:3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg>t</w>, <w n="22.4">n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="22.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="22.6" punct="vg:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>r</w>,</l>
					<l n="23" num="6.3" lm="6" met="6"><w n="23.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="23.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="23.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w></l>
					<l n="24" num="6.4" lm="6" met="6"><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="24.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="24.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="24.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="24.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>r</w>,</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1" lm="6" met="6"><w n="25.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="25.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="25.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w></l>
					<l n="26" num="7.2" lm="6" met="6"><w n="26.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="26.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="26.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="26.5" punct="vg:6">br<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>s</w>,</l>
					<l n="27" num="7.3" lm="6" met="6"><w n="27.1">D</w>’<w n="27.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="27.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="27.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="27.5">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
					<l n="28" num="7.4" lm="6" met="6"><w n="28.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="28.2">n</w>’<w n="28.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="28.4">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="6">oi</seg></w></l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1" lm="6" met="6"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="29.2">d</w>’<w n="29.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="29.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w></l>
					<l n="30" num="8.2" lm="6" met="6"><w n="30.1">J<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="30.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="30.3" punct="vg:3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" punct="vg">en</seg>t</w>, <w n="30.4">j<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="30.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="30.6" punct="vg:6">f<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
					<l n="31" num="8.3" lm="6" met="6"><w n="31.1">D</w>’<w n="31.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="31.3">m<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="31.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w></l>
					<l n="32" num="8.4" lm="6" met="6"><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="32.2">fl<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="32.3">d</w>’<w n="32.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>il</w></l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1" lm="6" met="6"><w n="33.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="33.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="33.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="33.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w></l>
					<l n="34" num="9.2" lm="6" met="6"><w n="34.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="34.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="3">oi</seg></w> <w n="34.3">n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="34.4" punct="vg:6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6" punct="vg">en</seg>t</w>,</l>
					<l n="35" num="9.3" lm="6" met="6"><w n="35.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="35.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="35.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w></l>
					<l n="36" num="9.4" lm="6" met="6"><w n="36.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="36.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="36.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="36.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="36.5" punct="pt:6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="pt">em</seg>ps</w>.</l>
				</lg>
			</div></body></text></TEI>