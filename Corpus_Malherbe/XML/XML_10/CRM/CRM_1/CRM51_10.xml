<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM51" rhyme="none" modus="cp" lm_max="12" metProfile="8, 6+6">
				<head type="main">LA LUNE PARUT</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="3" mp="M">y</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="1.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="1.5"><seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</w> <w n="1.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">s</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="2.5">p<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>pl<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">cl<seg phoneme="u" type="vs" value="1" rule="427" place="3" mp="M">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="3.5" punct="pt:6">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="pt" caesura="1">o</seg>l</w>.<caesura></caesura> <w n="3.6">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</w> <w n="3.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="3.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="3.10" punct="pt:12">ni<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1" lm="12" met="6+6"><w n="4.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="4.2">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="4.3">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="10">en</seg>t</w> <w n="4.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="4.6">l</w>’<w n="4.7" punct="pt:12"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					<l n="5" num="2.2" lm="12" met="6+6"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="5.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2" mp="M">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="5.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="5.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="5.6">d</w>’<w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="5.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="5.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="6" num="2.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.4" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1" lm="12" met="6+6"><w n="7.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ti<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2" punct="vg">en</seg>t</w>, <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="7.3">cr<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="353" place="7" mp="M">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="7.6" punct="vg:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="8" num="3.2" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="8.4">v<seg phoneme="wa" type="vs" value="1" rule="440" place="3" mp="M">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="8.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="8.6">d</w>’<w n="8.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435" place="8" mp="M">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="8.8">l<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="9" num="3.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="9.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="9.2">l</w>’<w n="9.3">h<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="9.4" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1" lm="12" met="6+6"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="10.3">l<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.4" punct="pt:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pt" caesura="1">u</seg>t</w>.<caesura></caesura> <w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="10.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="10.8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="11" num="4.2" lm="12" met="6+6"><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="11.2">c<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="11.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="11.4" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="189" place="7" punct="vg">e</seg>t</w>, <w n="11.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="11.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="11.7" punct="vg:12">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="12" num="4.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="12.5">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="12.6" punct="pt:8">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></w>.</l>
				</lg>
			</div></body></text></TEI>