<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM128" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="3(abab) 2(abba)">
				<head type="main">PAS DE NUAGES</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3" punct="pv:5">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="pv">e</seg>s</w> ; <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="1.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>l</w> <w n="1.6">n<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="2.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ssi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.5" punct="pt:8">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>mi<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="3.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="3.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="3.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="3.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.6">l</w>’<w n="3.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="3.8">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="4.3">n</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.5">j<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="4.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="4.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.9" punct="pt:8">t<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>ps</w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="5.7"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1" punct="vg:2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="6.2"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">f<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r</w> <w n="6.5" punct="vg:8">v<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="7.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">f<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.6" punct="vg:8">j<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2">c</w>’<w n="8.3" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2" punct="vg">e</seg>st</w>, <w n="8.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="8.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="8.7" punct="pt:8">f<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="9.4">s</w>’<w n="9.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>nt</rhyme></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="10.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>t</w> <w n="10.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="10.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.7" punct="pt:8">p<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></w>.</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="11.2">n</w>’<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="11.4">qu</w>’<w n="11.5"><seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="11.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="11.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="11.8" punct="vg:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="12.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>ps</w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="12.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.8" punct="pt:8">b<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abba">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="13.2">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="13.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="13.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="13.5">l<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rds</w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.7" punct="pv:8">p<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></w> ;</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2" punct="vg:3">p<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>mmi<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">er</seg>s</w>, <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="14.5">d</w>’<w n="14.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></rhyme></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">Qu</w>’<w n="15.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="15.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.4">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="15.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="15.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</rhyme></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">S</w>’<w n="16.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="16.3">n</w>’<w n="16.4"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="16.5" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>t</w>, <w n="16.6" punct="vg:5">l<seg phoneme="a" type="vs" value="1" rule="342" place="5" punct="vg">à</seg></w>, <w n="16.7"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="16.8">b<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abba">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="17.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="17.4" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="vg">e</seg>il</rhyme></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="18.2">r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="18.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="19.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="19.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="19.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="19.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ffr<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">qu</w>’<w n="20.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="20.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="20.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="20.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.7" punct="pt:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pt">e</seg>il</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>