<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FEMMES RÊVÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>234 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Femmes Rêvées</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/12365</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Femmes Rêvées</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://archive.org/details/femmesrves00ferl</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Chez l’auteur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1899">1899</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FEMMES RÊVÉES</head><div type="poem" key="FER51" modus="cp" lm_max="9" metProfile="8, (9)" form="suite de strophes" schema="3[abab]">
					<head type="main">Les Préceptes de l’Amour</head>
					<lg n="1" type="regexp" rhyme="ab">
						<l n="1" num="1.1" lm="9"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="1.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="1.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r</w> <w n="1.4" punct="vg:9">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">om</seg>pt<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="9" punct="vg">a</seg>s</rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4" punct="pt:8">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>gu<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pt">en</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="2" type="regexp" rhyme="ab">
						<l n="3" num="2.1" lm="8" met="8"><w n="3.1">Vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="3.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rps</w> <w n="3.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="3.5">g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></w></l>
						<l n="4" num="2.2" lm="8" met="8"><w n="4.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="4.3">l</w>’<w n="4.4">h<seg phoneme="i" type="vs" value="1" rule="497" place="3">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="221" place="4">en</seg></w> <w n="4.5">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8">en</seg>t</rhyme></w></l>
					</lg>
					<lg n="3" type="regexp" rhyme="ab">
						<l n="5" num="3.1" lm="8" met="8"><w n="5.1">H<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.2">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>t</w> <w n="5.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></w></l>
						<l n="6" num="3.2" lm="8" met="8"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg>s</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.4" punct="pt:8"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="pt">en</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="4" type="regexp" rhyme="ab">
						<l n="7" num="4.1" lm="8" met="8"><w n="7.1">N<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">f<seg phoneme="a" type="vs" value="1" rule="193" place="3">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.4">c<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">aî</seg>tr<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></w></l>
						<l n="8" num="4.2" lm="8" met="8"><w n="8.1">H<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rs</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">l</w>’<w n="8.4">h<seg phoneme="i" type="vs" value="1" rule="497" place="3">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="221" place="4">en</seg></w> <w n="8.5" punct="pt:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pt">en</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="5" type="regexp" rhyme="ab">
						<l n="9" num="5.1" lm="8" met="8"><w n="9.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="9.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="9.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="9.5">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></w></l>
						<l n="10" num="5.2" lm="8" met="8"><w n="10.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">f<seg phoneme="a" type="vs" value="1" rule="193" place="3">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.3" punct="pt:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pt">en</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="6" type="regexp" rhyme="ab">
						<l n="11" num="6.1" lm="8" met="8"><w n="11.1">Chr<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="11.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="11.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4">m<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>lt<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>pl<seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></w></l>
						<l n="12" num="6.2" lm="8" met="8"><w n="12.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="12.5">l</w>’<w n="12.6" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="6">ei</seg>gn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pt">en</seg>t</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>