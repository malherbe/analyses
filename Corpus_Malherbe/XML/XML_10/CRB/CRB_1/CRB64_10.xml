<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RACCROCS</head><div type="poem" key="CRB64" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="2(abba) 2(abab) 1(aabb)">
					<head type="main">VÉSUVES ET Cie</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">P<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M/mc">om</seg>p<seg phoneme="e" type="vs" value="1" rule="CRB64_1" place="2" mp="M/mc">e</seg>ï<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="Lc">a</seg></w>-<w n="1.2" punct="tc:6">st<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M/mc">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M/mc">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="ti" caesura="1">on</seg></w> —<caesura></caesura> <w n="1.3" punct="vg:8">V<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w>-<w n="1.5">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="11">o</seg>r</w> <w n="1.7" punct="pi:12">t<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pi">oi</seg></rhyme></w> ?</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="2.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="2.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="2.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="2.5" punct="vg:6">b<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="2.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="2.7" punct="vg:9">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg>t</w>, <w n="2.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="2.9" punct="vg:12">Br<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>t<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6">— <w n="3.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="3.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="3.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="3.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="3.6">f<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="3.7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="3.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="3.9" punct="tc:12">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>t<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ti" mp="F">e</seg></rhyme></w> —</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="4.3">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M/mc">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="Lc">a</seg>t</w>-<w n="4.5" punct="vg:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="4.6">ch<seg phoneme="e" type="vs" value="1" rule="347" place="7" mp="P">ez</seg></w> <w n="4.7"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="4.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.9"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="4.10" punct="dp:12">m<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="dp">oi</seg></rhyme></w> :</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="5.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="5.4" punct="vg:6">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="5.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>d</w> <w n="5.8" punct="vg:12">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="vg">en</seg>t</rhyme></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="6.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.4">gr<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="6.6">f<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="6.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="6.9" punct="pt:12">cr<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">C</w>’<w n="7.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="7.4" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="7.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="Lp">i</seg>t</w>-<w n="7.6" punct="vg:8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></w>, <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="7.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="7.9">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="Lc">an</seg>d</w>’<w n="7.10">m<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">t</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="8.4">r<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="8.6">R<seg phoneme="ɔ" type="vs" value="1" rule="441" place="8">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="8.8" punct="ps:12">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">am</seg>b<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="ps">an</seg>t</rhyme></w>…</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="9.2" punct="vg:2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>d</w>, <w n="9.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="9.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="9.7">l</w>’<w n="9.8"><seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M/mc">O</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M/mc">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="Lc">a</seg></w>-<w n="9.9" punct="pt:12">C<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M/mc">o</seg>m<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="10" num="3.2" lm="12" met="6+6">— <w n="10.1">R<seg phoneme="o" type="vs" value="1" rule="415" place="1">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="10.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="10.3">cr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="10.5" punct="dp:8">t<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="dp">oi</seg></w> : <hi rend="ital"><w n="10.6">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="10.7">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="11">er</seg></w> <w n="10.8">J<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>r</rhyme></w></hi></l>
						<l n="11" num="3.3" lm="12" met="6+6"><hi rend="ital"><w n="11.1" punct="pt:4">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="11.2">P<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">om</seg>p<seg phoneme="e" type="vs" value="1" rule="CRB64_1" place="3" mp="M">e</seg><seg phoneme="i" type="vs" value="1" rule="476" place="4" punct="pt ti">ï</seg></w></hi>. — <w n="11.3">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="11.4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="11.5">s</w>’<w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="11.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="11.9" punct="vg:12">m<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>s<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="12.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">s<seg phoneme="u" type="vs" value="1" rule="428" place="3" mp="M">ou</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="12.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="12.5" punct="vg:6">r<seg phoneme="o" type="vs" value="1" rule="415" place="6" punct="vg" caesura="1">ô</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="12.6" punct="ps:7"><seg phoneme="e" type="vs" value="1" rule="189" place="7" punct="ps">e</seg>t</w> … <w n="12.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="12.8">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="12.9">f<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</w> <w n="12.10">qu</w>’<w n="12.11"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11">un</seg></w> <w n="12.12" punct="pt:12">f<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="12" met="6+6">— <w n="13.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="13.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="13.3">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="13.4" punct="dp:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="dp" caesura="1">u</seg>s</w> :<caesura></caesura> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem/mc">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="Lc">an</seg>t</w>-<w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-6" place="9" mp="Lc">e</seg></w>-<w n="13.7" punct="vg:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem/mc">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M/mc">i</seg>n<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="14.2" punct="vg:3">M<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3" punct="vg">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="14.4" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="14.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="14.6" punct="vg:9">m<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="14.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="14.9" punct="dp:12">f<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="dp">eu</seg></rhyme></w> :</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">Bl<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="15.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="P">u</seg>r</w> <w n="15.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d</w> <w n="15.4" punct="vg:4">r<seg phoneme="o" type="vs" value="1" rule="444" place="4" punct="vg">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>c</w><caesura></caesura> <w n="15.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="15.7">M<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>n<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="16.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="440" place="3" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="16.3" punct="vg:6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg></w>,<caesura></caesura> <w n="16.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="16.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="P">u</seg>r</w> <w n="16.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="16.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">am</seg>p</w> <w n="16.8" punct="pt:12">bl<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg></rhyme></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="aabb">
						<l n="17" num="5.1" lm="12" met="6+6">— <w n="17.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="17.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="17.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>s</w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="17.5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="17.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="17.7" punct="vg:9">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="17.8"><seg phoneme="o" type="vs" value="1" rule="415" place="10">ô</seg></w> <w n="17.9" punct="pe:12">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>t<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
						<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="18.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="18.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ds</w> <w n="18.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="18.5" punct="vg:6">v<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="18.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>xpr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8" punct="vg">è</seg>s</w>, <w n="18.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="18.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="18.9" punct="pt:12">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">am</seg>p<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="19.2">Vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="19.3">V<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="19.5" punct="vg:6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="19.6">pu<seg phoneme="i" type="vs" value="1" rule="491" place="7" mp="Lc">i</seg>squ</w>’<w n="19.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="19.8">m</w>’<w n="19.9"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <hi rend="ital"><w n="19.10">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w></hi> <w n="19.11">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11">en</seg>t</w> <w n="19.12" punct="pe:12">fr<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pe">an</seg>cs</rhyme></w> !</l>
						<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
						<l n="20" num="5.4" lm="12" met="6+6"><w n="20.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="20.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>ts</w><caesura></caesura> <w n="20.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="20.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="20.7" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">em</seg>bl<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>ts</rhyme></w>.</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Pompeï</placeName> aprile.
						</dateline>
					</closer>
				</div></body></text></TEI>