<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RACCROCS</head><div type="poem" key="CRB65" modus="sp" lm_max="7" metProfile="7, (5)" form="suite de strophes" schema="5[aa] 1[abba]">
					<head type="main">SONNETO A NAPOLI</head>
					<head type="sub_1">ALL’ SOLE, ALL’ LUNA</head>
					<head type="sub_1">ALL’ SABATO, ALL’ CANONICO</head>
					<head type="sub_1">E TUTTI QUANTI</head>
					<head type="sub_1"></head>
					<head type="sub_1">— CON PULCINELLA —</head>
					<lg n="1" type="regexp" rhyme="aaaa">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.6">S<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>d<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">n</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="2.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="2.6" punct="pv:7">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pv">i</seg></rhyme></w> ;</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1">F<seg phoneme="a" type="vs" value="1" rule="193" place="1">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="3.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4" punct="vg:7">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="e" type="vs" value="1" rule="383" place="6">e</seg>ill<rhyme label="a" id="2" gender="m" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>t</rhyme></w>,</l>
						<l n="4" num="1.4" lm="7" met="7"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">n</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="4.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="4.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="4.6" punct="pe:7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<rhyme label="a" id="2" gender="m" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="pe ps">an</seg>t</rhyme></w> !…</l>
					</lg>
					<lg n="2" type="regexp" rhyme="aaaa">
						<l n="5" num="2.1" lm="7" met="7"><w n="5.1" punct="vg:2">L<seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="5.2" punct="vg:3">B<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>c</w>, <w n="5.3">C<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="5.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>f<rhyme label="a" id="3" gender="m" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd</rhyme></w></l>
						<l n="6" num="2.2" lm="7" met="7"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2">n</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="6.4">tr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.5" punct="pe:7">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rn<rhyme label="a" id="3" gender="m" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pe">a</seg>rd</rhyme></w> !</l>
						<l n="7" num="2.3" lm="7" met="7">— <w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="7.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="7.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="7.7">s<rhyme label="a" id="4" gender="m" type="a" stanza="4"><seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>il</rhyme></w></l>
						<l n="8" num="2.4" lm="7" met="7"><w n="8.1">Pr<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="8.3">m<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="8.4" punct="pt:7"><rhyme label="a" id="4" gender="m" type="e" stanza="4"><seg phoneme="œ" type="vs" value="1" rule="286" place="7" punct="pt ti">œ</seg>il</rhyme></w>. —</l>
					</lg>
					<lg n="3" type="regexp" rhyme="aaa">
						<l n="9" num="3.1" lm="7" met="7">… <hi rend="ital"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">Om</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c</w> <w n="9.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="9.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w></hi> <w n="9.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<rhyme label="a" id="5" gender="m" type="a" stanza="5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</rhyme></w></l>
						<l n="10" num="3.2" lm="7" met="7"><w n="10.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="10.3" punct="vg:7">br<seg phoneme="y" type="vs" value="1" rule="445" place="6">û</seg>l<rhyme label="a" id="5" gender="m" type="e" stanza="5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>t</rhyme></w>,</l>
						<l n="11" num="3.3" lm="7" met="7"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="11.3" punct="dp:7">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<rhyme label="a" id="6" gender="m" type="a" stanza="6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="dp">a</seg></rhyme></w> :</l>
					</lg>
					<lg n="4" type="regexp" rhyme="bb">
						<l n="12" num="4.1" lm="7" met="7"><w n="12.1" punct="vg:3">L<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>c<seg phoneme="i" type="vs" value="1" rule="dc-1" place="2">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg></w>, <w n="12.2">M<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>z</w>’<w n="12.3" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="341" place="5">A</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<rhyme label="b" id="7" gender="m" type="a" stanza="6"><seg phoneme="o" type="vs" value="1" rule="444" place="7" punct="vg">o</seg></rhyme></w>,</l>
						<l n="13" num="4.2" lm="7" met="7"><w n="13.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w>-<w n="13.2" punct="vg:4">P<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="13.3" punct="vg:7">Di<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<rhyme label="b" id="7" gender="m" type="e" stanza="6"><seg phoneme="o" type="vs" value="1" rule="444" place="7" punct="vg">o</seg></rhyme></w>,</l>
					</lg>
					<lg n="5" type="regexp" rhyme="a">
						<l n="14" num="5.1" lm="5">— <w n="14.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">ON</seg></w> <w n="14.2" punct="pt:5">P<seg phoneme="y" type="vs" value="1" rule="450" place="2">U</seg>LC<seg phoneme="i" type="vs" value="1" rule="467" place="3">I</seg>N<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">E</seg>LL<rhyme label="a" id="6" gender="m" type="e" stanza="6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pt ti">A</seg></rhyme></w>. —</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Mergelina-Venerdi</placeName> aprile 15.
						</dateline>
					</closer>
				</div></body></text></TEI>