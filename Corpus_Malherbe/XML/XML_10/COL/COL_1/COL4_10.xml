<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL4" modus="cp" lm_max="12" metProfile="6, 8, (6+6)" form="strophe unique" schema="1(aababcc)">
				<head type="main">L’ÉVENTAIL.</head>
				<head type="tune">Air : Noté, N°. 4.</head>
				<lg n="1" type="septain" rhyme="aababcc">
					<l n="1" num="1.1" lm="6" met="6"><space unit="char" quantity="12"></space><w n="1.1">L</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>V<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">EN</seg>T<seg phoneme="a" type="vs" value="1" rule="307" place="3">A</seg>IL</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.5" punct="vg:6">m<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" punct="vg">ain</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="12"></space><w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="2.5" punct="pv:6">j<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="pv">in</seg></rhyme></w> ;</l>
					<l n="3" num="1.3" lm="12" met="6+6" met_alone="True"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="3.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="3.3">fl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>r</w><caesura></caesura> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.8">s</w>’<w n="3.9"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="9">e</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.10"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11">e</seg>c</w> <w n="3.11" punct="pt:12">gr<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">c</w>’<w n="4.5" punct="dp:4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4" punct="dp">e</seg>st</w> : <w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="4.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="4.8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></w></l>
					<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="5.2" punct="vg:2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2" punct="vg">en</seg>t</w>, <w n="5.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="5.4">f<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="5.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="5.7" punct="dp:8">p<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
					<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">C</w>’<w n="6.2" punct="vg:2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>t</w>, <w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.4" punct="vg:4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" punct="vg">e</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="6.6">li<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="6.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="6.8" punct="vg:8">v<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="vg">en</seg>t</rhyme></w>,</l>
					<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="7.2">C<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="7.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="7.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="7.5" punct="pt:8">C<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="ri_1" place="8" punct="pt">en</seg>t</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>