<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL48" modus="sm" lm_max="7" metProfile="7" form="suite de strophes" schema="6[abab]">
				<head type="main">RONDE.</head>
				<head type="tune">Air : Comm’ j’l’étrille, trille, etc.</head>
				<head type="tune">Noté, N°. 28.</head>
				<lg n="1" type="regexp" rhyme="abab">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">E</seg></w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="y" type="vs" value="1" rule="391" place="2">eu</seg>s</w> <w n="1.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="1.6">f<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5" punct="pv:7">C<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pv">é</seg></rhyme></w> ;</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1">D</w>’<w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="3.3" punct="vg:3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="3.6" punct="vg:7">V<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="7" met="7"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">l</w>’<w n="4.4"><seg phoneme="y" type="vs" value="1" rule="391" place="3">eu</seg>s</w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="4.6" punct="pt:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
				<div type="section" n="1">
					<head type="main">Bis</head>
					<lg n="1" type="regexp" rhyme="ab">
						<l n="5" num="1.1" lm="7" met="7"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="5.6" punct="vg:7">V<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="6" num="1.2" lm="7" met="7"><w n="6.1">C</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="6.6" punct="vg:7">C<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></rhyme></w>,</l>
					</lg>
				</div>
				<lg n="2" type="regexp" rhyme="abab">
					<l n="7" num="2.1" lm="7" met="7"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">E</seg></w> <w n="7.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="7.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>t</w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5" punct="vg:7">V<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="8" num="2.2" lm="7" met="7"><w n="8.1" punct="pv:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="pv">on</seg></w> ; <w n="8.2">c</w>’<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="8.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.5">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.7" punct="pt:7">C<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></rhyme></w>.</l>
					<l n="9" num="2.3" lm="7" met="7"><w n="9.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="9.2" punct="vg:2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="9.3" punct="ps:3">ou<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="ps">i</seg></w>… <w n="9.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.6">sç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="9.7">gu<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="10" num="2.4" lm="7" met="7"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="10.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="10.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4" punct="pt:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
				<div type="section" n="2">
					<head type="main">Bis</head>
					<lg n="1" type="regexp" rhyme="ab">
						<l n="11" num="1.1" lm="7" met="7"><w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="11.6" punct="vg:7">V<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="12" num="1.2" lm="7" met="7"><w n="12.1">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="12.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="12.6" punct="vg:7">C<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></rhyme></w>,</l>
					</lg>
				</div>
				<lg n="3" type="regexp" rhyme="abab">
					<l n="13" num="3.1" lm="7" met="7"><w n="13.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>SP<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">E</seg>CT</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="13.4" punct="vg:7">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ct<rhyme label="a" id="7" gender="f" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="COL48_1" place="7">e</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="14" num="3.2" lm="7" met="7"><w n="14.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="14.3">m</w>’<w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="14.5" punct="pt:7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ø" type="vs" value="1" rule="405" place="6">eu</seg>r<rhyme label="b" id="8" gender="m" type="a" stanza="4"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></rhyme></w>.</l>
					<l n="15" num="3.3" lm="7" met="7"><w n="15.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="15.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="15.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="15.5" punct="vg:7">V<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<rhyme label="a" id="7" gender="f" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="16" num="3.4" lm="7" met="7"><w n="16.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="16.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">n</w>’<w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="16.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="16.7" punct="pt:7">C<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<rhyme label="b" id="8" gender="m" type="e" stanza="4"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
				<div type="section" n="3">
					<head type="main">Bis</head>
					<lg n="1" type="regexp" rhyme="ab">
						<l n="17" num="1.1" lm="7" met="7"><w n="17.1">C</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="17.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="17.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="17.6" punct="vg:7">V<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<rhyme label="a" id="9" gender="f" type="a" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="18" num="1.2" lm="7" met="7"><w n="18.1">C</w>’<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="18.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="18.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="18.6" punct="vg:7">C<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<rhyme label="b" id="10" gender="m" type="a" stanza="5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></rhyme></w>,</l>
					</lg>
				</div>
				<lg n="4" type="regexp" rhyme="abab">
					<l n="19" num="4.1" lm="7" met="7"><w n="19.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">AN</seg>S</w> <w n="19.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">Di<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.5" punct="vg:7">V<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<rhyme label="a" id="9" gender="f" type="e" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="20" num="4.2" lm="7" met="7"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="20.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.4">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.6" punct="vg:7">C<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<rhyme label="b" id="10" gender="m" type="e" stanza="5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></rhyme></w>,</l>
					<l n="21" num="4.3" lm="7" met="7"><w n="21.1">J</w>’<w n="21.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="21.3">l</w>’<w n="21.4" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>th<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<rhyme label="a" id="11" gender="f" type="a" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="22" num="4.4" lm="7" met="7"><w n="22.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="22.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="22.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="22.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="22.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="22.6" punct="pt:7">gr<rhyme label="b" id="12" gender="m" type="a" stanza="6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
				<div type="section" n="4">
					<head type="main">Bis</head>
					<lg n="1" type="regexp" rhyme="ab">
						<l n="23" num="1.1" lm="7" met="7"><w n="23.1">C</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="23.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="23.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="23.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="23.6" punct="vg:7">V<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<rhyme label="a" id="11" gender="f" type="e" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="24" num="1.2" lm="7" met="7"><w n="24.1">C</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="24.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="24.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="24.6" punct="vg:7">C<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<rhyme label="b" id="12" gender="m" type="e" stanza="6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></rhyme></w>,</l>
					</lg>
				</div>
			</div></body></text></TEI>