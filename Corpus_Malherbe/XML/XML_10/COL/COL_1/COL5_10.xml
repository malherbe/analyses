<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL5" modus="sp" lm_max="8" metProfile="8, 6, 2" form="suite périodique" schema="4(ababccdeedde)">
				<head type="main">LA CRAINTIVE RASSURÉE,</head>
				<head type="form">PARODIE.</head>
				<head type="tune">Air : Noté, N°. 5.</head>
				<lg n="1" type="douzain" rhyme="ababccdeedde">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">E</seg></w> <w n="1.2">gr<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="1.3" punct="vg:4">L<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</w>, <w n="1.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="1.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="1.6" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="2.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="2.2"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3" punct="pv:6">F<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>v<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2" punct="vg:3">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">î</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="3.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">î</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d</w> <w n="3.5">l</w>’<w n="3.6" punct="vg:8"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="4.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w>-<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="4.4" punct="pv:6">L<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.2" punct="vg:4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>c</w>, <w n="5.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="5.5">l</w>’<w n="5.6" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>r<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></w>.</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.4">s</w>’<w n="6.5" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="c" id="3" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg></rhyme></w> ;</l>
					<l n="7" num="1.7" lm="6" met="6"><space unit="char" quantity="4"></space><w n="7.1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w>-<w n="7.2" punct="vg:2">d<seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="vg">à</seg></w>, <w n="7.3">ou<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w>-<w n="7.4" punct="vg:4">d<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="7.5">ou<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w>-<w n="7.6" punct="vg:6">d<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="6" punct="vg">à</seg></rhyme></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">m<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7">ain</seg></w> <w n="8.7" punct="vg:8">l<rhyme label="e" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="vg">à</seg></rhyme></w>,</l>
					<l n="9" num="1.9" lm="2" met="2"><space unit="char" quantity="12"></space><w n="9.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="9.2" punct="pv:2">l<rhyme label="e" id="5" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="pv">à</seg></rhyme></w> ;</l>
					<l n="10" num="1.10" lm="6" met="6"><space unit="char" quantity="4"></space><w n="10.1" punct="pe:1">H<seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">o</seg></w> ! <w n="10.2" punct="pe:2">h<seg phoneme="o" type="vs" value="1" rule="444" place="2" punct="pe">o</seg></w> ! <w n="10.3" punct="pe:3">h<seg phoneme="o" type="vs" value="1" rule="444" place="3" punct="pe">o</seg></w> ! <w n="10.4" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">A</seg>h</w> ! <w n="10.5" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">a</seg>h</w> ! <w n="10.6" punct="pe:6"><rhyme label="d" id="5" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe">a</seg>h</rhyme></w> !</l>
					<l n="11" num="1.11" lm="8" met="8"><w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="11.3"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ttr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6" punct="vg:8">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></w>,</l>
					<l n="12" num="1.12" lm="2" met="2"><space unit="char" quantity="12"></space><w n="12.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="12.2" punct="pt:2">l<rhyme label="e" id="6" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="pt">à</seg></rhyme></w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2" type="douzain" rhyme="ababccdeedde">
					<l n="13" num="2.1" lm="8" met="8"><w n="13.1" punct="vg:2">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">ON</seg></w>, <w n="13.2">L<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="13.3">l</w>’<w n="13.4"><seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="13.5">s</w>’<w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="13.7" punct="ps:8">v<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="ps">a</seg></rhyme></w>…</l>
					<l n="14" num="2.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="14.1">S</w>’<w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="14.3">s</w>’<w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="14.5" punct="vg:3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg></w>, <w n="14.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="14.7" punct="pe:6">d<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>mm<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></w> !</l>
					<l n="15" num="2.3" lm="8" met="8"><w n="15.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="15.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w>-<w n="15.4">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="15.5" punct="pi:4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" punct="pi">un</seg></w> ? <w n="15.6" punct="vg:5">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="5" punct="vg">i</seg></w>, <w n="15.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.8" punct="ps:8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>l<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="ps">à</seg></rhyme></w>…</l>
					<l n="16" num="2.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="16.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="16.2">j</w>’<w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="16.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.6" punct="dp:6">pl<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></rhyme></w> :</l>
					<l n="17" num="2.5" lm="8" met="8"><w n="17.1">Qu</w>’<w n="17.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="17.4" punct="pe:4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="pe">an</seg>t</w> ! <w n="17.5">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rd</w>-<w n="17.7">t</w>-<w n="17.8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="17.9" punct="pi:8">p<rhyme label="c" id="9" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pi">a</seg>s</rhyme></w> ?</l>
					<l n="18" num="2.6" lm="8" met="8"><w n="18.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="3">e</seg></w> <w n="18.3" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="18.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="18.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>r</w> <w n="18.6" punct="pt:8">L<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>c<rhyme label="c" id="9" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></w>.</l>
					<l n="19" num="2.7" lm="6" met="6"><space unit="char" quantity="4"></space><w n="19.1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w>-<w n="19.2" punct="vg:2">d<seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="vg">à</seg></w>, <w n="19.3">ou<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w>-<w n="19.4" punct="vg:4">d<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="19.5">ou<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w>-<w n="19.6" punct="dp:6">d<rhyme label="d" id="10" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="6" punct="dp">à</seg></rhyme></w> :</l>
					<l n="20" num="2.8" lm="8" met="8"><w n="20.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>dr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w>-<w n="20.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="20.3">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="20.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7">ain</seg></w> <w n="20.6" punct="vg:8">l<rhyme label="e" id="10" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="vg">à</seg></rhyme></w>,</l>
					<l n="21" num="2.9" lm="2" met="2"><space unit="char" quantity="12"></space><w n="21.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="21.2" punct="pi:2">l<rhyme label="e" id="11" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="pi">à</seg></rhyme></w> ?</l>
					<l n="22" num="2.10" lm="6" met="6"><space unit="char" quantity="4"></space><w n="22.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="22.2" punct="pe:2"><seg phoneme="o" type="vs" value="1" rule="444" place="2" punct="pe">o</seg>h</w> ! <w n="22.3" punct="pe:3"><seg phoneme="o" type="vs" value="1" rule="444" place="3" punct="pe">o</seg>h</w> ! <w n="22.4" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">A</seg>h</w> ! <w n="22.5" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">a</seg>h</w> ! <w n="22.6" punct="pe:6"><rhyme label="d" id="11" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe">a</seg>h</rhyme></w> !</l>
					<l n="23" num="2.11" lm="8" met="8"><w n="23.1">C</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="23.3"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ttr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="23.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.6" punct="vg:8">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="d" id="12" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></w>,</l>
					<l n="24" num="2.12" lm="2" met="2"><space unit="char" quantity="12"></space><w n="24.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="24.2" punct="pt:2">l<rhyme label="e" id="12" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="pt">à</seg></rhyme></w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3" type="douzain" rhyme="ababccdeedde">
					<l n="25" num="3.1" lm="8" met="8"><w n="25.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>S<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">E</seg>TT<seg phoneme="ə" type="ee" value="0" rule="e-23">E</seg></w> <w n="25.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="25.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="25.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="25.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>dr<rhyme label="a" id="13" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</rhyme></w></l>
					<l n="26" num="3.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="26.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="26.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="26.3" punct="dp:6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>cr<rhyme label="b" id="14" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></rhyme></w> :</l>
					<l n="27" num="3.3" lm="8" met="8"><w n="27.1">L<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="27.2">l</w>’<w n="27.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>tr</w>’<w n="27.4" punct="vg:5"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>t</w>, <w n="27.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="27.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="27.7">dr<rhyme label="a" id="13" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</rhyme></w></l>
					<l n="28" num="3.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="28.1">D</w>’<w n="28.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd</w> <w n="28.3">l</w>’<w n="28.4"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="28.5">s</w>’<w n="28.6"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="28.7" punct="pt:6">j<rhyme label="b" id="14" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></w>.</l>
					<l n="29" num="3.5" lm="8" met="8"><w n="29.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="29.2">n</w>’<w n="29.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="29.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t</w> <w n="29.5" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</w>, <w n="29.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="29.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="29.8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>l<rhyme label="c" id="15" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></rhyme></w></l>
					<l n="30" num="3.6" lm="8" met="8"><w n="30.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="30.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="30.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>t</w> <w n="30.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="30.5" punct="vg:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="c" id="15" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></w>,</l>
					<l n="31" num="3.7" lm="6" met="6"><space unit="char" quantity="4"></space><w n="31.1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w>-<w n="31.2" punct="vg:2">d<seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="vg">à</seg></w>, <w n="31.3">ou<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w>-<w n="31.4" punct="vg:4">d<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="31.5">ou<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w>-<w n="31.6" punct="vg:6">d<rhyme label="d" id="16" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="6" punct="vg">à</seg></rhyme></w>,</l>
					<l n="32" num="3.8" lm="8" met="8"><w n="32.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="32.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="32.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="32.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="32.5">tr<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="32.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w> <w n="32.7" punct="vg:8">l<rhyme label="e" id="16" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="vg">à</seg></rhyme></w>,</l>
					<l n="33" num="3.9" lm="2" met="2"><space unit="char" quantity="12"></space><w n="33.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="33.2" punct="pe:2">l<rhyme label="e" id="17" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="pe">à</seg></rhyme></w> !</l>
					<l n="34" num="3.10" lm="6" met="6"><space unit="char" quantity="4"></space><w n="34.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="34.2" punct="pe:2"><seg phoneme="o" type="vs" value="1" rule="444" place="2" punct="pe">o</seg>h</w> ! <w n="34.3" punct="pe:3"><seg phoneme="o" type="vs" value="1" rule="444" place="3" punct="pe">o</seg>h</w> ! <w n="34.4" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">A</seg>h</w> ! <w n="34.5" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">a</seg>h</w> ! <w n="34.6" punct="pe:6"><rhyme label="d" id="17" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe">a</seg>h</rhyme></w> !</l>
					<l n="35" num="3.11" lm="8" met="8"><w n="35.1">L</w>’<w n="35.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="35.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="35.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="35.5" punct="vg:8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>l<rhyme label="d" id="18" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="vg">à</seg></rhyme></w>,</l>
					<l n="36" num="3.12" lm="2" met="2"><space unit="char" quantity="12"></space><w n="36.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="36.2" punct="pe:2">l<rhyme label="e" id="18" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="pe">à</seg></rhyme></w> !</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="4" type="douzain" rhyme="ababccdeedde">
					<l n="37" num="4.1" lm="8" met="8"><w n="37.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>R</w> <w n="37.2">qu<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="37.3">f<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="37.4">l</w>’<w n="37.5"><seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="37.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="a" id="19" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></rhyme></w></l>
					<l n="38" num="4.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="38.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="38.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="38.3" punct="pv:6">j<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<rhyme label="b" id="20" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="39" num="4.3" lm="8" met="8"><w n="39.1">Tr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="39.2">f<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="39.3">L<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="39.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ge<rhyme label="a" id="19" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="316" place="8">a</seg></rhyme></w></l>
					<l n="40" num="4.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="40.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="40.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="40.3" punct="pt:6">m<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>d<rhyme label="b" id="20" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></w>.</l>
					<l n="41" num="4.5" lm="8" met="8"><w n="41.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="41.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="41.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="41.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="41.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="41.6">s</w>’<w n="41.7" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>rh<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<rhyme label="c" id="21" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></w>.</l>
					<l n="42" num="4.6" lm="8" met="8"><w n="42.1">L<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="42.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rs</w> <w n="42.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="42.4" punct="vg:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<rhyme label="c" id="21" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></w>,</l>
					<l n="43" num="4.7" lm="6" met="6"><space unit="char" quantity="4"></space><w n="43.1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w>-<w n="43.2" punct="vg:2">d<seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="vg">à</seg></w>, <w n="43.3">ou<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w>-<w n="43.4" punct="vg:4">d<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="43.5">ou<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w>-<w n="43.6" punct="vg:6">d<rhyme label="d" id="22" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="6" punct="vg">à</seg></rhyme></w>,</l>
					<l n="44" num="4.8" lm="8" met="8"><w n="44.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="44.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="44.3"><seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="44.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w>-<w n="44.5" punct="vg:8">l<rhyme label="e" id="22" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="vg">à</seg></rhyme></w>,</l>
					<l n="45" num="4.9" lm="2" met="2"><space unit="char" quantity="12"></space><w n="45.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="45.2" punct="pv:2">l<rhyme label="e" id="23" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="pv">à</seg></rhyme></w> ;</l>
					<l n="46" num="4.10" lm="6" met="6"><space unit="char" quantity="4"></space><w n="46.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="46.2" punct="pe:2"><seg phoneme="o" type="vs" value="1" rule="444" place="2" punct="pe">o</seg>h</w> ! <w n="46.3" punct="pe:3"><seg phoneme="o" type="vs" value="1" rule="444" place="3" punct="pe">o</seg>h</w> ! <w n="46.4" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">A</seg>h</w> ! <w n="46.5" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">a</seg>h</w> ! <w n="46.6" punct="pe:6"><rhyme label="d" id="23" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe">a</seg>h</rhyme></w> !</l>
					<l n="47" num="4.11" lm="8" met="8"><w n="47.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="47.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="47.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>gr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w> <w n="47.4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="7">o</seg>p</w> <w n="47.5" punct="vg:8">l<rhyme label="d" id="24" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="vg">à</seg></rhyme></w>,</l>
					<l n="48" num="4.12" lm="2" met="2"><space unit="char" quantity="12"></space><w n="48.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="48.2" punct="pt:2">l<rhyme label="e" id="24" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="pt">à</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>