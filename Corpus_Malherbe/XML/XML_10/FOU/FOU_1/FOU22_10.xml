<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA NÉGRESSE BLONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="FOU">
					<name>
						<forename>Georges</forename>
						<surname>FOUREST</surname>
					</name>
					<date from="1864" to="1945">1864-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1172 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>La Négresse blonde</title>
						<author>Georges Fourest</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/La_N%C3%A9gresse_blonde_(recueil)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Négresse blonde</title>
								<author>Georges Fourest</author>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1269960g</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A MESSEIN</publisher>
									<date when="1909">1909</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1909">1909</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CARNAVAL DE CHEFS-D’ŒUVRE</head><div type="poem" key="FOU22" modus="cm" lm_max="12" metProfile="6+6" form="sonnet non classique" schema="abba cdcd eef gfg">
					<head type="main">IPHIGÉNIE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Quoi ! le sang d’une fille innocente était <lb></lb>
									nécessaire au départ d’une flotte et au succès <lb></lb>
									d’une guerre ?
								</quote>
								<bibl>
									<name>Joseph DE MAISTRE</name> (Sur les sacrifices, II.)
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="1.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>ts</w> <w n="1.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="1.4" punct="dp:4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="dp">o</seg>rts</w> : <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>t</w><caesura></caesura> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.7">c<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="1.10">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rp<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</rhyme></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="2.3">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="2.5">Gr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>cs</w><caesura></caesura> <w n="2.6">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="2.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="P">u</seg>r</w> <w n="2.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="10" mp="C">eu</seg>r</w> <w n="2.9">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="3.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="3.5">Tr<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="3.8">r<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">H<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="4.4" punct="vg:8">P<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">â</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</w>, <w n="4.5">l</w>’<w n="4.6">h<seg phoneme="o" type="vs" value="1" rule="415" place="9">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="4.7" punct="pt:12">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg>p<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="cdcd">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="5.2">d</w>’<w n="5.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="5.5">qu</w>’<w n="5.6"><seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">U</seg>l<seg phoneme="i" type="vs" value="1" rule="493" place="8">y</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="5.8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10">ain</seg></w> <w n="5.9" punct="vg:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>fr<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>g<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="366" place="3" mp="M">e</seg>mn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.3">r<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="6.5" punct="vg:8">r<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</w>, <w n="6.6">l</w>’<w n="6.7">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="6.9">p<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</rhyme></w></l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">d<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>pl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="7.3">m<seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="7.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>r</w> <w n="7.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rs</w> <w n="7.7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">qu</w>’<w n="8.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="8.3">n</w>’<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="8.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" mp="M">in</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="8.8">b<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg>x</w> <w n="8.9"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="8.10" punct="pt:12">v<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>p<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="eef">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="9.3" punct="vg:3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="9.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="9.6" punct="vg:6">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6" punct="vg" caesura="1">e</seg>ds</w>,<caesura></caesura> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="9.8">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.9"><seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">I</seg>ph<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>n<rhyme label="e" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="10.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="10.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="10.4">d<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>ts</w><caesura></caesura> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="10.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="10.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>n<rhyme label="e" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">s</w>’<w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w> <w n="11.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="11.5">fl<seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>ts</w><caesura></caesura> <w n="11.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="11.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="11.8">s<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r</w> <w n="11.9" punct="ps:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>ff<rhyme label="f" id="6" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="ps">an</seg>t</rhyme></w>…</l>
					</lg>
					<lg n="4" rhyme="gfg">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="vg:1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>rs</w>, <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="2" mp="M">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="12.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5" mp="M">ai</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="12.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="12.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="12.6" punct="vg:10">s<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="12.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="12.8">m<rhyme label="g" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="13.2">p<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="13.5">r<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="13.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="8">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="13.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="13.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>f<rhyme label="f" id="6" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</rhyme></w></l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="14.2" punct="dp:3">br<seg phoneme="a" type="vs" value="1" rule="307" place="2">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="dp in" mp="F">e</seg></w> : « <w n="14.3">Ç<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="14.4">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="14.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="14.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="14.7" punct="pe:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>m<rhyme label="g" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> ! »</l>
					</lg>
				</div></body></text></TEI>