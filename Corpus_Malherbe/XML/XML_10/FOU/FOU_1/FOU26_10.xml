<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA NÉGRESSE BLONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="FOU">
					<name>
						<forename>Georges</forename>
						<surname>FOUREST</surname>
					</name>
					<date from="1864" to="1945">1864-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1172 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>La Négresse blonde</title>
						<author>Georges Fourest</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/La_N%C3%A9gresse_blonde_(recueil)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Négresse blonde</title>
								<author>Georges Fourest</author>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1269960g</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A MESSEIN</publisher>
									<date when="1909">1909</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1909">1909</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">BALLADE <lb></lb>POUR FAIRE CONNAÎTRE MES OCCUPATIONS ORDINAIRES</head><div type="poem" key="FOU26" modus="sm" lm_max="8" metProfile="8" form="petite ballade" schema="3(ababbcbc) bcbc">
					<head type="main">BALLADE</head>
					<head type="sub">POUR FAIRE CONNAÎTRE MES OCCUPATIONS ORDINAIRES</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Au docteur Georges Boileau. <lb></lb>
									Pauvres gens que les gens !
								</quote>
								<bibl>
									<name>P. V.</name>
								</bibl>
							</cit>
							<cit>
								<quote>
									Considerato lilia agri…
								</quote>
								<bibl>
									<name>J. C.</name>
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1" type="huitain" rhyme="ababbcbc">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="1.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="1.4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="4">en</seg>s</w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ci<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg>s</rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ts</w> <w n="2.2"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ds</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.5">l<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="3.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="3.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="3.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="3.8">hu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ssi<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="4.2">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="4.5" punct="vg:8">M<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>gd<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg>s</w> <w n="5.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.5" punct="vg:8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="b" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="6.2">c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="6.5" punct="vg:8">f<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="c" id="4" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>s</rhyme></w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">D</w>’<w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="7.3">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="7.4">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="7.6" punct="dp:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="b" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="8.2">j</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ttr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="8.5" punct="pe:8">h<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="c" id="4" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>s</rhyme></w> !</l>
					</lg>
					<lg n="2" type="huitain" rhyme="ababbcbc">
						<l n="9" num="2.1" lm="8" met="8"><w n="9.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="9.2" punct="vg:3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" punct="vg">un</seg>s</w>, <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="9.4" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ssi<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg>s</rhyme></w>,</l>
						<l n="10" num="2.2" lm="8" met="8">— <w n="10.1">D<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>x</w> <w n="10.2" punct="pe:3">t<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="pe in">é</seg>s</w> ! (<w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="10.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rgu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="10.7" punct="pe:8">pl<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !)</l>
						<l n="11" num="2.3" lm="8" met="8"><w n="11.1" punct="vg:2">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>nt</w>, <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="11.3" punct="vg:4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg>t</w>, <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="11.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="11.6">p<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>ci<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</rhyme></w></l>
						<l n="12" num="2.4" lm="8" met="8"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="12.2">r<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.5" punct="dp:8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
						<l n="13" num="2.5" lm="8" met="8">« <hi rend="ital"><w n="13.1" punct="pe:3">P<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="13.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="3" punct="pe">en</seg>s</w></hi> ! » <w n="13.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="13.5">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rl<rhyme label="b" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="14" num="2.6" lm="8" met="8"><w n="14.1">C</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="14.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="14.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r</w> <w n="14.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="14.8" punct="pe:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="c" id="8" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>s</rhyme></w> !</l>
						<l n="15" num="2.7" lm="8" met="8"><w n="15.1">v<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="15.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3" punct="vg:4">br<seg phoneme="y" type="vs" value="1" rule="445" place="3">û</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="15.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.5" punct="pe:8">ph<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="b" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
						<l n="16" num="2.8" lm="8" met="8"><w n="16.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="16.2">j</w>’<w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ttr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="16.5" punct="pe:8">h<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="c" id="8" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>s</rhyme></w> !</l>
					</lg>
					<lg n="3" type="huitain" rhyme="ababbcbc">
						<l n="17" num="3.1" lm="8" met="8"><w n="17.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="17.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="17.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="17.4">ph<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>s</w> <w n="17.5">gr<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>ssi<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</rhyme></w></l>
						<l n="18" num="3.2" lm="8" met="8"><w n="18.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="18.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="18.5" punct="dp:8">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
						<l n="19" num="3.3" lm="8" met="8">« <w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="19.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="19.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="19.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="19.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ssi<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></rhyme></w></l>
						<l n="20" num="3.4" lm="8" met="8"><w n="20.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2" punct="pe:5">C<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="pe">e</seg></w> ! » <w n="20.3" punct="vg:6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg></w>, <w n="20.4" punct="vg:7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="vg">on</seg></w>, <w n="20.5" punct="pe:8">l<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
						<l n="21" num="3.5" lm="8" met="8"><w n="21.1" punct="vg:2">M<seg phoneme="e" type="vs" value="1" rule="353" place="1">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="vg">eu</seg>rs</w>, <w n="21.2">s<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="21.3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="21.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="21.5">l</w>’<w n="21.6" punct="vg:8"><rhyme label="b" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">Ai</seg>sn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="22" num="3.6" lm="8" met="8"><w n="22.1">M<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="22.3">p<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="22.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="22.5">c<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="c" id="12" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</rhyme></w></l>
						<l n="23" num="3.7" lm="8" met="8"><w n="23.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="23.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>pr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="23.3">l</w>’<w n="23.4" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>l<rhyme label="b" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
						<l n="24" num="3.8" lm="8" met="8"><w n="24.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="24.2">j</w>’<w n="24.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ttr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="24.5" punct="pe:8">h<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="c" id="12" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>s</rhyme></w> !</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="bcbc">
						<head type="main">ENVOI</head>
						<l n="25" num="4.1" lm="8" met="8"><w n="25.1" punct="vg:2">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="25.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="25.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="25.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="25.6" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<rhyme label="b" id="13" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="26" num="4.2" lm="8" met="8"><w n="26.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="26.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="26.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="26.4" punct="dp:8">J<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="c" id="14" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="dp">on</seg>s</rhyme></w> :</l>
						<l n="27" num="4.3" lm="8" met="8"><w n="27.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2">P<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="27.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="27.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="27.5" punct="pe:8">H<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<rhyme label="b" id="13" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
						<l n="28" num="4.4" lm="8" met="8"><w n="28.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="28.2">j</w>’<w n="28.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ttr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="28.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="28.5" punct="pe:8">h<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="c" id="14" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>s</rhyme></w> !</l>
					</lg>
				</div></body></text></TEI>