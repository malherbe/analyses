<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Enluminures</title>
				<title type="medium">Édition électronique</title>
				<author key="ELS">
					<name>
						<forename>Max</forename>
						<surname>ELSKAMP</surname>
					</name>
					<date from="1862" to="1931">1862-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>718 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">ELS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Enluminures</title>
						<author>Max Elskamp</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/maxelskampenluminures.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Enluminures / paysages, heures, vies, chansons, grotesques</title>
						<author>Max Elskamp</author>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k10575703.r=%22max%20elskamp%22?rk=42918;4</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>Paul Lacomblez, Éditeur</publisher>
							<date when="1898">1898</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1898">1898</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS</head><div type="poem" key="ELS19" modus="sp" lm_max="8" metProfile="5, (8)" form="suite périodique" schema="4(abab)">
					<head type="number">I</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="5" met="5"><space unit="char" quantity="6"></space><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="1.2">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d</w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="1.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</rhyme></w></l>
						<l n="2" num="1.2" lm="5" met="5"><space unit="char" quantity="6"></space><w n="2.1">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="2.3" punct="vg:5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>s<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="5" met="5"><space unit="char" quantity="6"></space><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="3.2">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="3.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</rhyme></w></l>
						<l n="4" num="1.4" lm="5" met="5"><space unit="char" quantity="6"></space><w n="4.1" punct="vg:1"><seg phoneme="u" type="vs" value="1" rule="426" place="1" punct="vg">où</seg></w>, <w n="4.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4" punct="vg:5">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rch<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg">é</seg></rhyme></w>,</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="5" met="5"><space unit="char" quantity="6"></space><w n="5.1">l</w>’<w n="5.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="5.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="5.5">l<rhyme label="a" id="8" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="5" met="5"><space unit="char" quantity="6"></space><w n="6.1">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">m<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>ts</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="6.4" punct="vg:5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<rhyme label="b" id="7" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></rhyme></w>,</l>
						<l n="7" num="2.3" lm="5" met="5"><space unit="char" quantity="6"></space><w n="7.1">c</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="7.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="7.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="7.6">j<rhyme label="a" id="8" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="5" met="5"><space unit="char" quantity="6"></space><w n="8.1">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg>s</w> <w n="8.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="8.5" punct="pt:5">p<rhyme label="b" id="7" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="pt">ai</seg>x</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="5" met="5"><space unit="char" quantity="6"></space><w n="9.1" punct="vg:1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1" punct="vg">O</seg>r</w>, <w n="9.2" punct="pe:2">g<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2" punct="pe">ai</seg></w> ! <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4" punct="vg:5">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rmi<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></rhyme></w>,</l>
						<l n="10" num="3.2" lm="5" met="5"><space unit="char" quantity="6"></space><w n="10.1" punct="pe:2">s<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="pe">u</seg>t</w> ! <w n="10.2">l</w>’<w n="10.3" punct="vg:5"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rg<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="5" met="5"><space unit="char" quantity="6"></space><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="11.2" punct="pe:2">j<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="pe">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> ! <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4" punct="vg:5">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rg<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="5" met="5"><space unit="char" quantity="6"></space><w n="12.1">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">m<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="12.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="12.4" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></w>,</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="5" met="5"><space unit="char" quantity="6"></space><w n="13.1">c</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="13.3" punct="vg:2">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2" punct="vg">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="13.5">br<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="13.6">n<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</rhyme></w></l>
						<l n="14" num="4.2" lm="5" met="5"><space unit="char" quantity="6"></space><w n="14.1">cu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="14.2" punct="vg:5">b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg>s</rhyme></w>,</l>
						<l n="15" num="4.3" lm="5" met="5"><space unit="char" quantity="6"></space><w n="15.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">e</seg>t</w>, <w n="15.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="15.3" punct="vg:5">ch<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="5" punct="vg">u</seg></rhyme></w>,</l>
						<l n="16" num="4.4" lm="5" met="5"><space unit="char" quantity="6"></space><w n="16.1">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="16.3" punct="pv:5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rch<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pv">er</seg>s</rhyme></w> ;</l>
					</lg>
					<lg n="5" rhyme="None">
						<l rhyme="none" n="17" num="5.1" lm="5" met="5"><space unit="char" quantity="6"></space><w n="17.1">pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="17.2">j<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="17.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="17.5">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d</w></l>
						<l rhyme="none" n="18" num="5.2" lm="5" met="5"><space unit="char" quantity="6"></space><w n="18.1">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="18.2" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>ts</w>, <w n="18.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="18.4" punct="vg:5">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg>s</w>,</l>
						<l rhyme="none" n="19" num="5.3" lm="5" met="5"><space unit="char" quantity="6"></space><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>ps</w></l>
						<l rhyme="none" n="20" num="5.4" lm="5" met="5"><space unit="char" quantity="6"></space><w n="20.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="20.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="20.3" punct="dp:5">c<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="dp">e</seg>s</w> :</l>
					</lg>
					<lg n="6" rhyme="None">
						<l rhyme="none" n="21" num="6.1" lm="8"><w n="21.1">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="2">ou</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="21.3">d</w>’<w n="21.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rgu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="21.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></w>.</l>
					</lg>
				</div></body></text></TEI>