<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FAMILLE</head><div type="poem" key="DES389" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="2(abba) 1(abab)">
					<head type="main">À MA SŒUR CÉCILE</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="1.4" punct="vg:6">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="1.6">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="1.8" punct="vg:12">br<seg phoneme="y" type="vs" value="1" rule="445" place="11" mp="M">û</seg>l<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="2.3">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5" mp="P">e</seg>rs</w> <w n="2.5">l<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="2.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="2.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="2.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="2.9" punct="pv:12"><seg phoneme="e" type="vs" value="1" rule="353" place="11" mp="M">e</seg>ff<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pv">o</seg>rt</rhyme></w> ;</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="3.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">m</w>’<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="3.6" punct="dp:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="dp in" caesura="1">i</seg>t</w> :<caesura></caesura> « <w n="3.7">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.8">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.9">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="3.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="3.11" punct="dp:12">s<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="dp">o</seg>rt</rhyme></w> : »</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="4.5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="4.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="4.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="4.8">h<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.9" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>bl<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="5.2" punct="vg:2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rd</w>, <w n="5.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="5.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="5.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.9">p<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ti<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="6.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="6.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="353" place="7" mp="M">e</seg>ss<seg phoneme="ɥi" type="vs" value="1" rule="462" place="8" mp="M">u</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>nt</w> <w n="6.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="6.8" punct="vg:12">ch<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="7.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.7">r<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="7.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="7.9" punct="pt:12">l<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="8.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="8.5">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="8.7" punct="pt:12">m<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>ti<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1" mp="M">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>t</w> <w n="9.2" punct="vg:3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="9.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457" place="6" caesura="1">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="9.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="9.9" punct="vg:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">em</seg>p<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">R<seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="10.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="10.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rs</w> <w n="10.7" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>nn<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="11.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="11.3">m</w>’<w n="11.4"><seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="C">eu</seg>r</w> <w n="11.6">v<seg phoneme="i" type="vs" value="1" rule="482" place="6" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="11.8">c<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="11.9"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="11.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="11.11" punct="pt:12">f<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">m</w>’<w n="12.4"><seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="12.5" punct="vg:5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5" punct="vg">en</seg></w>, <w n="12.6" punct="vg:6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="12.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="12.8">t<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="12.9">m</w>’<w n="12.10"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="12.11">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="12.12" punct="pt:12">d<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>nn<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>