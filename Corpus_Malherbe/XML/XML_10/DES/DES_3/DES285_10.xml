<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES285" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="3(abbacddc)">
				<head type="main">SOLITUDE</head>
				<lg n="1" type="huitain" rhyme="abbacddc">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="1.3" punct="pe:4">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="pe">e</seg>s</w> ! <w n="1.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="1.5" punct="pe:7">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg>s</rhyme></w> !</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="2.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="2.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="2.6" punct="pe:7">ci<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pe">eu</seg>x</rhyme></w> !</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="3.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="3.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s</w> <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="3.7" punct="vg:7">y<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="vg">eu</seg>x</rhyme></w>,</l>
					<l n="4" num="1.4" lm="7" met="7"><w n="4.1">J</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="4.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.5">p<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>pi<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
					<l n="5" num="1.5" lm="7" met="7"><w n="5.1">M<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="5.3" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>mm<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg>s</rhyme></w>,</l>
					<l n="6" num="1.6" lm="7" met="7"><w n="6.1">T<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="6.4" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="7" num="1.7" lm="7" met="7"><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="7.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="7.5" punct="vg:7">v<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="8" num="1.8" lm="7" met="7"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.4" punct="pe:7"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>m<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pe">é</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="2" type="huitain" rhyme="abbacddc">
					<l n="9" num="2.1" lm="7" met="7"><w n="9.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="9.2" punct="vg:2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>t</w>, <w n="9.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="9.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="9.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="9.7"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
					<l n="10" num="2.2" lm="7" met="7"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="10.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="10.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="10.5" punct="vg:7">fl<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="vg">eu</seg>rs</rhyme></w>,</l>
					<l n="11" num="2.3" lm="7" met="7"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="11.3">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="11.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>l<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</rhyme></w></l>
					<l n="12" num="2.4" lm="7" met="7"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="12.3">fl<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="12.4" punct="dp:7">ph<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="dp">e</seg>s</rhyme></w> :</l>
					<l n="13" num="2.5" lm="7" met="7"><w n="13.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="13.2">l<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="13.4" punct="vg:7">tr<seg phoneme="i" type="vs" value="1" rule="dc-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>ph<rhyme label="c" id="7" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" punct="vg">an</seg>s</rhyme></w>,</l>
					<l n="14" num="2.6" lm="7" met="7"><w n="14.1">R<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="14.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w>-<w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.5" punct="vg:7">t<rhyme label="d" id="8" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="15" num="2.7" lm="7" met="7"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ts</w> <w n="15.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>s</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.6" punct="vg:7">m<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>st<rhyme label="d" id="8" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="16" num="2.8" lm="7" met="7"><w n="16.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rç<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="16.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ts</w> <w n="16.4" punct="pi:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>f<rhyme label="c" id="7" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" punct="pi">an</seg>s</rhyme></w> ?</l>
				</lg>
				<lg n="3" type="huitain" rhyme="abbacddc">
					<l n="17" num="3.1" lm="7" met="7"><w n="17.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">im</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="17.3" punct="vg:4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg">em</seg>ps</w>, <w n="17.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>x</w> <w n="17.5" punct="pe:7">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ch<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></w> !</l>
					<l n="18" num="3.2" lm="7" met="7"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="18.2">l</w>’<w n="18.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.4"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="18.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="18.7" punct="vg:7">d<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" punct="vg">o</seg>rt</rhyme></w>,</l>
					<l n="19" num="3.3" lm="7" met="7"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="19.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="19.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="19.4">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="19.5">d</w>’<w n="19.6" punct="vg:7"><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7" punct="vg">o</seg>r</rhyme></w>,</l>
					<l n="20" num="3.4" lm="7" met="7"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="20.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="20.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="5">a</seg>il</w> <w n="20.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="20.6" punct="dp:7">ch<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="dp">e</seg></rhyme></w> :</l>
					<l n="21" num="3.5" lm="7" met="7"><w n="21.1" punct="vg:2">S<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="21.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>x</w> <w n="21.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="21.4" punct="vg:5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" punct="vg">em</seg>ps</w>, <w n="21.5" punct="vg:7">s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>nn<rhyme label="c" id="11" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="vg">ez</seg></rhyme></w>,</l>
					<l n="22" num="3.6" lm="7" met="7"><w n="22.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="22.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="22.4" punct="vg:7">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<rhyme label="d" id="12" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="23" num="3.7" lm="7" met="7"><w n="23.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="23.2">m</w>’<w n="23.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="23.5">l</w>’<w n="23.6" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<rhyme label="d" id="12" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="24" num="3.8" lm="7" met="7"><w n="24.1">C</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="24.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="24.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="24.6" punct="pe:7">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<rhyme label="c" id="11" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="pe">ez</seg></rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>