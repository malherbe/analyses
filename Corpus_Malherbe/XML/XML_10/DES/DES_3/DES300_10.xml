<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES300" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="3(aabccb)">
				<head type="main">À MADAME RÉCAMIER</head>
				<lg n="1" type="sizain" rhyme="aabccb">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="1.4" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>s</w>, <w n="1.5" punct="pe:7">M<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></w> !</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="2.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6" punct="dp:7"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="7">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="dp">e</seg></rhyme></w> :</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="3.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="3.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4" punct="vg:7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="vg">in</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="7" met="7"><w n="4.1">J</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="4.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="4.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="5" num="1.5" lm="7" met="7"><w n="5.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="5.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.6" punct="pv:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>cr<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="6" num="1.6" lm="7" met="7"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="6.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="6.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="6.6" punct="pt:7">m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7" punct="pt">ain</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="sizain" rhyme="aabccb">
					<l n="7" num="2.1" lm="7" met="7"><w n="7.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>t</w>-<w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="7.4">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rsqu</w>’<w n="7.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="8" num="2.2" lm="7" met="7"><w n="8.1">P<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="8.3">m<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="5">en</seg>t</w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.5">v<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="9" num="2.3" lm="7" met="7"><w n="9.1">D</w>’<w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="9.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="9.4" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="vg">e</seg>l</w>, <w n="9.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="9.6" punct="pt:7">n<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="pt">oi</seg>r</rhyme></w>.</l>
					<l n="10" num="2.4" lm="7" met="7"><w n="10.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>t</w>-<w n="10.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="10.4">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.5">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="10.6">m<rhyme label="c" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="11" num="2.5" lm="7" met="7"><w n="11.1">D<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2">s</w>’<w n="11.3"><seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="11.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="11.6">l</w>’<w n="11.7" punct="dp:7"><rhyme label="c" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="dp">e</seg></rhyme></w> :</l>
					<l n="12" num="2.6" lm="7" met="7"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="12.3" punct="vg:3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>r</w>, <w n="12.4">c</w>’<w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="12.6">l</w>’<w n="12.7" punct="pe:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="pe">oi</seg>r</rhyme></w> !</l>
				</lg>
				<lg n="3" type="sizain" rhyme="aabccb">
					<l n="13" num="3.1" lm="7" met="7"><w n="13.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="13.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="13.4" punct="vg:7">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="14" num="3.2" lm="7" met="7"><w n="14.1">C</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="14.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5">pr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
					<l n="15" num="3.3" lm="7" met="7"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="15.3" punct="pv:7"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pv">é</seg></rhyme></w> ;</l>
					<l n="16" num="3.4" lm="7" met="7"><w n="16.1">C</w>’<w n="16.2" punct="vg:1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1" punct="vg">e</seg>st</w>, <w n="16.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="16.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<rhyme label="c" id="9" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="17" num="3.5" lm="7" met="7"><w n="17.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="17.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.5">l</w>’<w n="17.6" punct="vg:7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<rhyme label="c" id="9" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
					<l n="18" num="3.6" lm="7" met="7"><w n="18.1" punct="pe:2">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="pe">é</seg></w> ! <w n="18.2" punct="pe:4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="pe">é</seg></w> ! <w n="18.3" punct="pe:7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pe">é</seg></rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>