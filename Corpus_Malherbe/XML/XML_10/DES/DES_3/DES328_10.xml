<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES328" modus="sp" lm_max="8" metProfile="8, 4" form="suite périodique" schema="4(ababcdcdcd)">
				<head type="main">LE BAPTÊME D’UN PRINCE</head>
				<head type="sub_1">À NOTRE-DAME</head>
				<lg n="1" type="dizain" rhyme="ababcdcdcd">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="1.2" punct="vg:2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg></w>, <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">c</w>’<w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="1.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="1.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>pt<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="4" met="4"><space quantity="12" unit="char"></space><w n="2.1">D</w>’<w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="2.3">r<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="2.4" punct="pe:4">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="pe">u</seg>r</rhyme></w> !</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Qu</w>’<w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="3.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="3.4">l<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.6">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="4" met="4"><space quantity="12" unit="char"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="4.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="4.4" punct="pe:4">p<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="pe">u</seg>r</rhyme></w> !</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="5.3" punct="vg:4">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">ez</seg></w>, <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="5.6">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7" punct="vg:8"><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="6" num="1.6" lm="4" met="4"><space quantity="12" unit="char"></space><w n="6.1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="6.3" punct="pv:4">m<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="pv">oi</seg></rhyme></w> ;</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="7.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="7.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.4">d</w>’<w n="7.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.6">f<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="8" num="1.8" lm="4" met="4"><space quantity="12" unit="char"></space><w n="8.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.3" punct="pt:4">f<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="pt">oi</seg></rhyme></w>.</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1">L</w>’<hi rend="ital"><w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">An</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w></hi> <w n="9.3">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="9.5">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w>-<w n="9.6" punct="pv:8">D<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="10" num="1.10" lm="8" met="8"><w n="10.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="10.3" punct="vg:4">r<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="10.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="10.6" punct="pe:8">r<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pe">oi</seg></rhyme></w> !</l>
				</lg>
				<lg n="2" type="dizain" rhyme="ababcdcdcd">
					<l n="11" num="2.1" lm="8" met="8"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="11.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="11.5" punct="vg:8">bl<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="12" num="2.2" lm="4" met="4"><space quantity="12" unit="char"></space><w n="12.1">R<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3" punct="pv:4">l<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="pv">ai</seg>t</rhyme></w> ;</l>
					<l n="13" num="2.3" lm="8" met="8"><w n="13.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.3">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="13.5" punct="vg:8">fr<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="14" num="2.4" lm="4" met="4"><space quantity="12" unit="char"></space><w n="14.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="14.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="14.4" punct="pt:4">pl<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="pt">aî</seg>t</rhyme></w>.</l>
					<l n="15" num="2.5" lm="8" met="8"><w n="15.1" punct="vg:2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="15.2" punct="vg:4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">ez</seg></w>, <w n="15.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="15.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="15.5">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6" punct="vg:8"><rhyme label="c" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="16" num="2.6" lm="4" met="4"><space quantity="12" unit="char"></space><w n="16.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="16.3" punct="pv:4">m<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="pv">oi</seg></rhyme></w> ;</l>
					<l n="17" num="2.7" lm="8" met="8"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="17.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="17.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.4">d</w>’<w n="17.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="17.6" punct="vg:8">f<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="18" num="2.8" lm="4" met="4"><space quantity="12" unit="char"></space><w n="18.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="18.3" punct="pt:4">f<rhyme label="d" id="8" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="pt">oi</seg></rhyme></w>.</l>
					<l n="19" num="2.9" lm="8" met="8"><w n="19.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="19.2">r<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="19.3">s</w>’<w n="19.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="19.6">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w>-<w n="19.7" punct="dp:8">D<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
					<l n="20" num="2.10" lm="8" met="8"><w n="20.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="20.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="20.3" punct="vg:4">r<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="20.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="20.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="20.6" punct="pe:8">r<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pe">oi</seg></rhyme></w> !</l>
				</lg>
				<lg n="3" type="dizain" rhyme="ababcdcdcd">
					<l n="21" num="3.1" lm="8" met="8"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="21.3">l</w>’<w n="21.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ch<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="22" num="3.2" lm="4" met="4"><space quantity="12" unit="char"></space><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="22.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="22.3" punct="pv:4">j<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pv">ou</seg>r</rhyme></w> ;</l>
					<l n="23" num="3.3" lm="8" met="8"><w n="23.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="23.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="23.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ch<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="24" num="3.4" lm="4" met="4"><space quantity="12" unit="char"></space><w n="24.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="24.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3" punct="pt:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pt">ou</seg>r</rhyme></w>.</l>
					<l n="25" num="3.5" lm="8" met="8"><w n="25.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="25.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="25.3" punct="vg:4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>c</w>, <w n="25.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="25.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="25.6">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.7" punct="vg:8"><rhyme label="c" id="11" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="26" num="3.6" lm="4" met="4"><space quantity="12" unit="char"></space><w n="26.1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="26.3" punct="vg:4">m<rhyme label="d" id="12" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></rhyme></w>,</l>
					<l n="27" num="3.7" lm="8" met="8"><w n="27.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="27.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="27.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="27.4">d</w>’<w n="27.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="27.6" punct="vg:8">f<rhyme label="c" id="11" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="28" num="3.8" lm="4" met="4"><space quantity="12" unit="char"></space><w n="28.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="28.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="28.3" punct="pt:4">f<rhyme label="d" id="12" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="pt">oi</seg></rhyme></w>.</l>
					<l n="29" num="3.9" lm="8" met="8"><w n="29.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="29.2">r<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="29.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="29.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="29.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="29.6">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w>-<w n="29.7" punct="dp:8">D<rhyme label="c" id="11" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
					<l n="30" num="3.10" lm="8" met="8"><w n="30.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="30.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="30.3" punct="vg:4">r<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="30.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="30.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="30.6" punct="pe:8">r<rhyme label="d" id="12" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pe">oi</seg></rhyme></w> !</l>
				</lg>
				<lg n="4" type="dizain" rhyme="ababcdcdcd">
					<l n="31" num="4.1" lm="8" met="8"><w n="31.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="31.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="31.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="31.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="5">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="31.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="32" num="4.2" lm="4" met="4"><space quantity="12" unit="char"></space><w n="32.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="32.2" punct="vg:4">Cr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>r</rhyme></w>,</l>
					<l n="33" num="4.3" lm="8" met="8"><w n="33.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="33.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="33.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="33.4">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="34" num="4.4" lm="4" met="4"><space quantity="12" unit="char"></space><w n="34.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="34.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="34.3" punct="pt:4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">Au</seg>t<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pt">eu</seg>r</rhyme></w>.</l>
					<l n="35" num="4.5" lm="8" met="8"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="35.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="35.3" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="35.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="35.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="35.6">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.7" punct="vg:8"><rhyme label="c" id="15" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="36" num="4.6" lm="4" met="4"><space quantity="12" unit="char"></space><w n="36.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="36.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="36.3" punct="pv:4">m<rhyme label="d" id="16" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="pv">oi</seg></rhyme></w> ;</l>
					<l n="37" num="4.7" lm="8" met="8"><w n="37.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="37.2" punct="pv:2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="pv">ou</seg>x</w> ; <w n="37.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="37.4">d</w>’<w n="37.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="37.6" punct="vg:8">f<rhyme label="c" id="15" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="38" num="4.8" lm="4" met="4"><space quantity="12" unit="char"></space><w n="38.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="38.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="38.3" punct="pt:4">f<rhyme label="d" id="16" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="pt">oi</seg></rhyme></w>.</l>
					<l n="39" num="4.9" lm="8" met="8"><w n="39.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="39.2">r<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="39.3">s</w>’<w n="39.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>scr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="39.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="39.6">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w>-<w n="39.7" punct="dp:8">D<rhyme label="c" id="15" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
					<l n="40" num="4.10" lm="8" met="8"><w n="40.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="40.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="40.3" punct="vg:4">r<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="40.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="40.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="40.6" punct="pe:8">r<rhyme label="d" id="16" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pe">oi</seg></rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>