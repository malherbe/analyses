<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES157" modus="cm" lm_max="12" metProfile="6+6" form="suite de strophes" schema="1(abbacddc) 1(ababcddc) 1(aa)">
						<head type="main">ÉLÉGIE</head>
						<lg n="1" type="huitain" rhyme="abbacddc">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="pe:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pe ti">i</seg></w> ! — <w n="1.2">F<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="Lp">u</seg>t</w>-<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="1.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>c</w><caesura></caesura> <w n="1.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="1.6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w> <w n="1.7">s<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.8" punct="vg:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rm<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="2.2">p<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="2.4"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="2.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="2.6">t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>t</w> <w n="2.7">l</w>’<w n="2.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ti<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.4" punct="vg:6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg" caesura="1">em</seg>ps</w>,<caesura></caesura> <w n="3.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7" mp="Lc">eu</seg>t</w>-<w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="P">a</seg>r</w> <w n="3.8" punct="vg:12">p<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ti<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></w>,</l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">j</w>’<w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="4.4" punct="vg:4">n<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-42">e</seg></w>, <w n="4.5" punct="vg:6">h<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="4.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="4.7">m<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="4.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="4.9" punct="pi:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
							<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">N</w>’<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="Lp">a</seg></w>-<w n="5.3">t</w>-<w n="5.4"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="5.5" punct="vg:4">j<seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s</w>, <w n="5.6">j<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="5.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="5.9">d<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>c<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</rhyme></w></l>
							<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="6.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="6.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="6.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="6.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="6.7">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="6.8" punct="pv:12">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="11" mp="M">ain</seg>t<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
							<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="7.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="3" mp="M">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="7.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="7.5" punct="vg:6">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="7.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.7" punct="pv:12">n<seg phoneme="a" type="vs" value="1" rule="343" place="11" mp="M">a</seg><rhyme label="d" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="477" place="12">ï</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
							<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="8.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="8.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="8.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="8.7">f<seg phoneme="œ" type="vs" value="1" rule="304" place="9" mp="M">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="8.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="8.9" punct="pi:12">s<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pi">œu</seg>r</rhyme></w> ?</l>
						</lg>
						<lg n="2" type="huitain" rhyme="ababcddc">
							<l n="9" num="2.1" lm="12" met="6+6"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">i</seg>l</w> <w n="9.3">m</w>’<w n="9.4"><seg phoneme="o" type="vs" value="1" rule="415" place="3" mp="M">ô</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.6" punct="vg:6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="6" punct="vg" caesura="1">om</seg></w>,<caesura></caesura> <w n="9.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="9.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.9">t<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>m<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="10" num="2.2" lm="12" met="6+6"><w n="10.1">L<seg phoneme="i" type="vs" value="1" rule="d-1" place="1" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="10.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>x</w> <w n="10.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="482" place="6" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="10.7">v<seg phoneme="ø" type="vs" value="1" rule="248" place="9">œu</seg>x</w> <w n="10.8" punct="pv:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>d<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pv">u</seg>s</rhyme></w> ;</l>
							<l n="11" num="2.3" lm="12" met="6+6"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">n</w>’<w n="11.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="11.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="11.5" punct="ps:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="ps" caesura="1">on</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>…<caesura></caesura> <w n="11.6" punct="pe:7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pe">A</seg>h</w> ! <w n="11.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="P">ou</seg>r</w> <w n="11.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="11.9">c<seg phoneme="œ" type="vs" value="1" rule="249" place="10">œu</seg>r</w> <w n="11.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="12" num="2.4" lm="12" met="6+6"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">d</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="12.4" punct="pe:6"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pe" caesura="1">é</seg>s</w> !<caesura></caesura> <w n="12.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="12.7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10">e</seg>ts</w> <w n="12.8" punct="pe:12">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rd<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pe">u</seg>s</rhyme></w> !</l>
							<l n="13" num="2.5" lm="12" met="6+6"><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="13.2">j</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="13.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="13.5" punct="pe:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pe" caesura="1">er</seg></w> !<caesura></caesura> <w n="13.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="13.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="13.8">h<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10">ain</seg></w> <w n="13.9">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>g<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="14" num="2.6" lm="12" met="6+6"><w n="14.1"><seg phoneme="y" type="vs" value="1" rule="251" place="1">Eû</seg>t</w> <w n="14.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="14.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="14.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="14.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="14.9">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="14.10" punct="vg:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>sc<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>rs</rhyme></w>,</l>
							<l n="15" num="2.7" lm="12" met="6+6"><w n="15.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="15.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="15.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" mp="M">o</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="15.5">m</w>’<w n="15.6"><seg phoneme="y" type="vs" value="1" rule="251" place="7">eû</seg>t</w> <w n="15.7">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8" mp="M">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="15.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="15.9" punct="vg:12">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>c<rhyme label="d" id="8" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>rs</rhyme></w>,</l>
							<l n="16" num="2.8" lm="12" met="6+6"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="16.2">m</w>’<w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2" mp="M">ai</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="16.4" punct="pe:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="pe" caesura="1">o</seg>r</w> !<caesura></caesura> <w n="16.5">J</w>’<w n="16.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7" mp="M">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w> <w n="16.7" punct="ps:9">tr<seg phoneme="o" type="vs" value="1" rule="433" place="9" punct="ps">o</seg>p</w>… <w n="16.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>l</w> <w n="16.9" punct="pe:12">d<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>mm<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
						</lg>
						<lg n="3" type="distique" rhyme="aa">
							<l n="17" num="3.1" lm="12" met="6+6"><w n="17.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="17.2" punct="vg:2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg></w>, <w n="17.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="17.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="17.5" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg" caesura="1">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="17.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="17.8">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>s</w> <w n="17.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="17.10" punct="vg:12">m<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></rhyme></w>,</l>
							<l n="18" num="3.2" lm="12" met="6+6"><w n="18.1" punct="pe:1">Qu<seg phoneme="wa" type="vs" value="1" rule="281" place="1" punct="pe">oi</seg></w> ! <w n="18.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="18.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="18.4">l</w>’<w n="18.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="18.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="18.7">j</w>’<w n="18.8"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="18.9">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>t</w> <w n="18.10" punct="pe:12">t<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pe">oi</seg></rhyme></w> !</l>
						</lg>
					</div></body></text></TEI>