<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>ROMANCES</head><div type="poem" key="DES192" modus="sm" lm_max="4" metProfile="4" form="suite périodique" schema="11(abab)">
						<head type="main">LA FIANCÉE DU MARIN</head>
						<lg n="1" type="quatrain" rhyme="abab">
							<l n="1" num="1.1" lm="4" met="4"><w n="1.1">Tr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
							<l n="2" num="1.2" lm="4" met="4"><w n="2.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="2.3" punct="pv:4">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pv">er</seg></rhyme></w> ;</l>
							<l n="3" num="1.3" lm="4" met="4"><w n="3.1" punct="vg:2">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="3.3" punct="vg:4">m<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
							<l n="4" num="1.4" lm="4" met="4"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="4.3" punct="pt:4">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pt">er</seg></rhyme></w>.</l>
						</lg>
						<lg n="2" type="quatrain" rhyme="abab">
							<l n="5" num="2.1" lm="4" met="4"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w>-<w n="5.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="5.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w>-<w n="5.4"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
							<l n="6" num="2.2" lm="4" met="4"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="6.2">m</w>’<w n="6.3" punct="pt:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pt">a</seg></rhyme></w>.</l>
							<l n="7" num="2.3" lm="4" met="4"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2">m</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="7.5">n<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
							<l n="8" num="2.4" lm="4" met="4"><w n="8.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2" punct="pt:4">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>dr<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pt">a</seg></rhyme></w>.</l>
						</lg>
						<lg n="3" type="quatrain" rhyme="abab">
							<l n="9" num="3.1" lm="4" met="4"><w n="9.1">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="9.3">m</w>’<w n="9.4"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
							<l n="10" num="3.2" lm="4" met="4"><w n="10.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="10.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3" punct="pv:4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>tt<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pv">er</seg></rhyme></w> ;</l>
							<l n="11" num="3.3" lm="4" met="4"><w n="11.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="11.3">m<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
							<l n="12" num="3.4" lm="4" met="4"><w n="12.1">D<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t</w> <w n="12.2">l</w>’<w n="12.3" punct="pt:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pt">er</seg></rhyme></w>.</l>
						</lg>
						<lg n="4" type="quatrain" rhyme="abab">
							<l n="13" num="4.1" lm="4" met="4"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="13.4" punct="vg:4">s<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
							<l n="14" num="4.2" lm="4" met="4"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="14.4">fl<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>ts</rhyme></w></l>
							<l n="15" num="4.3" lm="4" met="4"><w n="15.1">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="15.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="15.3">l</w>’<w n="15.4" punct="vg:4"><rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
							<l n="16" num="4.4" lm="4" met="4"><w n="16.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="16.2" punct="pi:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="438" place="4" punct="pi">o</seg>ts</rhyme></w> ?</l>
						</lg>
						<lg n="5" type="quatrain" rhyme="abab">
							<l n="17" num="5.1" lm="4" met="4"><w n="17.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="17.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="17.3">n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
							<l n="18" num="5.2" lm="4" met="4"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="18.2">s</w>’<w n="18.3" punct="pv:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ge<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="316" place="4" punct="pv">a</seg></rhyme></w> ;</l>
							<l n="19" num="5.3" lm="4" met="4"><w n="19.1">C</w>’<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r</w> <w n="19.4"><rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
							<l n="20" num="5.4" lm="4" met="4"><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="20.2" punct="pe:4">n<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>fr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ge<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="316" place="4" punct="pe">a</seg></rhyme></w> !</l>
						</lg>
						<lg n="6" type="quatrain" rhyme="abab">
							<l n="21" num="6.1" lm="4" met="4"><w n="21.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="21.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d</w> <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="21.4" punct="pv:4">v<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pv">e</seg></rhyme></w> ;</l>
							<l n="22" num="6.2" lm="4" met="4"><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="22.2" punct="pe:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sp<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pe">oi</seg>r</rhyme></w> !</l>
							<l n="23" num="6.3" lm="4" met="4"><w n="23.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="23.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
							<l n="24" num="6.4" lm="4" met="4"><w n="24.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="24.2">l</w>’<w n="24.3" punct="pt:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pt">oi</seg>r</rhyme></w>.</l>
						</lg>
						<lg n="7" type="quatrain" rhyme="abab">
							<l n="25" num="7.1" lm="4" met="4"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="25.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="25.3" punct="vg:4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>p<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
							<l n="26" num="7.2" lm="4" met="4"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="26.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="26.3" punct="vg:4">j<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>r</rhyme></w>,</l>
							<l n="27" num="7.3" lm="4" met="4"><w n="27.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="27.2">v<seg phoneme="ø" type="vs" value="1" rule="248" place="2">œu</seg></w> <w n="27.3">m</w>’<w n="27.4" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pp<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
							<l n="28" num="7.4" lm="4" met="4"><w n="28.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="28.2">v<seg phoneme="ø" type="vs" value="1" rule="248" place="2">œu</seg></w> <w n="28.3">d</w>’<w n="28.4" punct="pt:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pt">ou</seg>r</rhyme></w>.</l>
						</lg>
						<lg n="8" type="quatrain" rhyme="abab">
							<l n="29" num="8.1" lm="4" met="4"><w n="29.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="29.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="29.3">m</w>’<w n="29.4" punct="pv:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>tt<rhyme label="a" id="15" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pv">e</seg></rhyme></w> ;</l>
							<l n="30" num="8.2" lm="4" met="4"><w n="30.1">J</w>’<w n="30.2"><seg phoneme="i" type="vs" value="1" rule="497" place="1">y</seg></w> <w n="30.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<rhyme label="b" id="16" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</rhyme></w></l>
							<l n="31" num="8.3" lm="4" met="4"><w n="31.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="31.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="31.3" punct="vg:4">t<rhyme label="a" id="15" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
							<l n="32" num="8.4" lm="4" met="4"><w n="32.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="32.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>nn<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="32.3">d</w>’<w n="32.4" punct="pt:4"><rhyme label="b" id="16" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="pt">o</seg>r</rhyme></w>.</l>
						</lg>
						<lg n="9" type="quatrain" rhyme="abab">
							<l n="33" num="9.1" lm="4" met="4"><w n="33.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="33.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="33.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="33.4" punct="vg:4">p<rhyme label="a" id="17" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
							<l n="34" num="9.2" lm="4" met="4"><w n="34.1">C</w>’<w n="34.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="34.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="34.4" punct="dp:4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rni<rhyme label="b" id="18" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="dp">er</seg></rhyme></w> :</l>
							<l n="35" num="9.3" lm="4" met="4"><w n="35.1">Qu</w>’<w n="35.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="35.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="35.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>sp<rhyme label="a" id="17" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
							<l n="36" num="9.4" lm="4" met="4"><w n="36.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="36.2" punct="pe:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>ni<rhyme label="b" id="18" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pe">er</seg></rhyme></w> !</l>
						</lg>
						<lg n="10" type="quatrain" rhyme="abab">
							<l n="37" num="10.1" lm="4" met="4"><w n="37.1">C</w>’<w n="37.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="37.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="37.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="3">ym</seg>b<rhyme label="a" id="19" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
							<l n="38" num="10.2" lm="4" met="4"><w n="38.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="38.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <choice reason="analysis" type="false_verse" hand="CA"><sic>bien</sic><corr source="édition_1973"><w n="38.3">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><rhyme label="b" id="20" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4" punct="pv">en</seg></rhyme></w></corr></choice> ;</l>
							<l n="39" num="10.3" lm="4" met="4"><w n="39.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="39.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="39.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>d<rhyme label="a" id="19" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
							<l n="40" num="10.4" lm="4" met="4"><w n="40.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="40.2">n</w>’<w n="40.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="40.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="40.5" punct="pt:4">ri<rhyme label="b" id="20" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4" punct="pt">en</seg></rhyme></w>.</l>
						</lg>
						<lg n="11" type="quatrain" rhyme="abab">
							<l n="41" num="11.1" lm="4" met="4"><w n="41.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="41.2">j</w>’<w n="41.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ds</w> <w n="41.4">J<rhyme label="a" id="21" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
							<l n="42" num="11.2" lm="4" met="4"><w n="42.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="42.2" punct="dp:2">cr<seg phoneme="i" type="vs" value="1" rule="469" place="2" punct="dp in">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> : « <w n="42.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>di<rhyme label="b" id="22" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="pe">eu</seg></rhyme></w> ! »</l>
							<l n="43" num="11.3" lm="4" met="4"><w n="43.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="43.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="43.3">p<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="43.4"><rhyme label="a" id="21" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
							<l n="44" num="11.4" lm="4" met="4"><w n="44.1">S</w>’<w n="44.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="44.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="44.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="44.5" punct="pe:4">Di<rhyme label="b" id="22" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="pe">eu</seg></rhyme></w> !</l>
						</lg>
					</div></body></text></TEI>