<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA FLEUR DES EAUX</head><div type="poem" key="BCH48" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="4(ababb)">
					<head type="number">XLVII</head>
					<lg n="1" type="quintil" rhyme="ababb">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="1.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="1.5">s<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>v<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="2.3">l</w>’<w n="2.4">h<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6">l</w>’<w n="2.7" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>di<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg></rhyme></w> !</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="3.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6" punct="vg:8">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1" punct="vg:2">M<seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">o</seg>qu<seg phoneme="ø" type="vs" value="1" rule="403" place="2" punct="vg">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="4.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="4.5">p<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></rhyme></w></l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="5.4">l</w>’<w n="5.5">h<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.7">l</w>’<w n="5.8" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>di<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" type="quintil" rhyme="ababb">
						<l n="6" num="2.1" lm="8" met="8"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="6.3" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>nt</w>, <w n="6.4">l</w>’<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.6" punct="vg:8"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="7" num="2.2" lm="8" met="8"><w n="7.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="3">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.5" punct="pv:8">j<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pv">eu</seg>x</rhyme></w> ;</l>
						<l n="8" num="2.3" lm="8" met="8"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="8.3">d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>r</w> <w n="8.6" punct="tc:8">v<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg ti">e</seg></rhyme></w>, —</l>
						<l n="9" num="2.4" lm="8" met="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</rhyme></w></l>
						<l n="10" num="2.5" lm="8" met="8"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="10.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="10.3">br<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="10.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.5" punct="pt:8">ci<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quintil" rhyme="ababb">
						<l n="11" num="3.1" lm="8" met="8"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="3">en</seg>s</w> <w n="11.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="11.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l</w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.7" punct="vg:8">v<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="12" num="3.2" lm="8" met="8"><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>squ<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>f</w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></w></l>
						<l n="13" num="3.3" lm="8" met="8"><w n="13.1">Fl<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="13.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="13.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.5" punct="vg:8">l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="14" num="3.4" lm="8" met="8"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2" punct="vg:5">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rs<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg">ai</seg>t</w>, <w n="14.3" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">aî</seg>n<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
						<l n="15" num="3.5" lm="8" met="8"><w n="15.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="15.2">d</w>’<w n="15.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="15.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="15.5">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="5">y</seg>s</w> <w n="15.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" type="quintil" rhyme="ababb">
						<l n="16" num="4.1" lm="8" met="8"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="16.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5">m<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6">en</seg>t</w> <w n="16.6">s</w>’<w n="16.7" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ppr<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="17" num="4.2" lm="8" met="8"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">j</w>’<w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="17.6" punct="vg:8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>st<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></rhyme></w>,</l>
						<l n="18" num="4.3" lm="8" met="8"><w n="18.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="18.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="18.4" punct="vg:8">r<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="19" num="4.4" lm="8" met="8"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="19.2" punct="vg:2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2" punct="vg">e</seg>l</w>, <w n="19.3">gr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="19.5" punct="vg:5">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg">eu</seg></w>, <w n="19.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="19.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></w></l>
						<l n="20" num="4.5" lm="8" met="8"><w n="20.1">F<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="3">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="20.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="20.4">t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="20.5" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>