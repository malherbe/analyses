<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA FLEUR DES EAUX</head><div type="poem" key="BCH19" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="4(abab)">
					<head type="number">XVIII</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">m<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="1.3">d</w>’<w n="1.4" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>r</w>, <w n="1.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="1.6">su<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="1.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="1.10">chi<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12">en</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="2.2">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fm">e</seg></w>-<w n="2.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="2.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="2.5" punct="vg:6">b<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7">m<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="2.9">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="2.10">l<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="3.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="3.3" punct="pe:6">v<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>rg<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe ti" caesura="1">a</seg>l</w> !<caesura></caesura> — <w n="3.4" punct="vg:7">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="vg">on</seg></w>, <w n="3.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg></w> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg></w> <w n="3.7" punct="vg:11">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" punct="vg" mp="F">e</seg></w>, <w n="3.8">ri<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12">en</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="4.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3">n</w>’<w n="4.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5" mp="M">ein</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="4.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.7">f<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg></w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="4.9">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="4.10" punct="pt:12">fi<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="Lc">u</seg>squ</w>’<w n="5.2" punct="vg:3"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M/mc">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="5.3">l</w>’<w n="5.4"><seg phoneme="œ" type="vs" value="1" rule="286" place="4">œ</seg>il</w> <w n="5.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="5.7" punct="vg:8">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="vg">e</seg>l</w>, <w n="5.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="5.9">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg>ph<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</rhyme></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="6.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="6.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="6.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="6.7">p<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>qu<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="6.8"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="6.9">r<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="7.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="7.4" punct="vg:6">d<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>gts</w>,<caesura></caesura> <w n="7.5">n<seg phoneme="a" type="vs" value="1" rule="343" place="7" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="477" place="8">ï</seg>f</w> <w n="7.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="7.8" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>f<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</rhyme></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">t</w>’<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2" mp="M">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="8.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="8.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="8.6">n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="8.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="8.8"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="8.9" punct="pt:12">ch<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="9.3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="9.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.5">v<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="9.6">t</w>’<w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="9.9">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>x</rhyme></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="10.4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="10.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="10.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="10.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="10.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="10.9" punct="vg:12">p<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>ssi<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="6" caesura="1">um</seg></w><caesura></caesura> <w n="11.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="11.5">cr<seg phoneme="y" type="vs" value="1" rule="454" place="8" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="11.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="11.8">d<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>x</rhyme></w></l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="12.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="12.3">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" mp="M">e</seg>ill<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="12.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rps</w><caesura></caesura> <w n="12.5">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="7" mp="M">a</seg>y<seg phoneme="o" type="vs" value="1" rule="435" place="8" mp="M">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="12.7" punct="pt:12">l<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>mi<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="13.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="13.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="13.5">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6" caesura="1">e</seg>ds</w><caesura></caesura> <w n="13.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="13.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="13.8">pi<seg phoneme="e" type="vs" value="1" rule="241" place="10">e</seg>ds</w> <w n="13.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="13.10">di<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</rhyme></w></l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="14.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="14.5">d</w>’<w n="14.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="14.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="14.8">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg></w> <w n="14.9">n<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="14.10" punct="vg:12">tr<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="15.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="15.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="15.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="15.7">pl<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="15.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="15.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="15.10">ci<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</rhyme></w></l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="16.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="16.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="16.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="16.5">br<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="16.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="16.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r</w> <w n="16.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="16.9">f<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>t</w> <w n="16.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="16.11" punct="pt:12">r<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>