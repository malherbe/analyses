<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉNIGMES</head><head type="sub_1">BOTANIQUE<lb></lb>(<hi rend="smallcap">EMBLÈMES</hi>.)</head><div type="poem" key="BRT156" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="2[abab] 1[aa] 1[abba]">
				<head type="number">VI</head>
				<lg n="1" type="regexp" rhyme="ababaaabbaabab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="1.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="1.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="u" type="vs" value="1" rule="428" place="4">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="2.5" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg>x</rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="3.4">f<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="3.5">v<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">fru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="4.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="4.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.7" punct="pt:8">r<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg>x</rhyme></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="5.2">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>d</w> <w n="5.3" punct="vg:4">N<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="175" place="4" punct="vg">ë</seg>l</w>, <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="5.5" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">An</seg>gl<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>j<seg phoneme="u" type="vs" value="1" rule="d-2" place="3">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="6.3">l</w>’<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ct</w> <w n="6.5" punct="pv:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>st<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="7.4">m</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345" place="5">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="7.7" punct="pt:8">pr<rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8" punct="pt">ê</seg>t</rhyme></w>.</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="8.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="8.3">j</w>’<w n="8.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.6" punct="pv:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>t<rhyme label="b" id="5" gender="f" type="a" stanza="3"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="9.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rn<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="9.4"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="9.5" punct="pv:8">cl<seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg>t<rhyme label="b" id="5" gender="f" type="e" stanza="3"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="10" num="1.10" lm="8" met="8"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="10.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.4" punct="pe:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pe">e</seg>t</rhyme></w> !</l>
					<l n="11" num="1.11" lm="8" met="8"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="11.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="11.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="11.5" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="354" place="6">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<rhyme label="a" id="6" gender="f" type="a" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="12" num="1.12" lm="8" met="8"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="12.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="3">ym</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="12.5">m</w>’<w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="12.7" punct="dp:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<rhyme label="b" id="7" gender="m" type="a" stanza="4"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="dp">é</seg></rhyme></w> :</l>
					<l n="13" num="1.13" lm="8" met="8"><hi rend="ital"><w n="13.1" punct="pt:8">M<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>thr<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>p<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="13.2"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="13.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<rhyme label="a" id="6" gender="f" type="e" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w></hi>.</l>
					<l n="14" num="1.14" lm="8" met="8"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2" punct="ps:3">p<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="ps">e</seg></w>… <w n="14.3">l</w>’<w n="14.4">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="14.5">s</w>’<w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="14.7" punct="pt:8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>g<rhyme label="b" id="7" gender="m" type="e" stanza="4"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Houx.</note>
					</closer>
			</div></body></text></TEI>