<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT104" modus="cm" lm_max="12" metProfile="6+6" form="suite de strophes" schema="2[abab]">
				<head type="number">IV</head>
				<lg n="1" type="regexp" rhyme="ab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="1.2" punct="vg:2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>c</w>, <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="1.4" punct="vg:6">l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>rs</w>,<caesura></caesura> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.7">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.8" punct="vg:12">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="11" mp="M">oin</seg>t<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="2.2">j<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="2.3">d</w>’<w n="2.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="2.5">pi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="2.8">pi<seg phoneme="e" type="vs" value="1" rule="241" place="9">e</seg>ds</w> <w n="2.9" punct="pi:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rt<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pi">é</seg>s</rhyme></w> ?</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2" type="regexp" rhyme="ab">
					<l n="3" num="2.1" lm="12" met="6+6"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="3.3" punct="vg:4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="vg">eau</seg></w>, <w n="3.4" punct="vg:6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="345" place="6" punct="vg" caesura="1">ue</seg>il</w>,<caesura></caesura> <w n="3.5" punct="vg:7">cr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7" punct="vg">è</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="3.8" punct="vg:12">st<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="4" num="2.2" lm="12" met="6+6"><w n="4.1">R<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="4.3" punct="vg:6">h<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="vg" caesura="1">e</seg>rs</w>,<caesura></caesura> <w n="4.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="4.6" punct="pi:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pi">é</seg>s</rhyme></w> ?</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3" type="regexp" rhyme="ab">
					<l n="5" num="3.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="5.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="5.4">v<seg phoneme="wa" type="vs" value="1" rule="440" place="4" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="5.7">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10">ain</seg></w> <w n="5.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="5.9" punct="pi:12">f<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					<l n="6" num="3.2" lm="12" met="6+6"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">em</seg>pr<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="M">un</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="6.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="6.4" punct="pi:6">cr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pi" caesura="1">i</seg>t</w> ?<caesura></caesura> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="6.6">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" mp="M">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="6.8" punct="pi:12">f<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pi">a</seg>rd</rhyme></w> ?</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4" type="regexp" rhyme="ab">
					<l n="7" num="4.1" lm="12" met="6+6"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="7.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="7.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>g</w> <w n="7.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="7.10" punct="pi:12">b<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					<l n="8" num="4.2" lm="12" met="6+6"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.3" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">in</seg></w>,<caesura></caesura> <w n="8.4">qu</w>’<w n="8.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="8.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.7">tr<seg phoneme="o" type="vs" value="1" rule="433" place="11">o</seg>p</w> <w n="8.8" punct="pi:12">t<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pi ps">a</seg>rd</rhyme></w> ? …</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ A-bois.</note>
					</closer>
			</div></body></text></TEI>