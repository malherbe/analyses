<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT208" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique avec alternance de type 1" schema="3{1(aa) 1(abab)}">
				<head type="number">XXXIII</head>
				<lg n="1" type="distique" rhyme="aa">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">D</w>’<w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg>s</w> <w n="1.3">l</w>’<w n="1.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="1.7"><seg phoneme="œ" type="vs" value="1" rule="286" place="6" caesura="1">œ</seg>il</w><caesura></caesura> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.9">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s</w> <w n="1.10">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="P">u</seg>r</w> <w n="1.11" punct="pe:12">d<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg>x</rhyme></w> !</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="2.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>s</w> <w n="2.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>r</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="2.6" punct="vg:6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="2.7">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="2.8">c</w>’<w n="2.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="2.10">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt</w> <w n="2.11" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="10" mp="M">en</seg>n<seg phoneme="ɥi" type="vs" value="1" rule="462" place="11" mp="M">u</seg>y<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="3" num="2.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="3.1">D</w>’<w n="3.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="3.3"><seg phoneme="i" type="vs" value="1" rule="497" place="3">y</seg></w> <w n="3.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="3.6" punct="vg:8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<rhyme label="a" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="4" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="497" place="1">Y</seg></w> <w n="4.2">br<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="4.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="4.4" punct="vg:6">ch<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="4.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="4.6" punct="vg:8">n<rhyme label="b" id="3" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="8" punct="vg">om</seg></rhyme></w>,</l>
					<l n="5" num="2.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.6">br<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="a" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="6" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="497" place="1">Y</seg></w> <w n="6.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w>-<w n="6.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="6.4" punct="pi:4">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="pi">eu</seg>x</w> ? <w n="6.5" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">A</seg>h</w> ! <w n="6.6" punct="vg:7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" punct="vg">e</seg>s</w>, <w n="6.7" punct="pt:8">n<rhyme label="b" id="3" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></rhyme></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3" type="distique" rhyme="aa">
					<l n="7" num="3.1" lm="12" met="6+6"><w n="7.1">S</w>’<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="7.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="7.6">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="7.7" punct="vg:6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg" caesura="1">o</seg>rt</w>,<caesura></caesura> <w n="7.8">cr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" mp="M/mp">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="8" mp="Lp">ez</seg></w>-<w n="7.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="7.10">l</w>’<w n="7.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rt<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="8" num="3.2" lm="12" met="6+6"><w n="8.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="8.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="8.4" punct="vg:6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="8.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.6">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="8.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="8.8" punct="pt:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>t<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="9" num="4.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="9.2">n</w>’<w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>gti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="9.7" punct="vg:8">r<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>g</rhyme></w>,</l>
					<l n="10" num="4.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="10.3">n</w>’<w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="10.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="10.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>t</w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="10.8" punct="pt:8">gr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="11" num="4.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="11.1">N</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="11.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="11.7">gr<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</rhyme></w></l>
					<l n="12" num="4.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu</w>’<w n="12.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="12.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.6" punct="pi:8">pl<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi ps">e</seg></rhyme></w> ? …</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="5" type="distique" rhyme="aa">
					<l n="13" num="5.1" lm="12" met="6+6"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="13.2">D<seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="M">un</seg>k<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="13.4" punct="vg:6">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg" caesura="1">e</seg>rth</w>,<caesura></caesura> <w n="13.5">c</w>’<w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="13.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="13.8" punct="dp:9">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" punct="dp">a</seg>c</w> : <w n="13.9" punct="vg:12">Tr<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rs<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">ez</seg></rhyme></w>,</l>
					<l n="14" num="5.2" lm="12" met="6+6"><w n="14.1" punct="vg:2">F<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="M">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="14.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="14.5" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="vg" caesura="1">e</seg>r</w>,<caesura></caesura> <w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="14.7" punct="pt:9">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>lf<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="pt" mp="F">e</seg></w>. <w n="14.8">C</w>’<w n="14.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="14.10" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">ez</seg></rhyme></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="15" num="6.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">A</seg>d<seg phoneme="a" type="vs" value="1" rule="145" place="3">a</seg>m</w> <w n="15.3">c<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>lt<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="15.5" punct="vg:8">r<rhyme label="a" id="8" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="16" num="6.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="16.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="16.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="16.5">g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</w> <w n="16.6" punct="pv:8">v<rhyme label="b" id="9" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="pv">e</seg>rts</rhyme></w> ;</l>
					<l n="17" num="6.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="17.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.3">v<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg>s</w> <w n="17.4">s</w>’<w n="17.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>scr<rhyme label="a" id="8" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="18" num="6.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="18.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>ps</w> <w n="18.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="18.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>il</w> <w n="18.6" punct="pt:8"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<rhyme label="b" id="9" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="pt">e</seg>rts</rhyme></w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Taie, taie, thé, t, Tay, Thaix.</note>
					</closer>
			</div></body></text></TEI>