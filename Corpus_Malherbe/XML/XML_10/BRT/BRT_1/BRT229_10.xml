<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS CARRÉS</head><div type="poem" key="BRT229" modus="cm" lm_max="12" metProfile="6+6" form="suite de strophes" schema="3[abab]">
				<head type="number">IV</head>
				<lg n="1" type="regexp" rhyme="abababababab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="1.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="1.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>ls</w><caesura></caesura> <w n="1.6">p<seg phoneme="o" type="vs" value="1" rule="435" place="7" mp="M">o</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="1.8" punct="pt:12">sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="378" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="2.3" punct="vg:3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="2.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="C">i</seg>l</w> <w n="2.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="2.6" punct="pe:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pe" caesura="1">ou</seg>t</w> !<caesura></caesura> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="2.9" punct="pt:12">m<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pt">an</seg></rhyme></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6">— <w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem/mp">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M/mp">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="3.2" punct="vg:4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg></w>, <w n="3.3" punct="vg:6">m<seg phoneme="e" type="vs" value="1" rule="353" place="5" mp="M">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="397" place="6" punct="vg" caesura="1">eu</seg>rs</w>,<caesura></caesura> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="3.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="3.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">on</seg>sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="378" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="4.2">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rc<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" mp="M">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>x</w><caesura></caesura> <w n="4.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="4.4" punct="dp:12">fi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10" mp="M">è</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="dp">en</seg>t</rhyme></w> :</l>
					<l n="5" num="1.5" lm="12" met="6+6">— <w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="5.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="5.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="5.8">T<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="Lc">a</seg>rn</w>-<w n="5.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10" mp="Lc">e</seg>t</w>-<w n="5.10">G<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M/mc">a</seg>r<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="2">om</seg></w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="6.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>dr<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>t</w><caesura></caesura> <w n="6.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="6.7">C<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M/mc">a</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9" mp="Lc">e</seg>l</w>-<w n="6.8" punct="pe:12">S<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M/mc">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M/mc">a</seg>z<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pe">in</seg></rhyme></w> !</l>
					<l n="7" num="1.7" lm="12" met="6+6">— <w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="7.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="C">i</seg>l</w> <w n="7.4">c<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">aî</seg>t</w><caesura></caesura> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</w> <w n="7.6">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="7.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="7.8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rs<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="8.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="8.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>ph<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="8.7">z<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.8" punct="pt:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg></rhyme></w>.</l>
					<l n="9" num="1.9" lm="12" met="6+6">— <w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="9.2" punct="vg:3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>t</w>, <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="9.4" punct="vg:6">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" punct="vg" caesura="1">oin</seg></w>,<caesura></caesura> <w n="9.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="9.6">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="9.8" punct="vg:12">M<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>d<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="10.3">r<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="10.5" punct="vg:6">C<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>lch<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>xh<seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="M">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="10.8">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>x</w> <w n="10.9" punct="pe:12"><rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="o" type="vs" value="1" rule="432" place="12" punct="pe">o</seg>s</rhyme></w> !</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="11.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="11.6">m<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="11.8" punct="pv:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>d<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="12.2">c<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="12.3" punct="vg:6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg>t</w>,<caesura></caesura> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>s</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="12.6" punct="vg:10">lu<seg phoneme="i" type="vs" value="1" rule="491" place="10" punct="vg">i</seg></w>, <w n="12.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="12.8" punct="pt:12">s<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pt">o</seg>ts</rhyme></w>. »</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ MAGIE<lb></lb>▪ ALISE<lb></lb>▪ GIMAT<lb></lb>▪ ISAIE<lb></lb>▪ ÉÉTÈS</note>
					</closer>
			</div></body></text></TEI>