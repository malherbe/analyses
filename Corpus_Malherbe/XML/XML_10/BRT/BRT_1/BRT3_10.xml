<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT3" modus="cp" lm_max="12" metProfile="6+6, (6)" form="strophe unique" schema="1(aabccb)">
				<head type="number">III</head>
				<lg n="1" type="sizain" rhyme="aabccb">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="1.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="1.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="1.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="1.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="1.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r</w> <w n="1.8" punct="pi:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>tt<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pi ps">u</seg></rhyme></w> ? …</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="2.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" caesura="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="2.6" punct="vg:9">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9" punct="vg">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="2.8" punct="vg:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rt<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="6"><space unit="char" quantity="12"></space><w n="3.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w> <w n="3.2">fl<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="3.5" punct="pt:6">h<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></w>.</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>s</w> <w n="4.4">s<seg phoneme="y" type="vs" value="1" rule="445" place="4">û</seg>r</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="4.6" punct="vg:6">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="4.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="4.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.9">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>d</w> <w n="4.10" punct="vg:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg>b<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>t</rhyme></w>,</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="5.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="6" caesura="1">ue</seg>il</w><caesura></caesura> <w n="5.5">d</w>’<w n="5.6"><seg phoneme="e" type="vs" value="1" rule="170" place="7" mp="M">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="5.7">qu</w>’<w n="5.8"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l</w> <w n="5.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>b<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>t</rhyme></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="6.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="6.4" punct="vg:6">g<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="6.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.7">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt</w> <w n="6.8">v<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>t</w> <w n="6.9">qu</w>’<w n="6.10"><seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>l</w> <w n="6.11" punct="pt:12">m<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Qui peut le plus peut le moins</note>
					</closer>
			</div></body></text></TEI>