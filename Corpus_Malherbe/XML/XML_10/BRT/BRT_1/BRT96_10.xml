<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT96" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="6[abab]">
				<head type="number">XCVI</head>
				<lg n="1" type="regexp" rhyme="abababababababababababab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>th<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.5" punct="pv:8">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>bl<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg></w> <w n="3.2" punct="vg:3">l<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="3.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="3.4" punct="vg:6">p<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="3.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w> <w n="3.6" punct="vg:8">m<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="4.3">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-8" place="3">e</seg>r</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="4.5">d</w>’<w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="4.7">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="4.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.9" punct="pt:8">bl<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="5.2">qu</w>’<w n="5.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="5.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="5.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="5.7">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="7">ei</seg>gn<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</rhyme></w></l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">l</w>’<w n="6.4" punct="ps:4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="ps">o</seg>r</w>… <w n="6.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="6.6">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="6.8" punct="dp:8">g<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="7.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rd</w> <w n="7.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">im</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>gn<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</rhyme></w></l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="8.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">c<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.5" punct="pv:8">Br<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="9.3"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="9.5" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s<rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></rhyme></w>,</l>
					<l n="10" num="1.10" lm="8" met="8"><w n="10.1">D</w>’<w n="10.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg>s</w> <w n="10.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="10.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.5" punct="vg:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="b" id="6" gender="f" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="11" num="1.11" lm="8" met="8"><w n="11.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="11.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></w></l>
					<l n="12" num="1.12" lm="8" met="8"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.6" punct="pt:8">c<rhyme label="b" id="6" gender="f" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="13" num="1.13" lm="8" met="8"><w n="13.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="13.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="13.3">qu</w>’<w n="13.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="13.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="13.6">f<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>t</w> <w n="13.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.8">j<rhyme label="a" id="7" gender="m" type="a" stanza="4"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</rhyme></w></l>
					<l n="14" num="1.14" lm="8" met="8"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="14.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="14.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="14.6" punct="ps:8">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<rhyme label="b" id="8" gender="f" type="a" stanza="4"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></w>…</l>
					<l n="15" num="1.15" lm="8" met="8"><w n="15.1">N</w>’<w n="15.2" punct="pe:2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="pe">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="15.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="15.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="5">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.5" punct="vg:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>j<rhyme label="a" id="7" gender="m" type="e" stanza="4"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>rs</rhyme></w>,</l>
					<l n="16" num="1.16" lm="8" met="8"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="16.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="16.4">v<seg phoneme="ɛ" type="vs" value="1" rule="385" place="5">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.5" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<rhyme label="b" id="8" gender="f" type="e" stanza="4"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="17" num="1.17" lm="8" met="8"><w n="17.1" punct="vg:1">R<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="17.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="17.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w> <w n="17.4">l</w>’<w n="17.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="17.7" punct="dp:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<rhyme label="a" id="9" gender="m" type="a" stanza="5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="dp">o</seg>r</rhyme></w> :</l>
					<l n="18" num="1.18" lm="8" met="8"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">ch<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ff<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="18.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="18.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rt</w> <w n="18.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.6" punct="pv:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>fr<rhyme label="b" id="10" gender="f" type="a" stanza="5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="19" num="1.19" lm="8" met="8"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="19.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>j<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.3">d</w>’<w n="19.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="19.5">h<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>g</w> <w n="19.6">s<rhyme label="a" id="9" gender="m" type="e" stanza="5"><seg phoneme="ɔ" type="vs" value="1" rule="317" place="8">au</seg>r</rhyme></w></l>
					<l n="20" num="1.20" lm="8" met="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2">î</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="20.3">d</w>’<w n="20.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="20.5"><seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>f</w> <w n="20.6"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="20.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="20.8" punct="pt:8">c<rhyme label="b" id="10" gender="f" type="e" stanza="5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="21" num="1.21" lm="8" met="8"><w n="21.1">C</w>’<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="21.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="21.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="21.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ls</w> <w n="21.6">qu</w>’<w n="21.7"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="21.8">gr<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<rhyme label="a" id="11" gender="m" type="a" stanza="6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></rhyme></w></l>
					<l n="22" num="1.22" lm="8" met="8"><w n="22.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="22.2">fl<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>ts</w> <w n="22.3">pr<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="22.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="22.6" punct="ps:8">P<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ct<rhyme label="b" id="12" gender="f" type="a" stanza="6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></w>…</l>
					<l n="23" num="1.23" lm="8" met="8"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="23.2" punct="vg:2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg></w>, <w n="23.3" punct="vg:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4" punct="vg">ain</seg></w>, <w n="23.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<rhyme label="a" id="11" gender="m" type="e" stanza="6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></rhyme></w></l>
					<l n="24" num="1.24" lm="8" met="8"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="24.2" punct="pi:4">t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4" punct="pi">oin</seg>s</w> ? <w n="24.3">C</w>’<w n="24.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="24.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">A</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="b" id="12" gender="f" type="e" stanza="6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ La caque sent toujours le hareng.</note>
					</closer>
			</div></body></text></TEI>