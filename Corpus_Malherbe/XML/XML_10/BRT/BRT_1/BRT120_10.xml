<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT120" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="3(abab)">
				<head type="number">XX</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="1.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="1.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="1.7" punct="vg:12">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg>ph<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="2.3" punct="vg:3">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3" punct="vg">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="2.5" punct="vg:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="2.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="2.7">dr<seg phoneme="e" type="vs" value="1" rule="353" place="8" mp="M">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="2.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="2.9" punct="pv:12">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>mi<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pv">er</seg></rhyme></w> ;</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="3.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="Lc">ou</seg>rd</w>’<w n="3.4" punct="vg:6">hu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="3.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="3.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>ps</w> <w n="3.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="3.9" punct="vg:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>f<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="4.4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="4.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="4.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="4.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="4.10" punct="pt:12">f<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>mi<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg></rhyme></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="C">i</seg>l</w> <w n="5.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>pr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.4"><seg phoneme="y" type="vs" value="1" rule="453" place="7" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="5.6">l<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="5.7" punct="pv:12">s<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="6.2">d</w>’<w n="6.3" punct="vg:2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" punct="vg">un</seg></w>, <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="6.6" punct="vg:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="6.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="10">en</seg></w> <w n="6.9">s<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t</rhyme></w></l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2" mp="P">e</seg>rs</w> <w n="7.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="7.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d</w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="7.6" punct="pt:6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" punct="pt" caesura="1">ain</seg>s</w>.<caesura></caesura> <w n="7.7">P<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="7.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="7.9">n</w>’<w n="7.10"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="7.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="7.12">s<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="8.3">ch<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>ff<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>pi<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="8.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="8.7">l<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="8.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="8.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="8.10" punct="pt:12">v<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="pt">en</seg>t</rhyme></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="9.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="9.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="9.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="9.7">n<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="9.8" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>tr<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="307" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="10.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="10.5" punct="vg:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="10.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="10.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="10.8" punct="pt:12">tr<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>s<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="pt">o</seg>r</rhyme></w>.</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c</w> <w n="11.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="11.5">m<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="11.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="11.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="11.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>t<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="307" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="12.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>rs</w><caesura></caesura> <w n="12.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="12.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="12.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="12.9" punct="pt:12">s<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>rt</rhyme></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Char-bon.</note>
					</closer>
			</div></body></text></TEI>