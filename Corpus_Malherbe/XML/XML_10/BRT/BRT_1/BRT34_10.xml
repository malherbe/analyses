<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT34" modus="cm" lm_max="12" metProfile="6+6" form="suite de strophes" schema="3[abab]">
				<head type="number">XXXIV</head>
				<lg n="1" type="regexp" rhyme="abababababab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:1">Vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="1" punct="vg">ai</seg></w>, <w n="1.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="1.3" punct="vg:3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg></w>, <w n="1.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="1.6">l</w>’<w n="1.7">h<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>r</w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="1.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="1.10">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">am</seg>p<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="2.2" punct="vg:2">cl<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg>bs</w>, <w n="2.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="2.4" punct="pi:6">b<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pi" caesura="1">a</seg>rds</w> ?<caesura></caesura> <w n="2.5">Qu</w>’<w n="2.6"><seg phoneme="i" type="vs" value="1" rule="497" place="7">y</seg></w> <w n="2.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="Fm">e</seg>s</w>-<w n="2.8" punct="vg:10">v<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="vg">ou</seg>s</w>, <w n="2.9">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>d</w> <w n="2.10" punct="pt:12">Di<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg></rhyme></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6">— <w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">dr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="3.4" punct="vg:6">v<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="3.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="3.8" punct="pv:12">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>t<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2" punct="vg:2">l<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>, <w n="4.3">j</w>’<w n="4.4" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>s</w>, <w n="4.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="4.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.8">c<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.9"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg></w> <w n="4.10">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="10">oin</seg></w> <w n="4.11">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="4.12" punct="pt:12">f<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6">— <w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2" punct="pi:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pi ti" mp="F">e</seg></w> ? — <w n="5.3" punct="vg:6">P<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="5.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="5.5">f<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l</w> <w n="5.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="5.7">l</w>’<w n="5.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>p<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="6.2">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="6.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="6.5" punct="vg:6">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg>s</w>,<caesura></caesura> <w n="6.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="6.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="6.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</rhyme></w>,</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.3">l</w>’<w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="406" place="6" caesura="1">eu</seg>il</w><caesura></caesura> <w n="7.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="7.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="7.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="7.8">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>x</w> <w n="7.9">s<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="8.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="8.8" punct="vg:10">n<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rgu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="8.9" punct="pt:12">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="11" mp="M">o</seg>qu<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</rhyme></w>.</l>
					<l n="9" num="1.9" lm="12" met="6+6">— <w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2" punct="pi:2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="pi ti">i</seg>s</w> ? — <w n="9.3">S<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="9.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="9.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.8">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="9">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="9.10">tr<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="10.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.3">b<seg phoneme="y" type="vs" value="1" rule="445" place="4" mp="M">û</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="10.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.5">j</w>’<w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="10.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="9" mp="M">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="10">ô</seg>t</w> <w n="10.8" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>j<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="12" punct="pt">oin</seg>t</rhyme></w>.</l>
					<l n="11" num="1.11" lm="12" met="6+6">— <w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="11.3" punct="pi:2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="pi ti">ou</seg>t</w> ? — <w n="11.4">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="11.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ls</w> <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="11.7" punct="vg:6">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="11.8">d</w>’<w n="11.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.10"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="11.11">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="11.12" punct="vg:12">r<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="12.4" punct="pt:6">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pt" caesura="1">on</seg>s</w>.<caesura></caesura> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="12.7">s</w>’<w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="12.9">d<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="12.10" punct="pt:12">p<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="12" punct="pt">oin</seg>t</rhyme></w>. »</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Bon chien chasse de race.</note>
					</closer>
			</div></body></text></TEI>