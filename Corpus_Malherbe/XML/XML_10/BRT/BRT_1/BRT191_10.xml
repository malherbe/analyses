<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT191" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique avec alternance de type 2" schema="2{1(aabb) 1(abab)} 1(aabb)">
				<head type="number">XVI</head>
				<lg n="1" type="quatrain" rhyme="aabb">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="1.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="1.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d</w> <w n="1.5" punct="ps:6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="ps" caesura="1">an</seg>g</w>…<caesura></caesura> <w n="1.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="1.7"><seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="C">i</seg>l</w> <w n="1.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="1.9">b<seg phoneme="o" type="vs" value="1" rule="315" place="11">eau</seg></w> <w n="1.10" punct="vg:12">f<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="2.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="2.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="2.5">n</w>’<w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="2.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="2.8">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9">en</seg>t</w> <w n="2.9"><seg phoneme="y" type="vs" value="1" rule="453" place="10" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.10" punct="dp:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ff<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="3.2">n</w>’<w n="3.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>bti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.6">h<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="3.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="3.8" punct="vg:12">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>mi<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Qu<seg phoneme="wa" type="vs" value="1" rule="281" place="1" mp="Lc">oi</seg>qu</w>’<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">i</seg>l</w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="4.5">b<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="4.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="4.7">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="4.8"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="4.9" punct="pt:12">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rmi<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg></rhyme></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>rn<seg phoneme="ɔ" type="vs" value="1" rule="451" place="4" punct="vg">u</seg>m</w>, <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.4" punct="vg:8">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>vr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>f<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">L</w>’<w n="6.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rpr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.5">l</w>’<w n="6.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ti<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></w>.</l>
					<l n="7" num="2.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="7.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="7.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4" punct="vg:5">r<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="7.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.7">c<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="345" place="8">ue</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="8.2">h<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="8.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="8.4">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6" punct="pt:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>ti<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3" type="quatrain" rhyme="aabb">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">Qu</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="9.3">s</w>’<w n="9.4"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>ct<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="9.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="9.9">l</w>’<w n="9.10" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>g<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="10.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="10.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg></w> <w n="10.4">c<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="10.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>rb<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.8" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="11.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="11.5" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="11.6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="11.7">m</w>’<w n="11.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9" mp="M">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="11.9" punct="vg:12">p<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg></rhyme></w>,</l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="M">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="12.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="12.3">l</w>’<w n="12.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="12.5"><seg phoneme="i" type="vs" value="1" rule="497" place="5" mp="C">y</seg></w> <w n="12.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>t</w><caesura></caesura> <w n="12.7">f<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t</w> <w n="12.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="12.10">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11">e</seg>l</w> <w n="12.11" punct="pt:12">bl<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg></rhyme></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">sc<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ptr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="13.7" punct="pv:8">pu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ss<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="14" num="4.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="14.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="14.5" punct="pv:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pv">ai</seg>s</rhyme></w> ;</l>
					<l n="15" num="4.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="15.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="15.2" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="15.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="15.5">l</w>’<w n="15.6" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="16" num="4.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="16.2" punct="pv:3">c<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="pv">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ; <w n="16.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="16.4" punct="pv:7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" punct="pv">e</seg></w> ; <w n="16.5" punct="ps:8">m<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="ps">ai</seg>s</rhyme></w>…</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="5" type="quatrain" rhyme="aabb">
					<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1">J</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="17.3">v<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="17.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="17.5">d</w>’<w n="17.6">H<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>ds<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="17.8">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="17.9">s<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="10">en</seg>s</w> <w n="17.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>c<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="18.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="18.3">fl<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>ts</w> <w n="18.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>ts</w><caesura></caesura> <w n="18.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="18.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="18.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>il</w> <w n="18.8" punct="pt:12">c<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="19.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="19.3">N<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="19.4" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" punct="vg" caesura="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="19.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="19.6">pi<seg phoneme="e" type="vs" value="1" rule="241" place="8">e</seg>d</w> <w n="19.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="19.8">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="19.9" punct="vg:12">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>lc<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="vg">an</seg></rhyme></w>,</l>
					<l n="20" num="5.4" lm="12" met="6+6"><w n="20.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="20.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>s</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="20.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="20.5" punct="pt:6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt" caesura="1">eu</seg>x</w>.<caesura></caesura> <w n="20.6">L<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="20.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem/mp">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M/mp">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="10">ai</seg></w>-<w n="20.8" punct="pi:11">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="11" punct="pi" mp="Fm">e</seg></w> ? <w n="20.9" punct="pi:12">Qu<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pi">an</seg>d</rhyme></w> ?</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ B, baie, baie, bey, baie.</note>
					</closer>
			</div></body></text></TEI>