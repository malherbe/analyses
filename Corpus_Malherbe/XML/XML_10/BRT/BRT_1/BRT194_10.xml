<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT194" modus="cp" lm_max="12" metProfile="6+6, (2)" form="suite de strophes" schema="3[aa] 2[abba] 1[abbacca]">
				<head type="number">XIX</head>
				<lg n="1" type="regexp" rhyme="aaa">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">S<seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M/mp">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="Lp">ez</seg></w>-<w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="1.4">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="1.5">d</w>’<w n="1.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="9">ain</seg></w> <w n="1.8">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="1.10">s<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="y" type="vs" value="1" rule="445" place="12">û</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g</w> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="2.5" punct="vg:6">tr<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg" caesura="1">e</seg>t</w>,<caesura></caesura> <w n="2.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="2.7">ch<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.8">n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="2.9" punct="pi:12">bl<seg phoneme="e" type="vs" value="1" rule="353" place="11" mp="M">e</seg>ss<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					<l n="3" num="1.3" lm="2"><space unit="char" quantity="20"></space><w n="3.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="3.2" punct="pe:2">p<rhyme label="a" id="2" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</rhyme></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2" type="regexp" rhyme="bba">
					<l n="4" num="2.1" lm="12" met="6+6"><w n="4.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M/mp">ou</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M/mp">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="4.2" punct="vg:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>s</w>, <w n="4.3">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="5" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="4.7">d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="4.8">d</w>’<w n="4.9" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>v<rhyme label="b" id="3" gender="f" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="5" num="2.2" lm="12" met="6+6"><w n="5.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="5.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="5.3">sc<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ptr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="5.4">d</w>’<w n="5.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="5.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="5.8" punct="pi:12">v<rhyme label="b" id="3" gender="f" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					<l n="6" num="2.3" lm="2"><space unit="char" quantity="20"></space><w n="6.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="6.2" punct="pe:2">p<rhyme label="a" id="2" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</rhyme></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3" type="regexp" rhyme="aaa">
					<l n="7" num="3.1" lm="12" met="6+6"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">A</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="Lp">ez</seg></w>-<w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="7.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="7.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">am</seg>ps</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="7.8">gl<rhyme label="a" id="4" gender="f" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="8" num="3.2" lm="12" met="6+6"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="8.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="C">eu</seg>r</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="8.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6" caesura="1">e</seg>d</w><caesura></caesura> <w n="8.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>c<seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="8.8" punct="pi:12">tr<rhyme label="a" id="4" gender="f" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					<l n="9" num="3.3" lm="2"><space unit="char" quantity="20"></space><w n="9.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="9.2" punct="pe:2">p<rhyme label="a" id="5" gender="m" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</rhyme></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4" type="regexp" rhyme="bba">
					<l n="10" num="4.1" lm="12" met="6+6"><w n="10.1">R<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1" mp="M/mp">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="Lp">ez</seg></w>-<w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="10.3">d</w>’<w n="10.4">h<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="10.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="10.6">c<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="10.7">br<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>t<rhyme label="b" id="6" gender="f" type="a" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="11" num="4.2" lm="12" met="6+6"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="11.2">s<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>l</w><caesura></caesura> <w n="11.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="11.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.6">l</w>’<w n="11.7" punct="pi:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>t<rhyme label="b" id="6" gender="f" type="e" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					<l n="12" num="4.3" lm="2"><space unit="char" quantity="20"></space><w n="12.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="12.2" punct="pe:2">p<rhyme label="a" id="5" gender="m" type="e" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</rhyme></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="5" type="regexp" rhyme="aaa">
					<l n="13" num="5.1" lm="12" met="6+6"><w n="13.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M/mp">ou</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M/mp">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="13.4" punct="vg:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="13.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="13.7" punct="vg:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>s<rhyme label="a" id="7" gender="f" type="a" stanza="5"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="14" num="5.2" lm="12" met="6+6"><w n="14.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="14.3">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="14.4">m<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rs</w> <w n="14.5">d</w>’<w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">A</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.7"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="14.8" punct="pi:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493" place="10" mp="M">y</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>s<rhyme label="a" id="7" gender="f" type="e" stanza="5"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					<l n="15" num="5.3" lm="2"><space unit="char" quantity="20"></space><w n="15.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="15.2" punct="pe:2">p<rhyme label="a" id="8" gender="m" type="a" stanza="6"><seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</rhyme></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="6" type="regexp" rhyme="bba">
					<l n="16" num="6.1" lm="12" met="6+6"><w n="16.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1" mp="C">eu</seg>r</w> <w n="16.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M/mp">é</seg>f<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M/mp">é</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem/mp">e</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="5" mp="Lp">ez</seg></w>-<w n="16.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="16.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8">im</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="16.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">â</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<rhyme label="b" id="9" gender="f" type="a" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="17" num="6.2" lm="12" met="6+6"><w n="17.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="17.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="17.3">pr<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="17.4">v<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>lg<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="17.6">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="17.7" punct="pi:12">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="b" id="9" gender="f" type="e" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					<l n="18" num="6.3" lm="2"><space unit="char" quantity="20"></space><w n="18.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="18.2" punct="pe:2">p<rhyme label="a" id="8" gender="m" type="e" stanza="6"><seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</rhyme></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="7" type="regexp" rhyme="cca">
					<l n="19" num="7.1" lm="12" met="6+6"><w n="19.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M/mp">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M/mp">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="19.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="19.4">l</w>’<w n="19.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="19.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="19.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="19.8" punct="vg:12">b<rhyme label="c" id="10" gender="f" type="a" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="20" num="7.2" lm="12" met="6+6"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="20.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="20.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="20.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="20.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="20.7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="9" mp="M">ue</seg>ill<seg phoneme="ø" type="vs" value="1" rule="403" place="10">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="20.8">qu</w>’<w n="20.9" punct="pi:12"><rhyme label="c" id="10" gender="f" type="e" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg>s</rhyme></w> ?</l>
					<l n="21" num="7.3" lm="2"><space unit="char" quantity="20"></space><w n="21.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="21.2" punct="pe:2">p<rhyme label="a" id="8" gender="m" type="a" stanza="6"><seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</rhyme></w> !</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Rênes, reine, rennes, Rennes, Reine (Sainte), reine-marguerite, reine-claude, Reine.</note>
					</closer>
			</div></body></text></TEI>