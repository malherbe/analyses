<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT81" modus="cp" lm_max="12" metProfile="6, 6+6" form="suite de strophes" schema="3[abab]">
				<head type="number">LXXXI</head>
				<lg n="1" type="regexp" rhyme="abababababab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">di<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="Lp">en</seg>d</w>-<w n="1.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>l</w><caesura></caesura> <w n="1.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="1.7">v<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="1.8" punct="pi:12">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rt<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi ps" mp="F">e</seg>s</rhyme></w> ? …</l>
					<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="12"></space>— <w n="2.1">C</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="2.4">l</w>’<w n="2.5" punct="pt:6"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pt">é</seg></rhyme></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6">— <w n="3.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="3.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="Lp">ai</seg>t</w>-<w n="3.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="3.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="3.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="P">a</seg>r</w> <w n="3.9" punct="pi:12">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11" mp="M">en</seg>t<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi ps" mp="F">e</seg>s</rhyme></w> ? …</l>
					<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="12"></space>— <w n="4.1">C</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="4.5" punct="pt:6">v<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pt">é</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="C">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="5.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="5.5" punct="dp:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="dp" caesura="1">ou</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> :<caesura></caesura> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="5.7" punct="vg:8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>d</w>, <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="5.9">l<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="6" num="1.6" lm="6" met="6"><space unit="char" quantity="12"></space><w n="6.1">L</w>’<w n="6.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="6.5" punct="pe:6">b<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="pe">o</seg>rds</rhyme></w> !</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">È</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="7.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>t</w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="7.5" punct="vg:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="7.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" mp="M">im</seg>pr<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="7.8">m<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="8" num="1.8" lm="6" met="6"><space unit="char" quantity="12"></space><w n="8.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="8.3">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="8.5" punct="pe:6">t<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="pe ps">o</seg>rts</rhyme></w> ! …</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="9.2">p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="9.4">l</w>’<w n="9.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">É</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="221" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.7">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="9.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="9.9">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="10" num="1.10" lm="6" met="6"><space unit="char" quantity="12"></space><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="10.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.3" punct="ps:6">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="ps">on</seg></rhyme></w>…</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="11.3">m<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="11.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="11.5" punct="dp:6">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="dp" caesura="1">é</seg></w> :<caesura></caesura> <w n="11.6" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" punct="vg" mp="F">e</seg>s</w>, <w n="11.7" punct="vg:10">cr<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="vg" mp="F">e</seg>s</w>, <w n="11.8" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>m<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="12" num="1.12" lm="6" met="6"><space unit="char" quantity="12"></space><w n="12.1" punct="ps:1">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="ps">o</seg>rt</w>… <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="12.3" punct="pe:6">d<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>mn<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pe ps">on</seg></rhyme></w> ! …</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ L’oisiveté est la mère de tous les vices.</note>
					</closer>
			</div></body></text></TEI>