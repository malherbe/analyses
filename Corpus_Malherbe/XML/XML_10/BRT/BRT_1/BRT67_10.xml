<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT67" modus="cm" lm_max="10" metProfile="5+5" form="suite de strophes" schema="3[abab]">
				<head type="number">LXVII</head>
				<lg n="1" type="regexp" rhyme="abababababab">
					<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="1.3"><seg phoneme="i" type="vs" value="1" rule="497" place="4" mp="M">Y</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg></w><caesura></caesura> <w n="1.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="1.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d</w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="1.8">f<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l</w> <w n="2.4">gr<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>s</w><caesura></caesura> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="2.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.7" punct="vg:10">K<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rpi<rhyme label="b" id="6" gender="m" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" punct="vg">e</seg>ds</rhyme></w>,</l>
					<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="3.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" caesura="1">ai</seg>t</w><caesura></caesura> <w n="3.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="3.6" punct="vg:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>, <w n="3.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="9">en</seg></w> <w n="3.8" punct="vg:10">b<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="4.3">l</w>’<w n="4.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" mp="M">in</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>ct<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="4.7">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg>x</w> <w n="4.9" punct="pt:10">pi<rhyme label="b" id="6" gender="m" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="241" place="10" punct="pt">e</seg>ds</rhyme></w>.</l>
					<l n="5" num="1.5" lm="10" met="5+5"><w n="5.1" punct="vg:1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1" punct="vg">O</seg>r</w>, <w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="5.3">cl<seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="5.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>t</w><caesura></caesura> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg></w> <w n="5.6">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>r</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="5.9" punct="vg:10">b<rhyme label="a" id="2" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="6" num="1.6" lm="10" met="5+5"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="6.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="6.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="6.7">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="6.8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>v<rhyme label="b" id="3" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l</rhyme></w></l>
					<l n="7" num="1.7" lm="10" met="5+5"><w n="7.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="7.4" punct="pt:5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="4" mp="M">oi</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pt" caesura="1">er</seg></w>.<caesura></caesura> <w n="7.5"><seg phoneme="i" type="vs" value="1" rule="497" place="6" mp="M">Y</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="7.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rd</w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="7.8">t<rhyme label="a" id="2" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
					<l n="8" num="1.8" lm="10" met="5+5"><w n="8.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="8.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="8.4" punct="vg:5">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg" caesura="1">ou</seg>x</w>,<caesura></caesura> <w n="8.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="8.6">l</w>’<w n="8.7" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>m<rhyme label="b" id="3" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>l</rhyme></w>.</l>
					<l n="9" num="1.9" lm="10" met="5+5"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem/mp">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="2" mp="Lp">i</seg></w>-<w n="9.2" punct="vg:3">c<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="9.3" punct="vg:5">r<seg phoneme="y" type="vs" value="1" rule="d-3" place="4" mp="M">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="9.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="9.7" punct="pv:10">c<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>r<rhyme label="a" id="4" gender="f" type="a" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="10" num="1.10" lm="10" met="5+5"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.3">d</w>’<w n="10.4"><seg phoneme="i" type="vs" value="1" rule="497" place="4" mp="M">Y</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg></w><caesura></caesura> <w n="10.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="10.7" punct="vg:10">b<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>t<rhyme label="b" id="5" gender="m" type="a" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10" punct="vg">o</seg>r</rhyme></w>,</l>
					<l n="11" num="1.11" lm="10" met="5+5"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="11.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>ppl<seg phoneme="i" type="vs" value="1" rule="469" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="11.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5" caesura="1">ain</seg></w><caesura></caesura> <w n="11.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="P">ou</seg>r</w> <w n="11.6">qu</w>’<w n="11.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="11.8">lu<seg phoneme="i" type="vs" value="1" rule="491" place="8" mp="C">i</seg></w> <w n="11.9" punct="ps:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rd<rhyme label="a" id="4" gender="f" type="e" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg></rhyme></w>…</l>
					<l n="12" num="1.12" lm="10" met="5+5"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="12.2" punct="vg:2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>t</w>, <w n="12.3">n</w>’<w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w>-<w n="12.5">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="4" mp="F">e</seg></w> <w n="12.6" punct="pi:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pi" caesura="1">a</seg>s</w> ?<caesura></caesura> <w n="12.7">l</w>’<w n="12.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="12.9"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.10">l</w>’<w n="12.11"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="12.12" punct="pt:10">t<rhyme label="b" id="5" gender="m" type="e" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" punct="pt">o</seg>rt</rhyme></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Un clou chasse l’autre.</note>
					</closer>
			</div></body></text></TEI>