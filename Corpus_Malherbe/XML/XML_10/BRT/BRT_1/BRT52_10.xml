<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT52" modus="cm" lm_max="12" metProfile="6+6" form="strophe unique" schema="1(aabccbdede)">
				<head type="number">LII</head>
				<lg n="1" type="dizain" rhyme="aabccbdede">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>lch<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="1.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="1.5" punct="vg:6">bl<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" punct="vg" caesura="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.8">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.9" punct="vg:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">In</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="2.3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>cs</w><caesura></caesura> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.5">l</w>’<w n="2.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.7" punct="vg:12">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rn<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="3.2">m<seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="M">oi</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="3.3">f<seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="3.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="6" caesura="1">à</seg></w><caesura></caesura> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="3.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="3.8">d</w>’<w n="3.9" punct="ps:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>sp<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="ps">oi</seg>r</rhyme></w>…</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="4.2">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="4.5" punct="vg:6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="4.6">l</w>’<w n="4.7"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>s</w> <w n="4.9">l</w>’<w n="4.10" punct="vg:11"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" punct="vg" mp="F">e</seg></w>, <w n="4.11" punct="pv:12">s<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>nt</rhyme></w> ;</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d</w> <w n="5.5" punct="ps:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="ps" caesura="1">a</seg>s</w>…<caesura></caesura> <w n="5.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="5.7"><seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="5.8" punct="ps:12">b<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>rd<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg>nt</rhyme></w>…</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="6.5">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="8">en</seg>t</w> <w n="6.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="P">ou</seg>s</w> <w n="6.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="6.8" punct="pe:12">p<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pe">oi</seg>r</rhyme></w> !</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1" punct="vg:2">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="7.2">l</w>’<w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="7.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="7.5">l</w>’<w n="7.6">h<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="7.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="7.8">s</w>’<w n="7.9" punct="dp:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>cl<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="8.2">n</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.4" punct="vg:3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3" punct="vg">oin</seg>t</w>, <w n="8.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" mp="P">e</seg>rs</w> <w n="8.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="8.7" punct="vg:6">b<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>t</w>,<caesura></caesura> <w n="8.8"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="8.9">d</w>’<w n="8.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="8.11">s<seg phoneme="œ" type="vs" value="1" rule="407" place="11">eu</seg>l</w> <w n="8.12" punct="pe:12">p<rhyme label="e" id="5" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pe ps">a</seg>s</rhyme></w> ! …</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>rch<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="9.5" punct="vg:6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg" caesura="1">em</seg>ps</w>,<caesura></caesura> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>h</w> <w n="9.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="9.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="10">ez</seg></w> <w n="9.9">b<seg phoneme="o" type="vs" value="1" rule="315" place="11">eau</seg></w> <w n="9.10">f<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="10.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="10.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="10.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="10.7">n</w>’<w n="10.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>tt<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="10" mp="M">ein</seg>dr<seg phoneme="e" type="vs" value="1" rule="347" place="11">ez</seg></w> <w n="10.9" punct="pt:12">p<rhyme label="e" id="5" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>s</rhyme></w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Tête de fou ne blanchit pas.</note>
					</closer>
			</div></body></text></TEI>