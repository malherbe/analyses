<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><div type="poem" key="AIC25" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="9(abab)">
					<head type="number">IX</head>
					<head type="main">ΨΥΧΗ</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">P<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="1.5">l</w>’<w n="1.6"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="7">Â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">Gr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="2.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l</w> <w n="2.6" punct="pv:7">n<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="7" punct="pv">om</seg></rhyme></w> ;</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="3.2" punct="pe:4">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="3">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="pe">e</seg>s</w> ! <w n="3.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>cl<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="7" met="7"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">Gr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="4.5" punct="pt:7">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pt">on</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="7" met="7"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">Â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="5.4">l</w>’<w n="5.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ct<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="5.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="5.8"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
						<l n="6" num="2.2" lm="7" met="7"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="6.2">fu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>r</w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.7" punct="pv:7">m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pv">a</seg>l</rhyme></w> ;</l>
						<l n="7" num="2.3" lm="7" met="7"><w n="7.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="7.3">Ps<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="7.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="7.6"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
						<l n="8" num="2.4" lm="7" met="7"><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="8.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.3" punct="pt:7"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pt">a</seg>l</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="7" met="7"><w n="9.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="9.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.3" punct="vg:7">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>pr<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="10" num="3.2" lm="7" met="7"><w n="10.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="10.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="10.6" punct="vg:7">fl<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="vg">eu</seg>r</rhyme></w>,</l>
						<l n="11" num="3.3" lm="7" met="7"><w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w>-<w n="11.6">m<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="12" num="3.4" lm="7" met="7"><w n="12.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.4" punct="pt:7">b<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>nh<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="pt">eu</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="7" met="7"><w n="13.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="13.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="13.3">n</w>’<w n="13.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="13.5">qu</w>’<w n="13.6"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7" punct="ps:7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ss<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="ps">e</seg></rhyme></w>…</l>
						<l n="14" num="4.2" lm="7" met="7"><w n="14.1" punct="vg:1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1" punct="vg">eu</seg></w>, <w n="14.2">l</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="14.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="14.7" punct="vg:7">m<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7" punct="vg">ain</seg></rhyme></w>,</l>
						<l n="15" num="4.3" lm="7" met="7"><w n="15.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ffl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="15.2">l</w>’<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="15.5">l</w>’<w n="15.6"><seg phoneme="e" type="vs" value="1" rule="354" place="5">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>st<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="16" num="4.4" lm="7" met="7"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="16.2">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="16.4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="16.5" punct="pt:7">h<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>m<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7" punct="pt">ain</seg></rhyme></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="7" met="7"><w n="17.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="17.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">l</w>’<w n="17.5">h<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="5">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.6" punct="vg:7">d<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="18" num="5.2" lm="7" met="7"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">l</w>’<w n="18.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="18.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="18.5" punct="vg:7">S<seg phoneme="ɛ" type="vs" value="1" rule="384" place="6">ei</seg>gn<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="vg">eu</seg>r</rhyme></w>,</l>
						<l n="19" num="5.3" lm="7" met="7"><w n="19.1" punct="vg:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg></w>, <w n="19.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="19.3">l</w>’<w n="19.4">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="19.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="19.7" punct="vg:7">m<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="20" num="5.4" lm="7" met="7"><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="20.2">c<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="20.3">d</w>’<w n="20.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="20.5" punct="pt:7">fl<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="pt">eu</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="7" met="7"><w n="21.1" punct="vg:1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1" punct="vg">O</seg>r</w>, <w n="21.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="21.4" punct="vg:4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>p</w>, <w n="21.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="21.6">c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="22" num="6.2" lm="7" met="7"><w n="22.1">S</w>’<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="22.3" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>m<seg phoneme="y" type="vs" value="1" rule="457" place="3" punct="vg">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="22.4" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="189" place="4" punct="vg">e</seg>t</w>, <w n="22.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="22.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="22.7" punct="vg:7">ci<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="23" num="6.3" lm="7" met="7"><w n="23.1" punct="vg:3">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="23.3">s</w>’<w n="23.4" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>v<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="24" num="6.4" lm="7" met="7"><w n="24.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>c</w> <w n="24.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="24.3" punct="pv:7">r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><rhyme label="b" id="12" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pv">eu</seg>x</rhyme></w> ;</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="abab">
						<l n="25" num="7.1" lm="7" met="7"><w n="25.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="25.2">l</w>’<w n="25.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">É</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="221" place="3">en</seg></w> <w n="25.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="25.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="25.6">br<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></w></l>
						<l n="26" num="7.2" lm="7" met="7"><w n="26.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="26.2">pr<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>ds</w> <w n="26.3">p<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>mmi<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg>s</w> <w n="26.4" punct="vg:7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>bl<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>ts</rhyme></w>,</l>
						<l n="27" num="7.3" lm="7" met="7"><w n="27.1">N</w>’<w n="27.2"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="1">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="27.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="27.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="27.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="27.6" punct="vg:7">bl<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="28" num="7.4" lm="7" met="7"><w n="28.1">N</w>’<w n="28.2"><seg phoneme="y" type="vs" value="1" rule="391" place="1">eu</seg>t</w> <w n="28.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="28.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="28.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="28.6" punct="pt:7">bl<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="pt">an</seg>cs</rhyme></w>.</l>
					</lg>
					<lg n="8" type="quatrain" rhyme="abab">
						<l n="29" num="8.1" lm="7" met="7"><w n="29.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="29.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="29.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="29.4">p<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="29.5">d</w>’<w n="29.6" punct="vg:7"><rhyme label="a" id="15" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">È</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="30" num="8.2" lm="7" met="7"><w n="30.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="30.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="30.3">cl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="30.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="30.5">l</w>’<w n="30.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>th<rhyme label="b" id="16" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>r</rhyme></w></l>
						<l n="31" num="8.3" lm="7" met="7"><w n="31.1">N<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>l</w> <w n="31.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="31.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="31.4">s</w>’<w n="31.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<rhyme label="a" id="15" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="32" num="8.4" lm="7" met="7"><w n="32.1">Qu</w>’<w n="32.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="32.3">n</w>’<w n="32.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="32.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="32.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="32.8" punct="pt:7">v<rhyme label="b" id="16" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" punct="pt">e</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="9" type="quatrain" rhyme="abab">
						<l n="33" num="9.1" lm="7" met="7"><w n="33.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="33.2" punct="pe:3">m<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3" punct="pe">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="33.3"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">È</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="33.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="33.6">p<rhyme label="a" id="17" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="34" num="9.2" lm="7" met="7"><w n="34.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="34.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg></w> <w n="34.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="34.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="34.5" punct="vg:7">bl<rhyme label="b" id="18" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="vg">eu</seg></rhyme></w>,</l>
						<l n="35" num="9.3" lm="7" met="7"><w n="35.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="35.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="35.3">chr<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="35.5">l</w>’<w n="35.6" punct="vg:7">h<rhyme label="a" id="17" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="36" num="9.4" lm="7" met="7"><foreign lang="greek"><subst reason="analysis" type="phonemization" hand="RR"><del>Ψυχή</del><add rend="hidden"><w n="36.1" punct="vg:2">ps<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="414" place="2" punct="vg">ë</seg></w></add></subst></foreign>, <w n="36.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="36.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="36.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="36.5">Di<rhyme label="b" id="18" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></rhyme></w></l>
					</lg>
					<closer>
						<dateline>
							<date when="1866">6 septembre 1866</date>.
							</dateline>
					</closer>
				</div></body></text></TEI>