<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES DÉLIQUESCENCES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA" sort="1">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<author key="VIC" sort="2">
					<name>
						<forename>Gabriel</forename>
						<surname>VICAIRE</surname>
					</name>
					<date from="1848" to="1900">1848-1900</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>				
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>297 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Déliquescences</title>
						<author>Henri Beauclair et Gabriel Vicaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 - 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/deliqu01.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Déliquescences</title>
								<author>Henri Beauclair et Gabriel Vicaire</author>
								<imprint>
									<publisher>Les Editions Henri Jonquières et Cie, Paris</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEV3" modus="cm" lm_max="9" metProfile="3+6" form="suite périodique" schema="2(abba)">
				<head type="main">Pour être conspué</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="9" met="3+6"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="3" caesura="1">é</seg>s</w><caesura></caesura> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="1.3">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg></w> <w n="1.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="1.5" punct="vg:9">br<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="2" num="1.2" lm="9" met="3+6"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="2.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rç<seg phoneme="y" type="vs" value="1" rule="450" place="3" caesura="1">u</seg></w><caesura></caesura> <w n="2.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="2.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="2.6" punct="vg:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>ls</w>, <w n="2.7" punct="pv:9">pr<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					<l n="3" num="1.3" lm="9" met="3+6"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301" place="3" caesura="1">ai</seg>s</w><caesura></caesura> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rs</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6" mp="P">à</seg></w> <w n="3.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.6" punct="vg:9">fr<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="9" met="3+6"><w n="4.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">em</seg>br<seg phoneme="e" type="vs" value="1" rule="409" place="3" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="4.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="4.5">d</w>’<w n="4.6" punct="pe:9"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pe" mp="F">e</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abba">
					<l n="5" num="2.1" lm="9" met="3+6"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" caesura="1">an</seg>t</w><caesura></caesura> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="5.5">T<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6">en</seg>t</w> <w n="5.6">d</w>’<w n="5.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="5.8" punct="pt:9"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>sc<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="6" num="2.2" lm="9" met="3+6"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="6.2" punct="pi:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pi" caesura="1">a</seg>s</w> ?<caesura></caesura> <w n="6.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.5" punct="vg:6">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>s</w>, <w n="6.6" punct="vg:7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></w>, <w n="6.7">n<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>l</w> <w n="6.8" punct="vg:9"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="7" num="2.3" lm="9" met="3+6">— <w n="7.1">L</w>’<w n="7.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">Oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="7.3">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="3" caesura="1">eu</seg></w><caesura></caesura> <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.6">Chr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="7.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="7.8" punct="pt:9">v<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pt ti" mp="F">e</seg></rhyme></w>. —</l>
					<l n="8" num="2.4" lm="9" met="3+6"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2" punct="vg:3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" punct="vg" caesura="1">en</seg>d</w>,<caesura></caesura> <w n="8.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="8.5">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6">en</seg>s</w> <w n="8.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="8.7" punct="pe:9">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>z<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" punct="pe" mp="F">e</seg></rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>