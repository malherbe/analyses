<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VILLANELLES</head><div type="poem" key="BOR26" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="1(ababcbcb) 4(ababcdcd)">
							<head type="main">Origine d’une Comtesse</head>
							<opener>
								<epigraph>
									<cit>
										<quote>
										 Ah ! c’est très-bien !…
										</quote>
										<bibl>
											<name>LA CAMARADERIE</name>.
										</bibl>
									</cit>
									<cit>
										<quote>Très peu.</quote>
										<bibl>
											Idem<name></name>.
										</bibl>
									</cit>
								</epigraph>
							</opener>
							<lg n="1" type="huitain" rhyme="ababcbcb">
								<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="pe:1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1" punct="pe">eu</seg></w> ! <w n="1.2" punct="vg:3">M<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg></w>, <w n="1.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="5">en</seg>t</w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="6">e</seg>s</w>-<w n="1.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="1.6" punct="pi:8">f<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
								<l n="2" num="1.2" lm="8" met="8"><w n="2.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="2.2">m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="2.5" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
								<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">d<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="3.7">j<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>qu<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
								<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rt</w> <w n="4.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="4.5" punct="pt:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">im</seg>pr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
								<l n="5" num="1.5" lm="8" met="8"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">c<seg phoneme="œ" type="vs" value="1" rule="345" place="2">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="5.4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="5.5">l</w>’<w n="5.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
								<l n="6" num="1.6" lm="8" met="8"><w n="6.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="6.3" punct="pi:5">m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="5" punct="pi ps">i</seg>t</w> ?… <w n="6.4">V<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="6.5" punct="pe:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">ez</seg></rhyme></w> !</l>
								<l n="7" num="1.7" lm="8" met="8"><w n="7.1">S<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="7.2">d</w>’<w n="7.3" punct="vg:4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="7.4" punct="vg:6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">ez</seg></w>, <w n="7.5" punct="pe:8">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>qu<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
								<l n="8" num="1.8" lm="8" met="8"><w n="8.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="8.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="8.7" punct="pe:8">g<seg phoneme="u" type="vs" value="1" rule="425" place="7">oû</seg>t<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">ez</seg></rhyme></w> !</l>
							</lg>
							<lg n="2" type="huitain" rhyme="ababcdcd">
								<l n="9" num="2.1" lm="8" met="8"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd</w> <w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="9.5" punct="vg:8">h<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
								<l n="10" num="2.2" lm="8" met="8"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="10.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="10.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w> <w n="10.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="10.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="10.6" punct="pi:8">bl<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>m<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pi">i</seg>r</rhyme></w> ?</l>
								<l n="11" num="2.3" lm="8" met="8"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="11.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>x</w>-<w n="11.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>pt</w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="11.7" punct="pe:8">l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
								<l n="12" num="2.4" lm="8" met="8"><w n="12.1" punct="vg:2">M<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="12.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="12.5" punct="pt:8">fr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>r</rhyme></w>.</l>
								<l n="13" num="2.5" lm="8" met="8"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="13.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg>s</w>, <w n="13.4" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="13.5">j</w>’<w n="13.6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="13.7" punct="pv:8">n<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>v<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
								<l n="14" num="2.6" lm="8" met="8"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="14.2">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3" punct="pe:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="pe ps">an</seg>s</w> !… <w n="14.4">V<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="14.6" punct="pi:8">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pi">ez</seg></rhyme></w> ?</l>
								<l n="15" num="2.7" lm="8" met="8"><w n="15.1">C</w>’<w n="15.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="15.4" punct="vg:3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>t</w>, <w n="15.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="15.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="15.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.8" punct="pt:8">v<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
								<l n="16" num="2.8" lm="8" met="8"><w n="16.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="16.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="16.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="16.7" punct="pe:8">g<seg phoneme="u" type="vs" value="1" rule="425" place="7">oû</seg>t<rhyme label="d" id="8" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">ez</seg></rhyme></w> !</l>
							</lg>
							<lg n="3" type="huitain" rhyme="ababcdcd">
								<l n="17" num="3.1" lm="8" met="8"><w n="17.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rs</w>, <w n="17.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="17.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg></w>, <w n="17.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="17.5">p<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
								<l n="18" num="3.2" lm="8" met="8"><w n="18.1">M<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="18.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="18.3" punct="vg:4">m<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="18.6" punct="dp:8">r<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>ss<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="dp">ai</seg>t</rhyme></w> :</l>
								<l n="19" num="3.3" lm="8" met="8"><w n="19.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="19.2" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="pe">on</seg>t</w> ! <w n="19.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>z<seg phoneme="a" type="vs" value="1" rule="343" place="5">a</seg><seg phoneme="i" type="vs" value="1" rule="476" place="6" punct="vg">ï</seg>s</w>, <w n="19.4">j</w>’<w n="19.5" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
								<l n="20" num="3.4" lm="8" met="8"><w n="20.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="20.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="20.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="20.4">qu</w>’<w n="20.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="20.6" punct="pt:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>t</rhyme></w>.</l>
								<l n="21" num="3.5" lm="8" met="8"><w n="21.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="21.2" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">In</seg>chb<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>ld</w>, <w n="21.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="21.4">l</w>’<w n="21.5" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>g<rhyme label="c" id="11" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
								<l n="22" num="3.6" lm="8" met="8"><w n="22.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.2">s</w>’<w n="22.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="22.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="22.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="22.6" punct="pv:8">c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="d" id="12" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pv">é</seg>s</rhyme></w> ;</l>
								<l n="23" num="3.7" lm="8" met="8"><w n="23.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="23.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="23.4">l</w>’<w n="23.5" punct="pe:8"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>c<rhyme label="c" id="11" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
								<l n="24" num="3.8" lm="8" met="8"><w n="24.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="24.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="24.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="24.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="24.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="24.7" punct="pe:8">g<seg phoneme="u" type="vs" value="1" rule="425" place="7">oû</seg>t<rhyme label="d" id="12" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">ez</seg></rhyme></w> !</l>
							</lg>
							<lg n="4" type="huitain" rhyme="ababcdcd">
								<l n="25" num="4.1" lm="8" met="8"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="25.2">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="25.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="25.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="25.6" punct="vg:8">v<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
								<l n="26" num="4.2" lm="8" met="8"><w n="26.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="26.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="26.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="26.4" punct="pv:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rqu<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg></rhyme></w> ;</l>
								<l n="27" num="4.3" lm="8" met="8"><w n="27.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="27.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3">bl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="27.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="27.5">l</w>’<w n="27.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">É</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
								<l n="28" num="4.4" lm="8" met="8"><w n="28.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="28.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="28.3">dr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="28.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>bd<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>qu<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></w>.</l>
								<l n="29" num="4.5" lm="8" met="8"><w n="29.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="29.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="29.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="29.4" punct="vg:5">p<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" punct="vg">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="29.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>lt<rhyme label="c" id="15" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
								<l n="30" num="4.6" lm="8" met="8"><w n="30.1">Qu</w>’<w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="2">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="30.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="30.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>gn<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="d" id="16" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</rhyme></w></l>
								<l n="31" num="4.7" lm="8" met="8"><w n="31.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="31.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="31.3">gr<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="31.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="31.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="31.6" punct="pe:8">m<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>st<rhyme label="c" id="15" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
								<l n="32" num="4.8" lm="8" met="8"><w n="32.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="32.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="32.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="32.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="32.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="32.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="32.7" punct="pe:8">g<seg phoneme="u" type="vs" value="1" rule="425" place="7">oû</seg>t<rhyme label="d" id="16" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">ez</seg></rhyme></w> !</l>
							</lg>
							<lg n="5" type="huitain" rhyme="ababcdcd">
								<l n="33" num="5.1" lm="8" met="8"><w n="33.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" punct="vg">in</seg></w>, <w n="33.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="33.4" punct="vg:6">bl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg></w>, <w n="33.5" punct="pv:8">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ch<rhyme label="a" id="17" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
								<l n="34" num="5.2" lm="8" met="8"><w n="34.1">L</w>’<w n="34.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>gl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="34.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="34.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="34.5" punct="pv:8">b<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>dg<rhyme label="b" id="18" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pv">e</seg>t</rhyme></w> ;</l>
								<l n="35" num="5.3" lm="8" met="8"><w n="35.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="35.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="35.3" punct="vg:4">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>r</w>, <w n="35.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="35.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="35.6" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>vr<rhyme label="a" id="17" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
								<l n="36" num="5.4" lm="8" met="8"><w n="36.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="36.2">pr<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="36.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="36.4" punct="pv:8">C<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="b" id="18" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pv">e</seg>t</rhyme></w> ;</l>
								<l n="37" num="5.5" lm="8" met="8"><w n="37.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rd</w>’<w n="37.2" punct="vg:3">hu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg></w>, <w n="37.3" punct="vg:5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="352" place="5" punct="vg">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="37.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="37.5">l</w>’<w n="37.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>dm<rhyme label="c" id="19" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
								<l n="38" num="5.6" lm="8" met="8"><w n="38.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="38.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt</w> <w n="38.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="38.5" punct="pv:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="366" place="6">e</seg>mn<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="d" id="20" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pv">é</seg>s</rhyme></w> ;</l>
								<l n="39" num="5.7" lm="8" met="8"><w n="39.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="39.2">r<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="39.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="39.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="39.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="39.6" punct="dp:8">d<rhyme label="c" id="19" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
								<l n="40" num="5.8" lm="8" met="8"><w n="40.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="40.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="40.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="40.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="40.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="40.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="40.7" punct="pe:8">g<seg phoneme="u" type="vs" value="1" rule="425" place="7">oû</seg>t<rhyme label="d" id="20" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">ez</seg></rhyme></w> !</l>
							</lg>
						</div></body></text></TEI>