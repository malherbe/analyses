<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RAPSODIES</head><div type="poem" key="BOR9" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="5(aa) 4(abba) 1(abbabb)">
						<head type="main">Fantaisie</head>
						<opener>
							<epigraph>
								<cit>
									<quote>Ça trouillote !</quote>
									<bibl>
										<name>INCONNU</name>.
									</bibl>
								</cit>
								<cit>
									<quote>Surtout vive l’amour ! et bran pour les sergents.</quote>
									<bibl>
										<name>RÉGNIER</name>.
									</bibl>
								</cit>
							</epigraph>
						</opener>
						<lg n="1" type="distique" rhyme="aa">
							<l n="1" num="1.1" lm="7" met="7"><w n="1.1" punct="pe:2"><seg phoneme="wa" type="vs" value="1" rule="420" place="1">Oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="2" punct="pe">eau</seg>x</w> ! <w n="1.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.4">j</w>’<w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>v<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
							<l n="2" num="1.2" lm="7" met="7"><w n="2.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="2.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.5" punct="pe:7">v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></w> !</l>
						</lg>
						<lg n="2" type="quatrain" rhyme="abba">
							<l n="3" num="2.1" lm="7" met="7"><w n="3.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="3.3" punct="vg:7">g<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rn<rhyme label="a" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="307" place="7" punct="vg">a</seg>il</rhyme></w>,</l>
							<l n="4" num="2.2" lm="7" met="7"><w n="4.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.3" punct="vg:7">p<seg phoneme="ɛ" type="vs" value="1" rule="366" place="6">e</seg>nn<rhyme label="b" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
							<l n="5" num="2.3" lm="7" met="7"><w n="5.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="5.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.4" punct="vg:7">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="6">u</seg><rhyme label="b" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
							<l n="6" num="2.4" lm="7" met="7"><w n="6.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">br<seg phoneme="y" type="vs" value="1" rule="454" place="3">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="6.3" punct="pt:7"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t<rhyme label="a" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="307" place="7" punct="pt">a</seg>il</rhyme></w>.</l>
						</lg>
						<lg n="3" type="distique" rhyme="aa">
							<l n="7" num="3.1" lm="7" met="7"><w n="7.1" punct="pe:2"><seg phoneme="wa" type="vs" value="1" rule="420" place="1">Oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="2" punct="pe">eau</seg>x</w> ! <w n="7.2" punct="pe:4"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="pe">eau</seg>x</w> ! <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.4">j</w>’<w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>v<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
							<l n="8" num="3.2" lm="7" met="7"><w n="8.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="8.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5" punct="pe:7">v<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></w> !</l>
						</lg>
						<lg n="4" type="sizain" rhyme="abbabb">
							<l n="9" num="4.1" lm="7" met="7"><w n="9.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="9.2" punct="vg:2">j<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="9.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="9.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="9.6" punct="pv:7">ci<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="7" punct="pv">e</seg>l</rhyme></w> ;</l>
							<l n="10" num="4.2" lm="7" met="7"><w n="10.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>x</w> <w n="10.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="10.4" punct="vg:7">br<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
							<l n="11" num="4.3" lm="7" met="7"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>ch<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg></w> <w n="11.2">d</w>’<w n="11.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.5" punct="vg:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>tr<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="469" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
							<l n="12" num="4.4" lm="7" met="7"><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="12.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="12.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="12.6" punct="pt:7">fi<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="7" punct="pt">e</seg>l</rhyme></w>.</l>
							<l n="13" num="4.5" lm="7" met="7"><w n="13.1" punct="pe:2"><seg phoneme="wa" type="vs" value="1" rule="420" place="1">Oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="2" punct="pe">eau</seg>x</w> ! <w n="13.2" punct="pe:4"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="pe">eau</seg>x</w> ! <w n="13.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.4">j</w>’<w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>v<rhyme label="b" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
							<l n="14" num="4.6" lm="7" met="7"><w n="14.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="14.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="14.5" punct="pe:7">v<rhyme label="b" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></w> !</l>
						</lg>
						<lg n="5" type="quatrain" rhyme="abba">
							<l n="15" num="5.1" lm="7" met="7"><w n="15.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="15.2">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg></w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="15.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="15.5" punct="pv:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rr<rhyme label="a" id="8" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="pv">oi</seg></rhyme></w> ;</l>
							<l n="16" num="5.2" lm="7" met="7"><w n="16.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="16.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="16.4" punct="pv:7">r<rhyme label="b" id="9" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
							<l n="17" num="5.3" lm="7" met="7"><w n="17.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="17.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="17.4">l</w>’<w n="17.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="17.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="17.7" punct="pv:7">pl<rhyme label="b" id="9" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
							<l n="18" num="5.4" lm="7" met="7"><w n="18.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="18.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="18.5" punct="pe:7">r<rhyme label="a" id="8" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="pe">oi</seg></rhyme></w> !</l>
						</lg>
						<lg n="6" type="distique" rhyme="aa">
							<l n="19" num="6.1" lm="7" met="7"><w n="19.1" punct="pe:2"><seg phoneme="wa" type="vs" value="1" rule="420" place="1">Oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="2" punct="pe">eau</seg>x</w> ! <w n="19.2" punct="pe:4"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="pe">eau</seg>x</w> ! <w n="19.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.4">j</w>’<w n="19.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>v<rhyme label="a" id="10" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
							<l n="20" num="6.2" lm="7" met="7"><w n="20.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="20.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="20.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="20.5" punct="pe:7">v<rhyme label="a" id="10" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></w> !</l>
						</lg>
						<lg n="7" type="quatrain" rhyme="abba">
							<l n="21" num="7.1" lm="7" met="7"><w n="21.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="21.2" punct="vg:3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="21.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="21.4" punct="pv:7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<rhyme label="a" id="11" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="pv">an</seg>ts</rhyme></w> ;</l>
							<l n="22" num="7.2" lm="7" met="7"><w n="22.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="22.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="22.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="22.5" punct="pv:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<rhyme label="b" id="12" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
							<l n="23" num="7.3" lm="7" met="7"><w n="23.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="23.2">f<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="23.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="23.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="23.5" punct="pv:7">br<rhyme label="b" id="12" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
							<l n="24" num="7.4" lm="7" met="7"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="24.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="24.3">h<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg>s</w> <w n="24.4" punct="pe:7">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><rhyme label="a" id="11" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="pe">an</seg>ts</rhyme></w> !</l>
						</lg>
						<lg n="8" type="distique" rhyme="aa">
							<l n="25" num="8.1" lm="7" met="7"><w n="25.1" punct="pe:2"><seg phoneme="wa" type="vs" value="1" rule="420" place="1">Oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="2" punct="pe">eau</seg>x</w> ! <w n="25.2" punct="pe:4"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="pe">eau</seg>x</w> ! <w n="25.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="25.4">j</w>’<w n="25.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>v<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
							<l n="26" num="8.2" lm="7" met="7"><w n="26.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="26.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="26.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="26.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="26.5" punct="pe:7">v<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></w> !</l>
						</lg>
						<lg n="9" type="quatrain" rhyme="abba">
							<l n="27" num="9.1" lm="7" met="7"><w n="27.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="27.2">h<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="ø" type="vs" value="1" rule="403" place="3">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="27.3" punct="pv:7">v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>pt<rhyme label="a" id="14" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pv">é</seg></rhyme></w> ;</l>
							<l n="28" num="9.2" lm="7" met="7"><w n="28.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="28.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>j<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>g<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="28.3" punct="pv:7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<rhyme label="b" id="15" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg>s</rhyme></w> ;</l>
							<l n="29" num="9.3" lm="7" met="7"><w n="29.1" punct="pe:1">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" punct="pe">an</seg>cs</w> ! <w n="29.2" punct="pe:5">v<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="pe">e</seg>s</w> ! <w n="29.3" punct="pe:7">s<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>v<rhyme label="b" id="15" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg>s</rhyme></w> !</l>
							<l n="30" num="9.4" lm="7" met="7"><w n="30.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="30.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="30.3" punct="pe:7">l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rt<rhyme label="a" id="14" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pe">é</seg></rhyme></w> !</l>
						</lg>
						<lg n="10" type="distique" rhyme="aa">
							<l n="31" num="10.1" lm="7" met="7"><w n="31.1" punct="pe:2"><seg phoneme="wa" type="vs" value="1" rule="420" place="1">Oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="2" punct="pe">eau</seg>x</w> ! <w n="31.2" punct="pe:4"><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="pe">eau</seg>x</w> ! <w n="31.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="31.4">j</w>’<w n="31.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>v<rhyme label="a" id="16" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
							<l n="32" num="10.2" lm="7" met="7"><w n="32.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="32.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="32.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="32.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="32.5" punct="pe:7">v<rhyme label="a" id="16" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></w> !</l>
						</lg>
						<closer>
							<dateline>
								<placeName>Au cachot, à Écouy, près les Andelys</placeName>,
								<date when="1831">1831</date>
								</dateline>
						</closer>
					</div></body></text></TEI>