<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RAPSODIES</head><div type="poem" key="BOR23" modus="sm" lm_max="7" metProfile="7" form="suite de strophes" schema="1(abbacddc) 1(abbacddcd) 1(abab)">
						<head type="main">À Jules Vabre</head>
						<head type="sub_2">Architecte</head>
						<lg n="1" type="huitain" rhyme="abbacddc">
							<l n="1" num="1.1" lm="7" met="7"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3" punct="vg:4">f<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="1.4">J<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="1.5" punct="vg:7">V<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
							<l n="2" num="1.2" lm="7" met="7"><w n="2.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="2.2" punct="vg:7">m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>l<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="vg">eu</seg>x</rhyme></w>,</l>
							<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="3.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rds</w> <w n="3.3">m<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>l<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</rhyme></w></l>
							<l n="4" num="1.4" lm="7" met="7"><w n="4.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="4.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="4.5" punct="vg:7">gl<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
							<l n="5" num="1.5" lm="7" met="7"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w>-<w n="5.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="5.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="5.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ll<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="7">e</seg>t</rhyme></w></l>
							<l n="6" num="1.6" lm="7" met="7"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="6.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="6.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.7" punct="pe:7">r<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></w> !</l>
							<l n="7" num="1.7" lm="7" met="7"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w>-<w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="7.4" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>tr<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
							<l n="8" num="1.8" lm="7" met="7"><w n="8.1" punct="vg:1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="8.2">f<seg phoneme="œ" type="vs" value="1" rule="304" place="2">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="8.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">qu</w>’<w n="8.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="8.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="8.7" punct="pe:7">pl<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="pe">aî</seg>t</rhyme></w> !</l>
						</lg>
						<lg n="2" type="neuvain" rhyme="abbacddcd">
							<l n="9" num="2.1" lm="7" met="7"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="9.2" punct="vg:3">P<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>s</w>, <w n="9.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ccr<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>p<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
							<l n="10" num="2.2" lm="7" met="7"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="10.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="10.4">br<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="10.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="10.6">l</w>’<w n="10.7" punct="vg:7"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="7" punct="vg">eau</seg></rhyme></w>,</l>
							<l n="11" num="2.3" lm="7" met="7"><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="11.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d</w> <w n="11.4">ru<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>ss<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></rhyme></w></l>
							<l n="12" num="2.4" lm="7" met="7"><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="12.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4" punct="pt:7">cr<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>p<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
							<l n="13" num="2.5" lm="7" met="7"><w n="13.1" punct="vg:3">B<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>h<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3" punct="vg">en</seg>s</w>, <w n="13.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="13.3" punct="vg:5">t<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg">oi</seg>ts</w>, <w n="13.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="13.5" punct="vg:7">b<rhyme label="c" id="7" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>cs</rhyme></w>,</l>
							<l n="14" num="2.6" lm="7" met="7"><w n="14.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="354" place="2">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">aî</seg>n<rhyme label="d" id="8" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
							<l n="15" num="2.7" lm="7" met="7"><w n="15.1">M<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="15.2">v<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="15.3" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>nn<rhyme label="d" id="8" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
							<l n="16" num="2.8" lm="7" met="7"><w n="16.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="16.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="16.4">m<seg phoneme="wa" type="vs" value="1" rule="421" place="5">oi</seg>n<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>x</w> <w n="16.5">fr<rhyme label="c" id="7" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>cs</rhyme></w></l>
							<l n="17" num="2.9" lm="7" met="7"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="17.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>f</w> <w n="17.3">d</w>’<w n="17.4"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.5" punct="pe:7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<rhyme label="d" id="8" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></w> !</l>
						</lg>
						<lg n="3" type="quatrain" rhyme="abab">
							<l n="18" num="3.1" lm="7" met="7"><w n="18.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ts</w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3" punct="vg:4">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="18.4" punct="pe:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>v<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pe">é</seg>s</rhyme></w> !</l>
							<l n="19" num="3.2" lm="7" met="7"><w n="19.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="19.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>h<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
							<l n="20" num="3.3" lm="7" met="7"><w n="20.1" punct="vg:3">Tr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rs<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="20.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="20.3" punct="vg:7">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg>s</rhyme></w>,</l>
							<l n="21" num="3.4" lm="7" met="7"><w n="21.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">th<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="21.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="21.5" punct="pt:7">v<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
						</lg>
					</div></body></text></TEI>