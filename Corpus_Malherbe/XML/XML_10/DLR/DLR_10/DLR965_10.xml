<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">JACQUELINE</head><div type="poem" key="DLR965" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite de strophes" schema="3(abab) 2(abba)">
					<head type="number">XIII</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="10"></space><w n="1.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="2">e</seg>s</w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="1.7" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="2.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="2.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="2.7" punct="vg:9">n<seg phoneme="u" type="vs" value="1" rule="425" place="9" punct="vg">ou</seg>s</w>, <w n="2.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="10">en</seg></w> <w n="2.9" punct="vg:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="10"></space><w n="3.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="3.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="3.4">tr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="3.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="4.2">t</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="4.4" punct="vg:4">l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>t</w><caesura></caesura> <w n="4.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="4.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="4.8">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.9" punct="pi:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="8" met="8"><space unit="char" quantity="10"></space><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="5.6" punct="pt:8">t<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pt">oi</seg></rhyme></w>.</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="6.3" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg></w>,<caesura></caesura> <w n="6.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="6.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="6.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rni<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="7" num="2.3" lm="8" met="8"><space unit="char" quantity="10"></space><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="7.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="7.3" punct="ps:4">v<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="ps">u</seg>s</w>… <w n="7.4">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="7.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rni<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="1" mp="Lp">E</seg>s</w>-<w n="8.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="8.3" punct="vg:4">c<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="8.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d</w><caesura></caesura> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="8.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="8.8">s<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="8.9" punct="pi:12">fr<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pi">oi</seg>d</rhyme></w> ?</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><space unit="char" quantity="10"></space><w n="9.1">L</w>’<w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg></w> <w n="9.3" punct="vg:3">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ch<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3" punct="vg">ain</seg></w>, <w n="9.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.6">d<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="10.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="10.3" punct="pt:3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="pt">o</seg>rt</w>. <w n="10.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="10.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="10.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="10.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="9">en</seg></w> <w n="10.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="10.9" punct="pt:12">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="11" mp="M">oin</seg>t<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pt">ain</seg></rhyme></w>.</l>
						<l n="11" num="3.3" lm="8" met="8"><space unit="char" quantity="10"></space><w n="11.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="11.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>squ</w>’<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="11.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t</w> <w n="11.5">qu</w>’<w n="11.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="11.7">s</w>’<w n="11.8" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ccl<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="12.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="12.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d</w><caesura></caesura> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="12.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="12.8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="10">œu</seg>r</w> <w n="12.9" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>tt<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12" punct="pt">ein</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><space unit="char" quantity="10"></space><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="13.3" punct="pt:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pt">a</seg></w>. <w n="13.4">L</w>’<w n="13.5">h<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="14.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="14.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="14.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="14.6">r<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg><seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t</w> <w n="14.7" punct="pt:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>j<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>rs</rhyme></w>.</l>
						<l n="15" num="4.3" lm="8" met="8"><space unit="char" quantity="10"></space><w n="15.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="15.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="16.2" punct="dp:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="dp in">on</seg>s</w> : « <w n="16.3">J<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>cqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="16.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="10">en</seg>s</w> <w n="16.7">b<seg phoneme="o" type="vs" value="1" rule="315" place="11">eau</seg>x</w> <w n="16.8">j<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>rs</rhyme></w> »</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abba">
						<l n="17" num="5.1" lm="8" met="8"><space unit="char" quantity="10"></space><w n="17.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="17.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="17.3" punct="dp:6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="dp">a</seg></w> : <hi rend="ital"><w n="17.4" punct="pt:8">n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>gu<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w></hi>.</l>
						<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="18.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="18.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="18.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="18.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="18.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="18.7" punct="pt:9">d<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="pt" mp="F">e</seg></w>. <w n="18.8">C</w>’<w n="18.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="18.10" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="11" mp="M">ain</seg>s<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg></rhyme></w>.</l>
						<l n="19" num="5.3" lm="8" met="8"><space unit="char" quantity="10"></space><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="19.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="19.4" punct="pt:4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4" punct="pt">en</seg></w>. <w n="19.5" punct="vg:5">C<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>r</w>, <w n="19.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="19.7" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>ss<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></rhyme></w>,</l>
						<l n="20" num="5.4" lm="12" met="6+6"><w n="20.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="20.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="20.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="20.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="20.6">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>d</w> <w n="20.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="20.8">l</w>’<w n="20.9" punct="pt:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rni<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>