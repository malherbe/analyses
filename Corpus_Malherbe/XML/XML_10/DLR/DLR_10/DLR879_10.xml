<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">THE SKULL</head><div type="poem" key="DLR879" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="7(aa)">
					<head type="number">IX</head>
					<head type="main">CRANE</head>
					<lg n="1" type="distique" rhyme="aa">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w> <w n="1.3">m</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="1.6">y<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="2.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">cr<seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.5">s<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7" punct="vg:8">cr<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></w>,</l>
					</lg>
					<lg n="2" type="distique" rhyme="aa">
						<l n="3" num="2.1" lm="8" met="8"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="3.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="3.4">n<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<rhyme label="a" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
						<l n="4" num="2.2" lm="8" met="8"><w n="4.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.5">tr<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="4.6" punct="pt:8">v<rhyme label="a" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="3" type="distique" rhyme="aa">
						<l n="5" num="3.1" lm="8" met="8"><w n="5.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="5.2">p<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="5.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="5.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="5.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="5.6">f<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8">en</seg>t</rhyme></w></l>
						<l n="6" num="3.2" lm="8" met="8"><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="6.5" punct="pt:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pt">en</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="4" type="distique" rhyme="aa">
						<l n="7" num="4.1" lm="8" met="8"><w n="7.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="7.2" punct="vg:3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="7.3">n</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="7.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="7.6">d</w>’<w n="7.7" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="8" num="4.2" lm="8" met="8"><w n="8.1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="8.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>ts</w> <w n="8.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="8.5" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="5" type="distique" rhyme="aa">
						<l n="9" num="5.1" lm="8" met="8"><w n="9.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="9.2">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>q</w> <w n="9.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="3">en</seg>s</w> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="9.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="9.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="9.7" punct="vg:8">bi<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" punct="vg">en</seg></rhyme></w>,</l>
						<l n="10" num="5.2" lm="8" met="8"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="10.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="10.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="10.6" punct="pt:8">ri<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pt">en</seg></rhyme></w>.</l>
					</lg>
					<lg n="6" type="distique" rhyme="aa">
						<l n="11" num="6.1" lm="8" met="8"><w n="11.1" punct="vg:2">N<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="11.2" punct="vg:4">n<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="11.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.5" punct="dp:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<rhyme label="a" id="6" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
						<l n="12" num="6.2" lm="8" met="8"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>c</w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.4">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="12.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="12.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.7" punct="pi:8">t<rhyme label="a" id="6" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
					</lg>
					<lg n="7" type="distique" rhyme="aa">
						<l n="13" num="7.1" lm="8" met="8"><w n="13.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="13.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.6">v<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>rs</rhyme></w></l>
						<l n="14" num="7.2" lm="8" met="8"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="14.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>t</w> <w n="14.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="14.6">l</w>’<w n="14.7" punct="pt:8"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pt">e</seg>rs</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>