<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR720" modus="sp" lm_max="8" metProfile="4, 8" form="suite de strophes" schema="2[abab] 4[aa] 1[abba]">
				<head type="main">O Roll !</head>
				<lg n="1" type="regexp" rhyme="abababa">
					<l n="1" num="1.1" lm="4" met="4"><space unit="char" quantity="8"></space><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="1.2" punct="pe:2">R<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="pe">o</seg>ll</w> ! <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">O</seg></w> <w n="1.4" punct="pe:4">R<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="pe">o</seg>ll</rhyme></w> !</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.3">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.5" punct="vg:8">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rg<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="4" met="4"><space unit="char" quantity="8"></space><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="3.2" punct="pe:2">R<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="pe">o</seg>ll</w> ! <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">O</seg></w> <w n="3.4" punct="pe:4">R<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="pe">o</seg>ll</rhyme></w> !</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">V<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w>-<w n="4.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="4.3" punct="pi:4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pi">e</seg></w> ? <w n="4.4">V<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w>-<w n="4.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="4.6" punct="vg:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></w>,</l>
					<l n="5" num="1.5" lm="4" met="4"><space unit="char" quantity="8"></space><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="5.2" punct="pe:2">R<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="pe">o</seg>ll</w> ! <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">O</seg></w> <w n="5.4" punct="pe:4">R<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="pe">o</seg>ll</rhyme></w> !</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">Ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w>-<w n="6.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="6.3">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.5" punct="vg:8">p<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>rg<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
					<l n="7" num="1.7" lm="4" met="4"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="7.2" punct="pe:2">R<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="pe">o</seg>ll</w> ! <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">O</seg></w> <w n="7.4" punct="pi:4">R<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="pi ps">o</seg>ll</rhyme></w> ?…</l>
				</lg>
				<lg n="2" type="regexp" rhyme="baaaaaaaaa">
					<l n="8" num="2.1" lm="8" met="8"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="8.2" punct="vg:2">s<seg phoneme="o" type="vs" value="1" rule="318" place="2" punct="vg">au</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="8.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="8.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="8.6" punct="vg:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></w>,</l>
					<l n="9" num="2.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="9.2" punct="pe:2">R<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="pe">o</seg>ll</w> ! <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">O</seg></w> <w n="9.4" punct="pe:4">R<rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="pe">o</seg>ll</rhyme></w> !</l>
					<l n="10" num="2.3" lm="8" met="8"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="10.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">l</w>’<w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ssi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.7" punct="vg:8">b<rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>l</rhyme></w>,</l>
					<l n="11" num="2.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="11.2" punct="pe:2">R<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="pe">o</seg>ll</w> ! <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">O</seg></w> <w n="11.4" punct="pe:4">R<rhyme label="a" id="6" gender="m" type="a" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="pe">o</seg>ll</rhyme></w> !</l>
					<l n="12" num="2.5" lm="8" met="8"><w n="12.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="12.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="12.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="12.6">b<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<rhyme label="a" id="6" gender="m" type="e" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l</rhyme></w></l>
					<l n="13" num="2.6" lm="8" met="8"><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="13.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.4" punct="vg:8">r<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gn<rhyme label="a" id="7" gender="m" type="a" stanza="5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>l</rhyme></w>,</l>
					<l n="14" num="2.7" lm="8" met="8"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ttr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.3">b<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>scu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="14.5" punct="vg:8">v<rhyme label="a" id="7" gender="m" type="e" stanza="5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>l</rhyme></w>,</l>
					<l n="15" num="2.8" lm="8" met="8"><w n="15.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3" punct="vg:3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3" punct="vg">eau</seg></w>, <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="15.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="15.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.7" punct="vg:8">s<rhyme label="a" id="8" gender="m" type="a" stanza="6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>l</rhyme></w>,</l>
					<l n="16" num="2.9" lm="8" met="8"><w n="16.1">Fi<seg phoneme="ɛ" type="vs" value="1" rule="va-7" place="1">e</seg>r</w> <w n="16.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="16.4">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="4">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="16.5" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>gn<rhyme label="a" id="8" gender="m" type="e" stanza="6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>l</rhyme></w>,</l>
					<l n="17" num="2.10" lm="4" met="4"><space unit="char" quantity="8"></space><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="17.2" punct="pe:2">R<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="pe">o</seg>ll</w> ! <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">O</seg></w> <w n="17.4" punct="pe:4">R<rhyme label="a" id="9" gender="m" type="a" stanza="7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="pe">o</seg>ll</rhyme></w> !</l>
				</lg>
				<lg n="3" type="regexp" rhyme="bba">
					<l n="18" num="3.1" lm="8" met="8"><w n="18.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="18.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="18.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="18.4" punct="vg:4">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4" punct="vg">en</seg></w>, <w n="18.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="18.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="18.7">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7">en</seg></w> <w n="18.8">j</w>’<w n="18.9" punct="vg:8"><rhyme label="b" id="10" gender="m" type="a" stanza="7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8" punct="vg">ai</seg></rhyme></w>,</l>
					<l n="19" num="3.2" lm="8" met="8"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">AU</seg></w> <w n="19.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="19.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="19.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="19.5">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg></w> <w n="19.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="19.7" punct="pe:8">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rg<rhyme label="b" id="10" gender="m" type="e" stanza="7"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">er</seg></rhyme></w> !</l>
					<l n="20" num="3.3" lm="4" met="4"><space unit="char" quantity="8"></space><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="20.2" punct="pe:2">R<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="pe">o</seg>ll</w> ! <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">O</seg></w> <w n="20.4" punct="pe:4">R<rhyme label="a" id="9" gender="m" type="e" stanza="7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="pe">o</seg>ll</rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>