<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">DE LA VIE A LA MORT</head><div type="poem" key="DLR818" modus="sp" lm_max="8" metProfile="8, 4" form="suite périodique" schema="4(abba) 1(abab)">
					<head type="main">FINALE</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="1.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="1.3" punct="vg:5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg">é</seg></w>, <w n="1.4" punct="vg:8">b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rl<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>gu<rhyme label="a" id="10" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="2.1">Pl<seg phoneme="ø" type="vs" value="1" rule="405" place="1">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="2.3">l</w>’<w n="2.4" punct="vg:4"><rhyme label="b" id="1" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="3.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="3.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t</w> <w n="3.5">s<rhyme label="b" id="1" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="4.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="4.4" punct="vg:4">g<rhyme label="a" id="10" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" punct="vg">ai</seg></rhyme></w>,</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="5.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.4">l</w>’<w n="5.5">h<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<rhyme label="a" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="6.1">Ch<seg phoneme="e" type="vs" value="1" rule="347" place="1">ez</seg></w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="6.3">h<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<rhyme label="b" id="3" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s</rhyme></w></l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>tru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="7.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="6">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.5">m<rhyme label="b" id="3" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>s</rhyme></w></l>
						<l n="8" num="2.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2" punct="vg:4">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<rhyme label="a" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="9.2">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="9.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ds</w> <w n="9.6">cr<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</rhyme></w></l>
						<l n="10" num="3.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="10.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<rhyme label="b" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="11.3">d</w>’<w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="11.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>pr<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</rhyme></w></l>
						<l n="12" num="3.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="12.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="12.2">l</w>’<w n="12.3" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">im</seg>p<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>ss<rhyme label="b" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abba">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">S</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.3">d<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="13.4" punct="vg:5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg">an</seg>t</w>, <w n="13.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.7">m<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l</rhyme></w></l>
						<l n="14" num="4.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="14.2" punct="vg:2">s<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="14.4" punct="vg:4"><rhyme label="b" id="7" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="15.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="15.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="15.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<rhyme label="b" id="7" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
						<l n="16" num="4.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><rhyme label="a" id="6" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l</rhyme></w></l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abba">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="17.2">f<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="17.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="17.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="17.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w> <w n="17.6">s<rhyme label="a" id="8" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="18" num="5.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="18.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="18.4" punct="vg:4">j<rhyme label="b" id="9" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg></rhyme></w>,</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="19.2">n</w>’<w n="19.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="19.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="19.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="19.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="19.7">qu</w>’<w n="19.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="19.9">p<rhyme label="b" id="9" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></rhyme></w></l>
						<l n="20" num="5.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2" punct="ps:4">j<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<rhyme label="a" id="8" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="ps">e</seg></rhyme></w>…</l>
					</lg>
				</div></body></text></TEI>