<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE LONG DES JARDINS ET DE L’EAU</head><div type="poem" key="DLR257" modus="cp" lm_max="12" metProfile="8, 5+5, 6+6, (11)" form="suite périodique avec alternance de type 1" schema="2{2(abba) 1(abab)}">
					<head type="main">ENCORE LES BERGES</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" caesura="1">e</seg>c</w><caesura></caesura> <w n="1.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" mp="C">eu</seg>r</w> <w n="1.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</w> <w n="1.6">fr<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>ss<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D</w>’<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="354" place="1">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="2.5" punct="vg:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="3.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="3.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M">o</seg>rd<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="3.7">c<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>v<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">v<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="4.5">l</w>’<w n="4.6" punct="vg:8"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></w>,</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="11"><w n="5.1" punct="pe:2">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg></w> ! <w n="5.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="5.3">b<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="5.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" mp="M">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="7" mp="M">i</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9">en</seg>t</w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="5.6" punct="vg:11">n<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="415" place="11">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="6.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>x</w> <w n="6.7" punct="vg:8">s<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>rs</rhyme></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="7.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="7.3" punct="vg:4">l<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="7.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="7.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="7.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="9">ain</seg></w> <w n="7.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>sp<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r</rhyme></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">D</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="8.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.6" punct="vg:5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>t</w>, <w n="8.7">l</w>’<w n="8.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="8.9"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.10">l</w>’<w n="8.11" punct="pt:8"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="10" met="5+5"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="9.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" caesura="1">e</seg>c</w><caesura></caesura> <w n="9.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" mp="C">eu</seg>r</w> <w n="9.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</w> <w n="9.6" punct="vg:10">fr<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>ss<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">on</seg></rhyme></w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rm<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="10.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="10.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="10.6">b<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="11.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="11.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="11.5">m<seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.7">f<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg></w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="11.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="11.10" punct="vg:12">p<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>ss<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="12.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="12.4">d</w>’<w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="12.6">f<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="12.7">r<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abba">
						<l n="13" num="4.1" lm="10" met="5+5"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="13.2">S<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="13.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="13.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="5" caesura="1">i</seg>t</w><caesura></caesura> <w n="13.5">d</w>’<w n="13.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="13.7">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" mp="M">ai</seg>t<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="13.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</rhyme></w></l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="14.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="14.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="14.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="14.6" punct="pv:8">gu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>gu<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="15.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="15.3">l</w>’<w n="15.4"><seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="15.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.7" punct="vg:9">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="15.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="15.9">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>fl<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">î</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5" punct="pt:8">B<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>rt</rhyme></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abba">
						<l n="17" num="5.1" lm="10" met="5+5"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">c</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="17.4">l</w>’<w n="17.5"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" mp="M">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="5" caesura="1">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w><caesura></caesura> <w n="17.6">h<seg phoneme="y" type="vs" value="1" rule="453" place="6" mp="M">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="17.8">v<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>n<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="403" place="10">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">s<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="18.3">l</w>’<w n="18.4"><seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="18.6">l</w>’<w n="18.7" punct="vg:8"><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></w>,</l>
						<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="19.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="19.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="19.5" punct="vg:9">l<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="19.6">gu<seg phoneme="ø" type="vs" value="1" rule="405" place="10" mp="M">eu</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>t</w> <w n="19.7" punct="vg:12">h<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="12" punct="vg">au</seg>t</rhyme></w>,</l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="20.2">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg>gs</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="20.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="20.5">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="20.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="20.7" punct="pt:8">gu<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="10" met="5+5"><w n="21.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="21.2" punct="vg:3">m<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="21.3" punct="vg:5"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="21.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">em</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="21.5">l</w>’<w n="21.6" punct="pt:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="9" mp="M">en</seg>nu<rhyme label="a" id="11" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="10" punct="pt">i</seg></rhyme></w>.</l>
						<l n="22" num="6.2" lm="8" met="8">— <w n="22.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="22.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="22.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="22.4">v<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="22.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<rhyme label="b" id="12" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="23" num="6.3" lm="12" met="6+6"><w n="23.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="23.2">f<seg phoneme="œ" type="vs" value="1" rule="304" place="2" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="23.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="23.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="23.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="23.6">fr<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="23.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="23.9">nu<rhyme label="a" id="11" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>t</rhyme></w></l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="24.3">r<seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="24.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="24.6" punct="pt:8">g<rhyme label="b" id="12" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>