<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHEZ NOUS</head><div type="poem" key="DLR527" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique" schema="1(abab) 2(abba)">
					<head type="main">RIRE</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="1.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="1.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="1.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="1.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="1.9" punct="vg:12">s<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="2.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="2.4">b<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>vi<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="2.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.6" punct="pe:8">v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pe">oi</seg>x</rhyme></w> !</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="3.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="3" mp="C">o</seg>s</w> <w n="3.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ds</w> <w n="3.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="3.6" punct="vg:9">d<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="vg" mp="F">e</seg>s</w>, <w n="3.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="3.8">c<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">F<seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">au</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="4.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" mp="M">ê</seg>vi<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="4.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="4.6">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="4.8">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="4.9" punct="pe:12">b<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pe">oi</seg>s</rhyme></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="5.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="5.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="5.7">f<seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg></w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="5.9" punct="vg:12">v<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="6.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="6.3">gr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.5" punct="pt:8">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>t<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="pt">em</seg>ps</rhyme></w>.</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">n</w>’<w n="7.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M/mp">o</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M/mp">é</seg><seg phoneme="i" type="vs" value="1" rule="467" place="4">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="Fm">e</seg>s</w>-<w n="7.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="7.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="7.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>fr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10">ain</seg>s</w> <w n="7.8" punct="vg:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>t<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>ts</rhyme></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="8.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="8.3" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>rd</w>,<caesura></caesura> <w n="8.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="8.5">n<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>l</w> <w n="8.6" punct="vg:9">d<seg phoneme="u" type="vs" value="1" rule="425" place="9" punct="vg">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>t</w> <w n="8.8" punct="pi:12"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="12" met="6+6">— <w n="9.1" punct="vg:1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" punct="vg">an</seg>d</w>, <w n="9.2">l</w>’<w n="9.3">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>t</w> <w n="9.5">h<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.7">qu</w>’<w n="9.8"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l</w> <w n="9.9">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="9.10">b<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg></w> <w n="9.11">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>h<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rs</rhyme></w></l>
						<l n="10" num="3.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="10.3" punct="vg:5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg>s</w>, <w n="10.4" punct="vg:6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>s</w>, <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.6" punct="vg:8">f<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="11.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="11.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" caesura="1">oin</seg>s</w><caesura></caesura> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="11.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="11.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="11.9">n<seg phoneme="o" type="vs" value="1" rule="438" place="11" mp="C">o</seg>s</w> <w n="11.10"><rhyme label="b" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">d</w>’<w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" mp="M">in</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>cts</w> <w n="12.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>ds</w><caesura></caesura> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6">j<seg phoneme="wa" type="vs" value="1" rule="440" place="8" mp="M">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="12.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="12.8">n<seg phoneme="o" type="vs" value="1" rule="438" place="11" mp="C">o</seg>s</w> <w n="12.9" punct="pe:12">c<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pe ps">o</seg>rps</rhyme></w> !…</l>
					</lg>
				</div></body></text></TEI>