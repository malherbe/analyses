<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN PLEIN VENT</head><div type="poem" key="DLR18" modus="cm" lm_max="12" metProfile="6=6" form="suite de strophes" schema="1(abba) 1(abbacddcee)">
					<head type="main">TROISIÈME AUTOMNALE</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L</w>’<w n="1.2" punct="vg:3"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="1.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="1.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="1.6">s</w>’<w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="P">u</seg>r</w> <w n="1.9" punct="vg:12">n<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>s</rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg></w> <w n="2.3">l<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>th</w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="2.5">r<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="2.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="2.8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>fr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10">ain</seg></w> <w n="2.9">d</w>’<w n="2.10" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="343" place="11" mp="M">a</seg><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="3.3">d</w>’<w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="3.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="3.9">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11">en</seg>t</w> <w n="3.10" punct="vg:12">ti<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">R<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="M">u</seg>d<seg phoneme="wa" type="vs" value="1" rule="440" place="2" mp="M">o</seg>y<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="4.2">d</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="4.4">d</w>’<w n="4.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="4.7">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="4.9">ci<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>ls</w> <w n="4.10" punct="pt:12">f<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="2" type="dizain" rhyme="abbacddcee">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="5.2">f<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="5.3" punct="pv:4">ch<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="pv">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-29">e</seg>nt</w> ; <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="5.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="5.7">r<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="5.9">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="5.10" punct="pv:12">v<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>s</rhyme></w> ;</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nni<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg>s</w><caesura></caesura> <w n="6.4">r<seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>nt</w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="6.7" punct="pv:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pv">in</seg>s</rhyme></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="7.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="7.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>g</w><caesura></caesura> <w n="7.6" punct="ps:9">cr<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450" place="9" punct="ps">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="7.7" punct="pe:10"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pe">A</seg>h</w> ! <w n="7.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="7.9" punct="pe:12">m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="12" punct="pe">ain</seg>s</rhyme></w> !</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="8.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="8.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" caesura="1">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="8.8">b<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="9" num="2.5" lm="12" met="6+6"><w n="9.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="9.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="9.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="9.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="9.7" punct="pv:8"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="pv">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="9.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="9.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="9.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11" mp="M">en</seg>t<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r</rhyme></w></l>
						<l n="10" num="2.6" lm="12" met="6+6"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="10.3" punct="pv:4">pr<seg phoneme="ɛ" type="vs" value="1" rule="366" place="3">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="pv" mp="F">e</seg>nt</w> ; <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.5">fu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="10.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="10.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>r</w> <w n="10.8">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.9">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="10.10">n<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="426" place="12">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="11" num="2.7" lm="12" met="6+6"><w n="11.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" mp="P">e</seg>rs</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="Lc">à</seg></w>-<w n="11.3" punct="vg:3">b<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>s</w>, <w n="11.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" mp="P">e</seg>rs</w> <w n="11.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="11.6" punct="vg:6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" punct="vg" caesura="1">oin</seg></w>,<caesura></caesura> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="11.8">j<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="11.9">j<seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.10"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="11.11">j<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="426" place="12">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="12" num="2.8" lm="12" mp6="M" met="4+4+4"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="12.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="Lc">e</seg>lqu</w>’<w n="12.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" caesura="1">un</seg></w><caesura></caesura> <w n="12.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="12.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" caesura="2">ai</seg>t</w><caesura></caesura> <w n="12.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="12.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="12.8">s<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>p<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r</rhyme></w></l>
						<l n="13" num="2.9" lm="12" met="6+6"><w n="13.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="13.2" punct="vg:3">r<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3" punct="vg">eau</seg>x</w>, <w n="13.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="13.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="13.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="13.8">br<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.9"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>t</w> <w n="13.10">ti<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="14" num="2.10" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="14.3">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="14.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="14.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="14.9">d</w>’<w n="14.10" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="343" place="11" mp="M">a</seg><rhyme label="e" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe ps" mp="F">e</seg></rhyme></w> !…</l>
					</lg>
				</div></body></text></TEI>