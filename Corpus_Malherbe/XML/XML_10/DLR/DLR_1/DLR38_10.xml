<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PAROLES I</head><head type="main_subpart">TRILOGIE DE LA VIE ET DE LA MORT</head><div type="poem" key="DLR38" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 1" schema="abba abba ccd eed">
						<head type="number">III</head>
						<lg n="1" rhyme="abba">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="1" mp="M">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.4">tr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.5"><seg phoneme="u" type="vs" value="1" rule="426" place="9">ou</seg></w> <w n="1.6">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg></w> <w n="1.7" punct="vg:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">â</seg>t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L</w>’<w n="2.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="2.3">d</w>’<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="2.5">d<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="2.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>c</w><caesura></caesura> <w n="2.7">m</w>’<w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>tt<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.9" punct="vg:12">s<seg phoneme="y" type="vs" value="1" rule="445" place="10" mp="M">û</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="vg">en</seg>t</rhyme></w>,</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="3.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="4">en</seg>s</w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="3.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="3.7">pr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="3.8">d</w>’<w n="3.9"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12">en</seg>t</rhyme></w></l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="4.3">qu</w>’<w n="4.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="4.5">f<seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="4.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.8">m<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.9"><seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg></w> <w n="4.10">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="4.11" punct="pt:12">v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
						<lg n="2" rhyme="abba">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">F<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="5.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="5.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" caesura="1">un</seg></w><caesura></caesura> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="5.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="5.6">c<seg phoneme="o" type="vs" value="1" rule="415" place="9" mp="M">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="5.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="5.8" punct="vg:12">r<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="6.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="6.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>s</w> <w n="6.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="6.6">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="6.7" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="vg">en</seg>t</rhyme></w>,</l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>tr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</w> <w n="7.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="428" place="10" mp="M">ou</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12">en</seg>t</rhyme></w></l>
							<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="8.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="8.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rts</w> <w n="8.5"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="8.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="8.8">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="8.10">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>r</w> <w n="8.11" punct="pt:12">v<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
						<lg n="3" rhyme="ccd">
							<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">N</w>’<w n="9.2"><seg phoneme="e" type="vs" value="1" rule="354" place="1" mp="M/mp">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fp">e</seg></w>-<w n="9.3">t</w>-<w n="9.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" caesura="1">un</seg></w><caesura></caesura> <w n="9.6">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.7"><seg phoneme="u" type="vs" value="1" rule="426" place="9">où</seg></w> <w n="9.8">s</w>’<w n="9.9" punct="pi:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>pp<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pi">er</seg></rhyme></w> ?</l>
							<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">N<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="10.4" punct="pi:6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="M">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pi" caesura="1">er</seg></w> ?<caesura></caesura> <w n="10.5">n<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.7"><seg phoneme="u" type="vs" value="1" rule="426" place="10">où</seg></w> <w n="10.8">fr<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>pp<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></rhyme></w></l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="11.5">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="11.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="11.9" punct="pi:12">n<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
						</lg>
						<lg n="4" rhyme="eed">
							<l n="12" num="4.1" lm="12" met="6+6">… <w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">Ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="12.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="12.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="12.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="12.7">qu</w>’<w n="12.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="12.9" punct="pv:9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9" punct="pv">e</seg>st</w> ; <w n="12.10">n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="12.11">p<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>rqu<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="281" place="12">oi</seg></rhyme></w></l>
							<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>g<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="13.3">d</w>’<w n="13.4"><seg phoneme="e" type="vs" value="1" rule="354" place="4" mp="M">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="13.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="13.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="13.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="353" place="11" mp="M">e</seg>ffr<rhyme label="e" id="7" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></rhyme></w>,</l>
							<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">P<seg phoneme="ø" type="vs" value="1" rule="398" place="2" mp="Lc">eu</seg>t</w>-<w n="14.3" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="3" punct="vg">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="14.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="14.6">b<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>t</w><caesura></caesura> <w n="14.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="14.8" punct="ps:9">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" punct="ps">in</seg></w>… <w n="14.9" punct="vg:10">Cr<seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="vg">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="14.10"><seg phoneme="o" type="vs" value="1" rule="444" place="11">o</seg>h</w> <w n="14.11" punct="pe:12">cr<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe ps" mp="F">e</seg></rhyme></w> !…</l>
						</lg>
					</div></body></text></TEI>