<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN PLEIN VENT</head><div type="poem" key="DLR12" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="7((aa))">
					<head type="main">HEURE AUTOMNALE</head>
					<lg n="1" type="distiques" rhyme="aa…">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</w> <w n="1.7">m<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</w> <w n="1.8">l</w>’<w n="1.9"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="2.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="2.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="2.4">m<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="2.5" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="2.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="2.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="2.8">bru<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>t</w> <w n="2.9">m<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>t<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="3.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="3.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rt</w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="3.9">l</w>’<w n="3.10" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>g</rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">d</w>’<w n="4.3"><seg phoneme="u" type="vs" value="1" rule="426" place="2">où</seg></w> <w n="4.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="4.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="4.7">pr<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="4.8"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="4.9">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="4.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="M">e</seg>squ<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ls</w> <w n="4.11">s</w>’<w n="4.12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>d</rhyme></w></l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="5.2">cl<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="5.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="5.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="5.8">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="5.9" punct="pt:12">r<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="6.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="6.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="6.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="6.6">n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="6.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="11">è</seg>s</w> <w n="6.8">d<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>nt</w> <w n="7.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="7.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>ls</w><caesura></caesura> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="7.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="C">eu</seg>r</w> <w n="7.7">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t</w> <w n="7.8">ch<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</rhyme></w></l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="8.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="8.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="8.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="8.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="8.9" punct="vg:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>ch<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</rhyme></w>,</l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2" mp="C">y</seg></w> <w n="9.3">st<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="9.4">d</w>’<w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="5" mp="M">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="9.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="9.9" punct="pv:12">l<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>th<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rg<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="10.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="10.4" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="10.5">l</w>’<w n="10.6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="10.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="10.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="10.9">r<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>f<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>g<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="11.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="11.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="11.6">d</w>’<w n="11.7"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="11.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="11.9">fl<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</rhyme></w></l>
						<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="12.4">d<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>hl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="12.5" punct="vg:8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">im</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg" mp="F">e</seg></w>, <w n="12.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="11" mp="M">ain</seg>qu<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</rhyme></w></l>
						<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">D</w>’<w n="13.2" punct="vg:2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="13.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="13.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="13.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rs</w> <w n="13.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="13.8" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>s<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="14.2">r<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="14.3">qu</w>’<w n="14.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="14.5" punct="vg:6">b<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="14.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="14.9" punct="pt:12"><rhyme label="a" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>