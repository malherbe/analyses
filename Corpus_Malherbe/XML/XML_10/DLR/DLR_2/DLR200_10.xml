<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FUMÉES</head><div type="poem" key="DLR200" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="2(abba) 1(abab)">
					<head type="main">BÊTES</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="1.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="1.3">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="1.5">g<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>tti<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="354" place="1">E</seg>x<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="2.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="2.3" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>ch<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>ts</rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="3.3">h<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="3.5">ch<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ts</rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="4.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>ts</w> <w n="4.6" punct="pt:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>mi<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="5.3" punct="vg:3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>p</w>, <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="5.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d</w> <w n="5.6">d</w>’<w n="5.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="5.8" punct="vg:8">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg>t<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="vg">ain</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r</w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="6.4">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>q</w> <w n="6.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ct<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>b<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3" punct="vg:3">l<seg phoneme="y" type="vs" value="1" rule="453" place="3" punct="vg">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="7.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="7.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="7.7" punct="vg:8">j<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="8.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="8.6" punct="pt:8">b<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="9.3" punct="vg:3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3" punct="vg">oin</seg></w>, <w n="9.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="9.6">r<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>thm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>g<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>l</rhyme></w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="10.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="10.4">C<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="10.5" punct="vg:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>f<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1" punct="vg:1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>t</w>, <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="11.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ds</w> <w n="11.4" punct="vg:4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>ps</w>, <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="11.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="11.8">m<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="12.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>l</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>