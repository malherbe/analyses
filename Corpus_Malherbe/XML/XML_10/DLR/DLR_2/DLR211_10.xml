<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DÉCLARATIONS</head><div type="poem" key="DLR211" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="4(abba) 1(abab)">
					<head type="main">L’ÉCUME EST MORTE…</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>c<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="1.4" punct="pv:5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="pv" mp="F">e</seg></w> ; <w n="1.5">l</w>’<w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>lgu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="1.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="1.9">b<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="1.10">r<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11">an</seg></w> <w n="1.11">cl<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r</rhyme></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="pv:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">En</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="pv">é</seg></w> ; <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="2.3">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="2.5">pi<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="2.6">l<seg phoneme="y" type="vs" value="1" rule="453" place="10" mp="M">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>n<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="3.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="3.7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>r</w> <w n="3.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="3.9">v<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="3.10" punct="ps:12">cr<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg>s</rhyme></w>…</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="4.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="Fm">e</seg>s</w>-<w n="4.4" punct="vg:6">t<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg></w>,<caesura></caesura> <w n="4.5">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="4.8" punct="pi:12">M<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pi">e</seg>r</rhyme></w> ?</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">n</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="5.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="5.6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="5.7">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="5.9">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="5.10"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>p<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="6.2">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="6.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="6.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-27" place="9" mp="F">e</seg></w> <w n="6.6" punct="pv:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>t<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pv">an</seg>t</rhyme></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="7.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="7.8">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.9">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="7.10"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>tt<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>d</rhyme></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1" punct="vg:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>rs</w>, <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="8.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="8.4">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="8.6">m<seg phoneme="i" type="vs" value="1" rule="493" place="8" mp="M">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="8.8" punct="pv:12">p<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="415" place="12">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>s</rhyme></w> ;</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="9.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3">n</w>’<w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.7">cr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="9.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="9.9"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>d<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>s</rhyme></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="10.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>f</w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="10.5">v<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>t</w><caesura></caesura> <w n="10.6">s</w>’<w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>br<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="M">eu</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="10.8">d</w>’<w n="10.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>lt<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="11.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="11.3" punct="vg:3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3" punct="vg">œu</seg>r</w>, <w n="11.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="6" caesura="1">ue</seg>il</w><caesura></caesura> <w n="11.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="11.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="11.10" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="12.2">l<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="12.3">h<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>rs<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="12.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="12.7" punct="pv:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rd<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pv">on</seg>s</rhyme></w> ;</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abba">
						<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="13.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="13.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="13.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="13.8" punct="pv:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>s</rhyme></w> ;</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1" punct="vg:1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1" punct="vg">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="14.3" punct="vg:6">s<seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="14.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>g</w> <w n="14.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="14.7">fl<seg phoneme="o" type="vs" value="1" rule="438" place="10">o</seg>t</w> <w n="14.8" punct="vg:12">n<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>l</rhyme></w>,</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="15.3">t</w>’<w n="15.4"><seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="15.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="15.6">l</w>’<w n="15.7">h<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>c<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="15.8" punct="vg:12">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11" mp="M">en</seg>t<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>l</rhyme></w>,</l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="16.2" punct="pe:3">Sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe" mp="F">e</seg></w> ! <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="16.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="16.5">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="16.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="16.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>s</w> <w n="16.8" punct="pv:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>r<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>s</rhyme></w> ;</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>r</w>, <w n="17.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="17.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="17.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="17.5">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="17.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="17.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="17.8">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="17.9" punct="vg:12">d<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>s</rhyme></w>,</l>
						<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="18.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="18.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="18.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="18.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="18.6"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rn<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="19.2">j</w>’<w n="19.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="19.4">cr<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="19.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="19.6">n<seg phoneme="wa" type="vs" value="1" rule="440" place="5" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="19.7"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="19.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="19.9" punct="vg:12">f<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>s</rhyme></w>,</l>
						<l n="20" num="5.4" lm="12" met="6+6"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="20.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="20.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>bm<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rg<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="20.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="20.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="20.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="20.7">d<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="20.8" punct="pe:12">pr<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>n<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg>s</rhyme></w> !</l>
					</lg>
				</div></body></text></TEI>