<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIER ISLAM</head><div type="poem" key="DLR341" modus="cp" lm_max="12" metProfile="5+6, 6+6" form="suite périodique" schema="2(abab) 1(abba)">
					<head type="main">ERREMENT</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="11" met="5+6"><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="1" mp="M">A</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="1.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" caesura="1">em</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fc">e</seg></w> <w n="1.6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="1.7">d</w>’<w n="1.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>sph<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>d<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="11">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" mp="F">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="11" met="5+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>qu<seg phoneme="i" type="vs" value="1" rule="491" place="4" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg></w><caesura></caesura> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg></w> <w n="2.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="2.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="2.8" punct="vg:11"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>spr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="11" punct="vg">i</seg>t</rhyme></w>,</l>
						<l n="3" num="1.3" lm="11" met="5+6"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">r<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="3.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>g</w><caesura></caesura> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="3.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>r</w> <w n="3.8"><seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" mp="M">o</seg>rt<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" mp="F">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="11" met="5+6"><w n="4.1" punct="vg:1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg>t</w>, <w n="4.2">n<seg phoneme="y" type="vs" value="1" rule="457" place="2">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="4.4" punct="vg:5">s<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5" punct="vg" caesura="1">e</seg>il</w>,<caesura></caesura> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="4.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.7" punct="pt:11">n<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>qu<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="11" punct="pt">i</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="11" met="5+6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="5.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5" caesura="1">ain</seg>s</w><caesura></caesura> <w n="5.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="P">an</seg>s</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="5.7">v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" mp="F">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="11" met="5+6"><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.2">cr<seg phoneme="ø" type="vs" value="1" rule="403" place="3">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" caesura="1">o</seg>r</w><caesura></caesura> <w n="6.4">d</w>’<w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r</w> <w n="6.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>ç<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="6.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="6.8" punct="vg:11">di<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="11" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="7" num="2.3" lm="11" met="5+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="7.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5" caesura="1">oin</seg></w><caesura></caesura> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>x</w> <w n="7.7">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="7.9">ci<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>x</rhyme></w></l>
						<l n="8" num="2.4" lm="11" met="5+6"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="8.2">d</w>’<w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="8.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>r</w><caesura></caesura> <w n="8.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" mp="C">eu</seg>r</w> <w n="8.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="8.7" punct="pt:11"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>st<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="11">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="11" met="5+6"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="9.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4" punct="vg:5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="9.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="P">u</seg>r</w> <w n="9.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="9.8" punct="vg:11">b<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" punct="vg">o</seg>rd</rhyme></w>,</l>
						<l n="10" num="3.2" lm="11" met="5+6"><w n="10.1" punct="vg:3">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="10.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5" caesura="1">œu</seg>r</w><caesura></caesura> <w n="10.4">d</w>’<w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lgu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="10.6" punct="vg:11"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>ff<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="11">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="11.2" punct="vg:4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="11.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="11.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="11.5">d</w>’<w n="11.6" punct="vg:9"><seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="11.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="11.8">tr<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>s<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r</rhyme></w></l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="12.3">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.7">d</w>’<w n="12.8" punct="ps:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">O</seg>rph<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></rhyme></w>…</l>
					</lg>
				</div></body></text></TEI>