<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE IX</head><head type="main_part">Derniers Poèmes</head><head type="sub_part">INÉDITS</head><div type="poem" key="DLR1105" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique" schema="3(abab) 1(abba)">
					<head type="main">Distances</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>s</w> <w n="1.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="1.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="1.7">r<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="1.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r</rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="2.6" punct="pe:8">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="3.3" punct="vg:3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" punct="vg">em</seg>ps</w>, <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="3.5" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg" caesura="1">e</seg>t</w>,<caesura></caesura> <w n="3.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7" punct="vg:9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" punct="vg">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="3.9">m<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r</rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="4.2">qu</w>’<w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="4.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="4.6" punct="pe:8">n<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">L<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="5.4">j<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="M">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6" caesura="1">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="5.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="5.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="5.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="5.10">su<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>t</rhyme></w></l>
						<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="6.3" punct="pt:8">s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>s<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="7.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="7.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="7.8" punct="vg:12">nu<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="vg">i</seg>t</rhyme></w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">cl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="8.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5" punct="pt:8">pr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="9.2">d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" mp="P">è</seg>s</w> <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="9.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5" mp="M">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6" caesura="1">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="9.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="9.9">s<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ffr<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="10.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg></w> <w n="10.4">qu</w>’<w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="11.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M/mp">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fm">e</seg></w>-<w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="6" caesura="1">à</seg></w><caesura></caesura> <w n="11.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="11.5">qu</w>’<w n="11.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="11.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>v<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="12" num="3.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="12.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="12.4" punct="pt:8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1" punct="pe:3">N<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe" mp="F">e</seg></w> ! <w n="13.2" punct="pe:4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="pe">eu</seg></w> ! <w n="13.3">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="13.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" caesura="1">e</seg></w><caesura></caesura> <w n="13.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="13.6" punct="vg:8">s<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</w>, <w n="13.7">T<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="Fm">e</seg></w>-<w n="13.8">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="M/mc">i</seg>ss<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="14" num="4.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="14.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>sp<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="14.4" punct="vg:8">s<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rt</rhyme></w>,</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="15.2">qu</w>’<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="15.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="15.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="15.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="15.7" punct="vg:9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="15.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="15.9">d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>st<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="16" num="4.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="16.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="352" place="5">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="16.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="16.6" punct="pe:8">m<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pe">o</seg>rt</rhyme></w> !</l>
					</lg>
				</div></body></text></TEI>