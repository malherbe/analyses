<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRADUCTIONS <lb></lb>en vers français <lb></lb>par <lb></lb>LUCIE DELARUE-MARDRUS</head><head type="main_subpart">Poèmes de Anna Wickham</head><head type="sub_subpart">traduits en vers libres et réguliers</head><div type="poem" key="DLR1125" modus="cp" lm_max="14" metProfile="8, 4−6, 6−6, (11), (14)" form="suite de strophes" schema="1(aabcbcdd) 1(abab)">
						<head type="main">A l’Homme silencieux</head>
						<lg n="1" type="huitain" rhyme="aabcbcdd">
							<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3" mp="M">ai</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="1.4">n</w>’<w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="1.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></w> <w n="1.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="P">ou</seg>r</w> <w n="1.9" punct="vg:10">m<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="10" punct="vg">oi</seg></rhyme></w>,</l>
							<l n="2" num="1.2" lm="12" mp6="C" met="6−6"><w n="2.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="2.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="2.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" mp="C" caesura="1">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="2.6">d</w>’<w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="2.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="2.9" punct="pt:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>rt<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pt">oi</seg>s</rhyme></w>.</l>
							<l n="3" num="1.3" lm="12" mp6="C" met="6−6"><w n="3.1">S<seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="3.3" punct="vg:5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg" mp="F">e</seg></w>, <w n="3.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C" caesura="1">e</seg></w><caesura></caesura> <w n="3.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.6">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="3.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="3.8"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="3.9" punct="pt:12">s<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>l<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
							<l n="4" num="1.4" lm="11"><w n="4.1" punct="vg:3">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="P">a</seg>r</w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="4.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="6" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="4.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s</w> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="4.8" punct="vg:11">f<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="11" punct="vg">eu</seg>x</rhyme></w>,</l>
							<l n="5" num="1.5" lm="10" met="4−6" mp4="C"><w n="5.1" punct="pe:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="pe">ez</seg></w> ! <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C" caesura="1">ou</seg>s</w><caesura></caesura> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="5.5">pr<seg phoneme="i" type="vs" value="1" rule="469" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="5.7" punct="pt:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>st<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
							<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="6.5">d</w>’<w n="6.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></w>,</l>
							<l n="7" num="1.7" lm="8" met="8"><w n="7.1">Ch<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>ch<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ts</w> <w n="7.2"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="7.3">r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="7.4" punct="vg:8"><rhyme label="d" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
							<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="8.7" punct="pt:8">su<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						</lg>
						<lg n="2" type="quatrain" rhyme="abab">
							<l n="9" num="2.1" lm="10" met="4+6"><w n="9.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="9.2">l</w>’<w n="9.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="9.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="9.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.7" punct="vg:10">b<seg phoneme="o" type="vs" value="1" rule="315" place="9" mp="M">eau</seg>t<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></rhyme></w>,</l>
							<l n="10" num="2.2" lm="12" met="6+6"><w n="10.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="10.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="10.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></w> <w n="10.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="C">eu</seg>r</w> <w n="10.6" punct="vg:12">m<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>ss<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></rhyme></w>,</l>
							<l n="11" num="2.3" lm="14"><w n="11.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem/mp">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M/mp">i</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="4" mp="Lp">ez</seg></w>-<w n="11.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="11.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="11.7">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="11.9">m</w>’<w n="11.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="12">en</seg></w> <w n="11.11" punct="pi:14"><seg phoneme="a" type="vs" value="1" rule="340" place="13" mp="M">a</seg>ll<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="14" punct="pi">er</seg></rhyme></w> ?</l>
							<l n="12" num="2.4" lm="12" met="6+6"><w n="12.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="12.2" punct="vg:4">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="3" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" punct="vg">e</seg>t</w>, <w n="12.3" punct="pe:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pe" caesura="1">ez</seg></w> !<caesura></caesura> <w n="12.4">V<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w>-<w n="12.5">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="8" mp="Fm">e</seg></w> <w n="12.6">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="12.7"><seg phoneme="u" type="vs" value="1" rule="426" place="11">ou</seg></w> <w n="12.8" punct="pi:12">n<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pi">on</seg></rhyme></w> ?</l>
						</lg>
					</div></body></text></TEI>