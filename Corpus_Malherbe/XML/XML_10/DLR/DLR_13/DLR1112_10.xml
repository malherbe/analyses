<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE IX</head><head type="main_part">Derniers Poèmes</head><head type="sub_part">INÉDITS</head><div type="poem" key="DLR1112" modus="cp" lm_max="12" metProfile="8, 6−6" form="suite de strophes" schema="1(abab) 1(abba)">
					<head type="main">Vieillesse</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="1.2">d</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="1.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="1.7">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="9">en</seg>s</w> <w n="1.8"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="1.9" punct="vg:12">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg>b<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">C</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="2.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt</w> <w n="2.6" punct="pt:8">c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M/mp">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w>-<w n="3.4">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="5" mp="Fm">e</seg></w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.7" punct="vg:8">d<seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="vg">eu</seg>il</w>, <w n="3.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.9" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>p<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4" punct="pi:8">tr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w>-<w n="5.2">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="3" mp="Fm">e</seg></w> <w n="5.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="5.4">d</w>’<w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</w> <w n="5.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="5.7" punct="pi:11"><seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" punct="pi" mp="F">e</seg></w> ? <w n="5.8" punct="pt:12">N<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></rhyme></w>.</l>
						<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="6.6" punct="vg:8">t<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="7" num="2.3" lm="12" mp6="C" met="6−6"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="7.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3" punct="vg:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4" punct="vg">en</seg>s</w>, <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="P">a</seg>r</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C" caesura="1">a</seg></w><caesura></caesura> <w n="7.6" punct="vg:8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="7.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="7.8" punct="vg:12">c<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>ti<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="8.7" punct="pt:8">n<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="8" punct="pt">om</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>