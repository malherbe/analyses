<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR996" modus="sm" lm_max="8" metProfile="8" form="petite ballade" schema="3(ababbcbc) bcbc">
				<head type="main">BALLADE VILLON</head>
				<head type="sub_1">(EN HOMMAGE A CELUI DONT J’EMPRUNTAI LA FORME)</head>
				<lg n="1" type="huitain" rhyme="ababbcbc">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">O</seg>r</w> <w n="1.2" punct="vg:2">ç<seg phoneme="a" type="vs" value="1" rule="342" place="2" punct="vg">à</seg></w>, <w n="1.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ç<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="1.5" punct="vg:8">V<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="2.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">c<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">aî</seg>t</w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="2.7" punct="pe:8">t<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">N</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="1">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="3.4">v<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="3.6" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="227" place="7">o</seg>ign<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l</w> <w n="4.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t</w> <w n="4.4">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="4.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="4.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.8" punct="pt:8">t<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="5.2">n</w>’<w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="5.6">qu</w>’<w n="5.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<rhyme label="b" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="6.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="6.4">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="5">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="c" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg></rhyme></w></l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="7.3">qu</w>’<w n="7.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="7.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6" punct="pt:8">f<rhyme label="b" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1" punct="pi:2">V<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="pi">eu</seg>r</w> ? <w n="8.2">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="8.3" punct="vg:6">p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="8.4" punct="pe:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rt<rhyme label="c" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</rhyme></w> !</l>
				</lg>
				<lg n="2" type="huitain" rhyme="ababbcbc">
					<l n="9" num="2.1" lm="8" met="8"><w n="9.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="9.4">n</w>’<w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="9.6" punct="vg:8">b<seg phoneme="a" type="vs" value="1" rule="307" place="7">â</seg>ill<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></w>,</l>
					<l n="10" num="2.2" lm="8" met="8"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="10.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ch<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>t</w> <w n="10.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="10.5">l</w>’<w n="10.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="10.7">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.8" punct="pt:8">c<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="11" num="2.3" lm="8" met="8"><w n="11.1">R<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="11.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="11.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt</w> <w n="11.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6" punct="vg:8">h<seg phoneme="a" type="vs" value="1" rule="307" place="7">a</seg>ill<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></w>,</l>
					<l n="12" num="2.4" lm="8" met="8"><w n="12.1" punct="vg:2">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg">e</seg>s</w>, <w n="12.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="12.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="12.7" punct="pe:8">l<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="13" num="2.5" lm="8" met="8"><w n="13.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="307" place="1">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rd</w>, <w n="13.2" punct="vg:5">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="o" type="vs" value="1" rule="315" place="5" punct="vg">eau</seg></w>, <w n="13.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="13.4" punct="vg:8">v<rhyme label="b" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="14" num="2.6" lm="8" met="8"><w n="14.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2">an</seg>t</w> <w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="14.3">p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="c" id="8" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></w></l>
					<l n="15" num="2.7" lm="8" met="8"><w n="15.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="15.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="15.3">p<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="15.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.5">M<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rg<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>t</w> <w n="15.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.7" punct="vg:8">l<rhyme label="b" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="16" num="2.8" lm="8" met="8"><w n="16.1" punct="pi:2">V<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="pi">eu</seg>r</w> ? <w n="16.2">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="16.3" punct="vg:6">p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="16.4" punct="pe:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rt<rhyme label="c" id="8" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</rhyme></w> !</l>
				</lg>
				<lg n="3" type="huitain" rhyme="ababbcbc">
					<l n="17" num="3.1" lm="8" met="8"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="17.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="17.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="17.5" punct="vg:8">s<seg phoneme="u" type="vs" value="1" rule="428" place="7">ou</seg>ill<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></w>,</l>
					<l n="18" num="3.2" lm="8" met="8"><w n="18.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="18.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="18.4">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="18.5">t</w>’<w n="18.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rr<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="19" num="3.3" lm="8" met="8"><w n="19.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>rs</w> <w n="19.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>s</w> <w n="19.4">d</w>’<w n="19.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="19.6">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="19.7" punct="vg:8">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="7">a</seg>y<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></w>,</l>
					<l n="20" num="3.4" lm="8" met="8"><w n="20.1">F<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>t</w>-<w n="20.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="20.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="20.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="20.7">s<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="21" num="3.5" lm="8" met="8"><w n="21.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>r</w> <w n="21.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="21.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="21.5">s</w>’<w n="21.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<rhyme label="b" id="11" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="22" num="3.6" lm="8" met="8"><w n="22.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="22.3">l</w>’<w n="22.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-27" place="5">e</seg></w> <w n="22.5" punct="vg:8">h<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="c" id="12" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</rhyme></w>,</l>
					<l n="23" num="3.7" lm="8" met="8"><w n="23.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="23.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="23.3" punct="vg:3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" punct="vg">e</seg>rs</w>, <w n="23.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="23.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="23.7" punct="pi:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="b" id="11" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi ps">e</seg></rhyme></w> ?…</l>
					<l n="24" num="3.8" lm="8" met="8"><w n="24.1" punct="pi:2">V<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="pi">eu</seg>r</w> ? <w n="24.2">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="24.3" punct="vg:6">p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="24.4" punct="pe:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rt<rhyme label="c" id="12" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</rhyme></w> !</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="bcbc">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1" lm="8" met="8"><w n="25.1" punct="pe:1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">a</seg></w> ! <w n="25.2" punct="vg:2">B<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>s</w>, <w n="25.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="25.4" punct="vg:4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="25.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="25.6" punct="vg:6">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="25.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="25.8" punct="pe:8">g<rhyme label="b" id="13" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="26" num="4.2" lm="8" met="8"><w n="26.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="26.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="26.3">c<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>mm<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="26.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="26.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="26.6" punct="pe:8">t<rhyme label="c" id="14" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</rhyme></w> !</l>
					<l n="27" num="4.3" lm="8" met="8"><w n="27.1">C<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="27.3" punct="tc:4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="ti">on</seg></w> — <w n="27.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="27.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="27.6">p<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>n<rhyme label="b" id="13" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="28" num="4.4" lm="8" met="8">— <w n="28.1" punct="pi:2">V<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="pi">eu</seg>r</w> ? <w n="28.2">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="28.3" punct="vg:6">p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="28.4" punct="pe:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rt<rhyme label="c" id="14" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>