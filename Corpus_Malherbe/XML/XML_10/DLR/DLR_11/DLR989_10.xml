<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR989" modus="sm" lm_max="8" metProfile="8" form="ballade non classique" schema="3(ababbcbc) bcbc">
				<head type="main">BALLADE DES ÉCHECS</head>
				<lg n="1" type="huitain" rhyme="ababbcbc">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="1.2">l</w>’<w n="1.3" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qui<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="1.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="1.5" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>r</rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="2.2" punct="vg:3">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>nt</w>, <w n="2.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="2.5" punct="vg:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="3.2" punct="dp:3">p<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="dp">e</seg>s</w> : <w n="3.3">l</w>’<w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="3.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="3.7">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="3.8" punct="vg:8">n<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>r</rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L</w>’<w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="4.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="4.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="4.6">j<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="4.7" punct="vg:8">p<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1" punct="vg:1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" punct="vg">an</seg>d</w>, <w n="5.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>dr<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="5.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="5.4">h<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.5" punct="vg:8">t<rhyme label="b" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.5" punct="vg:5">r<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg">oi</seg></w>, <w n="6.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.7" punct="vg:8">f<rhyme label="c" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>t</rhyme></w>,</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="7.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="7.5" punct="vg:8">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rs<rhyme label="b" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.7" punct="pi:8">m<rhyme label="c" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pi">a</seg>t</rhyme></w> ?</l>
				</lg>
				<lg n="2" type="huitain" rhyme="ababbcbc">
					<l n="9" num="2.1" lm="8" met="8"><w n="9.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="9.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="9.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</rhyme></w></l>
					<l n="10" num="2.2" lm="8" met="8"><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="10.3" punct="vg:4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="10.4">v<seg phoneme="a" type="vs" value="1" rule="307" place="5">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.6" punct="pt:8">v<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="11" num="2.3" lm="8" met="8"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">r<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="11.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="11.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="11.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ff<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</rhyme></w>.</l>
					<l n="12" num="2.4" lm="8" met="8"><w n="12.1" punct="vg:1">F<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="12.2" punct="vg:3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="3" punct="vg">au</seg>x</w>, <w n="12.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.5" punct="vg:8">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="13" num="2.5" lm="8" met="8"><w n="13.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg>t</w> <w n="13.5">s</w>’<w n="13.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>g<rhyme label="b" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="14" num="2.6" lm="8" met="8"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="14.3">s</w>’<w n="14.4" punct="ps:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="ps">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">A</seg></w> <w n="14.6">di<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></w> <w n="14.7" punct="pe:8">v<rhyme label="c" id="8" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>t</rhyme></w> !</l>
					<l n="15" num="2.7" lm="8" met="8"><w n="15.1">L</w>’<w n="15.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="15.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d</w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="15.6">l</w>’<w n="15.7"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.8" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<rhyme label="b" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="16" num="2.8" lm="8" met="8"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="16.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.3">d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="16.4">l</w>’<w n="16.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="16.7" punct="pi:8">m<rhyme label="c" id="8" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pi">a</seg>t</rhyme></w> ?</l>
				</lg>
				<lg n="3" type="huitain" rhyme="ababbcbc">
					<l n="17" num="3.1" lm="8" met="8"><w n="17.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="17.2">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="17.3">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="17.5">l</w>’<w n="17.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>bb<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</rhyme></w>.</l>
					<l n="18" num="3.2" lm="8" met="8"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l</w> <w n="18.3">r<seg phoneme="y" type="vs" value="1" rule="457" place="4">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="18.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.6">f<seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="18.7" punct="vg:8">r<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="19" num="3.3" lm="8" met="8"><w n="19.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="19.2" punct="vg:3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" punct="vg">e</seg></w>, <w n="19.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="19.5">s</w>’<w n="19.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>r</rhyme></w>,</l>
					<l n="20" num="3.4" lm="8" met="8"><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="20.2" punct="vg:2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>r</w>, <w n="20.3">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="20.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="20.5" punct="vg:8">f<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="21" num="3.5" lm="8" met="8"><w n="21.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" punct="vg">en</seg>d</w>, <w n="21.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="21.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="21.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="21.5" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>tr<rhyme label="b" id="11" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="22" num="3.6" lm="8" met="8"><w n="22.1">L</w>’<w n="22.2"><seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>cc<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="22.3">d</w>’<w n="22.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="22.5" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="354" place="6">e</seg>x<seg phoneme="e" type="vs" value="1" rule="8" place="7">e</seg><rhyme label="c" id="12" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="9" place="8" punct="pt">a</seg>t</rhyme></w>.</l>
					<l n="23" num="3.7" lm="8" met="8">— <w n="23.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="23.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="23.3" punct="pe:4">r<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="pe ti">oi</seg></w> ! — <w n="23.4" punct="pe:5">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5" punct="pe">en</seg></w> ! <w n="23.5">Qu</w>’<w n="23.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="23.7">s</w>’<w n="23.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="23.9" punct="pe:8"><rhyme label="b" id="11" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="24" num="3.8" lm="8" met="8">… <w n="24.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="24.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="24.3">d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="24.4">l</w>’<w n="24.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="24.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="24.7" punct="pi:8">m<rhyme label="c" id="12" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pi">a</seg>t</rhyme></w> ?</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="bcbc">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1" lm="8" met="8"><w n="25.1">R<seg phoneme="ɛ" type="vs" value="1" rule="385" place="1">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="25.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="25.3">l</w>’<w n="25.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="5">ue</seg>il</w> <w n="25.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="25.6" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>f<rhyme label="b" id="13" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="26" num="4.2" lm="8" met="8"><w n="26.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="26.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="26.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="26.4" punct="vg:8">G<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="c" id="14" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>th</rhyme></w>,</l>
					<l n="27" num="4.3" lm="8" met="8"><w n="27.1">Cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>s</w> <w n="27.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3" punct="vg:4">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="27.4">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="5">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="27.5">c<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>n<rhyme label="b" id="13" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="28" num="4.4" lm="8" met="8"><w n="28.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="28.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="28.3">d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="28.4">l</w>’<w n="28.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="28.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="28.7" punct="pt:8">m<rhyme label="c" id="14" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>t</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>