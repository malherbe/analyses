<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES HORIZONTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Horizontales</title>
						<author>Henri Beauclair</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 — 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/horizont.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Horizontales</title>
								<author>Henri Beauclair</author>
								<edition>2e édition en partie originale</edition>
								<imprint>
									<publisher>Léon Vanier, Paris</publisher>
									<date when="1886">1886</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEA13" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="4(abbacddc)">
				<head type="number">VII</head>
				<head type="main">La Cueillette</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Allez, allez, ô jeunes filles, <lb></lb>
								Cueillir des bleuets dans les blés.
						</quote>
							<bibl>
								<name>V. HUGO</name>, <hi rend="ital">Orientales</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1" type="huitain" rhyme="abbacddc">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="1.3" punct="pv:4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pv">e</seg></w> ; <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>z</w> <w n="1.6">s</w>’<w n="1.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="2.2">l</w>’<w n="2.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="2.5">m<seg phoneme="œ" type="vs" value="1" rule="151" place="5">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="6">eu</seg>r</w> <w n="2.6">P<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>ss<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></rhyme></w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="3.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ç<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="4.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="5.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="5.3" punct="vg:5">tr<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>tt<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></w>, <w n="5.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6" punct="vg:8">s<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>r</rhyme></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rch<seg phoneme="ø" type="vs" value="1" rule="403" place="3">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="6.3" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>gu<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1" punct="vg:2">C<seg phoneme="œ" type="vs" value="1" rule="345" place="1">ue</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="7.2" punct="vg:4">c<seg phoneme="œ" type="vs" value="1" rule="345" place="3">ue</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">ez</seg></w>, <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="7.4" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="7">i</seg>ll<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>g</w> <w n="8.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="8.6" punct="pe:8">tr<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>tt<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pe">oi</seg>r</rhyme></w> !</l>
				</lg>
				<lg n="2" type="huitain" rhyme="abbacddc">
					<l n="9" num="2.1" lm="8" met="8"><w n="9.1" punct="pe:1">Ps<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="pe">i</seg>tt</w> ! <w n="9.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="9.3" punct="vg:4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="9.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="9.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="9.6">c<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="10" num="2.2" lm="8" met="8"><w n="10.1">Ch<seg phoneme="e" type="vs" value="1" rule="347" place="1">ez</seg></w> <w n="10.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="10.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="10.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="10.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="10.6" punct="pt:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>ç<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg></rhyme></w>.</l>
					<l n="11" num="2.3" lm="8" met="8"><w n="11.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="11.4" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>ss<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg></rhyme></w>,</l>
					<l n="12" num="2.4" lm="8" met="8"><w n="12.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="12.3" punct="vg:4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg></w>, <w n="12.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="12.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="12.6">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="12.7" punct="pe:8">h<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="13" num="2.5" lm="8" met="8"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w> <w n="13.2" punct="vg:2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>s</w>, <w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="13.4">g<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd</w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="13.6">l</w>’<w n="13.7"><seg phoneme="œ" type="vs" value="1" rule="286" place="7">œ</seg>il</w> <w n="13.8">n<rhyme label="c" id="7" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</rhyme></w></l>
					<l n="14" num="2.6" lm="8" met="8"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d</w> <w n="14.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="14.4">s<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="14.5" punct="pt:8">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="7">a</seg>y<rhyme label="d" id="8" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
					<l n="15" num="2.7" lm="8" met="8"><w n="15.1" punct="vg:2">C<seg phoneme="œ" type="vs" value="1" rule="345" place="1">ue</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="15.2" punct="vg:4">c<seg phoneme="œ" type="vs" value="1" rule="345" place="3">ue</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">ez</seg></w>, <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="15.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="7">i</seg>ll<rhyme label="d" id="8" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="16" num="2.8" lm="8" met="8"><w n="16.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="16.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>g</w> <w n="16.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="16.6" punct="pe:8">tr<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>tt<rhyme label="c" id="7" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pe">oi</seg>r</rhyme></w> !</l>
				</lg>
				<lg n="3" type="huitain" rhyme="abbacddc">
					<l n="17" num="3.1" lm="8" met="8"><w n="17.1">C</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="17.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="17.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>ps</w>-<w n="17.6">l<seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="17.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="17.8">R<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="18" num="3.2" lm="8" met="8"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="18.4">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="18.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="18.6" punct="pt:8">b<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>s</rhyme></w>.</l>
					<l n="19" num="3.3" lm="8" met="8">— «<w n="19.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="19.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="19.3" punct="vg:5">tr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tt<seg phoneme="ø" type="vs" value="1" rule="403" place="5" punct="vg">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="19.4" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>f<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</rhyme></w>,</l>
					<l n="20" num="3.4" lm="8" met="8"><w n="20.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w>-<w n="20.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="20.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>t</w> <w n="20.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="20.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="20.7" punct="pi:8">ch<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?»</l>
					<l n="21" num="3.5" lm="8" met="8">— <w n="21.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="21.2" punct="pe:4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="4" punct="pe">en</seg>t</w> ! <w n="21.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="21.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>c</w> <w n="21.5">t</w>’<w n="21.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<rhyme label="c" id="11" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="257" place="8" punct="pe">eoi</seg>r</rhyme></w> !</l>
					<l n="22" num="3.6" lm="8" met="8"><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="22.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="22.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="22.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="22.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="22.6" punct="pe:8">f<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>ill<rhyme label="d" id="12" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></w> !</l>
					<l n="23" num="3.7" lm="8" met="8"><w n="23.1" punct="vg:2">C<seg phoneme="œ" type="vs" value="1" rule="345" place="1">ue</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="23.2" punct="vg:4">c<seg phoneme="œ" type="vs" value="1" rule="345" place="3">ue</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">ez</seg></w>, <w n="23.3"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="23.4" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="7">i</seg>ll<rhyme label="d" id="12" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="24" num="3.8" lm="8" met="8"><w n="24.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="24.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="24.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="24.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>g</w> <w n="24.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="24.6" punct="pe:8">tr<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>tt<rhyme label="c" id="11" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pe">oi</seg>r</rhyme></w> !</l>
				</lg>
				<lg n="4" type="huitain" rhyme="abbacddc">
					<l n="25" num="4.1" lm="8" met="8"><w n="25.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="25.2">C</w>’<w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="25.4">l</w>’<w n="25.5">h<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="25.6"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="25.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="25.8">l</w>’<w n="25.9" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>tm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>sph<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="26" num="4.2" lm="8" met="8"><w n="26.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="26.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="26.3"><seg phoneme="e" type="vs" value="1" rule="353" place="4">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">aim</seg>s</w> <w n="26.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="26.5" punct="pt:8">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="b" id="16" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg>s</rhyme></w>.</l>
					<l n="27" num="4.3" lm="8" met="8"><w n="27.1">J<seg phoneme="ɑ̃" type="vs" value="1" rule="309" place="1">ean</seg></w> <w n="27.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="27.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="27.4" punct="dp:5">R<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="dp in">e</seg></w> : «<w n="27.5">S<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="27.6">j</w>’<w n="27.7" punct="pe:8"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<rhyme label="b" id="16" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pe">ai</seg>s</rhyme></w> !»</l>
					<l n="28" num="4.4" lm="8" met="8"><w n="28.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="28.2" punct="vg:2"><seg phoneme="o" type="vs" value="1" rule="444" place="2" punct="vg">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="28.4">R<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="28.5">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="28.6" punct="pe:8">f<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="29" num="4.5" lm="8" met="8"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="29.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="29.3" punct="vg:5">dr<seg phoneme="i" type="vs" value="1" rule="d-4" place="3">y</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="29.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="29.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="29.6">v<rhyme label="c" id="14" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</rhyme></w></l>
					<l n="30" num="4.6" lm="8" met="8"><w n="30.1">S<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="30.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="30.3" punct="vg:4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>s</w>, <w n="30.4" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="383" place="7">e</seg>ill<rhyme label="d" id="15" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
					<l n="31" num="4.7" lm="8" met="8"><w n="31.1" punct="vg:2">C<seg phoneme="œ" type="vs" value="1" rule="345" place="1">ue</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="31.2" punct="vg:4">c<seg phoneme="œ" type="vs" value="1" rule="345" place="3">ue</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">ez</seg></w>, <w n="31.3"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="31.4" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="7">i</seg>ll<rhyme label="d" id="15" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="32" num="4.8" lm="8" met="8"><w n="32.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="32.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="32.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="32.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>g</w> <w n="32.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="32.6" punct="pe:8">tr<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>tt<rhyme label="c" id="14" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pe">oi</seg>r</rhyme></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">Novembre 1883.</date>
						</dateline>
				</closer>
			</div></body></text></TEI>