<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La Nature et le Rêve</head><head type="main_subpart">La Mer de Bretagne</head><div type="poem" key="HER108" modus="cm" lm_max="12" metProfile="6−6" form="sonnet classique, prototype 2" schema="abba abba ccd ede">
						<head type="main">Mer montante</head>
						<lg n="1" rhyme="abba">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="1.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="1.5">ph<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.7">f<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="1.8">f<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="1.9"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="1.10" punct="pt:12">bl<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>cs</rhyme></w>.</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="2.2">R<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>z</w> <w n="2.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="Lc">u</seg>squ</w>’<w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="2.5">P<seg phoneme="e" type="vs" value="1" rule="HER108_1" place="5" mp="M">e</seg>nm<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rcʼh</w><caesura></caesura> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="2.7">c<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="2.9" punct="vg:12">f<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2" punct="vg:2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>ls</w>, <w n="3.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="3.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="3.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="3.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>br<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.8">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="C">eu</seg>r</w> <w n="3.9" punct="vg:12">pl<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="4.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="4.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" caesura="1">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="4.7" punct="pt:12">g<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="11" mp="M">ë</seg>l<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>ds</rhyme></w>.</l>
						</lg>
						<lg n="2" rhyme="abba">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w> <w n="5.4">l</w>’<w n="5.5" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" punct="vg">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>c</w><caesura></caesura> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="5.8">f<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="5.9" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>l<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="vg">an</seg>s</rhyme></w>,</l>
							<l n="6" num="2.2" lm="12" mp6="P" met="6−6"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="6.3">gl<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="6.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="P" caesura="1">ou</seg>s</w><caesura></caesura> <w n="6.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>r</w> <w n="6.6">cr<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.7">d</w>’<w n="6.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>c<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="7.3">t<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="7.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rd</w><caesura></caesura> <w n="7.5">s</w>’<w n="7.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rp<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="7.8">br<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">Em</seg>p<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>nt</w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="8.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" caesura="1">oin</seg></w><caesura></caesura> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="8.5">r<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>fs</w> <w n="8.6" punct="pt:12">ru<seg phoneme="i" type="vs" value="1" rule="491" place="10" mp="M">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>l<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>ts</rhyme></w>.</l>
						</lg>
						<lg n="3" rhyme="ccd">
							<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">j</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="9.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.7">fl<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>t</w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="9.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="9.10" punct="vg:12">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>s<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1" punct="vg:2">R<seg phoneme="ɛ" type="vs" value="1" rule="411" place="1">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg" mp="F">e</seg>s</w>, <w n="10.2" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>rs</w>, <w n="10.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>ts</w><caesura></caesura> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="10.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="10.6" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>s<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="11.2">qu</w>’<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="11.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="11.6">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="11.7">qu</w>’<w n="11.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="11.9">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</w> <w n="11.10" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pt">e</seg>r</rhyme></w>.</l>
						</lg>
						<lg n="4" rhyme="ede">
							<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg></w> <w n="12.3">m</w>’<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="12.6">d</w>’<w n="12.7"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="12.8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>x</w> <w n="12.9" punct="vg:12">fr<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rn<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="13.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="13.4">cl<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="13.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r</w> <w n="13.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="13.9">m<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12">e</seg>r</rhyme></w></l>
							<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="14.3">l</w>’<w n="14.4">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="14.6" punct="vg:6">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="14.7">v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7" mp="M">ai</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9">en</seg>t</w> <w n="14.8" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rn<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
					</div></body></text></TEI>