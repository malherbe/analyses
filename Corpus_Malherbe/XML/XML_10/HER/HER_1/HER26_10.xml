<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La Grèce et la Sicile</head><head type="main_subpart">Épigrammes bucoliques</head><div type="poem" key="HER26" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede">
						<head type="main">Les Bergers</head>
						<lg n="1" rhyme="abba">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="pt:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="pt">en</seg>s</w>. <w n="1.2">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" mp="M">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="1.4">s</w>’<w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="1.7">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="1.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="1.9" punct="pt:12">C<seg phoneme="i" type="vs" value="1" rule="493" place="11" mp="M">y</seg>ll<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="2.6" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.8">c</w>’<w n="2.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="2.10">l<seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="2.11">qu</w>’<w n="2.12"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>l</w> <w n="2.13">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="2.14">pl<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">aî</seg>t</rhyme></w></l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="3.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="3.5">l<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="3.6">d</w>’<w n="3.7">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="3.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="3.10">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>rp<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12">e</seg>t</rhyme></w></l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="4.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="4.6">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="4.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="4.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="4.10" punct="pt:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
						<lg n="2" rhyme="abba">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="5.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="5.4">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="5.5">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>c</w><caesura></caesura> <w n="5.6">m<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="5.8">br<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>s</w> <w n="5.9" punct="pt:12">pl<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">S<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" mp="Lp">ai</seg>s</w>-<w n="6.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="6.3">qu</w>’<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="6.6" punct="vg:6">m<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="6.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="6.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>gn<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>l<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="vg">e</seg>t</rhyme></w>,</l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="7.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3" mp="C">i</seg></w> <w n="7.3">d<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="7.5" punct="vg:10">fr<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="vg" mp="F">e</seg>s</w>, <w n="7.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="7.7" punct="pi:12">l<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pi">ai</seg>t</rhyme></w> ?</l>
							<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="8.2">N<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="2">ym</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="8.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="8.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="8.8" punct="pt:12">l<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
						<lg n="3" rhyme="ccd">
							<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="Lp">oi</seg>s</w>-<w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="9.3" punct="vg:5">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg" mp="F">e</seg></w>, <w n="9.4" punct="pe:6">P<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="pe" caesura="1">an</seg></w> !<caesura></caesura> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="9.6">Ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fm">e</seg></w>-<w n="9.7" punct="vg:10">pi<seg phoneme="e" type="vs" value="1" rule="241" place="10" punct="vg">e</seg>d</w>, <w n="9.8">g<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rdi<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12">en</seg></rhyme></w></l>
							<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="10.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="10.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="10.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="10.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="10.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">A</seg>rc<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="dc-1" place="11" mp="M">i</seg><rhyme label="c" id="5" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="vg">en</seg></rhyme></w>,</l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">t</w>’<w n="11.3" punct="ps:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" mp="M">in</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3" punct="ps">o</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>… <w n="11.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="C">I</seg>l</w> <w n="11.5" punct="pe:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="pe" caesura="1">en</seg>d</w> !<caesura></caesura> <w n="11.6">J</w>’<w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="11.8">v<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="11.9">tr<seg phoneme="e" type="vs" value="1" rule="353" place="9" mp="M">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="307" place="10" mp="M">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>r</w> <w n="11.10">l</w>’<w n="11.11" punct="pt:12"><rhyme label="d" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
						<lg n="4" rhyme="ede">
							<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="pt:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="pt">on</seg>s</w>. <w n="12.2">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="12.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>il</w> <w n="12.4">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="12.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="12.7" punct="pt:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="e" id="7" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></w>.</l>
							<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="13.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="13.4" punct="vg:4">p<seg phoneme="o" type="vs" value="1" rule="318" place="4" punct="vg">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="13.6">v<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t</w> <w n="13.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="13.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>l</w> <w n="13.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="13.10">m<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="14.2">d</w>’<w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="14.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="14.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">im</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="14.7">p<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r</w><caesura></caesura> <w n="14.8">l</w>’<w n="14.9"><seg phoneme="o" type="vs" value="1" rule="435" place="7" mp="M">o</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.10"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="14.11">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg>x</w> <w n="14.13">Di<rhyme label="e" id="7" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</rhyme></w></l>
						</lg>
					</div></body></text></TEI>