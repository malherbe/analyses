<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Le Moyen âge et la Renaissance</head><div type="poem" key="HER66" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede">
					<head type="main">Médaille</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">S<seg phoneme="ɛ" type="vs" value="1" rule="384" place="1" mp="M">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="1.3" punct="vg:6">R<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="1.4">V<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.6" punct="pt:12">P<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>st<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="12" punct="pt">à</seg></rhyme></w>.</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="2.2">pr<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="2.3">d</w>’<w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rvi<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="2.5" punct="vg:7">v<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>t</w>, <w n="2.6">s</w>’<w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.8"><seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg></w> <w n="2.9" punct="tc:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>c<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ti" mp="F">e</seg></rhyme></w> —</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="3.3">l<seg phoneme="y" type="vs" value="1" rule="d-3" place="3" mp="M">u</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="3.4">d</w>’<w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="3.6">d</w>’<w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="3.8">f<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.9" punct="vg:12">cr<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>sc<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.4"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="4.5">M<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>tt<seg phoneme="e" type="vs" value="1" rule="HER66_1" place="5" mp="M">e</seg><seg phoneme="o" type="vs" value="1" rule="444" place="6" caesura="1">o</seg></w><caesura></caesura> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w>’ <w n="4.7">P<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="4.8">l</w>’<w n="4.9" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>cr<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>st<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1" punct="vg">O</seg>r</w>, <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="5.5">t<seg phoneme="i" type="vs" value="1" rule="493" place="5" mp="M">y</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>s</w><caesura></caesura> <w n="5.6">qu</w>’<w n="5.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="5.8">p<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.9" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>st<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1" punct="vg:1">N<seg phoneme="y" type="vs" value="1" rule="450" place="1" punct="vg">u</seg>l</w>, <w n="6.2" punct="vg:3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="6.3" punct="vg:5">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rqu<seg phoneme="i" type="vs" value="1" rule="491" place="5" punct="vg">i</seg>s</w>, <w n="6.4" punct="vg:6">d<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>c</w>,<caesura></caesura> <w n="6.5">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.6"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></w> <w n="6.7" punct="vg:12">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>c<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">Qu</w>’<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="7.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="3">om</seg></w> <w n="7.5" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">E</seg>zz<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">in</seg></w>,<caesura></caesura> <w n="7.6" punct="vg:7">C<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" punct="vg">an</seg></w>, <w n="7.7" punct="vg:10">G<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>s</w>, <w n="7.8" punct="vg:12">H<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rc<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="8.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="8.5">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-7" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="8.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="8.8" punct="pt:12">M<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>st<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem/mp">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="2" mp="Lp">i</seg></w>-<w n="9.2" punct="vg:3">c<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="9.4" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5" mp="M">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="9.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.6">S<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>sm<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>d</w> <w n="9.7" punct="vg:12">P<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>d<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>lph<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">M<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="10.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g</w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="10.5">R<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="10.8">M<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="10.10">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="10.11" punct="vg:12">G<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>lf<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">â</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="11.3" punct="vg:5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg" mp="F">e</seg></w>, <w n="11.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="11.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="11.9" punct="pv:12">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>t<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pv">a</seg></rhyme></w> ;</l>
					</lg>
					<lg n="4" rhyme="ede">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="C">eu</seg>rs</w> <w n="12.3">f<seg phoneme="a" type="vs" value="1" rule="193" place="3">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="12.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="12.6">r<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="12.8" punct="vg:12">s<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>v<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="13.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="P">u</seg>r</w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="13.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="13.5">br<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="13.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="13.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="13.8" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">I</seg>s<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>tt<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg></rhyme></w>,</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="14.3">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>ph<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="14.4">f<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="14.6" punct="pt:12">pr<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>