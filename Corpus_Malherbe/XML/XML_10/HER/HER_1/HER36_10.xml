<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La Grèce et la Sicile</head><head type="main_subpart">Épigrammes bucoliques</head><div type="poem" key="HER36" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede">
						<head type="main">Le Coureur</head>
						<lg n="1" rhyme="abba">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="1.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>lph<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="1.4">l</w>’<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.6">v<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="1.7" punct="vg:7">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>d</w>, <w n="1.8">Th<seg phoneme="i" type="vs" value="1" rule="497" place="8" mp="M">y</seg>m<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>s</w> <w n="1.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="1.10" punct="vg:12">su<seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="M">i</seg>v<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</rhyme></w>,</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="2.2">v<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="P">a</seg>r</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="2.5">st<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="2.7">cl<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="2.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="2.10" punct="vg:12">f<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="3.2">L<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="3.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rt</w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.7">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.8">qu</w>’<w n="3.9"><seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>l</w> <w n="3.10">f<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D</w>’<w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="4.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="2">e</seg>d</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="4.5" punct="vg:5">br<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg" mp="F">e</seg></w>, <w n="4.6">sv<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>lt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="4.9">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>f</w> <w n="4.10">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="4.11">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="4.12" punct="pt:12">v<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="pt">en</seg>t</rhyme></w>.</l>
						</lg>
						<lg n="2" rhyme="abba">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="5.3" punct="vg:4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg></w>, <w n="5.4"><seg phoneme="œ" type="vs" value="1" rule="286" place="5">œ</seg>il</w> <w n="5.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>x<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.8">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="5.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</rhyme></w>,</l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="6.2">s<seg phoneme="y" type="vs" value="1" rule="d-3" place="3" mp="M">u</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="6.3">d</w>’<w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="6.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="6.7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="6.8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.9"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="6.10" punct="pv:12">c<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="7.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>thl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.7">j<seg phoneme="a" type="vs" value="1" rule="307" place="8" mp="M">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="7.8">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rs</w> <w n="7.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="7.10" punct="vg:12">m<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="8.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="8.4">sc<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>lpt<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.6" punct="vg:9">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" punct="vg">ai</seg>t</w>, <w n="8.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="8.8" punct="pt:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>t</rhyme></w>.</l>
						</lg>
						<lg n="3" rhyme="ccd">
							<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="9.2" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="9.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="C">i</seg>l</w> <w n="9.4">fr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="9.5">d</w>’<w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="9.9" punct="vg:12">fi<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="10.2">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c</w> <w n="10.3" punct="vg:5">h<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg" mp="F">e</seg></w>, <w n="10.4">l</w>’<w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>r</w><caesura></caesura> <w n="10.6">qu</w>’<w n="10.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="10.8">f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d</w> <w n="10.9">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.10"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="10.11">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="10.12">l<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="e" type="vs" value="1" rule="353" place="2" mp="M">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="11.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="11.5">s<seg phoneme="a" type="vs" value="1" rule="307" place="5" mp="M">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="11.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="11.7">m<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>scl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="11.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="11.9" punct="pv:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pv">a</seg>l</rhyme></w> ;</l>
						</lg>
						<lg n="4" rhyme="ede">
							<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>rr<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg></w><caesura></caesura> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="12.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.7">l</w>’<w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>tr<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
							<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="Lc">a</seg>r</w>-<w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem/mc">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="13.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="13.6">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="13.7" punct="vg:12">pi<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>st<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>l</rhyme></w>,</l>
							<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" mp="P">e</seg>rs</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="14.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="14.6">b<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>t</w><caesura></caesura> <w n="14.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="14.8">v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="14.9">fu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>r</w> <w n="14.10">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="14.11">l</w>’<w n="14.12" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
					</div></body></text></TEI>