<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CONTES</head><div type="poem" key="CHF32" modus="cp" lm_max="12" metProfile="8, 6+6, 4+6" form="suite de strophes" schema="2[aa] 1[aabba] 1[abab] 1[abba]">
					<head type="main">LA FIDÉLITÉ A TOUTE ÉPREUVE</head>
					<lg n="1" type="regexp" rhyme="aaaabbaaaabababba">
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="1.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">n<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="3">ym</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.4">l</w>’<w n="1.5" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="444" place="6">O</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1" punct="vg:2">L<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="2.2" punct="vg:4">fr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <hi rend="ital"><w n="2.4" punct="vg:8">c<seg phoneme="e" type="vs" value="1" rule="250" place="6">œ</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></w></hi>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="3.3">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="5" mp="M">ou</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.5">r<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.6">d</w>’<w n="3.7" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">I</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rt<rhyme label="a" id="2" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Cr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" mp="M">ai</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="4.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="4.4">cr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="4.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="4.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="4.7">ch<seg phoneme="e" type="vs" value="1" rule="347" place="11" mp="P">ez</seg></w> <w n="4.8" punct="pt:12"><rhyme label="a" id="2" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="5" num="1.5" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="5.1">F<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="5.3" punct="vg:4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>p<seg phoneme="o" type="vs" value="1" rule="438" place="4" punct="vg" caesura="1">o</seg>s</w>,<caesura></caesura> <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="5.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rqu<rhyme label="b" id="3" gender="m" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>s</rhyme></w></l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="6.2" punct="vg:4">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="vg">o</seg>rgn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="6.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="6.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="6.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="Lc">i</seg>s</w>-<w n="6.7"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="Lc">à</seg></w>-<w n="6.8" punct="pt:12">v<rhyme label="b" id="3" gender="m" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>s</rhyme></w>.</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1" punct="vg:2">Fou<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="7.2" punct="pe:4">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pe">er</seg></w> ! <w n="7.3">L</w>’<w n="7.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="7.5" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>rt</w>,<caesura></caesura> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="9">ain</seg></w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="7.9">cr<seg phoneme="y" type="vs" value="1" rule="454" place="11" mp="M">u</seg><rhyme label="a" id="2" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="8" num="1.8" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="8.2" punct="dp:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="dp in" caesura="1">er</seg></w> :<caesura></caesura> « <w n="8.3">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="8.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="8.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="9" mp="Lp">ain</seg></w>-<w n="8.7" punct="pi:10">l<rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="342" place="10" punct="pi">à</seg></rhyme></w> ?</l>
						<l n="9" num="1.9" lm="12" met="6+6">— <w n="9.1" punct="ps:1">Ch<seg phoneme="y" type="vs" value="1" rule="450" place="1" punct="ps">u</seg>t</w>… <w n="9.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="9.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="9.4">s</w>’<w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ccr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="9.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="9.8" punct="pt:12">f<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>lb<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg></rhyme></w>.</l>
						<l n="10" num="1.10" lm="12" met="6+6">— <w n="10.1" punct="vg:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">A</seg>h</w>, <w n="10.2" punct="pe:3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe" mp="F">e</seg></w> ! <w n="10.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="10.4" punct="pv:6">cr<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">î</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="pv" caesura="1">ai</seg></w> ;<caesura></caesura> <w n="10.5">j</w>’<w n="10.6"><seg phoneme="i" type="vs" value="1" rule="497" place="7">y</seg></w> <w n="10.7">su<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>s</w> <w n="10.8">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9" mp="Lc">è</seg>s</w>-<w n="10.9" punct="pt:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M/mc">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M/mc">o</seg>l<rhyme label="a" id="5" gender="f" type="a" stanza="4"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="11" num="1.11" lm="12" met="6+6">— <w n="11.1" punct="pe:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe ti" mp="F">e</seg></w> ! — <w n="11.2">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="11.3" punct="pe:6">h<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pe ti" caesura="1">eu</seg>r</w> !<caesura></caesura> — <w n="11.4">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8">en</seg>t</w> <w n="11.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="C">ou</seg>s</w> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="11.7" punct="pi:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<rhyme label="b" id="6" gender="m" type="a" stanza="4"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pi">ez</seg></rhyme></w> ?</l>
						<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="12.2" punct="pt:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="pt ti">on</seg>t</w>. — <w n="12.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="12.4" punct="pt:6">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pt ti" caesura="1">i</seg>r</w>.<caesura></caesura> — <w n="12.5">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="12.6" punct="ps:8">su<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="ps">i</seg>s</w>… <w n="12.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="12.8" punct="ps:10">su<seg phoneme="i" type="vs" value="1" rule="491" place="10" punct="ps">i</seg>s</w>… <w n="12.9" punct="pv:12">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="11" mp="M">ain</seg>c<rhyme label="a" id="5" gender="f" type="e" stanza="4"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="13.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="13.3" punct="vg:4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg">em</seg>ps</w>, <w n="13.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="13.5" punct="pv:6">f<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="pv" caesura="1">oi</seg></w> ;<caesura></caesura> <w n="13.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="13.7">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="13.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<rhyme label="b" id="6" gender="m" type="e" stanza="4"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg>s</rhyme></w>.</l>
						<l n="14" num="1.14" lm="12" met="6+6">— <w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="14.4">ch<seg phoneme="e" type="vs" value="1" rule="347" place="5" mp="P">ez</seg></w> <w n="14.5" punct="pv:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pv" caesura="1">ou</seg>s</w> ;<caesura></caesura> <w n="14.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="8">oi</seg></w> <w n="14.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="14.8" punct="pi:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>r<rhyme label="a" id="7" gender="f" type="a" stanza="5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg>s</rhyme></w> ?</l>
						<l n="15" num="1.15" lm="12" met="6+6">— <w n="15.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, <w n="15.2" punct="pt:3">m<seg phoneme="œ" type="vs" value="1" rule="151" place="2" mp="M">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="3" punct="pt ti">eu</seg>r</w>. — <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="P">En</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="15.5">r<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="15.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="15.8" punct="pi:12">p<rhyme label="b" id="8" gender="m" type="a" stanza="5"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="12" punct="pi">oin</seg>t</rhyme></w> ?</l>
						<l n="16" num="1.16" lm="12" met="6+6">— <w n="16.1">F<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="16.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="16.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="16.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="16.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="16.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="16.8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="190" place="11">e</seg>ts</w> <w n="16.9" punct="ps:12">p<rhyme label="b" id="8" gender="m" type="e" stanza="5"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="12" punct="ps">oin</seg>t</rhyme></w>…</l>
						<l n="17" num="1.17" lm="8" met="8"><space unit="char" quantity="8"></space>— <w n="17.1" punct="pe:1">Qu<seg phoneme="wa" type="vs" value="1" rule="281" place="1" punct="pe ti">oi</seg></w> ! — <w n="17.2">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="17.4" punct="pt:8">c<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>ss<rhyme label="a" id="7" gender="f" type="e" stanza="5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>