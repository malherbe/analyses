<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CHF71" modus="cm" lm_max="10" metProfile="4+6" form="strophe unique" schema="1(aabccbdeed)">
					<head type="main">ÉPIGRAMME CONTRE LAHARPE</head>
					<lg n="1" type="dizain" rhyme="aabccbdeed">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="1.3" punct="vg:4">L<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>h<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>rp<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="1.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="C">i</seg>l</w> <w n="1.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="1.6">si<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="1.7" punct="vg:10">p<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>s</rhyme></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="2.2" punct="vg:4">G<seg phoneme="a" type="vs" value="1" rule="307" place="3" mp="M">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>rd</w>,<caesura></caesura> <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="2.5">f<seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>il</w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="2.7" punct="pt:10">br<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>s</rhyme></w>.</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="3.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="3.4" punct="pv:4">f<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">â</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="pv" caesura="1">é</seg></w> ;<caesura></caesura> <w n="3.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="3.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" mp="M">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="3.8" punct="pt:10">f<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="4" num="1.4" lm="10" met="4+6">— <w n="4.1" punct="pe:1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="pe">ai</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">E</seg>t</w> <w n="4.3" punct="pi:4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="4" punct="pi ti" caesura="1">en</seg>t</w> ?<caesura></caesura> — <w n="4.4">C<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="4.5">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</w> <w n="4.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>g<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg>s</rhyme></w></l>
						<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="5.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="5.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="5.6">d</w>’<w n="5.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>g<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg>s</rhyme></w>,</l>
						<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">C</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="6.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" caesura="1">un</seg></w><caesura></caesura> <w n="6.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="6.6">m<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="6.7" punct="pe:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>c<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></w> !</l>
						<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="7.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>rt</w><caesura></caesura> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="7.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="7.7" punct="pt:9">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" punct="pt ti">on</seg>s</w>. — <w n="7.8" punct="pv:10">Ou<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="10" punct="pv">i</seg></rhyme></w> ;</l>
						<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" mp="P">è</seg>s</w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" caesura="1">o</seg>rd</w><caesura></caesura> <w n="8.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="8.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="8.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>fr<rhyme label="e" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="9" num="1.9" lm="10" met="4+6"><w n="9.1">V<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="9.3">l</w>’<w n="9.4" punct="vg:4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="4" punct="vg" caesura="1">ue</seg>il</w>,<caesura></caesura> <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.6" punct="vg:6">fi<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg">e</seg>l</w>, <w n="9.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.8" punct="dp:10">c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ct<rhyme label="e" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></w> :</l>
						<l n="10" num="1.10" lm="10" met="4+6"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="10.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="10.3" punct="pv:4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pv" caesura="1">eu</seg>l</w> ;<caesura></caesura> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="10.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="10.8" punct="pt:10">lu<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="10" punct="pt">i</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>