<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES HUMBLES</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1160 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES HUMBLES</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeeleshumbles.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 1</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP90" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="4(abab)">
				<head type="main">Dans la rue</head>
				<opener>
					<salute>A Jules Bonnassies</salute>
				</opener>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="1.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="1.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="1.6" punct="pv:8">d<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pv">eu</seg>il</rhyme></w> ;</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="2.4" punct="tc:5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="ti">e</seg></w> — <w n="2.5">c</w>’<w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.8" punct="tc:8">m<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ti">e</seg></rhyme></w> —</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="3.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="3.3">l</w>’<w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.5">j<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>squ</w>’<w n="3.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="3.7">s<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>il</rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.6" punct="pt:8">pr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.2" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="5.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.5" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>ni<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="7.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>p</w> <w n="7.5">d</w>’<w n="7.6"><seg phoneme="œ" type="vs" value="1" rule="286" place="5">œ</seg>il</w> <w n="7.7"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="7.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rni<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></rhyme></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="8.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="8.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>hi<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="8.4">d</w>’<w n="8.5" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="9.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">c</w>’<w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="9.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg></w> <w n="9.7">fr<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>d</rhyme></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="10.4">g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="10.7" punct="vg:8">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>g<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="11.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t</w> <w n="11.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.6">l</w>’<w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="11.8">s<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</rhyme></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t</w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="12.6">l</w>’<w n="12.7" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.3">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="13.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="13.5">n<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</rhyme></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="14.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="14.4">s</w>’<w n="14.5" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="6">em</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>fl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">L</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rs</w> <w n="15.4">t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="15.6" punct="vg:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ch<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>r</rhyme></w>,</l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="16.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d</w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">n<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="16.6">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="16.7" punct="dp:7">d<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="dp in">i</seg>t</w> : « <w n="16.8" punct="pt:8">S<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>. »</l>
				</lg>
			</div></body></text></TEI>