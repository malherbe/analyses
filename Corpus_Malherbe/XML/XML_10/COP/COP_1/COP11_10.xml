<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PROMENADES ET INTÉRIEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1057 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">COP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PROMENADES ET INTÉRIEURS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ebooksgratuits.com</publisher>
						<idno type="URL">http://www.ebooksgratuits.com/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Promenades et intérieurs</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Lemerre</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La date de création est celle donnée par l’Académie Française pour le recueil "Les Humbles"</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><div type="poem" key="COP11" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="10((aa))">
					<head type="main">La mémoire</head>
					<lg n="1" type="distiques" rhyme="aa…">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:2">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2" punct="vg">en</seg>t</w>, <w n="1.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="1.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="1.7" punct="vg:9">y<seg phoneme="ø" type="vs" value="1" rule="398" place="9" punct="vg">eu</seg>x</w>, <w n="1.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="1.9" punct="vg:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>d<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="2.2">m</w>’<w n="2.3" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg">aî</seg>t</w>, <w n="2.4">sv<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>lt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="2.7">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.8" punct="vg:12">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>t<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="3.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="3.3">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>ds</w> <w n="3.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="3.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="3.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rts</w> <w n="3.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="3.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="3.9" punct="pt:12">fr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg>t</rhyme></w>.</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Tr<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M/mp">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem/mp">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w>-<w n="4.2">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="4" mp="Fm">e</seg></w> <w n="4.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="4.5">m<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>ts</w> <w n="4.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="4.8" punct="vg:12">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="11" mp="M">ein</seg>dr<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg>t</rhyme></w>,</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="5.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="5.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="5.6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="10">oi</seg></w> <w n="5.7">j</w>’<w n="5.8"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="11">ai</seg></w> <w n="5.9" punct="pi:12">fu<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="6.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="6.6">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" caesura="1">ein</seg>t</w><caesura></caesura> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="6.8">r<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.9"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>s</w> <w n="6.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="6.11" punct="pi:12">plu<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1" mp="Lp">eu</seg>t</w>-<w n="7.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="7.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" caesura="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="7.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="7.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="7.8">b<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg></rhyme></w></l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="354" place="2" mp="M">e</seg>x<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="8.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="8.5">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r</w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="8.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="8.8" punct="pi:12">j<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pi">i</seg></rhyme></w> ?</l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1" mp="Lp">E</seg>st</w>-<w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="9.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="9.4">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="Lc">en</seg>tr</w>’<w n="9.6"><seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M/mc">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="9.8">r<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">in</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="10.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="10.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="11">en</seg>t</w> <w n="10.7" punct="pi:12">gr<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
						<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="11.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="11.5" punct="vg:6">p<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>rs</w>,<caesura></caesura> <w n="11.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="11.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="11.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="11.10" punct="vg:12">ci<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">Au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="12.2">n</w>’<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6">p<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</w> <w n="12.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="12.9" punct="pv:12">y<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pv">eu</seg>x</rhyme></w> ;</l>
						<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="13.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="13.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="13.7">t<seg phoneme="a" type="vs" value="1" rule="307" place="9" mp="M">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</w> <w n="13.8">h<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>m<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1">N</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="14.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="14.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>g</w> <w n="14.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rd</w><caesura></caesura> <w n="14.7">l<seg phoneme="y" type="vs" value="1" rule="453" place="7" mp="M">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="14.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="14.9" punct="pt:12">t<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>m<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="15.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="15.5">gr<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="15.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="15.8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.9" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>nn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="11" mp="M">o</seg>c<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="vg">en</seg>t</rhyme></w>,</l>
						<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="16.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="16.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="16.4">v<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>t</w><caesura></caesura> <w n="16.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="16.7" punct="pv:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" mp="M">im</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="M">i</seg>ss<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pv">an</seg>t</rhyme></w> ;</l>
						<l n="17" num="1.17" lm="12" met="6+6"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="17.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>t</w> <w n="17.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="17.6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" caesura="1">oin</seg>s</w><caesura></caesura> <w n="17.7">s</w>’<w n="17.8" punct="dp:9"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="9" punct="dp in">er</seg></w> : « <w n="17.9">S<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s</w> <w n="17.10" punct="vg:12">b<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>n<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="18" num="1.18" lm="12" met="6+6"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="18.2">f<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>lt<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="18.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="6" caesura="1">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="18.5">l</w>’<w n="18.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>l</w> <w n="18.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="18.8" punct="vg:12">g<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>n<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="19" num="1.19" lm="12" met="6+6"><w n="19.1" punct="vg:3">M<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="19.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="19.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="19.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>ds</w><caesura></caesura> <w n="19.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="19.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="19.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="19.9" punct="vg:12">v<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>x</rhyme></w>,</l>
						<l n="20" num="1.20" lm="12" met="6+6"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="20.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="20.4">qu</w>’<w n="20.5"><seg phoneme="e" type="vs" value="1" rule="354" place="4" mp="M">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="20.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg></w> <w n="20.7">d</w>’<w n="20.8" punct="vg:9"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="20.9">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="20.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="20.11" punct="pe:12">v<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pe">oi</seg>s</rhyme></w> ! »</l>
					</lg>
				</div></body></text></TEI>