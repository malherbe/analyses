<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PROMENADES ET INTÉRIEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1057 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">COP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PROMENADES ET INTÉRIEURS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ebooksgratuits.com</publisher>
						<idno type="URL">http://www.ebooksgratuits.com/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Promenades et intérieurs</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Lemerre</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La date de création est celle donnée par l’Académie Française pour le recueil "Les Humbles"</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><div type="poem" key="COP31" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 1" schema="abba abba ccd eed">
					<head type="main">Nostalgie parisienne</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="1.2">Su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>xp<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="1.5">tr<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="1.7" punct="vg:12">g<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg></w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="2.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>lp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="2.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ls</w> <w n="2.8" punct="pv:12">h<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pv">e</seg>rs</rhyme></w> ;</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="3.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rs</w><caesura></caesura> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="3.6">pr<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="3.8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>rs</w> <w n="3.9" punct="vg:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" punct="vg">e</seg>rts</rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="4.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="4.5" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="4.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="4.7" punct="vg:8">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="vg">oin</seg></w>, <w n="4.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="4.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="4.10" punct="pt:12">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>t<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="5.2" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>s</w>, <w n="5.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="5.4">fu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="5.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="5.9" punct="vg:12">b<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="6.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="6.4" punct="vg:4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg">in</seg></w>, <w n="6.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="6.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="6.8">S<seg phoneme="ɛ" type="vs" value="1" rule="385" place="9">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.9"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg>x</w> <w n="6.10">fl<seg phoneme="o" type="vs" value="1" rule="438" place="11">o</seg>ts</w> <w n="6.11" punct="pt:12">v<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" punct="pt">e</seg>rts</rhyme></w>.</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">c</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="7.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="7.7">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="7.8">d</w>’<w n="7.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="7.10">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="7.11">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="7.12" punct="vg:12"><seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12" punct="vg">e</seg>rts</rhyme></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l</w> <w n="8.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="8.5">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="6" caesura="1">y</seg>s</w><caesura></caesura> <w n="8.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.7" punct="vg:8">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="vg">en</seg>d</w>, <w n="8.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="8.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="8.10" punct="pt:12">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">am</seg>p<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="9.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="9.5">P<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="9.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>s</w> <w n="9.9">m<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="12">oin</seg>s</rhyme></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="10.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="10.5" punct="vg:7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" punct="vg">e</seg></w>, <w n="10.6" punct="vg:8">t<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></w>, <w n="10.7">l</w>’<w n="10.8"><seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="10.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="10.10" punct="pt:12">f<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="12" punct="pt">oin</seg>s</rhyme></w>.</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="11.2" punct="vg:6">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M">o</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>lg<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg in" caesura="1">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> – <w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="11.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t</w> <w n="11.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="11.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="11.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="11.8" punct="vg:12">s<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg in" mp="F">e</seg>s</rhyme></w>, –</l>
					</lg>
					<lg n="4" rhyme="eed">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="12.2" punct="vg:2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2" punct="vg">œu</seg>r</w>, <w n="12.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="12.5" punct="vg:6">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6" punct="vg" caesura="1">en</seg></w>,<caesura></caesura> <w n="12.6">f<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="12.8" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>s<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>ni<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></rhyme></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="13.3" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="354" place="3" mp="M">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>l</w>, <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="13.5">l</w>’<w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>r</w><caesura></caesura> <w n="13.7">str<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</w> <w n="13.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="13.9">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="11" mp="M">ai</seg>ni<rhyme label="e" id="7" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></rhyme></w></l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="14.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="14.5">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="14.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="14.7">qu</w>’<w n="14.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="14.9">R<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>z</w> <w n="14.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="14.11" punct="pt:12">V<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>