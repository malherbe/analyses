<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PROMENADES ET INTÉRIEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1057 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">COP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PROMENADES ET INTÉRIEURS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ebooksgratuits.com</publisher>
						<idno type="URL">http://www.ebooksgratuits.com/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Promenades et intérieurs</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Lemerre</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La date de création est celle donnée par l’Académie Française pour le recueil "Les Humbles"</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><div type="poem" key="COP19" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="5(abab)">
					<head type="main">Juin</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">v<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="1.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="1.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="1.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.7">s<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="2.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>ps</w> <w n="2.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="2.6">t<seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg>t</w> <w n="2.7" punct="vg:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L</w>’<w n="3.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>ct</w> <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="3.4"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="3.7">h<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="4.6" punct="pv:8">n<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg>d</rhyme></w> ;</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">d</w>’<w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="5.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="307" place="5">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.7"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="5.8">d</w>’<w n="5.9"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rg<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2">v<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="6.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4" punct="vg:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>stru<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="6.6" punct="vg:8">j<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></w>,</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="7.2">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="2">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3" punct="vg:4">t<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>t</w>, <w n="7.4">ch<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>d</w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="7.6" punct="vg:8">fr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">f<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="8.6">l</w>’<w n="8.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="9.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="9.4">d</w>’<w n="9.5"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.6">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="9.7">d</w>’<w n="9.8"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">È</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="10.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6">en</seg>t</w> <w n="10.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ch<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></w></l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="11.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="11.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>x</w> <w n="11.6">r<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">D</w>’<w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="12.3">b<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="12.6" punct="pt:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ch<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.3">j<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.6" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="14.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="14.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="14.4">n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d</w> <w n="14.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.6" punct="pv:8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>ge<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="301" place="8" punct="pv">ai</seg>s</rhyme></w> ;</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="15.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="15.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="15.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="15.5">d</w>’<w n="15.6"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>t</w> <w n="16.2">d</w>’<w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="16.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="16.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="16.6" punct="pv:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>j<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pv">e</seg>ts</rhyme></w> ;</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="17.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="17.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="17.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2" punct="vg:2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>s</w>, <w n="18.3">tr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="18.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="18.7" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rb<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="19.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="19.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>rs</w> <w n="19.4">br<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="19.6">t<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="20.3"><seg phoneme="ø" type="vs" value="1" rule="247" place="4">œu</seg>fs</w> <w n="20.4">d</w>’<w n="20.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="20.6">n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d</w> <w n="20.7" punct="pt:8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>b<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>