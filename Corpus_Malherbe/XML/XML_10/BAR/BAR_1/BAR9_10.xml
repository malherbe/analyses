<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR9" modus="cp" lm_max="10" metProfile="4+6, (8)" form="suite périodique" schema="3(ababcdcd)">
				<head type="main">Les Nénuphars</head>
				<opener>
					<salute>À la baronne de B…</salute>
				</opener>
				<lg n="1" type="huitain" rhyme="ababcdcd">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">N<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>ph<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rs</w> <w n="1.2" punct="vg:4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>cs</w>,<caesura></caesura> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="1.4">l<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>s</w> <w n="1.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg>x</w> <w n="1.7" punct="vg:10">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9" mp="M">im</seg>p<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">N<seg phoneme="ɛ" type="vs" value="1" rule="384" place="1">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="2.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="2.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>r</w> <w n="2.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>z<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="10" punct="vg">u</seg>r</rhyme></w>,</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="3.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="383" place="3" mp="M">e</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="3.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="6" mp="C">o</seg>s</w> <w n="3.5">t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="3.6" punct="vg:10">h<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="4.2" punct="vg:4">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4" punct="vg" caesura="1">oin</seg></w>,<caesura></caesura> <w n="4.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="4.4" punct="vg:7">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" mp="M">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>r</w>, <w n="4.5">d</w>’<w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="4.7">l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="4.8" punct="pv:10">p<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="10" punct="pv">u</seg>r</rhyme></w> ;</l>
					<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">Fl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="5.3" punct="vg:4">p<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="5.4" punct="pe:5">ou<seg phoneme="i" type="vs" value="1" rule="491" place="5" punct="pe">i</seg></w> ! <w n="5.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="C">ou</seg>s</w> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="5.7">tr<seg phoneme="o" type="vs" value="1" rule="433" place="9">o</seg>p</w> <w n="5.8">fi<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></w></l>
					<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="6.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="6.4" punct="ps:6">c<seg phoneme="œ" type="vs" value="1" rule="345" place="5" mp="M">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="ps">i</seg>r</w>… <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>pr<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10" punct="pt">è</seg>s</rhyme></w>.</l>
					<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">N<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>ph<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rs</w> <w n="7.2" punct="vg:4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>c</w>,<caesura></caesura> <w n="7.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="7.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="7.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="8" mp="C">o</seg>s</w> <w n="7.6" punct="vg:10">r<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>vi<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="8" num="1.8" lm="8"><space quantity="12" unit="char"></space><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="8.4">c<seg phoneme="œ" type="vs" value="1" rule="345" place="4">ue</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="8.5" punct="pe:8">j<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pe">ai</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="2" type="huitain" rhyme="ababcdcd">
					<l n="9" num="2.1" lm="10" met="4+6"><w n="9.1">N<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>ph<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rs</w> <w n="9.2" punct="vg:4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>cs</w>,<caesura></caesura> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="9.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="9.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg>x</w> <w n="9.7" punct="vg:10">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9" mp="M">ê</seg>v<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="403" place="10">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="10" num="2.2" lm="10" met="4+6"><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="10.3" punct="vg:4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3" mp="M">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg" caesura="1">ez</seg></w>,<caesura></caesura> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="10.5">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="6">oi</seg></w> <w n="10.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>c</w> <w n="10.7">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8" mp="M/mp">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="9" mp="Lp">ez</seg></w>-<w n="10.8" punct="pi:10">v<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pi ps">ou</seg>s</rhyme></w> ?…</l>
					<l n="11" num="2.3" lm="10" met="4+6"><w n="11.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="11.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="11.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3" mp="M">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="11.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="C">i</seg>l</w> <w n="11.5">f<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t</w> <w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>r<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="403" place="10">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="12" num="2.4" lm="10" met="4+6"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="12.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" caesura="1">oi</seg>r</w><caesura></caesura> <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="12.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="12.6" punct="ps:7">pr<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="ps">i</seg>s</w>… <w n="12.7"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></w> <w n="12.8" punct="pv:10">j<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>l<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pv">ou</seg>x</rhyme></w> ;</l>
					<l n="13" num="2.5" lm="10" met="4+6"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg></w> <w n="13.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="13.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.6">l</w>’<w n="13.7"><seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="13.8">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.9"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="13.10" punct="vg:10">pr<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>t<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="14" num="2.6" lm="10" met="4+6"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="14.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="14.3" punct="ps:4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3" mp="M">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="ps" caesura="1">er</seg></w>…<caesura></caesura> <w n="14.4">c</w>’<w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="14.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="14.8" punct="pe:10">fr<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="pe">ai</seg>s</rhyme></w> !</l>
					<l n="15" num="2.7" lm="10" met="4+6"><w n="15.1">N<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>ph<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rs</w> <w n="15.2" punct="vg:4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>cs</w>,<caesura></caesura> <w n="15.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="15.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="15.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="15.6" punct="pe:10">n<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="10">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					<l n="16" num="2.8" lm="8"><space quantity="12" unit="char"></space><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="16.4">c<seg phoneme="œ" type="vs" value="1" rule="345" place="4">ue</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="16.5" punct="pe:8">j<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="d" id="8" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pe">ai</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="3" type="huitain" rhyme="ababcdcd">
					<l n="17" num="3.1" lm="10" met="4+6"><w n="17.1">N<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>ph<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rs</w> <w n="17.2" punct="vg:4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>cs</w>,<caesura></caesura> <w n="17.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="17.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="17.5"><seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>x</w> <w n="17.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>g<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rd<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></w></l>
					<l n="18" num="3.2" lm="10" met="4+6"><w n="18.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="18.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>r</w><caesura></caesura> <w n="18.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="18.5">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>d</w> <w n="18.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="18.7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>rs</w> <w n="18.8" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rd<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10" punct="vg">en</seg>ts</rhyme></w>,</l>
					<l n="19" num="3.3" lm="10" met="4+6"><w n="19.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="19.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="19.3">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="19.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="19.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="6" mp="C">o</seg>s</w> <w n="19.6"><seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>x</w> <w n="19.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>d<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></w></l>
					<l n="20" num="3.4" lm="10" met="4+6"><w n="20.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="20.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" caesura="1">e</seg>il</w><caesura></caesura> <w n="20.4"><seg phoneme="i" type="vs" value="1" rule="497" place="5" mp="C">y</seg></w> <w n="20.5" punct="vg:6">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg>t</w>, <w n="20.6">N<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>ph<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rs</w> <w n="20.7" punct="pe:10">bl<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pe">an</seg>cs</rhyme></w> !</l>
					<l n="21" num="3.5" lm="10" met="4+6"><w n="21.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="21.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg>s</w><caesura></caesura> <w n="21.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="21.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="21.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="21.6" punct="vg:10">r<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>vi<rhyme label="c" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
					<l n="22" num="3.6" lm="10" met="4+6"><w n="22.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="22.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="22.3" punct="vg:4">br<seg phoneme="u" type="vs" value="1" rule="427" place="3" mp="M">ou</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>rds</w>,<caesura></caesura> <w n="22.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>s</w> <w n="22.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="22.6">s<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="22.7" punct="ps:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>p<rhyme label="d" id="12" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="ps">ai</seg>s</rhyme></w>…</l>
					<l n="23" num="3.7" lm="10" met="4+6"><w n="23.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="23.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="23.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="23.4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="4" caesura="1">eu</seg></w><caesura></caesura> <w n="23.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="C">ou</seg>s</w> <w n="23.6"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="23.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="23.8" punct="pe:10">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rni<rhyme label="c" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg>s</rhyme></w> !</l>
					<l n="24" num="3.8" lm="8"><space quantity="12" unit="char"></space><w n="24.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="24.4">c<seg phoneme="œ" type="vs" value="1" rule="345" place="4">ue</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="24.5" punct="pe:8">j<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="d" id="12" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pe">ai</seg>s</rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>