<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA20" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="4(abab)">
					<head type="main">AUTOMNE</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="1.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1">Gl<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4" punct="pv:7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rm<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="7" punct="pv">e</seg>il</rhyme></w> ;</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="3.3">h<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="7" met="7"><w n="4.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">j<seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="4.5" punct="pt:7">s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="7" punct="pt">e</seg>il</rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="7" met="7"><w n="5.1">L</w>’<w n="5.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ffr<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="7" met="7"><w n="6.1">M</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="6.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="6.5" punct="vg:7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="vg">ou</seg>r</rhyme></w>,</l>
						<l n="7" num="2.3" lm="7" met="7"><w n="7.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="7" met="7"><w n="8.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="8.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="8.5" punct="pt:7">j<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="pt">ou</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="7" met="7"><w n="9.1" punct="vg:1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" punct="vg">an</seg>d</w>, <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.5" punct="vg:7">plu<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="10" num="3.2" lm="7" met="7"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="10.3">fu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="10.4">d</w>’<w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="10.6">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>l</w> <w n="10.7" punct="vg:7">pr<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="vg">om</seg>pt</rhyme></w>,</l>
						<l n="11" num="3.3" lm="7" met="7"><w n="11.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="11.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="11.5">v<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="12" num="3.4" lm="7" met="7"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="12.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="12.3">s</w>’<w n="12.4" punct="pt:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pt">on</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="7" met="7"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="13.5">t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w>-<w n="13.6">d</w>’<w n="13.7" punct="vg:7"><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="14" num="4.2" lm="7" met="7"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="14.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="14.5" punct="pt:7">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="pt">u</seg>s</rhyme></w>.</l>
						<l n="15" num="4.3" lm="7" met="7"><w n="15.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg></w>, <w n="15.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="15.3" punct="vg:4">j<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>rs</w>, <w n="15.4" punct="pv:7">h<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="16" num="4.4" lm="7" met="7"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="16.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="16.5" punct="pe:7">pl<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="pe">u</seg>s</rhyme></w> !</l>
					</lg>
				</div></body></text></TEI>