<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO49" modus="cm" lm_max="10" metProfile="4+6" form="suite de strophes" schema="2[abba] 1[ababba] 1[abab]">
					<head type="main">LE CAS DÉCIDÉ</head>
					<lg n="1" type="regexp" rhyme="abbaababbaabbaabab">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="1.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">P<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4" caesura="1">ein</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="1.5">Pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="6" mp="M">i</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="1.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="1.7">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="Lc">an</seg>ds</w>-<w n="1.8">C<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></w></l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>t</w> <w n="2.2">s</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="2.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></w> <w n="2.8" punct="dp:10">n<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>v<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="dp">eau</seg></rhyme></w> :</l>
						<l n="3" num="1.3" lm="10" met="4+6">« <w n="3.1" punct="vg:2">P<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="3.2">j</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="3.4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4" caesura="1">ein</seg>t</w><caesura></caesura> <w n="3.5">V<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="3.6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="3.8">l</w>’<w n="3.9" punct="vg:10"><rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="vg">eau</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="10" met="4+6">» <w n="4.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="4.2" punct="vg:2">br<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>s</w>, <w n="4.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="4.4" punct="vg:4">c<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg" caesura="1">u</seg>l</w>,<caesura></caesura> <w n="4.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="4.6">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="4.9">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="4.10" punct="pt:10">ch<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
						<l n="5" num="1.5" lm="10" met="4+6">» <w n="5.1">D</w>’<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd</w> <w n="5.3">j</w>’<w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="5.5">f<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>s</w><caesura></caesura> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="5.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="5.9" punct="pv:10">f<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="u" type="vs" value="1" rule="426" place="10" punct="pv">ou</seg></rhyme></w> ;</l>
						<l n="6" num="1.6" lm="10" met="4+6">» <w n="6.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="6.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="6.3">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="3" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="6.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="6.6">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="6.7">qu</w>’<w n="6.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="6.9" punct="vg:10">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="9" mp="M">ein</seg>t<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="7" num="1.7" lm="10" met="4+6">» <w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">m</w>’<w n="7.3" punct="ps:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" punct="ps in" caesura="1">ai</seg></w>…<caesura></caesura> ‒ <w n="7.4">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="7.5" punct="pi:6">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="6" punct="pi in">oi</seg></w> ? ‒ <w n="7.6">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="7.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="7.9">tr<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg></rhyme></w></l>
						<l n="8" num="1.8" lm="10" met="4+6">» <w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="8.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="8.3" punct="vg:4">D<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352" place="4" punct="vg" caesura="1">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="P">a</seg>r</w> <w n="8.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.7" punct="vg:10"><seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rt<rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="9" num="1.9" lm="10" met="4+6">» <w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="9.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="9.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg></w><caesura></caesura> <w n="9.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="9.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="9.8" punct="vg:10">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>st<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="10" num="1.10" lm="10" met="4+6">» <w n="10.1">M</w>’<w n="10.2" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">in</seg>tr<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="3" mp="M">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="10.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="C">ou</seg>s</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></w> <w n="10.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="9">en</seg></w> <w n="10.6" punct="pt:10"><rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="u" type="vs" value="1" rule="426" place="10" punct="pt">où</seg></rhyme></w>.</l>
						<l n="11" num="1.11" lm="10" met="4+6">» <w n="11.1" punct="vg:1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1" punct="vg">O</seg>r</w>, <w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="11.4">ch<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="11.6" punct="pt:10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>sc<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="378" place="10">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="12" num="1.12" lm="10" met="4+6">» <w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="12.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="12.3" punct="vg:4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="12.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="12.5">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" mp="M">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l</w> <w n="12.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>ss<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="10">ein</seg></rhyme></w></l>
						<l n="13" num="1.13" lm="10" met="4+6">» <w n="13.1">F<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>t</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="13.3">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="3" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="13.4">d</w>’<w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="13.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" mp="M">o</seg>bj<seg phoneme="ɛ" type="vs" value="1" rule="190" place="7">e</seg>t</w> <w n="13.7" punct="dp:10">f<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>n<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="dp">in</seg></rhyme></w> :</l>
						<l n="14" num="1.14" lm="10" met="4+6">» <w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">p<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="14.3">n</w>’<w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4" caesura="1">e</seg>st</w><caesura></caesura> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="14.6">R<seg phoneme="ɔ" type="vs" value="1" rule="441" place="6">o</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="14.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="14.9" punct="pt:10">Fl<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="15" num="1.15" lm="10" met="4+6">» ‒ <w n="15.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="15.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="15.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="15.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="15.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="M">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>ds</w> <w n="15.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="15.7" punct="vg:10">c<rhyme label="a" id="7" gender="m" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>s</rhyme></w>, »</l>
						<l n="16" num="1.16" lm="10" met="4+6"><w n="16.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="16.3" punct="pv:4">P<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" punct="pv in" caesura="1">e</seg>r</w> ;<caesura></caesura> « <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="16.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" mp="M">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="16.6" punct="pe:10">f<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>l<rhyme label="b" id="8" gender="f" type="a" stanza="4"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></w> !</l>
						<l n="17" num="1.17" lm="10" met="4+6">» <w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="17.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="17.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>bs<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="17.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="17.5">n</w>’<w n="17.6"><seg phoneme="i" type="vs" value="1" rule="497" place="6">y</seg></w> <w n="17.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="17.8" punct="vg:10">p<rhyme label="a" id="7" gender="m" type="e" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>s</rhyme></w>,</l>
						<l n="18" num="1.18" lm="10" met="4+6">» <w n="18.1" punct="vg:1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>r</w>, <w n="18.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="18.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="18.4" punct="vg:4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg" caesura="1">on</seg>d</w>,<caesura></caesura> <w n="18.5">c</w>’<w n="18.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="18.7">p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="18.8" punct="pt:10">b<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<rhyme label="b" id="8" gender="f" type="e" stanza="4"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>. »</l>
					</lg>
				</div></body></text></TEI>