<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO24" modus="sm" lm_max="7" metProfile="7" form="suite de strophes" schema="2[abba] 1[ababa]">
					<head type="main">ÉLÉGIE</head>
					<lg n="1" type="regexp" rhyme="abbaabbaababa">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="1.2">di<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>z<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="2.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="2.6" punct="vg:7">m<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7" punct="vg">ain</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="3.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>br<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="3.4" punct="vg:7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="vg">in</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="7" met="7"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="4.4">ch<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="4.5" punct="pe:7"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></w> !</l>
						<l n="5" num="1.5" lm="7" met="7"><w n="5.1">S<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>t</w> <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="5.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w>-<w n="5.7" punct="pe:7">l<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="342" place="7" punct="pe">à</seg></rhyme></w> !</l>
						<l n="6" num="1.6" lm="7" met="7"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="6.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="6.5" punct="vg:7">ch<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></w>,</l>
						<l n="7" num="1.7" lm="7" met="7"><w n="7.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="7.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="7.5">d</w>’<w n="7.6"><rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></w></l>
						<l n="8" num="1.8" lm="7" met="7"><w n="8.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4" punct="pt:7"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pt">a</seg></rhyme></w>.</l>
						<l n="9" num="1.9" lm="7" met="7"><w n="9.1" punct="pe:1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">a</seg>s</w> ! <w n="9.2">j</w>’<w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="9.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.6" punct="pt:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<rhyme label="a" id="6" gender="f" type="a" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
						<l n="10" num="1.10" lm="7" met="7"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2" punct="pi:3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pi">e</seg></w> ? <w n="10.3">j</w>’<w n="10.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="10.5" punct="dp:7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg>c<rhyme label="b" id="5" gender="m" type="a" stanza="3"><seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="dp">u</seg></rhyme></w> :</l>
						<l n="11" num="1.11" lm="7" met="7"><w n="11.1">J<seg phoneme="ɑ̃" type="vs" value="1" rule="309" place="1">ean</seg></w> <w n="11.2">Ch<seg phoneme="u" type="vs" value="1" rule="d-2" place="2">ou</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt</w> <w n="11.3">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="4">ou</seg><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="11.5" punct="pv:7">r<rhyme label="a" id="6" gender="f" type="e" stanza="3"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="12" num="1.12" lm="7" met="7"><w n="12.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="12.4" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>c<rhyme label="b" id="5" gender="m" type="e" stanza="3"><seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="vg">u</seg></rhyme></w>,</l>
						<l n="13" num="1.13" lm="7" met="7"><w n="13.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="13.5" punct="pt:7">v<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<rhyme label="a" id="6" gender="f" type="a" stanza="3"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>