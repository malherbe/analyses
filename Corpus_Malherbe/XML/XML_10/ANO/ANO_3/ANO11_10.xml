<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO11" modus="cp" lm_max="11" metProfile="8, 6, 3, (11)" form="strophe unique" schema="1(ababccdeed)">
					<head type="main">COUPLET</head>
					<head type="tune">Air : <hi rend="ital">La faridondaine, la faridondon.</hi></head>
					<lg n="1" type="dizain" rhyme="ababccdeed">
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="6"></space><w n="1.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="1.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>x</w> <w n="1.3" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>s</w>, <w n="1.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="10"></space><w n="2.1">Cr<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>t</w> <w n="2.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.3">f<seg phoneme="a" type="vs" value="1" rule="193" place="3">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="2.4" punct="pv:6">h<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>dr<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>p<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="6"></space><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="3.3">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="3.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="3.6" punct="dp:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="dp">in</seg></rhyme></w> :</l>
						<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="10"></space>« <w n="4.1" punct="pe:1"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="1" punct="pe">A</seg>ye</w> ! <w n="4.2" punct="pe:2"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="2" punct="pe">a</seg>ye</w> ! <w n="4.3">J</w>’<w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.6" punct="vg:6">c<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>, »</l>
						<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="6"></space><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.3">d</w>’<w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="5.5">gr<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="5.6" punct="vg:8">g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rç<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></w>,</l>
						<l n="6" num="1.6" lm="11"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="6.2" punct="vg:6">f<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg" mp="F">e</seg></w>, <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="6.4" punct="vg:11">f<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">on</seg>d<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" punct="vg">on</seg></rhyme></w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="6"></space><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="7.4">p<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.5" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></rhyme></w>,</l>
						<l n="8" num="1.8" lm="3" met="3"><space unit="char" quantity="16"></space><w n="8.1" punct="vg:3">B<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>b<rhyme label="e" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></rhyme></w>,</l>
						<l n="9" num="1.9" lm="8" met="8"><space unit="char" quantity="6"></space><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5" punct="vg:8">B<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="e" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></rhyme></w>,</l>
						<l n="10" num="1.10" lm="3" met="3"><space unit="char" quantity="16"></space><w n="10.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.2" punct="pt:3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<rhyme label="d" id="5" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="pt">i</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>