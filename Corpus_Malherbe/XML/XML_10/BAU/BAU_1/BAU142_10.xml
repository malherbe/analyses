<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUPPLÉMENT AUX FLEURS DU MAL</head><div type="poem" key="BAU142" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="5(abab)">
					<head type="number">VI</head>
					<head type="main">Hymne</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w>-<w n="1.4" punct="vg:4">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w>-<w n="1.8">b<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="2.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="2.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6" punct="vg:8">cl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="3.2">l</w>’<w n="3.3" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.7" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="4.3" punct="pe:8"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="5.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="5.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.6">v<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r</w> <w n="6.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">im</seg>pr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6" punct="vg:8">s<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="vg">e</seg>l</rhyme></w>,</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="7.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.5"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.3">g<seg phoneme="u" type="vs" value="1" rule="425" place="4">oû</seg>t</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">l</w>’<w n="8.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rn<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="9.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rf<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>tm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>sph<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.3">d</w>’<w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="10.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>r</w> <w n="10.6" punct="vg:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>du<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>t</rhyme></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="11.2"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="11.4">f<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="12.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="12.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="12.6" punct="vg:8">nu<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>t</rhyme></w>,</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1" punct="vg:2">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="2" punct="vg">en</seg>t</w>, <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="13.3" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>rr<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>pt<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">T</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>xpr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="14.4" punct="pi:8">v<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pi">é</seg></rhyme></w> ?</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">Gr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="1">ain</seg></w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">m<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>sc</w> <w n="15.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="15.5" punct="vg:5">g<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>s</w>, <w n="15.6" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="16.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="16.5" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></w> !</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="17.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="17.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w>-<w n="17.4" punct="vg:4">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4" punct="vg">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="17.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="17.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w>-<w n="17.8">b<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="18.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="18.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="18.4">j<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="18.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="18.7" punct="vg:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="19.2">l</w>’<w n="19.3" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="19.5">l</w>’<w n="19.6"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.7" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="20.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="20.3" punct="pe:8"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></w> !</l>
					</lg>
				</div></body></text></TEI>