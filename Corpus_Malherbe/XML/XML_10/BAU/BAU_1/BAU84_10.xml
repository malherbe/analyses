<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU84" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="9((aa))">
					<head type="number">LXXX</head>
					<head type="main">Spleen</head>
					<lg n="1" type="distiques" rhyme="aa…">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="1.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="1.5">r<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="1.6">d</w>’<w n="1.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="1.8">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="8" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="9">y</seg>s</w> <w n="1.9" punct="vg:12">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:2">R<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="2.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="2.3" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" mp="M">im</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="2.4">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="2.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="2.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="11" mp="Lc">è</seg>s</w>-<w n="2.8" punct="vg:12">vi<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="3.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="3.4">pr<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>pt<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="3.5">m<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="3.7" punct="vg:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>rb<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S</w>’<w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="1" mp="M">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="4.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="4.5">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6" caesura="1">en</seg>s</w><caesura></caesura> <w n="4.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>c</w> <w n="4.8">d</w>’<w n="4.9"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="4.10" punct="pt:12">b<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w> <w n="5.4">l</w>’<w n="5.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5" mp="M">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="5.6">n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="5.7" punct="vg:9">g<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>bi<seg phoneme="e" type="vs" value="1" rule="347" place="9" punct="vg">er</seg></w>, <w n="5.8">n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="5.9" punct="vg:12">f<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>c<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></rhyme></w>,</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="6.3">p<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.4">m<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="6.6">f<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="6.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="6.8" punct="pt:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>lc<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></rhyme></w>.</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="7.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>ff<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="7.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="7.5">gr<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ll<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>str<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="8.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="8.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="8.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="8.8">cr<seg phoneme="y" type="vs" value="1" rule="454" place="9" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>l</w> <w n="8.9" punct="pv:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="9.2">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="9.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3" mp="M">eu</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="9.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>sf<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="9.7" punct="vg:12">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg>b<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></rhyme></w>,</l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="10.3">d<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="10.4">d</w>’<w n="10.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="10.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="10.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="10.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="10.9">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.10"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="10.11" punct="vg:12">b<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></rhyme></w>,</l>
						<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="11.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="11.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" mp="M">im</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.7">t<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>l<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="12.2">t<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="12.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="12.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="12.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="12.7">j<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.8" punct="pt:12">squ<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>l<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="13.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="13.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="C">i</seg></w> <w n="13.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="13.7">l</w>’<w n="13.8"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</w> <w n="13.9">n</w>’<w n="13.10"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="13.11">j<seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s</w> <w n="13.12">p<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg></rhyme></w></l>
						<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="14.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>xt<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>rp<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="9">en</seg>t</w> <w n="14.7" punct="vg:12">c<seg phoneme="o" type="vs" value="1" rule="435" place="10" mp="M">o</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg>p<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="vg">u</seg></rhyme></w>,</l>
						<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="15.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="15.4">b<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="15.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>g</w><caesura></caesura> <w n="15.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="15.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="15.9">R<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10">ain</seg>s</w> <w n="15.10">n<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="C">ou</seg>s</w> <w n="15.11" punct="vg:12">vi<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="366" place="12">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>nt</rhyme></w>,</l>
						<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="16.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="16.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="C">eu</seg>rs</w> <w n="16.5">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="16.6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="16.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="16.8">pu<seg phoneme="i" type="vs" value="1" rule="491" place="8" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ts</w> <w n="16.9">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="16.10" punct="vg:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>vi<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="366" place="12">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>nt</rhyme></w>,</l>
						<l n="17" num="1.17" lm="12" met="6+6"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="17.2">n</w>’<w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="17.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="17.5">r<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>ff<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="17.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="17.7">c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="17.8">h<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></w></l>
						<l n="18" num="1.18" lm="12" met="6+6"><w n="18.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="18.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="18.4">li<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="18.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="18.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>g</w><caesura></caesura> <w n="18.7">l</w>’<w n="18.8"><seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="18.9">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="18.10">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="18.11" punct="pt:12">L<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>th<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>