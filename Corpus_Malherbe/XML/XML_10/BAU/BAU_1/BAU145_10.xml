<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUPPLÉMENT AUX FLEURS DU MAL</head><div type="poem" key="BAU145" modus="sp" lm_max="8" metProfile="8, 6, 4" form="suite périodique avec alternance de type 1" schema="3{1(ababcdcd) 1(ababab)}">
					<head type="number">IX</head>
					<head type="main">Le Jet d’eau</head>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="1.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="1.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="1.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="1.5" punct="vg:5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>s</w>, <w n="1.6">p<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>ps</w> <w n="2.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="2.5" punct="vg:8">r<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>vr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>r</rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="3.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="4.2">t</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.6" punct="pt:8">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>r</rhyme></w>.</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5">j<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="5.6">d</w>’<w n="5.7"><seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="5.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="5.9">j<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="6.5">n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="6.6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="6.7">n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="6.8" punct="vg:8">j<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>t</w> <w n="7.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="7.3">l</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>xt<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="8.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="8.4">m</w>’<w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.6">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="8.7">l</w>’<w n="8.8" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="2" type="sizain" rhyme="ababab">
						<l n="9" num="2.1" lm="6" met="6"><space quantity="6" unit="char"></space><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="5">ou</seg><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="10" num="2.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="10.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3" punct="vg:4">fl<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>rs</rhyme></w>,</l>
						<l n="11" num="2.3" lm="6" met="6"><space quantity="6" unit="char"></space><w n="11.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="11.2">Ph<seg phoneme="e" type="vs" value="1" rule="250" place="2">œ</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="11.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>j<seg phoneme="u" type="vs" value="1" rule="d-2" place="5">ou</seg><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="12" num="2.4" lm="4" met="4"><space quantity="8" unit="char"></space><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="12.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3" punct="vg:4">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>rs</rhyme></w>,</l>
						<l n="13" num="2.5" lm="6" met="6"><space quantity="6" unit="char"></space><w n="13.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4">plu<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="14" num="2.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="14.3" punct="pt:4">pl<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pt">eu</seg>rs</rhyme></w>.</l>
					</lg>
					<lg n="3" type="huitain" rhyme="ababcdcd">
						<l n="15" num="3.1" lm="8" met="8"><w n="15.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="15.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.4">qu</w>’<w n="15.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="16" num="3.2" lm="8" met="8"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r</w> <w n="16.3">br<seg phoneme="y" type="vs" value="1" rule="445" place="3">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="16.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="16.5">v<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>pt<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</rhyme></w></l>
						<l n="17" num="3.3" lm="8" met="8"><w n="17.1">S</w>’<w n="17.2" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="17.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="17.5" punct="vg:8">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="18" num="3.4" lm="8" met="8"><w n="18.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="18.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="18.4">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="18.5" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg>s</rhyme></w>.</l>
						<l n="19" num="3.5" lm="8" met="8"><w n="19.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="19.3">s</w>’<w n="19.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="19.5" punct="vg:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<rhyme label="c" id="9" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="20" num="3.6" lm="8" met="8"><w n="20.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="20.3">fl<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>t</w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="20.5">tr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="20.6" punct="vg:8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>gu<rhyme label="d" id="10" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</rhyme></w>,</l>
						<l n="21" num="3.7" lm="8" met="8"><w n="21.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="21.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="21.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="21.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="21.5">p<rhyme label="c" id="9" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="22" num="3.8" lm="8" met="8"><w n="22.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d</w> <w n="22.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>squ</w>’<w n="22.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="22.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d</w> <w n="22.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="22.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="22.7" punct="pt:8">c<rhyme label="d" id="10" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="pt">œu</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="4" type="sizain" rhyme="ababab">
						<l n="23" num="4.1" lm="6" met="6"><space quantity="6" unit="char"></space><w n="23.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="23.2">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="5">ou</seg><rhyme label="a" id="11" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="24" num="4.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="24.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="24.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="24.3" punct="vg:4">fl<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>rs</rhyme></w>,</l>
						<l n="25" num="4.3" lm="6" met="6"><space quantity="6" unit="char"></space><w n="25.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="25.2">Ph<seg phoneme="e" type="vs" value="1" rule="250" place="2">œ</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="25.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>j<seg phoneme="u" type="vs" value="1" rule="d-2" place="5">ou</seg><rhyme label="a" id="11" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="26" num="4.4" lm="4" met="4"><space quantity="8" unit="char"></space><w n="26.1">M<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="26.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="26.3" punct="vg:4">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>rs</rhyme></w>,</l>
						<l n="27" num="4.5" lm="6" met="6"><space quantity="6" unit="char"></space><w n="27.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="27.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="27.4">plu<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="28" num="4.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="28.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="28.3" punct="pt:4">pl<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pt">eu</seg>rs</rhyme></w>.</l>
					</lg>
					<lg n="5" type="huitain" rhyme="ababcdcd">
						<l n="29" num="5.1" lm="8" met="8"><w n="29.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="29.2" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="29.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="29.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="29.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="29.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d</w> <w n="29.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="29.8" punct="vg:8">b<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="30" num="5.2" lm="8" met="8"><w n="30.1">Qu</w>’<w n="30.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="30.3">m</w>’<w n="30.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="30.5" punct="vg:3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>x</w>, <w n="30.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="30.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="30.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="30.9" punct="vg:8">s<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8" punct="vg">ein</seg>s</rhyme></w>,</l>
						<l n="31" num="5.3" lm="8" met="8"><w n="31.1">D</w>’<w n="31.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="31.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="31.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rn<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="32" num="5.4" lm="8" met="8"><w n="32.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="32.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>gl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="32.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="32.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="32.5" punct="pe:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pe">in</seg>s</rhyme></w> !</l>
						<l n="33" num="5.5" lm="8" met="8"><w n="33.1" punct="vg:1">L<seg phoneme="y" type="vs" value="1" rule="453" place="1" punct="vg">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="33.2"><seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="33.3" punct="vg:5">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="33.4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="33.5" punct="vg:8">b<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<rhyme label="c" id="15" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="34" num="5.6" lm="8" met="8"><w n="34.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="34.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="34.3">fr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="34.4" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t<rhyme label="d" id="16" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></w>,</l>
						<l n="35" num="5.7" lm="8" met="8"><w n="35.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="35.2">p<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="35.3">m<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="c" id="15" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="36" num="5.8" lm="8" met="8"><w n="36.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="36.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="36.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="36.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="36.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="36.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="d" id="16" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="6" type="sizain" rhyme="ababab">
						<l n="37" num="6.1" lm="6" met="6"><space quantity="6" unit="char"></space><w n="37.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="37.2">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="5">ou</seg><rhyme label="a" id="17" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="38" num="6.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="38.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="38.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="38.3" punct="vg:4">fl<rhyme label="b" id="18" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>rs</rhyme></w>,</l>
						<l n="39" num="6.3" lm="6" met="6"><space quantity="6" unit="char"></space><w n="39.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="39.2">Ph<seg phoneme="e" type="vs" value="1" rule="250" place="2">œ</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="39.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>j<seg phoneme="u" type="vs" value="1" rule="d-2" place="5">ou</seg><rhyme label="a" id="17" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="40" num="6.4" lm="4" met="4"><space quantity="8" unit="char"></space><w n="40.1">M<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="40.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="40.3" punct="vg:4">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<rhyme label="b" id="18" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>rs</rhyme></w>,</l>
						<l n="41" num="6.5" lm="6" met="6"><space quantity="6" unit="char"></space><w n="41.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="41.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="41.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="41.4">plu<rhyme label="a" id="17" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
						<l n="42" num="6.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="42.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="42.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="42.3" punct="pt:4">pl<rhyme label="b" id="18" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="pt">eu</seg>rs</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>