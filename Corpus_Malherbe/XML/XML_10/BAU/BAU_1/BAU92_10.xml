<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU92" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="6(abba)">
					<head type="number">LXXXVIII</head>
					<head type="main">L’Horloge</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="pe:3">H<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="M">o</seg>rl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe" mp="F">e</seg></w> ! <w n="1.2">di<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="1.3" punct="vg:6">s<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="1.4" punct="vg:9"><seg phoneme="e" type="vs" value="1" rule="353" place="7" mp="M">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="339" place="8" mp="M">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" punct="vg">an</seg>t</w>, <w n="1.5" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" mp="M">im</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>gt</w> <w n="2.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="2.5">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="2.8" punct="dp:9">d<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="dp in">i</seg>t</w> : « <hi rend="ital"><w n="2.9">S<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M/mp">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="11" mp="Lp">en</seg>s</w>-<w n="2.10" punct="pe:12">t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pe">oi</seg></rhyme></w> !</hi></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="3.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="3.3">D<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="3.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="3.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="3.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>r</w> <w n="3.7">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="10">ein</seg></w> <w n="3.8">d</w>’<w n="3.9"><seg phoneme="e" type="vs" value="1" rule="353" place="11" mp="M">e</seg>ffr<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="4.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5" mp="M">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="6" caesura="1">ô</seg>t</w><caesura></caesura> <w n="4.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="4.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="4.6"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="4.7" punct="pv:12">c<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">Pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="5.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="5.4">fu<seg phoneme="i" type="vs" value="1" rule="491" place="7" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="5.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9" mp="P">e</seg>rs</w> <w n="5.6">l</w>’<w n="5.7">h<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>z<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="6.2">qu</w>’<w n="6.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.4">s<seg phoneme="i" type="vs" value="1" rule="493" place="5" mp="M">y</seg>lph<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="6.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="6.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="6.9" punct="pv:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>l<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" mp="M">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="7.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="7.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="7.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rc<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="7.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="7.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>l<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="8.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="8.3">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="8.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="8.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="8.8" punct="pt:12">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>s<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">Tr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="9.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>x</w> <w n="9.4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>ts</w> <w n="9.5">f<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="9.7" punct="vg:9">h<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="9.9">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>c<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1" punct="dp:3">Ch<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="M">u</seg>ch<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="dp" mp="F">e</seg></w> : <hi rend="ital"><w n="10.2" punct="tc:6">S<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M/mp">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5" mp="Lp">en</seg>s</w>-<w n="10.3" punct="pe:6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="ti" caesura="1">oi</seg></w> !<caesura></caesura></hi> — <w n="10.4" punct="vg:8">R<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>c</w> <w n="10.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="10.7">v<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>x</rhyme></w></l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">D</w>’<w n="11.2" punct="vg:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">in</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="11.3">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" mp="M">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="11.4" punct="dp:7">d<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="dp">i</seg>t</w> : <w n="11.5">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="11.6">su<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>s</w> <w n="11.7" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="M">Au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>f<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>s</rhyme></w>,</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">j</w>’<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="12.4">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">om</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="12.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="12.6">v<seg phoneme="i" type="vs" value="1" rule="482" place="6" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="12.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="12.9">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">om</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.10" punct="pe:12"><seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>mm<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abba">
						<l n="13" num="4.1" lm="12" met="6+6"><foreign lang="ANG"><w n="13.1" punct="pe:3">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">em</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="pe">er</seg></w> !</foreign><hi rend="ital"><w n="13.2">S<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M/mp">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5" mp="Lp">en</seg>s</w>-<w n="13.3" punct="pe:6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="pe" caesura="1">oi</seg></w> !<caesura></caesura></hi> <w n="13.4" punct="pe:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <foreign lang="LAT"><w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">E</seg>st<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg></w> <w n="13.6" punct="pe:12">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="pe">o</seg>r</rhyme></w> !</foreign></l>
						<l n="14" num="4.2" lm="12" met="6+6">(<w n="14.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="14.2">g<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="14.4">m<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="14.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="14.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="14.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="14.8" punct="pt:12">l<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></w>.)</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="15.2" punct="vg:4">m<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg" mp="F">e</seg>s</w>, <w n="15.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>l</w><caesura></caesura> <w n="15.4" punct="vg:9">f<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="15.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="15.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="15.7">g<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></w></l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">Qu</w>’<w n="16.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="16.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="16.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="16.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">â</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="16.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="16.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="16.9"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>xtr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="16.10">l</w>’<w n="16.11" punct="pe:12"><rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="pe">o</seg>r</rhyme></w> !</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abba">
						<l n="17" num="5.1" lm="12" met="6+6"><hi rend="ital"><w n="17.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M/mp">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2" mp="Lp">en</seg>s</w>-<w n="17.2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w></hi> <w n="17.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="17.5">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>ps</w><caesura></caesura> <w n="17.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="17.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="17.8">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="9" mp="M">ou</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="17.9"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="18.2">g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="18.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="18.4" punct="vg:6">tr<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="18.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="18.7" punct="pe:9">c<seg phoneme="u" type="vs" value="1" rule="425" place="9" punct="pe">ou</seg>p</w> ! <w n="18.8">c</w>’<w n="18.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="18.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="18.11" punct="pt:12">l<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pt">oi</seg></rhyme></w>.</l>
						<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="19.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="19.3" punct="pv:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>cr<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pv">oî</seg>t</w> ; <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="19.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="19.6" punct="pv:9"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>gm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="pv" mp="F">e</seg></w> ; <hi rend="ital"><w n="19.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M/mp">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="11" mp="Lp">en</seg>s</w>-<w n="19.8" punct="pe:12">t<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pe">oi</seg></rhyme></w> !</hi></l>
						<l n="20" num="5.4" lm="12" met="6+6"><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="20.2">g<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="20.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="20.5" punct="pv:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pv" caesura="1">oi</seg>f</w> ;<caesura></caesura> <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="20.7">cl<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>ps<seg phoneme="i" type="vs" value="1" rule="493" place="9">y</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="20.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="20.9" punct="pt:12">v<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abba">
						<l n="21" num="6.1" lm="12" met="6+6"><w n="21.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>t</w> <w n="21.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="21.3">l</w>’<w n="21.4">h<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="21.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="21.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="21.7">d<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg></w> <w n="21.8" punct="vg:12">H<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>s<rhyme label="a" id="11" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>rd</rhyme></w>,</l>
						<l n="22" num="6.2" lm="12" met="6+6"><w n="22.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="22.2">l</w>’<w n="22.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="3">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="22.4" punct="vg:6">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg></w>,<caesura></caesura> <w n="22.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="22.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="11">o</seg>r</w> <w n="22.8" punct="vg:12">vi<rhyme label="b" id="12" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="23" num="6.3" lm="12" met="6+6"><w n="23.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="23.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="23.3">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="23.4" punct="po:6">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" caesura="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> (<w n="23.5" punct="pe:7"><seg phoneme="o" type="vs" value="1" rule="444" place="7" punct="pe">o</seg>h</w> ! <w n="23.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="23.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.8" punct="pe:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>b<rhyme label="b" id="12" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe vg" mp="F">e</seg></rhyme></w> !),</l>
						<l n="24" num="6.4" lm="12" met="6+6"><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="24.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="24.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="24.4" punct="dp:5">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="dp">a</seg></w> : <w n="24.5" punct="vg:6">M<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>rs</w>,<caesura></caesura> <w n="24.6">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="24.7" punct="pe:8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">â</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="24.8"><seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="C">i</seg>l</w> <w n="24.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="24.10">tr<seg phoneme="o" type="vs" value="1" rule="433" place="11">o</seg>p</w> <w n="24.11" punct="pe:12">t<rhyme label="a" id="11" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pe">a</seg>rd</rhyme></w> ! »</l>
					</lg>
				</div></body></text></TEI>