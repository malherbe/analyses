<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Au vent crispé du matin</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>407 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_3</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Au vent crispé du matin</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/auventcrispdum00carc/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Au vent crispé du matin</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Nouvelle Édition Nouvelle</publisher>
									<date when="1913">1913</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1913">1913</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA BOHÊME ET MON CŒUR</head><div type="poem" key="CRC34" rhyme="none" modus="sm" lm_max="7" metProfile="7">
			<head type="main"><hi rend="ital">LA BOHÊME ET MON CŒUR</hi></head>
			<lg n="1">
				<l n="1" num="1.1" lm="7" met="7"><w n="1.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">t</w>’<w n="1.4" punct="pe:3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3" punct="pe">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="1.5"><seg phoneme="u" type="vs" value="1" rule="426" place="4">Où</seg></w> <w n="1.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>c</w> <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="6">e</seg>s</w>-<w n="1.8">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w></l>
				<l n="2" num="1.2" lm="7" met="7"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="307" place="1">A</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="2.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="2.5" punct="pi:7">p<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pi">e</seg>s</w> ?</l>
				<l n="3" num="1.3" lm="7" met="7"><w n="3.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="3.2">l</w>’<w n="3.3">h<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>r</w> <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="3.5">r<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
				<l n="4" num="1.4" lm="7" met="7"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s</w> <w n="4.3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>rs</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="4.5" punct="pt:7">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="pt">u</seg>s</w>.</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1" lm="7" met="7"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="5.3">fr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>nt</w></l>
				<l n="6" num="2.2" lm="7" met="7"><w n="6.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d</w> <w n="6.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="6.6" punct="pt:7"><seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pt">eu</seg>x</w>.</l>
				<l n="7" num="2.3" lm="7" met="7"><w n="7.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="7.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3" punct="vg:4">ch<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s</w>, <w n="7.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="7.5" punct="vg:7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
				<l n="8" num="2.4" lm="7" met="7"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">n<seg phoneme="y" type="vs" value="1" rule="457" place="3">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="8.4">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg></w> <w n="8.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="8.6" punct="pt:7">f<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pt">eu</seg></w>.</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1" lm="7" met="7"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">plu<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="9.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.5" punct="pv:7">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></w> ;</l>
				<l n="10" num="3.2" lm="7" met="7"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="10.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="10.5" punct="ps:7">br<seg phoneme="y" type="vs" value="1" rule="445" place="6">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="ps">an</seg>t</w>…</l>
				<l n="11" num="3.3" lm="7" met="7">… <w n="11.1">J</w>’<w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="11.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c</w></l>
				<l n="12" num="3.4" lm="7" met="7"><w n="12.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="12.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="12.6" punct="pe:7">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg>s</w> !</l>
			</lg>
		</div></body></text></TEI>