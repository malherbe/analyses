<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Petits airs</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>280 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_2</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Petits airs</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/petitsairspome00carc/page/24/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies complètes</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RONALD DAVIS ET CIE</publisher>
									<date when="1920">1920</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRC21" rhyme="none" modus="sp" lm_max="8" metProfile="8, 7">
		<head type="main">Nuits d’hiver</head>
			<opener>
				<salute><hi rend="smallcap">À JEANNE DIRIS</hi>.</salute>
			</opener>
		<lg n="1">
			<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Nu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>ts</w> <w n="1.2">d</w>’<w n="1.3" punct="pe:3">h<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" punct="pe">e</seg>r</w> ! <w n="1.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="1.5">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>str<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
			<l n="2" num="1.2" lm="7" met="7"><space unit="char" quantity="2"></space><w n="2.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.5" punct="pi:7">m<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="pi">u</seg>r</w> ?</l>
			<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="3.2" punct="vg:3">qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3" punct="vg">e</seg>t</w>, <w n="3.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5" punct="vg:7">pl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="vg">on</seg>d</w>, <w n="3.6" punct="ps:8">f<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
			<l n="4" num="1.4" lm="7" met="7"><space unit="char" quantity="2"></space><w n="4.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>r</w>, <w n="4.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="5">e</seg>s</w> <w n="4.5" punct="pe:7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" punct="pe">e</seg>r</w> !</l>
		</lg>
		<lg n="2">
			<l n="5" num="2.1" lm="8" met="8"><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">n</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.6">r<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="5.8" punct="vg:8">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
			<l n="6" num="2.2" lm="7" met="7"><space unit="char" quantity="2"></space><w n="6.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="6.6" punct="vg:7">y<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="vg">eu</seg>x</w>,</l>
			<l n="7" num="2.3" lm="8" met="8"><w n="7.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="7.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">m<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w>-<w n="7.5" punct="ps:8">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
			<l n="8" num="2.4" lm="7" met="7"><space unit="char" quantity="2"></space><w n="8.1">S<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w>-<w n="8.2">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r</w> <w n="8.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.7" punct="pe:7">v<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pe">eu</seg>x</w> !</l>
		</lg>
		<lg n="3">
			<l n="9" num="3.1" lm="8" met="8"><w n="9.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="9.2" punct="vg:3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="9.3">c<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="9.4">l</w>’<w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="9.7">l</w>’<w n="9.8" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
			<l n="10" num="3.2" lm="7" met="7"><space unit="char" quantity="2"></space><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="10.4" punct="pt:7">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pt">eu</seg>x</w>.</l>
			<l n="11" num="3.3" lm="8" met="8"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="11.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
			<l n="12" num="3.4" lm="7" met="7"><space unit="char" quantity="2"></space><w n="12.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="12.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="12.3">m</w>’<w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="12.5" punct="pt:7">s<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></w>.</l>
		</lg>
		<lg n="4">
			<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="13.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="13.3" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>s</w>, <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg></w> <w n="13.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="13.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
			<l n="14" num="4.2" lm="7" met="7"><space unit="char" quantity="2"></space><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2">n</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="14.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r</w></l>
			<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2" punct="vg:2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg></w>, <w n="15.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="15.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="15.5">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ls</w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.7" punct="vg:8">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
			<l n="16" num="4.4" lm="7" met="7"><space unit="char" quantity="2"></space><w n="16.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="16.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="16.3" punct="pi:7">fr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="pi ps">ez</seg></w> ? …</l>
		</lg>
		<lg n="5">
			<l n="17" num="5.1" lm="8" met="8"><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="17.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w> <w n="17.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="17.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="17.6">l</w>’<w n="17.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
			<l n="18" num="5.2" lm="7" met="7"><space unit="char" quantity="2"></space><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">s</w>’<w n="18.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="vg">o</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="18.5" punct="ps:7">gr<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="ps">an</seg>t</w>…</l>
			<l n="19" num="5.3" lm="8" met="8"><w n="19.1" punct="vg:2">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rs</w>, <w n="19.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.3">qu</w>’<w n="19.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
			<l n="20" num="5.4" lm="7" met="7"><space unit="char" quantity="2"></space><w n="20.1">Pl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="20.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="20.5" punct="pt:7"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
		</lg>
		<lg n="6">
			<l n="21" num="6.1" lm="8" met="8"><w n="21.1">N</w>’<w n="21.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="21.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="21.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g</w> <w n="21.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="21.7">cr<seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
			<l n="22" num="6.2" lm="7" met="7"><space unit="char" quantity="2"></space><w n="22.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3">gr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="22.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="22.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="22.6" punct="pt:7">r<seg phoneme="y" type="vs" value="1" rule="457" place="7">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</w>.</l>
			<l n="23" num="6.3" lm="8" met="8"><w n="23.1" punct="vg:2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="23.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="23.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="23.5">l</w>’<w n="23.6" punct="ps:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="ps">i</seg></w>…</l>
			<l n="24" num="6.4" lm="7" met="7"><space unit="char" quantity="2"></space><w n="24.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="24.2">n</w>’<w n="24.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="24.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="24.5" punct="pt:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="pt">u</seg></w>.</l>
		</lg>
		<lg n="7">
			<l n="25" num="7.1" lm="8" met="8"><w n="25.1">Nu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>ts</w> <w n="25.2">d</w>’<w n="25.3" punct="pe:3">h<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" punct="pe">e</seg>r</w> ! <w n="25.4">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="25.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="25.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t</w> <w n="25.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="25.8">fl<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
			<l n="26" num="7.2" lm="7" met="7"><space unit="char" quantity="2"></space><w n="26.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="26.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="26.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="26.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="26.5" punct="ps:7">m<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="ps">u</seg>r</w>…</l>
			<l n="27" num="7.3" lm="8" met="8"><w n="27.1">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="27.2" punct="vg:4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="27.3"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="27.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>rs</w> <w n="27.5">d</w>’<w n="27.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
			<l n="28" num="7.4" lm="7" met="7"><space unit="char" quantity="2"></space><w n="28.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="28.2">r<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="28.4" punct="vg:7">p<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="vg">u</seg>rs</w>,</l>
		</lg>
		<lg n="8">
			<l n="29" num="8.1" lm="8" met="8"><w n="29.1" punct="vg:1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>t</w>, <w n="29.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="29.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="29.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>str<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="29.5" punct="vg:8">l<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
			<l n="30" num="8.2" lm="7" met="7"><space unit="char" quantity="2"></space><w n="30.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="30.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="30.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="30.5" punct="ps:7">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="ps">i</seg>t</w>…</l>
			<l n="31" num="8.3" lm="8" met="8"><w n="31.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="31.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="31.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ls<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="31.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="31.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
			<l n="32" num="8.4" lm="7" met="7"><space unit="char" quantity="2"></space><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="32.2">l</w>’<w n="32.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="32.5" punct="pt:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pt">i</seg>s</w>.</l>
		</lg>
	</div></body></text></TEI>