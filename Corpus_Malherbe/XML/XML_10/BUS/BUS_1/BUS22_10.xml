<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES CHEMINS</head><head type="sub_part">IMPRESSIONS DE VOYAGE</head><head type="sub_part">PORTUGAL-ESPAGNE</head><div type="poem" key="BUS22" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="1(abba) 3(abab)">
					<head type="number">XX</head>
					<head type="main">LA FOIRE DE SÉVILLE,</head>
					<head type="form">CHANSON ESPAGNOLE</head>
					<head type="sub_2">(Traduction)</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="1.6">d</w>’<w n="1.7" punct="pt:8"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pt">o</seg>r</rhyme></w>.</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D</w>’<w n="2.2" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2" punct="vg">en</seg>t</w>, <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="2.4">f<seg phoneme="a" type="vs" value="1" rule="193" place="4">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.5" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2" punct="vg:3">cu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.4">v<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>pl<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2" punct="vg:2">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2" punct="vg">e</seg>r</w>, <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="4.4" punct="tc:4">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" punct="ti">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> — <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="4.6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>s</w> <w n="4.7" punct="ps:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="ps">o</seg>r</rhyme></w>…</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="5.3">n</w>’<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="5.6">v<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="5.7">S<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>v<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">c<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>t</w> <w n="6.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">l</w>’<w n="6.6" punct="pt:8"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pt pt">e</seg>rs</rhyme></w>. .</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2" punct="vg:5">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="7.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.4">v<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="8.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="8.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="8.6" punct="pe:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>c<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="pe ps">e</seg>rts</rhyme></w> !…</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="pe:2">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="pe">e</seg></w> ! <w n="9.2">qu</w>’<w n="9.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="9.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="9.7" punct="vg:8">n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.4">f<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="10.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="5">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="10.8"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></rhyme></w></l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="11.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>pl<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.4">f<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.6" punct="vg:8">r<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="12.2">s</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>br<seg phoneme="ø" type="vs" value="1" rule="405" place="3">eu</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="12.6" punct="pt:8">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8">— <w n="13.1" punct="pe:1"><seg phoneme="aj" type="vs" value="1" rule="173" place="1" punct="pe">Aï</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> ! <w n="13.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="13.3">m</w>’<w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="13.5" punct="ps:5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="ps">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>… <w n="13.6" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="468" place="6">I</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>d<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="14.2">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="14.3">D<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="14.4">d</w>’<w n="14.5" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">A</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ch<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg></rhyme></w> !</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="15.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="15.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="15.4" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">m</w>’<w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="16.4">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.6">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="16.8" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></w>.</l>
					</lg>
					<closer>
						<placeName>Séville</placeName>.
					</closer>
				</div></body></text></TEI>