<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRISTESSES ET JOIES</head><div type="poem" key="BUS63" modus="cm" lm_max="10" metProfile="4+6" form="sonnet peu classique" schema="abba baab ccd eed">
					<head type="main">IMPROVISÉS</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="1.3">v<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="1.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="1.5" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="1.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.8" punct="vg:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg>nn<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="2.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="2.3" punct="vg:4">m<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg" caesura="1">on</seg>s</w>,<caesura></caesura> <w n="2.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ts</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="2.6" punct="pt:10">r<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg>r</rhyme></w>.</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">j</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="3.4" punct="tc:4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg ti" caesura="1">i</seg></w>,<caesura></caesura> — <w n="3.5">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t</w>-<w n="3.6">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="3.8" punct="pi:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pi">i</seg>r</rhyme></w> ?</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">L</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="4.3">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4" caesura="1">oin</seg></w><caesura></caesura> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="4.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="4.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="4.7">j<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.8" punct="pt:10">n<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="baab">
						<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t</w> <w n="5.2">qu</w>’<w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="5.4">ru<seg phoneme="i" type="vs" value="1" rule="491" place="3" mp="M">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" caesura="1">e</seg>l</w><caesura></caesura> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>pp<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="5.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="5.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>s<rhyme label="b" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</rhyme></w></l>
						<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">n<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="6.3">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="6.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="6.6">n<seg phoneme="a" type="vs" value="1" rule="343" place="7" mp="M">a</seg>ï<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ccr<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="454" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t</w> <w n="7.2">qu</w>’<w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="7.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="7.5">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="4" caesura="1">oi</seg>d</w><caesura></caesura> <w n="7.6">s</w>’<w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>g<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="7.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="P">a</seg>r</w> <w n="7.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="7.10">r<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="457" place="10">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="8.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="8.3" punct="vg:4">j<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg" caesura="1">on</seg>s</w>,<caesura></caesura> <w n="8.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="8.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>t</w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="8.7">s</w>’<w n="8.8" punct="pt:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="8" mp="M">en</seg>h<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rd<rhyme label="b" id="3" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="9.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg></w><caesura></caesura> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="9.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="9.7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>r</w> <w n="9.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="9.9" punct="pv:10">ch<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="10" num="3.2" lm="10" met="4+6"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="10.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="315" place="4" caesura="1">eau</seg>x</w><caesura></caesura> <w n="10.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="10.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="10.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.7">l</w>’<w n="10.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>ch<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" caesura="1">ai</seg></w><caesura></caesura> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.6" punct="ps:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ss<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="ps">er</seg></rhyme></w>…</l>
					</lg>
					<lg n="4" rhyme="eed">
						<l n="12" num="4.1" lm="10" met="4+6"><w n="12.1">Qu</w>’<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="12.3">v<seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="12.4">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4" caesura="1">eu</seg>x</w><caesura></caesura> <w n="12.5" punct="vg:7">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>t</w>, <w n="12.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="12.7" punct="pt:10">ch<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rm<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="13" num="4.2" lm="10" met="4+6"><w n="13.1">Qu</w>’<w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="13.3">f<seg phoneme="y" type="vs" value="1" rule="445" place="2">û</seg>t</w> <w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" caesura="1">o</seg>r</w><caesura></caesura> <w n="13.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="13.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="13.7">m<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="13.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="13.9" punct="pt:10">ch<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						<l n="14" num="4.3" lm="10" met="4+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="P">ou</seg>r</w> <w n="14.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="14.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="14.6">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="14.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="14.8" punct="pe:10">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg>s<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pe">er</seg></rhyme></w> !</l>
					</lg>
				</div></body></text></TEI>