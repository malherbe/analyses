<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS DE TOUS LES PAYS</head><div type="poem" key="BUS29" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="2(abba) 3(aa) 1(abab)">
					<head type="main">CHANSON DES ALPES</head>
					<head type="sub_1">PAYSAGE</head>
					<head type="form">Pantoun</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">l<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="2.5" punct="vg:8">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>ss<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L</w>’<w n="3.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="3.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>cr<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>ss<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="4.3">n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="4.6" punct="pv:8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>p<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					</lg>
					<lg n="2" type="distique" rhyme="aa">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="5.2">l<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="5.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="4">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6" punct="vg:8">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>c<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</rhyme></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">G<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="6.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="6.5" punct="ps:6">d<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="ps">ou</seg>x</w>… <w n="6.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="6.7" punct="pt:8">s<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="pt">œu</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="7" num="3.1" lm="8" met="8"><w n="7.1">L</w>’<w n="7.2">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="7.4">br<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.6" punct="pt:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="8" num="3.2" lm="8" met="8"><w n="8.1">C</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.6" punct="dp:8">d<seg phoneme="i" type="vs" value="1" rule="dc-1" place="6">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="dp">an</seg>s</rhyme></w> :</l>
						<l n="9" num="3.3" lm="8" met="8"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="9.4" punct="pt:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="10" num="3.4" lm="8" met="8"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">bru<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="10.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rgs</w> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="10.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg></w> <w n="10.7" punct="pv:8">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pv">an</seg>s</rhyme></w> ;</l>
					</lg>
					<lg n="4" type="distique" rhyme="aa">
						<l n="11" num="4.1" lm="8" met="8"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="11.2">l<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">aî</seg>t</w> <w n="11.4" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<rhyme label="a" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="12" num="4.2" lm="8" met="8"><w n="12.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="12.2" punct="ps:3">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="ps">e</seg></w>… <w n="12.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="12.5" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="a" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abba">
						<l n="13" num="5.1" lm="8" met="8"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="13.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.6">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>mm<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>t</rhyme></w></l>
						<l n="14" num="5.2" lm="8" met="8"><w n="14.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">p<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>cs</w> <w n="14.3" punct="vg:4">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>g<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg>x</w>, <w n="14.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="14.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg>t</w> <w n="14.7" punct="vg:8">r<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="15" num="5.3" lm="8" met="8"><w n="15.1" punct="vg:3">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>il</w> <w n="15.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.5" punct="dp:8">p<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
						<l n="16" num="5.4" lm="8" met="8"><w n="16.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="16.2" punct="vg:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg>t</w>, <w n="16.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="16.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.6" punct="pt:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>m<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pt">e</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="6" type="distique" rhyme="aa">
						<l n="17" num="6.1" lm="8" met="8"><w n="17.1">S<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>il</w> <w n="17.2" punct="vg:4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>r</w>, <w n="17.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r</w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="17.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></w></l>
						<l n="18" num="6.2" lm="8" met="8"><w n="18.1">G<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="18.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="18.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</rhyme></w>.</l>
					</lg>
					<closer>
						<placeName>Villequier</placeName>.
					</closer>
				</div></body></text></TEI>