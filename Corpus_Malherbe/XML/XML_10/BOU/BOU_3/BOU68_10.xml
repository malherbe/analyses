<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU68" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="5(abba)">
				<head type="number">I</head>
				<head type="main">IMITÉ DU CHINOIS</head>
				<head type="sub_1">In-Kiao-Lï.</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="1.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="1.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ts</w> <w n="1.4" punct="vg:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="vg">e</seg>rs</rhyme></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Pl<seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="2.3" punct="vg:3">f<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>rd</w>, <w n="2.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="2.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="2.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>v<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="3.3" punct="vg:4">m<seg phoneme="œ" type="vs" value="1" rule="389" place="4" punct="vg">oeu</seg>rs</w>, <w n="3.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="3.6" punct="vg:8">v<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="4.2">h<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="4.3" punct="vg:4">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4" punct="vg">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="4.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.7" punct="pe:8">v<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pe">e</seg>rs</rhyme></w> !</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abba">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="5.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l</w> <w n="5.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="5.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="5.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.7">s<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="6.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.4" punct="vg:5">si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="6.5" punct="pt:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></w>.</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>x</w> <w n="7.2" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" punct="vg">an</seg>s</w>, <w n="7.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w>-<w n="7.4" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4" punct="vg">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="7.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="7.6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></rhyme></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="8.2">m<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>ts</w> <w n="8.3">tr<seg phoneme="o" type="vs" value="1" rule="433" place="4">o</seg>p</w> <w n="8.4">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>pts</w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="8.7" punct="pt:8">c<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abba">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">str<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ph<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="9.4">gr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="9.5" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ss<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg>s</rhyme></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="10.2">l</w>’<w n="10.3" punct="vg:2"><seg phoneme="œ" type="vs" value="1" rule="381" place="2" punct="vg">oe</seg>il</w>, <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="10.5" punct="vg:4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4" punct="vg">ain</seg></w>, <w n="10.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="10.8" punct="vg:8">f<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">N</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="11.4">d</w>’<w n="11.5"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="11.7">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg>s</w> <w n="11.8">h<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="12.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="12.6" punct="pt:8">s<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8" punct="pt">ain</seg>ts</rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abba">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2" punct="vg:3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="13.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>t</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">fl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>tt<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>pt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="14.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.6" punct="pe:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>dr<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pe">oi</seg>t</rhyme></w> !</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">p<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="15.3">l<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ttr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="15.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>l</w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="15.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.7">dr<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</rhyme></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">D</w>’<w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="16.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="16.5" punct="pt:8">br<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abba">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="17.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="3">ë</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rd</w> <w n="17.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>pp<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></w></l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="18.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="18.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="18.5" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rl<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="19.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="19.3">tr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rs<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="19.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="19.5" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">C<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w>-<w n="20.2" punct="vg:3"><seg phoneme="i" type="vs" value="1" rule="497" place="3" punct="vg in">y</seg></w>, ‒ <w n="20.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="20.4">n</w>’<w n="20.5"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="20.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</w> <w n="20.7" punct="pe:8">p<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>s</rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>