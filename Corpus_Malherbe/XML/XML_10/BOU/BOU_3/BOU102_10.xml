<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU102" modus="cm" lm_max="10" metProfile="4+6" form="suite périodique" schema="6(abab)">
				<head type="number">XXXV</head>
				<head type="main">DERNIÈRE NUIT</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="1.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">am</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.5">br<seg phoneme="y" type="vs" value="1" rule="445" place="6" mp="M">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="1.6">g<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="1.8" punct="vg:10">g<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="2.2">f<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="2.3">s</w>’<w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4" caesura="1">ein</seg>t</w><caesura></caesura> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="2.8" punct="pt:10">bru<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="10" punct="pt">i</seg>t</rhyme></w>.</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="3.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="3.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="3.6">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="7">en</seg></w> <w n="3.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="3.8">m</w>’<w n="3.9" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>c<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3" punct="vg:4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg" caesura="1">eu</seg>l</w>,<caesura></caesura> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="4.6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.7" punct="pt:10">nu<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="10" punct="pt">i</seg>t</rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" caesura="1">oi</seg></w> ‒<caesura></caesura> <w n="5.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="5.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="5.7" punct="vg:10">t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3" punct="vg:4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg in" caesura="1">ai</seg>s</w>,<caesura></caesura> ‒ <w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="6.5">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="6.7" punct="dp:10">pl<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>c<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="dp">é</seg></rhyme></w> :</l>
					<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1">T<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg></w> <w n="7.2">f<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>l</w><caesura></caesura> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="7.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="7.6" punct="vg:10">f<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1">Sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ctr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="8.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="4" caesura="1">eau</seg>x</w><caesura></caesura> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="8.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="8.6">b<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="8.7" punct="pt:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ss<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="9.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="9.4" punct="vg:4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="vg" caesura="1">o</seg>rt</w>,<caesura></caesura> <w n="9.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r</w> <w n="9.7">qu</w>’<w n="9.8"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l</w> <w n="9.9" punct="pt:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="10" num="3.2" lm="10" met="4+6"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="10.3">m</w>’<w n="10.4" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.6">l</w>’<w n="10.7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="7">ue</seg>il</w> <w n="10.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="8" mp="M">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>st<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</rhyme></w></l>
					<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg></w> <w n="11.3">n<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="11.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="11.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="11.8" punct="vg:10">j<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="M">eu</seg>n<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="12" num="3.4" lm="10" met="4+6"><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="12.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="4" caesura="1">eau</seg></w><caesura></caesura> <w n="12.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="12.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="C">i</seg>l</w> <w n="12.6">f<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t</w> <w n="12.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="12.8" punct="pt:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>st<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pt">eu</seg>r</rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="10" met="4+6"><w n="13.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>il</w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="13.3">fl<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>x</w><caesura></caesura> <w n="13.4">d</w>’<w n="13.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="13.6">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>r</w> <w n="13.7" punct="vg:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>f<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>c<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="14" num="4.2" lm="10" met="4+6"><w n="14.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="14.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="14.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="14.5">s<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>lcr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rm<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></rhyme></w></l>
					<l n="15" num="4.3" lm="10" met="4+6"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2">en</seg>s</w> <w n="15.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="4" caesura="1">à</seg></w><caesura></caesura> <w n="15.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="15.5">l</w>’<w n="15.6"><seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="15.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="15.8">m<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
					<l n="16" num="4.4" lm="10" met="4+6"><w n="16.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="16.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="16.3" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="16.4">m</w>’<w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="16.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt</w> <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="16.8" punct="pt:10">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg></rhyme></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="10" met="4+6"><w n="17.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="17.2">L<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="17.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="17.4" punct="pe:4">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pe" caesura="1">oi</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> !<caesura></caesura> <w n="17.5" punct="pe:5"><seg phoneme="o" type="vs" value="1" rule="444" place="5" punct="pe">O</seg>h</w> ! <w n="17.6">L<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="17.7">nu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>t</w> <w n="17.8" punct="pe:10">d<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>r<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="403" place="10">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></w> !</l>
					<l n="18" num="5.2" lm="10" met="4+6"><w n="18.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="18.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="18.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="18.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="18.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="18.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7">ein</seg></w> <w n="18.7" punct="pt:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pt">an</seg>t</rhyme></w>.</l>
					<l n="19" num="5.3" lm="10" met="4+6"><w n="19.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="19.2">fr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="19.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="19.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="19.6">p<seg phoneme="wa" type="vs" value="1" rule="420" place="7" mp="M">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="19.7" punct="pi:10">cr<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="403" place="10">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					<l n="20" num="5.4" lm="10" met="4+6"><w n="20.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ls</w> <w n="20.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="20.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="20.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>ps</w><caesura></caesura> <w n="20.5">s<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="20.6">qu</w>’<w n="20.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="20.8" punct="pi:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>t<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" punct="pi">en</seg>d</rhyme></w> ?</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="10" met="4+6"><w n="21.1">Qu</w>’<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="1" mp="Lp">e</seg>s</w>-<w n="21.3" punct="pi:2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="pi">u</seg></w> ? <w n="21.4">Qu</w>’<w n="21.5"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="3" mp="Lp">e</seg>s</w>-<w n="21.6" punct="pi:4">t<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="pi" caesura="1">u</seg></w> ?<caesura></caesura> <w n="21.7" punct="vg:5">P<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="21.8"><seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg></w> <w n="21.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="21.10"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">om</seg>pt<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
					<l n="22" num="6.2" lm="10" met="4+6"><w n="22.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="22.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="22.3" punct="vg:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>ts</w>,<caesura></caesura> <w n="22.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="22.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="22.6">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>cs</w> <w n="22.7" punct="pi:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rm<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pi">é</seg></rhyme></w> ?</l>
					<l n="23" num="6.3" lm="10" met="4+6"><w n="23.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="23.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>x</w> <w n="23.3" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="23.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="23.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>x</w> <w n="23.6" punct="dp:10">l<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>t<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></w> :</l>
					<l n="24" num="6.4" lm="10" met="4+6">« <w n="24.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="24.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="24.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="24.4" punct="vg:4">c<seg phoneme="œ" type="vs" value="1" rule="389" place="4" punct="vg" caesura="1">oeu</seg>r</w>,<caesura></caesura> <w n="24.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="24.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="24.7">n</w>’<w n="24.8"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="24.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="24.10" punct="pe:10"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="9" mp="M">ai</seg>m<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pe">é</seg></rhyme></w> ! »</l>
				</lg>
			</div></body></text></TEI>