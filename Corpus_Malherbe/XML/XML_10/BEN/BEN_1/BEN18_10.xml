<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS SUR LA BEAUTÉ ET SUR LA LAIDEUR</head><div type="poem" key="BEN18" modus="sm" lm_max="8" metProfile="8" form="sonnet classique, prototype 1" schema="abba abba ccd eed">
					<head type="main">Sur la Beauté et la Laideur.</head>
					<head type="form">SONNET XVII.</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">CH<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">E</seg>V<seg phoneme="ø" type="vs" value="1" rule="398" place="2">EU</seg>X</w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="1.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>rpr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="2.2">s<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.5" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>li<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="3.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="3.5">li<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D</w>’<w n="4.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.3">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="4.6" punct="pv:8">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">aî</seg>n<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">Ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="5.4" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">Cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r</w>-<w n="6.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="6.4">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w> <w n="6.5">qu</w>’<w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="6.7" punct="vg:8">pi<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg></rhyme></w>,</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="7.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="7.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>st</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="7.7">Di<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="8.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.4" punct="pv:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">im</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg>x</w> <w n="9.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="9.3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>s</w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="9.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="c" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></rhyme></w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">S<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="10.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="10.6" punct="vg:8">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="c" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">ez</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="11.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="11.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="11.5">n<seg phoneme="ø" type="vs" value="1" rule="248" place="6">œu</seg>d</w> <w n="11.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="11.7" punct="pi:8">r<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
					</lg>
					<lg n="4" rhyme="eed">
						<l n="12" num="4.1" lm="8" met="8"><w n="12.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="12.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="2">ein</seg>s</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.7" punct="vg:8">p<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>x</rhyme></w>,</l>
						<l n="13" num="4.2" lm="8" met="8"><w n="13.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>s</w> <w n="13.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="13.3" punct="vg:5">gr<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>s</w>, <w n="13.4">qu</w>’<w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w>-<w n="13.6">v<rhyme label="e" id="7" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</rhyme></w></l>
						<l n="14" num="4.3" lm="8" met="8"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="14.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="14.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="14.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="14.7" punct="pi:8">c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>rr<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
					</lg>
				</div></body></text></TEI>