<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS SUR LA BEAUTÉ ET SUR LA LAIDEUR</head><div type="poem" key="BEN19" modus="sm" lm_max="8" metProfile="8" form="sonnet classique, prototype 1" schema="abba abba ccd eed">
					<head type="main">Sur la Beauté et la Laideur.</head>
					<head type="form">SONNET XVIII.</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">E</seg></w> <w n="1.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="1.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="1.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="1.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="1.6">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</rhyme></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="2.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>stru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="2.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.6">l</w>’<w n="2.7" punct="pt:8"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L</w>’<w n="3.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">C<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="3.6" punct="vg:8">bl<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4">en</seg>s</w> <w n="4.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.7" punct="pt:8">d<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="5.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.5" punct="vg:5"><seg phoneme="i" type="vs" value="1" rule="d-1" place="4">I</seg><seg phoneme="o" type="vs" value="1" rule="444" place="5" punct="vg">o</seg></w>, <w n="5.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="5.7">j<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</rhyme></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2">p<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>l</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4" punct="vg:5">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.7">m<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1" punct="vg:2">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>t</w>, <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="7.4" punct="vg:8">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">ch<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ds</w> <w n="8.3">cl<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ts</w> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="8.5" punct="pt:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>fr<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>d<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="9.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="9.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="9.4">l</w>’<w n="9.5"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.7">l</w>’<w n="9.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">C<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="10.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.5" punct="vg:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="11.4" punct="pt:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pt">en</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="4" rhyme="eed">
						<l n="12" num="4.1" lm="8" met="8"><w n="12.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="12.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="12.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.6">s</w>’<w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="12.8" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rr<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="13" num="4.2" lm="8" met="8"><w n="13.1">N</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="13.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="13.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.6">qu</w>’<w n="13.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="13.9" punct="vg:8">r<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="14" num="4.3" lm="8" met="8"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="14.2" punct="pi:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pi">en</seg>t</rhyme></w> ?</l>
					</lg>
				</div></body></text></TEI>