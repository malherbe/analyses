<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE CY GIST</head><head type="sub_part">
					OU 
					Diverses Épitaphes pour toute sorte de personnes 
					de l’un et de l’autre sexe, et pour l’Auteur même, 
					quoique vivant.
				</head><div type="poem" key="BEN146" modus="cp" lm_max="12" metProfile="6+6, 4+6" form="suite de strophes" schema="1[abab] 5[aa]">
					<head type="main">Épitaphe des plus grands Héros.</head>
					<lg n="1" type="regexp" rhyme="ababaaaaaaaaaa">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg></w> <w n="1.2">g<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="1.4">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="1.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="1.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="1.8">f<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg></w> <w n="1.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="P">a</seg>r</w> <w n="1.10" punct="vg:12">t<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>t</rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="2.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="P">a</seg>r</w> <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="2.8" punct="pv:12">C<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>m<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>s</rhyme></w> ;</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">sç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="Lp">ai</seg>t</w>-<w n="3.3" punct="vg:3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg></w>, <w n="3.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="Lc">à</seg></w>-<w n="3.6" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="3.7">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.8">n<seg phoneme="y" type="vs" value="1" rule="457" place="9">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="3.10" punct="vg:12">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>b<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>t</rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="4.2">n</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="4.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>t</w> <w n="4.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="Lc">au</seg></w>-<w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem/mc">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="4.7">d</w>’<w n="4.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="4.9">Cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="4.10">d</w>’<w n="4.11" punct="pi:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>l<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>m<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg>s</rhyme></w> ?</l>
						<l n="5" num="1.5" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="5.1">C<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg></w> <w n="5.2">g<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st</w> <w n="5.3" punct="vg:4">P<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">om</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="5.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">A</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="355" place="6" mp="M">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg" mp="F">e</seg></w>, <w n="5.5" punct="pt:10">C<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>s<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>r</rhyme></w>.</l>
						<l n="6" num="1.6" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="6.2"><seg phoneme="y" type="vs" value="1" rule="391" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="6.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="4" caesura="1">eau</seg></w><caesura></caesura> <w n="6.4">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="M">om</seg>ph<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="6.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="P">u</seg>r</w> <w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="6.7" punct="vg:10">ch<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg>r</rhyme></w>,</l>
						<l n="7" num="1.7" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="7.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-6" place="2">e</seg>r</w> <w n="7.3">c<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg></w> <w n="7.4">g<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>st</w><caesura></caesura> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="7.7">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="7.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8">en</seg></w> <w n="7.9" punct="pv:10">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>sc<rhyme label="a" id="4" gender="f" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="8" num="1.8" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="8.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" caesura="1">o</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</w> <w n="8.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="C">eu</seg>r</w> <w n="8.8" punct="vg:10">c<rhyme label="a" id="4" gender="f" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="9" num="1.9" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="9.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="Lc">a</seg>l</w>-<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M/mc">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M/mc">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="9.3"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="C">i</seg>ls</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="9.5" punct="pv:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8" mp="M">ê</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<rhyme label="a" id="5" gender="m" type="a" stanza="4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pv">on</seg>t</rhyme></w> ;</l>
						<l n="10" num="1.10" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="10.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg></w><caesura></caesura> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="10.6" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<rhyme label="a" id="5" gender="m" type="e" stanza="4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">on</seg>t</rhyme></w>,</l>
						<l n="11" num="1.11" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="11.1">S</w>’<w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="11.3">n</w>’<w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="11.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="11.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="11.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="11.10" punct="pv:10"><seg phoneme="e" type="vs" value="1" rule="353" place="9" mp="M">e</seg>ff<rhyme label="a" id="6" gender="f" type="a" stanza="5"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg>nt</rhyme></w> ;</l>
						<l n="12" num="1.12" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="12.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w> <w n="12.3" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg" caesura="1">ou</seg>t</w>,<caesura></caesura> <w n="12.4">qu<seg phoneme="wa" type="vs" value="1" rule="424" place="5">oy</seg></w> <w n="12.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.7">H<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>s</w> <w n="12.8" punct="vg:10">f<rhyme label="a" id="6" gender="f" type="e" stanza="5"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>nt</rhyme></w>,</l>
						<l n="13" num="1.13" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="13.1">Qu</w>’<w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="13.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fp">e</seg></w>-<w n="13.4">t</w>-<w n="13.5" punct="pi:4"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pi" caesura="1">i</seg>l</w> ?<caesura></caesura> <w n="13.6">qu</w>’<w n="13.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="13.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="13.9">l<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="13.10"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="13.11">v<rhyme label="a" id="7" gender="m" type="a" stanza="6"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10">ain</seg></rhyme></w></l>
						<l n="14" num="1.14" lm="10" met="4+6"><space unit="char" quantity="4"></space><w n="14.1" punct="vg:1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg>t</w>, <w n="14.2">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>st</w> <w n="14.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="14.4" punct="vg:4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>rd</w>,<caesura></caesura> <w n="14.5">c<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg></w> <w n="14.6">g<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>st</w> <w n="14.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="14.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="14.9" punct="pi:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>fr<rhyme label="a" id="7" gender="m" type="e" stanza="6"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10" punct="pi">ain</seg></rhyme></w> ?</l>
					</lg>
				</div></body></text></TEI>