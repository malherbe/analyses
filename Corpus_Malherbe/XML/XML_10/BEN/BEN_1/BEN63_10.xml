<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><head type="main_subpart">ÉPIGRAMMES.</head><div type="poem" key="BEN63" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababccdede)">
						<lg n="1" type="dizain" rhyme="ababccdede">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">E</seg></w> <w n="1.2">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="1.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>ge<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1">M</w>’<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="2.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="2.5" punct="vg:8">v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>s</rhyme></w>,</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">m</w>’<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="3.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>t</w> <w n="3.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="3.7">d</w>’<w n="3.8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ff<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="4.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="4.5" punct="pt:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rr<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>x</rhyme></w>.</l>
							<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="5.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="5.4">cr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="5.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.7" punct="vg:8">pl<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="6" num="1.6" lm="8" met="8"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="6.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="6.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="7" num="1.7" lm="8" met="8"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2">f<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.4">m<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="7.5" punct="pv:8">t<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pv">ou</seg>r</rhyme></w> ;</l>
							<l n="8" num="1.8" lm="8" met="8"><w n="8.1" punct="vg:2">Ph<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>, <w n="8.2">n</w>’<w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="8.4">s<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="8.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="8.7" punct="vg:8">p<rhyme label="e" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
							<l n="9" num="1.9" lm="8" met="8"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.3">n</w>’<w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>t</w> <w n="9.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg></w> <w n="9.7">d</w>’<w n="9.8" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></w>,</l>
							<l n="10" num="1.10" lm="8" met="8"><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="10.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">sç<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="10.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.6" punct="pt:8">h<rhyme label="e" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						</lg>
					</div></body></text></TEI>