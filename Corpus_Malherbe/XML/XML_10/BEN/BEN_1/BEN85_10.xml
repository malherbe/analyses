<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN85" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abab abab ccd eed">
					<head type="form">SONNET EN BOUTS-RIMEZ.</head>
					<lg n="1" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">E</seg></w> <w n="1.2">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="1.5">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="1.6">qu</w>’<w n="1.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="1.8">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">aî</seg>t</w> <w n="1.9"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> ── <w n="1.10" punct="vg:12">J<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>r</rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="2.3">m<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="2.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="Lc">u</seg>squ</w>’<w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="C">au</seg></w> ── <w n="2.6" punct="vg:12">ph<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rm<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>p<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L</w>’<w n="3.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="3.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="3.6" punct="vg:6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="6" punct="vg" caesura="1">en</seg>s</w>,<caesura></caesura> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.9">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="9">oin</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> ── <w n="3.10">fr<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12">e</seg>r</rhyme></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="4.4">sç<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="4.5">qu</w>’<w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">A</seg>rn<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>d</w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="4.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> ── <w n="4.9" punct="pt:12">N<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>c<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="5.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3" punct="vg:3">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3" punct="vg">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="5.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="5.5">s</w>’<w n="5.6" punct="pv:6">h<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pv" caesura="1">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ;<caesura></caesura> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="Lp">a</seg></w>-<w n="5.8">t</w>’<w n="5.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="5.10">d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="5.11"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> ── <hi rend="ital"><w n="5.12" punct="pi:12">P<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pi">e</seg>r</rhyme></w></hi> ?</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="6.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="6.5" punct="pt:6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="pt" caesura="1">ai</seg>t</w>.<caesura></caesura> <w n="6.6">L</w>’<w n="6.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>c<seg phoneme="ɥi" type="vs" value="1" rule="462" place="8" mp="M">u</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> ── <w n="6.8" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>c<rhyme label="b" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">M<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="7.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.5">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>x</w><caesura></caesura> <w n="7.6">c<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> ── <w n="7.8" punct="vg:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>sp<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>t<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">p<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="8.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="8.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>bs<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> ── <w n="8.8" punct="vg:12">b<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>ss<rhyme label="b" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">am</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="9.3">tr<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="6" caesura="1">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="9.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="9.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ── <w n="9.7" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rt<rhyme label="c" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="vg">e</seg>l</rhyme></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">Br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="10.4">v<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>t</w> <w n="10.5">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.7">m<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> ── <w n="10.9" punct="pv:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rt<rhyme label="c" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="pv">e</seg>l</rhyme></w> ;</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="11.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="11.4" punct="vg:6">tr<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="11.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="11.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> ── <w n="11.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ff<rhyme label="d" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="4" rhyme="eed">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="12.3">s</w>’<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="12.7" punct="vg:6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="6" punct="vg" caesura="1">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="12.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="12.9"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t</w> <w n="12.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> ── <w n="12.12" punct="pv:12">v<rhyme label="e" id="6" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pv">e</seg>rs</rhyme></w> ;</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2">s</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M/mp">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="Lp">i</seg>t</w>-<w n="13.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="13.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" caesura="1">ain</seg>cr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="13.8">r<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="13.9">l</w>’── <w n="13.10" punct="pi:12"><seg phoneme="y" type="vs" value="1" rule="453" place="10" mp="M">U</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<rhyme label="e" id="6" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pi">e</seg>rs</rhyme></w> ?</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="14.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="14.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="14.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>l</w> <w n="14.6">R<seg phoneme="wa" type="vs" value="1" rule="424" place="8">oy</seg></w> <w n="14.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="14.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> ── <w n="14.9" punct="pt:12">f<rhyme label="d" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>