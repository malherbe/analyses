<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><head type="sub_part">(1867-1870)</head><div type="poem" key="DUC85" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="8(abab)">
					<head type="main">Le Prince Pierre est acquitté !!!…</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="1.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="1.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="1.6" punct="vg:8">z<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="2.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="2.5" punct="pv:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>pp<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pv">é</seg></rhyme></w> ;</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="3.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="3.5" punct="pi:8">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">Pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="4.5" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cqu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>tt<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="5.2">qu<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>s</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.6" punct="vg:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>v<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="6.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="6.5" punct="pt:8">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>lp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="7.2" punct="tc:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="dp ti in">i</seg>t</w> : — « <w n="7.3">V<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="7.4" punct="pi:6">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pi ti">ez</seg></w> ? — <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6" punct="pi:8">Pr<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">Pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="8.5" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cqu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>tt<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></w> !</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8">— « <w n="9.1" punct="pi:1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pi in">a</seg>h</w> ? » « <w n="9.2" punct="pe:2">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="pe in">i</seg></w> ! » « <w n="9.3" punct="pi:4">Vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="4" punct="pi in">en</seg>t</w> ? » « <w n="9.4">C</w>’<w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="9.6" punct="pe:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">im</seg>p<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>ss<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> ! »</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ct</w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="10.5" punct="pt:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>sc<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></w>.</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="11.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="11.6" punct="vg:8">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">Pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="12.5" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cqu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>tt<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></w> !</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1" punct="vg:3">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="13.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="13.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.6" punct="vg:8">v<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">c</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="14.5">bru<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="14.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ccr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1" punct="dp:1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" punct="dp">e</seg></w> : <w n="15.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg></w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="15.7">l</w>’<w n="15.8" punct="ps:8"><seg phoneme="e" type="vs" value="1" rule="354" place="7">e</seg>x<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></w>…</l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">Pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">n</w>’<w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="16.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>c</w> <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="16.6" punct="pi:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cqu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>tt<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pi">é</seg></rhyme></w> ?</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="17.2" punct="tc:3"><seg phoneme="e" type="vs" value="1" rule="354" place="2">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg ti">i</seg>l</w>, — <w n="17.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="17.5">ch<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="17.7" punct="tc:8">vr<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg ti">e</seg></rhyme></w>, —</l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="18.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="18.3" punct="pv:8">c<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>lp<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pv">é</seg></rhyme></w> ;</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">H<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rs</w> <w n="19.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="19.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>p</w> <w n="19.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="19.5">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="19.6">l</w>’<w n="19.7" punct="pi:8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>vr<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi ps">e</seg></rhyme></w> ?…</l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">Pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">n</w>’<w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="20.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>c</w> <w n="20.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="20.6" punct="pi:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cqu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>tt<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pi">é</seg></rhyme></w> ?</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="21.2" punct="vg:2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="21.3">c</w>’<w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="21.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="21.6">bru<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="21.7" punct="vg:8">r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="22.2">bru<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="22.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="22.4" punct="pv:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>t<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pv">é</seg></rhyme></w> ;</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="23.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>br<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="23.4" punct="pv:8">c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>rc<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="24.3">Pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="24.5" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cqu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>tt<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></w> !</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="abab">
						<l n="25" num="7.1" lm="8" met="8"><w n="25.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="25.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="25.4">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="25.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="25.6" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="26" num="7.2" lm="8" met="8"><w n="26.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="26.2">h<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="26.3">l</w>’<w n="26.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r</w> <w n="26.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="26.6" punct="vg:8">l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></w>,</l>
						<l n="27" num="7.3" lm="8" met="8"><w n="27.1">Mi<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="27.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3">R<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt</w> <w n="27.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="27.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="27.6" punct="vg:8">ge<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>, <ref type="noteAnchor" target="(1)">(1)</ref></l>
						<l n="28" num="7.4" lm="8" met="8"><w n="28.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="28.3">Pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="28.5" punct="pi:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cqu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>tt<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pi">é</seg></rhyme></w> ?</l>
					</lg>
					<lg n="8" type="quatrain" rhyme="abab">
						<l n="29" num="8.1" lm="8" met="8"><w n="29.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="29.3" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>cqu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="pe ps">é</seg></w> !… <w n="29.4" punct="vg:6">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg">ai</seg>s</w>, <w n="29.5">j</w>’<w n="29.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>br<rhyme label="a" id="15" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="30" num="8.2" lm="8" met="8"><w n="30.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">b<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="30.3" punct="ps:8">pr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="16" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="ps">é</seg></rhyme></w>…</l>
						<l n="31" num="8.3" lm="8" met="8"><w n="31.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="31.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="31.3" punct="vg:3">tr<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="31.4" punct="pe:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pe ps">é</seg></w> !… <w n="31.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="31.6">s<rhyme label="a" id="15" gender="f" type="e" part="I"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</rhyme></w>-<w n="31.7" punct="pi:8"><rhyme label="a" id="15" gender="f" type="e" part="F">j<seg phoneme="ə" type="ef" value="1" rule="e-1" place="9" punct="pi">e</seg></rhyme></w> ?</l>
						<l n="32" num="8.4" lm="8" met="8"><w n="32.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="32.2">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="32.3">Pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="32.5" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cqu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>tt<rhyme label="b" id="16" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1870">30 Mars 1870</date>.
						</dateline>
						<note type="footnote" id="1">
							Tandis que M. Rochefort, député de la Seine, était sévèrement <lb></lb>
							détenu en prison pour un simple délit de presse, le tueur <lb></lb>
							acquitté de Victor Noir recevait et rendait des visites à <lb></lb>
							l’occasion de son scandaleux acquittement.</note>
					</closer>
				</div></body></text></TEI>