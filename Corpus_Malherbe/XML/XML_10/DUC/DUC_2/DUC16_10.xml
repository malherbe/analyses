<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC16" modus="sp" lm_max="8" metProfile="6, (8)" form="suite périodique" schema="6(aabccb)">
				<head type="main">Le Ménestrel</head>
				<lg n="1" type="sizain" rhyme="aabccb">
					<l n="1" num="1.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="1.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>l</w> <w n="1.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="1.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.6" punct="vg:6">t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="2.3">M<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.4">l<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="3" num="1.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">pl<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="3.5" punct="pt:6">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>p<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="pt">e</seg>l</rhyme></w>.</l>
					<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="4.3" punct="vg:4">g<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" punct="vg">ai</seg></w>, <w n="4.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5" punct="vg:6">ch<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="5" num="1.5" lm="6" met="6"><space unit="char" quantity="4"></space><w n="5.1" punct="vg:1">L<seg phoneme="ɛ" type="vs" value="1" rule="306" place="1" punct="vg">ai</seg></w>, <w n="5.2" punct="vg:4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="5.3" punct="vg:6">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>rv<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="6" num="1.6" lm="8"><w n="6.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="6.2">d</w>’<w n="6.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="6.5" punct="pt:8">M<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>str<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</rhyme></w>.</l>
				</lg>
				<lg n="2" type="sizain" rhyme="aabccb">
					<l n="7" num="2.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.4" punct="vg:6">m<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="8" num="2.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="8.2" punct="vg:3">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>r</w>, <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.5" punct="vg:6">r<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="9" num="2.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="9.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="9.4" punct="pv:6">C<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>st<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="pv">e</seg>l</rhyme></w> ;</l>
					<l n="10" num="2.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="10.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="10.4">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<rhyme label="c" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="11" num="2.5" lm="6" met="6"><space unit="char" quantity="4"></space><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="11.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.5" punct="pv:6">p<rhyme label="c" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="6">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="12" num="2.6" lm="8"><w n="12.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="12.2">d</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="12.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="12.5" punct="pt:8">M<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>str<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</rhyme></w>.</l>
				</lg>
				<lg n="3" type="sizain" rhyme="aabccb">
					<l n="13" num="3.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="13.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="13.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.5" punct="vg:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="14" num="3.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.3" punct="vg:6">p<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>c<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="15" num="3.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="15.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="2">an</seg>t</w> <w n="15.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="15.3" punct="vg:6">D<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg">e</seg>l</rhyme></w>,</l>
					<l n="16" num="3.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="16.4">s</w>’<w n="16.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<rhyme label="c" id="9" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="17" num="3.5" lm="6" met="6"><space unit="char" quantity="4"></space><w n="17.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="17.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>x</w> <w n="17.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<rhyme label="c" id="9" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="18" num="3.6" lm="8"><w n="18.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="18.2">d</w>’<w n="18.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="18.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="18.5" punct="pt:8">M<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>str<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</rhyme></w>.</l>
				</lg>
				<lg n="4" type="sizain" rhyme="aabccb">
					<l n="19" num="4.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="19.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2">pr<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="19.5" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tr<rhyme label="a" id="10" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="469" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="20" num="4.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">c<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="20.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="20.4" punct="pv:6">v<rhyme label="a" id="10" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="21" num="4.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="21.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="21.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt<rhyme label="b" id="11" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</rhyme></w></l>
					<l n="22" num="4.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="22.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">gr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="22.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="22.4" punct="vg:6">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ct<rhyme label="c" id="12" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="23" num="4.5" lm="6" met="6"><space unit="char" quantity="4"></space><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">j<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg>s</w> <w n="23.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="23.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="23.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.6" punct="vg:6">gl<rhyme label="c" id="12" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="24" num="4.6" lm="8"><w n="24.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="24.2">d</w>’<w n="24.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="24.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="24.5" punct="pt:8">M<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>str<rhyme label="b" id="11" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</rhyme></w>.</l>
				</lg>
				<lg n="5" type="sizain" rhyme="aabccb">
					<l n="25" num="5.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="25.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ</w>’<w n="25.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="25.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="25.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="25.5">l</w>’<w n="25.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="25.7" punct="pv:6">m<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="26" num="5.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="26.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="26.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="26.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="26.4" punct="vg:6">h<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="27" num="5.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="27.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3">tr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="27.4" punct="vg:6">cr<seg phoneme="y" type="vs" value="1" rule="454" place="5">u</seg><rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg">e</seg>l</rhyme></w>,</l>
					<l n="28" num="5.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="28.1" punct="vg:2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="28.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="28.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="28.4" punct="vg:6">pr<rhyme label="c" id="15" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="29" num="5.5" lm="6" met="6"><space unit="char" quantity="4"></space><w n="29.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r</w> <w n="29.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="29.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<rhyme label="c" id="15" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="30" num="5.6" lm="8"><w n="30.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="30.2">d</w>’<w n="30.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="30.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="30.5" punct="pt:8">M<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>str<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</rhyme></w>.</l>
				</lg>
				<lg n="6" type="sizain" rhyme="aabccb">
					<l n="31" num="6.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="31.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="31.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="31.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="31.5" punct="vg:6">fr<rhyme label="a" id="16" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="32" num="6.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="32.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="32.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="32.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="32.4" punct="vg:6">r<rhyme label="a" id="16" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="33" num="6.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="33.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="33.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="33.3" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rn<rhyme label="b" id="17" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg">e</seg>l</rhyme></w>,</l>
					<l n="34" num="6.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="34.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="34.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="34.3">r<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>l</w> <w n="34.4" punct="vg:6">ch<rhyme label="c" id="18" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="35" num="6.5" lm="6" met="6"><space unit="char" quantity="4"></space><w n="35.1" punct="vg:1">L<seg phoneme="ɛ" type="vs" value="1" rule="306" place="1" punct="vg">ai</seg></w>, <w n="35.2" punct="vg:4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="35.3" punct="pv:6">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>rv<rhyme label="c" id="18" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="36" num="6.6" lm="8"><w n="36.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="36.2">d</w>’<w n="36.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="36.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="36.5" punct="pt:8">M<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>str<rhyme label="b" id="17" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1847">1847</date>
					</dateline>
				</closer>
			</div></body></text></TEI>