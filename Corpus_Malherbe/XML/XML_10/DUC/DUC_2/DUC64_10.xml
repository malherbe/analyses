<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC64" modus="sp" lm_max="8" metProfile="6, 4, 8" form="suite périodique" schema="5(aabbcddc)">
				<head type="main">Marie</head>
				<lg n="1" type="huitain" rhyme="aabbcddc">
					<l n="1" num="1.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="1.1">J<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="1.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.4" punct="vg:6">M<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="2.3" punct="vg:4">ch<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="3.3">d</w>’<w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="3.5" punct="vg:6">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>qu<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg">e</seg>t</rhyme></w>,</l>
					<l n="4" num="1.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="4.1">Fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="4.3" punct="vg:4">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>qu<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" punct="vg">e</seg>t</rhyme></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="5.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="5.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.6" punct="pv:8">f<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="6.3">tr<seg phoneme="o" type="vs" value="1" rule="433" place="3">o</seg>p</w> <w n="6.4" punct="pe:4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe ti">a</seg>rd</w> ! — <w n="6.5">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="6.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="6.7" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>pl<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="vg">e</seg>ts</rhyme></w>,</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1" punct="vg:2">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2" punct="vg">e</seg>ts</w>, <w n="7.2" punct="vg:4">h<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</w>, <w n="7.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="7.4" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>pl<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="vg">e</seg>ts</rhyme></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="8.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="8.4">v<seg phoneme="ø" type="vs" value="1" rule="248" place="5">œu</seg>x</w> <w n="8.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="8.6" punct="pt:8">p<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="huitain" rhyme="aabbcddc">
					<l n="9" num="2.1" lm="8" met="8"><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="9.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="9.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="9.6">j<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</rhyme></w></l>
					<l n="10" num="2.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="10.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="10.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg>f<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>ts</rhyme></w></l>
					<l n="11" num="2.3" lm="8" met="8"><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="11.6" punct="pt:8">v<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					<l n="12" num="2.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="12.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="12.2">l</w>’<w n="12.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>v<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
					<l n="13" num="2.5" lm="8" met="8"><w n="13.1">Qu</w>’<w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="13.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="13.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="13.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="13.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<rhyme label="c" id="7" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</rhyme></w></l>
					<l n="14" num="2.6" lm="8" met="8"><w n="14.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="14.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="14.3">r<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="14.4" punct="vg:6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg>s</w>, <w n="14.5" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>gn<rhyme label="d" id="8" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="15" num="2.7" lm="8" met="8"><w n="15.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="15.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="15.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.6" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<rhyme label="d" id="8" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="16" num="2.8" lm="8" met="8"><w n="16.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="16.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="16.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5">s<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="16.7">d</w>’<w n="16.8" punct="pe:8"><rhyme label="c" id="7" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pe">o</seg>r</rhyme></w> !</l>
				</lg>
				<lg n="3" type="huitain" rhyme="aabbcddc">
					<l n="17" num="3.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="17.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">An</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="17.2">s<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="17.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="17.4">j<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="18" num="3.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="18.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>pl<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></w></l>
					<l n="19" num="3.3" lm="8" met="8"><w n="19.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">f<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="19.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="19.6" punct="vg:8">b<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></rhyme></w>,</l>
					<l n="20" num="3.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="20.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="20.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>x</w> <w n="20.4" punct="pt:4">n<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pt">i</seg>d</rhyme></w>.</l>
					<l n="21" num="3.5" lm="8" met="8"><w n="21.1">S<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="21.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>x</w> <w n="21.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="21.5" punct="tc:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="dp ti">i</seg>t</w> : — <w n="21.6" punct="pe:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">E</seg>sp<rhyme label="c" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="22" num="3.6" lm="8" met="8"><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">b<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="22.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="22.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="22.5">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6">en</seg>t</w> <w n="22.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="22.7" punct="pv:8">s<rhyme label="d" id="12" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="406" place="8" punct="pv">eu</seg>il</rhyme></w> ;</l>
					<l n="23" num="3.7" lm="8" met="8"><w n="23.1">S<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="23.2">l</w>’<w n="23.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>r</w>, <w n="23.4">s<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="23.5">l</w>’<w n="23.6" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rg<rhyme label="d" id="12" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="344" place="8" punct="vg">ue</seg>il</rhyme></w>,</l>
					<l n="24" num="3.8" lm="8" met="8"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="24.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="24.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="24.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="24.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="24.8" punct="pe:8">t<rhyme label="c" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
				</lg>
				<lg n="4" type="huitain" rhyme="aabbcddc">
					<l n="25" num="4.1" lm="8" met="8"><w n="25.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="25.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="25.4" punct="vg:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>j<rhyme label="a" id="13" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>rs</rhyme></w>,</l>
					<l n="26" num="4.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="26.1">D<seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="26.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="26.3" punct="vg:4">j<rhyme label="a" id="13" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>rs</rhyme></w>,</l>
					<l n="27" num="4.3" lm="8" met="8"><w n="27.1">D</w>’<w n="27.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="27.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="27.4" punct="vg:5">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="27.5"><seg phoneme="o" type="vs" value="1" rule="415" place="6">ô</seg></w> <w n="27.6" punct="pe:8">M<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="b" id="14" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					<l n="28" num="4.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="28.1">R<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="28.2" punct="vg:4">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="3">eu</seg>r<rhyme label="b" id="14" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
					<l n="29" num="4.5" lm="8" met="8"><w n="29.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="29.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="29.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>j<seg phoneme="u" type="vs" value="1" rule="d-2" place="5">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>r</w> <w n="29.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="29.5" punct="vg:8">y<rhyme label="c" id="15" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></w>,</l>
					<l n="30" num="4.6" lm="8" met="8"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="30.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="30.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="30.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="30.5">pr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<rhyme label="d" id="16" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="31" num="4.7" lm="8" met="8"><w n="31.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>xh<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="31.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="5">um</seg></w> <w n="31.4">d</w>’<w n="31.5" punct="pv:8"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>c<rhyme label="d" id="16" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="32" num="4.8" lm="8" met="8"><w n="32.1">Fl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="32.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="32.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="32.4" punct="vg:4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="32.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="32.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="32.7" punct="pe:8">ci<rhyme label="c" id="15" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg>x</rhyme></w> !</l>
				</lg>
				<lg n="5" type="huitain" rhyme="aabbcddc">
					<l n="33" num="5.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="33.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="33.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w>-<w n="33.3">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="3">e</seg></w> <w n="33.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.5" punct="pi:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<rhyme label="a" id="17" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pi">e</seg></rhyme></w> ?</l>
					<l n="34" num="5.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="34.1" punct="vg:1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="34.2">qu</w>’<w n="34.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="34.4" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<rhyme label="a" id="17" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
					<l n="35" num="5.3" lm="8" met="8"><w n="35.1" punct="vg:1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="35.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="35.3">l</w>’<w n="35.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="35.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="35.6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="35.7">d</w>’<w n="35.8" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<rhyme label="b" id="18" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></w>,</l>
					<l n="36" num="5.4" lm="4" met="4"><space unit="char" quantity="8"></space><w n="36.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ls</w> <w n="36.2">v<seg phoneme="ø" type="vs" value="1" rule="248" place="2">œu</seg>x</w> <w n="36.3" punct="pi:4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rm<rhyme label="b" id="18" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pi">er</seg></rhyme></w> ?</l>
					<l n="37" num="5.5" lm="8" met="8"><w n="37.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="37.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="37.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="37.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="37.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="37.6" punct="vg:8">b<rhyme label="c" id="19" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="38" num="5.6" lm="8" met="8"><w n="38.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="38.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <foreign lang="lat"><w n="38.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg><rhyme label="d" id="20" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></w></foreign>,</l>
					<l n="39" num="5.7" lm="8" met="8"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="39.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="39.3" punct="tc:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="dp ti">i</seg>s</w> : — <foreign lang="lat"><w n="39.4" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="39.5">M<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="d" id="20" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg></rhyme></w></foreign> !</l>
					<l n="40" num="5.8" lm="8" met="8"><w n="40.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="40.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="40.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="40.4">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="40.5" punct="pt:8">P<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>tr<rhyme label="c" id="19" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>