<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC29" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="5(abab)">
				<head type="main">Lydie</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J</w> <w n="1.2"><seg phoneme="ə" type="ef" value="1" rule="e-24" place="1">e</seg></w> <w n="1.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="1.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.7"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="2.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="2.3" punct="vg:4">l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="2.6" punct="pv:8">y<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pv">eu</seg>x</rhyme></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">s</w>’<w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="3.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="3.6">l</w>’<w n="3.7">h<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>mm<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="4.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="4.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="4.5" punct="pt:8">j<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="5.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="5.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.7" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<rhyme label="a" id="10" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">im</seg>pl<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="6.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.5" punct="vg:8">j<rhyme label="b" id="3" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="7.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="7.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5">f<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="7.6">l</w>’<w n="7.7" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>m<rhyme label="a" id="10" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">D</w>’<w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="8.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.7">p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="8.8" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="b" id="3" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pv">ou</seg>r</rhyme></w> ;</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="9.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.6" punct="vg:8">v<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>gl<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="10.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="10.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.5" punct="pe:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7">im</seg>p<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pe">u</seg>r</rhyme></w> !</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2">cl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="11.3">m</w>’<w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="11.5" punct="pv:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="12.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="12.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w>-<w n="12.5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="12.6">l</w>’<w n="12.7" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>z<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pe">u</seg>r</rhyme></w> !</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="13.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>s</w> <w n="13.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="13.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="13.6">g<rhyme label="a" id="6" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="14.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="14.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="14.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="14.6" punct="pv:8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>br<rhyme label="b" id="7" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg></rhyme></w> ;</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">S</w>’<w n="15.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="15.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="15.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="15.5">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="15.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="15.8" punct="vg:8">s<rhyme label="a" id="6" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="16.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.3">m</w>’<w n="16.4" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="7" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg></rhyme></w> !</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2" punct="vg:4">r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="17.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.4" punct="vg:8">L<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>d<rhyme label="a" id="8" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="18.3">l</w>’<w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="18.5">R<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ph<seg phoneme="a" type="vs" value="1" rule="343" place="7">a</seg><rhyme label="b" id="9" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="414" place="8">ë</seg>l</rhyme></w></l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="19.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="19.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="19.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rts</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="19.6">T<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>b<rhyme label="a" id="8" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="20.2" punct="vg:2">v<seg phoneme="i" type="vs" value="1" rule="482" place="2" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="20.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="20.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="20.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="20.7" punct="pt:8">fi<rhyme label="b" id="9" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</rhyme></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1853">1853</date>
					</dateline>
				</closer>
			</div></body></text></TEI>