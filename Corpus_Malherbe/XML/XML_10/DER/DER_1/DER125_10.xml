<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER125" modus="cp" lm_max="12" metProfile="8, 6, 6+6" form="suite périodique" schema="4(abba)">
				<head type="number">CXXV</head>
				<opener>
					<salute>A Lucien Dubech.</salute>
				</opener>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="8" met="8"><space quantity="12" unit="char"></space><w n="1.1">L</w>’<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>c</w> <w n="1.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6">b<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="6" met="6"><space quantity="20" unit="char"></space><w n="2.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="y" type="vs" value="1" rule="457" place="2">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="2.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="2.5" punct="pv:6">s<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pv">oi</seg>r</rhyme></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><space quantity="12" unit="char"></space><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2">r<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="3.3">b<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="3.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg></w> <w n="3.6">n<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</rhyme></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="4.3">gl<seg phoneme="i" type="vs" value="1" rule="493" place="3" mp="M">y</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="4.4">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="4.8">l</w>’<w n="4.9" punct="pt:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>b<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abba">
					<l n="5" num="2.1" lm="8" met="8"><space quantity="12" unit="char"></space><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="5.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="5.4">s</w>’<w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="5.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="5.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg>s</rhyme></w>,</l>
					<l n="6" num="2.2" lm="6" met="6"><space quantity="20" unit="char"></space><w n="6.1" punct="vg:2">R<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">er</seg>s</w>, <w n="6.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="6.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="6.4" punct="pv:6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rr<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg>s</rhyme></w> ;</l>
					<l n="7" num="2.3" lm="8" met="8"><space quantity="12" unit="char"></space><w n="7.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="7.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="7.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg></w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="7.6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>cr<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="8.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="8.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg>s</w> <w n="8.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rts</w><caesura></caesura> <w n="8.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="8.8">f<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.9"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="8.10" punct="pt:12">br<seg phoneme="y" type="vs" value="1" rule="445" place="11" mp="M">û</seg>l<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg>s</rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abba">
					<l n="9" num="3.1" lm="8" met="8"><space quantity="8" unit="char"></space><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="9.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w>-<w n="9.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="9.4">qu</w>’<w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="9.6">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="9.7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>x</w> <w n="9.8">d<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="10" num="3.2" lm="6" met="6"><space quantity="20" unit="char"></space><w n="10.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="10.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="10.4">j<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</rhyme></w></l>
					<l n="11" num="3.3" lm="8" met="8"><space quantity="12" unit="char"></space><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="11.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</rhyme></w></l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="12.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="12.4"><seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="12.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="12.8" punct="vg:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rd<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></w>,</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abba">
					<l n="13" num="4.1" lm="8" met="8"><space quantity="12" unit="char"></space><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="13.2">qu</w>’<w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</rhyme></w></l>
					<l n="14" num="4.2" lm="6" met="6"><space quantity="20" unit="char"></space><w n="14.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="14.4">br<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></w></l>
					<l n="15" num="4.3" lm="8" met="8"><space quantity="12" unit="char"></space><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="15.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="15.6">l</w>’<w n="15.7"><seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="15.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="15.9">bl<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="16.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="16.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="16.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="16.5">t</w>’<w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="16.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="16.8" punct="pi:12">s<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pi">oi</seg>r</rhyme></w> ?</l>
				</lg>
			</div></body></text></TEI>