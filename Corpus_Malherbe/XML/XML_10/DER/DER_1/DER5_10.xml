<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" rhyme="none" key="DER5" modus="cp" lm_max="14" metProfile="8, 6+8, (4+6)">
				<head type="number">V</head>
				<lg n="1">
					<l n="1" num="1.1" lm="14" met="6+8"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.2">j</w>’<w n="1.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s</w>, <w n="1.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rt</w><caesura></caesura> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>ssi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="1.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="12" mp="C">u</seg></w> <w n="1.9" punct="vg:14">v<seg phoneme="wa" type="vs" value="1" rule="440" place="13" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="14">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="15" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><space quantity="12" unit="char"></space><w n="2.1">H<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="2.3">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="2.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="2.5" punct="vg:8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="3" num="1.3" lm="14" met="6+8"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="3.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>lc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="3.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="3.7">gl<seg phoneme="i" type="vs" value="1" rule="493" place="9" mp="M">y</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="3.8"><seg phoneme="o" type="vs" value="1" rule="318" place="12" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="13" mp="M">o</seg>mn<seg phoneme="a" type="vs" value="1" rule="340" place="14">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="15" mp="F">e</seg>s</w></l>
					<l n="4" num="1.4" lm="8" met="8"><space quantity="12" unit="char"></space><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="4.4" punct="pt:8">f<seg phoneme="œ" type="vs" value="1" rule="406" place="7">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					<l n="5" num="1.5" lm="10" met="4+6" met_alone="True"><space quantity="8" unit="char"></space><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="5.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="5.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="5.7">l</w>’<w n="5.8" punct="vg:10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><space quantity="12" unit="char"></space><w n="6.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="6.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="6.5">p<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</w></l>
					<l n="7" num="1.7" lm="8" met="8"><space quantity="12" unit="char"></space><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="7.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">cr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.4">d</w>’<w n="7.5" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ct<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="8" num="1.8" lm="8" met="8"><space quantity="12" unit="char"></space><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="8.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="8.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="9" num="1.9" lm="8" met="8"><space quantity="12" unit="char"></space><w n="9.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="d-2" place="2">ou</seg><seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="9.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="9.5">d</w>’<w n="9.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>r</w>,</l>
					<l n="10" num="1.10" lm="14" met="6+8"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2" punct="vg:2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" punct="vg">e</seg></w>, <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="10.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="10.5" punct="vg:6">p<seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467" place="6" punct="vg" caesura="1">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="10.7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>c<seg phoneme="u" type="vs" value="1" rule="d-2" place="9" mp="M">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="10.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="10.9" punct="vg:14"><seg phoneme="e" type="vs" value="1" rule="409" place="12" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="13" mp="M">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="14">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="15" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="11" num="1.11" lm="8" met="8"><space quantity="12" unit="char"></space><w n="11.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="11.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.6" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>