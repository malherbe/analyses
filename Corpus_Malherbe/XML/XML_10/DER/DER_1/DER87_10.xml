<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER87" modus="sm" lm_max="6" metProfile="6" form="suite périodique" schema="4(abab)">
				<head type="number">LXXXVII</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="6" met="6"><w n="1.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="1.4">vi<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="6" met="6">(<w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w>-<w n="2.3" punct="pi:3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="pi">u</seg></w> ? <w n="2.4">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w>-<w n="2.6" punct="pi:6">t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pi">u</seg></rhyme></w> ?)</l>
					<l n="3" num="1.3" lm="6" met="6"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="3.2">d<seg phoneme="i" type="vs" value="1" rule="467" place="2">î</seg>ni<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="3.4">l</w>’<hi rend="ital"><w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">Au</seg>b<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></hi></l>
					<l n="4" num="1.4" lm="6" met="6"><hi rend="ital"><w n="4.1" punct="pt:6">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="4.2">C<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">P<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>l<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pt">u</seg></rhyme></w></hi>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="6" met="6"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="5.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.4">b<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>c<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="6" num="2.2" lm="6" met="6"><w n="6.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="6.3">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="6.4" punct="pv:6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>gni<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pv">er</seg></rhyme></w> ;</l>
					<l n="7" num="2.3" lm="6" met="6"><w n="7.1">T<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="7.3" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>gl<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="8" num="2.4" lm="6" met="6"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="8.3" punct="pt:6">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="307" place="5">a</seg>ill<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pt">er</seg></rhyme></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="6" met="6"><w n="9.1">Bu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="9.2">s<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.5">m<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="307" place="6">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="10" num="3.2" lm="6" met="6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="10.4" punct="ps:6">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rr<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="ps">eau</seg>x</rhyme></w>…</l>
					<l n="11" num="3.3" lm="6" met="6"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="11.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="11.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.4">p<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="307" place="6">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="12" num="3.4" lm="6" met="6"><w n="12.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="12.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.4" punct="pt:6">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="433" place="6" punct="pt">o</seg>ps</rhyme></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="6" met="6"><w n="13.1">Gu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="13.3">t<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5" punct="vg:6">ch<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></w>,</l>
					<l n="14" num="4.2" lm="6" met="6"><w n="14.1">M<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="14.2">d</w>’<w n="14.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>cr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4" punct="ps:6"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ss<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="ps">é</seg></rhyme></w>…</l>
					<l n="15" num="4.3" lm="6" met="6"><w n="15.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="15.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="15.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="15.4">j<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></w></l>
					<l n="16" num="4.4" lm="6" met="6"><w n="16.1">Fl<seg phoneme="ø" type="vs" value="1" rule="405" place="1">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="16.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4" punct="pt:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pt">é</seg></rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>