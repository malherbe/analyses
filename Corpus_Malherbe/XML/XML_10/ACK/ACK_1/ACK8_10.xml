<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PREMIÈRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ACK">
					<name>
						<forename>Louise-Victorine</forename>
						<surname>ACKERMANN</surname>
					</name>
					<date from="1813" to="1890">1813-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>588 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ACK_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies Diverses</title>
						<author>Louise-Victorine Ackermann</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/louiseackermaNpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de L. Ackermann</title>
						<author>Louise-Victorine Ackermann</author>
						<imprint>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1862">1862</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ACK8" modus="cm" lm_max="12" metProfile="6+6" form="strophe unique" schema="1(aabccb)">
				<head type="number">VIII</head>
				<head type="main">La Jeunesse</head>
				<lg n="1" type="sizain" rhyme="aabccb">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Pr<seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="1.3">tr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rs</w><caesura></caesura> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.5">d</w>’<w n="1.6"><seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.7" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="2.2">J<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="M">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="2.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="2.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.7" punct="dp:12">Cl<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>p<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></w> :</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="3.2">p<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="3.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">im</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="3.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="3.7">tr<seg phoneme="o" type="vs" value="1" rule="433" place="8">o</seg>p</w> <w n="3.8">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>d</w> <w n="3.9">p<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="P">ou</seg>r</w> <w n="3.10">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="3.11" punct="pv:12">c<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pv">œu</seg>r</rhyme></w> ;</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="4.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2" mp="C">y</seg></w> <w n="4.3">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="4.5" punct="vg:6">j<seg phoneme="wa" type="vs" value="1" rule="440" place="5" mp="M">o</seg>y<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg" caesura="1">au</seg></w>,<caesura></caesura> <w n="4.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="4.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="4.8">f<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.9" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" mp="M">im</seg>pr<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>d<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="5.2">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="Lp">a</seg></w>-<w n="5.4">t</w>-<w n="5.5" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="5.6" punct="pe:6">h<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe" caesura="1">a</seg>s</w> !<caesura></caesura> <w n="5.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="5.9">c<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rd<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="6.3">n</w>’<w n="6.4"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="6.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="6.7" punct="vg:6">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6" punct="vg" caesura="1">en</seg></w>,<caesura></caesura> <w n="6.8">n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="6.9" punct="vg:9">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="6.10">n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="6.11" punct="pt:12">l<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>qu<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</rhyme></w>.</l>
				</lg>
			</div></body></text></TEI>