<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret de Crusoé</title>
				<title type="medium">Édition électronique</title>
				<author key="DAN">
					<name>
						<forename>Louis</forename>
						<surname>DANTIN</surname>
					</name>
					<date from="1865" to="1945">1865-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2468 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">DAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louisdantinlecofretdecrusoee.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
						<imprint>
							<publisher>ÉDITIONS ALBERT LÉVESQUE</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">CHANSON GRAVE</head><div type="poem" key="DAN7" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abba abba ccd dcd">
						<head type="main">Mosaïque ancienne</head>
						<lg n="1" rhyme="abba">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="1.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3" punct="vg:6">Th<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg></w>,<caesura></caesura> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="1.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="1.7" punct="vg:12">B<seg phoneme="i" type="vs" value="1" rule="493" place="11" mp="M">y</seg>z<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg></w> <w n="2.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="2.5">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="2.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="2.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="2.9">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg>gt</w> <w n="2.10" punct="vg:12"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="vg">an</seg>s</rhyme></w>,</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="3.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>gts</w> <w n="3.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>ts</w><caesura></caesura> <w n="3.5">l<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>str<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="3.7">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ts</rhyme></w></l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Ti<seg phoneme="ɛ" type="vs" value="1" rule="366" place="1">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="P">ou</seg>r</w> <w n="4.3">sc<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ptr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="4.4">l</w>’<w n="4.5" punct="vg:6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg" caesura="1">o</seg>r</w>,<caesura></caesura> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="4.7">gr<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="4.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="4.10" punct="pt:12">pu<seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="M">i</seg>ss<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
						<lg n="2" rhyme="abba">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">O</seg>r</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3" mp="M">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="5.4" punct="vg:6">Qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>s</w>,<caesura></caesura> <w n="5.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.7">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>c<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="6.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="6.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="341" place="6" caesura="1">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="6.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="6.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="6.8" punct="vg:12">fl<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>tt<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>ts</rhyme></w>,</l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="7.4" punct="vg:4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="7.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.6" punct="vg:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="7" mp="M">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="7.9" punct="vg:12">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11" mp="M">in</seg>t<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12" punct="vg">em</seg>ps</rhyme></w>,</l>
							<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="8.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="8.3">f<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="8.4">sc<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="8.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="C">eu</seg>r</w> <w n="8.6" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ll<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
						<lg n="3" rhyme="ccd">
							<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.3">s<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>il</w> <w n="9.4">r<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6" caesura="1">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="9.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="9.8">c<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>rsi<seg phoneme="e" type="vs" value="1" rule="347" place="11">er</seg>s</w> <w n="9.9">r<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>x</rhyme></w></l>
							<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="10.3">c<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>th<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="10.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="10.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="10.7">pr<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="10.8">d<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>x</rhyme></w></l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="11.2">ch<seg phoneme="œ" type="vs" value="1" rule="389" place="2">oeu</seg>rs</w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="4" mp="M">i</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="11.5">j<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>th<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="11.8" punct="pv:12">r<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						</lg>
						<lg n="4" rhyme="dcd">
							<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="12.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.4">s</w>’<w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="12.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="12.8">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="12.9" punct="vg:12">cl<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
							<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="C">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="13.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="13.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="13.6">c<seg phoneme="œ" type="vs" value="1" rule="389" place="6" caesura="1">oeu</seg>r</w><caesura></caesura> <w n="13.7">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="7" mp="M">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="13.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="P">ou</seg>r</w> <w n="13.9">s<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>l</w> <w n="13.10"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>p<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>x</rhyme></w></l>
							<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2" punct="vg:2">Chr<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>st</w>, <w n="14.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="14.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4" mp="C">i</seg></w> <w n="14.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="14.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="14.7"><seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" mp="M">o</seg>th<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><rhyme label="d" id="6" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></w>.</l>
						</lg>
					</div></body></text></TEI>