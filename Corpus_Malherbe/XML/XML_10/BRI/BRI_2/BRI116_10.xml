<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SIXIÈME</head><head type="sub_part">A NAPLE</head><div type="poem" key="BRI116" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="3(abab)">
					<head type="main">Le Chemin nouveau</head>
					<opener>
						<salute>À Monsieur Amédée Chéron</salute>
					</opener>
					<lg n="1" type="quatrain" rhyme="abab">
						 
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">AN</seg>S</w> <w n="1.2">l</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">E</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>bs<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rb<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="1.5">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="1.7" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ld<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="2.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="2.5" punct="vg:6">bru<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="2.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="2.8">bru<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>t</w> <w n="2.9">d</w>’<w n="2.10" punct="pt:12"><seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>g<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="pt">an</seg></rhyme></w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="3.3">m<seg phoneme="wa" type="vs" value="1" rule="421" place="3">oi</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="3.4" punct="vg:6">tr<seg phoneme="e" type="vs" value="1" rule="353" place="5" mp="M">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="307" place="6" punct="vg" caesura="1">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="3.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="3.6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="3.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="3.9">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>ll<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">d</w>’<w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="4.4"><seg phoneme="œ" type="vs" value="1" rule="286" place="3">œ</seg>il</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="4.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="4.8" punct="dp:12">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>lc<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="dp">an</seg></rhyme></w> :</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">V<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.2" punct="vg:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="383" place="5" mp="M">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg" caesura="1">ai</seg>t</w>,<caesura></caesura> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="5.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="5.6" punct="pv:12">h<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="M">eu</seg>r<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg></w> <w n="6.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="3">e</seg>d</w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="6.5" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="?" type="va" value="1" rule="162" place="6" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="6.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="6.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="6.8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg></w> <w n="6.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="6.10" punct="pt:12">f<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pt">e</seg>r</rhyme></w>.</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">R<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="7.3" punct="vg:4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>rs</w>, <w n="7.4">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>r</w> <w n="7.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>p<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="7.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>lf<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>r<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>ts</w> <w n="8.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="P">a</seg>r</w> <w n="8.4">M<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rc<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.7">l</w>’<w n="8.8" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>f<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="pt">e</seg>r</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="9.2" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="421" place="2">oi</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="Lp">ai</seg>s</w>-<w n="9.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="9.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="9.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.8">sph<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.9" punct="pi:12"><seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1" punct="vg:1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" punct="vg">oi</seg>s</w>, <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="10.5" punct="vg:6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5" mp="M">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg></w>,<caesura></caesura> <w n="10.6">l</w>’<w n="10.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="10.9" punct="pt:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>ch<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></w>.</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1" punct="vg:1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="11.2" punct="vg:3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" punct="vg">e</seg>ls</w>, <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="11.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="11.7" punct="vg:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>cch<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>n<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M/mp">Ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M/mp">î</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="12.4">b<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>t</w><caesura></caesura> <w n="12.5" punct="vg:8">f<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>l</w>, <w n="12.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="12.7">b<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>t</w> <w n="12.8" punct="pi:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ch<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pi">é</seg></rhyme></w> ?</l>
					</lg>
					<closer>
						<placeName>De Naples à Castellamare</placeName>
					</closer>
				</div></body></text></TEI>