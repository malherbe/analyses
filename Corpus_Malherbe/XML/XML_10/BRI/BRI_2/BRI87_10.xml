<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><head type="sub_part">A FLORENCE</head><div type="poem" key="BRI87" modus="cm" lm_max="10" metProfile="5+5" form="suite périodique" schema="3(abab)">
					<head type="main">Les Frères de la Miséricorde</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="1.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ps</w> <w n="1.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg>s</w><caesura></caesura> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="1.5">B<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rg<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>ll<seg phoneme="o" type="vs" value="1" rule="444" place="9">o</seg></w> <w n="1.6" punct="vg:10">s<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="2.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" mp="M">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" caesura="1">in</seg></w><caesura></caesura> <w n="2.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="2.6" punct="pv:10">c<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>f<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pv">é</seg></rhyme></w> ;</l>
						<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="3.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="3.3">br<seg phoneme="y" type="vs" value="1" rule="454" place="4" mp="M">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="3.5">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>cs<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></w> <w n="3.6" punct="vg:10">r<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>s<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3">s</w>’<w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="4.5" punct="dp:5">v<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="dp" caesura="1">a</seg></w> :<caesura></caesura> <w n="4.6">qu</w>’<w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6" mp="Lp">e</seg>st</w>-<w n="4.8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="4.9" punct="pi:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pi">é</seg></rhyme></w> ?</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="10" met="5+5">« <w n="5.1" punct="vg:2">S<seg phoneme="ɛ" type="vs" value="1" rule="384" place="1" mp="M">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>r</w>, <w n="5.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="5.3">l<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>ge<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>s</w><caesura></caesura> <w n="5.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="P">an</seg>s</w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="5.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7" punct="vg:10"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>b<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ls</w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="6.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="6.4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="4">en</seg>s</w> <w n="6.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="5" caesura="1">oi</seg>rs</w><caesura></caesura> <w n="6.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rts</w> <w n="6.7">j<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="Lc">u</seg>squ</w>’<w n="6.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg>x</w> <w n="6.9" punct="pi:10">y<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pi">eu</seg>x</rhyme></w> ?</l>
						<l n="7" num="2.3" lm="10" met="5+5"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="7.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="7.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="7.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" caesura="1">o</seg>rts</w><caesura></caesura> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="7.6">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</w> <w n="7.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="7.8" punct="vg:10">ci<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="10" met="5+5"><w n="8.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1" mp="C">eu</seg>rs</w> <w n="8.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>gts</w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="8.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="8.5" punct="pi:5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="pi" caesura="1">an</seg>cs</w> ?<caesura></caesura> <w n="8.6">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="8.7">su<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>s</w> <w n="8.8" punct="pt:10">c<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pt">eu</seg>x</rhyme></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="10" met="5+5">— <w n="9.1">S<seg phoneme="ɛ" type="vs" value="1" rule="384" place="1" mp="M">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="9.2" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="9.3">n<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>l</w> <w n="9.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>t</w> <w n="9.6">c<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg>nn<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></w></l>
						<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="10.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="10.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" mp="M">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg>s</w><caesura></caesura> <w n="10.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="P">ou</seg>r</w> <w n="10.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="10.7" punct="dp:10">bi<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="10" punct="dp">en</seg></rhyme></w> :</l>
						<l n="11" num="3.3" lm="10" met="5+5"><w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="11.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="11.4" punct="vg:5"><seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="11.6">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="Lc">an</seg>d</w>-<w n="11.7">D<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>c</w> <w n="11.8">p<seg phoneme="ø" type="vs" value="1" rule="398" place="9" mp="Lc">eu</seg>t</w>-<w n="11.9" punct="pv:10"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></w> ;</l>
						<l n="12" num="3.4" lm="10" met="5+5"><w n="12.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="12.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2" mp="C">e</seg>t</w> <w n="12.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="12.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="5" caesura="1">oi</seg>r</w><caesura></caesura> <w n="12.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="12.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="12.7" punct="pt:10">chr<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>ti<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="10" punct="pt">en</seg></rhyme></w>. »</l>
					</lg>
				</div></body></text></TEI>