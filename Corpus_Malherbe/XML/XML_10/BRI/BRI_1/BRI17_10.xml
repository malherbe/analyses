<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Marie</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2412 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRI_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Marie</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>FRANTEXT</publisher>
						<idno type="FRANTEXT">M547</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Marie</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<publisher>Garnier,Paris</publisher>
									<date when="1910">1910</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Auguste Brizeux</author>
						<imprint>
							<publisher>Alphonse Lemerre,Éditeur</publisher>
							<date when="1884">1879-1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>Les astérisques placées devant les noms propres ont été supprimés</p>
				<p>Les vers imcomplets (avec renvoi de la fin de vers) ont été restitués.</p>
				<p>Les citations et dédicaces ont été restituées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BRI17" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="5((aa))">
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="1.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="1.6">l</w>’<w n="1.7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="8">ue</seg>il</w> <w n="1.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="1.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="1.10">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>s<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">n</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>pt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="2.5" punct="vg:6">fr<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" punct="vg" caesura="1">ein</seg></w>,<caesura></caesura> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>c<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.7">l<seg phoneme="wa" type="vs" value="1" rule="423" place="10">oi</seg></w> <w n="2.8" punct="vg:12">tr<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>c<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="3.4">r<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>l</w><caesura></caesura> <w n="3.5">s</w>’<w n="3.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="3.8">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.9"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="3.10" punct="vg:12">v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>r</rhyme></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="4.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="4.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="4.5">s</w>’<w n="4.6" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="353" place="5" mp="M">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="vg" caesura="1">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.8">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="4.9">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="4.10" punct="pv:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">on</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pv">oi</seg>r</rhyme></w> ;</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="5.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" mp="C">e</seg>t</w> <w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="5.5">j</w>’<w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.9" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>n<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="1">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="6.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="6.4" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">in</seg>cts</w>,<caesura></caesura> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="6.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="6.7" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2">s</w>’<w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="7.5" punct="vg:6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg></w>,<caesura></caesura> <w n="7.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="7.8" punct="vg:12">h<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>nn<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</rhyme></w>,</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="8.2">scr<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="8.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="8.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="8.7" punct="vg:12">b<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>nh<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</rhyme></w>,</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1" punct="vg:1">Su<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>t</w>, <w n="9.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="9.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="9.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>rts</w>,<caesura></caesura> <w n="9.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="9.7">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="8" mp="M">oi</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="P">ou</seg>r</w> <w n="9.9" punct="vg:12">gu<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="10.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>tru<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="10.4" punct="vg:6">f<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="10.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="P">ou</seg>r</w> <w n="10.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.8">t<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>m<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
				</lg>
			</div></body></text></TEI>