<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GRAINS DE SEL</head><div type="poem" key="CRO88" modus="sm" lm_max="7" metProfile="7" form="" schema="">
					<head type="main">Le Hareng saur</head>
					<opener>
						<salute>A Guy</salute>
					</opener>
					<lg n="1" rhyme="None">
						<l rhyme="none" n="1" num="1.1" lm="7" met="7"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="1.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="1.5">m<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="1.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c</w> <del hand="RR" reason="analysis" type="irrelevant">— nu, nu, nu,</del></l>
						<l rhyme="none" n="2" num="1.2" lm="7" met="7"><w n="2.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">m<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="2.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w> <del hand="RR" reason="analysis" type="irrelevant">— haute, haute, haute,</del></l>
						<l rhyme="none" n="3" num="1.3" lm="7" met="7"><w n="3.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="3.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="3.3" punct="vg:3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" punct="vg">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="3.5">h<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>g</w> <w n="3.6">s<seg phoneme="ɔ" type="vs" value="1" rule="317" place="7">au</seg>r</w> <del hand="RR" reason="analysis" type="irrelevant">— sec, sec, sec,</del></l>
					</lg>
					<lg n="2" rhyme="None">
						<l rhyme="none" n="4" num="2.1" lm="7" met="7"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="4.2" punct="vg:2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2" punct="vg">en</seg>t</w>, <w n="4.3">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>s</w><del hand="RR" reason="analysis" type="irrelevant">— sales, sales, sales,</del></l>
						<l rhyme="none" n="5" num="2.2" lm="7" met="7"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="5.3" punct="vg:4">l<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>rd</w>, <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="5.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="5.6">cl<seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <del hand="RR" reason="analysis" type="irrelevant">— pointu, pointu, pointu</del></l>
						<l rhyme="none" n="6" num="2.3" lm="7" met="7"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="6.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w> <del hand="RR" reason="analysis" type="irrelevant">— gros, gros, gros,</del></l>
					</lg>
					<lg n="3" rhyme="None">
						<l rhyme="none" n="7" num="3.1" lm="7" met="7"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="7.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="7.5">l</w>’<w n="7.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w> <del hand="RR" reason="analysis" type="irrelevant">— haute, haute, haute,</del></l>
						<l rhyme="none" n="8" num="3.2" lm="7" met="7"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">cl<seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="8.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <del hand="RR" reason="analysis" type="irrelevant">— toc, toc, toc,</del></l>
						<l rhyme="none" n="9" num="3.3" lm="7" met="7"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="9.3">h<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="9.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="9.6">m<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="9.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c</w> <del hand="RR" reason="analysis" type="irrelevant">— nu, nu, nu</del></l>
					</lg>
					<lg n="4" rhyme="None">
						<l rhyme="none" n="10" num="4.1" lm="7" met="7"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <del hand="RR" reason="analysis" type="irrelevant">— qui tombe, qui tombe, qui tombe,</del></l>
						<l rhyme="none" n="11" num="4.2" lm="7" met="7"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="11.3">cl<seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w> <del hand="RR" reason="analysis" type="irrelevant">— longue, longue, longue,</del></l>
						<l rhyme="none" n="12" num="4.3" lm="7" met="7"><w n="12.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="12.3" punct="vg:3">b<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>t</w>, <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.5">h<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>g</w> <w n="12.6">s<seg phoneme="ɔ" type="vs" value="1" rule="317" place="7">au</seg>r</w> <del hand="RR" reason="analysis" type="irrelevant">— sec, sec, sec,</del></l>
					</lg>
					<lg n="5" rhyme="None">
						<l rhyme="none" n="13" num="5.1" lm="7" met="7"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="13.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.4">l</w>’<w n="13.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w> <del hand="RR" reason="analysis" type="irrelevant">— haute, haute, haute,</del></l>
						<l rhyme="none" n="14" num="5.2" lm="7" met="7"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <del hand="RR" reason="analysis" type="irrelevant">— lourd, lourd, lourd,</del></l>
						<l rhyme="none" n="15" num="5.3" lm="7" met="7"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2" punct="vg:2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg>s</w>, <w n="15.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="15.4">s</w>’<w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="15.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="307" place="6">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <del hand="RR" reason="analysis" type="irrelevant">— loin, loin, loin,</del></l>
					</lg>
					<lg n="6" rhyme="None">
						<l rhyme="none" n="16" num="6.1" lm="7" met="7"><w n="16.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="16.2" punct="vg:3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg>s</w>, <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4">h<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>g</w> <w n="16.5">s<seg phoneme="ɔ" type="vs" value="1" rule="317" place="7">au</seg>r</w> <del hand="RR" reason="analysis" type="irrelevant">— sec, sec, sec,</del></l>
						<l rhyme="none" n="17" num="6.2" lm="7" met="7"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="17.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w> <del hand="RR" reason="analysis" type="irrelevant">— longue, longue, longue,</del></l>
						<l rhyme="none" n="18" num="6.3" lm="7" met="7"><w n="18.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="18.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="18.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w> <del hand="RR" reason="analysis" type="irrelevant">— toujours, toujours, toujours,</del></l>
					</lg>
					<lg n="7" rhyme="None">
						<l rhyme="none" n="19" num="7.1" lm="7" met="7"><w n="19.1">J</w>’<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="19.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="19.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="19.5">h<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w> <del hand="RR" reason="analysis" type="irrelevant">— simple, simple, simple,</del></l>
						<l rhyme="none" n="20" num="7.2" lm="7" met="7"><w n="20.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="20.2">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="20.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="20.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="20.6">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="7">en</seg>s</w> <del hand="RR" reason="analysis" type="irrelevant">— graves, graves, graves,</del></l>
						<l rhyme="none" n="21" num="7.3" lm="7" met="7"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="21.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="21.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ts</w> <del hand="RR" reason="analysis" type="irrelevant">— petits, petits, petits.</del></l>
					</lg>
				</div></body></text></TEI>