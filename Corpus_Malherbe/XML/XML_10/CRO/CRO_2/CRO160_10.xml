<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TENDRESSE</head><div type="poem" key="CRO160" modus="cp" lm_max="12" metProfile="3, 8, 4, 6+6, 5+5" form="suite de strophes" schema="1(ababcdcd) 1(abba) 1(aabb) 1(ababa) 1(abab)">
					<head type="main">Aux femmes</head>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">N<seg phoneme="wa" type="vs" value="1" rule="440" place="1" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="1.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="1.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rd</w><caesura></caesura> <w n="1.5" punct="vg:8">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" mp="M">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.6" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="343" place="9" mp="M">a</seg><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="vg">en</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="3" met="3"><space quantity="20" unit="char"></space><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2" punct="pt:3">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="pt">eu</seg>rs</rhyme></w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="3.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="3.5" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>l</w>,<caesura></caesura> <w n="3.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.7">d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="3.8">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="10">en</seg></w> <w n="3.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="3.10" punct="vg:12">bi<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="12" punct="vg">en</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="3" met="3"><space quantity="20" unit="char"></space><w n="4.1">S<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="4.2" punct="pt:3">fl<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="pt">eu</seg>rs</rhyme></w>.</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">S<seg phoneme="wa" type="vs" value="1" rule="440" place="1" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="5.2" punct="dp:3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="dp">eu</seg>rs</w> : <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="P">a</seg>r</w> <w n="5.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="5.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>ps</w><caesura></caesura> <w n="5.6" punct="vg:9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="9" punct="vg">é</seg>s</w>, <w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>f<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>m<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg>s</rhyme></w></l>
						<l n="6" num="1.6" lm="3" met="3"><space quantity="20" unit="char"></space><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2" punct="vg:3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rb<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg></rhyme></w>,</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">S<seg phoneme="wa" type="vs" value="1" rule="440" place="1" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="7.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="7.4" punct="pt:6">l<seg phoneme="i" type="vs" value="1" rule="493" place="6" punct="pt" caesura="1">y</seg>s</w>.<caesura></caesura> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">E</seg>t</w> <w n="7.6" punct="vg:8">pu<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>s</w>, <w n="7.7" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="9" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">ez</seg></w>, <w n="7.8" punct="pe:12"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="11" mp="M">ai</seg>m<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pe">ez</seg></rhyme></w> !</l>
						<l n="8" num="1.8" lm="3" met="3"><space quantity="20" unit="char"></space><w n="8.1">C</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="8.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="8.4" punct="pe:3">b<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="pe ps">on</seg></rhyme></w> !…</l>
					</lg>
					<ab type="star">※※※</ab>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="9" num="2.1" lm="10" met="5+5"><space quantity="4" unit="char"></space><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2" mp="C">y</seg></w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="9.5" punct="vg:5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="9.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="C">i</seg>l</w> <w n="9.7"><seg phoneme="i" type="vs" value="1" rule="497" place="7" mp="C">y</seg></w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="9.10" punct="vg:10">f<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="193" place="10">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="10" num="2.2" lm="10" met="5+5"><space quantity="4" unit="char"></space><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2" mp="C">y</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="10.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5" caesura="1">oi</seg>s</w><caesura></caesura> <w n="10.6"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="10.7">l</w>’<w n="10.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="10.9">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>t</w> <w n="10.10">c<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>r<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</rhyme></w></l>
						<l n="11" num="2.3" lm="10" met="5+5"><space quantity="4" unit="char"></space><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="11.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2" mp="C">y</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="11.4">l</w>’<w n="11.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>g</w><caesura></caesura> <w n="11.6"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="11.7">l</w>’<w n="11.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="11.9">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>t</w> <w n="11.10" punct="pt:10">m<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>r<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg>r</rhyme></w>.</l>
						<l n="12" num="2.4" lm="10" met="5+5"><space quantity="4" unit="char"></space><w n="12.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rs</w>, <w n="12.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="12.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" caesura="1">ai</seg>t</w><caesura></caesura> <w n="12.5">l</w>’<w n="12.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.7"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></w> <w n="12.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="12.9" punct="pi:10">bl<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					</lg>
					<ab type="star">※※※</ab>
					<lg n="3" type="quatrain" rhyme="aabb">
						<l n="13" num="3.1" lm="8" met="8"><space quantity="8" unit="char"></space><w n="13.1">L</w>’<w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">aî</seg>t</w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="13.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt</w> <w n="13.7" punct="pt:8">vi<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="8" punct="pt">en</seg>t</rhyme></w>.</l>
						<l n="14" num="3.2" lm="8" met="8"><space quantity="8" unit="char"></space><w n="14.1">Qu</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w>-<w n="14.3">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="14.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l</w> <w n="14.7"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="14.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.9" punct="pi:8">bi<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" punct="pi">en</seg></rhyme></w> ?</l>
						<l n="15" num="3.3" lm="8" met="8"><space quantity="8" unit="char"></space><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="15.5">l</w>’<w n="15.6" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>r<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="16" num="3.4" lm="8" met="8"><space quantity="8" unit="char"></space><w n="16.1" punct="vg:2">R<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="16.2" punct="vg:4">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="3">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="16.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="16.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="16.5" punct="pt:8">s<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<ab type="star">※※※</ab>
					<lg n="4" type="quintil" rhyme="ababa">
						<l n="17" num="4.1" lm="8" met="8"><space quantity="8" unit="char"></space><w n="17.1">C<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="17.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="17.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="17.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</rhyme></w></l>
						<l n="18" num="4.2" lm="4" met="4"><space quantity="20" unit="char"></space><w n="18.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="18.2" punct="vg:2">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rt</w>, <w n="18.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="18.4" punct="vg:4">t<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="vg">o</seg>rt</rhyme></w>,</l>
						<l n="19" num="4.3" lm="8" met="8"><space quantity="8" unit="char"></space><w n="19.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="19.4">br<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="19.5" punct="pv:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>j<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pv">ou</seg>rs</rhyme></w> ;</l>
						<l n="20" num="4.4" lm="4" met="4"><space quantity="20" unit="char"></space><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="20.2" punct="vg:2">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rt</w>, <w n="20.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="20.4">M<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt</rhyme></w></l>
						<l n="21" num="4.5" lm="8" met="8"><space quantity="8" unit="char"></space><w n="21.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>t</w> <w n="21.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="21.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="21.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg>s</w> <w n="21.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="21.7" punct="pt:8">c<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>rts</rhyme></w>.</l>
					</lg>
					<ab type="star">※※※</ab>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="22" num="5.1" lm="8" met="8"><space quantity="8" unit="char"></space><w n="22.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="22.3" punct="vg:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="22.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="22.5" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="7">a</seg><rhyme label="a" id="11" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="321" place="8" punct="vg">y</seg>s</rhyme></w>,</l>
						<l n="23" num="5.2" lm="8" met="8"><space quantity="8" unit="char"></space><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="23.2" punct="vg:4">l<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="403" place="4" punct="vg">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="23.3"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="23.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="23.5" punct="vg:8">Fr<rhyme label="b" id="12" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="24" num="5.3" lm="8" met="8"><space quantity="8" unit="char"></space><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="24.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="24.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="24.4">p<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="24.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>h<rhyme label="a" id="11" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</rhyme></w></l>
						<l n="25" num="5.4" lm="8" met="8"><space quantity="8" unit="char"></space><w n="25.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="366" place="3">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="25.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="25.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="25.5" punct="pt:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ffr<rhyme label="b" id="12" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>