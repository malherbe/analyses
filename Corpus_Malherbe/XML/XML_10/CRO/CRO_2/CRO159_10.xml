<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TENDRESSE</head><div type="poem" key="CRO159" modus="sm" lm_max="8" metProfile="8" form="sonnet classique" schema="aabb aabb cdd cdc">
					<head type="main">Cueillette</head>
					<lg n="1" rhyme="aabb">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="1.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="1.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="1.6" punct="vg:8">v<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="vg">ou</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="2.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="2.6">d</w>’<w n="2.7" punct="vg:8"><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="vg">où</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">l</w>’<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="3.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.7" punct="pt:8">b<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3" punct="vg:5">j<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="4.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="4.5" punct="pt:8">f<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="aabb">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="5.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="5.5">c<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></rhyme></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="6.3">r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="6.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="6.7" punct="pt:8">f<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="pt">ou</seg></rhyme></w>.</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1" punct="vg:2">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>f<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2" punct="vg">oin</seg></w>, <w n="7.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="7.3">d</w>’<w n="7.4" punct="vg:5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="vg">o</seg>r</w>, <w n="7.5" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rv<seg phoneme="e" type="vs" value="1" rule="383" place="2">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.3">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="8.5" punct="pt:8">t<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="cdd">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="9.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="9.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></rhyme></w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="10.2" punct="pv:3">d<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="pv">é</seg></w> ; <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.4">j<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="11.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="11.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="11.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="11.7" punct="pv:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></w> ;</l>
					</lg>
					<lg n="4" rhyme="cdc">
						<l n="12" num="4.1" lm="8" met="8"><w n="12.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="12.2" punct="vg:2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>l</w>, <w n="12.3">f<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="12.6" punct="vg:8">fr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></w>,</l>
						<l n="13" num="4.2" lm="8" met="8"><w n="13.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="13.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="13.4">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="13.5" punct="vg:8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
						<l n="14" num="4.3" lm="8" met="8"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="14.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="14.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.6">f<seg phoneme="u" type="vs" value="1" rule="428" place="6">ou</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="14.7" punct="pt:8">bl<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>d</rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>