<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FANTAISIES TRAGIQUES</head><div type="poem" key="CRO137" modus="sp" lm_max="7" metProfile="7, 3" form="" schema="">
					<head type="main">In morte vita</head>
					<lg n="1" rhyme="None">
						<l rhyme="none" n="1" num="1.1" lm="7" met="7"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="1.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t</w></l>
						<l rhyme="none" n="2" num="1.2" lm="3" met="3"><space quantity="8" unit="char"></space><w n="2.1">C</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.4" punct="pt:3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="pt">o</seg>rt</w>.</l>
						<l rhyme="none" n="3" num="1.3" lm="7" met="7"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="3.2">qu</w>’<w n="3.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="3.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="3.5">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="3.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l rhyme="none" n="4" num="1.4" lm="3" met="3"><space quantity="8" unit="char"></space><w n="4.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="4.2" punct="pt:3">f<seg phoneme="a" type="vs" value="1" rule="193" place="3">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg>s</w>.</l>
						<l rhyme="none" n="5" num="1.5" lm="7" met="7"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> [<w n="5.4">br<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w>] <w n="5.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>cs</w></l>
						<l rhyme="none" n="6" num="1.6" lm="3" met="3"><space quantity="8" unit="char"></space><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">dr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p</w> <w n="6.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w></l>
						<l rhyme="none" n="7" num="1.7" lm="7" met="7"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2">l</w>’<w n="7.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="7.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="7.6">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
						<l rhyme="none" n="8" num="1.8" lm="3" met="3"><space quantity="8" unit="char"></space><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="8.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3" punct="pt:3">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></w>.</l>
						<l rhyme="none" n="9" num="1.9" lm="7" met="7"><w n="9.1">B<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="9.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="9.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="9.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w></l>
						<l rhyme="none" n="10" num="1.10" lm="3" met="3"><space quantity="8" unit="char"></space><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="10.3" punct="pv:3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3" punct="pv">ain</seg></w> ;</l>
						<l rhyme="none" n="11" num="1.11" lm="7" met="7"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="11.2"><seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="11.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="7">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
						<l rhyme="none" n="12" num="1.12" lm="3" met="3"><space quantity="12" unit="char"></space><w n="12.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="12.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="12.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></w></l>
						<l rhyme="none" n="13" num="1.13" lm="7" met="7"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="13.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="13.5">j<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w></l>
						<l rhyme="none" n="14" num="1.14" lm="3" met="3"><space quantity="8" unit="char"></space><w n="14.1">C</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="14.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="14.4" punct="pt:3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="pt">o</seg>rt</w>.</l>
					</lg>
				</div></body></text></TEI>