<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES LÈVRES CLOSES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1986 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les lèvres closes</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxleslevrescloses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				<p>Le poème "la Beauté" a été ajouté à partir d’une saisie manuelle</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX27" modus="sp" lm_max="8" metProfile="8, 4" form="suite périodique" schema="7(aabcbc)">
				<head type="main">LES RYTHMES</head>
				<lg n="1" type="sizain" rhyme="aabcbc">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">R<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="1.3">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4" punct="vg:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>sc<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="2" num="1.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="2.3" punct="vg:4">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">aî</seg>n<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="6">um</seg>s</w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="3.5" punct="vg:8">v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="vg">en</seg>t</rhyme></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="4.2">qu</w>’<w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w>-<w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="4.7">bl<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></w></l>
					<l n="5" num="1.5" lm="4" met="4"><space quantity="8" unit="char"></space><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="5.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</rhyme></w></l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="6.3">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>fl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="6.6" punct="pe:8">h<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="2" type="sizain" rhyme="aabcbc">
					<l n="7" num="2.1" lm="8" met="8"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="7.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="7.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.6">fr<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</rhyme></w></l>
					<l n="8" num="2.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="8.3" punct="vg:4">f<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="4" punct="vg">ê</seg>ts</rhyme></w>,</l>
					<l n="9" num="2.3" lm="8" met="8"><w n="9.1" punct="vg:1"><seg phoneme="u" type="vs" value="1" rule="426" place="1" punct="vg">Où</seg></w>, <w n="9.2">ru<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="9.5" punct="vg:8">l<rhyme label="b" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="10" num="2.4" lm="8" met="8"><w n="10.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="10.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="10.3">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="10.4">cr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<rhyme label="c" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</rhyme></w></l>
					<l n="11" num="2.5" lm="4" met="4"><space quantity="8" unit="char"></space><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="11.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<rhyme label="b" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg>s</rhyme></w></l>
					<l n="12" num="2.6" lm="8" met="8"><w n="12.1">R<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>gs</w> <w n="12.5" punct="pe:8">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="c" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">er</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="3" type="sizain" rhyme="aabcbc">
					<l n="13" num="3.1" lm="8" met="8"><w n="13.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="13.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="13.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="13.4" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">im</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="14" num="3.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4" punct="vg:4">s<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
					<l n="15" num="3.3" lm="8" met="8"><w n="15.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="15.2">l</w>’<w n="15.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="15.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="15.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="15.7">fl<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>ts</w> <w n="15.8">bl<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s</rhyme></w></l>
					<l n="16" num="3.4" lm="8" met="8"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="16.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="16.5" punct="pv:8">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="7">i</seg>ll<rhyme label="c" id="9" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></w> ;</l>
					<l n="17" num="3.5" lm="4" met="4"><space quantity="8" unit="char"></space><w n="17.1">Fl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>x</w> <w n="17.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>l<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</rhyme></w></l>
					<l n="18" num="3.6" lm="8" met="8"><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="18.2">l<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="18.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="18.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="18.6" punct="pe:8">pl<rhyme label="c" id="9" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="4" type="sizain" rhyme="aabcbc">
					<l n="19" num="4.1" lm="8" met="8"><w n="19.1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">Ai</seg>r</w> <w n="19.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>f</w> <w n="19.3">d</w>’<w n="19.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>str<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ts</w> <w n="19.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="19.6">ch<rhyme label="a" id="10" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>r</rhyme></w></l>
					<l n="20" num="4.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="20.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="20.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.4" punct="vg:4">c<rhyme label="a" id="10" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="4" punct="vg">œu</seg>r</rhyme></w>,</l>
					<l n="21" num="4.3" lm="8" met="8"><w n="21.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="21.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rs<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="21.4" punct="vg:8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="6">ym</seg>ph<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>n<rhyme label="b" id="11" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
					<l n="22" num="4.4" lm="8" met="8"><w n="22.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>s</w> <w n="22.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="22.3" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>rs</w>, <w n="22.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="22.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="22.6">n<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<rhyme label="c" id="12" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></w></l>
					<l n="23" num="4.5" lm="4" met="4"><space quantity="8" unit="char"></space><w n="23.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="23.2">l</w>’<w n="23.3" punct="vg:4">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>n<rhyme label="b" id="11" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></w>,</l>
					<l n="24" num="4.6" lm="8" met="8"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="24.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="24.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rd</w> <w n="24.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="24.5" punct="pe:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<rhyme label="c" id="12" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></w> !</l>
				</lg>
				<lg n="5" type="sizain" rhyme="aabcbc">
					<l n="25" num="5.1" lm="8" met="8"><w n="25.1" punct="vg:2">H<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2" punct="vg">e</seg>rs</w>, <w n="25.2" punct="vg:4">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg">em</seg>ps</w>, <w n="25.3" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg>s</w>, <w n="25.4" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="26" num="5.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="26.1">J<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rs</w> <w n="26.2" punct="vg:4">m<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>t<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="27" num="5.3" lm="8" met="8"><w n="27.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>rs</w> <w n="27.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="27.3" punct="pv:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>j<seg phoneme="ø" type="vs" value="1" rule="405" place="7">eu</seg>n<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg>s</rhyme></w> ;</l>
					<l n="28" num="5.4" lm="8" met="8"><w n="28.1">M<seg phoneme="ɛ" type="vs" value="1" rule="411" place="1">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="28.2">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="28.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="28.4">t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="28.5">d</w>’<w n="28.6" punct="vg:8"><rhyme label="c" id="15" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="29" num="5.5" lm="4" met="4"><space quantity="8" unit="char"></space><w n="29.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg></w> <w n="29.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="29.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="29.4">n<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ds</rhyme></w></l>
					<l n="30" num="5.6" lm="8" met="8"><w n="30.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="30.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="30.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="30.4" punct="pe:8">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>d<rhyme label="c" id="15" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></w> !</l>
				</lg>
				<lg n="6" type="sizain" rhyme="aabcbc">
					<l n="31" num="6.1" lm="8" met="8"><w n="31.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="31.2">m</w>’<w n="31.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="31.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="31.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>rs</w> <w n="31.6" punct="vg:8">f<rhyme label="a" id="16" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>s</rhyme></w>,</l>
					<l n="32" num="6.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="32.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="32.2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="32.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="32.4">v<rhyme label="a" id="16" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</rhyme></w></l>
					<l n="33" num="6.3" lm="8" met="8"><w n="33.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="33.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>f</w> <w n="33.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="33.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="33.5" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<rhyme label="b" id="17" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="34" num="6.4" lm="8" met="8"><w n="34.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>ts</w> <w n="34.2">d</w>’<w n="34.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="34.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="34.5" punct="pe:8">h<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="c" id="18" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg>x</rhyme></w> !</l>
					<l n="35" num="6.5" lm="4" met="4"><space quantity="8" unit="char"></space><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="35.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="35.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<rhyme label="b" id="17" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg>s</rhyme></w></l>
					<l n="36" num="6.6" lm="8" met="8"><w n="36.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="36.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="36.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="36.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="36.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="36.6" punct="dp:8">y<rhyme label="c" id="18" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="dp">eu</seg>x</rhyme></w> :</l>
				</lg>
				<lg n="7" type="sizain" rhyme="aabcbc">
					<l n="37" num="7.1" lm="8" met="8"><w n="37.1">R<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="37.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="37.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="37.4">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="37.5" punct="vg:8">fl<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>tt<rhyme label="a" id="19" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="38" num="7.2" lm="4" met="4"><space quantity="8" unit="char"></space><w n="38.1">F<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>ts</w> <w n="38.2" punct="vg:4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<rhyme label="a" id="19" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="39" num="7.3" lm="8" met="8"><w n="39.1">H<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="39.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="39.3" punct="vg:4">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" punct="vg">e</seg>rs</w>, <w n="39.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="39.5" punct="vg:8">v<rhyme label="b" id="20" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>x</rhyme></w>,</l>
					<l n="40" num="7.4" lm="8" met="8"><w n="40.1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">Ai</seg>rs</w> <w n="40.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>bs<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="40.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="40.4" punct="vg:8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="6">ym</seg>ph<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>n<rhyme label="c" id="21" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></w>,</l>
					<l n="41" num="7.5" lm="4" met="4"><space quantity="8" unit="char"></space><w n="41.1">J<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rs</w> <w n="41.2">d</w>’<w n="41.3" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>f<rhyme label="b" id="20" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>s</rhyme></w>,</l>
					<l n="42" num="7.6" lm="8" met="8"><w n="42.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="42.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="42.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="42.4" punct="pe:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<rhyme label="c" id="21" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</rhyme></w> !</l>
				</lg>
			</div></body></text></TEI>