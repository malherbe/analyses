<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA SAINT JEAN D’ÉTÉ</head><head type="main_subpart">SONNETS POUR UNE ANNÉE</head><div type="poem" key="HRV34" modus="cm" lm_max="12" metProfile="6÷6" form="sonnet classique, prototype 2" schema="abba abba ccd ede">
					<head type="main">OCTOBRE</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="1.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="1.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="1.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="1.5">l</w>’<w n="1.6">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="1.8" punct="vg:8">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="vg">e</seg>rcl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.10">n<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>l</w> <w n="1.11">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="1.12">s<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>t</rhyme></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="2.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="C">eu</seg>rs</w> <w n="2.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="5">e</seg>ds</w> <w n="2.5">n<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="2.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="2.9" punct="vg:12">pl<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="3.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="3.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="3.5" punct="vg:6">su<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg>t</w>,<caesura></caesura> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>s</w> <w n="3.7">l</w>’<w n="3.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.9" punct="vg:12">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>ch<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L</w>’<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>ct<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="4.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w> <w n="4.6">m<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s</w> <w n="4.7" punct="pt:12">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="pt">e</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="5.2" punct="vg:3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">O</seg>ct<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="vg">o</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="5.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="5.5">l</w>’<w n="5.6" punct="vg:9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="5.7" punct="vg:10">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" punct="vg">o</seg>rt</w>, <w n="5.8" punct="ps:12">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="ps">e</seg>t</rhyme></w>…</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="6.2" punct="vg:2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="6.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="6.6">br<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>s</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="6.8" punct="vg:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rv<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="7" num="2.3" lm="12" mp6="M" met="4+4+4"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="7.2">cl<seg phoneme="o" type="vs" value="1" rule="438" place="4" caesura="1">o</seg>s</w><caesura></caesura> <w n="7.3" punct="pv:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>bst<seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="pv" caesura="2">en</seg>t</w> ;<caesura></caesura> <w n="7.4"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="Fc">e</seg></w> <w n="7.5" punct="vg:12">ph<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="8.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="8.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.4" punct="vg:6">p<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="8.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>d</w> <w n="8.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="8.8" punct="pt:12">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>cr<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="pt">e</seg>t</rhyme></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="9.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="9.3">m<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="9.4" punct="pe:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" mp="M">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pe" caesura="1">u</seg></w> !<caesura></caesura> <w n="9.5">N</w>’<w n="9.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="9.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>c<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></w></l>
						<l n="10" num="3.2" lm="12" mp6="C" met="6−6"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="10.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="10.3">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="10.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C" caesura="1">on</seg></w><caesura></caesura> <w n="10.6" punct="vg:7">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="7" punct="vg">om</seg></w>, <w n="10.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>f</w> <w n="10.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="10.9" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>n<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="11.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg></w><caesura></caesura> <w n="11.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>rs</w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>s</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="11.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="11.8">s<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r</rhyme></w></l>
					</lg>
					<lg n="4" rhyme="ede">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="Fc">e</seg>s</w> <w n="12.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="12.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="12.5">j<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="M">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="12.7" punct="ps:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ll<rhyme label="e" id="7" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></rhyme></w>…</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="Lp">on</seg>t</w>-<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="13.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>rd<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="13.5">d<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>il</w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="13.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="13.8" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>sp<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>r</rhyme></w>,</l>
						<l n="14" num="4.3" lm="12" mp6="M" mp4="C" mp8="P" met="12"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="14.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="14.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="14.4" punct="vg:5">j<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="14.5" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="vg">in</seg></w>, <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="14.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="14.8" punct="pi:12">v<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="e" id="7" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></w> ?</l>
					</lg>
				</div></body></text></TEI>