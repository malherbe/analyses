<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="FLO49" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="4(abab) 2(abba) 1(aabb)">
					<head type="number">FABLE V</head>
					<head type="main">Le Rossignol et le Paon</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="1.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.5" punct="vg:8">Ph<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="2.2">c<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="2.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>x</w> <w n="2.5" punct="vg:8">j<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>rs</rhyme></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="3.3">l</w>’<w n="3.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg></w> <w n="3.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>d<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>rs</rhyme></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="5.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="5.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="290" place="4">aon</seg></w> <w n="5.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="5.6" punct="vg:8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="6.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>lt<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6" punct="vg:8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></w>,</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="7.3">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6" punct="vg:8">t<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>t</w> <w n="8.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="8.4" punct="dp:8">r<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="9.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="9.5" punct="vg:4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="9.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="6">en</seg>n<seg phoneme="ɥi" type="vs" value="1" rule="462" place="7">u</seg>y<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="10.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="10.4">tr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.5" punct="vg:8">pl<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g</w> <w n="11.4" punct="vg:4">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="vg">e</seg>c</w>, <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="11.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="11.7">gr<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="11.8" punct="vg:8">y<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></w>,</l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="12.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="12.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.5" punct="pe:8">b<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>c<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></w> !</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abba">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="13.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="13.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="13.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.7">bi<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8">en</seg></rhyme></w></l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">D</w>’<w n="14.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="14.3">c<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>br<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="14.5" punct="dp:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>dr<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="15.3">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="15.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w>-<w n="15.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="15.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="15.7" punct="pi:8">c<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></w> ?</l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="16.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="16.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="16.4" punct="vg:4">b<seg phoneme="o" type="vs" value="1" rule="315" place="4" punct="vg">eau</seg></w>, <w n="16.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.7">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="16.8" punct="pt:8">ri<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pt">en</seg></rhyme></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="aabb">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="17.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="17.3" punct="dp:8">Ph<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></w> :</l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="18.3" punct="vg:3">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3" punct="vg">ai</seg></w>, <w n="18.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.6">su<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="18.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="18.8" punct="pv:8">b<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="19.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="19.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="19.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="19.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="19.7" punct="vg:8">b<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</rhyme></w>,</l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">n</w>’<w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.5">t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="20.8" punct="pt:8">v<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>x</rhyme></w>.</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="21.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="21.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="21.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="21.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>g<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></w></l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1">M</w>’<w n="22.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="22.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="22.6" punct="vg:8">b<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</rhyme></w>,</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="23.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="23.3">t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="23.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="23.5" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">im</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ss<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="24.2">n</w>’<w n="24.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="24.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="24.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="24.6">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>ls</w> <w n="24.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>pp<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></w>.</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="abab">
						<l n="25" num="7.1" lm="8" met="8"><w n="25.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="25.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="25.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="5">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>r</w> <w n="25.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="25.5" punct="pv:8">d<rhyme label="a" id="13" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></w> ;</l>
						<l n="26" num="7.2" lm="8" met="8"><w n="26.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="26.2">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="26.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="26.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="26.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.7" punct="pi:8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pi">er</seg></rhyme></w> ?</l>
						<l n="27" num="7.3" lm="8" met="8"><w n="27.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="27.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>squ</w>’<w n="27.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="27.4">n</w>’<w n="27.5"><seg phoneme="i" type="vs" value="1" rule="497" place="6">y</seg></w> <w n="27.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t</w> <w n="27.7" punct="vg:8">g<rhyme label="a" id="13" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></w>,</l>
						<l n="28" num="7.4" lm="8" met="8"><w n="28.1">C</w>’<w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="28.3">l</w>’<w n="28.4"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="28.5">qu</w>’<w n="28.6"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="28.7">f<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t</w> <w n="28.8" punct="pt:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></w>.</l>
					</lg>
				</div></body></text></TEI>