<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Au vent crispé du matin</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>407 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_3</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Au vent crispé du matin</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/auventcrispdum00carc/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Au vent crispé du matin</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Nouvelle Édition Nouvelle</publisher>
									<date when="1913">1913</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1913">1913</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS AIGRES-DOUCES</head><div type="poem" key="CRC53" modus="cp" lm_max="12" metProfile="8, 5, (6+6), 5+5, (4)" form="suite périodique" schema="1(abbcac) 2(abaab)" er_moy="2.89" er_max="14" er_min="0" er_mode="0(5/9)" er_moy_et="4.63">
			<head type="main"><hi rend="ital">C’est ton amoureux</hi></head>
			<lg n="1" type="sizain" rhyme="abbcac">
				<l n="1" num="1.1" lm="12" met="6+6" met_alone="True"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="1.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="1.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="P">ou</seg>s</w> <w n="1.8"><pgtc id="4" weight="14" schema="[CV[CVCR" part="1">t<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></pgtc></w> <w n="1.9" punct="pt:12"><pgtc id="4" weight="14" schema="[CV[CVCR" part="2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">C</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="5" caesura="1">eu</seg>x</w><caesura></caesura> <w n="2.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="2.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="2.8">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="2.9" punct="vg:10">v<pgtc id="1" weight="0" schema="R" part="1"><rhyme label="b" id="1" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="vg">oi</seg>r</rhyme></pgtc></w>,</l>
				<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1">C</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="3.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="5" caesura="1">eu</seg>x</w><caesura></caesura> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="3.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.8">tr<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg>tt<pgtc id="1" weight="0" schema="R" part="1"><rhyme label="b" id="1" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r</rhyme></pgtc></w></l>
				<l n="4" num="1.4" lm="4"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="4.3" punct="vg:4">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="c" id="2" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></rhyme></pgtc></w>,</l>
				<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="5.3">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rgn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="5.5"><pgtc id="4" weight="14" schema="[CV[CVCR" part="1">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></pgtc></w> <w n="5.6"><pgtc id="4" weight="14" schema="[CV[CVCR" part="2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
				<l n="6" num="1.6" lm="5" met="5"><w n="6.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">s</w>’<w n="6.3"><seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="6.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="6.5" punct="pt:5">n<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="c" id="2" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="pt">on</seg></rhyme></pgtc></w>.</l>
			</lg>
			<lg n="2" type="quintil" rhyme="abaab">
				<l n="7" num="2.1" lm="10" met="5+5"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="7.2" punct="vg:2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="7.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="C">i</seg>l</w> <w n="7.4" punct="vg:5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg" caesura="1">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="7.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="C">i</seg>l</w> <w n="7.6" punct="vg:7">s<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="7.7"><seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="C">i</seg>l</w> <w n="7.8"><pgtc id="3" weight="8" schema="[C[VCR" part="1">s</pgtc></w>’<w n="7.9" punct="ps:10"><pgtc id="3" weight="8" schema="[C[VCR" part="2"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rr<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg></rhyme></pgtc></w>…</l>
				<l n="8" num="2.2" lm="5" met="5"><w n="8.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="8.3" punct="pv:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="b" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
				<l n="9" num="2.3" lm="10" met="5+5"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="9.4">s</w>’<w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="9.6" punct="vg:5">v<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg" caesura="1">a</seg></w>,<caesura></caesura> <w n="9.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="7">en</seg>t</w> <w n="9.8"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="9.9"><pgtc id="3" weight="8" schema="[C[VCR" part="1">s</pgtc></w>’<w n="9.10" punct="vg:10"><pgtc id="3" weight="8" schema="[C[VCR" part="2"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rr<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
				<l n="10" num="2.4" lm="10" met="5+5"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="10.4">j<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="10.5">cr<seg phoneme="y" type="vs" value="1" rule="454" place="4" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" caesura="1">e</seg>l</w><caesura></caesura> <w n="10.6"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="10.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="10.8">s</w>’<w n="10.9" punct="pv:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>t<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
				<l n="11" num="2.5" lm="10" met="5+5"><w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="11.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="11.4">j<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="11.5">cr<seg phoneme="y" type="vs" value="1" rule="454" place="4" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" caesura="1">e</seg>l</w><caesura></caesura> <w n="11.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="11.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="11.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="11.9" punct="ps:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>ffr<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="b" id="7" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="ps">i</seg>r</rhyme></pgtc></w>…</l>
			</lg>
			<lg n="3" type="quintil" rhyme="abaab">
				<l n="12" num="3.1" lm="10" met="5+5"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="Lc">u</seg>squ</w>’<w n="12.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="12.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" caesura="1">in</seg></w><caesura></caesura> <w n="12.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">em</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="12.7" punct="vg:10">r<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg><pgtc id="6" weight="2" schema="CR" part="1">s<rhyme label="a" id="6" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
				<l n="13" num="3.2" lm="8" met="8"><w n="13.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="13.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="13.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="13.6" punct="pt:8">br<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="b" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
				<l n="14" num="3.3" lm="5" met="5"><w n="14.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="14.2">n</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="14.5"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><pgtc id="6" weight="2" schema="CR" part="1">s<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></rhyme></pgtc></w></l>
				<l n="15" num="3.4" lm="10" met="5+5"><w n="15.1">Pl<seg phoneme="œ" type="vs" value="1" rule="407" place="1" mp="M">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="15.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="15.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="5" caesura="1">oi</seg></w><caesura></caesura> <w n="15.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="15.5">t</w>’<w n="15.6"><seg phoneme="y" type="vs" value="1" rule="391" place="7">eu</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg><pgtc id="6" weight="2" schema="CR" part="1">s<rhyme label="a" id="6" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
				<l n="16" num="3.5" lm="10" met="5+5"><w n="16.1">M<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="16.2" punct="vg:5">d<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="16.3">m<seg phoneme="wa" type="vs" value="1" rule="420" place="6" mp="M">oi</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="16.4" punct="pt:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><pgtc id="5" weight="0" schema="R" part="1"><rhyme label="b" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
			</lg>
		</div></body></text></TEI>