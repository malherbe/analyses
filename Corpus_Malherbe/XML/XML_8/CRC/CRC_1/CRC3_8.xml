<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies complètes</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>227 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies complètes</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k3366885p.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies complètes</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher></publisher>
									<date when="1955">1955</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1955">1955</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRC3" modus="cm" lm_max="12" metProfile="6=6" form="suite de distiques" schema="8((aa))" er_moy="1.12" er_max="6" er_min="0" er_mode="0(5/8)" er_moy_et="1.96">
			<head type="main">IL PLEUT</head>
			<lg n="1" type="distiques" rhyme="aa…">
				<l n="1" num="1.1" lm="12" mp6="Pem" met="6−6"><w n="1.1">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem" caesura="1">e</seg></w><caesura></caesura> <w n="1.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.5"><seg phoneme="u" type="vs" value="1" rule="426" place="9">où</seg></w> <w n="1.6">l</w>’<w n="1.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="1.8">s</w>’<w n="1.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="11" mp="M">en</seg>n<pgtc id="1" weight="1" schema="GR">u<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
				<l n="2" num="1.2" lm="12" mp6="C" met="6−6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="2.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="2.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C" caesura="1">e</seg></w><caesura></caesura> <w n="2.6">br<seg phoneme="u" type="vs" value="1" rule="427" place="7" mp="M">ou</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd</w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="2.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="2.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="2.10">pl<pgtc id="1" weight="1" schema="GR">u<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
				<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="3.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.5">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="3.6">d</w>’<w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="3.8" punct="pt:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11">ou</seg><pgtc id="2" weight="2" schema="CR">v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="ri_1" place="12" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
				<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">C</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.4" punct="ps:3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="ps">oi</seg>r</w>… <w n="4.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>d</w><caesura></caesura> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="4.8">bru<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="4.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="4.10">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="4.11">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="4.12" punct="vg:12"><pgtc id="2" weight="2" schema="[CR">v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
				<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="5.4">bru<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>ts</w> <w n="5.5">d</w>’<w n="5.6"><seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="5.7">t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="5.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg>x</w> <w n="5.9">v<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="5.10" punct="ps:12">gr<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg>s</rhyme></pgtc></w>…</l>
				<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="6.2" punct="ps:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" punct="ps">en</seg>d</w>… <w n="6.3">l</w>’<w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="4" mp="M">En</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="6.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rd</w><caesura></caesura> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="6.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="6.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.9"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="6.10" punct="vg:12">cr<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
				<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="7.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="7.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>ps</w><caesura></caesura> <w n="7.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="7.6" punct="tc:9">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="ti" mp="F">e</seg></w> — <w n="7.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="7.8" punct="vg:12">s<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>r</rhyme></pgtc></w>,</l>
				<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>, <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="8.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="8.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="8.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="8.7">v<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="8.8" punct="tc:12">n<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="ti">oi</seg>rs</rhyme></pgtc></w> —</l>
				<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">Qu</w>’<w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="9.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="9.4">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="9.7">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="9.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.9">fr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.10">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="9.11" punct="vg:12">S<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>l<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
				<l n="10" num="1.10" lm="12" mp6="M" mp8="F" met="4+8"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="10.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg>t</w><caesura></caesura> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="10.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="10.6" punct="vg:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>sc<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
				<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="11.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="11.4">b<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="11.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="353" place="10" mp="M">e</seg>ff<pgtc id="6" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>c<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></pgtc></w></l>
				<l n="12" num="1.12" lm="12" mp6="M" met="4+4+4"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg>x</w> <w n="12.4" punct="vg:5">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" caesura="1">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg" mp="F">e</seg>s</w>,<caesura></caesura> <w n="12.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="12.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" caesura="2">an</seg>t</w><caesura></caesura> <w n="12.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="12.8">s<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>rs</w> <w n="12.9">p<pgtc id="6" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg>s</rhyme></pgtc></w></l>
				<l n="13" num="1.13" lm="12" mp7="F" met="4+4+4"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="13.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.4"><seg phoneme="u" type="vs" value="1" rule="426" place="4" caesura="1">où</seg></w><caesura></caesura> <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="13.6">l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="13.7">qu</w>’<w n="13.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" caesura="2">on</seg></w><caesura></caesura> <w n="13.9">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="13.10">p<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>t</w> <w n="13.11">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="13.12">l<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
				<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="14.2">qu</w>’<w n="14.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="14.4">n</w>’<w n="14.5"><seg phoneme="i" type="vs" value="1" rule="497" place="4">y</seg></w> <w n="14.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="14.7" punct="vg:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>s</w>,<caesura></caesura><w n="14.8">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.9"><seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rt</w> <w n="14.10">p<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="P">ou</seg>r</w> <w n="14.11">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>x</w> <w n="14.12">d<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
				<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M/mc">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" mp="Lc">è</seg>s</w>-<w n="15.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M/mc">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="15.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="15.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="15.8" punct="ps:12">st<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="ps">eu</seg>x</rhyme></pgtc></w>…</l>
				<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rs</w> <w n="16.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="16.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="16.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>ts</w><caesura></caesura> <w n="16.6" punct="ps:8">br<seg phoneme="u" type="vs" value="1" rule="427" place="7" mp="M">ou</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="ps">a</seg>rds</w>… <w n="16.7"><seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="C">i</seg>l</w> <w n="16.8" punct="ps:10">pl<seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="ps">eu</seg>t</w>… <w n="16.9"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="C">i</seg>l</w> <w n="16.10" punct="ps:12">pl<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="ps">eu</seg>t</rhyme></pgtc></w>…</l>
			</lg>
		</div></body></text></TEI>