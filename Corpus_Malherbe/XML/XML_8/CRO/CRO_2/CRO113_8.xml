<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VISIONS</head><div type="poem" key="CRO113" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="7(aaa)" er_moy="0.93" er_max="3" er_min="0" er_mode="0(8/14)" er_moy_et="1.1">
					<head type="main">Chanson des peintres</head>
					<lg n="1" type="tercet" rhyme="aaa">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="1.3">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.5">gr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="2.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="2.6" punct="vg:8">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rv<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="3.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="3.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="3.6" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="tercet" rhyme="aaa">
						<l n="4" num="2.1" lm="8" met="8"><w n="4.1"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">O</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="4.2" punct="vg:4">j<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="4.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.5">br<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="m" type="a"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg>s</rhyme></pgtc></w></l>
						<l n="5" num="2.2" lm="8" met="8"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="5.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rf<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="m" type="e"><seg phoneme="œ̃" type="vs" value="1" rule="268" place="8">um</seg>s</rhyme></pgtc></w></l>
						<l n="6" num="2.3" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="6.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="6.5">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="6">y</seg>s</w> <w n="6.6" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="2" weight="2" schema="CR">f<rhyme label="a" id="2" gender="m" type="a"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" punct="pt">un</seg>ts</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="tercet" rhyme="aaa">
						<l n="7" num="3.1" lm="8" met="8"><w n="7.1" punct="vg:1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="7.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">c<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.5">m<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>d<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="3.2" lm="8" met="8"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">t<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="8.5" punct="vg:6">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg">i</seg>s</w>, <w n="8.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="9" num="3.3" lm="8" met="8"><w n="9.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="9.5">l</w>’<w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="6">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="9.7" punct="pt:8"><pgtc id="3" weight="2" schema="[CR">t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="tercet" rhyme="aaa">
						<l n="10" num="4.1" lm="8" met="8"><w n="10.1" punct="vg:3"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" punct="vg">e</seg>rs</w>, <w n="10.2" punct="vg:5">C<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>lts</w>, <w n="10.3" punct="vg:8">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>s</rhyme></pgtc></w>,</l>
						<l n="11" num="4.2" lm="8" met="8"><w n="11.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="451" place="3">u</seg>m</w> <w n="11.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="11.3">v<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="11.5" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>lli<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>s</rhyme></pgtc></w>,</l>
						<l n="12" num="4.3" lm="8" met="8"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="12.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="12.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="12.5" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="383" place="7">e</seg><pgtc id="4" weight="3" schema="GR">ill<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="tercet" rhyme="aaa">
						<l n="13" num="5.1" lm="8" met="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="13.4">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="13.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="13.6">ç<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="13.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="13.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.9">t<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="14" num="5.2" lm="8" met="8"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="14.4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>t</w> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="14.6">f<seg phoneme="a" type="vs" value="1" rule="193" place="5">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="14.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="14.8">v<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="15" num="5.3" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="15.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="6" type="tercet" rhyme="aaa">
						<l n="16" num="6.1" lm="8" met="8"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="16.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="16.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="16.7">d</w>’<w n="16.8" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rg<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
						<l n="17" num="6.2" lm="8" met="8"><w n="17.1">L</w>’<w n="17.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="17.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>ps</w> <w n="17.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ge<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="8">an</seg>t</rhyme></pgtc></w></l>
						<l n="18" num="6.3" lm="8" met="8"><w n="18.1">N</w>’<w n="18.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="18.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="18.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="18.5" punct="pt:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="6" weight="2" schema="CR">g<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="7" type="tercet" rhyme="aaa">
						<l n="19" num="7.1" lm="8" met="8"><w n="19.1">Qu</w>’<w n="19.2" punct="pe:2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="pe">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="19.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="19.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="19.7" punct="vg:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="7" weight="2" schema="CR">s<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="20" num="7.2" lm="8" met="8"><w n="20.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="20.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>rpr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="20.4" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="7" weight="2" schema="CR">s<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="21" num="7.3" lm="8" met="8"><w n="21.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="21.2" punct="vg:5">n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="21.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="21.4" punct="pt:8">p<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="7" weight="2" schema="CR">s<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>