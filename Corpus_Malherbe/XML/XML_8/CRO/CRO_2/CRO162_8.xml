<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TENDRESSE</head><div type="poem" key="CRO162" modus="sm" lm_max="7" metProfile="7" form="suite périodique" schema="3(abba)" er_moy="0.33" er_max="2" er_min="0" er_mode="0(5/6)" er_moy_et="0.75">
					<head type="main">Paroles d’un miroir à une belle dame</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1" punct="vg:2">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="1.2" punct="vg:4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="1.3" punct="vg:6">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="1.4" punct="pe:7">b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="2.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="2.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6">d<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="3.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">fr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4" punct="pi:7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>xqu<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
						<l n="4" num="1.4" lm="7" met="7"><w n="4.1" punct="vg:2">R<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="4.2" punct="vg:4">r<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="4.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="4.4" punct="pt:7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rv<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="7" met="7"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="5.4">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="5.5" punct="vg:7">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="vg">oi</seg>r</rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="7" met="7"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">gl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="6.6">d</w>’<w n="6.7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7">ain</seg></rhyme></pgtc></w>’</l>
						<l n="7" num="2.3" lm="7" met="7"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="7.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="7.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.6"><pgtc id="4" weight="2" schema="[CR">t<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7">ein</seg>t</rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="7" met="7"><w n="8.1">S</w>’<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="8.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="8.5" punct="pt:7">v<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="7" met="7"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="9.2" punct="vg:3">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>rs</w>, <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="5">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="9.5" punct="vg:7">p<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="7" met="7"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="10.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="10.5" punct="pv:7">tr<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="11" num="3.3" lm="7" met="7"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3" punct="po:3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> (<w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="11.5">t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>c</w> <w n="11.6">d</w>’<w n="11.7" punct="pf:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w>)</l>
						<l n="12" num="3.4" lm="7" met="7"><w n="12.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>fl<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="12.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="12.4" punct="pt:7">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>