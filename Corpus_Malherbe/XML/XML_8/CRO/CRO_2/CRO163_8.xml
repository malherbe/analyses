<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TENDRESSE</head><div type="poem" key="CRO163" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="4[abab]" er_moy="0.75" er_max="2" er_min="0" er_mode="0(5/8)" er_moy_et="0.97">
					<head type="main">Lilas</head>
					<lg n="1" type="regexp" rhyme="aba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="1.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="1.6" punct="pt:8">sc<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="2.2">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="4">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="2" weight="2" schema="CR">l<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></pgtc></w></l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>s</w> <w n="3.3">h<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="3.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <w n="3.6" punct="pt:8">s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="regexp" rhyme="bab">
						<l n="4" num="2.1" lm="8" met="8"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">m<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>ds</w> <w n="4.3" punct="dp:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" punct="dp">e</seg>nt</w> : <w n="4.4" punct="pe:8">H<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="2" weight="2" schema="CR">l<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
						<l n="5" num="2.2" lm="8" met="8"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="5.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="5.5">m<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>ts</w> <w n="5.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>bsc<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="6" num="2.3" lm="8" met="8"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="6.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">bru<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <w n="6.7" punct="pt:8">gl<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="regexp" rhyme="ababab">
						<l n="7" num="3.1" lm="8" met="8"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">b<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>s</w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.8">ch<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="8" num="3.2" lm="8" met="8"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="8.3">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="8.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="8.5" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cl<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>ts</rhyme></pgtc></w>.</l>
						<l n="9" num="3.3" lm="8" met="8"><w n="9.1" punct="pi:3">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pi">e</seg></w> ? <w n="9.2" punct="pi:6">D<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="pi">e</seg></w> ? <w n="9.3" punct="pt:8">Ph<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="10" num="3.4" lm="8" met="8"><w n="10.1">Plu<seg phoneme="i" type="vs" value="1" rule="482" place="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>th<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="10.4" punct="vg:8">l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="6" weight="2" schema="CR">l<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
						<l n="11" num="3.5" lm="8" met="8"><w n="11.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="11.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="11.3">d</w>’<w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</w> <w n="11.5" punct="vg:8">v<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="12" num="3.6" lm="8" met="8"><w n="12.1"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">O</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="12.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="12.4">gu<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="12.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="12.6" punct="pt:8">g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="6" weight="2" schema="CR">l<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="regexp" rhyme="aba">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2" punct="vg:2">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2" punct="vg">e</seg>r</w>, <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="13.4" punct="vg:6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" punct="vg">e</seg>s</w>, <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.6" punct="vg:8">pl<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="14.3" punct="pt:5"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="pt">é</seg></w>. <w n="14.4">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>s</w> <w n="14.6" punct="vg:8"><pgtc id="8" weight="2" schema="[CR">l<rhyme label="b" id="8" gender="m" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="15.4">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="15.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="15.7" punct="pt:8">h<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="regexp" rhyme="b">
						<l n="16" num="5.1" lm="8" met="8"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="16.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="16.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="16.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">aî</seg>t</w> <w n="16.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="16.6" punct="pt:8">l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="8" weight="2" schema="CR">l<rhyme label="b" id="8" gender="m" type="e" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>