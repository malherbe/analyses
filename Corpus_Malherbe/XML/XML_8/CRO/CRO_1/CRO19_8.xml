<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS PERPÉTUELLES</head><div type="poem" key="CRO19" modus="cp" lm_max="10" metProfile="5, 5+5" form="suite de strophes" schema="4[abab] 1[ababab]" er_moy="2.08" er_max="8" er_min="0" er_mode="2(7/12)" er_moy_et="1.89">
					<head type="main">Croquis d’hospitalité</head>
					<opener>
						<salute>A Démètre Perticari</salute>
					</opener>
					<lg n="1" type="regexp" rhyme="abababababababababab">
						<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="1.2" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="3" punct="vg">um</seg>s</w>, <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="1.4" punct="vg:5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="vg" caesura="1">eu</seg>rs</w>,<caesura></caesura> <w n="1.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="1.6" punct="vg:7">sch<seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="vg">a</seg>lls</w>, <w n="1.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="1.8">c<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg><pgtc id="1" weight="2" schema="CR">lli<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg>s</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="5" met="5"><space quantity="18" unit="char"></space><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="2.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="2.4" punct="pt:5">v<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ts</w> <w n="3.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="5" caesura="1">eu</seg>x</w><caesura></caesura> <w n="3.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="P">u</seg>r</w> <w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="3.7" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">li<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">er</seg>s</rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="5" met="5"><space quantity="18" unit="char"></space><w n="4.1">G<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="1">en</seg>s</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">h<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4" punct="pt:5">c<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="5" num="1.5" lm="10" met="5+5"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="5.2">j<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">am</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="5.3" punct="vg:5">j<seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="5.4">s<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="5.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="P">ou</seg>s</w> <w n="5.6">l</w>’<w n="5.7"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg><pgtc id="3" weight="2" schema="CR">v<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t</rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="5" met="5"><space quantity="18" unit="char"></space><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3" punct="pt:5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg><pgtc id="4" weight="8" schema="CVCR">m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="7" num="1.7" lm="10" met="5+5">(<w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" caesura="1">o</seg>rs</w><caesura></caesura> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="7.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="7.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="7.7"><pgtc id="3" weight="2" schema="[CR">v<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t</rhyme></pgtc></w></l>
						<l n="8" num="1.8" lm="5" met="5"><space quantity="18" unit="char"></space><w n="8.1">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="8.2" punct="vg:5">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>r<pgtc id="4" weight="8" schema="CVCR">m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="9" num="1.9" lm="10" met="5+5"><w n="9.1">L<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">em</seg>pr<seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="M">un</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="342" place="6" mp="P">à</seg></w> <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>ps</w> <w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>c<pgtc id="5" weight="1" schema="GR">i<rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="10">en</seg>s</rhyme></pgtc></w></l>
						<l n="10" num="1.10" lm="5" met="5"><space quantity="18" unit="char"></space><w n="10.1">Pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.2"><seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="10.3" punct="pt:5">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg><pgtc id="6" weight="2" schema="CR">sc<rhyme label="b" id="6" gender="f" type="a" stanza="3"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></pgtc></w>.)</l>
						<l n="11" num="1.11" lm="10" met="5+5"><w n="11.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="11.2" punct="vg:2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>ts</w>, <w n="11.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>x</w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg>x</w> <w n="11.5" punct="vg:5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5" punct="vg" caesura="1">ain</seg>s</w>,<caesura></caesura> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="11.7">r<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="11.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ds</w> <w n="11.9" punct="pt:10">ch<pgtc id="5" weight="1" schema="GR">i<rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="10" punct="pt">en</seg>s</rhyme></pgtc></w>.</l>
						<l n="12" num="1.12" lm="5" met="5"><space quantity="18" unit="char"></space><w n="12.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="12.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.4" punct="pt:5">l<seg phoneme="e" type="vs" value="1" rule="353" place="4">e</seg><pgtc id="6" weight="2" schema="CR">ss<rhyme label="b" id="6" gender="f" type="e" stanza="3"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="13" num="1.13" lm="10" met="5+5"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="13.3" punct="vg:3">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3" punct="vg">oin</seg></w>, <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg></w><caesura></caesura> <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="13.6" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="vg">a</seg>rcs</w>, <w n="13.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="13.8" punct="vg:10">f<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg><pgtc id="7" weight="2" schema="CR">l<rhyme label="a" id="7" gender="m" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" punct="vg">e</seg>ts</rhyme></pgtc></w>,</l>
						<l n="14" num="1.14" lm="5" met="5"><space quantity="18" unit="char"></space><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">gu<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="14.3" punct="vg:5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>r<pgtc id="8" weight="2" schema="CR">d<rhyme label="b" id="8" gender="f" type="a" stanza="4"><seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="15" num="1.15" lm="10" met="5+5"><w n="15.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="15.2">f<seg phoneme="œ" type="vs" value="1" rule="304" place="2" mp="M">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="15.3" punct="vg:5">r<seg phoneme="u" type="vs" value="1" rule="428" place="4" mp="M">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="15.5">ge<seg phoneme="ɛ" type="vs" value="1" rule="301" place="7">ai</seg>s</w> <w n="15.6" punct="vg:10">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg><pgtc id="7" weight="2" schema="CR">l<rhyme label="a" id="7" gender="m" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" punct="vg">e</seg>ts</rhyme></pgtc></w>,</l>
						<l n="16" num="1.16" lm="5" met="5"><space quantity="18" unit="char"></space><w n="16.1">Pl<seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="16.2" punct="pt:5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>fr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg><pgtc id="8" weight="2" schema="CR">d<rhyme label="b" id="8" gender="f" type="e" stanza="4"><seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="17" num="1.17" lm="10" met="5+5"><w n="17.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="17.2">p<seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="M">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="17.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4" mp="M">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>t</w><caesura></caesura> <w n="17.4">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s</w> <w n="17.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="17.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="17.7">f<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l</w> <w n="17.8"><pgtc id="9" weight="2" schema="[CR">gr<rhyme label="a" id="9" gender="m" type="a" stanza="5"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</rhyme></pgtc></w></l>
						<l n="18" num="1.18" lm="5" met="5"><space quantity="18" unit="char"></space><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="18.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<pgtc id="10" weight="1" schema="GR">i<rhyme label="b" id="10" gender="f" type="a" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
						<l n="19" num="1.19" lm="10" met="5+5"><w n="19.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="19.2">s</w>’<w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>t</w><caesura></caesura> <w n="19.4" punct="vg:9">c<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="vg" mp="F">e</seg>s</w>, <w n="19.5" punct="vg:10"><pgtc id="9" weight="2" schema="[CR">gr<rhyme label="a" id="9" gender="m" type="e" stanza="5"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg>ls</rhyme></pgtc></w>,</l>
						<l n="20" num="1.20" lm="5" met="5"><space quantity="18" unit="char"></space><w n="20.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>p<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.3" punct="pt:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>p<pgtc id="10" weight="1" schema="GR">i<rhyme label="b" id="10" gender="f" type="e" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="regexp" rhyme="ab">
						<l n="21" num="2.1" lm="10" met="5+5"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2" punct="vg:2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg>s</w>, <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="21.4" punct="vg:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="21.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="21.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="21.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="21.9"><pgtc id="9" weight="2" schema="[CR">gr<rhyme label="a" id="9" gender="m" type="a" stanza="5"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</rhyme></pgtc></w></l>
						<l n="22" num="2.2" lm="5" met="5"><space quantity="18" unit="char"></space><w n="22.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="22.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.5" punct="pt:5">b<pgtc id="10" weight="1" schema="GR">i<rhyme label="b" id="10" gender="f" type="a" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>