<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS PERPÉTUELLES</head><div type="poem" key="CRO9" modus="sp" lm_max="6" metProfile="4, 6" form="suite périodique" schema="3(ababcdcd)" er_moy="1.33" er_max="6" er_min="0" er_mode="0(6/12)" er_moy_et="1.7">
					<head type="main">Romance</head>
					<opener>
						<salute>A Philippe Burty</salute>
					</opener>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<l n="1" num="1.1" lm="4" met="4"><space quantity="4" unit="char"></space><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="6" met="6"><w n="2.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="2.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="2.4" punct="pt:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="4" met="4"><space quantity="4" unit="char"></space><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r</w> <w n="3.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4">ain</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="6" met="6"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">br<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="4.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.6" punct="pt:6">v<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="5" num="1.5" lm="4" met="4"><space quantity="4" unit="char"></space><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3">l</w>’<w n="5.4">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.5"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="5.6" punct="vg:4">v<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>t</rhyme></pgtc></w>,</l>
						<l n="6" num="1.6" lm="6" met="6"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="6.2">bru<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="6.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r</w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="6.5" punct="vg:6">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>sc<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="7" num="1.7" lm="4" met="4"><space quantity="4" unit="char"></space><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="7.3" punct="vg:4">r<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>d</rhyme></pgtc></w>,</l>
						<l n="8" num="1.8" lm="6" met="6"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3" punct="vg:3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="8.5" punct="pt:6">Dr<seg phoneme="i" type="vs" value="1" rule="d-4" place="5">y</seg><pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="huitain" rhyme="ababcdcd">
						<l n="9" num="2.1" lm="4" met="4"><space quantity="4" unit="char"></space><w n="9.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="9.2" punct="vg:4">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg><pgtc id="5" weight="2" schema="CR">qu<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
						<l n="10" num="2.2" lm="6" met="6"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rs</w> <w n="10.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="10.5" punct="pt:6"><pgtc id="6" weight="2" schema="[CR">r<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="11" num="2.3" lm="4" met="4"><space quantity="4" unit="char"></space><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="11.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="11.4"><pgtc id="5" weight="2" schema="[CR">c<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</rhyme></pgtc></w></l>
						<l n="12" num="2.4" lm="6" met="6"><w n="12.1">R<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="3">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="12.4" punct="pt:6">m<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg><pgtc id="6" weight="2" schema="CR">r<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="13" num="2.5" lm="4" met="4"><space quantity="4" unit="char"></space><w n="13.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="13.2">j</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rf<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</rhyme></pgtc></w></l>
						<l n="14" num="2.6" lm="6" met="6"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="14.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="14.3" punct="vg:3">br<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>s</w>, <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="14.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="14.6" punct="vg:6">h<pgtc id="8" weight="0" schema="R"><rhyme label="d" id="8" gender="f" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="15" num="2.7" lm="4" met="4"><space quantity="4" unit="char"></space><w n="15.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="15.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.3" punct="pt:4">b<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pt">oi</seg>s</rhyme></pgtc></w>.</l>
						<l n="16" num="2.8" lm="6" met="6"><w n="16.1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="16.3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="16.4">j</w>’<w n="16.5"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="16.6" punct="pt:6">pl<pgtc id="8" weight="0" schema="R"><rhyme label="d" id="8" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="huitain" rhyme="ababcdcd">
						<l n="17" num="3.1" lm="4" met="4"><space quantity="4" unit="char"></space><w n="17.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="17.3" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg><pgtc id="9" weight="2" schema="CR">r<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">aî</seg>t</rhyme></pgtc></w>,</l>
						<l n="18" num="3.2" lm="6" met="6"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="18.2">br<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="18.4" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg><pgtc id="10" weight="2" schema="CR">r<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="19" num="3.3" lm="4" met="4"><space quantity="4" unit="char"></space><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="19.3">f<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg><pgtc id="9" weight="2" schema="CR">r<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>t</rhyme></pgtc></w></l>
						<l n="20" num="3.4" lm="6" met="6"><w n="20.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="20.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rpr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="20.5" punct="pt:6">d<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg><pgtc id="10" weight="2" schema="CR">r<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="21" num="3.5" lm="4" met="4"><space quantity="4" unit="char"></space><w n="21.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="21.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="21.3">r<pgtc id="11" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<rhyme label="c" id="11" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></rhyme></pgtc></w></l>
						<l n="22" num="3.6" lm="6" met="6"><w n="22.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="22.2">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="22.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="22.4">m</w>’<w n="22.5" punct="vg:6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>ppr<pgtc id="12" weight="0" schema="R"><rhyme label="d" id="12" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="23" num="3.7" lm="4" met="4"><space quantity="4" unit="char"></space><w n="23.1">J</w>’<w n="23.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="23.3">pi<pgtc id="11" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<rhyme label="c" id="11" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></rhyme></pgtc></w></l>
						<l n="24" num="3.8" lm="6" met="6"><w n="24.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="24.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="24.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="24.4" punct="pt:6">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><pgtc id="12" weight="0" schema="R"><rhyme label="d" id="12" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>