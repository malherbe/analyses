<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GRAINS DE SEL</head><div type="poem" key="CRO98" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="5((aa))" er_moy="1.2" er_max="2" er_min="0" er_mode="2(3/5)" er_moy_et="0.98">
					<head type="main">Pituite</head>
					<lg n="1" type="distiques" rhyme="aa…">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="1" mp="M">A</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="1.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="1.3" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="353" place="4" mp="M">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5" mp="M">a</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="1.4" punct="vg:8">bl<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg" mp="F">e</seg></w>, <w n="1.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="1.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="1.7">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>s</w> <w n="1.8"><pgtc id="1" weight="2" schema="[CR">pl<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>s</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg>x</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="2.3" punct="vg:6">m<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>scl<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="2.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>p<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="2.6" punct="pv:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pv">u</seg>s</rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="3.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="3.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="3.5">m<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="3.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="3.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="3.9" punct="vg:12">m<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>n<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">T<seg phoneme="y" type="vs" value="1" rule="d-3" place="1" mp="M">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="P">a</seg>r</w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="4.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="4.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="4.8">l<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1" punct="vg:4">L<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg" mp="F">e</seg>s</w>, <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="5.3">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" caesura="1">ain</seg>s</w><caesura></caesura> <w n="5.4">l</w>’<w n="5.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="8" mp="M">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="3" weight="2" schema="CR">c<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="6.2">ch<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ds</w> <w n="6.3" punct="pt:6"><seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>st<seg phoneme="i" type="vs" value="1" rule="493" place="6" punct="pt" caesura="1">y</seg>s</w>.<caesura></caesura> <w n="6.4">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="6.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="6.6"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="6.7" punct="vg:12"><pgtc id="3" weight="2" schema="[CR">s<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="vg">œu</seg>r</rhyme></pgtc></w>,</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="7.2">f<seg phoneme="a" type="vs" value="1" rule="193" place="3">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="7.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" mp="M">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="7.5" punct="vg:9">m<seg phoneme="wa" type="vs" value="1" rule="423" place="9" punct="vg">oi</seg></w>, <w n="7.6">c<seg phoneme="o" type="vs" value="1" rule="415" place="10">ô</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="7.8" punct="vg:12">c<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="415" place="12">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1" punct="vg:4">Fr<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="8.2">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="5" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="8.3">qu</w>’<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="8.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="8.6"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="8.7" punct="vg:12">f<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="9.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" mp="M">ai</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>ri<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="9.5">d</w>’<w n="9.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="9.8" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>r<pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="vg">e</seg>l</rhyme></pgtc></w>,</l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="10.2">st<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="10.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="10.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.8">d</w>’<w n="10.9" punct="pt:12">h<seg phoneme="o" type="vs" value="1" rule="415" place="11" mp="M">ô</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="12" punct="pt">e</seg>l</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>