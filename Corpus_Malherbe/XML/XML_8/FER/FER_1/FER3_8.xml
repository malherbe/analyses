<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MÉLODIES POÉTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1014 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Mélodies poétiques</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/albertferlandmelodiespoetiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Mélodies poétiques</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://books.google.fr/books?id=TBENAAAAYAAJ</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Pierre J. Bédard, Imprimeur-relieur</publisher>
									<date when="1893">1893</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VERS L’IDÉAL</head><div type="poem" key="FER3" modus="sm" lm_max="8" metProfile="8" form="sonnet classique" schema="abab abab ccd eed" er_moy="0.29" er_max="2" er_min="0" er_mode="0(6/7)" er_moy_et="0.7">
					<head type="main">AU CIEL</head>
					<opener>
						<salute>À M J. N. FERLAND, P<hi rend="sup">tre</hi></salute>
					</opener>
					<lg n="1" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg></w>, <w n="1.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.4">f<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.5" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg><pgtc id="1" weight="2" schema="CR">rr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="2.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</rhyme></pgtc></w></l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="3.2">v<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="3.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="3.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="1" weight="2" schema="CR">r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="4.5" punct="vg:8">z<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ph<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="493" place="8" punct="vg">y</seg>r</rhyme></pgtc></w>,</l>
					</lg>
					<lg n="2" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1" punct="vg:1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg></w>, <w n="5.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.5">br<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="6.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="6.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="6.4">l</w>’<w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</rhyme></pgtc></w></l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">c<seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="7.5">t<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.6" punct="vg:8">pl<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="8.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>t</w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.6" punct="vg:8">c<seg phoneme="œ" type="vs" value="1" rule="345" place="7">ue</seg>ill<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="vg:1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg></w>, <w n="9.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">l</w>’<w n="9.4"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.7" punct="vg:8">gr<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="10.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="10.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="10.5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="10.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.7">m</w>’<w n="10.8" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="11.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>squ</w>’<w n="11.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="11.6" punct="vg:8">m<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
					</lg>
					<lg n="4" rhyme="eed">
						<l n="12" num="4.1" lm="8" met="8"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="12.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="12.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.5">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="12.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="12.7">b<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="13" num="4.2" lm="8" met="8"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="13.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.5"><pgtc id="7" weight="0" schema="[R"><rhyme label="e" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="14" num="4.3" lm="8" met="8"><w n="14.1">Qu</w>’<w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="14.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="14.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="14.6">v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="14.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>rs</w> <w n="14.8" punct="pt:8">t<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pt">oi</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>