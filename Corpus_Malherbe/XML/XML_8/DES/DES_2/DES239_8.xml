<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES239" modus="cp" lm_max="12" metProfile="3, 4+6, 6+6" form="suite de strophes" schema="1(abba) 1(ababcdcd) 1(ababcdcdee)" er_moy="1.45" er_max="6" er_min="0" er_mode="0(4/11)" er_moy_et="1.67">
					<head type="main">NADÈGE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Au ciel elle a rendu sa vie <lb></lb>
									Et doucement s’est endormie, <lb></lb>
									Sans murmurer contre ses lois. <lb></lb>
									Ainsi le sourire s’efface ; <lb></lb>
									Ainsi meurt, sans laisser de trace, <lb></lb>
									Le chant d’un oiseau dans les bois.
								</quote>
								 <bibl><hi rend="smallcap">Parny.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<div type="section" n="1">
						<lg n="1" type="quatrain" rhyme="abba">
							<l n="1" num="1.1" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="C">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg>x</w> <w n="1.4" punct="vg:4">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="1.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="1.7">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="1.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="1.9" punct="pv:10">n<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="10">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg>s</rhyme></pgtc></w> ;</l>
							<l n="2" num="1.2" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="2.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="2.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg>d</w><caesura></caesura> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="2.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rd</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="2.8" punct="pt:10">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" punct="pt">em</seg>ps</rhyme></pgtc></w>.</l>
							<l n="3" num="1.3" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="3.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="Lp">oi</seg>t</w>-<w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="3.3">m<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="3.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="3.6">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="3.7" punct="pe:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>s<pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pe">an</seg>ts</rhyme></pgtc></w> !</l>
							<l n="4" num="1.4" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">i</seg>ls</w> <w n="4.3" punct="vg:4">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4" punct="vg" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>,<caesura></caesura> <w n="4.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="4.5" punct="pe:6">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pe">eu</seg></w> ! <w n="4.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="4.8" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>br<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="2" type="huitain" rhyme="ababcdcd">
							<l n="5" num="2.1" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="5.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="5.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="5.6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="5.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="5.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="5.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="5.10" punct="pe:10"><pgtc id="3" weight="1" schema="GR">y<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pe">eu</seg>x</rhyme></pgtc></w> !</l>
							<l n="6" num="2.2" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="6.1">C</w>’<w n="6.2" punct="vg:2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="vg">ai</seg>t</w>, <w n="6.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="6.4" punct="vg:4">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg" caesura="1">oi</seg>t</w>,<caesura></caesura> <w n="6.5">l</w>’<w n="6.6"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="6.7">d</w>’<w n="6.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="6.9" punct="vg:10"><pgtc id="4" weight="0" schema="[R"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="7" num="2.3" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="7.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">om</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="7.3">l</w>’<w n="7.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="7.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>gr<seg phoneme="e" type="vs" value="1" rule="353" place="7" mp="M">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.7"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg>x</w> <w n="7.8" punct="pt:10">c<pgtc id="3" weight="1" schema="GR">i<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
							<l n="8" num="2.4" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="8.1">D</w>’<w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="8.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="8.5" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="482" place="4" punct="vg" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>,<caesura></caesura> <w n="8.6" punct="pe:5"><seg phoneme="o" type="vs" value="1" rule="444" place="5" punct="pe">o</seg>h</w> ! <w n="8.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="8.9">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt</w> <w n="8.10">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="8.11" punct="vg:10">v<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="9" num="2.5" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="9.1">Fl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="9.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="9.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="9.5">d</w>’<w n="9.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="9.7" punct="pe:10">s<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>r<pgtc id="5" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ph<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="pe">in</seg></rhyme></pgtc></w> !</l>
							<l n="10" num="2.6" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="10.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="10.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="10.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>g</w><caesura></caesura> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="10.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>t</w> <w n="10.7" punct="pv:10">m<seg phoneme="i" type="vs" value="1" rule="493" place="9" mp="M">y</seg>s<pgtc id="6" weight="2" schema="CR">t<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
							<l n="11" num="2.7" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="11.4">d</w>’<w n="11.5" punct="vg:4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="vg" caesura="1">o</seg>r</w>,<caesura></caesura> <w n="11.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="11.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="C">ou</seg>s</w> <w n="11.8">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="7" mp="M">eu</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</w> <w n="11.9">l<pgtc id="5" weight="6" schema="V[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></pgtc></w> <w n="11.10" punct="vg:10"><pgtc id="5" weight="6" schema="V[CR" part="2">f<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="vg">in</seg></rhyme></pgtc></w>,</l>
							<l n="12" num="2.8" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="12.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt</w> <w n="12.6">qu</w>’<w n="12.7"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="12.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="12.9" punct="pe:10"><pgtc id="6" weight="2" schema="CR" part="1">t<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="main">SUR SA TOMBE</head>
						<lg n="1" type="dizain" rhyme="ababcdcdee">
							<l n="13" num="1.1" lm="12" met="6+6"><w n="13.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="13.3">fr<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="13.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="13.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rd</w><caesura></caesura> <w n="13.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="13.7">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="13.8" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg><pgtc id="7" weight="2" schema="CR" part="1">rm<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="14" num="1.2" lm="12" met="6+6"><w n="14.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="14.3">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="14.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="14.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>g</w><caesura></caesura> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.8">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="14.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="14.10" punct="vg:12">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>s<pgtc id="8" weight="2" schema="CR" part="1">t<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></rhyme></pgtc></w>,</l>
							<l n="15" num="1.3" lm="12" met="6+6"><w n="15.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg>x</w> <w n="15.3">pl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="15.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="15.5">dr<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="15.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="15.8">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="7" weight="2" schema="CR" part="1">rm<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="16" num="1.4" lm="12" met="6+6"><w n="16.1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="16.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2" mp="Lc">i</seg></w>-<w n="16.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="16.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="16.6"><seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>g<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="16.7" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="8" weight="2" schema="CR" part="1">t<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></rhyme></pgtc></w>,</l>
							<l n="17" num="1.5" lm="12" met="6+6"><w n="17.1">L</w>’<w n="17.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">in</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>d<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="17.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="17.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>r</w> <w n="17.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="17.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="17.8" punct="pv:12">gl<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="c" id="9" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
							<l n="18" num="1.6" lm="12" met="6+6"><w n="18.1" punct="vg:1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="18.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="18.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="6" caesura="1">um</seg></w><caesura></caesura> <w n="18.5">s</w>’<w n="18.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>xh<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="18.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10" mp="P">e</seg>rs</w> <w n="18.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="18.9" punct="pt:12">c<pgtc id="10" weight="1" schema="GR" part="1">i<rhyme label="d" id="10" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
							<l n="19" num="1.7" lm="12" met="6+6"><w n="19.1" punct="pe:2">N<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" punct="pe">è</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="19.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="C">i</seg>l</w> <w n="19.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="19.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="19.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="Fc">e</seg></w> <w n="19.7" punct="vg:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>m<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="c" id="9" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="20" num="1.8" lm="12" met="6+6"><w n="20.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="20.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="20.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rd</w><caesura></caesura> <w n="20.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="20.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="20.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="20.8"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>d<pgtc id="10" weight="1" schema="GR" part="1">i<rhyme label="d" id="10" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</rhyme></pgtc></w></l>
							<l n="21" num="1.9" lm="3" met="3"><space quantity="16" unit="char"></space><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="21.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>gt</w> <w n="21.3" punct="pt:3"><pgtc id="11" weight="0" schema="R" part="1"><rhyme label="e" id="11" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="pt">an</seg>s</rhyme></pgtc></w>.</l>
							<l n="22" num="1.10" lm="3" met="3"><space quantity="16" unit="char"></space><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="22.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>gt</w> <w n="22.3" punct="pe:3"><pgtc id="11" weight="0" schema="R" part="1"><rhyme label="e" id="11" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="pe">an</seg>s</rhyme></pgtc></w> !</l>
						</lg>
					</div>
				</div></body></text></TEI>