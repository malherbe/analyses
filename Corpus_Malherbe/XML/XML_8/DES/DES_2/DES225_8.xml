<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES225" modus="sm" lm_max="6" metProfile="6" form="suite périodique" schema="3(ababcdcd)" er_moy="1.17" er_max="6" er_min="0" er_mode="0(7/12)" er_moy_et="1.72">
					<head type="main">NE ME PLAINS PAS !</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Playzir ne l’est qu’autant qu’on le partage.
								</quote>
								 <bibl><hi rend="smallcap">Clotilde.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<l n="1" num="1.1" lm="6" met="6"><w n="1.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="1.2" punct="vg:3">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="1.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4">t</w>’<w n="1.5" punct="pv:6"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg><pgtc id="1" weight="2" schema="CR">bl<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="469" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="2" num="1.2" lm="6" met="6"><w n="2.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>s</w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="2.5" punct="pe:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg><pgtc id="2" weight="2" schema="CR">j<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pe">ou</seg>rs</rhyme></pgtc></w> !</l>
						<l n="3" num="1.3" lm="6" met="6"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">m<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="6" met="6"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.4" punct="pt:6"><pgtc id="2" weight="2" schema="[CR">j<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>rs</rhyme></pgtc></w>.</l>
						<l n="5" num="1.5" lm="6" met="6"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="5.3" punct="vg:3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="5.5" punct="pv:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>tr<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="6" num="1.6" lm="6" met="6"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="6.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="6.5" punct="pv:6">d<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pv">eu</seg>x</rhyme></pgtc></w> ;</l>
						<l n="7" num="1.7" lm="6" met="6"><w n="7.1">Qu</w>’<w n="7.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.5">l</w>’<w n="7.6" punct="dp:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ppr<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="366" place="6">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="8" num="1.8" lm="6" met="6"><w n="8.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="8.2" punct="vg:3">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="8.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="8.4" punct="pe:6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pe">eu</seg>x</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="huitain" rhyme="ababcdcd">
						<l n="9" num="2.1" lm="6" met="6"><w n="9.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="9.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>c</w> <w n="9.4" punct="pe:6">p<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg><pgtc id="5" weight="2" schema="CR">ss<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="10" num="2.2" lm="6" met="6"><w n="10.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="10.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="10.6" punct="pt:6">t<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
						<l n="11" num="2.3" lm="6" met="6"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg><pgtc id="5" weight="2" schema="CR">s<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="12" num="2.4" lm="6" met="6"><w n="12.1">J</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="12.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="12.4">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="12.5">l</w>’<w n="12.6" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pe">ou</seg>r</rhyme></pgtc></w> !</l>
						<l n="13" num="2.5" lm="6" met="6"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="13.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="13.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="13.4" punct="pv:6"><pgtc id="7" weight="6" schema="[VCR"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>vr<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="14" num="2.6" lm="6" met="6"><w n="14.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1">e</seg>t</w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="14.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>t</w> <w n="14.4" punct="ps:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ffr<pgtc id="8" weight="0" schema="R"><rhyme label="d" id="8" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="ps">eu</seg>x</rhyme></pgtc></w>…</l>
						<l n="15" num="2.7" lm="6" met="6"><w n="15.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="15.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="15.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.5" punct="vg:6">c<pgtc id="7" weight="6" schema="VCR"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="16" num="2.8" lm="6" met="6"><w n="16.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="16.2" punct="vg:3">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="16.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="16.4" punct="pe:6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<pgtc id="8" weight="0" schema="R"><rhyme label="d" id="8" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pe">eu</seg>x</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" type="huitain" rhyme="ababcdcd">
						<l n="17" num="3.1" lm="6" met="6"><w n="17.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="17.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="17.3" punct="vg:3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="3" punct="vg">om</seg></w>, <w n="17.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="17.6">gl<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="18" num="3.2" lm="6" met="6"><w n="18.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="18.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.5" punct="vg:6">f<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>s</rhyme></pgtc></w>,</l>
						<l n="19" num="3.3" lm="6" met="6"><w n="19.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="19.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="19.4">m<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="20" num="3.4" lm="6" met="6"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="20.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ts</w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="20.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="20.6" punct="vg:6">v<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg">oi</seg>x</rhyme></pgtc></w>,</l>
						<l n="21" num="3.5" lm="6" met="6"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="21.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="21.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="21.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<pgtc id="11" weight="0" schema="R"><rhyme label="c" id="11" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="22" num="3.6" lm="6" met="6"><w n="22.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w>-<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="22.3" punct="pt:6">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg><pgtc id="12" weight="2" schema="CR">r<rhyme label="d" id="12" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
						<l n="23" num="3.7" lm="6" met="6"><w n="23.1">Qu</w>’<w n="23.2" punct="pe:2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="pe ps">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> !… <w n="23.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="23.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="23.5" punct="dp:6">d<pgtc id="11" weight="0" schema="R"><rhyme label="c" id="11" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="24" num="3.8" lm="6" met="6"><w n="24.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="24.2" punct="vg:3">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="24.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="24.4" punct="pe:6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg><pgtc id="12" weight="2" schema="CR">r<rhyme label="d" id="12" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pe">eu</seg>x</rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>