<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES252" modus="cm" lm_max="12" metProfile="6+6" form="suite de strophes" schema="1(abbacc) 1(abba) 1(abbacdcd)" er_moy="1.89" er_max="8" er_min="0" er_mode="2(4/9)" er_moy_et="2.33">
					<head type="main">L’IMPOSSIBLE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									On ne jette point l’ancre dans le fleuve de <lb></lb>la vie. Il emporte également celui qui lutte <lb></lb>contre son cours et celui qui s’y abandonne.
								</quote>
								 <bibl><hi rend="smallcap">Bernardin de Saint-Pierre.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1" type="sizain" rhyme="abbacc">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="1.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="1.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="1.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.8">v<seg phoneme="i" type="vs" value="1" rule="482" place="9">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.9"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="1.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="1.11"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2" punct="vg:3">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="2.3">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="2.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>l<seg phoneme="u" type="vs" value="1" rule="d-2" place="9" mp="M">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg>x</w> <w n="2.9" punct="vg:12">c<pgtc id="2" weight="1" schema="GR">i<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="3.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="3.4">cl<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="3.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="3.8" punct="vg:12"><pgtc id="2" weight="1" schema="GR">y<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="4.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="5" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="482" place="6" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="4.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="4.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="4.8" punct="vg:10">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="vg">eu</seg>rs</w>, <w n="4.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="4.10">c<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>nt</w> <w n="5.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="5.4" punct="vg:6">n<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>d</w>,<caesura></caesura> <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="5.6" punct="vg:9"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="5.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="5.8" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>mm<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="vg">e</seg>il</rhyme></pgtc></w>,</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="6.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="6.4">pl<seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="M">u</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="6.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="P">a</seg>r</w> <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="6.8" punct="pe:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="pe">e</seg>il</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="7" num="2.1" lm="12" met="6+6"><w n="7.1" punct="pe:1">Ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1" punct="pe">e</seg>l</w> ! <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="7.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="7.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ls</w> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="7.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="7.9"><seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="7.10">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="7.11" punct="vg:12">j<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<pgtc id="4" weight="2" schema="CR">n<rhyme label="a" id="4" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="2.2" lm="12" met="6+6"><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="8.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="8.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="8.5">pr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>sm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="8.7">br<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="8.8" punct="pe:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="5" weight="2" schema="CR">l<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pe">eu</seg>rs</rhyme></pgtc></w> !</l>
						<l n="9" num="2.3" lm="12" met="6+6"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="9.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="9.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="9.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="9.6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="9.9">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="9.10">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="9.11" punct="vg:12"><pgtc id="5" weight="2" schema="[CR">fl<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>rs</rhyme></pgtc></w>,</l>
						<l n="10" num="2.4" lm="12" met="6+6"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="10.2" punct="pe:2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2" punct="pe">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="10.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="10.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="10.5">s<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="10.6" punct="vg:6">l<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="10.7" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>, <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="10.9">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="10">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="10.10" punct="vg:12"><pgtc id="4" weight="2" schema="CR">n<rhyme label="a" id="4" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					</lg>
					<lg n="3" type="huitain" rhyme="abbacdcd">
						<l n="11" num="3.1" lm="12" met="6+6"><w n="11.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="11.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="11.6">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="11.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="11.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg><pgtc id="6" weight="8" schema="CVCR">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
						<l n="12" num="3.2" lm="12" met="6+6"><w n="12.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="12.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="12.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="12.4">m<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</w> <w n="12.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="12.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="12.9" punct="vg:12">f<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="13" num="3.3" lm="12" met="6+6"><w n="13.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="13.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="13.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="13.5" punct="vg:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="13.6">v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="13.7">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="13.8" punct="pe:12">f<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
						<l n="14" num="3.4" lm="12" met="6+6"><w n="14.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="14.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="14.5" punct="vg:6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg" caesura="1">e</seg>l</w>,<caesura></caesura> <w n="14.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="14.7">s</w>’<w n="14.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="14.9" punct="pv:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg><pgtc id="6" weight="8" schema="CVCR">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pv">i</seg>r</rhyme></pgtc></w> ;</l>
						<l n="15" num="3.5" lm="12" met="6+6"><w n="15.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="15.2">j</w>’<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2" mp="M">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="15.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="15.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="15.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="15.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="15.8">j</w>’<w n="15.9" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="9" mp="M">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="vg">ai</seg>s</w>, <w n="15.10">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>d</w> <w n="15.11"><pgtc id="8" weight="2" schema="[C[R" part="1">l</pgtc></w>’<w n="15.12"><pgtc id="8" weight="2" schema="[C[R" part="2"><rhyme label="c" id="8" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="16" num="3.6" lm="12" met="6+6"><w n="16.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="16.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="16.3" punct="vg:6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="6" punct="vg" caesura="1">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="16.6" punct="pi:9">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="9" punct="pi">oi</seg></w> ? <w n="16.7">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="16.8">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="16.9" punct="pv:12"><pgtc id="9" weight="2" schema="[CR" part="1">s<rhyme label="d" id="9" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pv">ai</seg>s</rhyme></pgtc></w> ;</l>
						<l n="17" num="3.7" lm="12" met="6+6"><w n="17.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="17.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="17.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="17.4">n<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="17.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="10">um</seg></w> <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="17.8" punct="vg:12"><pgtc id="8" weight="2" schema="[CR" part="1">fl<rhyme label="c" id="8" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="18" num="3.8" lm="12" met="6+6"><w n="18.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="18.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="18.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="18.4">br<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="18.5">s</w>’<w n="18.6"><seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w><caesura></caesura> <w n="18.7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="18.8">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="18.9" punct="ps:10">j<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="ps">ou</seg>rs</w>… <w n="18.10" punct="pt:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="9" weight="2" schema="CR" part="1">ss<rhyme label="d" id="9" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg>s</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>