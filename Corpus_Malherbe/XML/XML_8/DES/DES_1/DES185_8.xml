<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES185" modus="cp" lm_max="12" metProfile="6+6, (8)" form="suite de strophes" schema="1(abab) 1(abbacdcd)" er_moy="0.67" er_max="2" er_min="0" er_mode="0(4/6)" er_moy_et="0.94">
						<head type="main">PRIÈRE</head>
						<lg n="1" type="quatrain" rhyme="abab">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="1.5">m<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="1.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="1.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="1.8">gl<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="1.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="1.10">l</w>’<w n="1.11" punct="vg:12"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="2.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="2.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>rm<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="2.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="2.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="2.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="2.7">f<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="2.8">p<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="2.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="2.10">l</w>’<w n="2.11" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pv">ou</seg>r</rhyme></pgtc></w> ;</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="3.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="3.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg></w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="3.7">l</w>’<w n="3.8" punct="pt:12"><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="pe:1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1" punct="pe">eu</seg></w> ! <w n="4.2">j</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="4.4">p<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="4.7" punct="pt:6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="pt" caesura="1">i</seg>t</w>.<caesura></caesura> <w n="4.8">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.9">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.10">m</w>’<w n="4.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg></w> <w n="4.13" punct="pe:12">j<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>r</rhyme></pgtc></w> !</l>
						</lg>
						<lg n="2" type="huitain" rhyme="abbacdcd">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="5.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">j</w>’<w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5" mp="M">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg></w><caesura></caesura> <w n="5.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.8">v<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="5.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="5.10">m</w>’<w n="5.11" punct="pv:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12">ein</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="6.5">m<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="6.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.8">d<seg phoneme="œ" type="vs" value="1" rule="406" place="9">eu</seg>il</w> <w n="6.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="6.10">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="6.11" punct="dp:12">m<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="dp">o</seg>rt</rhyme></pgtc></w> :</l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="7.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="7.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.5" punct="vg:6">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg" caesura="1">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="7.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="7.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="7.9" punct="vg:12">s<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>rt</rhyme></pgtc></w>,</l>
							<l n="8" num="2.4" lm="8"><space quantity="6" unit="char"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="8.4">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>d</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6">m</w>’<w n="8.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="3" weight="2" schema="CR">tt<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
							<l n="9" num="2.5" lm="12" met="6+6"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rs</w> <w n="9.3">s</w>’<w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="9.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="9.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>r</w> <w n="9.7" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="5" weight="2" schema="CR">m<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
							<l n="10" num="2.6" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="10.3">l</w>’<w n="10.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="10.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="10.6"><seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="10.7" punct="pt:12">fl<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
							<l n="11" num="2.7" lm="12" met="6+6"><w n="11.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="11.2">n</w>’<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="11.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="Lp">i</seg>s</w>-<w n="11.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="11.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.8">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</w> <w n="11.9">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg><pgtc id="5" weight="2" schema="CR">m<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="12">en</seg>t</rhyme></pgtc></w></l>
							<l n="12" num="2.8" lm="12" met="6+6"><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d</w> <w n="12.4">d</w>’<w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="12.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="12.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="12.8">s</w>’<w n="12.9"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="306" place="10">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="12.10">n<seg phoneme="o" type="vs" value="1" rule="438" place="11" mp="C">o</seg>s</w> <w n="12.11" punct="pt:12"><pgtc id="6" weight="0" schema="[R"><rhyme label="d" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
						</lg>
					</div></body></text></TEI>