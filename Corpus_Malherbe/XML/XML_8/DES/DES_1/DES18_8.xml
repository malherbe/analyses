<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES18" modus="cp" lm_max="12" metProfile="8, 6+6, (6)" form="suite de strophes" schema="3[abba] 2[abab]" er_moy="1.8" er_max="6" er_min="0" er_mode="0(5/10)" er_moy_et="2.27">
						<head type="main">L’INQUIÉTUDE</head>
						<lg n="1" type="regexp" rhyme="abba">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Qu</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="1.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2" mp="F">e</seg></w> <w n="1.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="1.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="1.7" punct="pi:6">tr<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pi" caesura="1">ou</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ?<caesura></caesura> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.9">qu</w>’<w n="1.10"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w>-<w n="1.11">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="9" mp="F">e</seg></w> <w n="1.12">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="1.13">j</w>’<w n="1.14" punct="pi:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">tt<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12" punct="pi">en</seg>ds</rhyme></pgtc></w> ?</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="2.3">tr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="2.6" punct="vg:6">v<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.8">m</w>’<w n="2.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="8" mp="M">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="482" place="9">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.10"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="2.11" punct="pv:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ll<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
							<l n="3" num="1.3" lm="6"><space quantity="10" unit="char"></space><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>rs</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="3.5"><pgtc id="2" weight="0" schema="[R"><rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">p<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="4.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="4.4">s<seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="4.7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="4.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="4.9" punct="pt:12"><pgtc id="1" weight="2" schema="[CR">t<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12" punct="pt">em</seg>ps</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="2" type="regexp" rhyme="abbaabbaabababab">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:3"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">Au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>s</w>, <w n="5.2">l</w>’<w n="5.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="5.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="5.7">l</w>’<w n="5.8" punct="vg:12"><pgtc id="3" weight="6" schema="[VCR"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="353" place="5" mp="M">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="6.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="6.5">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="6.6" punct="pt:12">l<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg><pgtc id="4" weight="2" schema="CR">s<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>rs</rhyme></pgtc></w>.</l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="7.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="7.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>c</w> <w n="7.5">l</w>’<w n="7.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>bj<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="7.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="7.9">v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="7.10" punct="pi:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="4" weight="2" schema="CR">s<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pi">i</seg>rs</rhyme></pgtc></w> ?</l>
							<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="8.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="8.8" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><pgtc id="3" weight="6" schema="VCR"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
							<l n="9" num="2.5" lm="12" met="6+6"><w n="9.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="9.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="9.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="9.5">b<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="9.6">n</w>’<w n="9.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="9.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="9.10" punct="vg:12">g<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">aî</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></pgtc></w>,</l>
							<l n="10" num="2.6" lm="12" met="6+6"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="10.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="10.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="10.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="10.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="10.8" punct="pv:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>c<pgtc id="6" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="b" id="6" gender="f" type="a" stanza="3"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
							<l n="11" num="2.7" lm="12" met="6+6"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="11.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="11.4">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s</w> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="11.6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="11.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="11.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="11.10" punct="vg:12">f<pgtc id="6" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<rhyme label="b" id="6" gender="f" type="e" stanza="3"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="12" num="2.8" lm="8" met="8"><space quantity="6" unit="char"></space><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="12.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.4" punct="pi:8">f<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pi">é</seg></rhyme></pgtc></w> ?</l>
							<l n="13" num="2.9" lm="8" met="8"><space quantity="6" unit="char"></space><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="13.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="13.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>di<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="13.6" punct="vg:8">h<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" stanza="4"><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
							<l n="14" num="2.10" lm="12" met="6+6"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">A</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="Lp">ez</seg></w>-<w n="14.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="14.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="14.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="14.6">fu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>r</w> <w n="14.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="14.8" punct="pi:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>t<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a" stanza="4"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pi">ou</seg>r</rhyme></pgtc></w> ?</l>
							<l n="15" num="2.11" lm="12" met="6+6"><w n="15.1" punct="vg:3">R<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="15.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="15.3" punct="pv:6">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pv" caesura="1">on</seg></w> ;<caesura></caesura> <w n="15.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305" place="9">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="15.6" punct="vg:12">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg>p<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e" stanza="4"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="16" num="2.12" lm="12" met="6+6"><w n="16.1">M</w>’<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M/mp">an</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M/mp">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem/mp">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="5" mp="Lp">ez</seg></w>-<w n="16.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="16.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="16.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r</w> <w n="16.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="16.7">l</w>’<w n="16.8" punct="pi:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">A</seg>m<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e" stanza="4"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pi ps">ou</seg>r</rhyme></pgtc></w> ?…</l>
							<l n="17" num="2.13" lm="12" met="6+6"><w n="17.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="17.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="17.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="17.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="6" caesura="1">om</seg></w><caesura></caesura> <w n="17.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="17.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="17.7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="17.8">d</w>’<w n="17.9" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>t<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a" stanza="5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
							<l n="18" num="2.14" lm="12" met="6+6"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="18.2">l</w>’<w n="18.3"><seg phoneme="e" type="vs" value="1" rule="353" place="2" mp="M">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="18.4">qu</w>’<w n="18.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="18.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="18.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="18.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="18.9"><seg phoneme="e" type="vs" value="1" rule="353" place="9" mp="M">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423" place="10">oi</seg></w> <w n="18.10">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="18.11" punct="pe:12">d<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a" stanza="5"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>x</rhyme></pgtc></w> !</l>
							<l n="19" num="2.15" lm="12" met="6+6"><w n="19.1" punct="vg:2">R<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="19.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="19.3">n</w>’<w n="19.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="19.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="19.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="19.7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="9">e</seg>t</w> <w n="19.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="19.9">m</w>’<w n="19.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ppr<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e" stanza="5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="20" num="2.16" lm="12" met="6+6"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="20.3" punct="vg:3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="3" punct="vg">om</seg></w>, <w n="20.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="20.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="20.6" punct="vg:6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="6" punct="vg" caesura="1">en</seg>s</w>,<caesura></caesura> <w n="20.7">m</w>’<w n="20.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="20.9"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="20.10">d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="20.11">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="20.12">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="20.13" punct="pt:12">v<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e" stanza="5"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>s</rhyme></pgtc></w>.</l>
						</lg>
					</div></body></text></TEI>