<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES451" modus="sm" lm_max="5" metProfile="5" form="suite périodique" schema="3(ababcdcd)" er_moy="0.92" er_max="2" er_min="0" er_mode="0(6/12)" er_moy_et="0.95">
					<head type="main">OÙ VAS-TU ?</head>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<l n="1" num="1.1" lm="5" met="5"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="352" place="1">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.3">m</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg><pgtc id="1" weight="2" schema="CR">ppr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="5" met="5"><w n="2.1">D</w>’<w n="2.2"><seg phoneme="u" type="vs" value="1" rule="426" place="1">où</seg></w> <w n="2.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>t</w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="2.5" punct="pv:5">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="pv">eu</seg>r</rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="5" met="5"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="3.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg><pgtc id="1" weight="2" schema="CR">pr<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="5" met="5"><w n="4.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="4.2">t</w>-<w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="4.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="4.5" punct="pi:5">c<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="5" punct="pi">œu</seg>r</rhyme></pgtc></w> ?</l>
						<l n="5" num="1.5" lm="5" met="5"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l</w> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>pr<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="5" met="5"><w n="6.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="6.3">l</w>’<w n="6.4" punct="pv:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>xpr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pv">er</seg></rhyme></pgtc></w> ;</l>
						<l n="7" num="1.7" lm="5" met="5"><w n="7.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="7.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="7.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="4">oi</seg></w> <w n="7.4">j</w>’<w n="7.5" punct="dp:5"><pgtc id="3" weight="0" schema="[R"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="8" num="1.8" lm="5" met="5"><w n="8.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="8.4" punct="pe:5"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">ai</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pe">er</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="huitain" rhyme="ababcdcd">
						<l n="9" num="2.1" lm="5" met="5"><w n="9.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="9.2" punct="vg:3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>s</w>, <w n="9.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="9.4">ch<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg>s</rhyme></pgtc></w></l>
						<l n="10" num="2.2" lm="5" met="5"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.5" punct="pt:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<pgtc id="6" weight="2" schema="CR">l<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pt">er</seg></rhyme></pgtc></w>.</l>
						<l n="11" num="2.3" lm="5" met="5"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="11.3">j</w>’<w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="11.6">l<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg>s</rhyme></pgtc></w></l>
						<l n="12" num="2.4" lm="5" met="5"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="12.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="12.4" punct="pt:5">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg><pgtc id="6" weight="2" schema="CR">l<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pt">er</seg></rhyme></pgtc></w>.</l>
						<l n="13" num="2.5" lm="5" met="5"><w n="13.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
						<l n="14" num="2.6" lm="5" met="5"><w n="14.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.3" punct="vg:5">l<pgtc id="8" weight="1" schema="GR">i<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg">eu</seg></rhyme></pgtc></w>,</l>
						<l n="15" num="2.7" lm="5" met="5"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="15.2">mi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="2">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3">s</w>’<w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>v<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
						<l n="16" num="2.8" lm="5" met="5"><w n="16.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="16.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="16.3" punct="pe:5">D<pgtc id="8" weight="1" schema="GR">i<rhyme label="d" id="8" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="pe">eu</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" type="huitain" rhyme="ababcdcd">
						<l n="17" num="3.1" lm="5" met="5"><w n="17.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="17.3"><pgtc id="9" weight="2" schema="[C[R" part="1">l</pgtc></w>’<w n="17.4"><pgtc id="9" weight="2" schema="[C[R" part="2"><rhyme label="a" id="9" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="5">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
						<l n="18" num="3.2" lm="5" met="5"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="18.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3" punct="pv:5">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>j<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pv">ou</seg>rs</rhyme></pgtc></w> ;</l>
						<l n="19" num="3.3" lm="5" met="5"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="19.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.4" punct="pt:5"><pgtc id="9" weight="2" schema="[CR" part="1">fl<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="20" num="3.4" lm="5" met="5"><w n="20.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="20.3">l</w>’<w n="20.4" punct="vg:3"><seg phoneme="o" type="vs" value="1" rule="315" place="3" punct="vg">eau</seg></w>, <w n="20.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="20.6" punct="pt:5">c<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="pt">ou</seg>rs</rhyme></pgtc></w>.</l>
						<l n="21" num="3.5" lm="5" met="5"><w n="21.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="21.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>t</w> <w n="21.4">l</w>’<w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg><pgtc id="11" weight="2" schema="CR" part="1">v<rhyme label="c" id="11" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></rhyme></pgtc></w></l>
						<l n="22" num="3.6" lm="5" met="5"><w n="22.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="22.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="22.4" punct="vg:5">t<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="d" id="12" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
						<l n="23" num="3.7" lm="5" met="5"><w n="23.1">J</w>’<w n="23.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="23.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="23.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="23.5" punct="ps:5"><pgtc id="11" weight="2" schema="[CR" part="1">v<rhyme label="c" id="11" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="ps">e</seg></rhyme></pgtc></w>…</l>
						<l n="24" num="3.8" lm="5" met="5"><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="24.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w>-<w n="24.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="24.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="24.5" punct="pe:5">m<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="d" id="12" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="pe">oi</seg></rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>