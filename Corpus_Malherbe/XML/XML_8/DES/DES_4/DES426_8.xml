<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ENFANTS ET JEUNES FILLES</head><div type="poem" key="DES426" modus="cp" lm_max="12" metProfile="5, 7, 6+6" form="suite périodique" schema="2(aabccb) 2(aa) 1(ababccddee)" er_moy="2.08" er_max="6" er_min="0" er_mode="2(7/13)" er_moy_et="1.86">
					<head type="main">LA PETITE PLEUREUSE, À SA MÈRE</head>
					<lg n="1" type="sizain" rhyme="aabccb">
						<l n="1" num="1.1" lm="5" met="5"><space quantity="10" unit="char"></space><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="1.2">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">l</w>’<w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg><pgtc id="1" weight="2" schema="CR">f<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="5" met="5"><space quantity="10" unit="char"></space><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="2.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="2.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><pgtc id="1" weight="2" schema="CR">f<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d</rhyme></pgtc></w></l>
						<l n="3" num="1.3" lm="7" met="7"><space quantity="8" unit="char"></space><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="3.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="3.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="3.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="3.6" punct="pv:7"><pgtc id="2" weight="2" schema="[CR">s<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="4" num="1.4" lm="5" met="5"><space quantity="10" unit="char"></space><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="4.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.5">fl<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</rhyme></pgtc></w></l>
						<l n="5" num="1.5" lm="5" met="5"><space quantity="10" unit="char"></space><w n="5.1">S<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="5.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="5.4" punct="ps:5">pl<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="ps">eu</seg>rs</rhyme></pgtc></w>…</l>
						<l n="6" num="1.6" lm="7" met="7"><space quantity="8" unit="char"></space><w n="6.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="6.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="6.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>c</w> <w n="6.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="6.5" punct="pi:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg><pgtc id="2" weight="2" schema="CR">s<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
					</lg>
					<lg n="2" type="distique" rhyme="aa">
						<l n="7" num="2.1" lm="12" met="6+6"><w n="7.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="7.2" punct="vg:3">m<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg></w>, <w n="7.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="7.4">t</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="7.6">v<seg phoneme="y" type="vs" value="1" rule="457" place="6" caesura="1">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="7.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="7.9"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r</w> <w n="7.10" punct="vg:12">j<seg phoneme="wa" type="vs" value="1" rule="440" place="11" mp="M">o</seg><pgtc id="4" weight="1" schema="GR">y<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="8" num="2.2" lm="12" met="6+6"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="8.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="8.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="8.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="8.9"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg>x</w> <w n="8.10" punct="pt:12"><pgtc id="4" weight="1" schema="[GR">y<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="distique" rhyme="aa">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="9.2" punct="vg:2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>l</w>, <w n="9.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="P">ou</seg>s</w> <w n="9.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="9.5" punct="vg:6">b<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" punct="vg" caesura="1">e</seg>ts</w>,<caesura></caesura> <w n="9.6">j</w>’<w n="9.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="9.8">v<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="9.9">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="M">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="9.10">m<pgtc id="5" weight="6" schema="V[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></pgtc></w> <w n="9.11" punct="pt:12"><pgtc id="5" weight="6" schema="V[CR" part="2">m<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">J</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="10.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="10.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="10.5" punct="vg:6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="C">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="10.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="10">en</seg></w> <w n="10.9" punct="pt:12"><pgtc id="5" weight="6" schema="[VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="sizain" rhyme="aabccb">
						<l n="11" num="4.1" lm="12" met="6+6"><w n="11.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>s</w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="11.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="11.5" punct="pt:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="pt" caesura="1">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>.<caesura></caesura> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M/mp">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="Lp">ai</seg>s</w>-<w n="11.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="11.8">tr<seg phoneme="o" type="vs" value="1" rule="433" place="10">o</seg>p</w> <w n="11.9" punct="pi:12">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg><pgtc id="6" weight="2" schema="CR" part="1">s<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pi">é</seg></rhyme></pgtc></w> ?</l>
						<l n="12" num="4.2" lm="12" met="6+6"><w n="12.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="12.4">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="12.5" punct="pe:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pe" caesura="1">a</seg>s</w> !<caesura></caesura> <w n="12.6" punct="vg:7">M<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></w>, <w n="12.7">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="12.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="12.9">pi<seg phoneme="e" type="vs" value="1" rule="241" place="10">e</seg>d</w> <w n="12.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="6" weight="2" schema="CR" part="1">ss<rhyme label="a" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></pgtc></w></l>
						<l n="13" num="4.3" lm="7" met="7"><space quantity="8" unit="char"></space><w n="13.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="13.3">d</w>’<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="13.6" punct="pt:7"><pgtc id="7" weight="0" schema="[R" part="1"><rhyme label="b" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="14" num="4.4" lm="5" met="5"><space quantity="10" unit="char"></space><w n="14.1">L</w>’<w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="1">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="14.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="14.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.5"><pgtc id="8" weight="2" schema="[CR" part="1">pr<rhyme label="c" id="8" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d</rhyme></pgtc></w></l>
						<l n="15" num="4.5" lm="5" met="5"><space quantity="10" unit="char"></space><w n="15.1">M</w>’<w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="15.4" punct="vg:5">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg><pgtc id="8" weight="2" schema="CR" part="1">r<rhyme label="c" id="8" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
						<l n="16" num="4.6" lm="7" met="7"><space quantity="8" unit="char"></space><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">m</w>’<w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rs</w> <w n="16.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="16.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="16.7" punct="pt:7">ch<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="b" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="dizain" rhyme="ababccddee">
						<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="17.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="17.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="17.4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4">en</seg>s</w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="17.6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="M">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="17.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="P">u</seg>r</w> <w n="17.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="17.9" punct="vg:12">g<pgtc id="9" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>x</rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1" punct="vg:2">M<seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" punct="vg">an</seg></w>, <w n="18.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="18.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="18.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="18.5" punct="dp:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="dp in" caesura="1">u</seg>s</w> :<caesura></caesura> « <w n="18.6">V<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="18.7">n</w>’<w n="18.8"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="18.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="18.10" punct="pe:12">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11" mp="M">en</seg>t<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> ! »</l>
						<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1" punct="vg:2">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="19.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="19.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="19.4" punct="vg:6">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg>s</w>,<caesura></caesura> <w n="19.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="19.6">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="8" mp="M">eu</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>s</w> <w n="19.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>tr<pgtc id="9" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></pgtc></w> <w n="19.8" punct="vg:12"><pgtc id="9" weight="6" schema="V[CR" part="2">n<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>s</rhyme></pgtc></w>,</l>
						<l n="20" num="5.4" lm="12" met="6+6"><w n="20.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="20.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="20.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="20.4">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="20.5" punct="dp:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="dp" caesura="1">a</seg>s</w> :<caesura></caesura> <w n="20.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7" mp="Lp">oi</seg>s</w>-<w n="20.7" punct="vg:8">t<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg></w>, <w n="20.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="20.9">su<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>s</w> <w n="20.10">t<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="20.11" punct="vg:12">f<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="21" num="5.5" lm="12" met="6+6"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="21.3">t</w>’<w n="21.4" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3" punct="vg">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="21.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="21.7">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="21.8">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="21.9">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg></w> <w n="21.10">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="21.11">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="21.12"><pgtc id="11" weight="2" schema="[CR" part="1">j<rhyme label="c" id="11" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>rs</rhyme></pgtc></w></l>
						<l n="22" num="5.6" lm="12" met="6+6"><w n="22.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="22.2">m</w>’<w n="22.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3" mp="M">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="22.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5" mp="M">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>p</w><caesura></caesura> <w n="22.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="22.6">t</w>’<w n="22.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="339" place="9" mp="M">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="22.8" punct="pe:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="11" weight="2" schema="CR" part="1">j<rhyme label="c" id="11" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>rs</rhyme></pgtc></w> !</l>
						<l n="23" num="5.7" lm="12" met="6+6"><w n="23.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">Em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="23.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>c</w> <w n="23.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="23.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="23.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="23.6">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="23.7" punct="vg:12">ch<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="12" weight="2" schema="CR" part="1">r<rhyme label="d" id="12" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="24" num="5.8" lm="12" met="6+6"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="24.2" punct="vg:3">j<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>s</w>, <w n="24.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="24.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="24.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="24.6" punct="dp:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="dp in">i</seg>s</w> : « <w n="24.7" punct="ps:9">v<seg phoneme="u" type="vs" value="1" rule="425" place="9" punct="ps vg">ou</seg>s</w> »…, <w n="24.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="24.9">t</w>’<w n="24.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="24.11" punct="pe:12"><pgtc id="12" weight="2" schema="[CR" part="1">pr<rhyme label="d" id="12" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
						<l n="25" num="5.9" lm="12" met="6+6"><w n="25.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="25.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M/mp">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M/mp">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="Lp">on</seg>s</w>-<w n="25.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="25.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="25.5">d<seg phoneme="o" type="vs" value="1" rule="435" place="8" mp="M/mp">o</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="Lp">on</seg>s</w>-<w n="25.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="25.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="25.8" punct="dp:12"><pgtc id="13" weight="2" schema="[CR" part="1">m<rhyme label="e" id="13" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="dp">ain</seg></rhyme></pgtc></w> :</l>
						<l n="26" num="5.10" lm="12" met="6+6"><w n="26.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="26.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="26.3">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="3" mp="M">eu</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="26.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="26.5" punct="vg:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="26.6" punct="pe:7">v<seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pe">a</seg></w> ! <w n="26.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="26.8">r<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w> <w n="26.9" punct="pe:12">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="13" weight="2" schema="CR" part="1">m<rhyme label="e" id="13" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="pe">ain</seg></rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>