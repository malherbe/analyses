<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AMOUR</head><div type="poem" key="DES367" modus="sp" lm_max="7" metProfile="7, 5" form="suite périodique" schema="3(ababcdcd)" er_moy="1.17" er_max="2" er_min="0" er_mode="2(7/12)" er_moy_et="0.99">
					<head type="main">DANS L’ÉTÉ</head>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="1.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="1.3">c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>rc<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="1.5">l</w>’<w n="1.6" punct="vg:7"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="5" met="5"><space quantity="2" unit="char"></space><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="2.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">l</w>’<w n="2.5" punct="vg:5"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg><pgtc id="2" weight="2" schema="CR">s<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="5" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2" punct="vg:3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" punct="vg">en</seg>d</w>, <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="3.4">qu</w>’<w n="3.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="3.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="3.7" punct="vg:7">s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="5" met="5"><space quantity="2" unit="char"></space><w n="4.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="4.4" punct="pt:5">r<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><pgtc id="2" weight="2" schema="CR">s<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="5" punct="pt">eau</seg></rhyme></pgtc></w>.</l>
						<l n="5" num="1.5" lm="7" met="7"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="5.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="5.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>s<pgtc id="3" weight="2" schema="CR">p<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="5" met="5"><space quantity="2" unit="char"></space><w n="6.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="6.4" punct="pv:5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg><pgtc id="4" weight="2" schema="CR">v<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pv">er</seg></rhyme></pgtc></w> ;</l>
						<l n="7" num="1.7" lm="7" met="7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">ru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="7.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="7.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg><pgtc id="3" weight="2" schema="CR">p<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="1.8" lm="5" met="5"><space quantity="2" unit="char"></space><w n="8.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">l</w>’<w n="8.3" punct="pt:5"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>pr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg><pgtc id="4" weight="2" schema="CR">v<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pt">er</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="huitain" rhyme="ababcdcd">
						<l n="9" num="2.1" lm="7" met="7"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="9.3">n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ds</w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="9.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="9.6"><pgtc id="5" weight="0" schema="[R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></pgtc></w></l>
						<l n="10" num="2.2" lm="5" met="5"><space quantity="2" unit="char"></space><w n="10.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="10.2" punct="vg:5">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg><pgtc id="6" weight="2" schema="CR">m<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
						<l n="11" num="2.3" lm="7" met="7"><w n="11.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="11.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>r<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</rhyme></pgtc></w></l>
						<l n="12" num="2.4" lm="5" met="5"><space quantity="2" unit="char"></space><w n="12.1">L</w>’<w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="12.3" punct="pv:5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<pgtc id="6" weight="2" schema="CR">m<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="pv">an</seg>t</rhyme></pgtc></w> ;</l>
						<l n="13" num="2.5" lm="7" met="7"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="13.3">br<seg phoneme="y" type="vs" value="1" rule="445" place="3">û</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="13.5">m<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>s<pgtc id="7" weight="2" schema="CR">t<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="14" num="2.6" lm="5" met="5"><space quantity="2" unit="char"></space><w n="14.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="14.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ts</w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="14.5">fl<pgtc id="8" weight="0" schema="R"><rhyme label="d" id="8" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</rhyme></pgtc></w></l>
						<l n="15" num="2.7" lm="7" met="7"><w n="15.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="15.2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>ls</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="15.6"><pgtc id="7" weight="2" schema="CR">t<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="16" num="2.8" lm="5" met="5"><space quantity="2" unit="char"></space><w n="16.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="16.2">bl<seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="16.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="16.5" punct="pt:5">pl<pgtc id="8" weight="0" schema="R"><rhyme label="d" id="8" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="huitain" rhyme="ababcdcd">
						<l n="17" num="3.1" lm="7" met="7"><choice reason="analysis" type="false_verse" hand="CA"><sic>Et</sic><corr source="édition_1973"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="vg">é</seg></w></corr></choice>, <w n="17.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="17.3">tr<seg phoneme="o" type="vs" value="1" rule="433" place="4">o</seg>p</w> <w n="17.4">j<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="18" num="3.2" lm="5" met="5"><space quantity="2" unit="char"></space><w n="18.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="18.2">fu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>r</w> <w n="18.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="18.4" punct="pt:5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg><pgtc id="10" weight="2" schema="CR">g<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pt">er</seg></rhyme></pgtc></w>.</l>
						<l n="19" num="3.3" lm="7" met="7"><w n="19.1">L</w>’<w n="19.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="19.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="19.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.5">j</w>’<w n="19.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="20" num="3.4" lm="5" met="5"><space quantity="2" unit="char"></space><w n="20.1">S</w>’<w n="20.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="20.4" punct="vg:5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>r<pgtc id="10" weight="2" schema="CR">g<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></rhyme></pgtc></w>,</l>
						<l n="21" num="3.5" lm="7" met="7"><w n="21.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="21.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="21.3">l</w>’<w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="21.5"><pgtc id="11" weight="2" schema="[CR">n<rhyme label="c" id="11" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="457" place="7">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="22" num="3.6" lm="5" met="5"><space quantity="2" unit="char"></space><w n="22.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="22.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="22.3" punct="vg:5">cr<seg phoneme="y" type="vs" value="1" rule="454" place="4">u</seg><pgtc id="12" weight="0" schema="R"><rhyme label="d" id="12" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="vg">e</seg>l</rhyme></pgtc></w>,</l>
						<l n="23" num="3.7" lm="7" met="7"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">s<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="23.3">l</w>’<w n="23.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="23.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><pgtc id="11" weight="2" schema="CR">n<rhyme label="c" id="11" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="457" place="7">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="24" num="3.8" lm="5" met="5"><space quantity="2" unit="char"></space><w n="24.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="24.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="24.3" punct="pe:5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<pgtc id="12" weight="0" schema="R"><rhyme label="d" id="12" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="pe">e</seg>l</rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>