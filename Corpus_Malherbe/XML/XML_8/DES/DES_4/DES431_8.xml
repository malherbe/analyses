<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ENFANTS ET JEUNES FILLES</head><div type="poem" key="DES431" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="4(ababcdcd)" er_moy="1.56" er_max="6" er_min="0" er_mode="2(8/16)" er_moy_et="1.54">
					<head type="main">LE CHIEN ET L’ENFANT</head>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="1.2">d</w>’<w n="1.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.4">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.5">l<pgtc id="1" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">bl<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="2.6" punct="pe:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="2" weight="2" schema="CR">r<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</rhyme></pgtc></w> !</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w>-<w n="3.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="3.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="3.6">p<pgtc id="1" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">N</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="4.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rps</w> <w n="4.6" punct="pi:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg><pgtc id="2" weight="2" schema="CR">rr<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pi">an</seg>t</rhyme></pgtc></w> ?</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w>-<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="5.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="5.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>ct</w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.7"><pgtc id="3" weight="2" schema="[CR">pr<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3">pr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="6.4">qu</w>’<w n="6.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="6.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w>-<w n="6.8" punct="ps:8">b<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="ps">a</seg>s</rhyme></pgtc></w>…</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="7.2">n</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="7.4">qu</w>’<w n="7.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="3" weight="2" schema="CR">r<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="8.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="8.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="8.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rd</w> <w n="8.8" punct="pe:8">p<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="huitain" rhyme="ababcdcd">
						<l n="9" num="2.1" lm="8" met="8"><w n="9.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rdi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="9.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="9.5" punct="pt:8"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="5" weight="2" schema="CR">v<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="10" num="2.2" lm="8" met="8"><w n="10.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="1">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="10.4" punct="vg:8">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="6" weight="2" schema="CR">c<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg>x</rhyme></pgtc></w>,</l>
						<l n="11" num="2.3" lm="8" met="8"><w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="11.3">l</w>’<w n="11.4"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="11.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="11.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="11.8"><pgtc id="5" weight="2" schema="[CR">v<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="12" num="2.4" lm="8" met="8"><w n="12.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.3">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>ds</w> <w n="12.4" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rbr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="6" weight="2" schema="CR">ss<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg>x</rhyme></pgtc></w>.</l>
						<l n="13" num="2.5" lm="8" met="8"><w n="13.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="13.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="13.6">l</w>’<w n="13.7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ppr<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="14" num="2.6" lm="8" met="8"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="14.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="14.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="14.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>t</w>-<w n="14.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="14.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="14.8" punct="pi:8">b<pgtc id="8" weight="0" schema="R"><rhyme label="d" id="8" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pi">a</seg>s</rhyme></pgtc></w> ?</l>
						<l n="15" num="2.7" lm="8" met="8"><w n="15.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="15.2">n</w>’<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="15.4">qu</w>’<w n="15.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="15.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="16" num="2.8" lm="8" met="8"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="16.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="16.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="16.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rd</w> <w n="16.8" punct="pt:8">p<pgtc id="8" weight="0" schema="R"><rhyme label="d" id="8" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="huitain" rhyme="ababcdcd">
						<l n="17" num="3.1" lm="8" met="8"><w n="17.1">H<seg phoneme="o" type="vs" value="1" rule="415" place="1">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="17.4">p<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.5">ch<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg><pgtc id="9" weight="3" schema="CGR">mi<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="18" num="3.2" lm="8" met="8"><w n="18.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="18.2">s</w>’<w n="18.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="18.4">d</w>’<w n="18.5">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="5">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="18.6" punct="vg:8">vi<seg phoneme="e" type="vs" value="1" rule="383" place="7">e</seg>ill<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>rds</rhyme></pgtc></w>,</l>
						<l n="19" num="3.3" lm="8" met="8"><w n="19.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">l</w>’<w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>gl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="19.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="19.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="19.7" punct="vg:8">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg><pgtc id="9" weight="3" schema="CGR">mi<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="20" num="3.4" lm="8" met="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="20.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="20.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="20.4" punct="pt:8">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>rds</rhyme></pgtc></w>.</l>
						<l n="21" num="3.5" lm="8" met="8"><w n="21.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="21.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="21.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="21.4" punct="vg:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg><pgtc id="11" weight="2" schema="CR">dr<rhyme label="c" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="22" num="3.6" lm="8" met="8"><w n="22.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="22.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="22.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="22.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="22.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="22.6" punct="pi:8"><pgtc id="12" weight="2" schema="[CR">p<rhyme label="d" id="12" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pi">a</seg>s</rhyme></pgtc></w> ?</l>
						<l n="23" num="3.7" lm="8" met="8"><w n="23.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="23.2">n</w>’<w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="23.4">qu</w>’<w n="23.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="23.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="11" weight="2" schema="CR">r<rhyme label="c" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="24" num="3.8" lm="8" met="8"><w n="24.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="24.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="24.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="24.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="24.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="24.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rd</w> <w n="24.8" punct="pt:8"><pgtc id="12" weight="2" schema="[CR">p<rhyme label="d" id="12" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="huitain" rhyme="ababcdcd">
						<l n="25" num="4.1" lm="8" met="8"><w n="25.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="25.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="25.3">gl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="25.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="25.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="25.7">gu<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="26" num="4.2" lm="8" met="8"><w n="26.1">Fr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="26.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="26.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="26.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="26.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<pgtc id="14" weight="2" schema="CR">m<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></pgtc></w>,</l>
						<l n="27" num="4.3" lm="8" met="8"><w n="27.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="27.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="27.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="27.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="28" num="4.4" lm="8" met="8"><w n="28.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="28.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="28.4">qu</w>’<w n="28.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="28.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="28.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="28.8" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg><pgtc id="14" weight="2" schema="CR">m<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
						<l n="29" num="4.5" lm="8" met="8"><w n="29.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rç<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="29.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="29.3">f<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="29.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="29.5">s</w>’<w n="29.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg><pgtc id="15" weight="2" schema="CR">pr<rhyme label="c" id="15" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="30" num="4.6" lm="8" met="8"><w n="30.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="30.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="30.3">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="30.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="30.5">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="30.6" punct="ps:8">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ld<pgtc id="16" weight="0" schema="R"><rhyme label="d" id="16" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="ps">a</seg>ts</rhyme></pgtc></w>…</l>
						<l n="31" num="4.7" lm="8" met="8"><w n="31.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="31.2">n</w>’<w n="31.3"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="31.4">qu</w>’<w n="31.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="31.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="15" weight="2" schema="CR">r<rhyme label="c" id="15" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="32" num="4.8" lm="8" met="8"><w n="32.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="32.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="32.3">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="32.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="32.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="32.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="32.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rd</w> <w n="32.8" punct="pt:8">p<pgtc id="16" weight="0" schema="R"><rhyme label="d" id="16" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>