<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AMOUR</head><div type="poem" key="DES364" modus="cm" lm_max="10" metProfile="4+6" form="suite de strophes" schema="2[aa] 2[abab] 2[abaabb]" er_moy="0.86" er_max="8" er_min="0" er_mode="0(11/14)" er_moy_et="2.1">
					<head type="main">TROP TARD</head>
					<lg n="1" type="regexp" rhyme="aaaa">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3" punct="pt:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="pt" caesura="1">é</seg></w>.<caesura></caesura> <w n="1.4">Pr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>v<seg phoneme="wa" type="vs" value="1" rule="440" place="6" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.5"><seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg></w> <w n="1.6" punct="vg:10">l<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>g<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="2.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>x</w> <w n="2.3">cr<seg phoneme="y" type="vs" value="1" rule="454" place="3" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="2.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="2.6">m</w>’<w n="2.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="2.8">s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="2.9">ch<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="3.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="3.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="3.4">m<seg phoneme="o" type="vs" value="1" rule="438" place="4" caesura="1">o</seg>ts</w><caesura></caesura> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="3.6">m</w>’<w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="384" place="7" mp="M">ei</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="3.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="3.9" punct="dp:10">b<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="dp">a</seg>s</rhyme></pgtc></w> :</l>
						<l n="4" num="1.4" lm="10" met="4+6">« <w n="4.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="4.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="4.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="4.4" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="4.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="4.6">m</w>’<w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="4.8" punct="pe:10">p<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="regexp" rhyme="ababab">
						<l n="5" num="2.1" lm="10" met="4+6">« <w n="5.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">m</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="5.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="5.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="C">ou</seg>s</w> <w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="5.8" punct="pv:10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>s<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="3"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="6" num="2.2" lm="10" met="4+6">« <w n="6.1">J<seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="6.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="6.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" caesura="1">oi</seg></w><caesura></caesura> <w n="6.4">n</w>’<w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.6">pl<seg phoneme="a" type="vs" value="1" rule="341" place="6" mp="M">a</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.8" punct="pt:10">b<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>nh<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" stanza="3"><seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pt">eu</seg>r</rhyme></pgtc></w>.</l>
						<l n="7" num="2.3" lm="10" met="4+6">« <w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="7.3">b<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>z<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="7.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6" mp="Lc">eu</seg>t</w>-<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.7" punct="pv:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="355" place="9" mp="M">e</seg>x<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="3"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="8" num="2.4" lm="10" met="4+6">« <w n="8.1">L</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="8.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w> <w n="8.4" punct="dp:4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="4" punct="dp" caesura="1">o</seg>p</w> :<caesura></caesura> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="8.7">v<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>t</w> <w n="8.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="8.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="8.10" punct="pt:10">c<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" stanza="3"><seg phoneme="œ" type="vs" value="1" rule="249" place="10" punct="pt">œu</seg>r</rhyme></pgtc></w>.</l>
						<l n="9" num="2.5" lm="10" met="4+6">« <w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">h<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="9.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="9.4" punct="vg:4">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg" caesura="1">eu</seg>rs</w>,<caesura></caesura> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="9.6">gr<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="9.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.9" punct="pv:10">c<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg><pgtc id="5" weight="2" schema="CR">l<rhyme label="a" id="5" gender="f" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="10" num="2.6" lm="10" met="4+6">« <w n="10.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="10.2">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>rs</w> <w n="10.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="10.4">n</w>’<w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="10.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="10.7" punct="pt:10">p<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>s</rhyme></pgtc></w>. »</l>
					</lg>
					<lg n="3" type="regexp" rhyme="aabb">
						<l n="11" num="3.1" lm="10" met="4+6"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="11.4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="11.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="11.6">m</w>’<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="11.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg></w> <w n="11.9" punct="ps:10"><pgtc id="5" weight="2" schema="[CR">pl<rhyme label="a" id="5" gender="f" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg></rhyme></pgtc></w>…</l>
						<l n="12" num="3.2" lm="10" met="4+6"><w n="12.1">Qu</w>’<w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="12.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="12.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="12.5">t<seg phoneme="o" type="vs" value="1" rule="415" place="4" caesura="1">ô</seg>t</w><caesura></caesura> <w n="12.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="12.7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>x</w> <w n="12.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="12.9">m</w>’<w n="12.10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>cl<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="13" num="3.3" lm="10" met="4+6"><w n="13.1">N</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="Lp">a</seg></w>-<w n="13.3">t</w>-<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="13.5" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="13.6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>s</w> <w n="13.7">fl<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>tt<seg phoneme="ø" type="vs" value="1" rule="403" place="7">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="13.9">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="9">oin</seg>s</w> <w n="13.10" punct="dp:10">b<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="dp">a</seg>s</rhyme></pgtc></w> :</l>
						<l n="14" num="3.4" lm="10" met="4+6">« <w n="14.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="14.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="14.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="14.4" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="14.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="14.6">m</w>’<w n="14.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="14.8" punct="pe:10">p<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pe">a</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="4" type="regexp" rhyme="ababab">
						<l n="15" num="4.1" lm="10" met="4+6">« <w n="15.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">m</w>’<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="15.4" punct="pv:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pv" caesura="1">a</seg>s</w> ;<caesura></caesura> <w n="15.5">l</w>’<w n="15.6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="15.7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg><pgtc id="7" weight="8" schema="CV[C[R" part="1">d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></pgtc></w> <w n="15.8"><pgtc id="7" weight="8" schema="CV[C[R" part="2">l</pgtc></w>’<w n="15.9" punct="pv:10"><pgtc id="7" weight="8" schema="CV[C[R" part="3"><rhyme label="a" id="7" gender="f" type="a" stanza="5"><seg phoneme="a" type="vs" value="1" rule="341" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="16" num="4.2" lm="10" met="4+6">« <w n="16.1">L</w>’<w n="16.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">in</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ct<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="16.4">br<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="16.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>s</w> <w n="16.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="16.8" punct="pt:10">fl<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="m" type="a" stanza="5"><seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</l>
						<l n="17" num="4.3" lm="10" met="4+6">« <w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="17.2" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="3" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="17.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="17.4"><seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="C">i</seg>l</w> <w n="17.5">n</w>’<w n="17.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="17.7">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg>t</w> <w n="17.8"><pgtc id="7" weight="8" schema="[CV[CR" part="1">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></pgtc></w> <w n="17.9" punct="pv:10"><pgtc id="7" weight="8" schema="[CV[CR" part="2">fl<rhyme label="a" id="7" gender="f" type="e" stanza="5"><seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="18" num="4.4" lm="10" met="4+6">« <w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="18.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="18.4">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="4" caesura="1">oi</seg>d</w><caesura></caesura> <w n="18.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>s</w> <w n="18.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="18.7">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="18.8" punct="pt:10">l<seg phoneme="y" type="vs" value="1" rule="d-3" place="9" mp="M">u</seg><pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="m" type="e" stanza="5"><seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</l>
						<l n="19" num="4.5" lm="10" met="4+6">« <w n="19.1">V<seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" mp="M">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="19.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="19.6" punct="vg:10">c<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="a" stanza="6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="20" num="4.6" lm="10" met="4+6">« <w n="20.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="20.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w> <w n="20.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="20.4">br<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="20.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="20.6">v<seg phoneme="o" type="vs" value="1" rule="438" place="9" mp="C">o</seg>s</w> <w n="20.7" punct="pt:10">p<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="a" stanza="6"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>s</rhyme></pgtc></w>. »</l>
					</lg>
					<lg n="5" type="regexp" rhyme="aabb">
						<l n="21" num="5.1" lm="10" met="4+6"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="21.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.3" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="21.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="21.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="21.6">j</w>’<w n="21.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="21.8">cr<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="21.9">s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="21.10" punct="pe:10">t<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="e" stanza="6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
						<l n="22" num="5.2" lm="10" met="4+6"><w n="22.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="22.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="22.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="22.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="22.5">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="22.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="22.7">l</w>’<w n="22.8" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg><pgtc id="9" weight="2" schema="CR" part="1">t<rhyme label="a" id="9" gender="f" type="a" stanza="6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="23" num="5.3" lm="10" met="4+6"><w n="23.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="23.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="23.3">tr<seg phoneme="o" type="vs" value="1" rule="433" place="3">o</seg>p</w> <w n="23.4" punct="vg:4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>rd</w>,<caesura></caesura> <w n="23.5"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="23.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="23.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="23.8">d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="23.9">tr<seg phoneme="o" type="vs" value="1" rule="433" place="9">o</seg>p</w> <w n="23.10" punct="dp:10">b<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="e" stanza="6"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="dp">a</seg>s</rhyme></pgtc></w> :</l>
						<l n="24" num="5.4" lm="10" met="4+6">« <w n="24.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="24.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="24.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="24.4" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">er</seg></w>, <w n="24.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="24.6">m</w>’<w n="24.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="24.8" punct="pe:10">p<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="a" stanza="6"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pe">a</seg>s</rhyme></pgtc></w> ! »</l>
					</lg>
				</div></body></text></TEI>