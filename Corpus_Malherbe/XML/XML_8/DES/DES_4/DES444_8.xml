<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES444" modus="cm" lm_max="12" metProfile="6+6" form="strophe unique" schema="1(aabb)" er_moy="3.0" er_max="6" er_min="0" er_mode="0(1/2)" er_moy_et="3.0">
					<head type="main">LES FLEURS DE JEAN PAUL</head>
					<head type="sub_1">SUR UN ENFANT</head>
					<lg n="1" type="quatrain" rhyme="aabb">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="1.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="1.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="1.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="1.5" punct="vg:6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>rs</w>,<caesura></caesura> <w n="1.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="1.7" punct="vg:8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>rs</w>, <w n="1.8">j<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="1.9" punct="pe:12">pl<pgtc id="1" weight="6" schema="VCR"><seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="M">eu</seg>r<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg>s</rhyme></pgtc></w> !</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="2.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="2.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="2.7" punct="pt:12">h<pgtc id="1" weight="6" schema="VCR"><seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="M">eu</seg>r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="3.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="Lc">en</seg>tr</w>’<w n="3.5"><seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M/mc">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="3.7">s</w>’<w n="3.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.9" punct="dp:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="dp">an</seg>t</rhyme></pgtc></w> :</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="pe:2">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="pe">ez</seg></w> ! <w n="4.2">c</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="Lc">ou</seg>rd</w>’<w n="4.5">hu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="4.7">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="4.9">l</w>’<w n="4.10" punct="pe:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>f<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pe">an</seg>t</rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>