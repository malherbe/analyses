<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RONDELS POUR APRÈS</head><div type="poem" key="CRB100" modus="cm" lm_max="10" metProfile="5+5" form="rondel relâché" schema="Abba abA abbaa A" er_moy="2.0" er_max="6" er_min="0" er_mode="0(4/7)" er_moy_et="2.62">
					<head type="main">MALE-FLEURETTE</head>
					<lg n="1" rhyme="Abba">
						<l n="1" num="1.1" lm="10" met="5+5"><hi rend="ital"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="1.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg></w><caesura></caesura> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="1.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="M">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.5">bl<pgtc id="1" weight="0" schema="R"><rhyme label="A" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></hi></l>
						<l n="2" num="1.2" lm="10" met="5+5"><hi rend="ital"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="2.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="5" caesura="1">eau</seg>x</w><caesura></caesura> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</w> <w n="2.6" punct="ps:10">p<pgtc id="2" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ss<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="ps">é</seg>s</rhyme></pgtc></w>…</hi></l>
						<l n="3" num="1.3" lm="10" met="5+5"><hi rend="ital"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="3.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>rs</w> <w n="3.4" punct="vg:5"><seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" punct="vg" caesura="1">e</seg>rts</w>,<caesura></caesura> <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="P">u</seg>r</w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.7"><seg phoneme="o" type="vs" value="1" rule="432" place="8">o</seg>s</w> <w n="3.8" punct="vg:10">t<pgtc id="2" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ss<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg>s</rhyme></pgtc></w>,</hi></l>
						<l n="4" num="1.4" lm="10" met="5+5"><hi rend="ital"><w n="4.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="4.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="4.3" punct="vg:5">br<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg" caesura="1">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="4.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="4.6" punct="vg:8">j<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</w>, <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="4.8" punct="ps:10">s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg></rhyme></pgtc></w>…</hi></l>
					</lg>
					<lg n="2" rhyme="abA">
						<l n="5" num="2.1" lm="10" met="5+5"><hi rend="ital"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="5.2">cr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.3" punct="pv:5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="5" punct="pv" caesura="1">u</seg>s</w> ;<caesura></caesura> <w n="5.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="5.5">l</w>’<w n="5.6"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.7" punct="vg:10">m<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</hi></l>
						<l n="6" num="2.2" lm="10" met="5+5"><hi rend="ital"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="6.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="353" place="3" mp="M">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4" mp="M">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="6.5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="7">en</seg>s</w> <w n="6.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8" mp="Lc">è</seg>s</w>-<w n="6.7" punct="ps:10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M/mc">en</seg><pgtc id="4" weight="2" schema="CR">s<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="ps">é</seg>s</rhyme></pgtc></w>…</hi></l>
						<l n="7" num="2.3" lm="10" met="5+5"><hi rend="ital"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg></w><caesura></caesura> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="7.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="M">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.5" punct="pt:10">bl<pgtc id="3" weight="0" schema="R"><rhyme label="A" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</hi></l>
					</lg>
					<lg n="3" rhyme="abbaa">
						<l n="8" num="3.1" lm="10" met="5+5">— <hi rend="ital"><w n="8.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3">cr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>s</w><caesura></caesura> <w n="8.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="8.6">h<seg phoneme="œ̃" type="vs" value="1" rule="261" place="7">um</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>th<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></hi></l>
						<l n="9" num="3.2" lm="10" met="5+5"><hi rend="ital"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="9.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2" mp="C">o</seg>s</w> <w n="9.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="9.4" punct="vg:5">m<seg phoneme="y" type="vs" value="1" rule="445" place="5" punct="vg" caesura="1">û</seg>rs</w>,<caesura></caesura> <w n="9.5" punct="pe:10">C<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="M">u</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>rb<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="4" weight="2" schema="CR">c<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pe">é</seg>s</rhyme></pgtc></w> !</hi></l>
						<l n="10" num="3.3" lm="10" met="5+5"><hi rend="ital"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="10.2">c<seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">aî</seg>t</w> <w n="10.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5" caesura="1">en</seg></w><caesura></caesura> <w n="10.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="10.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="10.6" punct="pe:10">tr<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>p<pgtc id="4" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ss<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pe">é</seg>s</rhyme></pgtc></w> !</hi></l>
						<l n="11" num="3.4" lm="10" met="5+5"><hi rend="ital"><w n="11.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="11.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="11.4" punct="vg:5">t<seg phoneme="y" type="vs" value="1" rule="457" place="5" punct="vg" caesura="1">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fc">e</seg></w> <w n="11.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="11.7">qu</w>’<w n="11.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="11.9">l</w>’<w n="11.10" punct="ps:10"><pgtc id="5" weight="0" schema="[R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="10">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ps" mp="F">e</seg></rhyme></pgtc></w>…</hi></l>
						<l n="12" num="3.5" lm="10" met="5+5">— <hi rend="ital"><w n="12.1" punct="tc:10">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="12.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fm">e</seg></w>-<w n="12.5" punct="vg:5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="12.7">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="12.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="12.9" punct="pt:10">b<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>h<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="ti" mp="F">e</seg></rhyme></pgtc></w>.</hi> —</l>
					</lg>
					<lg n="4" rhyme="A">
						<l n="13" num="4.1" lm="10" met="5+5"><hi rend="ital"><w n="13.1" punct="pt:10"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="13.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg></w><caesura></caesura> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="13.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="M">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="13.5">bl<pgtc id="6" weight="0" schema="R"><rhyme label="A" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w></hi>.</l>
					</lg>
				</div></body></text></TEI>