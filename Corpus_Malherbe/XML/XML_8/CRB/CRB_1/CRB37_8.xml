<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SÉRÉNADE DES SÉRÉNADES</head><div type="poem" key="CRB37" modus="sp" lm_max="8" metProfile="8, 4" form="suite périodique" schema="5(aabb)" er_moy="0.4" er_max="2" er_min="0" er_mode="0(8/10)" er_moy_et="0.8">
					<head type="main">GUITARE</head>
					<lg n="1" type="quatrain" rhyme="aabb">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="1.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="1.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="1" weight="2" schema="CR">r<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="4" met="4"><space quantity="12" unit="char"></space><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="2.2" punct="vg:4">c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg><pgtc id="1" weight="2" schema="CR">r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="3.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="3.4">l</w>’<w n="3.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="3.8" punct="pe:8">pl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>ts</rhyme></pgtc></w> !</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>x</w> <w n="4.7" punct="pe:8">dr<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>ps</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="aabb">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>s</w> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="5.6" punct="dp:8">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>d<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg>s</rhyme></pgtc></w> :</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="6.2">m<seg phoneme="y" type="vs" value="1" rule="445" place="2">û</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="6.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="6.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="5">e</seg>ds</w> <w n="6.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="6.7" punct="pv:8"><pgtc id="3" weight="0" schema="[R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">V<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3" punct="vg:4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg>t</w>, <w n="7.4">h<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>b<seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="7.5">d</w>’<w n="7.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
						<l n="8" num="2.4" lm="4" met="4"><space quantity="12" unit="char"></space><w n="8.1">M</w>’<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="8.4" punct="pt:4">j<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="aabb">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">C<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w>-<w n="9.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="9.3" punct="pi:5">Ps<seg phoneme="i" type="vs" value="1" rule="493" place="4">y</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="pi ti">é</seg></w> ? — <w n="9.4" punct="pi:6">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pi ti">on</seg></w> ? — <w n="9.5" punct="pi:8">M<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rc<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi ps">e</seg></rhyme></pgtc></w> ?…</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">C<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>dr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="10.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="10.4" punct="pi:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
						<l n="11" num="3.3" lm="8" met="8">— <w n="11.1" punct="pi:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="pi ti ps">on</seg></w> ? — … <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="133" place="2">E</seg>h</w> <w n="11.3" punct="pe:3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3" punct="pe">en</seg></w> ! <w n="11.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="11.5" punct="vg:6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></w>, <w n="11.6">c</w>’<w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="11.8" punct="dp:8">m<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="dp">oi</seg></rhyme></pgtc></w> :</l>
						<l n="12" num="3.4" lm="4" met="4"><space quantity="12" unit="char"></space><w n="12.1">N<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>l</w> <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4" punct="pt:4">v<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pt">oi</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="aabb">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="13.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w> <w n="13.6">fr<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="14.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="14.4">J<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="14.6" punct="vg:8">cr<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="15.4" punct="ps:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>scr<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="ps">e</seg>t</rhyme></pgtc></w>…</l>
						<l n="16" num="4.4" lm="4" met="4"><space quantity="12" unit="char"></space> — <w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="16.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="16.4" punct="pe:4">l<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="pe ti">ai</seg>d</rhyme></pgtc></w> ! —</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="aabb">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="17.3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="17.5" punct="vg:8">c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="9" weight="2" schema="CR">r<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="4" met="4"><space quantity="12" unit="char"></space><w n="18.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg><pgtc id="9" weight="2" schema="CR">r<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">Ch<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ff<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="19.3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="19.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="19.5" punct="vg:8">dr<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>ps</rhyme></pgtc></w>,</l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">M<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="20.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="20.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="20.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="20.6" punct="pe:8">pl<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>ts</rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>