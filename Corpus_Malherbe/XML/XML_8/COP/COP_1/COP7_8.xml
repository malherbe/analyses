<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PROMENADES ET INTÉRIEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1057 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">COP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PROMENADES ET INTÉRIEURS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ebooksgratuits.com</publisher>
						<idno type="URL">http://www.ebooksgratuits.com/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Promenades et intérieurs</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Lemerre</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La date de création est celle donnée par l’Académie Française pour le recueil "Les Humbles"</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><div type="poem" key="COP7" modus="cm" lm_max="10" metProfile="5+5" form="suite périodique" schema="4(abab)" er_moy="2.25" er_max="6" er_min="0" er_mode="2(6/8)" er_moy_et="1.56">
					<head type="main">Ritournelle</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="1.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="1.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="1.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="1.8" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">ll<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="2.2">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="2.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345" place="5" caesura="1">ue</seg>il</w><caesura></caesura> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg></w> <w n="2.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>x</w> <w n="2.7" punct="vg:10">m<seg phoneme="e" type="vs" value="1" rule="353" place="8" mp="M">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg><pgtc id="2" weight="2" schema="CR">d<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10" punct="vg">o</seg>r</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="3.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="3.5">ch<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="3.6" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="4.3" punct="vg:3">str<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="vg">o</seg>ph<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="4.5" punct="vg:5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="4.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>s</w> <w n="4.8"><pgtc id="2" weight="2" schema="[C[R" part="1">d</pgtc></w>’<w n="4.9" punct="pt:10"><pgtc id="2" weight="2" schema="[C[R" part="2"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10" punct="pt">o</seg>r</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="10" met="5+5"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="5.3">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="3" mp="M">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>s</w><caesura></caesura> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="5.5">r<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="5.6" punct="vg:10">t<pgtc id="3" weight="6" schema="VCR" part="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="6.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="6.4">gr<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>s</w><caesura></caesura> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="6.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="6.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="6.8" punct="vg:10">r<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg><pgtc id="4" weight="2" schema="CR" part="1">s<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="vg">eau</seg>x</rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="10" met="5+5"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="7.2">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="7.5">ch<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="7.6" punct="vg:10">ch<pgtc id="3" weight="6" schema="VCR" part="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>t<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="8" num="2.4" lm="10" met="5+5"><w n="8.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3" punct="vg:3">r<seg phoneme="i" type="vs" value="1" rule="493" place="3" punct="vg">y</seg>thm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="8.5" punct="vg:5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="8.7">ch<seg phoneme="œ" type="vs" value="1" rule="249" place="7">œu</seg>r</w> <w n="8.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="8.9" punct="pt:10"><seg phoneme="wa" type="vs" value="1" rule="420" place="9" mp="M">oi</seg><pgtc id="4" weight="2" schema="CR" part="1">s<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="pt">eau</seg>x</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="10" met="5+5"><w n="9.1">Su<seg phoneme="i" type="vs" value="1" rule="491" place="1" mp="M">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="9.4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5" caesura="1">eu</seg>x</w><caesura></caesura> <w n="9.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="9.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="9.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<pgtc id="5" weight="2" schema="CR" part="1">m<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>t</w><caesura></caesura> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="10.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="10.7">fl<seg phoneme="o" type="vs" value="1" rule="438" place="8">o</seg>ts</w> <w n="10.8" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rl<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="vg">eu</seg>rs</rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="10" met="5+5"><w n="11.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="11.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="11.3" punct="vg:5">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg" caesura="1">on</seg>s</w>,<caesura></caesura> <w n="11.4">ch<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="11.5" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg><pgtc id="5" weight="2" schema="CR" part="1">m<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="12" num="3.4" lm="10" met="5+5"><w n="12.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="12.2">gl<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="12.4" punct="vg:5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5" punct="vg" caesura="1">e</seg>rs</w>,<caesura></caesura> <w n="12.5" punct="vg:6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg">oi</seg></w>, <w n="12.6">c<seg phoneme="œ" type="vs" value="1" rule="345" place="7" mp="M">ue</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="12.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="12.8" punct="pt:10">fl<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="10" met="5+5"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">l</w>’<w n="13.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>r</w>, <w n="13.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>t</w><caesura></caesura> <w n="13.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fc">e</seg></w> <w n="13.6" punct="vg:10">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg><pgtc id="7" weight="2" schema="CR" part="1">s<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="4.2" lm="10" met="5+5"><w n="14.1" punct="vg:2">F<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg></w>, <w n="14.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="14.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="Lp">ou</seg>r</w>-<w n="14.4" punct="vg:5">l<seg phoneme="a" type="vs" value="1" rule="342" place="5" punct="vg" caesura="1">à</seg></w>,<caesura></caesura> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="14.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="14.8" punct="dp:10">ch<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<pgtc id="8" weight="2" schema="CR" part="1">m<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="dp">an</seg>t</rhyme></pgtc></w> :</l>
						<l n="15" num="4.3" lm="10" met="5+5"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="15.3" punct="vg:5">p<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5" punct="vg" caesura="1">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="15.5">t<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w> <w n="15.6" punct="pv:10">p<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg><pgtc id="7" weight="2" schema="CR" part="1">s<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="16" num="4.4" lm="10" met="5+5"><w n="16.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="16.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="16.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="16.4" punct="vg:5">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" punct="vg" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="16.6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w> <w n="16.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="16.8" punct="pt:10"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="9" mp="M">ai</seg><pgtc id="8" weight="2" schema="CR" part="1">m<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>