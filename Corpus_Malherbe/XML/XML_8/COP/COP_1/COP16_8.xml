<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PROMENADES ET INTÉRIEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1057 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">COP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PROMENADES ET INTÉRIEURS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ebooksgratuits.com</publisher>
						<idno type="URL">http://www.ebooksgratuits.com/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Promenades et intérieurs</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Lemerre</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La date de création est celle donnée par l’Académie Française pour le recueil "Les Humbles"</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><div type="poem" key="COP16" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="6(abab)" er_moy="1.5" er_max="6" er_min="0" er_mode="2(6/12)" er_moy_et="1.66">
					<head type="main">Février</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="1.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w>-<w n="1.3" punct="vg:4">t<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg></w>, <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.5">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="1.6">n<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.3">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>l</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg><pgtc id="2" weight="2" schema="C[R" part="1">s</pgtc></w> <w n="2.6" punct="pv:8"><pgtc id="2" weight="2" schema="C[R" part="2"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pv">eau</seg>x</rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="3.4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="3.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="3.7" punct="vg:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>t<pgtc id="1" weight="0" schema="R" part="1"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="4.3">n</w>’<w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="4.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="4.6">d</w>’<w n="4.7" punct="pe:8"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg><pgtc id="2" weight="2" schema="CR" part="1">s<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pe">eau</seg>x</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="5.2" punct="vg:4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="5.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.4" punct="pv:8">p<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg><pgtc id="3" weight="2" schema="CR" part="1">r<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="6.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="6.4">n</w>’<w n="6.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="6.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>t</w> <w n="6.7" punct="pt:8">p<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="4" weight="2" schema="CR" part="1">r<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg></rhyme></pgtc></w>.</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.5">r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.6"><pgtc id="3" weight="2" schema="[CR" part="1">cr<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="8.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="8.4">ch<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>d</w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.6">s<seg phoneme="y" type="vs" value="1" rule="445" place="6">û</seg>r</w> <w n="8.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="4" weight="2" schema="CR" part="1">br<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="9.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="9.4">l</w>’<w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="9.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="9.7">l</w>’<w n="9.8"><pgtc id="5" weight="0" schema="[R" part="1"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">bl<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.6" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>bsc<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>r</rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">Pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>s</w> <w n="11.2">d</w>’<w n="11.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>il</w> <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="11.7" punct="vg:8">n<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="12.3">l</w>’<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">A</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="12.5" punct="pv:8">f<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pv">u</seg>r</rhyme></pgtc></w> ;</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="13.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.4">b<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="13.6">p<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="14.3">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="14.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> <w n="14.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="14.7" punct="vg:8">fr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="8" weight="2" schema="CR" part="1">ss<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>s</rhyme></pgtc></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="15.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="15.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>x</w> <w n="15.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="15.6">b<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="16.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="5">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="16.4" punct="pt:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg><pgtc id="8" weight="2" schema="CR" part="1">s<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1" punct="vg:2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="17.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="17.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="9" weight="2" schema="CR" part="1">r<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="18.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="18.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="18.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="18.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg><pgtc id="10" weight="6" schema="CVR" part="1">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>t</rhyme></pgtc></w>,</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="19.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.3">t</w>’<w n="19.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="19.5" punct="vg:8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg><pgtc id="9" weight="2" schema="CR" part="1">tr<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="20.3">s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>lcr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="20.5" punct="pv:8"><pgtc id="10" weight="6" schema="[CVR" part="1">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pv">e</seg>t</rhyme></pgtc></w> ;</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="21.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="21.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="21.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>r</w> <w n="21.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd</w> <w n="21.6"><seg phoneme="i" type="vs" value="1" rule="497" place="7">y</seg></w> <w n="21.7" punct="vg:8"><pgtc id="11" weight="2" schema="[CR" part="1">t<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="22.2">p<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="22.3">qu</w>’<w n="22.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="22.5">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="22.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="22.7" punct="vg:8">j<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="23.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="23.3">j<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="23.6"><pgtc id="11" weight="2" schema="[CR" part="1">t<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="24.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="24.3"><seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4">aim</seg></w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="24.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="24.6">d</w>’<w n="24.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>