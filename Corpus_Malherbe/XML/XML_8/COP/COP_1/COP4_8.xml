<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PROMENADES ET INTÉRIEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1057 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">COP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PROMENADES ET INTÉRIEURS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ebooksgratuits.com</publisher>
						<idno type="URL">http://www.ebooksgratuits.com/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Promenades et intérieurs</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Lemerre</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La date de création est celle donnée par l’Académie Française pour le recueil "Les Humbles"</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><div type="poem" key="COP4" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="7(abab)" er_moy="1.14" er_max="6" er_min="0" er_mode="0(8/14)" er_moy_et="1.64">
					<head type="main">Morceau à quatre mains</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="1.3">s</w>’<w n="1.4"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.7">p<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rc</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="2.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ds</w> <w n="2.4" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="2.5">d</w>’<w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="2.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt</w> <w n="2.8" punct="vg:8">s<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="3.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="3.3">r<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>x</w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="3.5"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rc</rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ls</w> <w n="4.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>nt</w> <w n="4.7">d</w>’<w n="4.8" punct="pt:8"><pgtc id="2" weight="0" schema="[R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="3" weight="2" schema="CR">d<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8">ain</seg></rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406" place="4">eu</seg>il</w> <w n="6.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="6.5">j</w>’<w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="6.7">pr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="6.8" punct="vg:8">pl<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.5">j<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<pgtc id="3" weight="2" schema="CR">d<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="8.6" punct="pv:8">gl<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">g<seg phoneme="u" type="vs" value="1" rule="425" place="3">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.4">l</w>’<w n="9.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="5" weight="2" schema="CR">m<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8">en</seg>t</rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">D</w>’<w n="10.2" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>r</w>, <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="10.4">g<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="10.7" punct="vg:8">dr<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">D<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="11.2" punct="vg:2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rcs</w>, <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>ils</w> <w n="11.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bs<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg><pgtc id="5" weight="2" schema="CR">m<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="12.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="12.6">gl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>tr<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="13.3">j<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="13.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="13.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="13.6" punct="vg:8">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="7" weight="2" schema="CR">s<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>rd</rhyme></pgtc></w>,</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="14.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="14.4" punct="vg:5">s<seg phoneme="œ" type="vs" value="1" rule="249" place="5" punct="vg">œu</seg>rs</w>, <w n="14.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="14.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>xqu<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="15.2">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="2">ou</seg><seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="15.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="15.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.6" punct="vg:8">M<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="7" weight="2" schema="CR">z<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>rt</rhyme></pgtc></w>,</l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="16.2">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg></w> <w n="16.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="16.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="17.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="17.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rcs</w> <w n="17.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="17.6" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>r</rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="18.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="18.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="18.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="18.6" punct="pv:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="10" weight="2" schema="CR">r<rhyme label="b" id="10" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2">qu<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="19.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="19.4">b<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>x</w> <w n="19.5">d</w>’<w n="19.6"><pgtc id="9" weight="0" schema="[R"><rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</rhyme></pgtc></w></l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">Sc<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="20.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="20.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="20.4">qu<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="20.5" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="10" weight="2" schema="CR">r<rhyme label="b" id="10" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abab">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1">J</w>’<w n="21.2"><seg phoneme="e" type="vs" value="1" rule="354" place="1">e</seg>x<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="21.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="21.5">j<pgtc id="11" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></pgtc></w> <w n="21.6" punct="vg:8"><pgtc id="11" weight="6" schema="V[CR" part="2">v<rhyme label="a" id="11" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1">Gr<seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="22.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="22.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="22.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="22.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="22.7" punct="vg:8">t<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="23.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="23.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="23.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="23.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="23.6" punct="vg:8">ch<pgtc id="11" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<rhyme label="a" id="11" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="24.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="24.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="24.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="24.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="24.6">d<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="24.7" punct="pv:8">b<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="abab">
						<l n="25" num="7.1" lm="8" met="8"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="25.2" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>s</w>, <w n="25.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="25.4">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="25.5" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<pgtc id="13" weight="2" schema="CR" part="1">d<rhyme label="a" id="13" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></pgtc></w>,</l>
						<l n="26" num="7.2" lm="8" met="8"><w n="26.1">B<seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p</w> <w n="26.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="26.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="26.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="26.5">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="26.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>t<pgtc id="14" weight="0" schema="R" part="1"><rhyme label="b" id="14" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="27" num="7.3" lm="8" met="8"><w n="27.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="27.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>s</w> <w n="27.6">m</w>’<w n="27.7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="13" weight="2" schema="CR" part="1">d<rhyme label="a" id="13" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></rhyme></pgtc></w></l>
						<l n="28" num="7.4" lm="8" met="8"><w n="28.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="28.2">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg></w> <w n="28.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="28.4" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<pgtc id="14" weight="0" schema="R" part="1"><rhyme label="b" id="14" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>