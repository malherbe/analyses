<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DES VERS FRANÇAIS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2263 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DES VERS FRANÇAIS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppeedesversfrancais.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1906">1906</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP185" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="7(abba)" er_moy="1.14" er_max="2" er_min="0" er_mode="2(8/14)" er_moy_et="0.99">
				<head type="main">Chauvinisme</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="1.3" punct="pt:3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" punct="pt">en</seg>s</w>. <w n="1.4">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="1.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd</rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="2.4" punct="pt:8">h<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J</w>’<w n="3.2" punct="vg:1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1" punct="vg">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg></w> <w n="3.4" punct="vg:4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="3.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.7"><pgtc id="2" weight="2" schema="[CR">t<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="4.2" punct="vg:3">ch<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg></w>, <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="4.4" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="dc-1" place="6">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>rd</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abba">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="5.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5">ein</seg>s</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.5" punct="vg:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>p<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.4">cr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="6.6">l</w>’<w n="6.7" punct="vg:8"><pgtc id="14" weight="0" schema="[R"><rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>r</rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">h<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="7.4">m<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>t</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.6" punct="dp:8">Sch<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="dp">er</seg></rhyme></pgtc></w> :</l>
					<l n="8" num="2.4" lm="8" met="8">« <w n="8.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="8.2">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="8.3">c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="363" place="6">en</seg>s</w> <w n="8.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="8.5" punct="pe:8">m<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abba">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">l</w>’<w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="9.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="4" weight="2" schema="CR">j<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="8">à</seg></rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">S</w>’<w n="10.2" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">er</seg></w>, <w n="10.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.4">cl<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="10.5" punct="pt:8">v<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="vg">o</seg>rs</w>, <w n="11.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="11.5">fl<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>t</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.7" punct="vg:8">h<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="12.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="12.4" punct="pt:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>bm<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="4" weight="2" schema="CR">ge<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="316" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abba">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="13.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="13.3">qu</w>’<w n="13.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="13.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="6" weight="2" schema="CR">mm<rhyme label="a" id="6" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="14.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="14.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="14.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="14.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="14.7" punct="pt:8">d<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>x</rhyme></pgtc></w>.</l>
					<l n="15" num="4.3" lm="8" met="8">« <w n="15.1">P<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="15.2" punct="vg:4">fr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</w>-<w n="15.4" punct="pe:8">n<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pe">ou</seg>s</rhyme></pgtc></w> ! »</l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="16.3" punct="pe:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="6" weight="2" schema="CR">m<rhyme label="a" id="6" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abba">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">C<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="17.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="17.6">tr<seg phoneme="o" type="vs" value="1" rule="433" place="6">o</seg>p</w> <w n="17.7" punct="pt:8">v<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="8" weight="2" schema="CR">c<rhyme label="a" id="8" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg></rhyme></pgtc></w>.</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-6" place="2">e</seg>r</w> <w n="18.3">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="4">y</seg>s</w> <w n="18.4">s</w>’<w n="18.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="18.7" punct="pv:8">r<pgtc id="9" weight="0" schema="R"><rhyme label="b" id="9" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="19.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3" punct="vg:5">c<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="19.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="19.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="19.6" punct="vg:8">h<pgtc id="9" weight="0" schema="R"><rhyme label="b" id="9" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="20.2" punct="vg:3">fr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>s</w>, <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg></w> <w n="20.4">dr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="20.5" punct="pe:8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg><pgtc id="8" weight="2" schema="CR">c<rhyme label="a" id="8" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pe">u</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abba">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="21.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="21.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="21.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="21.5" punct="vg:8">t<seg phoneme="y" type="vs" value="1" rule="463" place="7">u</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg><pgtc id="10" weight="2" schema="CR">r<rhyme label="a" id="10" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="22.2">rh<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="22.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="22.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="11" weight="2" schema="CR">l<rhyme label="b" id="11" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</rhyme></pgtc></w></l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="23.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="23.3">qu</w>’<w n="23.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="23.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="23.6">f<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t</w> <w n="23.7"><pgtc id="11" weight="2" schema="[CR">pl<rhyme label="b" id="11" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</rhyme></pgtc></w></l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="24.3">n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.5" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="10" weight="2" schema="CR">tr<rhyme label="a" id="10" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abba">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1" punct="pe:3">Ch<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe">e</seg></w> ! <w n="25.2">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="25.3" punct="pe:6">cr<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pe">eu</seg>x</w> ! <w n="25.4" punct="pe:8">R<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="12" weight="2" schema="CR">m<rhyme label="a" id="12" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pe ps">an</seg></rhyme></pgtc></w> !…</l>
					<l n="26" num="7.2" lm="8" met="8">« <w n="26.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="26.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>c</w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="26.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="26.5">m<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="26.6">pl<pgtc id="13" weight="0" schema="R"><rhyme label="b" id="13" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="27.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="27.3" punct="tc:3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3" punct="vg ti">œu</seg>r</w>, — <w n="27.4" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>s</w>, <w n="27.5">l</w>’<w n="27.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="27.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="27.8" punct="tc:8">p<pgtc id="13" weight="0" schema="R"><rhyme label="b" id="13" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg ti">e</seg></rhyme></pgtc></w>, —</l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="28.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="28.3"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="28.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="28.5" punct="pi:8">m<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="12" weight="2" schema="CR">m<rhyme label="a" id="12" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pi">an</seg></rhyme></pgtc></w> ? »</l>
				</lg>
			</div></body></text></TEI>