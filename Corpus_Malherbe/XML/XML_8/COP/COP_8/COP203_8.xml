<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES DIVERS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>626 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2019</date>
				<idno type="local">COP_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes Divers II</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/francoiscopeepoemesdivers2.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>François Coppée</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie ALEXANDRE HOUSSIAUX</publisher>
									<date when="1885">1885</date>
								</imprint>
								<biblScope unit="tome">Poésie, tome 1</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP203" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="7(abab)" er_moy="0.71" er_max="2" er_min="0" er_mode="0(9/14)" er_moy_et="0.96">
				<head type="main">LA MORT DU SINGE</head>
				<opener>
					<salute>A Ernest D’Hervilly.</salute>
				</opener>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Fr<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="1.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.5" punct="vg:8">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="192" place="8">oe</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1" punct="vg:2">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="vg">é</seg></w>, <w n="2.2">f<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="2.4" punct="vg:8">m<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>b<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>d</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="3.3" punct="vg:4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="3.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="3.7" punct="vg:8">p<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="158" place="8">oê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="4.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="4.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.6" punct="pt:8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rf<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>d</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1" punct="vg:3">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="5.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.4" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>tr<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="6.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="6.6" punct="vg:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="4" weight="2" schema="CR">r<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="7.2" punct="dp:2">g<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="2" punct="dp">ein</seg>t</w> : <w n="7.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="7.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="7.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.6">s</w>’<w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>dr<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="8.2">p<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c</w> <w n="8.3">qu</w>’<w n="8.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="8.5">v<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>t</w> <w n="8.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg><pgtc id="4" weight="2" schema="CR">dr<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>r</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">phth<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.5">dr<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="341" place="1">â</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="10.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.4">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="5">ei</seg>g<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="10.5" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="384" place="7">ei</seg>gn<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>rs</rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="11.2" punct="vg:2">j<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2" punct="vg">oin</seg>t</w>, <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>rs</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="11.7" punct="pt:8">f<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="12.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ts</w> <w n="12.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>gts</w> <w n="12.4">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6" punct="pv:8">n<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pv">oi</seg>rs</rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="13.3" punct="vg:3">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>rs</w>, <w n="13.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="13.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="13.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.7">f<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">D<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="14.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="14.5">p<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>ls</w> <w n="14.6" punct="vg:8"><pgtc id="8" weight="2" schema="[CR">r<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>x</rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="15.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="15.3">n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="15.5">gr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.3">r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.6" punct="pt:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="8" weight="2" schema="CR">rr<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>x</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">Vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="1">e</seg>il</w> <w n="17.2" punct="vg:4">h<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>str<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="17.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg></w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="17.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="17.6" punct="vg:8">pl<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="18.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="18.3">n</w>’<w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="18.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="18.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>gr<seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg><pgtc id="10" weight="2" schema="CR">tt<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></pgtc></w></l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>ds</w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="19.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="19.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="19.6" punct="vg:8">br<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">L</w>’<w n="20.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="20.3" punct="vg:4"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="20.5" punct="pe:8">l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="10" weight="2" schema="CR">t<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="21.4" punct="vg:5">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="21.5">c</w>’<w n="21.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="21.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="21.8"><pgtc id="11" weight="2" schema="CR">sc<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="22.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="22.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="22.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="22.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.7">f<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>r</rhyme></pgtc></w></l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="23.2" punct="vg:3">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3" punct="vg">e</seg>l</w>, <w n="23.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="23.4" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>b<pgtc id="11" weight="2" schema="CR">sc<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1">Gr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="24.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="24.3" punct="vg:4">p<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>l</w>, <w n="24.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="24.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="24.6" punct="vg:8">ch<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>r</rhyme></pgtc></w>,</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1" punct="vg:2">R<seg phoneme="a" type="vs" value="1" rule="307" place="1">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>r</w>, <w n="25.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="25.3">f<seg phoneme="œ" type="vs" value="1" rule="304" place="4">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="25.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="25.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>x</w> <w n="25.6">h<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">qu</w>’<w n="26.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="26.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="26.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="26.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="26.7"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="26.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="26.9" punct="vg:8"><pgtc id="14" weight="2" schema="[CR">l<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>d</rhyme></pgtc></w>,</l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="27.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="27.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="27.4">tr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="27.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="27.6">p<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="28.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="28.3">c<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="28.4">t</w>’<w n="28.5" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="14" weight="2" schema="CR">bl<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pe">ai</seg>t</rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>