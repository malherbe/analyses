<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP55" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 1" schema="abba abba ccd eed" er_moy="1.43" er_max="2" er_min="0" er_mode="2(5/7)" er_moy_et="0.9">
				<head type="main">Désespérément</head>
				<opener>
					<salute>A Henry Cazalis</salute>
				</opener>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="M">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="3" mp="M">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg></w>, <w n="1.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="1.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>ls</w><caesura></caesura> <w n="1.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">â</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="1.9" punct="vg:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="2.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="2.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" mp="M">in</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="2.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="2.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="2.9">l</w>’<w n="2.10" punct="vg:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>b<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg>s</w> <w n="3.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="3.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5" mp="M">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6" caesura="1">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="3.8" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>cr<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>p<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="4.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>s</w><caesura></caesura> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.7">g<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="4.9" punct="pt:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="5.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="5.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8" punct="vg">ein</seg>ts</w>, <w n="5.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="Fc">e</seg></w> <w n="5.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">â</seg><pgtc id="3" weight="2" schema="CR">l<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="6.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="6.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="6.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="6.8">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="9">en</seg></w> <w n="6.9">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="6.10" punct="pv:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<pgtc id="4" weight="2" schema="CR">p<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="7.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="7.7">m</w>’<w n="7.8"><seg phoneme="i" type="vs" value="1" rule="497" place="9">y</seg></w> <w n="7.9">pr<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="4" weight="2" schema="CR">p<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="8.2">qu</w>’<w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="8.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rt</w><caesura></caesura> <w n="8.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.7">n</w>’<w n="8.8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="8.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="8.10"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="8.11" punct="pt:12"><pgtc id="3" weight="2" schema="[CR">fl<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="vg:2">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="9.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" mp="P">e</seg>rs</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="9.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="9.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.6">br<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="9.7" punct="vg:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg><pgtc id="5" weight="2" schema="CR">ff<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="10.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="M">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="10.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="10.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="9">en</seg>t</w> <w n="10.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="P">a</seg>r</w> <w n="10.7" punct="pt:12">b<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="5" weight="2" schema="CR">ff<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">J</w>’<w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r</w> <w n="11.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="11.6" punct="vg:6">p<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>r</w>,<caesura></caesura> <w n="11.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="11.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="11.10">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>l</w> <w n="11.11">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="11.12" punct="pt:12"><pgtc id="6" weight="2" schema="[CR">b<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" rhyme="eed">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="12.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="12.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="12.5">m</w>’<w n="12.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="12.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="12.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="12.9" punct="vg:12">pr<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>s<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="13.4">n</w>’<w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="13.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="13.7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="13.8">qu</w>’<w n="13.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="13.10"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="13.11">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="13.12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="14.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="14.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="14.5" punct="vg:6">n<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>d</w>,<caesura></caesura> <w n="14.6">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="7" mp="M">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="14.8" punct="pt:12">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg><pgtc id="6" weight="2" schema="CR">b<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg></rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>