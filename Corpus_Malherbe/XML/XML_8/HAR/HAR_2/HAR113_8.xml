<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURE</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><head type="main_subpart">L’AUBE</head><div type="poem" key="HAR113" modus="cm" lm_max="12" metProfile="6+6" form="suite de strophes" schema="1[ababcbc]" er_moy="0.5" er_max="2" er_min="0" er_mode="0(3/4)" er_moy_et="0.87">
						<lg n="1" type="regexp" rhyme="aba">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="1.3">v<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-42">e</seg></w> <w n="1.4">h<seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="M">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6" caesura="1">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="1.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="1.8" punct="dp:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>gr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="dp">e</seg>ts</rhyme></pgtc></w> :</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>s</w>, <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="2.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="C">ou</seg>s</w> <w n="2.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="2.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</w> <w n="2.9">n<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="C">ou</seg>s</w> <w n="2.10" punct="vg:12"><pgtc id="2" weight="2" schema="[CR">l<rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="3.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="3" mp="C">o</seg>s</w> <w n="3.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="3.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>x</w> <w n="3.6">v<seg phoneme="ø" type="vs" value="1" rule="248" place="6" caesura="1">œu</seg>x</w><caesura></caesura> <w n="3.7">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="3.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="3.9">m<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>x</w> <w n="3.10" punct="pt:12">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>cr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="pt">e</seg>ts</rhyme></pgtc></w>.</l>
						</lg>
						<lg n="2" type="regexp" rhyme="bcb">
							<l n="4" num="2.1" lm="12" met="6+6"><w n="4.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="4.2">n</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="4.4" punct="pv:3">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3" punct="pv">ai</seg></w> ; <w n="4.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg></w> <w n="4.6">n</w>’<w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="4.8" punct="pt:6">s<seg phoneme="y" type="vs" value="1" rule="445" place="6" punct="pt" caesura="1">û</seg>r</w>.<caesura></caesura> <w n="4.9">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="4.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="4.11" punct="vg:9">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="9" punct="vg">oi</seg>t</w>, <w n="4.12">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="4.13"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="4.14" punct="pt:12"><pgtc id="2" weight="2" schema="[CR">pl<rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
							<l n="5" num="2.2" lm="12" met="6+6"><w n="5.1" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">E</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">er</seg></w>, <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M/mc">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="Lc">i</seg></w>-<w n="5.3" punct="vg:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="5.4">c</w>’<w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="5.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="5.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10" mp="P">e</seg>rs</w> <w n="5.8" punct="pt:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>ffr<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>r</rhyme></pgtc></w>.</l>
							<l n="6" num="2.3" lm="12" met="6+6"><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="6.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="6.6" punct="vg:6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg>g</w>,<caesura></caesura> <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="6.8">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="6.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.10"><seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="6.11" punct="vg:12">h<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						</lg>
						<lg n="3" type="regexp" rhyme="c">
							<l n="7" num="3.1" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="7.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>ts</w><caesura></caesura> <w n="7.5">j<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="Lc">u</seg>squ</w>’<w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="C">au</seg></w> <w n="7.7">j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="7.8">d</w>’<w n="7.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="7.10" punct="pt:12">m<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>r</rhyme></pgtc></w>.</l>
						</lg>
					</div></body></text></TEI>