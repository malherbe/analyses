<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURELE SOIR</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><div type="poem" key="HAR142" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede" er_moy="1.14" er_max="8" er_min="0" er_mode="0(6/7)" er_moy_et="2.8">
						<head type="main">CRI DU COQ</head>
						<opener>
							<salute>À ÉMILE MICHELET</salute>
						</opener>
						<lg n="1" rhyme="abba">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="1.2">br<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">s</w>’<w n="1.4" punct="pt:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pt" caesura="1">i</seg>t</w>.<caesura></caesura> <w n="1.5">P<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="1.6" punct="vg:9">m<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="9" punct="vg">u</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.7"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="1.8" punct="vg:12">g<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:2">L<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="2.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="2.4">t<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>ts</w><caesura></caesura> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.6">cl<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="2.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="2.9" punct="pt:12">r<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>cs</rhyme></pgtc></w>.</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="3.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="3.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="3.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>c</w><caesura></caesura> <w n="3.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>t</w> <w n="3.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="P">u</seg>r</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="3.8">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10">e</seg>r</w> <w n="3.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="3.10" punct="pv:12">s<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pv">o</seg>cs</rhyme></pgtc></w> ;</l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L</w>’<w n="4.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="4.3" punct="vg:3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3" punct="vg">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="4.4" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>mm<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>c<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						</lg>
						<lg n="2" rhyme="abba">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:2">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2" punct="vg">ain</seg></w>, <w n="5.2" punct="vg:3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>f</w>, <w n="5.3">p<seg phoneme="wa" type="vs" value="1" rule="420" place="4" mp="M">oi</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.5" punct="vg:8">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="vg">e</seg>l</w>, <w n="5.6">tr<seg phoneme="u" type="vs" value="1" rule="427" place="9" mp="M">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="5.8" punct="vg:12">v<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>q</w> <w n="6.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="6.5">cr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="6.6">d</w>’<w n="6.7" punct="dp:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ci<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="dp">er</seg></w> : <w n="6.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="6.9">cr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="6.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="6.11">c<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>qs</rhyme></pgtc></w></l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1" punct="vg:2">R<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>d</w>, <w n="7.2">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="7.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>ss<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="7.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg></w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="7.8">ch<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>cs</w> <w n="7.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="7.10" punct="pt:12">ch<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>cs</rhyme></pgtc></w>.</l>
							<l n="8" num="2.4" lm="12" met="6+6">« <w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rs</w> <w n="8.4" punct="pe:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe">a</seg>s</w> ! » <w n="8.5">L<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="8.6">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="8.7">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="8.9">fr<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="8.10" punct="pt:12">t<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						</lg>
						<lg n="3" rhyme="ccd">
							<l n="9" num="3.1" lm="12" met="6+6">— <w n="9.1" punct="vg:2"><seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">Ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="9.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="9.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="9.4" punct="pe:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pe" caesura="1">eu</seg>r</w> !<caesura></caesura> <w n="9.5">L</w>’<w n="9.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.7">v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="9.8">s</w>’<w n="9.9" punct="ps:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg><pgtc id="5" weight="8" schema="CVCR">ss<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>p<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="ps">i</seg>r</rhyme></pgtc></w>…</l>
							<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="10.2">qu</w>’<w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="10.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="10.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="10.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="10.9" punct="vg:12"><pgtc id="5" weight="8" schema="[CVCR">s<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>p<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="11.2" punct="vg:2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>l</w>, <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="11.5">v<seg phoneme="i" type="vs" value="1" rule="482" place="6" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="11.7">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="11.8">s</w>’<w n="11.9"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.10"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="11.11" punct="pe:12">tr<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
						</lg>
						<lg n="4" rhyme="ede">
							<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="12.4" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg">en</seg>d</w>, <w n="12.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="12.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d</w><caesura></caesura> <w n="12.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.8">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="12.9" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg>s</w>, <w n="12.10">l<seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="Lc">à</seg></w>-<w n="12.11" punct="vg:12">b<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
							<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1" punct="vg:3">Str<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="13.4" punct="vg:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="13.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="13.6" punct="vg:8">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></w>, <w n="13.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="13.8" punct="vg:10">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.9" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>s<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="14.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2" mp="M">oin</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="14.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="14.4" punct="dp:8">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="dp in">er</seg></w> : « <w n="14.5">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="14.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="14.7">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>rs</w> <w n="14.8" punct="pe:12">p<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pe">a</seg>s</rhyme></pgtc></w> ! »</l>
						</lg>
					</div></body></text></TEI>