<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les poèmes dorés</title>
				<title type="medium">Une édition électronique</title>
				<author key="FRA">
					<name>
						<forename>Anatole</forename>
						<surname>FRANCE</surname>
					</name>
					<date from="1844" to="1924">1844-1924</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>727 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">FRA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Poèmes dorés</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesie-francaise.fr</publisher>
						<idno type="URL">http://www.poesie-francaise.fr/anatole-france-les-poemes-dores/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les poèmes dorés</title>
						<author>Anatole France</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>EDOUARD-JOSEPH, EDITEUR</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Remise en ordre des poèmes conforme à l’édition de référence.</p>
				<p>Les dates de fin de poème ont été ajoutées (édition : EDOUARD-JOSEPH, EDITEUR)</p>
				<p>Les deux sections manquantes du poème "Le Désir" ont été ajoutées (édition : EDOUARD-JOSEPH, EDITEUR)</p>
				<p>Nombreuses corrections de ponctuation</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>

				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRA11" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 1" schema="abba abba ccd eed" er_moy="2.57" er_max="6" er_min="2" er_mode="2(6/7)" er_moy_et="1.4">
				<head type="main">Sur une signature de Marie Stuart</head>
				<opener>
					<salute>À Étienne Charavay.</salute>
				</opener>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="1.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>xh<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="9">um</seg></w> <w n="1.6">d</w>’<w n="1.7" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="1" weight="2" schema="CR">g<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="2.3">r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.4">d</w>’<w n="2.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">É</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg" caesura="1">o</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="2.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="2.9" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<pgtc id="2" weight="2" schema="CR">m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="3.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="3.3">R<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rd</w><caesura></caesura> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>l</w> <w n="3.7" punct="vg:12">r<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="2" weight="2" schema="CR">m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="vg">ain</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="497" place="1" mp="C">Y</seg></w> <w n="4.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="4.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="4.7">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="4.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="4.10" punct="pt:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">g<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="5.2">r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.3" punct="vg:4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>c</w><caesura></caesura> <w n="5.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="5.6">fr<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg><pgtc id="3" weight="2" schema="CR">rg<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.2">M<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">A</seg>R<seg phoneme="i" type="vs" value="1" rule="482" place="4">I</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">E</seg></w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="6.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="6.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.7">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="6.8" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">f<seg phoneme="œ" type="vs" value="1" rule="406" place="3" mp="M">eu</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="7.4">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.6">ti<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="7.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="P">ou</seg>s</w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="7.9"><pgtc id="4" weight="2" schema="[CR">m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12">ain</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">bl<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="M">eu</seg><seg phoneme="i" type="vs" value="1" rule="492" place="3" mp="M">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="8.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>g</w><caesura></caesura> <w n="8.5">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-7" place="7">e</seg>r</w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="8.7">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>pt</w> <w n="8.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="8.9">l</w>’<w n="8.10" punct="pt:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg><pgtc id="3" weight="2" schema="CR">rg<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="9.3">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" mp="M">e</seg>ill<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="9.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>gts</w><caesura></caesura> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="9.6">f<seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="9.8" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="5" weight="2" schema="CR">ss<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg>s</rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">em</seg>pr<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>ts</w> <w n="10.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="6" caesura="1">um</seg></w><caesura></caesura> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="10.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="10.7">c<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353" place="11" mp="M">e</seg><pgtc id="5" weight="2" schema="CR">ss<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg>s</rhyme></pgtc></w></l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">r<seg phoneme="wa" type="vs" value="1" rule="440" place="3" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l</w> <w n="11.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="6" caesura="1">ue</seg>il</w><caesura></caesura> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="11.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="11.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>l<pgtc id="6" weight="2" schema="CR">t<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" rhyme="eed">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">J</w>’<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="497" place="1">y</seg></w> <w n="12.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="12.8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10">e</seg>ts</w> <w n="12.9">r<pgtc id="7" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>s<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg>s</rhyme></pgtc></w></l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="13.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="13.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>gts</w> <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M/mc">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="Lc">ou</seg>rd</w>’<w n="13.5">hu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="13.6" punct="vg:8">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="7" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="vg">e</seg>ts</w>, <w n="13.7" punct="vg:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">om</seg>p<pgtc id="7" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>s<rhyme label="e" id="7" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg>s</rhyme></pgtc></w>,</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="14.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="3" mp="Lc">eu</seg>t</w>-<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="14.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="14.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="14.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">am</seg>p</w> <w n="14.9" punct="pt:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="6" weight="2" schema="CR">t<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1868">1868</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>