<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM1" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="1(abba) 1(abab)" er_moy="0.75" er_max="2" er_min="0" er_mode="0(2/4)" er_moy_et="0.83">
				<head type="main">LE BRABANT</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="1.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">m</w>’<w n="1.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="1.6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="1.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="1.8">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>rs</rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S</w>’<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="2.3">n<seg phoneme="y" type="vs" value="1" rule="457" place="4">u</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="2.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="2.6" punct="pe:8">r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg><pgtc id="2" weight="2" schema="CR">ch<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">er</seg>s</rhyme></pgtc></w> !</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2" punct="vg:3">Br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="3.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l</w> <w n="3.6" punct="vg:8">r<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg><pgtc id="2" weight="2" schema="CR">ch<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="4.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rd<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="4.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>rs</rhyme></pgtc></w></l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="5.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="5.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">F<seg phoneme="œ" type="vs" value="1" rule="304" place="1">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="6.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">î</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="6.5">c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>ll<pgtc id="4" weight="1" schema="GR">i<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</rhyme></pgtc></w></l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1" punct="vg:1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" punct="vg">an</seg>d</w>, <w n="7.2">ch<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="7.3" punct="vg:3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>s</w>, <w n="7.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.6">pl<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="8.3">t<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="8.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>c</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6" punct="pi:8">r<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<pgtc id="4" weight="1" schema="GR">i<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pi">er</seg>s</rhyme></pgtc></w> ?</l>
				</lg>
			</div></body></text></TEI>