<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM186" modus="sm" lm_max="6" metProfile="6" form="suite de strophes" schema="3(abab) 2(abba)" er_moy="0.2" er_max="2" er_min="0" er_mode="0(9/10)" er_moy_et="0.6">
				<head type="main">COMMENT VOUS LA DIRAIS-JE ?</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="6" met="6"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="2">en</seg>t</w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<pgtc id="1" weight="0" schema="R" part="1"><rhyme label="a" id="1" gender="f" type="a" part="I"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</rhyme></pgtc></w>-<w n="1.5"><pgtc id="1" weight="0" schema="R" part="2"><rhyme label="a" id="1" gender="f" type="a" part="F">j<seg phoneme="ə" type="ef" value="1" rule="e-1" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="6" met="6"><w n="2.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4" punct="pi:6">bl<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
					<l n="3" num="1.3" lm="6" met="6"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="3.2">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="2">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4">n<pgtc id="1" weight="0" schema="R" part="1"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="384" place="6">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="6" met="6"><w n="4.1">Tr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="4.3" punct="pt:6">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abba">
					<l n="5" num="2.1" lm="6" met="6"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="5.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="5.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>ti<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="6" met="6"><w n="6.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="6.2">d</w>’<w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="u" type="vs" value="1" rule="d-2" place="5">ou</seg><pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></pgtc></w></l>
					<l n="7" num="2.3" lm="6" met="6"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="7.2">tr<seg phoneme="o" type="vs" value="1" rule="433" place="2">o</seg>p</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="7.6">t<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="6" met="6"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="8.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="8.4" punct="pt:6">bl<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pt">é</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abba">
					<l n="9" num="3.1" lm="6" met="6"><w n="9.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="9.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.4">ti<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6">en</seg>t</rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="6" met="6"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="10.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg><pgtc id="6" weight="2" schema="CR" part="1">l<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="6" met="6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="11.3">p<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5"><pgtc id="6" weight="2" schema="[CR" part="1">cl<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="6" met="6"><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">Ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="12.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="12.5" punct="pt:6">f<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="m" type="e"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" punct="pt">oin</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="6" met="6"><w n="13.1">J</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="13.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="6" met="6"><w n="14.1">D<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</rhyme></pgtc></w></l>
					<l n="15" num="4.3" lm="6" met="6"><w n="15.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="15.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="15.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="15.5"><pgtc id="7" weight="0" schema="[R" part="1"><rhyme label="a" id="7" gender="f" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="6" met="6"><w n="16.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="16.3">c<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="16.4" punct="pt:6">gl<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="6" met="6"><w n="17.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="2">en</seg>t</w> <w n="17.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="17.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="17.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="a" part="I"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</rhyme></pgtc></w>-<w n="17.5"><pgtc id="9" weight="0" schema="R" part="2"><rhyme label="a" id="9" gender="f" type="a" part="F">j<seg phoneme="ə" type="ef" value="1" rule="e-1" place="7">e</seg></rhyme></pgtc></w></l>
					<l n="18" num="5.2" lm="6" met="6"><w n="18.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="18.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> <w n="18.5" punct="pi:6">bl<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pi">an</seg>c</rhyme></pgtc></w> ?</l>
					<l n="19" num="5.3" lm="6" met="6"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="19.2">m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="19.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="19.4" punct="vg:6">d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="20" num="5.4" lm="6" met="6"><w n="20.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="20.2">m<seg phoneme="ø" type="vs" value="1" rule="405" place="2">eu</seg>ni<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="20.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="20.4" punct="pt:6">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rm<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pt">an</seg>ts</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>