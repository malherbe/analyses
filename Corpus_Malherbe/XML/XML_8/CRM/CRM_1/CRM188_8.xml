<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM188" modus="cp" lm_max="12" metProfile="8, 6+6" form="strophe unique" schema="1(abab)" er_moy="1.0" er_max="2" er_min="0" er_mode="0(1/2)" er_moy_et="1.0">
				<head type="main">A LA SANTÉ DU GEL</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">m</w>’<w n="1.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2" mp="M">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="1.5">pr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.7">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg>g</w> <w n="1.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r</w> <w n="1.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="1.10">l</w>’<w n="1.11" punct="vg:12">h<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="1" weight="2" schema="CR">v<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>r</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6" punct="pe:8">s<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pe">e</seg>l</rhyme></pgtc></w> !</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="3.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="3.3" punct="vg:4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4" punct="vg">ain</seg></w>, <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="3.5" punct="vg:6">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg" caesura="1">ai</seg>t</w>,<caesura></caesura> <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.7" punct="vg:8">fru<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>ts</w>, <w n="3.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="3.9">r<seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="11">eau</seg>x</w> <w n="3.10"><pgtc id="1" weight="2" schema="[CR">v<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rts</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="4.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="4.7" punct="pt:8">g<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="pt">e</seg>l</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>