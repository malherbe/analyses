<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM61" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="12(aa)" er_moy="0.25" er_max="2" er_min="0" er_mode="0(10/12)" er_moy_et="0.6">
				<head type="main">MARIE EN BRABANT</head>
				<lg n="1" type="distique" rhyme="aa">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="1.2">s</w>’<w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="1.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="1" weight="2" schema="CR">m<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">T<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="2.3">J<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.6" punct="pv:8"><pgtc id="1" weight="2" schema="[CR">m<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="pv">ain</seg></rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="2" type="distique" rhyme="aa">
					<l n="3" num="2.1" lm="8" met="8"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="3.3">n</w>’<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.5">j<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="3.6">v<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="3.7">d</w>’<w n="3.8"><pgtc id="2" weight="0" schema="[R"><rhyme label="a" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="2.2" lm="8" met="8"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="4.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="4.5" punct="vg:8">bl<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="3" type="distique" rhyme="aa">
					<l n="5" num="3.1" lm="8" met="8"><w n="5.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="5.2">n</w>’<w n="5.3"><seg phoneme="y" type="vs" value="1" rule="391" place="2">eu</seg>t</w> <w n="5.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="5.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="5.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="5.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</rhyme></pgtc></w></l>
					<l n="6" num="3.2" lm="8" met="8"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="6.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="6.6">m<seg phoneme="u" type="vs" value="1" rule="428" place="5">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.8" punct="pt:8">bl<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="distique" rhyme="aa">
					<l n="7" num="4.1" lm="8" met="8"><w n="7.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="7.2">s</w>’<w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="7.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="7.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="7.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="4.2" lm="8" met="8"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="8.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>s</w> <w n="8.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="8.5">r<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.7">p<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
				</lg>
				<lg n="5" type="distique" rhyme="aa">
					<l n="9" num="5.1" lm="8" met="8"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="9.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></pgtc></w></l>
					<l n="10" num="5.2" lm="8" met="8"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">cr<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="10.6" punct="pv:8">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pv">er</seg></rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="6" type="distique" rhyme="aa">
					<l n="11" num="6.1" lm="8" met="8"><w n="11.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="11.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="11.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ds</w> <w n="11.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="11.6">s</w>’<w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>pl<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>nt</rhyme></pgtc></w></l>
					<l n="12" num="6.2" lm="8" met="8"><w n="12.1">D</w>’<w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>gn<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="12.6" punct="pv:8">g<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="7" type="distique" rhyme="aa">
					<l n="13" num="7.1" lm="8" met="8"><w n="13.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="13.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>il</rhyme></pgtc></w></l>
					<l n="14" num="7.2" lm="8" met="8"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="14.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="14.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="14.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="14.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ils</rhyme></pgtc></w></l>
				</lg>
				<lg n="8" type="distique" rhyme="aa">
					<l n="15" num="8.1" lm="8" met="8"><w n="15.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="15.2" punct="vg:4">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="15.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="15.5" punct="vg:8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ss<pgtc id="8" weight="1" schema="GR">i<rhyme label="a" id="8" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="16" num="8.2" lm="8" met="8"><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">f<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="16.3">tr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.5" punct="pv:8">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<pgtc id="8" weight="1" schema="GR">i<rhyme label="a" id="8" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="9" type="distique" rhyme="aa">
					<l n="17" num="9.1" lm="8" met="8"><w n="17.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="17.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="17.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="17.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="17.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="17.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</rhyme></pgtc></w></l>
					<l n="18" num="9.2" lm="8" met="8"><w n="18.1">Qu</w>’<w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg></w>-<w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="18.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="421" place="6">oi</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="18.6">d</w>’<w n="18.7" punct="vg:8"><pgtc id="9" weight="0" schema="[R"><rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>r</rhyme></pgtc></w>,</l>
				</lg>
				<lg n="10" type="distique" rhyme="aa">
					<l n="19" num="10.1" lm="8" met="8"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2">b<seg phoneme="y" type="vs" value="1" rule="445" place="2">û</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="19.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="19.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="19.5">b<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</rhyme></pgtc></w></l>
					<l n="20" num="10.2" lm="8" met="8"><w n="20.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="20.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="20.4">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="20.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="20.6" punct="vg:8">cr<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>x</rhyme></pgtc></w>,</l>
				</lg>
				<lg n="11" type="distique" rhyme="aa">
					<l n="21" num="11.1" lm="8" met="8"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="21.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="21.5">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>ci<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg>s</w> <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ppr<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>nt</rhyme></pgtc></w></l>
					<l n="22" num="11.2" lm="8" met="8"><w n="22.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="22.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="22.3">dr<seg phoneme="y" type="vs" value="1" rule="454" place="5">u</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="22.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="22.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="22.6">t<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
				</lg>
				<lg n="12" type="distique" rhyme="aa">
					<l n="23" num="12.1" lm="8" met="8"><w n="23.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">J<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="23.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="23.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="23.5">ru<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ss<pgtc id="12" weight="0" schema="R"><rhyme label="a" id="12" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></rhyme></pgtc></w></l>
					<l n="24" num="12.2" lm="8" met="8"><w n="24.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="24.2"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="24.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="24.4" punct="pt:8"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<pgtc id="12" weight="0" schema="R"><rhyme label="a" id="12" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg>x</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>