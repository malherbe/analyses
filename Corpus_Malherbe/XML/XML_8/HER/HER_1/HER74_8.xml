<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Le Moyen âge et la Renaissance</head><div type="poem" key="HER74" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede" er_moy="1.43" er_max="2" er_min="0" er_mode="2(5/7)" er_moy_et="0.9">
					<head type="main">Le Vieil Orfèvre</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Mi<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="1.2">qu</w>’<w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="1.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>scr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="1.7">l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="1.9" punct="vg:12">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">aî</seg>tr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="2.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="3">om</seg></w> <w n="2.5" punct="vg:4">Ru<seg phoneme="i" type="vs" value="1" rule="497" place="4" punct="vg">y</seg>z</w>, <w n="2.6" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">A</seg>rph<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="2.7" punct="vg:9">X<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg>z</w>, <w n="2.8" punct="vg:12">B<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg><pgtc id="2" weight="2" schema="CR">rr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>l</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="3.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="3.5" punct="vg:6">r<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="3.7">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="3.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="3.10" punct="vg:12">b<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="2" weight="2" schema="CR">r<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="493" place="12" punct="vg">y</seg>l</rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">T<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="M">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="4.4">d</w>’<w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="4.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="4.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="4.10" punct="pt:12">fr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="5.2">l</w>’<w n="5.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" punct="vg">en</seg>t</w>, <w n="5.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="5.5">l</w>’<w n="5.6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307" place="6" caesura="1">a</seg>il</w><caesura></caesura> <w n="5.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.9">p<seg phoneme="a" type="vs" value="1" rule="307" place="9" mp="M">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="5.10">s</w>’<w n="5.11" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">J</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="6.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="2">ein</seg>t</w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="6.5">j</w>’<w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="6.7" punct="vg:6">sc<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>lpt<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="6.8">m<seg phoneme="e" type="vs" value="1" rule="353" place="7" mp="M">e</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="6.9">l</w>’<w n="6.10"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="6.12" punct="vg:12">p<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="4" weight="2" schema="CR">r<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>l</rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="7.2">li<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="7.4">Chr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>st</w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="7.6">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>x</w><caesura></caesura> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="7.9">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9">ain</seg>t</w> <w n="7.10">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="7.11">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="7.12" punct="vg:12"><pgtc id="4" weight="2" schema="[CR">gr<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>l</rhyme></pgtc></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="8.2" punct="pe:3">h<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pe" mp="F">e</seg></w> ! <w n="8.3">B<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>cch<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="8.4"><seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="8.6">D<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>n<seg phoneme="a" type="vs" value="1" rule="343" place="9" mp="M">a</seg><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="8.7" punct="pt:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>rpr<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">J</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="9.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="9.5">d</w>’<w n="9.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="9.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>st<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>c</w><caesura></caesura> <w n="9.8">d<seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>squ<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="9.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="9.10"><pgtc id="5" weight="2" schema="[CR">f<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12">e</seg>r</rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="10.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="10.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4">ain</seg></w> <w n="10.5"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="6" caesura="1">ue</seg>il</w><caesura></caesura> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="10.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="10.8"><seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="10.9">d</w>’<w n="10.10" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">En</seg><pgtc id="5" weight="2" schema="CR">f<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>r</rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2" mp="M">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="11.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rt</w><caesura></caesura> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="11.7" punct="pt:12"><pgtc id="6" weight="2" schema="[CR">V<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" rhyme="ede">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="vg:2"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="12.2">v<seg phoneme="wa" type="vs" value="1" rule="440" place="3" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="12.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">â</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="12.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="12.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="10" mp="P">e</seg>rs</w> <w n="12.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="12.8" punct="vg:12"><pgtc id="7" weight="2" schema="[CR">s<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>r</rhyme></pgtc></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2" punct="vg:2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="13.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="13.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="13.6">Fr<seg phoneme="ɛ" type="vs" value="1" rule="323" place="7">ay</seg></w> <w n="13.7">Ju<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8">an</seg></w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="13.9" punct="vg:12">S<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="6" weight="2" schema="CR">v<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">M<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="14.3">c<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="14.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</w> <w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="14.8" punct="pt:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" mp="M">o</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg><pgtc id="7" weight="2" schema="CR">s<rhyme label="e" id="7" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>