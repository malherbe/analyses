<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ARV">
					<name>
						<forename>Félix</forename>
						<surname>ARVERS</surname>
					</name>
					<date from="1806" to="1850">1806-1850</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4560 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ARV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Félix Arvers</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/felixarverspoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies de Félix Arvers: mes heures perdues. Pièces inédites</title>
						<author>Félix Arvers</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">http://www.archive.org/details/posiesdeflixOOarve </idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Félix Arvers</author>
								<imprint>
									<publisher>H. Floury, éditeur</publisher>
									<date when="1900">1900</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PIÈCES INÉDITES</head><div type="poem" key="ARV35" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abab abab ccd eed" er_moy="1.71" er_max="6" er_min="0" er_mode="0(3/7)" er_moy_et="1.98">
					<head type="main">La Villégiature</head>
					<lg n="1" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="1.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="1.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="1.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>g<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><pgtc id="1" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg>x</w> <w n="2.2">ph<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="2.3">d</w>’<w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="2.5">v<seg phoneme="wa" type="vs" value="1" rule="440" place="5" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="2.8">c<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>mm<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="12">un</seg></rhyme></pgtc></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg>s</w><caesura></caesura> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="3.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.6">n<pgtc id="1" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="4.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="4.3">n</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="4.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="4.7">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="4.8">v<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="4.9">n<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="4.10">c<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="4.11">p<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>s</w> <w n="4.12" punct="pt:12"><pgtc id="2" weight="0" schema="[R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="12" punct="pt">un</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="5.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="5.5" punct="vg:6">r<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="5.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="5.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="5.9" punct="vg:12">v<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="6.2">s</w>’<w n="6.3" punct="tc:4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>bs<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="dp ti" mp="F">e</seg></w> : — <w n="6.4">l</w>’<w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="6.6">l</w>’<w n="6.7"><seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="6.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="6.9">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.10">tr<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.11" punct="pv:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" mp="M">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rt<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="12" punct="pv">un</seg></rhyme></pgtc></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="7.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.4">m<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rt</w> <w n="7.5">f<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="7.7" punct="ps:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">â</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></rhyme></pgtc></w>…</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="8.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="8.4" punct="vg:6">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="8.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="8.6">s</w>’<w n="8.7" punct="pv:9"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="9" punct="pv">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="8.9" punct="pe:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>c<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="12" punct="pe">un</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="9.4">g<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="9.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="9.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="9.8">s</w>’<w n="9.9" punct="dp:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ss<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="5" weight="2" schema="CR">c<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="10.2" punct="vg:2">c<seg phoneme="o" type="vs" value="1" rule="318" place="2" punct="vg">au</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="10.4">s</w>’<w n="10.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6" punct="vg" caesura="1">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="10.8">d</w>’<w n="10.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="10.10">s</w>’<w n="10.11" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ppr<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="5" weight="2" schema="CR">c<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="11" num="3.3" lm="12" met="6+6">— <w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="11.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="11.3">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="11.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="11.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="11.6">s</w>’<w n="11.7" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="11" mp="M">ê</seg><pgtc id="6" weight="2" schema="CR">t<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pv">er</seg></rhyme></pgtc></w> ;</l>
					</lg>
					<lg n="4" rhyme="eed">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">c</w>’<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="12.4">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="Lc">o</seg>rsqu</w>’<w n="12.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="12.6">c<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="12.8">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.9"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="12.10">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="12.11" punct="vg:12">c<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>nn<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="13.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="13.5">j<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="13.6" punct="tc:6">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg ti" caesura="1">eu</seg>x</w>,<caesura></caesura> — <w n="13.7">qu</w>’<w n="13.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="13.9">s</w>’<w n="13.10"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8" mp="M">ai</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="13.11">p<seg phoneme="ø" type="vs" value="1" rule="398" place="11" mp="Lc">eu</seg>t</w>-<w n="13.12" punct="vg:12"><pgtc id="7" weight="0" schema="[R"><rhyme label="e" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="4.3" lm="12" met="6+6">— <w n="14.1">C</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rs</w> <w n="14.4">qu</w>’<w n="14.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="14.6" punct="tc:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg ti" caesura="1">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> — <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="14.8">qu</w>’<w n="14.9"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l</w> <w n="14.10">f<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>t</w> <w n="14.11">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="14.12" punct="pt:12">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="M">i</seg><pgtc id="6" weight="2" schema="CR">tt<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>