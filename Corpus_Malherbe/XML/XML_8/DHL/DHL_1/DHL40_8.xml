<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">RONDEAUX</head><div type="poem" key="DHL40" modus="cp" lm_max="10" metProfile="4+6, (4)" form="rondeau classique" schema="aabba aabC aabbaC" er_moy="2.22" er_max="12" er_min="0" er_mode="0(6/9)" er_moy_et="3.94">
					<head type="main">À Monsieur ***</head>
					<lg n="1" rhyme="aabbaaabC">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">QU<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">AN</seg>D</w> <w n="1.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="1.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="1.4">d</w>’<w n="1.5" punct="vg:4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="vg" caesura="1">o</seg>r</w>,<caesura></caesura> <w n="1.6">n</w>’<w n="1.7"><seg phoneme="y" type="vs" value="1" rule="251" place="5" mp="Lp">eû</seg>t</w>-<w n="1.8" punct="vg:6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg">on</seg></w>, <w n="1.9">j</w>’<w n="1.10"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.11">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="1.12" punct="vg:10">d<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">N<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>l</w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="2.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="4" caesura="1">en</seg>s</w><caesura></caesura> <w n="2.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5" punct="vg:8">p<seg phoneme="o" type="vs" value="1" rule="435" place="6" mp="M">o</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">ez</seg></w>, <w n="2.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="2.7" punct="vg:10">s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg>t</w> <w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">i</seg>l</w> <w n="3.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="3.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="4" caesura="1">au</seg>t</w><caesura></caesura> <w n="3.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="3.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="3.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>r<pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="4.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ds</w> <w n="4.3" punct="pv:4">p<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pv" caesura="1">i</seg>ls</w> ;<caesura></caesura> <w n="4.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>s</w> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r</w> <w n="4.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></rhyme></pgtc></w></l>
						<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">D</w>’<w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">om</seg>br<seg phoneme="i" type="vs" value="1" rule="dc-1" place="3" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><ref type="noteAnchor">1</ref> <caesura></caesura><w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="341" place="6" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="5.5" punct="pt:10">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rt<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="493" place="10">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="P">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>c<seg phoneme="œ" type="vs" value="1" rule="345" place="4" caesura="1">ue</seg>ils</w><caesura></caesura> <w n="6.4">br<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="6.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="6.6" punct="vg:10">n<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>v<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <hi rend="ital"><w n="7.2" punct="pt:4"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>x</w> <w n="7.3">v<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="4" caesura="1">o</seg></w><caesura></caesura> </hi><w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="7.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="7.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="7.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="C">ou</seg>s</w> <w n="7.8" punct="dp:10">t<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
						<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="8.3">l</w>’<w n="8.4" punct="pt:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="pt" caesura="1">en</seg>d</w>.<caesura></caesura> <w n="8.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">On</seg></w> <w n="8.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="8.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="8.8" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>c<pgtc id="2" weight="6" schema="VCR"><seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">er</seg></rhyme></pgtc></w>,</l>
						<l n="9" num="1.9" lm="4"><space unit="char" quantity="12"></space><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="9.4">d</w>’<w n="9.5" punct="pt:4"><pgtc id="6" weight="0" schema="R" part="1"><rhyme label="C" id="6" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="pt">o</seg>r</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="aabbaC">
						<l n="10" num="2.1" lm="10" met="4+6"><w n="10.1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">O</seg>r</w> <w n="10.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>x</w><caesura></caesura> <w n="10.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="10.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" mp="M">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="10.7">c<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="a" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="11" num="2.2" lm="10" met="4+6"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="11.2">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>n<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oî</seg>t</w> <w n="11.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" caesura="1">ain</seg>t</w><caesura></caesura> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="11.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="C">ou</seg>s</w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="11.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="11.8">m</w>’<w n="11.9"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>cr<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="a" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="12" num="2.3" lm="10" met="4+6"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>ts</w> <w n="12.3">l<seg phoneme="u" type="vs" value="1" rule="d-2" place="3" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="12.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="12.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>ts</w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="12.7">lu<pgtc id="5" weight="12" schema="V[CVCR" part="1"><seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></pgtc></w> <w n="12.8" punct="pi:10"><pgtc id="5" weight="12" schema="V[CVCR" part="2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">om</seg>pt<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pi">er</seg></rhyme></pgtc></w> ?</l>
						<l n="13" num="2.4" lm="10" met="4+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="13.4" punct="pv:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="pv" caesura="1">oi</seg></w> ;<caesura></caesura> <w n="13.5" punct="vg:5">c<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>r</w>, <w n="13.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>c<pgtc id="5" weight="12" schema="V[CVCR" part="1"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></pgtc></w> <w n="13.8" punct="pi:10"><pgtc id="5" weight="12" schema="V[CVCR" part="2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>t<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pi">er</seg></rhyme></pgtc></w> ?</l>
						<l n="14" num="2.5" lm="10" met="4+6"><w n="14.1">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="14.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="14.4">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="6">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="14.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>t</w> <w n="14.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>ff<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="a" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="15" num="2.6" lm="4"><space unit="char" quantity="12"></space><w n="15.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="15.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="15.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="15.4">d</w>’<w n="15.5" punct="pt:4"><pgtc id="6" weight="0" schema="R" part="1"><rhyme label="C" id="6" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="pt">o</seg>r</rhyme></pgtc></w>.</l>
					</lg>
					<closer>
						<note type="footnote" id="1">D’avoir quelque malheur.</note>
					</closer>
				</div></body></text></TEI>