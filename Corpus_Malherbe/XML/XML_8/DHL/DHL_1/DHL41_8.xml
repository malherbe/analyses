<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">RONDEAUX</head><div type="poem" key="DHL41" modus="cp" lm_max="10" metProfile="4+6, (4)" form="rondeau classique" schema="aabba aabC aabbaC" er_moy="0.67" er_max="2" er_min="0" er_mode="0(6/9)" er_moy_et="0.94">
					<head type="main">À Monsieur l’abbé ***,</head>
					<head type="sub_1">QUI LUI AVAIT ÉCRIT QU’IL N’Y AVAIT RIEN <lb></lb>DE SI TRISTE QU’UNE EXTRÊME SAGESSE</head>
					<lg n="1" rhyme="aabbaaabC">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">FL<seg phoneme="œ" type="vs" value="1" rule="407" place="1">EU</seg>R</w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="1.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>gt</w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" caesura="1">an</seg>s</w><caesura></caesura> <w n="1.5">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>t</w> <w n="1.6">li<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.9" punct="dp:10">ch<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="2.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w> <w n="2.3" punct="vg:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg" caesura="1">ai</seg>t</w>,<caesura></caesura> <w n="2.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="2.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="2.8" punct="vg:10">d<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>sp<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="3.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2" mp="C">o</seg>s</w> <w n="3.3">p<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg>s</w><caesura></caesura> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="3.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="3.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.7" punct="vg:10">r<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>j<seg phoneme="ø" type="vs" value="1" rule="405" place="9" mp="M">eu</seg><pgtc id="2" weight="2" schema="CR">n<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">Pr<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t</w> <w n="4.2" punct="vg:4">f<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg" caesura="1">u</seg>r</w>,<caesura></caesura> <w n="4.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="4.4">s<seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="4.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="4.6">p<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg><pgtc id="2" weight="2" schema="CR">n<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</rhyme></pgtc></w></l>
						<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="5.4">m<seg phoneme="o" type="vs" value="1" rule="318" place="4" caesura="1">au</seg>x</w><caesura></caesura> <w n="5.5"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="5.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="5.8">m</w>’<w n="5.9" punct="pt:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>xp<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg>t</w> <w n="6.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">cr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="6.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="6.5" punct="pv:10">m<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rph<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1">Tr<seg phoneme="o" type="vs" value="1" rule="433" place="1">o</seg>p</w> <w n="7.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="7.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="7.4" punct="vg:5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" punct="vg">e</seg></w>, <w n="7.5">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="6">oi</seg></w> <w n="7.6">qu</w>’<w n="7.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="7.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="7.9" punct="vg:10">pr<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>p<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="8.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="8.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4" caesura="1">ain</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="8.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="8.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg><pgtc id="2" weight="2" schema="CR">n<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r</rhyme></pgtc></w></l>
						<l n="9" num="1.9" lm="4"><space unit="char" quantity="12"></space><w n="9.1">Fl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>gt</w> <w n="9.4" punct="pt:4"><pgtc id="6" weight="0" schema="R" part="1"><rhyme label="C" id="6" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="pt">an</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="aabbaC">
						<l n="10" num="2.1" lm="10" met="4+6"><w n="10.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="10.2" punct="pe:4">s<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="pe" caesura="1">eu</seg>x</w> !<caesura></caesura> <w n="10.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M/mp">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" mp="Lp">ai</seg>t</w>-<w n="10.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="10.6">qu</w>’<w n="10.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="10.8">n</w>’<w n="10.9"><pgtc id="4" weight="0" schema="[R" part="1"><rhyme label="a" id="4" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="11" num="2.2" lm="10" met="4+6"><w n="11.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="11.3" punct="pi:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pi" caesura="1">ou</seg>s</w> ?<caesura></caesura> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">En</seg></w> <w n="11.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="11.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r</w> <w n="11.8" punct="pv:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="9" mp="M">im</seg>p<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="a" id="4" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="12" num="2.3" lm="10" met="4+6"><w n="12.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="12.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="12.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4" caesura="1">en</seg></w><caesura></caesura> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="12.5">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="6">oi</seg></w> <w n="12.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="12.8" punct="pt:10">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg><pgtc id="5" weight="2" schema="CR" part="1">n<rhyme label="b" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg>r</rhyme></pgtc></w>.</l>
						<l n="13" num="2.4" lm="10" met="4+6"><w n="13.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="13.3" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="13.4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="13.5">v<seg phoneme="œ" type="vs" value="1" rule="406" place="6">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="13.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="13.7" punct="vg:10">b<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg><pgtc id="5" weight="2" schema="CR" part="1">n<rhyme label="b" id="5" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
						<l n="14" num="2.5" lm="10" met="4+6"><w n="14.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="14.2" punct="vg:4">c<seg phoneme="œ" type="vs" value="1" rule="345" place="2" mp="M">ue</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg" caesura="1">ez</seg></w>,<caesura></caesura> <w n="14.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="14.4" punct="vg:6">s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="14.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="14.7" punct="vg:10">cl<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="a" id="4" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="15" num="2.6" lm="4"><space unit="char" quantity="12"></space><w n="15.1">Fl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>gt</w> <w n="15.4" punct="pt:4"><pgtc id="6" weight="0" schema="R" part="1"><rhyme label="C" id="6" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="pt">an</seg>s</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>