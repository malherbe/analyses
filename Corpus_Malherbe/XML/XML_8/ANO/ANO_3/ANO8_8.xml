<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO8" modus="sm" lm_max="6" metProfile="6" form="strophe unique" schema="1(abab)" er_moy="3.0" er_max="4" er_min="2" er_mode="2(1/2)" er_moy_et="1.0">
					<head type="main">BADINAGE IN-PROMPTU</head>
					<head type="sub_1"><hi rend="ital">En voyant la Statue de la Pucelle d’Orléans <lb></lb>dans la place publique de cette Ville.</hi></head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="6" met="6"><w n="1.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>ts</w>, <w n="1.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ct<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="1.3"><pgtc id="1" weight="2" schema="[CR">c<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="6" met="6"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="2.3">v<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="2.4" punct="dp:6">c<pgtc id="2" weight="4" schema="VR"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="dp">an</seg>s</rhyme></pgtc></w> :</l>
						<l n="3" num="1.3" lm="6" met="6"><w n="3.1">C</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.5">p<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg><pgtc id="1" weight="2" schema="CR">c<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="6" met="6"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="4.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="4.4" punct="pt:6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">O</seg>rl<pgtc id="2" weight="4" schema="VR"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="pt">an</seg>s</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>