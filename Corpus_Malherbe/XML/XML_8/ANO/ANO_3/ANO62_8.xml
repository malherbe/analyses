<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO62" modus="cm" lm_max="10" metProfile="4+6" form="suite de strophes" schema="3[abab]" er_moy="2.0" er_max="6" er_min="0" er_mode="2(3/6)" er_moy_et="2.0">
					<head type="main">LA COLÈRE NAÏVE</head>
					<lg n="1" type="regexp" rhyme="abababababab">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="1.3" punct="vg:4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="1.5">fr<seg phoneme="i" type="vs" value="1" rule="d-1" place="6" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.6">C<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="2.2">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg>t</w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="2.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">en</seg>d<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t</w> <w n="2.6" punct="pv:10"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">Au</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="9" mp="M">u</seg>s<pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="pv">in</seg></rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">L<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="M">u</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="3.3" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="3.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6" mp="C">i</seg></w> <w n="3.6" punct="dp:7">d<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="dp in">i</seg>t</w> : « <w n="3.7" punct="pe:8">Ou<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pe">ai</seg>s</w> ! <w n="3.8" punct="vg:10">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="10" met="4+6">» <w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M/mp">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="4.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="4.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="4.6">li<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></w> <w n="4.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="4.8" punct="pi:10">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="pi">in</seg></rhyme></pgtc></w> ?</l>
						<l n="5" num="1.5" lm="10" met="4+6">» ‒ <w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="5.2" punct="vg:2">n<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>d</w>, <w n="5.3" punct="pt:4">L<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pt in" caesura="1">a</seg>s</w>.<caesura></caesura> ‒ <w n="5.4">C</w>’<w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="5.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="5.7" punct="vg:7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="vg">ai</seg>t</w>, <w n="5.8" punct="vg:10">p<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>nn<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>, »</l>
						<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1" mp="C">i</seg></w> <w n="6.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ge<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="6.5" punct="pv:10">r<pgtc id="4" weight="6" schema="VCR"><seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>s<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pv">é</seg></rhyme></pgtc></w> ;</l>
						<l n="7" num="1.7" lm="10" met="4+6">« <w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="7.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" caesura="1">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="7.5"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="7.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>c</w> <w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="7.8">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9" punct="pi:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>ch<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
						<l n="8" num="1.8" lm="10" met="4+6">» <w n="8.1" punct="vg:2">T<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="8.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="8.3" punct="vg:4">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>c</w>,<caesura></caesura> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="8.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="8.7" punct="pv:10"><pgtc id="4" weight="6" schema="[VCR"><seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>s<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pv">é</seg></rhyme></pgtc></w> ;</l>
						<l n="9" num="1.9" lm="10" met="4+6">» <w n="9.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="9.2" punct="ps:3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="ps">ez</seg></w>… <w n="9.3" punct="ps:4">l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="ps" caesura="1">à</seg></w>…<caesura></caesura> <w n="9.4">n</w>’<w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5" mp="Lp">e</seg>st</w>-<w n="9.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="9.8" punct="vg:8">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8" punct="vg">ai</seg></w>, <w n="9.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="9.10" punct="pi:10">b<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi ps" mp="F">e</seg></rhyme></pgtc></w> ?… »</l>
						<l n="10" num="1.10" lm="10" met="4+6"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="10.2" punct="vg:4">L<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="10.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="10.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t</w> <w n="10.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="10.7" punct="pt:10">f<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">â</seg><pgtc id="6" weight="2" schema="CR">ch<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pt">er</seg></rhyme></pgtc></w>.</l>
						<l n="11" num="1.11" lm="10" met="4+6">‒ « <w n="11.1" punct="pe:1"><seg phoneme="e" type="vs" value="1" rule="133" place="1" punct="pe">E</seg>h</w> ! <w n="11.2" punct="vg:2">ou<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg></w>, <w n="11.3" punct="vg:4">m<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="11.4">pu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" mp="Fm">e</seg>s</w>-<w n="11.5" punct="vg:7">t<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="vg">u</seg></w>, » <w n="11.6">lu<seg phoneme="i" type="vs" value="1" rule="491" place="8" mp="C">i</seg></w> <w n="11.7">d<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="Lp">i</seg>t</w>-<w n="11.8" punct="vg:10"><pgtc id="5" weight="0" schema="[R"><rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="12" num="1.12" lm="10" met="4+6">« <w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="12.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg></w><caesura></caesura> <w n="12.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="12.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="12.6">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="8">en</seg>s</w> <w n="12.7" punct="pe:10">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>r<pgtc id="6" weight="2" schema="CR">ch<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pe">er</seg></rhyme></pgtc></w> ! »</l>
					</lg>
				</div></body></text></TEI>