<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO85" modus="cp" lm_max="12" metProfile="8, (4+6), 6+6" form="strophe unique" schema="1(abbacac)" er_moy="0.5" er_max="2" er_min="0" er_mode="0(3/4)" er_moy_et="0.87">
					<head type="main">L’ÉPOUSE NAÏVE</head>
					<lg n="1" type="septain" rhyme="abbacac">
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="1.1">Bl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="1.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.4" punct="pt:8">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>z<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="2" num="1.2" lm="10" met="4+6" met_alone="True"><space unit="char" quantity="4"></space><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="2.2">l</w>’<w n="2.3" punct="pt:4"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pt" caesura="1">a</seg></w>.<caesura></caesura> <w n="2.4">D<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5" mp="P">è</seg>s</w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="2.6">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.7" punct="vg:10">nu<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="10" punct="vg">i</seg>t</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3" punct="vg:5">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353" place="4">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg">an</seg>t</w>, <w n="3.4"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="3.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="3.6" punct="dp:8">d<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="dp">i</seg>t</rhyme></pgtc></w> :</l>
						<l n="4" num="1.4" lm="12" met="6+6">« <w n="4.1">J</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="4.3">p<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="4" mp="C">o</seg>s</w> <w n="4.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>rs</w><caesura></caesura> <w n="4.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="4.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.9" punct="vg:10">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" punct="vg">em</seg>ps</w>, <w n="4.10">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="4.11" punct="vg:12">b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space>» <w n="5.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">c<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="5.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="5.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="5.6" punct="ps:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<pgtc id="3" weight="2" schema="CR">m<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="ps">en</seg>t</rhyme></pgtc></w>…</l>
						<l n="6" num="1.6" lm="12" met="6+6">» ‒ <w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>s</w> <w n="6.3" punct="vg:3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3" punct="vg">en</seg></w>, » <w n="6.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4" mp="C">i</seg></w> <w n="6.5">r<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d</w><caesura></caesura> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="6.7">n<seg phoneme="a" type="vs" value="1" rule="343" place="8" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="477" place="9">ï</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.8" punct="pv:12">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="8"></space>« <w n="7.1" punct="vg:2">Bl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="7.2">j</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="7.4" punct="pt:8">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pt">en</seg>t</rhyme></pgtc></w>. »</l>
					</lg>
				</div></body></text></TEI>