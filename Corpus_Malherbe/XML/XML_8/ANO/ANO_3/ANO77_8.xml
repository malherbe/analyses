<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO77" modus="cp" lm_max="12" metProfile="8, 6+6" form="strophe unique" schema="1(ababcbc)" er_moy="1.0" er_max="2" er_min="0" er_mode="0(2/4)" er_moy_et="1.0">
					<head type="main">ÉPIGRAMME CONTRE UN SOT POLITIQUE</head>
					<lg n="1" type="septain" rhyme="ababcbc">
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="1.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="1.2">G<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>z<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.5" punct="vg:8">T<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="2.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="2.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5" punct="vg:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>l<pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>t</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">F<seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M/mp">au</seg>dr<seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="Lp">oi</seg>t</w>-<w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="3.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="3.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="3.5">d</w>’<w n="3.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg" mp="F">e</seg></w>, <w n="3.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="3.9" punct="vg:12">M<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="477" place="12">ï</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="4.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="4.4">m<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="4.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="4.8" punct="pi:12">P<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pi">a</seg>t</rhyme></pgtc></w> ?</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="5.2">f<seg phoneme="a" type="vs" value="1" rule="193" place="2">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="5.4">l</w>’<w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.6" punct="dp:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="dp" caesura="1">i</seg>t</w> :<caesura></caesura> <w n="5.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="5.8">s<seg phoneme="o" type="vs" value="1" rule="435" place="8" mp="M">o</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="5.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="5.11" punct="vg:12">b<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="6.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="6.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>bl<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="6.6">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" mp="C">e</seg>t</w> <w n="6.7">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.8">d</w>’<w n="6.9" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">É</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>t</rhyme></pgtc></w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.7" punct="pt:8">c<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>