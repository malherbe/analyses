<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO4" modus="sm" lm_max="8" metProfile="8" form="strophe unique" schema="1(ababbccdcd)" er_moy="1.5" er_max="6" er_min="0" er_mode="0(3/6)" er_moy_et="2.14">
					<head type="main">LA CROYANCE FONDÉE</head>
					<lg n="1" type="dizain" rhyme="ababbccdcd">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="1.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.4">M<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.5" punct="vg:8">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>r<pgtc id="1" weight="2" schema="CR">m<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>t</rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">M<seg phoneme="œ" type="vs" value="1" rule="151" place="1">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>r</w> <w n="2.2">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="2.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="2.4" punct="pv:8">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">am</seg>br<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="3.2">c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg><pgtc id="1" weight="2" schema="CR">m<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>t</rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="y" type="vs" value="1" rule="d-3" place="2">u</seg><seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="4.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt</w> <w n="4.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="4.5" punct="dp:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rni<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">O</seg>r</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3" punct="vg:5">C<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>qu<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.5" punct="vg:8">f<pgtc id="2" weight="1" schema="GR">i<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2" punct="dp:2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="dp in">i</seg>t</w> : « <w n="6.3" punct="vg:4">M<seg phoneme="œ" type="vs" value="1" rule="151" place="3">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="4" punct="vg">eu</seg>r</w>, <w n="6.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="6.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.6" punct="vg:8">f<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
						<l n="7" num="1.7" lm="8" met="8">» <w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="7.4" punct="vg:4">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg>x</w>, <w n="7.5">M<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="7.7" punct="pi:8">m<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pi">oi</seg></rhyme></pgtc></w> ?</l>
						<l n="8" num="1.8" lm="8" met="8">» ‒ <w n="8.1">C</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="8.3" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="8.4" punct="vg:4">B<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="8.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="8.6" punct="pt:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>tr<pgtc id="4" weight="6" schema="VCR"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>d<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>t</rhyme></pgtc></w>.</l>
						<l n="9" num="1.9" lm="8" met="8">» ‒ <w n="9.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>t</w> <w n="9.2" punct="pe:2">J<seg phoneme="ɑ̃" type="vs" value="1" rule="309" place="2" punct="pe">ean</seg></w> ! » <w n="9.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w>-<w n="9.4" punct="vg:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg in">e</seg></w>, « <w n="9.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7" punct="pv:8">cr<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pv">oi</seg></rhyme></pgtc></w> ;</l>
						<l n="10" num="1.10" lm="8" met="8">» <w n="10.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="10.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.6">l<pgtc id="4" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></pgtc></w> <w n="10.7" punct="pt:8"><pgtc id="4" weight="6" schema="V[CR" part="2">d<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>t</rhyme></pgtc></w>. »</l>
					</lg>
				</div></body></text></TEI>