<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO51" modus="sp" lm_max="8" metProfile="8, 4" form="suite périodique" schema="9(aabb)" er_moy="0.94" er_max="6" er_min="0" er_mode="0(13/18)" er_moy_et="1.9">
					<head type="main">LE SOMMEIL DE VÉNUS</head>
					<head type="form">CHANSON</head>
					<head type="tune">Sur l’air : <hi rend="ital">Ô Filii et Filiæ</hi></head>
					<lg n="1" type="quatrain" rhyme="aabb">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rs</w> <w n="1.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.3">V<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="1.5" punct="pv:8">P<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ph<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="pv">o</seg>s</rhyme></pgtc></w> ;</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="2.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rm<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="2.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6" punct="dp:8">d<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="dp">o</seg>s</rhyme></pgtc></w> :</l>
						<l n="3" num="1.3" lm="8" met="8">« <w n="3.1" punct="vg:2">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, » <w n="3.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w>-<w n="3.3" punct="vg:4"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg in">i</seg>l</w>, « <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="3.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6">qu</w>’<w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8" punct="vg:8"><pgtc id="2" weight="0" schema="[R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="4" met="4"><space unit="char" quantity="6"></space>» <w n="4.1" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="u" type="vs" value="1" rule="211" place="3">u</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ja" type="vs" value="1" rule="ANO51_1" place="4" punct="pe">ia</seg></rhyme></pgtc></w> ! »</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="aabb">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="5.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>d<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8">ain</seg></rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="6.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>vr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w> <w n="6.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="6.6" punct="pv:8">s<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8" punct="pv">ein</seg></rhyme></pgtc></w> ;</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="7.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c</w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="4">ei</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.7" punct="pt:8">tr<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
						<l n="8" num="2.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="8.1" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="u" type="vs" value="1" rule="211" place="3">u</seg><pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ja" type="vs" value="1" rule="ANO51_1" place="4" punct="pe">ia</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="aabb">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="9.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="9.3"><seg phoneme="y" type="vs" value="1" rule="391" place="3">eu</seg>t</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.5">t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<pgtc id="5" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">D</w>’<w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="10.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="10.5" punct="pv:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<pgtc id="5" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pv">é</seg></rhyme></pgtc></w> ;</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="11.3" punct="vg:4">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="11.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="11.5">s</w>’<w n="11.6" punct="dp:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="dp">a</seg></rhyme></pgtc></w> :</l>
						<l n="12" num="3.4" lm="4" met="4"><space unit="char" quantity="6"></space>« <w n="12.1" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="u" type="vs" value="1" rule="211" place="3">u</seg><pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ja" type="vs" value="1" rule="ANO51_1" place="4" punct="pe">ia</seg></rhyme></pgtc></w> ! »</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="aabb">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="1">En</seg><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="13.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="13.5" punct="vg:8">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg><pgtc id="7" weight="2" schema="CR">s<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>rs</rhyme></pgtc></w>,</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="14.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rm<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="14.5" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="7" weight="2" schema="CR">s<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>rs</rhyme></pgtc></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="15.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5" punct="pt:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
						<l n="16" num="4.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="16.1" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="u" type="vs" value="1" rule="211" place="3">u</seg><pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ja" type="vs" value="1" rule="ANO51_1" place="4" punct="pe">ia</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="aabb">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="17.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="17.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="17.5">l</w>’<w n="17.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>dm<pgtc id="9" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="18.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="18.6" punct="dp:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<pgtc id="9" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="dp">er</seg></rhyme></pgtc></w> :</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="19.3">s</w>’<w n="19.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>gm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="19.5">s</w>’<w n="19.6" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>gm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>t<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
						<l n="20" num="5.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="20.1" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="u" type="vs" value="1" rule="211" place="3">u</seg><pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ja" type="vs" value="1" rule="ANO51_1" place="4" punct="pe">ia</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="aabb">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1" punct="vg:2">V<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg>s</w>, <w n="21.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="21.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="21.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="21.5" punct="vg:8"><pgtc id="11" weight="1" schema="GR">y<rhyme label="a" id="11" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">pl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ç<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="22.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="22.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="22.6" punct="vg:8">m<pgtc id="11" weight="1" schema="GR">i<rhyme label="a" id="11" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="23.3">Gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rri<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="23.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="23.5" punct="pt:8">pr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
						<l n="24" num="6.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="24.1" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="u" type="vs" value="1" rule="211" place="3">u</seg><pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="m" type="e"><seg phoneme="ja" type="vs" value="1" rule="ANO51_1" place="4" punct="pe">ia</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="aabb">
						<l n="25" num="7.1" lm="8" met="8">« <w n="25.1" punct="vg:1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, <w n="25.2" punct="vg:2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, » <w n="25.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="25.4">M<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rs</w> <w n="25.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="25.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</rhyme></pgtc></w></l>
						<l n="26" num="7.2" lm="8" met="8"><w n="26.1">Qu</w>’<w n="26.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="26.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="26.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="26.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="26.6" punct="vg:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>t</rhyme></pgtc></w>,</l>
						<l n="27" num="7.3" lm="8" met="8">« <w n="27.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="27.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="27.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="27.4" punct="pt:8">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
						<l n="28" num="7.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="28.1" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="u" type="vs" value="1" rule="211" place="3">u</seg><pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="m" type="e"><seg phoneme="ja" type="vs" value="1" rule="ANO51_1" place="4" punct="pe">ia</seg></rhyme></pgtc></w> ! »</l>
					</lg>
					<lg n="8" type="quatrain" rhyme="aabb">
						<l n="29" num="8.1" lm="8" met="8"><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="29.2">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="29.4">j<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="29.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="29.6">f<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="15" weight="2" schema="CR">ss<rhyme label="a" id="15" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</rhyme></pgtc></w></l>
						<l n="30" num="8.2" lm="8" met="8"><w n="30.1">Qu</w>’<w n="30.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="30.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="30.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="30.5" punct="dp:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg><pgtc id="15" weight="2" schema="CR">ç<rhyme label="a" id="15" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="dp">oi</seg>t</rhyme></pgtc></w> :</l>
						<l n="31" num="8.3" lm="8" met="8"><w n="31.1">Tr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="31.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="31.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="31.4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="31.5" punct="pt:8">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<pgtc id="16" weight="0" schema="R"><rhyme label="b" id="16" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
						<l n="32" num="8.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="32.1" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="u" type="vs" value="1" rule="211" place="3">u</seg><pgtc id="16" weight="0" schema="R"><rhyme label="b" id="16" gender="m" type="e"><seg phoneme="ja" type="vs" value="1" rule="ANO51_1" place="4" punct="pe">ia</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="9" type="quatrain" rhyme="aabb">
						<l n="33" num="9.1" lm="8" met="8"><w n="33.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="33.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="33.3">V<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="33.4">s</w>’<w n="33.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>v<seg phoneme="e" type="vs" value="1" rule="383" place="7">e</seg>ill<pgtc id="17" weight="0" schema="R"><rhyme label="a" id="17" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
						<l n="34" num="9.2" lm="8" met="8"><w n="34.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="34.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="34.3" punct="vg:3">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg></w>, <w n="34.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="34.6" punct="dp:8">r<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<pgtc id="17" weight="0" schema="R"><rhyme label="a" id="17" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="dp">an</seg>t</rhyme></pgtc></w> :</l>
						<l n="35" num="9.3" lm="8" met="8">‒ « <w n="35.1" punct="pe:1"><seg phoneme="e" type="vs" value="1" rule="133" place="1" punct="pe">E</seg>h</w> ! <w n="35.2" punct="vg:2">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="2" punct="vg">oi</seg></w>, <w n="35.3" punct="vg:4">M<seg phoneme="œ" type="vs" value="1" rule="151" place="3">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="4" punct="vg">eu</seg>r</w>, <w n="35.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="35.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="35.6" punct="pe:8">l<pgtc id="18" weight="0" schema="R"><rhyme label="b" id="18" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="342" place="8" punct="pe">à</seg></rhyme></pgtc></w> !</l>
						<l n="36" num="9.4" lm="4" met="4"><space unit="char" quantity="6"></space>» <w n="36.1" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="u" type="vs" value="1" rule="211" place="3">u</seg><pgtc id="18" weight="0" schema="R"><rhyme label="b" id="18" gender="m" type="e"><seg phoneme="ja" type="vs" value="1" rule="ANO51_1" place="4" punct="pe">ia</seg></rhyme></pgtc></w> ! »</l>
					</lg>
				</div></body></text></TEI>