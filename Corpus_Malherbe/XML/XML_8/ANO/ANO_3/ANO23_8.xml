<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO23" modus="sp" lm_max="8" metProfile="8, 6" form="suite de strophes" schema="3[aa] 3[abba]" er_moy="0.33" er_max="2" er_min="0" er_mode="0(7/9)" er_moy_et="0.67">
					<head type="main">LES DÉSOLATIONS ET LES CONSOLATIONS</head>
					<head type="form">VAUDEVILLE</head>
					<lg n="1" type="regexp" rhyme="aaabbaaaabbaaaabba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Cl<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="1.4">gr<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="1.5" punct="vg:8">S<seg phoneme="ɛ" type="vs" value="1" rule="384" place="7">ei</seg>gn<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L</w>’<w n="2.2">h<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="2.3" punct="vg:4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="2.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="2.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.6" punct="pv:8">fl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pv">eu</seg>r</rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="6" met="6"><space unit="char" quantity="4"></space><w n="3.1">C</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="3.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.6" punct="pt:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="a" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rs</w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4">n</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t</w> <w n="4.6" punct="vg:8">r<pgtc id="3" weight="1" schema="GR">i<rhyme label="b" id="3" gender="m" type="a" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="vg">en</seg></rhyme></pgtc></w>,</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="5.6" punct="pv:8">b<pgtc id="3" weight="1" schema="GR">i<rhyme label="b" id="3" gender="m" type="e" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" punct="pv">en</seg></rhyme></pgtc></w> ;</l>
						<l n="6" num="1.6" lm="6" met="6"><space unit="char" quantity="4"></space><w n="6.1">C</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="6.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="6.6" punct="pt:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="e" stanza="2"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="7.3">L<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="7.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="7.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.4">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="8.6" punct="pv:8">C<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="ri_1" place="8" punct="pv">en</seg>t</rhyme></pgtc></w> ;</l>
						<l n="9" num="1.9" lm="6" met="6"><space unit="char" quantity="4"></space><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="9.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.6" punct="pt:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="10" num="1.10" lm="8" met="8"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="10.2">D<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="10.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="10.4">v<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t</w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.6">l</w>’<w n="10.7" punct="vg:8"><pgtc id="6" weight="0" schema="[R"><rhyme label="b" id="6" gender="m" type="a" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>r</rhyme></pgtc></w>,</l>
						<l n="11" num="1.11" lm="8" met="8"><w n="11.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="11.2">d</w>’<w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="11.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.5" punct="pv:8">L<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>d<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pv">o</seg>r</rhyme></pgtc></w> ;</l>
						<l n="12" num="1.12" lm="6" met="6"><space unit="char" quantity="4"></space><w n="12.1">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="12.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.6" punct="pt:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="13" num="1.13" lm="8" met="8"><w n="13.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="13.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="13.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg><pgtc id="7" weight="2" schema="CR">c<rhyme label="a" id="7" gender="m" type="a" stanza="5"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></rhyme></pgtc></w></l>
						<l n="14" num="1.14" lm="8" met="8"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="14.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="14.6" punct="pv:8">c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="7" weight="2" schema="CR">c<rhyme label="a" id="7" gender="m" type="e" stanza="5"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pv">u</seg></rhyme></pgtc></w> ;</l>
						<l n="15" num="1.15" lm="6" met="6"><space unit="char" quantity="4"></space><w n="15.1">C</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="15.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.6" punct="pt:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="f" type="a" stanza="6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="16" num="1.16" lm="8" met="8"><w n="16.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="16.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4">dr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="16.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<pgtc id="9" weight="0" schema="R"><rhyme label="b" id="9" gender="m" type="a" stanza="6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></pgtc></w></l>
						<l n="17" num="1.17" lm="8" met="8"><w n="17.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d</w> <w n="17.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="17.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="17.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="17.6" punct="pv:8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<pgtc id="9" weight="0" schema="R"><rhyme label="b" id="9" gender="m" type="e" stanza="6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pv">in</seg></rhyme></pgtc></w> ;</l>
						<l n="18" num="1.18" lm="6" met="6"><space unit="char" quantity="4"></space><w n="18.1">C</w>’<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="18.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="18.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.6" punct="pt:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="f" type="e" stanza="6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>