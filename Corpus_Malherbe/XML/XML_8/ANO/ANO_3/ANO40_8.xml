<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO40" modus="cp" lm_max="12" metProfile="8, 6+6, 4+6" form="suite de strophes" schema="1[ababb] 1[aa] 1[ababcbcc] 1[abba]" er_moy="1.27" er_max="6" er_min="0" er_mode="0(8/11)" er_moy_et="2.3">
					<head type="main">LE CONSEIL INUTILE</head>
					<lg n="1" type="regexp" rhyme="ababbaaababcbccabba">
						<l n="1" num="1.1" lm="12" met="6+6">« <w n="1.1" punct="vg:2">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="2" punct="vg">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="C">i</seg>l</w> <w n="1.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="1.4">r<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>d</w><caesura></caesura> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="1.6">bru<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="1.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="1.8">v<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="C">ou</seg>s</w> <w n="1.9" punct="dp:12"><seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>tr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space>» <w n="2.1">M<seg phoneme="œ" type="vs" value="1" rule="151" place="1">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>r</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3" punct="vg:6">Pr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="?" type="va" value="1" rule="162" place="6" punct="vg">en</seg>t</w>, <w n="2.4">d<pgtc id="2" weight="6" schema="VC[R" part="1"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</pgtc></w>-<w n="2.5" punct="vg:8"><pgtc id="2" weight="6" schema="VC[R" part="2"><rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="10" met="4+6"><space unit="char" quantity="4"></space>» <w n="3.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="3.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>ct<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="3.4">n<seg phoneme="ø" type="vs" value="1" rule="388" place="6">oeu</seg>ds</w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="3.6" punct="vg:10">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><pgtc id="1" weight="0" schema="R" part="1"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6">» <w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="4.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="4.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="4.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="4.8">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t</w> <w n="4.9">G<pgtc id="2" weight="6" schema="VCR" part="1"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></rhyme></pgtc></w></l>
						<l n="5" num="1.5" lm="10" met="4+6"><space unit="char" quantity="4"></space>» <w n="5.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="5.2">Ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="5.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="5.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="5.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg>s<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space>» <w n="6.1"><seg phoneme="o" type="vs" value="1" rule="435" place="1">O</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.3">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="7" num="1.7" lm="12" met="6+6">» <w n="7.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="7.2" punct="vg:3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>c</w>, <w n="7.3">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="4" mp="M/mp">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="5" mp="Lp">ez</seg></w>-<w n="7.4" punct="vg:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="7.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="9">ain</seg></w> <w n="7.7" punct="vg:12">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>rs<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>nn<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="8"></space>» <w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="8.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="8.6" punct="vg:8"><pgtc id="4" weight="6" schema="[VCR" part="1">r<seg phoneme="a" type="vs" value="1" rule="307" place="7">a</seg>ill<rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>rs</rhyme></pgtc></w>,</l>
						<l n="9" num="1.9" lm="8" met="8"><space unit="char" quantity="8"></space>» <w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="9.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="9.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5" punct="dp:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="b" id="5" gender="f" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="10" num="1.10" lm="12" met="6+6">» <w n="10.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>x</w> <w n="10.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="10.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="10.7">r<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg><pgtc id="4" weight="6" schema="[VCR" part="1">dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></pgtc></w> <w n="10.8"><pgtc id="4" weight="6" schema="[VCR" part="2"><seg phoneme="a" type="vs" value="1" rule="307" place="11" mp="M">a</seg>ill<rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>rs</rhyme></pgtc></w></l>
						<l n="11" num="1.11" lm="8" met="8"><space unit="char" quantity="8"></space>» <w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="11.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="11.4">n</w>’<w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="11.6">qu</w>’<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="11.8">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.9" punct="pt:8"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="b" id="5" gender="f" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="12" num="1.12" lm="12" met="6+6">» ‒ <w n="12.1">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="12.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="12.4" punct="dp:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="dp">i</seg>t</w> : <w n="12.5">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="12.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="12.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="12.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="12.9">tr<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="11">ez</seg></w> <w n="12.10" punct="vg:12">b<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="c" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></rhyme></pgtc></w>,</l>
						<l n="13" num="1.13" lm="12" met="6+6">» <w n="13.1" punct="vg:3">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="13.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="4" mp="C">o</seg>s</w> <w n="13.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>ils</w><caesura></caesura> <w n="13.4">n</w>’<w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="13.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="13.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="13.8" punct="pv:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>ffr<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="b" id="5" gender="f" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="14" num="1.14" lm="12" met="6+6">» <w n="14.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="14.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3">c<seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="14.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="14.6">Ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="14.7" punct="dp:12">Cl<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="6" weight="0" schema="R" part="1"><rhyme label="c" id="6" gender="m" type="e" stanza="3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="dp">on</seg></rhyme></pgtc></w> :</l>
						<l n="15" num="1.15" lm="10" met="4+6"><space unit="char" quantity="4"></space>» <w n="15.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="15.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="15.4">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5" mp="C">e</seg>t</w> <w n="15.5">h<seg phoneme="o" type="vs" value="1" rule="435" place="6" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="15.6">g<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rç<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="c" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></rhyme></pgtc></w></l>
						<l n="16" num="1.16" lm="8" met="8"><space unit="char" quantity="8"></space>» <w n="16.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="16.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="16.6" punct="pv:8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rs<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="a" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="17" num="1.17" lm="8" met="8"><space unit="char" quantity="8"></space>» <w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="17.2">n</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="17.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="17.5">t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="17.6">qu</w>’<w n="17.7"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="17.8">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="17.9" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="8" weight="2" schema="CR" part="1">r<rhyme label="b" id="8" gender="m" type="a" stanza="4"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oî</seg>t</rhyme></pgtc></w>,</l>
						<l n="18" num="1.18" lm="8" met="8"><space unit="char" quantity="8"></space>» <w n="18.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="18.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="18.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="8" weight="2" schema="CR" part="1">r<rhyme label="b" id="8" gender="m" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t</rhyme></pgtc></w></l>
						<l n="19" num="1.19" lm="8" met="8"><space unit="char" quantity="8"></space>» <w n="19.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4">Pr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="?" type="va" value="1" rule="162" place="6">en</seg>t</w> <w n="19.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="19.6" punct="pt:8">d<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="e" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>. »</l>
					</lg>
				</div></body></text></TEI>