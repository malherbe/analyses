<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cap Éternité</title>
				<title type="medium">Édition électronique</title>
				<author key="GIL">
					<name>
						<forename>Charles</forename>
						<surname>GILL</surname>
					</name>
					<date from="1871" to="1918">1871-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cap Éternité</title>
						<author>Charles Gill</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/charlesgilllcapeeternite.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Cap Éternité</title>
								<author>Charles Gill</author>
								<idno type="URL">https://archive.org/details/lecapternitpomes00gill</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Édition du devoir</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le poème "Premier amour" a été divisé en autant de poèmes qu’il y a de sonnets.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉTOILES FILANTES</head><div type="poem" key="GIL27" modus="cm" lm_max="12" metProfile="6+6" form="sonnet non classique" schema="abba cdcd eef gfg" er_moy="1.86" er_max="6" er_min="0" er_mode="2(3/7)" er_moy_et="1.88">
					<head type="main">Sonnet à Lamartine</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="1.2">pl<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="1.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="1.4">f<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="9">oû</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.8" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg><pgtc id="1" weight="2" schema="CR">n<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="2.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="2.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="2.4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.7" punct="vg:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="3.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="3.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="3.6" punct="vg:6">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg" caesura="1">o</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.8">m<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="3.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="3.10">mi<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">H<seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>s<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>nn<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="4.2">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>ph<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.5">d</w>’<w n="4.6" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="1" weight="2" schema="CR">n<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" rhyme="cdcd">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">Fr<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.2">d</w>’<w n="5.3" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="5.4"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="5.5">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="8" mp="M">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="5.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>d<pgtc id="3" weight="1" schema="GR">i<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="6.2" punct="vg:2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="6.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="6.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rs<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="6.6">l</w>’<w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="7" mp="M">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="6.8" punct="vg:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="4" weight="2" schema="CR">n<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">aî</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="7.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="8" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="9" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="7.7" punct="vg:12">c<pgtc id="3" weight="1" schema="GR">i<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="8.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.4">l<seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="M">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="8.6">br<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="8.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="8.8" punct="pe:12">g<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="4" weight="2" schema="CR">n<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" rhyme="eef">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="9.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="9.4"><seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="9.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ps</w> <w n="9.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="9.8">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt</w> <w n="9.9"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<pgtc id="5" weight="0" schema="R"><rhyme label="e" id="5" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12">e</seg>r</rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="10.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2" mp="M">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="10.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="10.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="10.5" punct="vg:6">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" punct="vg" caesura="1">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="10.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="10.8">l</w>’<w n="10.9"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>th<pgtc id="5" weight="0" schema="R"><rhyme label="e" id="5" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12">e</seg>r</rhyme></pgtc></w></l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="11.4">d</w>’<w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r</w><caesura></caesura> <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="11.7">str<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="11.8" punct="pe:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>d<pgtc id="6" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>c<rhyme label="f" id="6" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="4" rhyme="gfg">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">P<seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg>x</w> <w n="12.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="12.4" punct="pe:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="pe" caesura="1">in</seg>s</w> !<caesura></caesura> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="12.6">j<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="12.7">v<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>br<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="7" weight="2" schema="CR">r<rhyme label="g" id="7" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>t</rhyme></pgtc></w></l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="13.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>x</w> <w n="13.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="13.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="13.6" punct="vg:6">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" punct="vg" caesura="1">e</seg>rs</w>,<caesura></caesura> <w n="13.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="13.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="13.9">c<seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>rs</w> <w n="13.10">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="13.11" punct="vg:12">p<pgtc id="6" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>s<rhyme label="f" id="6" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">I</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="14.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="14.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="14.6" punct="pe:12"><pgtc id="7" weight="2" schema="[CR">fr<rhyme label="g" id="7" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pe">on</seg>t</rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>