<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">CHEVAUX DE LA MER</head><div type="poem" key="DLR589" modus="cp" lm_max="12" metProfile="6, 6+6" form="suite périodique" schema="3(abab)" er_moy="1.67" er_max="6" er_min="0" er_mode="0(3/6)" er_moy_et="2.13">
					<head type="main">NOCTURNE</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="1.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">em</seg>pl<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="1.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" caesura="1">oin</seg></w><caesura></caesura> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="1.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>r</w> <w n="1.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>ctr<pgtc id="1" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>s<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="12"></space><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4" punct="pt:6">f<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg></rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="3.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.6">fr<pgtc id="1" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>s<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="4.3">ph<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>sph<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4" punct="pt:6">bl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="5.3">m</w>’<w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>f<seg phoneme="y" type="vs" value="1" rule="dc-3" place="5" mp="M">u</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="5.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="5.7">nu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>t</w> <w n="5.8" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>g<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="6" met="6"><space unit="char" quantity="12"></space><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="6.2">l</w>’<w n="6.3" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="7.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="7.3">l<seg phoneme="y" type="vs" value="1" rule="dc-3" place="5" mp="M">u</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="7.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="7.7" punct="vg:12">cr<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="2.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="8.1" punct="pt:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">Im</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="4">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="9.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="9.8">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="5" weight="2" schema="CR">n<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="6" met="6"><space unit="char" quantity="12"></space><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>l</w> <w n="10.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="10.5">v<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</rhyme></pgtc></w></l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">ph<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>sph<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="11.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="11.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="11.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">o<pgtc id="5" weight="2" schema="(c)[R" part="1">n</pgtc></seg></w> <w n="11.8"><pgtc id="5" weight="2" schema="(c)[R" part="2"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="12" num="3.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="12.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="12.3"><seg phoneme="œ" type="vs" value="1" rule="286" place="5">œ</seg>il</w> <w n="12.4" punct="pt:6">n<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>