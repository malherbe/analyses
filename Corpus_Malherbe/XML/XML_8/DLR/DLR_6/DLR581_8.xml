<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">LE SPHINX</head><div type="poem" key="DLR581" modus="cp" lm_max="12" metProfile="8, 4, 6=6" form="suite périodique" schema="5(abab)" er_moy="8.7" er_max="36" er_min="0" er_mode="2(3/10)" er_moy_et="11.28">
					<head type="main">D’ALEXANDRIE</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="1.1">R<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg>s</w> <w n="1.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rs</w> <w n="1.3"><pgtc id="1" weight="20" schema="[CV[CV[CV[CR" part="1">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</pgtc></w> <w n="1.4"><pgtc id="1" weight="20" schema="[CV[CV[CV[CR" part="2">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></pgtc></w> <w n="1.5"><pgtc id="1" weight="20" schema="[CV[CV[CV[CR" part="3">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>r</pgtc></w> <w n="1.6"><pgtc id="1" weight="20" schema="[CV[CV[CV[CR" part="4">bl<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="4" met="4"><space unit="char" quantity="16"></space><w n="2.1">D</w>’<w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="2.3">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.5" punct="pt:4">n<pgtc id="2" weight="1" schema="GR" part="1">u<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="pt">i</seg>t</rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="12" mp6="C" met="6−6"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="C" caesura="1">u</seg></w><caesura></caesura> <w n="3.5" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>, <w n="3.6"><pgtc id="1" weight="20" schema="[CV[CV[CV[CR" part="1">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="P">u</seg>r</pgtc></w> <w n="3.7"><pgtc id="1" weight="20" schema="[CV[CV[CV[CR" part="2">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></pgtc></w> <w n="3.8"><pgtc id="1" weight="20" schema="[CV[CV[CV[CR" part="3">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="11">e</seg>r</pgtc></w> <w n="3.9" punct="vg:12"><pgtc id="1" weight="20" schema="[CV[CV[CV[CR" part="4">bl<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d</w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="4.3"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.4" punct="pt:8">c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>rc<pgtc id="2" weight="1" schema="GR" part="1">u<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pt">i</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.4">pr<seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="6">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">ë</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.7"><pgtc id="3" weight="2" schema="[CR" part="1">c<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="4" met="4"><space unit="char" quantity="16"></space><w n="6.1" punct="vg:1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" punct="vg">e</seg></w>, <w n="6.2" punct="vg:4">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg><pgtc id="4" weight="2" schema="CR" part="1">m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="7.3" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>rqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="7.5">cr<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="7.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="7.8">cr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="7.9" punct="vg:12"><pgtc id="3" weight="2" schema="[CR" part="1">c<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="8.2">gr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="8.3">d</w>’<w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="8.5">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="8.6" punct="pt:8">m<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>l<pgtc id="4" weight="2" schema="CR" part="1">m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pt">an</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="9.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="9.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lmi<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="9.4"><pgtc id="5" weight="8" schema="[CV[CR" part="1">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</pgtc></w> <w n="9.5" punct="vg:8"><pgtc id="5" weight="8" schema="[CV[CR" part="2">t<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="4" met="4"><space unit="char" quantity="16"></space><w n="10.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="10.2" punct="vg:4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg><pgtc id="6" weight="2" schema="CR" part="1">f<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>ls</rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>T</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="11.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="11.5">l</w>’<w n="11.6">h<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="11.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>ct<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.9"><pgtc id="5" weight="8" schema="[CV[CR" part="1">l<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="C">eu</seg>rs</pgtc></w> <w n="11.10"><pgtc id="5" weight="8" schema="[CV[CR" part="2">t<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="12" num="3.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">s</w>’<w n="12.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="12.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>gs</w> <w n="12.6" punct="pt:8"><pgtc id="6" weight="2" schema="[CR" part="1">f<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>ls</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3">n<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><pgtc id="7" weight="16" schema="[VCVCVR" part="1"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>pt<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="366" place="8">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="14" num="4.2" lm="4" met="4"><space unit="char" quantity="16"></space><w n="14.1">S</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="14.4" punct="pv:4">l<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="m" type="a"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4" punct="pv">oin</seg></rhyme></pgtc></w> ;</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="15.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="15.3">l</w>’<w n="15.4" punct="vg:5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg" mp="F">e</seg></w>, <w n="15.5">l</w>’<w n="15.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6" caesura="1">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="15.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.8"><pgtc id="7" weight="16" schema="[VCVCVR" part="1"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>g<seg phoneme="i" type="vs" value="1" rule="493" place="10" mp="M">y</seg>pt<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="366" place="12">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="16" num="4.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="16.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="16.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="16.6" punct="pt:8">c<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="m" type="e"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="pt">oin</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abab">
						<l n="17" num="5.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2"><pgtc id="9" weight="36" schema="[C[VCV[C[V[CV[CVVCR" part="1">l</pgtc></w>’<w n="17.3"><pgtc id="9" weight="36" schema="[C[VCV[C[V[CV[CVVCR" part="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</pgtc></w> <w n="17.4"><pgtc id="9" weight="36" schema="[C[VCV[C[V[CV[CVVCR" part="3">d</pgtc></w>’<w n="17.5"><pgtc id="9" weight="36" schema="[C[VCV[C[V[CV[CVVCR" part="4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</pgtc></w> <w n="17.6"><pgtc id="9" weight="36" schema="[C[VCV[C[V[CV[CVVCR" part="5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></pgtc></w> <w n="17.7" punct="vg:8"><pgtc id="9" weight="36" schema="[C[VCV[C[V[CV[CVVCR" part="6">Cl<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>p<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="4" met="4"><space unit="char" quantity="16"></space><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="18.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d</w> <w n="18.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="18.4" punct="vg:4">s<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>r</rhyme></pgtc></w>,</l>
						<l n="19" num="5.3" lm="12" mp6="M" met="4+4+4"><w n="19.1">N<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="19.3" punct="vg:5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg" mp="F">e</seg></w>,<caesura></caesura> <w n="19.4"><pgtc id="9" weight="36" schema="[C[VCV[C[V[CV[CVVCR" part="1">l</pgtc></w>’<w n="19.5"><pgtc id="9" weight="36" schema="[C[VCV[C[V[CV[CVVCR" part="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</pgtc></w> <w n="19.6"><pgtc id="9" weight="36" schema="[C[VCV[C[V[CV[CVVCR" part="3">d</pgtc></w>’<w n="19.7"><pgtc id="9" weight="36" schema="[C[VCV[C[V[CV[CVVCR" part="4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" caesura="2">o</seg>r</pgtc></w><caesura></caesura> <w n="19.8"><pgtc id="9" weight="36" schema="[C[VCV[C[V[CV[CVVCR" part="5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></pgtc></w> <w n="19.9" punct="vg:12"><pgtc id="9" weight="36" schema="[C[VCV[C[V[CV[CVVCR" part="6">Cl<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>p<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="20" num="5.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="20.1">F<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="20.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>gti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.4">si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="20.5" punct="pt:8">n<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>