<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">LA GUERRE</head><div type="poem" key="DLR644" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="2(abbab) 1(abbba) 1(abbaa) 1(ababa)" er_moy="0.53" er_max="2" er_min="0" er_mode="0(11/15)" er_moy_et="0.88">
					<head type="main">REFRAIN</head>
					<lg n="1" type="quintil" rhyme="abbab">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="1.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg><pgtc id="1" weight="2" schema="CR">s<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">R<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>m<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="6">um</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.5" punct="vg:8">p<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">M<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="3.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.5">l</w>’<w n="3.6">h<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>br<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>s<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ts</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.4">l</w>’<w n="4.5" punct="vg:8">h<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="1" weight="2" schema="CR">z<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg></rhyme></pgtc></w>,</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">c<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.7" punct="pt:8">S<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quintil" rhyme="abbba">
						<l n="6" num="2.1" lm="8" met="8"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">m</w>’<w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="6.5">s<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.7">gl<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></pgtc></w></l>
						<l n="7" num="2.2" lm="8" met="8"><w n="7.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="7.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="7.4" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rs<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>nn<pgtc id="4" weight="2" schema="CR"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="2.3" lm="8" met="8"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="8.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">qu</w>’<w n="8.4"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23"><pgtc id="4" weight="2" schema="(c)[R" part="1">e</pgtc></seg></w> <w n="8.7"><pgtc id="4" weight="2" schema="(c)[R" part="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></pgtc></w> <w n="8.8"><pgtc id="4" weight="2" schema="(c)[R" part="3"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="9" num="2.4" lm="8" met="8"><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="9.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="9.3">qu</w>’<w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="9.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="9.8">l</w>’<w n="9.9" punct="vg:8"><pgtc id="4" weight="0" schema="[R" part="1"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="2.5" lm="8" met="8"><w n="10.1">Ch<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="10.2">d</w>’<w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="10.4" punct="vg:4">h<seg phoneme="o" type="vs" value="1" rule="318" place="4" punct="vg">au</seg>t</w>, <w n="10.5">fl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="10.6">d</w>’<w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="10.8" punct="pv:8">b<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg>s</rhyme></pgtc></w> ;</l>
					</lg>
					<lg n="3" type="quintil" rhyme="abbaa">
						<l n="11" num="3.1" lm="8" met="8"><w n="11.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="11.2" punct="vg:3">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg></w>, <w n="11.3">d</w>’<w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="11.5">l<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="11.6">ch<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="12" num="3.2" lm="8" met="8"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="12.5" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
						<l n="13" num="3.3" lm="8" met="8"><w n="13.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>s</w> <w n="13.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>rs</w> <w n="13.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="13.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sc<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d</rhyme></pgtc></w></l>
						<l n="14" num="3.4" lm="8" met="8"><w n="14.1">R<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="14.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="14.6">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>rs</w> <w n="14.7" punct="pv:8">r<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
						<l n="15" num="3.5" lm="8" met="8"><w n="15.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="15.2" punct="vg:3">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg></w>, <w n="15.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="15.4">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="15.5" punct="pv:8">m<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="5" weight="2" schema="CR" part="1">r<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
					</lg>
					<lg n="4" type="quintil" rhyme="abbab">
						<l n="16" num="4.1" lm="8" met="8"><w n="16.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="16.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="16.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="16.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="16.7" punct="vg:8"><pgtc id="7" weight="2" schema="[CR" part="1">bl<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>c</rhyme></pgtc></w>,</l>
						<l n="17" num="4.2" lm="8" met="8"><w n="17.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="17.2">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="17.3" punct="vg:6">qu<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6" punct="vg">en</seg></w>, <w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="17.5" punct="vg:8">s<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="18" num="4.3" lm="8" met="8"><w n="18.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="18.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="18.3">s</w>’<w n="18.4" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="353" place="4">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="18.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="18.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="18.7" punct="dp:8">c<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="19" num="4.4" lm="8" met="8"><w n="19.1">Bru<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>t</w> <w n="19.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="19.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="19.5" punct="vg:8">M<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="7" weight="2" schema="CR" part="1">l<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>ch</rhyme></pgtc></w>,</l>
						<l n="20" num="4.5" lm="8" met="8"><w n="20.1">J</w>’<w n="20.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="20.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.4">c<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="20.7" punct="pt:8">S<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="quintil" rhyme="ababa">
						<l n="21" num="5.1" lm="8" met="8"><w n="21.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="21.3">v<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="21.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="21.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="21.6" punct="vg:8">t<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="vg">o</seg>rd</rhyme></pgtc></w>,</l>
						<l n="22" num="5.2" lm="8" met="8"><w n="22.1" punct="vg:2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="22.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="22.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="22.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <w n="22.5" punct="vg:8">bl<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="23" num="5.3" lm="8" met="8"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="23.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="23.4">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="23.5" punct="pi:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pi">o</seg>r</rhyme></pgtc></w> ?</l>
						<l n="24" num="5.4" lm="8" met="8"><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="24.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="24.3" punct="vg:4">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="24.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="24.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="24.6" punct="pi:8">li<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg>s</rhyme></pgtc></w> ?</l>
						<l n="25" num="5.5" lm="8" met="8">‒ <w n="25.1">J</w>’<w n="25.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="25.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.4">c<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="25.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="25.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="25.7" punct="pt:8">m<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pt">o</seg>rt</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>