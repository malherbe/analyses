<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR762" modus="sp" lm_max="8" metProfile="6, 8" form="" schema="" er_moy="4.15" er_max="32" er_min="0" er_mode="2(8/13)" er_moy_et="8.17">
				<head type="main">Berceuse de la Grande Sœur</head>
				<lg n="1" rhyme="None">
					<l n="1" num="1.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="1.1" punct="vg:2">D<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="2" punct="vg">o</seg></w>, <w n="1.2" punct="vg:4">b<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="1.3" punct="pe:6">d<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg><pgtc id="1" weight="2" schema="CR">d<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="6" punct="pe">o</seg></rhyme></pgtc></w> !</l>
					<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="2.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5" punct="pt:6">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg><pgtc id="1" weight="2" schema="CR">d<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="pt">eau</seg></rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="3.2">l</w>’<w n="3.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="3.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="3.6" punct="pt:8">m<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>s<pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="4.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="4.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="4.6" punct="vg:8">d<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="9" weight="2" schema="CR">r<rhyme label="c" id="9" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1" punct="pe:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="pe">o</seg>rs</w> ! <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">I</seg>l</w> <w n="5.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="5.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="5.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.7" punct="pt:8"><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="6.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="6.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4" punct="vg:4">t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s</w>, <w n="6.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="9" weight="2" schema="CR">r<rhyme label="c" id="9" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></rhyme></pgtc></w></l>
					<l n="7" num="1.7" lm="6" met="6"><space unit="char" quantity="4"></space>« <w n="7.1" punct="vg:2"><pgtc id="3" weight="32" schema="CVCV[CVCV[CVCR" part="1">D<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="2" punct="vg">o</seg></pgtc></w>, <w n="7.2" punct="vg:4"><pgtc id="3" weight="32" schema="CVCV[CVCV[CVCR" part="2">b<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></pgtc></w>, <w n="7.3" punct="pe:6"><pgtc id="3" weight="32" schema="CVCV[CVCV[CVCR" part="3">d<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>d<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="6" punct="pe">o</seg></rhyme></pgtc></w> ! »</l>
				</lg>
				<lg n="2" rhyme="None">
					<l n="8" num="2.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="8.1" punct="vg:2"><pgtc id="3" weight="32" schema="CVCV[CVCV[CVCR" part="1">D<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="2" punct="vg">o</seg></pgtc></w>, <w n="8.2" punct="vg:4"><pgtc id="3" weight="32" schema="CVCV[CVCV[CVCR" part="2">b<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></pgtc></w>, <w n="8.3" punct="pe:6"><pgtc id="3" weight="32" schema="CVCV[CVCV[CVCR" part="3">d<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>d<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="6" punct="pe">o</seg></rhyme></pgtc></w> !</l>
					<l n="9" num="2.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="9.1">J</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="9.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.5" punct="pt:6">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg><pgtc id="3" weight="2" schema="CR" part="1">d<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="pt">eau</seg></rhyme></pgtc></w>.</l>
					<l n="10" num="2.3" lm="8" met="8"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w>-<w n="10.2" punct="pe:3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pe">oi</seg></w> ! <w n="10.3">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>il</w> <w n="10.5">t</w>’<w n="10.6" punct="pt:8"><pgtc id="4" weight="6" schema="[VCR" part="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>v<rhyme label="e" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="11" num="2.4" lm="8" met="8"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="11.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="11.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="11.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="11.6">l</w>’<w n="11.7" punct="vg:8"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="5" weight="2" schema="CR" part="1">v<rhyme label="f" id="5" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="vg">e</seg>rs</rhyme></pgtc></w>,</l>
					<l n="12" num="2.5" lm="8" met="8"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="12.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="12.3">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="12.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="12.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="12.6">bi<pgtc id="4" weight="6" schema="V[CR" part="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></pgtc></w> <w n="12.7" punct="vg:8"><pgtc id="4" weight="6" schema="V[CR" part="2">v<rhyme label="e" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="13" num="2.6" lm="8" met="8"><w n="13.1" punct="pe:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="pe ps">o</seg>rs</w> !… <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="13.3">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg>s</w> <w n="13.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="13.5" punct="vg:8"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="5" weight="2" schema="CR" part="1">v<rhyme label="f" id="5" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="vg">e</seg>rts</rhyme></pgtc></w>,</l>
					<l n="14" num="2.7" lm="6" met="6"><space unit="char" quantity="4"></space><w n="14.1" punct="vg:2">D<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="2" punct="vg">o</seg></w>, <w n="14.2" punct="vg:4">b<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="14.3" punct="pe:6">d<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg><pgtc id="6" weight="2" schema="CR" part="1">d<rhyme label="a" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="6" punct="pe">o</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="3" rhyme="None">
					<l n="15" num="3.1" lm="6" met="6"><space unit="char" quantity="4"></space><w n="15.1" punct="vg:2">D<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="2" punct="vg">o</seg></w>, <w n="15.2" punct="vg:4">b<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="15.3" punct="pe:6">d<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg><pgtc id="6" weight="2" schema="CR" part="1">d<rhyme label="a" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="6" punct="pe">o</seg></rhyme></pgtc></w> !</l>
					<l n="16" num="3.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="16.1">J</w>’<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="16.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="16.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.5" punct="pt:6">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg><pgtc id="6" weight="2" schema="CR" part="1">d<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="pt">eau</seg></rhyme></pgtc></w>.</l>
					<l n="17" num="3.3" lm="8" met="8"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="17.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="17.3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="17.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="17.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="17.7" punct="pt:8">r<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="g" id="7" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					<l n="18" num="3.4" lm="8" met="8"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="18.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="18.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="18.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="18.7" punct="vg:8"><pgtc id="8" weight="2" schema="[CR" part="1">t<rhyme label="h" id="8" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>t</rhyme></pgtc></w>,</l>
					<l n="19" num="3.5" lm="8" met="8"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="19.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="19.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="19.6" punct="pt:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>p<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="g" id="7" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					<l n="20" num="3.6" lm="8" met="8"><w n="20.1" punct="pe:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="pe">o</seg>rs</w> ! <w n="20.2">M<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="20.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.4">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="20.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="20.6" punct="pt:8"><pgtc id="8" weight="2" schema="[CR" part="1">t<rhyme label="h" id="8" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pt">oi</seg></rhyme></pgtc></w>.</l>
					<l n="21" num="3.7" lm="6" met="6"><space unit="char" quantity="4"></space><w n="21.1" punct="vg:2">D<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="2" punct="vg">o</seg></w>, <w n="21.2" punct="vg:4">b<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg></w>, <w n="21.3" punct="pe:6">d<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>d<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="a" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="6" punct="pe">o</seg></rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>