<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR729" modus="sp" lm_max="7" metProfile="7, 5" form="suite de strophes" schema="4(abab) 2(abba)" er_moy="1.33" er_max="6" er_min="0" er_mode="0(6/12)" er_moy_et="1.7">
				<head type="main">La Toupie</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1" punct="vg:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="1.2" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.4" punct="vg:7">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg><pgtc id="1" weight="2" schema="CR">p<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="5" met="5"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="2.5" punct="vg:5">f<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" punct="vg">o</seg>rt</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1" punct="vg:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="3.2" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="3.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4">t</w>’<w n="3.5" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><pgtc id="1" weight="2" schema="CR">p<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="5" met="5"><w n="4.1" punct="vg:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3" punct="pt:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="pt">o</seg>r</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abba">
					<l n="5" num="2.1" lm="7" met="7"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="5.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.6"><pgtc id="3" weight="2" schema="[CR">t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="5" met="5"><w n="6.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="6.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4" punct="vg:5">t<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="7" met="7"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">im</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.5" punct="vg:7">cr<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="vg">oi</seg>t</rhyme></pgtc></w>,</l>
					<l n="8" num="2.4" lm="5" met="5"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="8.4" punct="pe:5"><pgtc id="3" weight="2" schema="[CR">t<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abba">
					<l n="9" num="3.1" lm="7" met="7"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="9.3">bru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="9.6">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg><pgtc id="5" weight="2" schema="CR">r<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="5" met="5"><w n="10.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="10.3" punct="vg:5"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="7" met="7"><w n="11.1" punct="vg:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="11.2" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="11.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.4" punct="vg:7">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rv<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="7">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="5" met="5"><w n="12.1" punct="vg:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="12.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="12.4" punct="pt:5"><pgtc id="5" weight="2" schema="[CR">r<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="pt">on</seg>d</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="7" met="7"><w n="13.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="13.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="13.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="13.4" punct="vg:7">f<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<pgtc id="12" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>gu<rhyme label="a" id="12" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="4.2" lm="5" met="5"><w n="14.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="14.2">t</w>’<w n="14.3" punct="pt:5"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
					<l n="15" num="4.3" lm="7" met="7"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="15.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="15.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ls<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ss<pgtc id="12" weight="6" schema="V[CR" part="1"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></pgtc></w> <w n="15.5"><pgtc id="12" weight="6" schema="V[CR" part="2">g<rhyme label="a" id="12" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="5" met="5"><w n="16.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">s</w>’<w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.4" punct="pt:5">p<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="b" id="7" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="7" met="7"><w n="17.1" punct="vg:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="17.2" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="17.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="17.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><pgtc id="8" weight="2" schema="CR" part="1">m<rhyme label="a" id="8" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
					<l n="18" num="5.2" lm="5" met="5"><w n="18.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="18.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="18.4" punct="vg:5">c<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="b" id="9" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="249" place="5" punct="vg">œu</seg>r</rhyme></pgtc></w>,</l>
					<l n="19" num="5.3" lm="7" met="7"><w n="19.1" punct="vg:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="19.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="19.4" punct="vg:7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg><pgtc id="8" weight="2" schema="CR" part="1">mm<rhyme label="a" id="8" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="20" num="5.4" lm="5" met="5"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="20.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="20.3" punct="pe:5">s<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="b" id="9" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="5" punct="pe">œu</seg>r</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="7" met="7"><w n="21.1" punct="vg:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="21.2" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="21.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="21.4" punct="vg:7">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg><pgtc id="10" weight="2" schema="CR" part="1">p<rhyme label="a" id="10" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="22" num="6.2" lm="5" met="5"><w n="22.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="22.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="22.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="22.5" punct="vg:5">f<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="b" id="11" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" punct="vg">o</seg>rt</rhyme></pgtc></w>,</l>
					<l n="23" num="6.3" lm="7" met="7"><w n="23.1" punct="vg:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="23.2" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="23.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.4">t</w>’<w n="23.5" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><pgtc id="10" weight="2" schema="CR" part="1">p<rhyme label="a" id="10" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="7">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="24" num="6.4" lm="5" met="5"><w n="24.1" punct="vg:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="24.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3" punct="pe:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="b" id="11" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="pe">o</seg>r</rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>