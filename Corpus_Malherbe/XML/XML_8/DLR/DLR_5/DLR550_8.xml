<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHEZ NOUS</head><div type="poem" key="DLR550" modus="cm" lm_max="12" metProfile="6=6" form="suite périodique" schema="7(aa)" er_moy="0.86" er_max="2" er_min="0" er_mode="0(4/7)" er_moy_et="0.99">
					<head type="main">NOCTURNE PARISIEN</head>
					<lg n="1" type="distique" rhyme="aa">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="1.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="1.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="1.5" punct="vg:6">ju<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">in</seg></w>,<caesura></caesura> <w n="1.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="1.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="1.9"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg><pgtc id="1" weight="2" schema="CR">nn<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="2.3">f<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="2.5" punct="vg:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="2.6">m<seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>s</w> <w n="2.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="2.9" punct="pe:12">d<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">î</seg><pgtc id="1" weight="2" schema="CR">n<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pe">er</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="distique" rhyme="aa">
						<l n="3" num="2.1" lm="12" met="6+6"><w n="3.1">L</w>’<w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="3.3">j<seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="3.5" punct="vg:6">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="6" punct="vg" caesura="1">o</seg></w>,<caesura></caesura> <w n="3.6">l</w>’<w n="3.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="3.8">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.9">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="3.10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="3.11">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="11">oi</seg></w> <w n="3.12" punct="vg:12">f<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="2.2" lm="12" met="6+6"><w n="4.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="4.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="4.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="4.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="4.9">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="4.10" punct="pt:12">cl<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="distique" rhyme="aa">
						<l n="5" num="3.1" lm="12" met="6+6"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="5.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ls</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>ts</w><caesura></caesura> <w n="5.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="5.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="5.7">r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="5.8" punct="vg:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>g<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="448" place="12" punct="vg">u</seg>s</rhyme></pgtc></w>,</l>
						<l n="6" num="3.2" lm="12" met="6+6"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>s</w>, <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="6.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>ps</w><caesura></caesura> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="6.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="6.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="10" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="6.8" punct="pt:12">pl<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pt">u</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="distique" rhyme="aa">
						<l n="7" num="4.1" lm="12" met="6+6"><w n="7.1">Tr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="7.2" punct="pt:4">p<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="M">eu</seg>pl<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pt">er</seg>s</w>. <w n="7.3">P<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="7.4" punct="pt:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="pt">an</seg>s</w>. <w n="7.5">L<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="7.6">S<seg phoneme="ɛ" type="vs" value="1" rule="385" place="10">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="7.7" punct="pt:12">r<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="8" num="4.2" lm="12" met="6+6"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="8.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="8.3" punct="pt:3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="pt">ai</seg>r</w>. <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">E</seg>t</w> <w n="8.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="8.6">c</w>’<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="8.8">l</w>’<w n="8.9">h<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.11">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="8.12" punct="pt:12">l<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="distique" rhyme="aa">
						<l n="9" num="5.1" lm="12" met="6+6"><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="9.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="9.5">d</w>’<w n="9.6" punct="ps:6"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="ps" caesura="1">oi</seg>s</w>…<caesura></caesura> <w n="9.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">On</seg></w> <w n="9.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="9.9" punct="vg:10">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="9" mp="M">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="vg">ai</seg>t</w>, <w n="9.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="vg">o</seg>rs</rhyme></pgtc></w>,</l>
						<l n="10" num="5.2" lm="12" met="6+6"><w n="10.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="10.2">ch<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="10.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="Lc">e</seg>lqu</w>’<w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" caesura="1">un</seg></w><caesura></caesura> <w n="10.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="10.8" punct="pt:12">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>h<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>rs</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="6" type="distique" rhyme="aa">
						<l n="11" num="6.1" lm="12" mp6="F" met="4+4+4"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="11.3" punct="vg:4">l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg" caesura="1">à</seg></w>,<caesura></caesura> <w n="11.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="11.6" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg" caesura="2">i</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="11.7" punct="pt:12"><seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="6" weight="2" schema="CR">v<rhyme label="a" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="12" num="6.2" lm="12" met="6+6"><w n="12.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="12.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="12.4" punct="pt:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pt" caesura="1">a</seg>s</w>.<caesura></caesura> <w n="12.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">On</seg></w> <w n="12.6"><seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="12.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="12.8" punct="pt:12"><pgtc id="6" weight="2" schema="[CR">v<rhyme label="a" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="7" type="distique" rhyme="aa">
						<l n="13" num="7.1" lm="12" met="6+6"><w n="13.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="13.2">l</w>’<w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="13.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="13.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="13.6" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="13.7">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>l</w> <w n="13.8" punct="vg:9"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9" punct="vg">é</seg></w>, <w n="13.9">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>l</w> <w n="13.10" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="7" weight="2" schema="CR">t<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="7.2" lm="12" met="6+6"><w n="14.1" punct="vg:1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" punct="vg">e</seg></w>, <w n="14.2" punct="vg:3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" punct="vg">o</seg>rs</w>, <w n="14.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="C">i</seg>l</w> <w n="14.4">n</w>’<w n="14.5"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="14.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="14.9" punct="ps:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="7" weight="2" schema="CR">t<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="ps">é</seg></rhyme></pgtc></w>…</l>
					</lg>
				</div></body></text></TEI>