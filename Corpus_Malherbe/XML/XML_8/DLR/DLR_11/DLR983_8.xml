<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR983" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="3(ababcdcd) 1(abab)" er_moy="1.86" er_max="8" er_min="0" er_mode="0(6/14)" er_moy_et="2.33">
				<head type="main">BALLADE DE LA BALLADE</head>
				<lg n="1" type="huitain" rhyme="ababcdcd">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.3">n</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="1.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.7"><pgtc id="1" weight="0" schema="[R" part="1"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="2.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>s</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7"><pgtc id="2" weight="2" schema="[C[R" part="1">d</pgtc></w>’<w n="2.8" punct="pt:8"><pgtc id="2" weight="2" schema="[C[R" part="2"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>rt</rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">V<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="3.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="3.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="3.6" punct="pt:8">m<pgtc id="1" weight="0" schema="R" part="1"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="u" type="vs" value="1" rule="d-2" place="1">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="4.3">g<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="4.4" punct="pe:8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg><pgtc id="2" weight="2" schema="CR" part="1">d<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>rd</rhyme></pgtc></w> !</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="5.4">d</w>’<w n="5.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="5.6">l</w>’<w n="5.7"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg></w>-<w n="5.8"><pgtc id="3" weight="2" schema="[CR" part="1">c<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r</rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">m<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>t</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6" punct="pt:8"><pgtc id="4" weight="8" schema="[CVCR" part="1">B<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<rhyme label="d" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="7.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.4" punct="vg:7">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>t</w>, <w n="7.5"><pgtc id="3" weight="2" schema="[CR" part="1">c<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r</rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="8.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.4" punct="tc:5">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="ti">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> — <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="8.6" punct="pt:8"><pgtc id="4" weight="8" schema="[CVCR" part="1">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="d" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="huitain" rhyme="ababcdcd">
					<l n="9" num="2.1" lm="8" met="8"><w n="9.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="9.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="9.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>x<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.6" punct="dp:8">c<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
					<l n="10" num="2.2" lm="8" met="8"><w n="10.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="10.3">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="10.4">r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="10.6" punct="pt:8"><pgtc id="6" weight="2" schema="[CR" part="1">p<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>rt</rhyme></pgtc></w>.</l>
					<l n="11" num="2.3" lm="8" met="8">(<w n="11.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="11.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.3">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>x</w> <w n="11.4">n</w>’<w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="11.7" punct="pe:8">c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>mm<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !)</l>
					<l n="12" num="2.4" lm="8" met="8"><w n="12.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="12.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="12.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="12.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="6" weight="2" schema="CR" part="1">p<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rt</rhyme></pgtc></w></l>
					<l n="13" num="2.5" lm="8" met="8"><w n="13.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="13.2">d</w>’<w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="13.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="13.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="13.6">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg><pgtc id="7" weight="2" schema="CR" part="1">t<rhyme label="c" id="7" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd</rhyme></pgtc></w></l>
					<l n="14" num="2.6" lm="8" met="8"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="14.3">p<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="14.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="14.5" punct="vg:8">m<pgtc id="8" weight="6" schema="VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="d" id="8" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="15" num="2.7" lm="8" met="8"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="15.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="15.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="15.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="7">o</seg>p</w> <w n="15.6" punct="pt:8"><pgtc id="7" weight="2" schema="[CR" part="1">t<rhyme label="c" id="7" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>rd</rhyme></pgtc></w>.</l>
					<l n="16" num="2.8" lm="8" met="8">— <w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="16.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="16.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="16.6" punct="pt:8">b<pgtc id="8" weight="6" schema="VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<rhyme label="d" id="8" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="huitain" rhyme="ababcdcd">
					<l n="17" num="3.1" lm="8" met="8"><w n="17.1" punct="vg:1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1" punct="vg">O</seg>r</w>, <w n="17.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">r<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="17.4">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="17.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="17.6">H<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="18" num="3.2" lm="8" met="8"><w n="18.1">V<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w> <w n="18.2">qu</w>’<w n="18.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="18.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="18.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="18.7" punct="pt:8">ch<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>r</rhyme></pgtc></w>.</l>
					<l n="19" num="3.3" lm="8" met="8"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="19.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.4">n<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>l</w> <w n="19.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="19.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="20" num="3.4" lm="8" met="8"><w n="20.1">M<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rn</w>-<w n="20.2" punct="pi:4">St<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pi">e</seg></w> ? <w n="20.3" punct="pe:5">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="pe">on</seg></w> ! <w n="20.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">En</seg></w> <w n="20.5" punct="pt:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>rd</rhyme></pgtc></w>.</l>
					<l n="21" num="3.5" lm="8" met="8"><w n="21.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="21.2">cr<seg phoneme="y" type="vs" value="1" rule="454" place="2">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="21.3">j<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>t</w> <w n="21.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="21.5" punct="vg:8">p<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="c" id="11" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>rt</rhyme></pgtc></w>,</l>
					<l n="22" num="3.6" lm="8" met="8">(<w n="22.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="22.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="22.3"><seg phoneme="y" type="vs" value="1" rule="251" place="3">eû</seg>t</w> <w n="22.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="22.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="22.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="22.7" punct="pf:8">H<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg><pgtc id="12" weight="2" schema="CR" part="1">ll<rhyme label="d" id="12" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,)</l>
					<l n="23" num="3.7" lm="8" met="8"><w n="23.1">C</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="23.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="23.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="23.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>fr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5">ain</seg></w> <w n="23.6">qu</w>’<w n="23.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="23.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="23.9" punct="pt:8">d<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="c" id="11" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>rd</rhyme></pgtc></w>.</l>
					<l n="24" num="3.8" lm="8" met="8"><w n="24.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="24.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="24.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="24.6" punct="pt:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="12" weight="2" schema="CR" part="1">ll<rhyme label="d" id="12" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1" lm="8" met="8"><w n="25.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="25.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.3">fu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="25.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="25.6" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="d-2" place="7">ou</seg><pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>rd</rhyme></pgtc></w>,</l>
					<l n="26" num="4.2" lm="8" met="8"><w n="26.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="26.2">j</w>’<w n="26.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="26.4">l</w>’<w n="26.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="26.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="26.7" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rm<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="14" weight="2" schema="CR" part="1">l<rhyme label="b" id="14" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="27" num="4.3" lm="8" met="8"><w n="27.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="27.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3" punct="ps:3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" punct="ps">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ds</w> <w n="27.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="27.6">p<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rt</rhyme></pgtc></w></l>
					<l n="28" num="4.4" lm="8" met="8"><w n="28.1">Qu</w>’<w n="28.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="28.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="28.5">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="28.7" punct="pt:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="14" weight="2" schema="CR" part="1">ll<rhyme label="b" id="14" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>