<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">THE SKULL</head><div type="poem" key="DLR880" modus="sp" lm_max="8" metProfile="8, (6)" form="strophe unique" schema="1(abba)" er_moy="1.0" er_max="2" er_min="0" er_mode="0(1/2)" er_moy_et="1.0">
					<head type="number">X</head>
					<head type="main">TÊTE DE MORT</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="1.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">ch<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4">cr<seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.6" punct="vg:8">r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="2.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">qu</w>’<w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="2.5">c<seg phoneme="a" type="vs" value="1" rule="307" place="6">a</seg>ill<seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="2.6" punct="vg:8"><pgtc id="2" weight="2" schema="[CR">gr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">t</w>’<w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="3.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="3.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.7" punct="dp:8"><pgtc id="2" weight="2" schema="[CR">r<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="dp">i</seg>s</rhyme></pgtc></w> :</l>
						<l n="4" num="1.4" lm="6"><space unit="char" quantity="4"></space><w n="4.1">J</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="4.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5" punct="pt:6">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>