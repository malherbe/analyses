<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE VIII</head><head type="main_part">Poète</head><div type="poem" key="DLR1096" modus="cp" lm_max="12" metProfile="8, 4, 6+6, (6)" form="suite de strophes" schema="3[abab] 1[abba] 2[aa]" er_moy="0.6" er_max="2" er_min="0" er_mode="0(7/10)" er_moy_et="0.92">
					<head type="main">L’Archange</head>
					<head type="sub_2">LES SEPT DOULEURS D’OCTOBRE, Ferenczi, 1930</head>
					<lg n="1" type="regexp" rhyme="abababb">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.4" punct="pt:6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pt in" caesura="1">an</seg>t</w>.<caesura></caesura> (<w n="1.5">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="1.7">d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="1.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="1.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="1.10" punct="pt:12">n<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="12" punct="pt">om</seg></rhyme></pgtc></w>.)</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">L</w>’<w n="2.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="2.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="2.6" punct="pt:8">d<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6" punct="vg:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>c<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>d</rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="M">oi</seg>si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="4.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="4.5" punct="pt:6">g<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="pt" caesura="1">au</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>.<caesura></caesura> <w n="4.6" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="189" place="7" punct="vg">E</seg>t</w>, <w n="4.7">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="4.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="4.9">y<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>x</w> <w n="4.10" punct="vg:12">tr<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">Qu<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="5.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="5.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="5.8" punct="pt:12">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>t</rhyme></pgtc></w>.</l>
						<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rsqu</w>’<w n="6.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="6.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="6.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="6.8" punct="vg:8">qu<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="7.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="7.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="M">eu</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="7.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="8">en</seg>s</w> <w n="7.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="7.7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="10">œu</seg>r</w> <w n="7.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="11">en</seg></w> <w n="7.9" punct="pt:12">b<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="regexp" rhyme="aaaa">
						<l n="8" num="2.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">Tr<seg phoneme="o" type="vs" value="1" rule="433" place="1">o</seg>p</w> <w n="8.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="8.5">d</w>’<w n="8.6"><seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="8.8" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></rhyme></pgtc></w>,</l>
						<l n="9" num="2.2" lm="12" met="6+6"><w n="9.1">Tr<seg phoneme="o" type="vs" value="1" rule="433" place="1">o</seg>p</w> <w n="9.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="9.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="9.4">m</w>’<w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="9.6">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.9" punct="vg:9">v<seg phoneme="i" type="vs" value="1" rule="482" place="9" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="9.10" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="2.3" lm="4" met="4"><space unit="char" quantity="16"></space><w n="10.1" punct="vg:1">Tr<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.2" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>pl<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="11" num="2.4" lm="12" met="6+6"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="11.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="11.5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="11.6">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="11.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="P">a</seg>r</w> <w n="11.8">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" mp="C">e</seg>t</w> <w n="11.9" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>b<pgtc id="6" weight="2" schema="CR">s<rhyme label="a" id="6" gender="m" type="a" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="regexp" rhyme="babaaabab">
						<l n="12" num="3.1" lm="6"><space unit="char" quantity="12"></space><w n="12.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="12.5" punct="vg:6"><pgtc id="7" weight="0" schema="[R"><rhyme label="b" id="7" gender="f" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="13" num="3.2" lm="4" met="4"><space unit="char" quantity="16"></space><w n="13.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>g</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="13.4" punct="vg:4"><pgtc id="6" weight="2" schema="CR">s<rhyme label="a" id="6" gender="m" type="e" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>g</rhyme></pgtc></w>,</l>
						<l n="14" num="3.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="14.2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="14.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="14.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="14.5">cr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="14.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="14.7">qu</w>’<w n="14.8">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="14.10">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="14.11">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="14.12" punct="vg:12">f<pgtc id="7" weight="0" schema="R"><rhyme label="b" id="7" gender="f" type="e" stanza="4"><seg phoneme="a" type="vs" value="1" rule="193" place="12">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="15" num="3.4" lm="4" met="4"><space unit="char" quantity="16"></space><w n="15.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="15.3" punct="vg:4"><pgtc id="8" weight="2" schema="[CR">m<rhyme label="a" id="8" gender="m" type="a" stanza="5"><seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
						<l n="16" num="3.5" lm="12" met="6+6"><w n="16.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="M">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>r</w>, <w n="16.2" punct="vg:4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>r</w>, <w n="16.3" punct="vg:6">b<seg phoneme="o" type="vs" value="1" rule="315" place="5" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="16.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>l</w> <w n="16.5" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="8" weight="2" schema="CR">m<rhyme label="a" id="8" gender="m" type="e" stanza="5"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
						<l n="17" num="3.6" lm="4" met="4"><space unit="char" quantity="16"></space><w n="17.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="17.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="17.3">m<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="m" type="a" stanza="6"><seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></rhyme></pgtc></w></l>
						<l n="18" num="3.7" lm="12" met="6+6"><w n="18.1" punct="vg:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>rs</w>, <w n="18.2" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>rs</w>, <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="18.4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="18.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="18.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="18.7">l</w>’<w n="18.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.9" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11" mp="M">in</seg>c<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="f" type="a" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="19" num="3.8" lm="4" met="4"><space unit="char" quantity="16"></space><w n="19.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="19.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="19.3" punct="vg:4">m<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="m" type="e" stanza="6"><seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
						<l n="20" num="3.9" lm="12" met="6+6"><w n="20.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="20.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="20.5">j<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="20.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="20.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="20.9">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="20.10" punct="pe:12"><pgtc id="10" weight="0" schema="[R" part="1"><rhyme label="b" id="10" gender="f" type="e" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg>s</rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>