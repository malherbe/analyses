<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">MAI, JOLI MAI !</head><div type="poem" key="DLR804" modus="cm" lm_max="12" metProfile="6=6" form="suite périodique" schema="4(abab)" er_moy="1.25" er_max="2" er_min="0" er_mode="2(5/8)" er_moy_et="0.97">
					<head type="main">PRIMAVERA</head>
					<lg n="1" type="quatrain" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="1.3" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg></w>,<caesura></caesura> <w n="1.4">r<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rs</w> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="1.7" punct="vg:12">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Pr<seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="M">i</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.6">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="2.8" punct="vg:12">g<seg phoneme="ɛ" type="vs" value="1" rule="306" place="11" mp="M">ai</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">H<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="3.2">n<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="3.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="3.5" punct="vg:6">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg></w>,<caesura></caesura> <w n="3.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="3.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="3.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="3.9" punct="dp:11">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" punct="dp in" mp="F">e</seg></w> : « <w n="3.10">J</w>’<w n="3.11" punct="pe:12"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> ! »</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="4.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>c<seg phoneme="a" type="vs" value="1" rule="365" place="5" mp="M">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="4.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="4.6" punct="pe:12">n<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="11" mp="M">eau</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe">é</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="5.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="5.3" punct="vg:4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>t</w>, <w n="5.4">pl<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="5.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="5.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>rs</w> <w n="5.7">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="5.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="5.9" punct="vg:12">f<seg phoneme="œ" type="vs" value="1" rule="406" place="11" mp="M">eu</seg>ill<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="6.3">p<seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg>s</w><caesura></caesura> <w n="6.4">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="6.6" punct="vg:12">l<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>x<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="7.4" punct="vg:3">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>t</w>, <w n="7.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="7.6">l</w>’<w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r</w><caesura></caesura> <w n="7.8">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="7.10">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t</w> <w n="7.11" punct="vg:12">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="8.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="8.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>cs</w><caesura></caesura> <w n="8.7">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="7">en</seg>t</w> <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="8.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="8.10"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg>x</w> <w n="8.11" punct="pt:12">ci<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="9" num="3.1" lm="12" mp6="M" mp4="F" met="8+4"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="9.2">l</w>’<w n="9.3" punct="vg:4">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rb<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="9.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>ll<seg phoneme="i" type="vs" value="1" rule="d-1" place="7" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" caesura="1">on</seg>s</w><caesura></caesura> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">â</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="5" weight="2" schema="CR">r<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="10.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="10.5">g<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="10.7" punct="pt:12"><pgtc id="6" weight="2" schema="[CR">l<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pt">ai</seg>t</rhyme></pgtc></w>.</l>
						<l n="11" num="3.3" lm="12" mp7="F" met="4+4+4"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="11.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" caesura="1">e</seg></w><caesura></caesura> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="11.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="11.6" punct="vg:8">r<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg" caesura="2">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>s</w> <w n="11.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="11.9" punct="vg:12"><pgtc id="5" weight="2" schema="[CR">tr<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="12.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="12.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="12.4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="12.5">j<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="12.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.7"><seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="C">i</seg>l</w> <w n="12.8" punct="pt:12">f<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="6" weight="2" schema="CR">ll<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pt">ai</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abab">
						<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="13.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="13.4">ch<seg phoneme="e" type="vs" value="1" rule="347" place="5" mp="P">ez</seg></w> <w n="13.5" punct="vg:6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="13.6">n<seg phoneme="a" type="vs" value="1" rule="343" place="7" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="477" place="8">ï</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.7" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="7" weight="2" schema="CR">r<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1" punct="vg:2">Fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" mp="M">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>r</w>, <w n="14.2" punct="vg:4">r<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="14.3" punct="vg:6"><seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="vg" caesura="1">eau</seg>x</w>,<caesura></caesura> <w n="14.4" punct="vg:8">b<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="vg">e</seg>ts</w>, <w n="14.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="14.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="14.7" punct="vg:12"><pgtc id="8" weight="2" schema="[CR">j<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>rs</rhyme></pgtc></w>,</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fm">e</seg></w>-<w n="15.2" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="15.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="15.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="15.5" punct="vg:6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>rs</w>,<caesura></caesura> <w n="15.6" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg" mp="F">e</seg></w>, <w n="15.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="15.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="15.9" punct="vg:12">pr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg><pgtc id="7" weight="2" schema="CR">r<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">Cr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="16.2" punct="vg:3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" punct="vg">e</seg></w>, <w n="16.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="16.4" punct="vg:6">f<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="16.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="16.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="16.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="P">ou</seg>r</w> <w n="16.8" punct="pe:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="8" weight="2" schema="CR">j<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>rs</rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>