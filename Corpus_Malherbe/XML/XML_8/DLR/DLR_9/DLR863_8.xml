<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VIII</head><head type="main_part">BONHEURS</head><div type="poem" key="DLR863" modus="sp" lm_max="8" metProfile="8, 3" form="suite périodique" schema="8(abba)" er_moy="0.38" er_max="2" er_min="0" er_mode="0(13/16)" er_moy_et="0.78">
					<head type="main">ART POÉTIQUE</head>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">di<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="1.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6" punct="vg:8">t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="2.2">d</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rd</w> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="2.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="2.6" punct="vg:8"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="3" met="3"><space unit="char" quantity="10"></space><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">m<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ti<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="4.6" punct="pt:8">p<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="5.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>c<pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2" punct="vg:2">l<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>s</w>, <w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>th<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.4" punct="pt:8">p<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="7" num="2.3" lm="3" met="3"><space unit="char" quantity="10"></space><w n="7.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="7.2">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>s<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>t</w> <w n="8.5" punct="pt:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">Cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">ain</seg>s</w> <w n="9.2" punct="vg:5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rb<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="9.3" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>th<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="10.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="10.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="10.5" punct="pt:8">b<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>t</rhyme></pgtc></w>.</l>
						<l n="11" num="3.3" lm="3" met="3"><space unit="char" quantity="10"></space><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="11.2">t<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</rhyme></pgtc></w></l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="12.2" punct="vg:2">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="12.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w>-<w n="12.5"><seg phoneme="i" type="vs" value="1" rule="497" place="6">y</seg></w> <w n="12.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.7" punct="pt:8"><pgtc id="5" weight="0" schema="[R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abba">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="13.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">im</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="13.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.5" punct="pt:8">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>j<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
						<l n="14" num="4.2" lm="8" met="8">(<w n="14.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">n</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="14.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="14.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="14.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="14.7" punct="pe:8">c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg><pgtc id="8" weight="2" schema="CR">mm<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !)</l>
						<l n="15" num="4.3" lm="3" met="3"><space unit="char" quantity="10"></space><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="15.3"><pgtc id="8" weight="2" schema="[CR">m<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></rhyme></pgtc></w></l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="16.4">l</w>’<w n="16.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="16.7" punct="pt:8">c<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>rt</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abba">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="17.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="17.3">l</w>’<w n="17.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>dv<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="17.6" punct="vg:8">m<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="18.3">fu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="18.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="18.5">l<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="10" weight="2" schema="CR">t<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>f</rhyme></pgtc></w></l>
						<l n="19" num="5.3" lm="3" met="3"><space unit="char" quantity="10"></space><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>dj<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>c<pgtc id="10" weight="2" schema="CR">t<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>f</rhyme></pgtc></w></l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="20.4" punct="pt:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>rch<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abba">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="21.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="21.3">r<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg>thm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="21.5" punct="pt:8">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>s<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="m" type="a"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="pt">oin</seg></rhyme></pgtc></w>.</l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="22.2" punct="vg:3">fi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="22.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="22.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="22.5">c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
						<l n="23" num="6.3" lm="3" met="3"><space unit="char" quantity="10"></space><w n="23.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="23.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<pgtc id="12" weight="0" schema="R"><rhyme label="b" id="12" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg>s</rhyme></pgtc></w></l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="24.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="24.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="24.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="24.7" punct="pt:8">l<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="m" type="e"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="pt">oin</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="abba">
						<l n="25" num="7.1" lm="8" met="8"><w n="25.1">F<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="25.2" punct="vg:4">l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="25.4" punct="pe:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>s<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="26" num="7.2" lm="8" met="8"><w n="26.1">C</w>’<w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="26.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="26.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="26.5">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="26.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="26.8">dr<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</rhyme></pgtc></w></l>
						<l n="27" num="7.3" lm="3" met="3"><space unit="char" quantity="10"></space><w n="27.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3" punct="vg:3">v<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>t</rhyme></pgtc></w>,</l>
						<l n="28" num="7.4" lm="8" met="8"><w n="28.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="28.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="28.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="28.4" punct="vg:6">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></w>, <w n="28.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="28.6" punct="pt:8">l<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="8" type="quatrain" rhyme="abba">
						<l n="29" num="8.1" lm="8" met="8"><w n="29.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="29.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="29.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="29.4"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<pgtc id="15" weight="0" schema="R"><rhyme label="a" id="15" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l</rhyme></pgtc></w></l>
						<l n="30" num="8.2" lm="8" met="8"><w n="30.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="30.3">t</w>’<w n="30.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="30.5" punct="pv:5">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="pv">er</seg></w> ; <w n="30.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="30.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="30.8" punct="vg:8">p<pgtc id="16" weight="0" schema="R"><rhyme label="b" id="16" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="31" num="8.3" lm="3" met="3"><space unit="char" quantity="10"></space><w n="31.1">F<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="31.2" punct="vg:3">r<pgtc id="16" weight="0" schema="R"><rhyme label="b" id="16" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="32" num="8.4" lm="8" met="8"><w n="32.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="32.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="32.3">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="32.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="32.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="32.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="32.7" punct="pt:8">m<pgtc id="15" weight="0" schema="R"><rhyme label="a" id="15" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>l</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>