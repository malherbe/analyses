<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR783" modus="cp" lm_max="12" metProfile="4, 8, 6+6" form="suite périodique" schema="11(aaa)" er_moy="0.5" er_max="2" er_min="0" er_mode="0(16/22)" er_moy_et="0.84">
				<head type="main">PRÉFACE</head>
				<lg n="1" type="tercet" rhyme="aaa">
					<l n="1" num="1.1" lm="12" met="6+6"><hi rend="ital"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ctr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="1.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="1.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.7">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>rs</w> <w n="1.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="1.9">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11">en</seg>t</w> <w n="1.10" punct="vg:12"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</hi></l>
					<l n="2" num="1.2" lm="4" met="4"><space unit="char" quantity="16"></space><hi rend="ital"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4" punct="vg:4">cu<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</hi></l>
					<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space><hi rend="ital"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.7" punct="pt:8">s<pgtc id="1" weight="1" schema="GR">u<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</hi></l>
				</lg>
				<lg n="2" type="tercet" rhyme="aaa">
					<l n="4" num="2.1" lm="12" met="6+6"><hi rend="ital"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="4.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="4.5" punct="vg:6">t<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ll<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>ls</w>,<caesura></caesura> <w n="4.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="4.7">d<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="4.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9">ain</seg>s</w> <w n="4.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="4.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="2" weight="2" schema="CR">v<rhyme label="a" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</rhyme></pgtc></w>,</hi></l>
					<l n="5" num="2.2" lm="4" met="4"><space unit="char" quantity="16"></space><hi rend="ital"><w n="5.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="5.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="5.3"><pgtc id="2" weight="2" schema="[CR">v<rhyme label="a" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</rhyme></pgtc></w></hi></l>
					<l n="6" num="2.3" lm="8" met="8"><space unit="char" quantity="8"></space><hi rend="ital"><w n="6.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.4" punct="dp:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="2" weight="2" schema="CR">v<rhyme label="a" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="dp">an</seg>t</rhyme></pgtc></w> :</hi></l>
				</lg>
				<lg n="3" type="tercet" rhyme="aaa">
					<l n="7" num="3.1" lm="12" met="6+6"><hi rend="ital">‒ <w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="2" mp="Lp">e</seg>s</w>-<w n="7.3" punct="pi:3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="pi ps">u</seg></w> ?… <w n="7.4">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="5" mp="Lp">e</seg>s</w>-<w n="7.6" punct="pi:6">t<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pi ps" caesura="1">u</seg></w> ?<caesura></caesura>… <w n="7.7">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fm">e</seg></w>-<w n="7.8">m<seg phoneme="wa" type="vs" value="1" rule="423" place="9">oi</seg></w> <w n="7.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="7.10" punct="pe:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>s<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</hi></l>
					<l n="8" num="3.2" lm="4" met="4"><space unit="char" quantity="16"></space><hi rend="ital"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="8.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="8.4" punct="vg:4">r<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</hi></l>
					<l n="9" num="3.3" lm="8" met="8"><space unit="char" quantity="8"></space><hi rend="ital"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="4">um</seg></w> <w n="9.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="9.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="9.6" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</hi></l>
				</lg>
				<lg n="4" type="tercet" rhyme="aaa">
					<l n="10" num="4.1" lm="12" met="6+6"><hi rend="ital"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg></w> <w n="10.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="10.4">fl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="10.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="10.6">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="10.7" punct="pt:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></pgtc></w>.</hi></l>
					<l n="11" num="4.2" lm="4" met="4"><space unit="char" quantity="16"></space><hi rend="ital"><w n="11.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="11.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4" punct="vg:4">p<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg>x</rhyme></pgtc></w>,</hi></l>
					<l n="12" num="4.3" lm="8" met="8"><space unit="char" quantity="8"></space><hi rend="ital"><w n="12.1">R<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>ds</w>-<w n="12.2" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="12.3">sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ctr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.4" punct="pe:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">im</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg>x</rhyme></pgtc></w> !</hi></l>
				</lg>
				<lg n="5" type="tercet" rhyme="aaa">
					<l n="13" num="5.1" lm="12" met="6+6"><hi rend="ital"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="13.3">s</w>’<w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" mp="M">ê</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="13.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="13.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="13.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>xtr<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></hi></l>
					<l n="14" num="5.2" lm="4" met="4"><space unit="char" quantity="16"></space><hi rend="ital"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2">ch<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.3" punct="pt:4">bl<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></rhyme></pgtc></w>.</hi></l>
					<l n="15" num="5.3" lm="8" met="8"><space unit="char" quantity="8"></space><hi rend="ital"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="15.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.3">m</w>’<w n="15.4" punct="dp:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="dp in">ai</seg></w> : « <w n="15.5">M<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w>-<w n="15.6" punct="pe:8">m<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> ! »</hi></l>
				</lg>
				<lg n="6" type="tercet" rhyme="aaa">
					<l n="16" num="6.1" lm="12" met="6+6"><hi rend="ital"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="16.3" punct="vg:3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>s</w>, <w n="16.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="16.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d</w><caesura></caesura> <w n="16.7">d</w>’<w n="16.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="16.9">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="16.10" punct="vg:12">m<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>r<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>r</rhyme></pgtc></w>,</hi></l>
					<l n="17" num="6.2" lm="4" met="4"><space unit="char" quantity="16"></space><hi rend="ital"><w n="17.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="17.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="17.3" punct="vg:4">n<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>r</rhyme></pgtc></w>,</hi></l>
					<l n="18" num="6.3" lm="8" met="8"><space unit="char" quantity="8"></space><hi rend="ital"><w n="18.1">L<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rd</w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="18.5">d</w>’<w n="18.6" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>r</rhyme></pgtc></w>,</hi></l>
				</lg>
				<lg n="7" type="tercet" rhyme="aaa">
					<l n="19" num="7.1" lm="12" met="6+6"><hi rend="ital"><w n="19.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="19.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="19.4" punct="vg:6">l<seg phoneme="i" type="vs" value="1" rule="493" place="6" punct="vg" caesura="1">y</seg>s</w>,<caesura></caesura> <w n="19.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="19.6">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="19.7" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg><pgtc id="7" weight="2" schema="CR">n<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</hi></l>
					<l n="20" num="7.2" lm="4" met="4"><space unit="char" quantity="16"></space><hi rend="ital"><w n="20.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>gt</w>-<w n="20.2">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>q</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg><pgtc id="7" weight="2" schema="CR">nn<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg>s</rhyme></pgtc></w></hi></l>
					<l n="21" num="7.3" lm="8" met="8"><space unit="char" quantity="8"></space><hi rend="ital"><w n="21.1">R<seg phoneme="i" type="vs" value="1" rule="d-1" place="1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="21.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="21.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="21.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="21.5" punct="pt:8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="7" weight="2" schema="CR">n<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</hi></l>
				</lg>
				<lg n="8" type="tercet" rhyme="aaa">
					<l n="22" num="8.1" lm="12" met="6+6"><hi rend="ital"><w n="22.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="22.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="22.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="22.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="22.5">l<seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="22.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="22.7">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="22.8" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">em</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</rhyme></pgtc></w>,</hi></l>
					<l n="23" num="8.2" lm="4" met="4"><space unit="char" quantity="16"></space><hi rend="ital"><w n="23.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="23.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="23.4">c<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</rhyme></pgtc></w></hi></l>
					<l n="24" num="8.3" lm="8" met="8"><space unit="char" quantity="8"></space><hi rend="ital"><w n="24.1">Fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="24.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="24.5" punct="pt:8">fl<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>r</rhyme></pgtc></w>.</hi></l>
				</lg>
				<lg n="9" type="tercet" rhyme="aaa">
					<l n="25" num="9.1" lm="12" met="6+6"><hi rend="ital">‒ <w n="25.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="25.2">t</w>’<w n="25.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="25.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="25.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="25.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="25.7" punct="vg:6">v<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="25.8"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="25.9">v<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1" place="9" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="25.10">tr<seg phoneme="o" type="vs" value="1" rule="433" place="11">o</seg>p</w> <w n="25.11" punct="pe:12"><pgtc id="9" weight="2" schema="[CR">br<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</hi></l>
					<l n="26" num="9.2" lm="4" met="4"><space unit="char" quantity="16"></space><hi rend="ital"><w n="26.1" punct="vg:1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" punct="vg">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="26.2"><seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg></w> <w n="26.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="26.4" punct="vg:4"><pgtc id="9" weight="2" schema="[CR">r<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</hi></l>
					<l n="27" num="9.3" lm="8" met="8"><space unit="char" quantity="8"></space><hi rend="ital"><w n="27.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="27.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="27.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="27.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t</w> <w n="27.6" punct="pe:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</hi></l>
				</lg>
				<lg n="10" type="tercet" rhyme="aaa">
					<l n="28" num="10.1" lm="12" met="6+6"><hi rend="ital"><w n="28.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="28.2">l</w>’<w n="28.3" punct="vg:2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="28.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="28.6" punct="vg:6">s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="28.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="28.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="28.9">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="28.10">c<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="m" type="a"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="12">oin</seg></rhyme></pgtc></w></hi></l>
					<l n="29" num="10.2" lm="4" met="4"><space unit="char" quantity="16"></space><hi rend="ital"><w n="29.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="29.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="29.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="29.4" punct="vg:4">f<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="m" type="e"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4" punct="vg">oin</seg></rhyme></pgtc></w>,</hi></l>
					<l n="30" num="10.3" lm="8" met="8"><space unit="char" quantity="8"></space><hi rend="ital"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="30.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="30.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="30.4">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="30.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="30.6" punct="dp:8">l<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="m" type="a"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="dp">oin</seg></rhyme></pgtc></w> :</hi></l>
				</lg>
				<lg n="11" type="tercet" rhyme="aaa">
					<l n="31" num="11.1" lm="12" met="6+6"><hi rend="ital">‒ <w n="31.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="31.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="Lp">a</seg>s</w>-<w n="31.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="31.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="31.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="31.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="31.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="31.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="31.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="31.10">j<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>r</w> <w n="31.11">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="31.12" punct="vg:12">b<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</hi></l>
					<l n="32" num="11.2" lm="4" met="4"><space unit="char" quantity="16"></space><hi rend="ital"><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="32.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="32.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="32.4" punct="pi:4">pr<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pi ps">e</seg></rhyme></pgtc></w> ?…</hi></l>
					<l n="33" num="11.3" lm="8" met="8"><space unit="char" quantity="8"></space><hi rend="ital"><w n="33.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="33.2" punct="dp:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="dp in">i</seg>t</w> : ‒ <w n="33.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="33.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s</w> <w n="33.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="33.6" punct="pt:8">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</hi></l>
				</lg>
			</div></body></text></TEI>