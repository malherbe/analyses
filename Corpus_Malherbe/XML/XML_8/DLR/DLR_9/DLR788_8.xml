<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">L’AUTOMNE</head><div type="poem" key="DLR788" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique" schema="8(aa)" er_moy="1.25" er_max="6" er_min="0" er_mode="0(5/8)" er_moy_et="1.98">
					<head type="main">TEMPÊTE D’OCTOBRE</head>
					<lg n="1" type="distique" rhyme="aa">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="1.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">em</seg>pl<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="1.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>t</w><caesura></caesura> <w n="1.4">l</w>’<w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366" place="9">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>r<pgtc id="1" weight="2" schema="CR">d<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="2.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="2.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="2.6" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="1" weight="2" schema="CR">d<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					</lg>
					<lg n="2" type="distique" rhyme="aa">
						<l n="3" num="2.1" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="3.2">f<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="3.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="3.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="3.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="3.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="3.8">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="2" weight="2" schema="CR">l<rhyme label="a" id="2" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>rs</rhyme></pgtc></w></l>
						<l n="4" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="4.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="4.4">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.5">qu</w>’<w n="4.6"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="4.7">pl<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>t</w> <w n="4.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="4.9" punct="pt:8"><pgtc id="2" weight="2" schema="[CR">fl<rhyme label="a" id="2" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="distique" rhyme="aa">
						<l n="5" num="3.1" lm="12" met="6+6"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">f<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="5.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.7">b<pgtc id="3" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ll<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="3.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rt</w> <w n="6.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rpr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.4" punct="pt:8">m<pgtc id="3" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="distique" rhyme="aa">
						<l n="7" num="4.1" lm="12" met="6+6"><w n="7.1" punct="pe:2">T<seg phoneme="a" type="vs" value="1" rule="343" place="1" mp="M">a</seg>ï<seg phoneme="o" type="vs" value="1" rule="318" place="2" punct="pe">au</seg>t</w> ! <w n="7.2">C</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="7.5" punct="vg:6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" punct="vg" caesura="1">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="7.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="7.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="7.9">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="10" mp="M">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="11">ain</seg></w> <w n="7.10">d</w>’<w n="7.11" punct="vg:12"><pgtc id="4" weight="0" schema="[R"><rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="vg">o</seg>r</rhyme></pgtc></w>,</l>
						<l n="8" num="4.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="8.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="8.5">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="8.7" punct="pt:8">c<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pt">o</seg>r</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="distique" rhyme="aa">
						<l n="9" num="5.1" lm="12" met="6+6"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="9.2">s</w>’<w n="9.3" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="9.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.5" punct="vg:6">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="vg" caesura="1">o</seg>rd</w>,<caesura></caesura> <w n="9.6">s</w>’<w n="9.7" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pt">i</seg>t</w>. <w n="9.8">L<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="9.9">f<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="11">ê</seg>t</w> <w n="9.10" punct="vg:12">br<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="5.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="10.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.6" punct="pt:8">j<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="6" type="distique" rhyme="aa">
						<l n="11" num="6.1" lm="12" met="6+6"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="11.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="11.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="11.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.7">v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w> <w n="11.8">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="11.9">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>b<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>t</rhyme></pgtc></w></l>
						<l n="12" num="6.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="12.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.6" punct="pt:8">t<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="7" type="distique" rhyme="aa">
						<l n="13" num="7.1" lm="12" met="6+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="13.4">s</w>’<w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="13.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="13.8">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.9"><seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.10">f<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="14" num="7.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="14.2" punct="vg:3">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="14.4"><seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.6" punct="vg:8">b<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					</lg>
					<lg n="8" type="distique" rhyme="aa">
						<l n="15" num="8.1" lm="12" met="6+6"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="15.2">l</w>’<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="15.4" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>r</w>,<caesura></caesura> <w n="15.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="15.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="15.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rb<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>s</w> <w n="15.8">d</w>’<w n="15.9" punct="vg:12"><pgtc id="8" weight="0" schema="[R"><rhyme label="a" id="8" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12" punct="vg">o</seg>r</rhyme></pgtc></w>,</l>
						<l n="16" num="8.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">v<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="16.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="16.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="16.7" punct="pt:8">m<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" punct="pt">o</seg>rt</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>