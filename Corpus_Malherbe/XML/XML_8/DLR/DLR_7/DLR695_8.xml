<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">MÉDITATIONS</head><div type="poem" key="DLR695" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite de strophes" schema="2[aa] 2[abba]" er_moy="2.0" er_max="6" er_min="0" er_mode="2(3/6)" er_moy_et="2.0">
					<head type="main">OMBRES</head>
					<lg n="1" type="regexp" rhyme="aaa">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">c<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.3"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="1.4">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="1.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="1.6" punct="vg:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="428" place="9">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="1.7" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">m<rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12" punct="vg">an</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.3"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="1" weight="2" schema="CR">m<rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8">en</seg>t</rhyme></pgtc></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="3.4">h<seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="M">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" caesura="1">ain</seg>s</w><caesura></caesura> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="3.6">p<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="3.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="3.8" punct="pe:12">r<seg phoneme="wa" type="vs" value="1" rule="440" place="11" mp="M">o</seg>y<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="a" stanza="2"><seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="regexp" rhyme="bba">
						<l n="4" num="2.1" lm="12" met="6+6"><w n="4.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="4.2">c</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="4.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.8">s<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>il</w> <w n="4.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="4.10" punct="vg:12"><pgtc id="3" weight="2" schema="[CR">b<rhyme label="b" id="3" gender="m" type="a" stanza="2"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
						<l n="5" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2" punct="vg:2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>s</w>, <w n="5.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="5.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="5.5">tr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.6" punct="vg:8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg><pgtc id="3" weight="2" schema="CR">b<rhyme label="b" id="3" gender="m" type="e" stanza="2"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="2.3" lm="12" met="6+6"><w n="6.1">S</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="6.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="6.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="6.5" punct="vg:9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="6.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="6.7" punct="vg:12">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>t<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="e" stanza="2"><seg phoneme="o" type="vs" value="1" rule="415" place="12">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					</lg>
					<lg n="3" type="regexp" rhyme="aaa">
						<l n="7" num="3.1" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4" mp="M">im</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>lp<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="7.8">v<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="4" weight="2" schema="CR">v<rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</rhyme></pgtc></w></l>
						<l n="8" num="3.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="8.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rts</w> <w n="8.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.6" punct="vg:8">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg><pgtc id="4" weight="2" schema="CR">v<rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
						<l n="9" num="3.3" lm="12" met="6+6"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="9.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.5">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>t</w><caesura></caesura> <w n="9.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="9.9">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="9.10">m<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					</lg>
					<lg n="4" type="regexp" rhyme="bba">
						<l n="10" num="4.1" lm="12" met="6+6"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="10.4" punct="vg:6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="10.5">v<seg phoneme="wa" type="vs" value="1" rule="440" place="7" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="10.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="C">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">om</seg>br<pgtc id="6" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fm">e</seg></pgtc></w>-<w n="10.8"><pgtc id="6" weight="6" schema="V[CR" part="2">l<rhyme label="b" id="6" gender="m" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="342" place="12">à</seg></rhyme></pgtc></w></l>
						<l n="11" num="4.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="11.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">am</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="11.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="11.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="11.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.5" punct="vg:8">c<pgtc id="6" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>l<rhyme label="b" id="6" gender="m" type="e" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></pgtc></w>,</l>
						<l n="12" num="4.3" lm="12" met="6+6"><w n="12.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="12.3" punct="vg:4">ti<seg phoneme="ɛ" type="vs" value="1" rule="366" place="3">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="12.4" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg" caesura="1">an</seg></w>,<caesura></caesura> <w n="12.5">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="12.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="12.8" punct="pt:12">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>c<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="e" stanza="4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>