<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES DÉLIQUESCENCES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA" sort="1">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<author key="VIC" sort="2">
					<name>
						<forename>Gabriel</forename>
						<surname>VICAIRE</surname>
					</name>
					<date from="1848" to="1900">1848-1900</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>				
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>297 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Déliquescences</title>
						<author>Henri Beauclair et Gabriel Vicaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 - 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/deliqu01.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Déliquescences</title>
								<author>Henri Beauclair et Gabriel Vicaire</author>
								<imprint>
									<publisher>Les Editions Henri Jonquières et Cie, Paris</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEV13" modus="cm" lm_max="10" metProfile="5−5" form="suite périodique" schema="4(abba)" er_moy="2.12" er_max="4" er_min="1" er_mode="2(6/8)" er_moy_et="0.78">
				<head type="main">Remords</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>gl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3">sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>ctr<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="1.6" punct="pt:10">G<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg></rhyme></pgtc></w>.</l>
					<l n="2" num="1.2" lm="10" met="5−5" mp5="C"><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="2.3" punct="vg:4">fr<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>fr<seg phoneme="u" type="vs" value="1" rule="426" place="4" punct="vg">ou</seg></w>, <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C" caesura="1">e</seg>s</w><caesura></caesura> <w n="2.5">f<seg phoneme="a" type="vs" value="1" rule="193" place="6">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="2.7" punct="pt:10"><pgtc id="2" weight="2" schema="[CR">v<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="10" met="5−5" mp5="C"><w n="3.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>c</w> <w n="3.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="P">u</seg>r</w> <w n="3.3" punct="vg:3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>c</w>, <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="3.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C" caesura="1">on</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.7" punct="vg:10">l<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg><pgtc id="2" weight="2" schema="CR">v<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1">L</w>’<w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="4.4" punct="vg:4">ch<seg phoneme="œ" type="vs" value="1" rule="249" place="4" punct="vg">œu</seg>r</w>, <w n="4.5" punct="vg:5">d<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg" caesura="1">ou</seg>x</w>,<caesura></caesura> <w n="4.6" punct="vg:10">t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" mp="M">in</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>nn<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="vg">a</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abba">
					<l n="5" num="2.1" lm="10" met="5+5"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M/mp">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w>-<w n="5.2">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="5.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="5.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>c</w> <w n="5.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="5.7" punct="pi:10">s<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg><pgtc id="3" weight="2" schema="CR">nn<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="307" place="10">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg>s</rhyme></pgtc></w> ?</l>
					<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">C<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="6.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="5" caesura="1">oi</seg>r</w><caesura></caesura> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="6.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>t</w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="6.8" punct="vg:10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">er</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="10" met="5+5"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rds</w> <w n="7.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="7.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>t</w><caesura></caesura> <w n="7.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="7.7" punct="pt:10">g<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pt">er</seg></rhyme></pgtc></w>.</l>
					<l n="8" num="2.4" lm="10" met="5+5"><w n="8.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="8.2" punct="pe:2"><seg phoneme="o" type="vs" value="1" rule="444" place="2" punct="pe">o</seg>h</w> ! <w n="8.3" punct="pe:3"><seg phoneme="o" type="vs" value="1" rule="444" place="3" punct="pe">o</seg>h</w> ! <w n="8.4" punct="pe:5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" punct="pe" caesura="1">o</seg>rds</w> !<caesura></caesura> <w n="8.5">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="8.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="8.8" punct="pe:10">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg><pgtc id="3" weight="2" schema="CR">n<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="307" place="10">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe pe pe" mp="F">e</seg>s</rhyme></pgtc></w> !!!</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abba">
					<l n="9" num="3.1" lm="10" met="5−5" mp5="C"><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="9.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="9.4" punct="vg:4">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="9.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C" caesura="1">e</seg></w><caesura></caesura> <w n="9.6">su<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="9.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="9.8" punct="vg:10">m<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>cr<pgtc id="5" weight="4" schema="VR"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg><rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1">J</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="10.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="10.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="10.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" caesura="1">en</seg>t</w><caesura></caesura> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="10.7" punct="vg:10">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>ch<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg><pgtc id="6" weight="2" schema="CR">r<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="10" met="5−5" mp5="C"><w n="11.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg></w> <w n="11.3">R<seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C" caesura="1">e</seg>s</w><caesura></caesura> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">É</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="11.6" punct="vg:10">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="9" mp="M">eu</seg><pgtc id="6" weight="2" schema="CR">r<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="10" met="5+5"><w n="12.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="12.2" punct="pe:3">l<seg phoneme="i" type="vs" value="1" rule="493" place="3" punct="pe">y</seg>s</w> ! <w n="12.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ds</w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5" caesura="1">en</seg></w><caesura></caesura> <w n="12.5">p<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="12.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="12.7" punct="pe:10">N<pgtc id="5" weight="4" schema="VR"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg><rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pe">an</seg>t</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abba">
					<l n="13" num="4.1" lm="10" met="5+5"><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="13.4">hu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="13.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="13.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="13.7">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="13.8">p<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="13.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="13.10" punct="vg:10">C<pgtc id="7" weight="1" schema="GR">i<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="4.2" lm="10" met="5+5"><w n="14.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M/mp">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w>-<w n="14.3">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="4" mp="Fm">e</seg></w> <w n="14.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>c</w><caesura></caesura> <w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="14.6" punct="pi:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg><pgtc id="8" weight="2" schema="CR">nn<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pi">é</seg></rhyme></pgtc></w> ?</l>
					<l n="15" num="4.3" lm="10" met="5+5"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="15.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="15.4" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="343" place="4" mp="M">a</seg>ï<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5" punct="vg" caesura="1">en</seg></w>,<caesura></caesura> <w n="15.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="15.6">su<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>s</w> <w n="15.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="15.8" punct="vg:10">D<seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg><pgtc id="8" weight="2" schema="CR">mn<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="16" num="4.4" lm="10" met="5+5"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="16.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="16.3">t</w>’<w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="16.5" punct="vg:5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="16.6">C<seg phoneme="a" type="vs" value="1" rule="341" place="6" mp="M">a</seg>n<seg phoneme="a" type="vs" value="1" rule="307" place="7">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="16.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="16.8" punct="pe:10">V<pgtc id="7" weight="1" schema="GR">i<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>