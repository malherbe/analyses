<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE SOIR EN FÊTE</head><div type="poem" key="BRS21" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="7(abba)" er_moy="1.14" er_max="2" er_min="0" er_mode="2(8/14)" er_moy_et="0.99">
					<head type="main">TOI</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Comme avec de la charité.
								</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">cr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.4" punct="vg:8">f<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="1" weight="2" schema="CR">n<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>il</w> <w n="2.4">t</w>’<w n="2.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="3.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.4">v<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="3.6">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="7">a</seg>y<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="4.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="4.4">d</w>’<w n="4.5" punct="pt:8"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="1" weight="2" schema="CR">n<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="quatrain" rhyme="abba">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">t</w>’<w n="5.3" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.5">v<seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="5.7">s<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg><pgtc id="3" weight="2" schema="CR">v<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="6.2" punct="vg:3">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="6.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.5" punct="ps:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="4" weight="2" schema="CR">j<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="ps">ou</seg>rs</rhyme></pgtc></w>…</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="7.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.6"><pgtc id="4" weight="2" schema="[CR">j<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">r<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">t</w>’<w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r</w> <w n="8.6" punct="pe:8">tr<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="3" weight="2" schema="CR">v<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abba">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="9.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="9.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="9.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7" punct="vg:8">t<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="10.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="10.4" punct="vg:4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</w>, <w n="10.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.6">t</w>’<w n="10.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="10.8" punct="vg:8">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="6" weight="2" schema="CR">v<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="11.2">m</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345" place="3">ue</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="11.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="11.6"><pgtc id="6" weight="2" schema="CR">v<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="12.3">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="12.6" punct="pe:8">m<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pe">oi</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="abba">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">t</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.4" punct="vg:4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="13.5" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">In</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<pgtc id="7" weight="2" schema="CR">f<rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="14.5" punct="ps:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="ps">eu</seg>x</rhyme></pgtc></w>…</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">qu</w>’<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="15.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="15.5" punct="vg:5">h<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="15.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="15.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="15.8">y<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</rhyme></pgtc></w></l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2">en</seg>s</w> <w n="16.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="16.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="16.8" punct="pe:8"><pgtc id="7" weight="2" schema="CR">f<rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abba">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="17.2" punct="vg:2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="17.3" punct="vg:4">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="17.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="17.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="17.6" punct="vg:8"><pgtc id="9" weight="2" schema="[CR">f<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>x</rhyme></pgtc></w>,</l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="18.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="18.3">m</w>’<w n="18.4" punct="vg:5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>lt<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg">er</seg></w>, <w n="18.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.6" punct="pv:8">m<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>d<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="19.2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="19.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="19.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="5">en</seg>s</w> <w n="19.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="19.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="20.3">p<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="20.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="20.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="20.7" punct="pt:8"><pgtc id="9" weight="2" schema="[CR">f<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="6" type="quatrain" rhyme="abba">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="21.2" punct="vg:3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, <w n="21.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="21.4">gr<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="21.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="22.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="22.5">m</w>’<w n="22.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="22.7" punct="pt:8">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="12" weight="2" schema="CR">t<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="2">en</seg>t</w> <w n="23.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w>-<w n="23.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="23.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="23.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg><pgtc id="12" weight="2" schema="CR">t<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></pgtc></w></l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="24.2">m<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="24.3">pu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>squ</w>’<w n="24.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="24.6" punct="ps:8">b<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></pgtc></w>…</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="abba">
						<l n="25" num="7.1" lm="8" met="8"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="25.2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="25.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="25.4">n</w>’<w n="25.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="25.6">j<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="25.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="13" weight="2" schema="CR">t<rhyme label="a" id="13" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></pgtc></w></l>
						<l n="26" num="7.2" lm="8" met="8"><w n="26.1">Qu</w>’<w n="26.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="26.4">h<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="26.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>dr<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="27" num="7.3" lm="8" met="8"><w n="27.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="27.2" punct="vg:2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>, <w n="27.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="27.4">c</w>’<w n="27.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="27.6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="27.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="27.8">m</w>’<w n="27.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="27.10" punct="vg:8">pr<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="28" num="7.4" lm="8" met="8"><w n="28.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2" punct="vg:2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>, <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="28.4">c</w>’<w n="28.5" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4" punct="vg">e</seg>st</w>, <w n="28.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="28.7" punct="pt:8">v<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg><pgtc id="13" weight="2" schema="CR">t<rhyme label="a" id="13" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>