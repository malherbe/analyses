<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN270" modus="sp" lm_max="8" metProfile="8, 4" form="suite périodique" schema="7(aabccb)" er_moy="1.81" er_max="6" er_min="0" er_mode="2(11/21)" er_moy_et="1.65">
				<head type="number">LXXVIIl</head>
				<head type="main">Grâce !</head>
				<lg n="1" type="sizain" rhyme="aabccb">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">T<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="1.2" punct="vg:3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>s</w>, <w n="1.3">r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="4">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="œ" type="vs" value="1" rule="286" place="7">œ</seg>il</w> <w n="1.7" punct="pe:8"><pgtc id="1" weight="2" schema="[CR">cl<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pe">ai</seg>r</rhyme></pgtc></w> !</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">C</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>p<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="2.7"><pgtc id="1" weight="2" schema="[C[R" part="1">l</pgtc></w>’<w n="2.8"><pgtc id="1" weight="2" schema="[C[R" part="2"><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r</rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">d</w>’<w n="3.3" punct="pv:4"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>gr<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="4.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="4.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="4.5" punct="pt:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="3" weight="2" schema="CR" part="1">p<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="5.2" punct="vg:3">f<seg phoneme="a" type="vs" value="1" rule="193" place="2">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="5.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="5.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="5.6"><pgtc id="3" weight="2" schema="[CR" part="1">p<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l</w> <w n="6.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="6.4" punct="pe:4">f<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="193" place="4">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg>s</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="2" type="sizain" rhyme="aabccb">
					<l n="7" num="2.1" lm="8" met="8"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">È</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6" punct="vg:8">j<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="a" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
					<l n="8" num="2.2" lm="8" met="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="8.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="8.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6">l</w>’<w n="8.7"><seg phoneme="a" type="vs" value="1" rule="341" place="7">A</seg>m<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="a" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</rhyme></pgtc></w></l>
					<l n="9" num="2.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="9.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="9.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg><pgtc id="5" weight="2" schema="CR" part="1">l<rhyme label="b" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg>s</rhyme></pgtc></w></l>
					<l n="10" num="2.4" lm="8" met="8"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="10.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="10.4">fi<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.6" punct="pt:8">tr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="6" weight="2" schema="CR" part="1">s<rhyme label="c" id="6" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></rhyme></pgtc></w>.</l>
					<l n="11" num="2.5" lm="8" met="8"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="11.3">b<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.5">p<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg><pgtc id="6" weight="2" schema="CR" part="1">s<rhyme label="c" id="6" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></rhyme></pgtc></w></l>
					<l n="12" num="2.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="12.2" punct="pe:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><pgtc id="5" weight="2" schema="CR" part="1">l<rhyme label="b" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg>s</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="3" type="sizain" rhyme="aabccb">
					<l n="13" num="3.1" lm="8" met="8"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="13.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3" punct="vg:5">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>sc<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>rs</w>, <w n="13.4"><seg phoneme="u" type="vs" value="1" rule="426" place="6">où</seg></w> <w n="13.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="13.6" punct="vg:8"><pgtc id="7" weight="3" schema="[CGR" part="1">nu<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>t</rhyme></pgtc></w>,</l>
					<l n="14" num="3.2" lm="8" met="8"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2" punct="vg:3">br<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.6" punct="vg:8"><pgtc id="7" weight="3" schema="[CGR" part="1">nu<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>t</rhyme></pgtc></w>,</l>
					<l n="15" num="3.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="15.3">s<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="15.4"><pgtc id="8" weight="2" schema="[CR" part="1">bl<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="3.4" lm="8" met="8"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="16.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="16.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>c<pgtc id="9" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<rhyme label="c" id="9" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></pgtc></w></l>
					<l n="17" num="3.5" lm="8" met="8"><w n="17.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="17.2">f<seg phoneme="ɥi" type="vs" value="1" rule="462" place="2">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="17.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.4">l<pgtc id="9" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></pgtc></w> <w n="17.5"><pgtc id="9" weight="6" schema="V[CR" part="2">v<rhyme label="c" id="9" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</rhyme></pgtc></w></l>
					<l n="18" num="3.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3"><pgtc id="8" weight="2" schema="[C[R" part="1">l</pgtc></w>’<w n="18.4" punct="pt:4"><pgtc id="8" weight="2" schema="[C[R" part="2"><rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="sizain" rhyme="aabccb">
					<l n="19" num="4.1" lm="8" met="8"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="19.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="19.4">d</w>’<w n="19.5" punct="vg:8"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg><pgtc id="10" weight="2" schema="CR" part="1">s<rhyme label="a" id="10" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
					<l n="20" num="4.2" lm="8" met="8"><w n="20.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="20.2">c<seg phoneme="ɛ" type="vs" value="1" rule="352" place="2">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="20.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="20.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="20.5" punct="vg:7">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" punct="vg">e</seg><pgtc id="10" weight="2" schema="C[R" part="1">s</pgtc></w>, <w n="20.6"><pgtc id="10" weight="2" schema="C[R" part="2"><rhyme label="a" id="10" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg></rhyme></pgtc></w></l>
					<l n="21" num="4.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="21.1">Cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r</w> <w n="21.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="21.4" punct="dp:4">l<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="b" id="11" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="dp">e</seg></rhyme></pgtc></w> :</l>
					<l n="22" num="4.4" lm="8" met="8"><w n="22.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="22.2">f<seg phoneme="a" type="vs" value="1" rule="193" place="2">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="22.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="22.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="22.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="22.7" punct="pv:8">b<pgtc id="12" weight="1" schema="GR" part="1">i<rhyme label="c" id="12" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" punct="pv">en</seg></rhyme></pgtc></w> ;</l>
					<l n="23" num="4.5" lm="8" met="8"><w n="23.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="23.3">qu</w>’<w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="23.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="23.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>nt</w> <w n="23.7" punct="vg:8">r<pgtc id="12" weight="1" schema="GR" part="1">i<rhyme label="c" id="12" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="vg">en</seg></rhyme></pgtc></w>,</l>
					<l n="24" num="4.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="24.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="24.2">j</w>’<w n="24.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="24.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="24.5" punct="pe:4"><pgtc id="11" weight="0" schema="[R" part="1"><rhyme label="b" id="11" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="5" type="sizain" rhyme="aabccb">
					<l n="25" num="5.1" lm="8" met="8"><w n="25.1">C</w>’<w n="25.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="25.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="25.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="25.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="25.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="25.7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="7">œu</seg>r</w> <w n="25.8" punct="pv:8"><pgtc id="13" weight="2" schema="[CR" part="1">v<rhyme label="a" id="13" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg>t</rhyme></pgtc></w> ;</l>
					<l n="26" num="5.2" lm="8" met="8"><w n="26.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="26.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="26.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="26.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="26.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="26.6" punct="vg:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="13" weight="2" schema="CR" part="1">v<rhyme label="a" id="13" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>t</rhyme></pgtc></w>,</l>
					<l n="27" num="5.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="27.1" punct="vg:1">L<seg phoneme="i" type="vs" value="1" rule="493" place="1" punct="vg">y</seg>s</w>, <w n="27.2">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="27.4" punct="vg:4">r<pgtc id="14" weight="0" schema="R" part="1"><rhyme label="b" id="14" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="28" num="5.4" lm="8" met="8"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="28.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="28.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="28.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rv<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="28.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="28.6" punct="pt:8">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="15" weight="2" schema="CR" part="1">n<rhyme label="c" id="15" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>x</rhyme></pgtc></w>.</l>
					<l n="29" num="5.5" lm="8" met="8"><w n="29.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="29.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="29.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="29.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="29.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="29.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="29.7"><pgtc id="15" weight="2" schema="[CR" part="1">n<rhyme label="c" id="15" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</rhyme></pgtc></w></l>
					<l n="30" num="5.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="30.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t</w> <w n="30.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="30.3" punct="pt:4">ch<pgtc id="14" weight="0" schema="R" part="1"><rhyme label="b" id="14" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="6" type="sizain" rhyme="aabccb">
					<l n="31" num="6.1" lm="8" met="8"><w n="31.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="31.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="31.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="31.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="31.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="31.6" punct="pv:8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>s<pgtc id="16" weight="2" schema="CR" part="1">t<rhyme label="a" id="16" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pv">in</seg>s</rhyme></pgtc></w> ;</l>
					<l n="32" num="6.2" lm="8" met="8"><w n="32.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="32.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="32.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="32.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="32.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="32.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="16" weight="2" schema="CR" part="1">t<rhyme label="a" id="16" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>s</rhyme></pgtc></w></l>
					<l n="33" num="6.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="33.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="33.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="33.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="33.4" punct="vg:4"><pgtc id="17" weight="2" schema="[CR" part="1">m<rhyme label="b" id="17" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="34" num="6.4" lm="8" met="8"><w n="34.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="34.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="34.3">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="34.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="34.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>bs<pgtc id="18" weight="0" schema="R" part="1"><rhyme label="c" id="18" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>t</rhyme></pgtc></w>,</l>
					<l n="35" num="6.5" lm="8" met="8"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="35.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="35.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="35.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="35.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="35.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="35.7">t<pgtc id="18" weight="0" schema="R" part="1"><rhyme label="c" id="18" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</rhyme></pgtc></w></l>
					<l n="36" num="6.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="36.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="36.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="36.3" punct="pt:4">M<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><pgtc id="17" weight="2" schema="CR" part="1">m<rhyme label="b" id="17" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="7" type="sizain" rhyme="aabccb">
					<l n="37" num="7.1" lm="8" met="8"><w n="37.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="37.2">l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="37.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="37.4">j<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="37.5" punct="pt:8">b<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<pgtc id="19" weight="6" schema="VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>t<rhyme label="a" id="19" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg>s</rhyme></pgtc></w>.</l>
					<l n="38" num="7.2" lm="8" met="8"><w n="38.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ct<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="38.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="38.3">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="38.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="38.5">b<pgtc id="19" weight="6" schema="VCR" part="1"><seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>t<rhyme label="a" id="19" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</rhyme></pgtc></w></l>
					<l n="39" num="7.3" lm="4" met="4"><space quantity="8" unit="char"></space><w n="39.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="39.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="39.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="39.4" punct="vg:4">v<pgtc id="20" weight="0" schema="R" part="1"><rhyme label="b" id="20" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="40" num="7.4" lm="8" met="8"><w n="40.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="40.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="40.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="40.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<seg phoneme="u" type="vs" value="1" rule="425" place="6">oû</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="40.5"><pgtc id="21" weight="2" schema="[CR" part="1">p<rhyme label="c" id="21" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></pgtc></w></l>
					<l n="41" num="7.5" lm="8" met="8"><w n="41.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="41.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="41.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="41.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="41.5">fi<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="41.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="21" weight="2" schema="CR" part="1">pp<rhyme label="c" id="21" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
					<l n="42" num="7.6" lm="4" met="4"><space quantity="8" unit="char"></space><w n="42.1">C</w>’<w n="42.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="42.3" punct="vg:2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="42.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="42.5" punct="pe:4"><pgtc id="20" weight="0" schema="[R" part="1"><rhyme label="b" id="20" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg>s</rhyme></pgtc></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">27 février 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>