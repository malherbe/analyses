<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ROSES DE NOËL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>600 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_15</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ROSES DE NOËL</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1878">1878</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN613" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="3(abab)" er_moy="4.0" er_max="6" er_min="2" er_mode="2(3/6)" er_moy_et="2.0">
				<head type="main">Ta Voix</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="1.4" punct="vg:4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>x</w>, <w n="1.5">j<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="1.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="1.8">m</w>’<w n="1.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="1.10" punct="pt:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">s<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="2.2" punct="vg:3">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="2.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="2.4" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>rd</w>,<caesura></caesura> <w n="2.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="2.6">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</w> <w n="2.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="2.8">l</w>’<w n="2.9"><seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">O</seg><pgtc id="2" weight="6" schema="CVR">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t</rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="3.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="3.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="3.4">f<seg phoneme="œ" type="vs" value="1" rule="304" place="5" mp="M">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="3.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7" mp="M">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="3.7" punct="vg:12">p<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="1" weight="2" schema="CR">s<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="4.3">m</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="Lc">en</seg>tr</w>’<w n="4.6"><seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M/mc">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rt</w><caesura></caesura> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="4.8" punct="vg:8">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>, <w n="4.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="4.10" punct="pe:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg><pgtc id="2" weight="6" schema="CVR">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pe">an</seg>t</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="5.3">f<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>t</w> <w n="5.4">m</w>’<w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345" place="6" caesura="1">ue</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="5.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="5.8">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="5.9" punct="vg:12">h<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">l</w>’<w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="6.5" punct="pv:4">d<seg phoneme="y" type="vs" value="1" rule="445" place="4" punct="pv">û</seg></w> ; <w n="6.6">c</w>’<w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="6.8" punct="vg:6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="6.9" punct="vg:8">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg" mp="F">e</seg></w>, <w n="6.10">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="6.11">m</w>’<w n="6.12"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="6.13"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="4" weight="2" schema="CR">ppr<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s</rhyme></pgtc></w></l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="7.2">m</w>’<w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="2" mp="M">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="7.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.6">rh<seg phoneme="i" type="vs" value="1" rule="493" place="7" mp="M">y</seg>thm<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="7.8" punct="vg:12">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>g<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="8.4">n<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="8.7">c<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="8.8" punct="pe:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="4" weight="2" schema="CR">pr<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pe">i</seg>s</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="9.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>x</w> <w n="9.5">m<seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>ts</w><caesura></caesura> <w n="9.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t</w> <w n="9.9" punct="pv:12">s<pgtc id="5" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="10.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="10.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="10.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="10.5">p<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>f</w> <w n="10.7">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>s</w> <w n="10.8">d<pgtc id="6" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></pgtc></w> <w n="10.9" punct="vg:12"><pgtc id="6" weight="6" schema="V[CR" part="2">n<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>s</rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="11.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="11.5">m</w>’<w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="11.7">d<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="11.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="11.9"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.10"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="11.11">t<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="11.12" punct="pe:12">br<pgtc id="5" weight="6" schema="VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">Em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="12.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="12.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>ls</w><caesura></caesura> <w n="12.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="12.6">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="12.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="12.9" punct="pt:12">g<pgtc id="6" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>x</rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1856"> 19 novembre 1856.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>