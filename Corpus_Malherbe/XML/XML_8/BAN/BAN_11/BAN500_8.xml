<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN500" modus="sm" lm_max="8" metProfile="8" form="petite ballade" schema="3(ababbcbc) bcbc" er_moy="1.57" er_max="6" er_min="0" er_mode="2(8/14)" er_moy_et="1.55">
					<head type="number">XXVI</head>
					<head type="main">Ballade sur lui-même</head>
					<lg n="1" type="huitain" rhyme="ababbcbc">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.3" punct="vg:6">r<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" punct="vg">e</seg>s</w>, <w n="1.4" punct="vg:8">B<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg><pgtc id="1" weight="2" schema="CR">v<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">C</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="2.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="2.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="2" weight="2" schema="CR">r<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>ts</rhyme></pgtc></w></l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="3.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="3.4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6" punct="pv:8">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="1" weight="2" schema="CR">v<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w>-<w n="4.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="4.4">ch<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.6">T<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>rc<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="2" weight="2" schema="CR">r<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>ts</rhyme></pgtc></w></l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="5.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="5.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="5.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="5.6" punct="pi:8">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ffr<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pi">e</seg>ts</rhyme></pgtc></w> ?</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1">Pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="6.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="6.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="6.5" punct="pe:8">f<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg><pgtc id="4" weight="2" schema="CR">r<rhyme label="c" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">C</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="7.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="7.4" punct="vg:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>t</w>, <w n="7.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="7.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.8" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>s</rhyme></pgtc></w>,</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="8.4">p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="5">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5" punct="pt:8">l<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg><pgtc id="4" weight="2" schema="CR">r<rhyme label="c" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="huitain" rhyme="ababbcbc">
						<l n="9" num="2.1" lm="8" met="8"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="9.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.6"><pgtc id="5" weight="2" schema="CR">v<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="10" num="2.2" lm="8" met="8"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="10.3">fl<seg phoneme="y" type="vs" value="1" rule="445" place="4">û</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.5" punct="pv:8">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>gr<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pv">ai</seg>s</rhyme></pgtc></w> ;</l>
						<l n="11" num="2.3" lm="8" met="8"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="354" place="2">e</seg>x<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="11.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt</w> <w n="11.5" punct="vg:8">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="5" weight="2" schema="CR">v<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="12" num="2.4" lm="8" met="8"><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="2">en</seg>t</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>ri<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w>-<w n="12.4" punct="vg:6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>s</w>, <w n="12.5" punct="pe:8">p<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>vr<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pe">e</seg>ts</rhyme></pgtc></w> !</l>
						<l n="13" num="2.5" lm="8" met="8"><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4" punct="vg:5">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="vg">ai</seg>s</w>, <w n="13.5">j</w>’<w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>m<pgtc id="7" weight="6" schema="VCR"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="7" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</rhyme></pgtc></w></l>
						<l n="14" num="2.6" lm="8" met="8"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2">t<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="14.3">cu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>r</w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="14.5">l</w>’<w n="14.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">A</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="8" weight="2" schema="CR">r<rhyme label="c" id="8" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="15" num="2.7" lm="8" met="8"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="3">oi</seg></w> <w n="15.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="15.6" punct="pi:8">r<pgtc id="7" weight="6" schema="VCR"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>gr<rhyme label="b" id="7" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pi">e</seg>ts</rhyme></pgtc></w> ?</l>
						<l n="16" num="2.8" lm="8" met="8"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="16.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="16.4">p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="5">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="16.5" punct="pt:8">l<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg><pgtc id="8" weight="2" schema="CR">r<rhyme label="c" id="8" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="huitain" rhyme="ababbcbc">
						<l n="17" num="3.1" lm="8" met="8"><w n="17.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="17.4">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="17.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="17.6" punct="pt:8">c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="9" weight="2" schema="CR">v<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="18" num="3.2" lm="8" met="8"><w n="18.1" punct="po:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> (<w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="18.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.4" punct="pf:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rr<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>s</rhyme></pgtc></w>,)</l>
						<l n="19" num="3.3" lm="8" met="8"><w n="19.1">F<seg phoneme="ɥi" type="vs" value="1" rule="462" place="1">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="19.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.3">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="19.4" punct="vg:8"><pgtc id="9" weight="2" schema="[CR">v<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="20" num="3.4" lm="8" met="8"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="20.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>s</w> <w n="20.5" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>tr<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>ts</rhyme></pgtc></w>,</l>
						<l n="21" num="3.5" lm="8" met="8"><w n="21.1" punct="vg:2">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="21.2">f<seg phoneme="ɥi" type="vs" value="1" rule="462" place="3">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="21.3">l</w>’<w n="21.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">im</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="21.5" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="11" weight="2" schema="CR">r<rhyme label="b" id="11" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>s</rhyme></pgtc></w>,</l>
						<l n="22" num="3.6" lm="8" met="8"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="22.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="22.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="22.4">n<seg phoneme="y" type="vs" value="1" rule="457" place="5">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="22.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ctr<pgtc id="12" weight="0" schema="R"><rhyme label="c" id="12" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="23" num="3.7" lm="8" met="8"><w n="23.1">L</w>’<w n="23.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="23.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="23.5" punct="pv:8">f<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="11" weight="2" schema="CR">r<rhyme label="b" id="11" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8" punct="pv">ê</seg>ts</rhyme></pgtc></w> ;</l>
						<l n="24" num="3.8" lm="8" met="8"><w n="24.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="24.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="24.4">p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="5">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="24.5" punct="pt:8">l<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>r<pgtc id="12" weight="0" schema="R"><rhyme label="c" id="12" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="bcbc">
						<head type="form">Envoi</head>
						<l n="25" num="4.1" lm="8" met="8"><w n="25.1" punct="vg:2">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="25.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="25.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="25.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="25.5" punct="vg:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>cr<pgtc id="13" weight="0" schema="R"><rhyme label="b" id="13" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="vg">e</seg>ts</rhyme></pgtc></w>,</l>
						<l n="26" num="4.2" lm="8" met="8"><w n="26.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="26.3">m</w>’<w n="26.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>ds</w> <w n="26.5">qu</w>’<w n="26.6"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="26.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="26.8" punct="dp:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="14" weight="2" schema="CR">tr<rhyme label="c" id="14" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="27" num="4.3" lm="8" met="8"><w n="27.1">F<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ls</w> <w n="27.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="27.3">di<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="27.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="27.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="27.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="27.7" punct="vg:8">tr<pgtc id="13" weight="0" schema="R"><rhyme label="b" id="13" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>ts</rhyme></pgtc></w>,</l>
						<l n="28" num="4.4" lm="8" met="8"><w n="28.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="28.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="28.4">p<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="5">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="28.5" punct="pt:8">l<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg><pgtc id="14" weight="2" schema="CR">r<rhyme label="c" id="14" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1869">Juillet 1869.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>