<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN508" modus="sm" lm_max="8" metProfile="8" form="ballade non classique" schema="3(ababbcbc) bcbc" er_moy="2.29" er_max="6" er_min="0" er_mode="0(5/14)" er_moy_et="2.12">
					<head type="number">XXXIV</head>
					<head type="main">Ballade de la joyeuse chanson du cor</head>
					<lg n="1" type="huitain" rhyme="ababbcbc">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="1.2">qu</w>’<w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.5">t<pgtc id="1" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>x</w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="2.5" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<pgtc id="2" weight="4" schema="VR"><seg phoneme="i" type="vs" value="1" rule="dc-1" place="7">i</seg><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="vg">en</seg>s</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r</w> <w n="3.3" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" punct="vg">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="3.5">r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<pgtc id="1" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="4.3">c<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="4.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="343" place="5">a</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<pgtc id="2" weight="4" schema="VR"><seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="vg">en</seg>s</rhyme></pgtc></w>,</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="5.2">ch<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="5.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>ts</w> <w n="5.5" punct="pt:8">m<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="i" type="vs" value="1" rule="dc-1" place="7">i</seg><pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pt">en</seg>s</rhyme></pgtc></w>.</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1" punct="vg:2">S<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="6.2">p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="6.3">g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s</w> <w n="6.4">d</w>’<w n="6.5" punct="pe:8"><pgtc id="4" weight="0" schema="[R"><rhyme label="c" id="4" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pe">o</seg>r</rhyme></pgtc></w> !</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="wa" type="vs" value="1" rule="422" place="5">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ts</w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.5">chi<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg>s</rhyme></pgtc></w></l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="8.4">j<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="8.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="8.8" punct="pe:8">c<pgtc id="4" weight="0" schema="R"><rhyme label="c" id="4" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pe">o</seg>r</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="huitain" rhyme="ababbcbc">
						<l n="9" num="2.1" lm="8" met="8"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r</w> <w n="9.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="9.5" punct="vg:8">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="6">a</seg>y<pgtc id="5" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
						<l n="10" num="2.2" lm="8" met="8"><w n="10.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="10.2">d</w>’<w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="2">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="10.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.7">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg>s</rhyme></pgtc></w></l>
						<l n="11" num="2.3" lm="8" met="8"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="11.2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="11.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="11.5">fr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ss<pgtc id="5" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></pgtc></w></l>
						<l n="12" num="2.4" lm="8" met="8"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">n</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="12.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.6">l<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ps</w> <w n="12.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="12.8" punct="pe:8">g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rdi<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pe">en</seg>s</rhyme></pgtc></w> !</l>
						<l n="13" num="2.5" lm="8" met="8"><w n="13.1" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="13.2">cr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="13.3" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="6">ym</seg>p<pgtc id="7" weight="4" schema="VR"><seg phoneme="i" type="vs" value="1" rule="dc-1" place="7">i</seg><rhyme label="b" id="7" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="vg">en</seg>s</rhyme></pgtc></w>,</l>
						<l n="14" num="2.6" lm="8" met="8"><w n="14.1" punct="pe:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2" punct="pe">o</seg>r</w> ! <w n="14.2" punct="pe:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="pe">o</seg>r</w> ! <w n="14.3" punct="pe:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="pe">o</seg>r</w> ! <w n="14.4" punct="pe:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">En</seg><pgtc id="8" weight="2" schema="CR">c<rhyme label="c" id="8" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pe">o</seg>r</rhyme></pgtc></w> !</l>
						<l n="15" num="2.7" lm="8" met="8"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="15.2" punct="vg:3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>rs</w>, <w n="15.3">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>cs</w> <w n="15.4" punct="vg:8">b<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>h<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<pgtc id="7" weight="4" schema="VR"><seg phoneme="i" type="vs" value="1" rule="dc-1" place="7">i</seg><rhyme label="b" id="7" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="vg">en</seg>s</rhyme></pgtc></w>,</l>
						<l n="16" num="2.8" lm="8" met="8"><w n="16.1">Qu</w>’<w n="16.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="16.4">j<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="16.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="16.8" punct="pe:8"><pgtc id="8" weight="2" schema="[CR">c<rhyme label="c" id="8" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pe">o</seg>r</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" type="huitain" rhyme="ababbcbc">
						<l n="17" num="3.1" lm="8" met="8"><w n="17.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="17.3" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="17.5" punct="vg:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<pgtc id="9" weight="2" schema="CR">n<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
						<l n="18" num="3.2" lm="8" met="8"><w n="18.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="18.2">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="18.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>ts</w> <w n="18.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ci<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="vg">en</seg>s</rhyme></pgtc></w>,</l>
						<l n="19" num="3.3" lm="8" met="8"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">l</w>’<w n="19.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="19.4">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="19.5"><seg phoneme="i" type="vs" value="1" rule="497" place="4">y</seg></w> <w n="19.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="19.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="9" weight="2" schema="CR">n<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</rhyme></pgtc></w></l>
						<l n="20" num="3.4" lm="8" met="8"><w n="20.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="20.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="20.3">r<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="20.4" punct="pt:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>d<seg phoneme="i" type="vs" value="1" rule="dc-1" place="7">i</seg><pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pt">en</seg>s</rhyme></pgtc></w>.</l>
						<l n="21" num="3.5" lm="8" met="8"><w n="21.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="21.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>gl<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="21.3" punct="vg:6">g<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>, <w n="21.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="21.5">vi<pgtc id="11" weight="0" schema="R"><rhyme label="b" id="11" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="8">en</seg>s</rhyme></pgtc></w></l>
						<l n="22" num="3.6" lm="8" met="8"><w n="22.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="22.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="22.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="22.4">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="22.5" punct="dp:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="12" weight="2" schema="CR">c<rhyme label="c" id="12" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="dp">o</seg>r</rhyme></pgtc></w> :</l>
						<l n="23" num="3.7" lm="8" met="8"><w n="23.1" punct="pe:2">H<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>h</w> ! <w n="23.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="23.3">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg>s</w> <w n="23.4" punct="pe:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="11" weight="0" schema="R"><rhyme label="b" id="11" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pe">en</seg>s</rhyme></pgtc></w> !</l>
						<l n="24" num="3.8" lm="8" met="8"><w n="24.1">Qu</w>’<w n="24.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="24.4">j<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="24.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="24.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="24.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="24.8" punct="pe:8"><pgtc id="12" weight="2" schema="[CR">c<rhyme label="c" id="12" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pe">o</seg>r</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="4" type="quatrain" rhyme="bcbc">
						<head type="form">Envoi</head>
						<l n="25" num="4.1" lm="8" met="8"><w n="25.1" punct="vg:2">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="25.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="25.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="25.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>d<pgtc id="13" weight="4" schema="VR"><seg phoneme="i" type="vs" value="1" rule="dc-1" place="7">i</seg><rhyme label="b" id="13" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg>s</rhyme></pgtc></w></l>
						<l n="26" num="4.2" lm="8" met="8"><w n="26.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="26.3">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg>s</w> <w n="26.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="26.5">r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg><pgtc id="14" weight="2" schema="CR">ss<rhyme label="c" id="14" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>r</rhyme></pgtc></w>,</l>
						<l n="27" num="4.3" lm="8" met="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="27.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="27.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>ts</w> <w n="27.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<pgtc id="13" weight="4" schema="VR"><seg phoneme="i" type="vs" value="1" rule="dc-1" place="7">i</seg><rhyme label="b" id="13" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg>s</rhyme></pgtc></w></l>
						<l n="28" num="4.4" lm="8" met="8"><w n="28.1">Qu</w>’<w n="28.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="28.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="28.4">j<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="28.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="28.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="28.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="28.8" punct="pe:8"><pgtc id="14" weight="2" schema="[CR">c<rhyme label="c" id="14" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pe">o</seg>r</rhyme></pgtc></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1869">Octobre 1869.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>