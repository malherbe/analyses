<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">RIMES DORÉES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1492 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_14</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">RIMES DORÉES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN589" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique, prototype 2" schema="abba abba ccd ede" er_moy="1.43" er_max="2" er_min="0" er_mode="2(4/7)" er_moy_et="0.73">
				<head type="main">A Alphonse Lemerre</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="1.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="1.4">d</w>’<w n="1.5">h<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="1.7" punct="vg:10">p<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="9">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="vg" mp="F">e</seg>s</w>, <w n="1.8" punct="vg:12">L<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="1" weight="2" schema="CR">m<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="2.3">M<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg>x</w> <w n="2.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="2.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="2.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" mp="P">e</seg>rs</w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="2.9">cl<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="2.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="2.11" punct="vg:12">s<pgtc id="2" weight="1" schema="GR">u<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="vg">i</seg>t</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="3.2">qu</w>’<w n="3.3"><seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="3.4">l</w>’<w n="3.5">h<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">em</seg>ps</w> <w n="3.9">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="3.10">s</w>’<w n="3.11" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>f<pgtc id="2" weight="1" schema="GR">u<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="vg">i</seg>t</rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="4.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="4.3">H<seg phoneme="y" type="vs" value="1" rule="453" place="3" mp="M">u</seg>m<seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="4.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="Fc">e</seg></w> <w n="4.6" punct="pt:12">Ch<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg><pgtc id="1" weight="2" schema="CR">m<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">m<seg phoneme="y" type="vs" value="1" rule="d-3" place="3" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="5.4"><seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">Ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="5.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="5.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d</w> <w n="5.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="5.8">c<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1" mp="C">eu</seg>r</w> <w n="6.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>x</w> <w n="6.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="6.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rs<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.6">n</w>’<w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="6.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="6.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="6.10">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="11">ain</seg></w> <w n="6.11" punct="pv:12">br<pgtc id="4" weight="1" schema="GR">u<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pv">i</seg>t</rhyme></pgtc></w> ;</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="7.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="7.4">qu</w>’<w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="7.6">sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="7.9">n<pgtc id="4" weight="1" schema="GR">u<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="12">i</seg>t</rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">S</w>’<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="8.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="8.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="8.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="8.9">d</w>’<w n="8.10" punct="pt:12">H<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="9.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="9.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.6">fr<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="9.7" punct="vg:12">r<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg><pgtc id="5" weight="2" schema="CR">m<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>rs</rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="10.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="10.3">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3" mp="M">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="10.5">ch<seg phoneme="e" type="vs" value="1" rule="347" place="7" mp="P">ez</seg></w> <w n="10.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg>s</w> <w n="10.8">r<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg><pgtc id="5" weight="2" schema="CR">m<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>rs</rhyme></pgtc></w></l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="11.3">m<seg phoneme="i" type="vs" value="1" rule="493" place="3" mp="M">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="11.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>rs</w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="11.8" punct="pv:12">M<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="6" weight="2" schema="CR">tr<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="4" rhyme="ede">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="12.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="12.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="3">om</seg></w> <w n="12.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="12.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="12.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt</w> <w n="12.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="12.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="12.9" punct="vg:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>s<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>rd</rhyme></pgtc></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="13.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="13.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="13.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="13.7">M<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.8">l<seg phoneme="i" type="vs" value="1" rule="493" place="11" mp="M">y</seg><pgtc id="6" weight="2" schema="CR">r<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="14.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="14.5">l<seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="14.7" punct="pt:12">R<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>s<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>rd</rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1875">Mercredi 31 mars 1875.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>