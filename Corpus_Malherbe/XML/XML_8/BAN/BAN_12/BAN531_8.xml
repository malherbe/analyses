<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PRINCESSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>294 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_12</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Princesses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvillelesprincesses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Princesses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1994">1994</date>
								</imprint>
								<biblScope unit="tome">IV</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1891">1889-1891</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VII</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN531" modus="cm" lm_max="12" metProfile="6−6" form="sonnet classique, prototype 2" schema="abba abba ccd ede" er_moy="1.14" er_max="2" er_min="0" er_mode="2(4/7)" er_moy_et="0.99">
				<head type="number">XIX</head>
				<head type="main">Madame Tallien</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								… la Dona Theresia, que Bordeaux a vue passer, <lb></lb>
								dans la stupeur, debout sur un char, le bonnet rouge <lb></lb>
								sur la tête, une pique à la main, un bras sur <lb></lb>
								l’épaule du maître, la Tallien se montre un soir, la <lb></lb>
								gorge enserrée dans une rivière de diamants… <lb></lb>
							,
							</quote>
							<bibl>
								<name>Edmond et Jules de Goncourt</name>, <lb></lb>
								<hi rend="ital">Histoire de la Société française pendant le Directoire</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="1.2" punct="vg:6">Th<seg phoneme="e" type="vs" value="1" rule="BAN531_1" place="3" mp="M">e</seg>r<seg phoneme="e" type="vs" value="1" rule="BAN531_1" place="4" mp="M">e</seg>s<seg phoneme="i" type="vs" value="1" rule="dc-1" place="5" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg></w>,<caesura></caesura> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="1.5">r<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="1.8">g<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="2.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="2.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="2.4"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="2.5">p<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="2.8"><pgtc id="2" weight="2" schema="[CR">m<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12">ain</seg></rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>c</w><caesura></caesura> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="3.5">pr<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>l</w> <w n="3.6">r<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="2" weight="2" schema="CR">m<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12">ain</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="4.3" punct="vg:3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>r</w>, <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="4.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="4.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rts</w> <w n="4.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="4.8">b<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="190" place="11">e</seg>t</w> <w n="4.9" punct="vg:12">r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="5.3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="5.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="5.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="5.7" punct="vg:12"><pgtc id="3" weight="2" schema="[CR">b<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="6.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="5">e</seg>ds</w> <w n="6.5">n<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="6.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="6.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="6.9" punct="pv:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pv">in</seg></rhyme></pgtc></w> ;</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="7.2" punct="vg:3">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="7.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="7.4">fr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="7.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="7.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rps</w> <w n="7.8" punct="vg:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>rh<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12" punct="vg">ain</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="8.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="8.4">t<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="8.5">d</w>’<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>r</w><caesura></caesura> <w n="8.7" punct="vg:8">tr<seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>, <w n="8.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="8.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="8.10">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11">en</seg>t</w> <w n="8.11" punct="pt:12"><pgtc id="3" weight="2" schema="[CR">b<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" rhyme="ccd">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="9.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="9.3" punct="vg:3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3" punct="vg">ein</seg>s</w>, <w n="9.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>c</w><caesura></caesura> <w n="9.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="9" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="10" mp="M">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="5" weight="2" schema="CR">m<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>ts</rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="12" mp6="C" met="6−6"><w n="10.1">D</w>’<w n="10.2" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="vg" mp="F">e</seg>s</w>, <w n="10.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="10.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="10.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="10.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C" caesura="1">un</seg></w><caesura></caesura> <w n="10.7">c<seg phoneme="o" type="vs" value="1" rule="435" place="7" mp="M">o</seg>lli<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="10.9">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg><pgtc id="5" weight="2" schema="CR">m<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ts</rhyme></pgtc></w></l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="11.3">br<seg phoneme="y" type="vs" value="1" rule="445" place="3">û</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.4">d</w>’<w n="11.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="11.6">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>r</w><caesura></caesura> <w n="11.7">sc<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10">en</seg>t</w> <w n="11.8">d</w>’<w n="11.9" punct="pv:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>s</rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="4" rhyme="ede">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2" punct="vg:5">v<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>ct<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg">eu</seg>x</w>, <w n="12.3">fi<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>rs</w><caesura></caesura> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="12.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="C">eu</seg>rs</w> <w n="12.6">b<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w> <w n="12.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rm<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ils</rhyme></pgtc></w></l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="13.2">l<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="13.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="13.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="13.7">l<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="11">er</seg>s</w> <w n="13.8" punct="vg:12">v<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="14.3">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>c</w><caesura></caesura> <w n="14.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="14.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rc<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9">an</seg></w> <w n="14.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="14.8" punct="pt:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="pt">e</seg>ils</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>