<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">AMÉTHYSTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>319 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">AMÉTHYSTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleamethystes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1860-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN191" modus="cp" lm_max="10" metProfile="5, 5+5" form="" schema="" er_moy="1.43" er_max="2" er_min="0" er_mode="2(10/14)" er_moy_et="0.9">
				<head type="main">Vers sapphiques</head>
				<lg n="1" rhyme="None">
					<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="1.2" punct="vg:2">f<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="1.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="1.4" punct="vg:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="5" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="1.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="1.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ts</w> <w n="1.7">fi<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>rs</w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.9" punct="vg:10">d<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="vg">ou</seg>x</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">t</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="2.5" punct="vg:5">d<seg phoneme="o" type="vs" value="1" rule="435" place="4" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="2.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="Lc">u</seg>squ</w>’<w n="2.7"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="2.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="2.9" punct="pt:10">c<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pt">ou</seg>x</rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">n</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="3.5" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg" caesura="1">ez</seg></w>,<caesura></caesura> <w n="3.6">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="3.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="3.8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>r</w> <w n="3.9" punct="pt:10">j<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>l<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pt">ou</seg>x</rhyme></pgtc></w>.</l>
					<l n="4" num="1.4" lm="5" met="5"><space quantity="12" unit="char"></space><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="4.4" punct="pe:5">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg><pgtc id="2" weight="2" schema="CR">s<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="pe">on</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="2" rhyme="None">
					<l n="5" num="2.1" lm="10" met="5+5"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="5.4">b<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>r</w><caesura></caesura> <w n="5.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="5.6">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="5.7" punct="vg:10">t<seg phoneme="wa" type="vs" value="1" rule="420" place="9" mp="M">oi</seg><pgtc id="2" weight="2" schema="CR">s<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">on</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="6.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="6.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg>s</w><caesura></caesura> <w n="6.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="P">ou</seg>r</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="6.7" punct="vg:10">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg><pgtc id="3" weight="2" schema="CR">s<rhyme label="b" id="3" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">on</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="10" met="5+5"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="7.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg></w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="7.5" punct="vg:5">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="5" punct="vg" caesura="1">ei</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.8">n<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</w> <w n="7.9">p<seg phoneme="wa" type="vs" value="1" rule="420" place="9" mp="M">oi</seg><pgtc id="3" weight="2" schema="CR">s<rhyme label="b" id="3" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="5" met="5"><space quantity="12" unit="char"></space><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="8.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.5" punct="pe:5"><pgtc id="4" weight="2" schema="[CR">m<rhyme label="c" id="4" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5" punct="pe">ain</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="3" rhyme="None">
					<l n="9" num="3.1" lm="10" met="5+5"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="9.3" punct="pe:5">b<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5" punct="pe" caesura="1">ai</seg></w> !<caesura></caesura> <w n="9.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>r</w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.6" punct="vg:10"><seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>nh<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="c" id="4" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10" punct="vg">ain</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1">Fl<seg phoneme="ø" type="vs" value="1" rule="405" place="1" mp="M">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="10.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg></w> <w n="10.5">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="7" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="10.6" punct="pe:10">c<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<pgtc id="5" weight="2" schema="CR">m<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="pe">in</seg></rhyme></pgtc></w> !</l>
					<l n="11" num="3.3" lm="10" met="5+5"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="11.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="11.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="11.5">pi<seg phoneme="e" type="vs" value="1" rule="241" place="5" caesura="1">e</seg>d</w><caesura></caesura> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="11.7">tr<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="11.9" punct="vg:10">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg><pgtc id="5" weight="2" schema="CR">m<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="vg">in</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="5" met="5"><space quantity="12" unit="char"></space><w n="12.1">F<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="12.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="3">e</seg>ds</w> <w n="12.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="12.5" punct="pt:5"><pgtc id="6" weight="2" schema="[CR">c<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="249" place="5" punct="pt">œu</seg>r</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" rhyme="None">
					<l n="13" num="4.1" lm="10" met="5+5"><w n="13.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="13.2">s<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>rs</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="13.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg></w> <w n="13.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="13.7" punct="vg:10">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9" mp="M">o</seg><pgtc id="6" weight="2" schema="CR">qu<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
					<l n="14" num="4.2" lm="10" met="5+5"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="14.3">n<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="14.4"><seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg></w><caesura></caesura> <w n="14.5">rh<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>ps<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="14.6" punct="vg:10">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9" mp="M">ain</seg><pgtc id="7" weight="2" schema="CR">qu<rhyme label="d" id="7" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="10" met="5+5"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="15.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>rs</w> <w n="15.3">fr<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>ts</w><caesura></caesura> <w n="15.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" mp="M">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="15.6"><pgtc id="7" weight="2" schema="[CR">ch<rhyme label="d" id="7" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="10">œu</seg>r</rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="5" met="5"><space quantity="12" unit="char"></space><w n="16.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="16.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="2">om</seg></w> <w n="16.3" punct="pt:5"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg><pgtc id="8" weight="2" schema="CR">r<rhyme label="e" id="8" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="pt">é</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" rhyme="None">
					<l n="17" num="5.1" lm="10" met="5+5"><w n="17.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="Lc">u</seg>squ</w>’<w n="17.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg>x</w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="17.4">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" caesura="1">ai</seg>rs</w><caesura></caesura> <w n="17.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="17.6">l</w>’<w n="17.7" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<pgtc id="9" weight="0" schema="R"><rhyme label="f" id="9" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="10" punct="vg">ai</seg></rhyme></pgtc></w>,</l>
					<l n="18" num="5.2" lm="10" met="5+5"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="18.3" punct="vg:3">l<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg>th</w>, <w n="18.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4" mp="Lc">eu</seg>t</w>-<w n="18.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" caesura="1">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="18.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="18.7">j<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="18.8" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg><pgtc id="8" weight="2" schema="CR">r<rhyme label="e" id="8" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="19" num="5.3" lm="10" met="5+5"><w n="19.1">F<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="19.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.3">l</w>’<w n="19.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>t</w><caesura></caesura> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="19.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="19.7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="19.8">d<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg><pgtc id="8" weight="2" schema="CR">r<rhyme label="e" id="8" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></rhyme></pgtc></w></l>
					<l n="20" num="5.4" lm="5" met="5"><space quantity="12" unit="char"></space><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="20.2" punct="pt:5"><seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>r<pgtc id="10" weight="2" schema="CR">t<rhyme label="g" id="10" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="pt">e</seg>l</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="10" met="5+5"><w n="21.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fp">e</seg></w>-<w n="21.2">t</w>-<w n="21.3" punct="vg:3"><seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>l</w>, <w n="21.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="5" caesura="1">eau</seg></w><caesura></caesura> <w n="21.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="21.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="21.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>r</w> <w n="21.8" punct="vg:10"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg><pgtc id="10" weight="2" schema="CR">t<rhyme label="g" id="10" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="10" punct="vg">e</seg>l</rhyme></pgtc></w>,</l>
					<l n="22" num="6.2" lm="10" met="5+5"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>bl<seg phoneme="u" type="vs" value="1" rule="427" place="2" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>r</w> <w n="22.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="22.3">f<seg phoneme="ø" type="vs" value="1" rule="398" place="5" caesura="1">eu</seg></w><caesura></caesura> <w n="22.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="22.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg>s</w> <w n="22.6" punct="vg:10">s<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>mm<pgtc id="9" weight="0" schema="R"><rhyme label="f" id="9" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" punct="vg">e</seg>ts</rhyme></pgtc></w>,</l>
					<l n="23" num="6.3" lm="10" met="5+5"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="P">u</seg>r</w> <w n="23.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="23.4">p<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg>s</w><caesura></caesura> <w n="23.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="23.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ph<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</w> <w n="23.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="23.8">ci<pgtc id="10" weight="0" schema="R"><rhyme label="g" id="10" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>l</rhyme></pgtc></w></l>
					<l n="24" num="6.4" lm="5" met="5"><space quantity="12" unit="char"></space><w n="24.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="24.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="24.3" punct="pt:5">j<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg><pgtc id="9" weight="2" schema="CR">m<rhyme label="f" id="9" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="pt">ai</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1861">Février 1861.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>