<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">RONDELS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>312 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">RONDELS</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillerondels.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN320" modus="sm" lm_max="8" metProfile="8" form="rondel classique" schema="ABba abAB abbaA" er_moy="1.14" er_max="2" er_min="0" er_mode="2(4/7)" er_moy_et="0.99">
				<head type="number">V</head>
				<head type="main"> L’Automne</head>
				<lg n="1" rhyme="ABba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3" punct="vg:5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="5" punct="vg">u</seg></w>, <w n="1.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">Au</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="A" id="1" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="2.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="2.4">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<pgtc id="2" weight="0" schema="R"><rhyme label="B" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="vg">e</seg>il</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.3">c<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="3.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>il</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="4.6" punct="pt:8">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>s<pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" rhyme="abAB">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1" punct="vg:2">P<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="5.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.5"><pgtc id="3" weight="2" schema="[CR">t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="6.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="6.6" punct="pv:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>mm<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pv">e</seg>il</rhyme></pgtc></w> ;</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3" punct="vg:5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="5" punct="vg">u</seg></w>, <w n="7.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">Au</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="A" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="8.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="8.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="8.4">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<pgtc id="4" weight="0" schema="R"><rhyme label="B" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pt">e</seg>il</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" rhyme="abbaA">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.3">N<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="4">ym</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="9.5">s</w>’<w n="9.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="10.4">n<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="10.6">l</w>’<w n="10.7" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rt<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="vg">e</seg>il</rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">R<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="11.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ts</w> <w n="11.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>il</rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">g<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="12.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="12.5" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="13" num="3.5" lm="8" met="8"><w n="13.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3" punct="vg:5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="5" punct="vg">u</seg></w>, <w n="13.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">Au</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="A" id="5" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
	</div></body></text></TEI>