<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">RONDELS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>312 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">RONDELS</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillerondels.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN327" modus="sm" lm_max="8" metProfile="8" form="rondel classique" schema="ABba abAB abbaA" er_moy="2.57" er_max="6" er_min="2" er_mode="2(6/7)" er_moy_et="1.4">
				<head type="number">XII</head>
				<head type="main">Le Midi</head>
				<lg n="1" rhyme="ABba">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="1.3" punct="vg:3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>r</w>, <w n="1.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="1.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="1.7" punct="vg:8">M<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="1" weight="2" schema="CR">d<rhyme label="A" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="2.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="2.5" punct="vg:8">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>l<pgtc id="2" weight="2" schema="CR">t<rhyme label="B" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Fr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="3.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>t</w> <w n="3.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.6" punct="vg:8"><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">B<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="4.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="4.4" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<pgtc id="1" weight="2" schema="CR">d<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" rhyme="abAB">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rt</w> <w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="5.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt</w> <w n="5.4" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ppl<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg><pgtc id="3" weight="2" schema="CR">d<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">B<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>ll<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg></w> <w n="6.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>t</w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="6.4" punct="pv:8">c<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="7.3" punct="vg:3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>r</w>, <w n="7.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="7.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="7.7" punct="vg:8">M<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="3" weight="2" schema="CR">d<rhyme label="A" id="3" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="8.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="8.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="8.5" punct="pt:8">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>l<pgtc id="4" weight="2" schema="CR">t<rhyme label="B" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" rhyme="abbaA">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2" punct="vg:3">W<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="3" punct="vg">eau</seg></w>, <w n="9.3">n<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>ni<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="9.4" punct="vg:8">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<pgtc id="5" weight="2" schema="CR">d<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="10.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="10.4" punct="vg:4">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>t</w>, <w n="10.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="10.6" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<pgtc id="6" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="6" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="11.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="11.4">qu</w>’<w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.7">tr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.8">C<pgtc id="6" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>th<rhyme label="b" id="6" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="12.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="12.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="12.5" punct="vg:8">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg><pgtc id="5" weight="2" schema="CR">d<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></rhyme></pgtc></w>,</l>
					<l n="13" num="3.5" lm="8" met="8"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="13.3" punct="vg:3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>r</w>, <w n="13.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="13.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="13.7" punct="pt:8">M<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="5" weight="2" schema="CR">d<rhyme label="A" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg></rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>