<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRADUCTIONS</head><div type="poem" key="CHE71" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="4((aa))" er_moy="1.0" er_max="2" er_min="0" er_mode="0(2/4)" er_moy_et="1.0">
					<head type="number">VI</head>
					<head type="main">Traduction de Platon</head>
					<lg n="1" type="distiques" rhyme="aa…">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w> <w n="1.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="1.3">l</w>’<w n="1.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="P">u</seg>r</w> <w n="1.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="1.8">j<seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="1.10"><pgtc id="1" weight="2" schema="[CR">fl<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D</w>’<w n="2.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="2.3">p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="2.7" punct="pt:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2" punct="vg:2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>, <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3" mp="P">è</seg>s</w> <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.5">j</w>’<w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg></w><caesura></caesura> <w n="3.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="3.8">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" mp="C">e</seg>t</w> <w n="3.9"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="3.10" punct="vg:12">b<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>c<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rc</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="4.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="4.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="4.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="4.7"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="4.8" punct="pt:12">f<seg phoneme="œ" type="vs" value="1" rule="406" place="11" mp="M">eu</seg>ill<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="5.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="5.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>c<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="5.5">r<seg phoneme="o" type="vs" value="1" rule="444" place="6" caesura="1">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="5.7">c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="6.2" punct="pt:3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="pt">ai</seg>t</w>. <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">Un</seg></w> <w n="6.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="6.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="6.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="6.7">b<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.8">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>r<pgtc id="3" weight="2" schema="CR">m<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></pgtc></w></l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="Lc">en</seg>tr</w>’<w n="7.3"><seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M/mc">ou</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="7.4" punct="vg:6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M">o</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="7.7">j<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="7.8"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>b<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.2">c<seg phoneme="œ" type="vs" value="1" rule="345" place="3" mp="M">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="8.4">mi<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>l</w><caesura></caesura> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="8.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="8.8" punct="pt:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rm<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>