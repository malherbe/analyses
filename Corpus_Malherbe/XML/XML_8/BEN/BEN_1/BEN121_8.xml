<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE CY GIST</head><head type="sub_part">
					OU 
					Diverses Épitaphes pour toute sorte de personnes 
					de l’un et de l’autre sexe, et pour l’Auteur même, 
					quoique vivant.
				</head><div type="poem" key="BEN121" modus="sm" lm_max="7" metProfile="7" form="strophe unique" schema="1(aabcbc)" er_moy="1.33" er_max="2" er_min="0" er_mode="2(2/3)" er_moy_et="0.94">
					<head type="main">Épitaphe d’un Homme d’esprit.</head>
					<lg n="1" type="sizain" rhyme="aabcbc">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1">C<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg></w> <w n="1.2">g<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st</w> <w n="1.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="1.4">n</w>’<w n="1.5"><seg phoneme="y" type="vs" value="1" rule="391" place="4">eu</seg>t</w> <w n="1.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>t</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.8" punct="pv:7"><pgtc id="1" weight="2" schema="CR">pr<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pv">i</seg>x</rhyme></pgtc></w> ;</l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="2.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="2.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="2.5" punct="vg:7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>s<pgtc id="1" weight="2" schema="CR">pr<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>ts</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg></w> <w n="3.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="3.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.5" punct="vg:7">j<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg><pgtc id="2" weight="2" schema="CR">n<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="7">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="7" met="7"><w n="4.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>sf<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>cl<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t</rhyme></pgtc></w></l>
						<l n="5" num="1.5" lm="7" met="7"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.4">f<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg><pgtc id="2" weight="2" schema="CR">n<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="7">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="7" met="7"><w n="6.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="6.2">C<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg></w> <w n="6.3" punct="pt:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pt">a</seg>t</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>