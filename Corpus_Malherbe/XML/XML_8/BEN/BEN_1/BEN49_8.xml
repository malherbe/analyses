<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN49" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abab abab ccd ede" er_moy="1.71" er_max="2" er_min="0" er_mode="2(6/7)" er_moy_et="0.7">
					<head type="form">SONNET.</head>
					<lg n="1" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">E</seg>S</w> <w n="1.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="1.3">fi<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="1.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>m<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg" caesura="1">au</seg>x</w>,<caesura></caesura> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="1.8">l</w>’<w n="1.9"><seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.10" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="435" place="10" mp="M">o</seg>pp<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="1" weight="2" schema="CR">s<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">ez</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="a" type="vs" value="1" rule="341" place="7">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.8" punct="pv:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="10" mp="M">o</seg>nn<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg><pgtc id="2" weight="2" schema="CR">ss<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="3.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>t</w> <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5" punct="vg:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="3.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>ls</w> <w n="3.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="3.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg><pgtc id="1" weight="2" schema="CR">s<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">ez</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="vg:1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="4.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="4.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="4.6">n</w>’<w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="4.8">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg></w> <w n="4.9">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="9">oin</seg>s</w> <w n="4.10">qu</w>’<w n="4.11" punct="pt:12"><seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="2" weight="2" schema="CR">c<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="abab">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg></w> <w n="5.2">d</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="5.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="5.5">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>rs</w><caesura></caesura> <w n="5.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>vr<seg phoneme="wa" type="vs" value="1" rule="423" place="9">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.8"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.9" punct="vg:12"><seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg><pgtc id="3" weight="2" schema="CR">s<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">ez</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="6.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" mp="M">in</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>lt<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="6.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="Lc">ou</seg>s</w>-<w n="6.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" caesura="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="6.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="6.7">d<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="6.8" punct="pv:12">pr<seg phoneme="e" type="vs" value="1" rule="353" place="11" mp="M">e</seg><pgtc id="4" weight="2" schema="CR">ss<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="7.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="7.5">sç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="7.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="7.8">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="7.9" punct="vg:12">m<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="3" weight="2" schema="CR">s<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">ez</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1" punct="vg:3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">In</seg>gr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="8.2">n</w>’<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w>-<w n="8.4">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="5" mp="F">e</seg></w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></w> <w n="8.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="8.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="8.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="8.10" punct="pi:12"><pgtc id="4" weight="2" schema="[CR">s<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="9.3">bru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="9.5">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">om</seg>ph<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" mp="M">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="9.8">v<seg phoneme="o" type="vs" value="1" rule="438" place="10" mp="C">o</seg>s</w> <w n="9.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="5" weight="2" schema="CR">pp<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="10.2">n</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2" mp="Lp">e</seg>st</w>-<w n="10.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="10.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>t</w> <w n="10.6">c<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="10.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.8">v<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="10.9">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="10.10">m</w>’<w n="10.11"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="10" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="11">ez</seg></w> <w n="10.12" punct="pi:12"><pgtc id="5" weight="2" schema="[CR">p<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pi">a</seg>s</rhyme></pgtc></w> ?</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="11.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="11.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="11.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="11.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="11.7">qu</w>’<w n="11.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="11.9">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="11.10" punct="pt:12">n<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" rhyme="ede">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="12.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="12.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>t</w><caesura></caesura> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.6">d<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</w> <w n="12.7"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="12.8" punct="pv:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>s<pgtc id="7" weight="2" schema="CR">t<rhyme label="e" id="7" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pv">an</seg>t</rhyme></pgtc></w> ;</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="13.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" mp="M">in</seg>j<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="13.3" punct="vg:6">b<seg phoneme="o" type="vs" value="1" rule="315" place="5" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="13.4">sç<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="8">ez</seg></w> <w n="13.5">qu</w>’<w n="13.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="13.7">h<seg phoneme="o" type="vs" value="1" rule="435" place="10" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="412" place="11">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="13.8">h<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">s</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>cc<seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="14.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" caesura="1">oin</seg>t</w><caesura></caesura> <w n="14.5">d</w>’<w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="14.7">m<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>s</w> <w n="14.8" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="7" weight="2" schema="CR">t<rhyme label="e" id="7" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>