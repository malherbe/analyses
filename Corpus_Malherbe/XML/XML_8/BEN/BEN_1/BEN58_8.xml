<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN58" modus="cp" lm_max="12" metProfile="8, 6+6" form="strophe unique" schema="1(aabcbc)" er_moy="2.67" er_max="6" er_min="0" er_mode="0(1/3)" er_moy_et="2.49">
					<head type="main">Pour une femme grosse.</head>
					<head type="form">MADRIGAL.</head>
					<lg n="1" type="sizain" rhyme="aabcbc">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">OU</seg>S</w> <w n="1.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="1.4">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>q</w> <w n="1.5">m<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="1.6">f<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r</w> <w n="1.7">v<seg phoneme="o" type="vs" value="1" rule="415" place="9">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.8" punct="pv:12">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>gu<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pv">eu</seg>r</rhyme></pgtc></w> ;</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="2.2" punct="pe:2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="pe">eu</seg>x</w> ! <w n="2.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="2.4">f<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="2.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="2.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="2.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="2.9">c<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="12">œu</seg>r</rhyme></pgtc></w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="3.2">c<seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="3.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="4" mp="C">o</seg>s</w> <w n="3.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="3.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">v<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.8" punct="pi:12">t<seg phoneme="i" type="vs" value="1" rule="493" place="10" mp="M">y</seg>r<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg><pgtc id="2" weight="2" schema="CR">nn<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="323" place="3">ay</seg></w> <w n="4.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>gn<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="4.4">d</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="4.6">r<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">om</seg>p<pgtc id="3" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>s<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></pgtc></w></l>
						<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.5">f<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="2" weight="2" schema="CR">n<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="6.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">où</seg></w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.4">v<seg phoneme="o" type="vs" value="1" rule="415" place="4">ô</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.6" punct="pt:8">c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>mm<pgtc id="3" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>