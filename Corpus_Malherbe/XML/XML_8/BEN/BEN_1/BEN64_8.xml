<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><head type="main_subpart">ÉPIGRAMMES.</head><div type="poem" key="BEN64" modus="cm" lm_max="12" metProfile="6+6" form="strophe unique" schema="1(abab)" er_moy="1.0" er_max="2" er_min="0" er_mode="0(1/2)" er_moy_et="1.0">
						<head type="main">ÉPIGRAMME</head>
						<lg n="1" type="quatrain" rhyme="abab">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="1.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="1.4">m<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="1.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="1.6">f<seg phoneme="a" type="vs" value="1" rule="193" place="9">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rd<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">V<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>t</w> <w n="2.2" punct="pv:3">m<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="pv">i</seg>r</w> ; <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="2.4">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt</w> <w n="2.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="2.8">f<seg phoneme="a" type="vs" value="1" rule="193" place="9">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.9" punct="dp:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">â</seg><pgtc id="2" weight="2" schema="CR">l<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="dp">i</seg>t</rhyme></pgtc></w> :</l>
							<l n="3" num="1.3" lm="12" met="6+6">« <w n="3.1">C</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="3.4" punct="vg:3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg></w>, <w n="3.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="3.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="3.7" punct="vg:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="3.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="3.9">v<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="3.10"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="9">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="3.11" punct="vg:12">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="457" place="12">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1" mp="C">i</seg></w> <w n="4.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="Lp">i</seg>t</w>-<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="4.5" punct="dp:6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="dp" caesura="1">an</seg>t</w> :<caesura></caesura> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="4.7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="4.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="4.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="4.10" punct="pt:12"><pgtc id="2" weight="2" schema="[CR">l<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>t</rhyme></pgtc></w>. »</l>
						</lg>
					</div></body></text></TEI>