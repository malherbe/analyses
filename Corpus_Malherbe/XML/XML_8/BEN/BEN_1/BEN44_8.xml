<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN44" modus="cm" lm_max="12" metProfile="6+6" form="sonnet peu classique" schema="abab baba ccd ede" er_moy="0.43" er_max="2" er_min="0" er_mode="0(5/7)" er_moy_et="0.73">
					<head type="main">À Iris.</head>
					<head type="form">SONNET.</head>
					<lg n="1" rhyme="abab">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">OU</seg>S</w> <w n="1.2">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2" mp="M/mp">o</seg>qu<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="1.3" punct="vg:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>s</w>, <w n="1.4" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">I</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="1.5">d</w>’<w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="1.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="1.8" punct="pi:12">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3" punct="vg:3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>t</w>, <w n="2.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="Fm">e</seg>s</w>-<w n="2.5" punct="vg:6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>s</w>,<caesura></caesura> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M/mp">on</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="9" mp="Lp">i</seg>t</w>-<w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="2.9">v<seg phoneme="o" type="vs" value="1" rule="438" place="11" mp="C">o</seg>s</w> <w n="2.10" punct="pt:12"><pgtc id="2" weight="2" schema="[CR">p<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="3.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="Lp">aî</seg>t</w>-<w n="3.3" punct="vg:3"><seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>l</w>, <w n="3.4">qu</w>’<w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="3.6">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="3.7">m<seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>ts</w><caesura></caesura> <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="3.9">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="3.10">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="3.11" punct="pi:12">r<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>p<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="4.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="4.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="4.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6" caesura="1">en</seg>s</w><caesura></caesura> <w n="4.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.7">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="4.8">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="4.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="4.10">v<seg phoneme="ø" type="vs" value="1" rule="398" place="11">eu</seg>t</w> <w n="4.11" punct="pt:12"><pgtc id="2" weight="2" schema="[CR">p<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="baba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="5.3">qu</w>’<w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="5.5">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="5.6"><seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>vr<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="5.8"><seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M/mc">i</seg>c<seg phoneme="i" type="vs" value="1" rule="493" place="11" mp="Lc">y</seg></w>-<w n="5.9" punct="pt:12">b<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="6.3">l</w>’<w n="6.4"><seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="6.5">l</w>’<w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="6.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="6.8" punct="pv:12">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>c<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="7.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="7.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="7.6">br<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="7.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="7.8">v<seg phoneme="o" type="vs" value="1" rule="438" place="10" mp="C">o</seg>s</w> <w n="7.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>pp<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="8.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="8.4" punct="vg:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" punct="vg" caesura="1">e</seg>il</w>,<caesura></caesura> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.6" punct="vg:8">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8" punct="vg">e</seg>l</w>, <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="8.8" punct="vg:10">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" punct="vg">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.9"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="8.10">l</w>’<w n="8.11" punct="pt:12"><pgtc id="4" weight="0" schema="[R"><rhyme label="a" id="4" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="9.2">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="2" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="9.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="9.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="9.8">v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="9.9">gu<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.10"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="9.11" punct="vg:12">D<pgtc id="5" weight="1" schema="GR">i<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="10.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rs<seg phoneme="y" type="vs" value="1" rule="d-3" place="4" mp="M">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="10.4">qu</w>’<w n="10.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="10.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="10.7">s<seg phoneme="o" type="vs" value="1" rule="318" place="9">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="10.9">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="10.10" punct="pv:12">l<pgtc id="5" weight="1" schema="GR">i<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pv">eu</seg></rhyme></pgtc></w> ;</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="11.3">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="11.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.5">c<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="11.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="10">oin</seg>t</w> <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg></w> <w n="11.8" punct="pt:12">v<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="415" place="12">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" rhyme="ede">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="12.3">pr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="12.5" punct="vg:6">ch<seg phoneme="o" type="vs" value="1" rule="444" place="6" punct="vg" caesura="1">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="12.8">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="12.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="12.10" punct="dp:12">n<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="dp">ez</seg></rhyme></pgtc></w> :</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="13.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="13.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">l</w>’<w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="13.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="13.7">d<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="13.8">l</w>’<w n="13.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg></w> <w n="13.11">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="P">u</seg>r</w> <w n="13.12">l</w>’<w n="13.13" punct="pt:12"><pgtc id="6" weight="0" schema="[R"><rhyme label="d" id="6" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="12">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="14.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="14.5" punct="vg:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="366" place="6" punct="vg" caesura="1">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="14.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="14.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="14.8">v<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="C">ou</seg>s</w> <w n="14.9">m</w>’<w n="14.10" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="11" mp="M">aî</seg>ni<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">ez</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>