<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR16" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique" schema="1(ababcdcd) 3(abab)" er_moy="0.1" er_max="1" er_min="0" er_mode="0(9/10)" er_moy_et="0.3">
				<lg n="1" type="huitain" rhyme="ababcdcd">
					<l n="1" num="1.1" lm="8" met="8"><space quantity="12" unit="char"></space><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="1.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="1.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w>-<w n="1.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="1.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="1.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="1.7" punct="vg:8">r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="2.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">om</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.4">j</w>’<w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="2.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="Lc">en</seg>tr</w>’<w n="2.9" punct="vg:12"><seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M/mc">ou</seg>vr<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="Lc">En</seg>tr</w>’<w n="3.2"><seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M/mc">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="3.3">d</w>’<w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="3.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="3.6">p<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="3.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="3.9" punct="vg:12">gl<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="4.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="4.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="4.4">c</w>’<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="4.7" punct="tc:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg ti" caesura="1">oi</seg></w>,<caesura></caesura> — <w n="4.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.9">t<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="4.10">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="4.11">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="4.12" punct="pe:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>ffr<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pe">i</seg>r</rhyme></pgtc></w> !</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="5.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="5.3" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">â</seg>tr<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="5.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="5.5" punct="tc:9">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="9" punct="vg ti">é</seg></w>, — <w n="5.6">n<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r</w> <w n="5.7" punct="vg:12">m<seg phoneme="i" type="vs" value="1" rule="493" place="11" mp="M">y</seg>st<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="6.2">n<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="6.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="6.6">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>rs</w><caesura></caesura> <w n="6.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="6.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="6.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="6.10">Nu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>t</w> <w n="6.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="6.12" punct="vg:12">m<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="vg">oi</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="7.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="7.5">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="7.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="7.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>sp<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="8" met="8"><space quantity="12" unit="char"></space><w n="8.1">L</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="8.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">j</w>’<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="8.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="8.8" punct="pe:8">t<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="pe">oi</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="9" num="2.1" lm="8" met="8"><space quantity="12" unit="char"></space><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">n</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="9.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="9.5">j<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="9.6">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="9.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="9.8" punct="vg:8">p<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="2.2" lm="12" met="6+6"><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="10.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="10.3" punct="tc:3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="3" punct="vg ti">en</seg>s</w>, — <w n="10.4">s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="10.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="10.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="10.7">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t</w> <w n="10.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="10.10"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="10.11" punct="vg:12">c<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="vg">œu</seg>r</rhyme></pgtc></w>,</l>
					<l n="11" num="2.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">s</w>’<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="11.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="11.6">ch<seg phoneme="o" type="vs" value="1" rule="444" place="6" caesura="1">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="11.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="11.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="11.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="11.11">s<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>l<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="2.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="M">O</seg>bst<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="4">en</seg>t</w> <w n="12.2" punct="vg:6">g<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="12.3">cr<seg phoneme="y" type="vs" value="1" rule="454" place="7" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10">en</seg>t</w> <w n="12.4" punct="pe:12">b<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>d<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pe">eu</seg>r</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="13" num="3.1" lm="12" met="6+6"><w n="13.1" punct="pe:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="pe">on</seg></w> ! <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.3">n</w>’<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="13.5">j<seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="13.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg></w><caesura></caesura> <w n="13.7">s</w>’<w n="13.8"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="13.9"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="13.10">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="13.11">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="13.12"><pgtc id="7" weight="0" schema="[R"><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="3.2" lm="12" met="6+6"><w n="14.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="14.2">pl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="14.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="14.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rd</w><caesura></caesura> <w n="14.6">p<seg phoneme="y" type="vs" value="1" rule="445" place="7">û</seg>t</w> <w n="14.7">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="14.9" punct="vg:12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>m<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="12" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
					<l n="15" num="3.3" lm="12" met="6+6"><w n="15.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="15.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="15.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="15.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="15.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>st<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="15.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7" punct="vg:9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" punct="vg">an</seg>t</w>, <w n="15.8">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="10">oi</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="15.9" punct="vg:12">f<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="193" place="12">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="16" num="3.4" lm="8" met="8"><space quantity="12" unit="char"></space><w n="16.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2" punct="pe:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="pe">an</seg>t</w> ! <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="16.4" punct="pe:5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" punct="pe ti">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! — <w n="16.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="16.6" punct="pe:8">n<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="17" num="4.1" lm="8" met="8"><space quantity="12" unit="char"></space><w n="17.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="17.2">n<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="17.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="17.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="17.6" punct="pe:8">v<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="18" num="4.2" lm="12" met="6+6"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="18.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="18.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="18.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="18.5"><seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="18.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="18.7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>rs</w> <w n="18.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="18.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="18.10" punct="pv:12">m<pgtc id="10" weight="1" schema="GR">i<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="pv">en</seg></rhyme></pgtc></w> ;</l>
					<l n="19" num="4.3" lm="12" met="6+6"><w n="19.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="19.2">l</w>’<w n="19.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.4"><seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>n<seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="19.5">qu</w>’<w n="19.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="19.7" punct="vg:9"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="19.8">n<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="C">ou</seg>s</w> <w n="19.9" punct="pe:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>f<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					<l n="20" num="4.4" lm="12" met="6+6"><w n="20.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="20.2">br<seg phoneme="y" type="vs" value="1" rule="445" place="2" mp="M">û</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="20.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="20.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="20.6">l</w>’<w n="20.7" punct="pe:9"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8" mp="M">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" punct="pe ti">an</seg>t</w> ! — <w n="20.8">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="20.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="20.10" punct="pe:12">r<pgtc id="10" weight="1" schema="GR">i<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="12" punct="pe pe">en</seg></rhyme></pgtc></w> !!</l>
					<l ana="incomplete" where="F" n="21" num="4.5"><w n="21.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="21.3">v<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.4">d</w>’<w n="21.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="21.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w>................................</l>
				</lg>
			</div></body></text></TEI>