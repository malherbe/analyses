<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HYMNES, ROMANCES ET CHANSONNETTES</head><div type="poem" key="CAO54" modus="cm" lm_max="10" metProfile="4+6" form="suite périodique" schema="3(ababcdcd)" er_moy="1.17" er_max="2" er_min="0" er_mode="2(7/12)" er_moy_et="0.99">
					<head type="main">LA CRÈCHE DE NOËL <ref target="7" type="noteAnchor">7</ref></head>
					<head type="main">Musique de M. N. Crépault</head>
					<div type="section" n="1">
					<head type="main">I</head>
						<lg n="1" type="huitain" rhyme="ababcdcd">
							<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="1.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg></w><caesura></caesura> <w n="1.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="P">u</seg>r</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="1.7"><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
							<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="2.2">l<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rd</w> <w n="2.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="4" caesura="1">eau</seg></w><caesura></caesura> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="2.5">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="6">ei</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="2.8" punct="pv:10">fr<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pv">a</seg>s</rhyme></pgtc></w> ;</l>
							<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="3.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="3.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4" caesura="1">oi</seg>r</w><caesura></caesura> <w n="3.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="3.7">m<seg phoneme="i" type="vs" value="1" rule="493" place="9" mp="M">y</seg>s<pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
							<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="4.3">r<seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="4.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="4.5">br<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.7" punct="pt:10">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rgl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
							<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="5.3" punct="pt:4">m<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="pt" caesura="1">i</seg>t</w>.<caesura></caesura> <w n="5.4">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="5.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="5.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="5.7"><pgtc id="3" weight="2" schema="[CR">t<rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
							<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">J<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg>x</w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="438" place="4" caesura="1">o</seg>s</w><caesura></caesura> <w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="6.5">h<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="6.6" punct="vg:10">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">om</seg><pgtc id="4" weight="2" schema="CR">ph<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
							<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3" punct="vg:4">chr<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4" punct="vg" caesura="1">en</seg></w>,<caesura></caesura> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="7.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="7.6" punct="vg:8">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>x</w>, <w n="7.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w> <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg><pgtc id="4" weight="2" schema="CR">f<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</rhyme></pgtc></w> <del reason="analysis" hand="RR" type="repetition">(bis)</del>.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="main">II</head>
						<lg n="1" type="huitain" rhyme="ababcdcd">
							<l n="9" num="1.1" lm="10" met="4+6"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="9.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="9.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>d</w><caesura></caesura> <w n="9.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.8">r<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</w> <w n="9.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="9.10" punct="vg:10">m<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="10" num="1.2" lm="10" met="4+6"><w n="10.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="10.2">r<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="4" caesura="1">eu</seg>x</w><caesura></caesura> <w n="10.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6" punct="vg:10"><seg phoneme="y" type="vs" value="1" rule="453" place="7" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rs<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="10" punct="vg">e</seg>l</rhyme></pgtc></w>,</l>
							<l n="11" num="1.3" lm="10" met="4+6"><w n="11.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3" mp="M">o</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="11.5">f<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="11.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="11.7" punct="vg:10">gr<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="12" num="1.4" lm="10" met="4+6"><w n="12.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="12.2">p<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="12.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" caesura="1">ain</seg>t</w><caesura></caesura> <w n="12.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="12.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="12.9" punct="pe:10">ci<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="10" punct="pe">e</seg>l</rhyme></pgtc></w> !</l>
							<l n="13" num="1.5" lm="10" met="4+6"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2" punct="vg:4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="C">i</seg>l</w> <w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="13.5">n<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="13.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="P">u</seg>r</w> <w n="13.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="13.8" punct="pv:10">p<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="307" place="10">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
							<l n="14" num="1.6" lm="10" met="4+6"><w n="14.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="14.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="14.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" caesura="1">o</seg>rps</w><caesura></caesura> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>pr<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="14.6" punct="ps:10">d<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg><pgtc id="8" weight="2" schema="CR">l<rhyme label="d" id="8" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="ps">eu</seg>rs</rhyme></pgtc></w>…</l>
							<l n="15" num="1.7" lm="10" met="4+6"><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">l</w>’<w n="15.3"><seg phoneme="y" type="vs" value="1" rule="453" place="2" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" caesura="1">e</seg>rs</w><caesura></caesura> <w n="15.4">d</w>’<w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="7">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="15.6" punct="vg:10">tr<seg phoneme="e" type="vs" value="1" rule="353" place="9" mp="M">e</seg>ss<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="307" place="10">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>, <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="16" num="1.8" lm="10" met="4+6"><w n="16.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="16.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2" mp="C">e</seg>t</w> <w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="16.4">r<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="16.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="8" mp="C">o</seg>s</w> <w n="16.6" punct="pe:10">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="8" weight="2" schema="CR">lh<rhyme label="d" id="8" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pe">eu</seg>rs</rhyme></pgtc></w> ! <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="main">III</head>
						<lg n="1" type="huitain" rhyme="ababcdcd">
							<l n="17" num="1.1" lm="10" met="4+6"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="17.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="17.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="17.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" caesura="1">e</seg>l</w><caesura></caesura> <w n="17.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="17.7" punct="vg:10">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="9" mp="M">a</seg>y<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
							<l n="18" num="1.2" lm="10" met="4+6"><w n="18.1">Gu<seg phoneme="i" type="vs" value="1" rule="491" place="1" mp="M">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="18.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="18.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="18.5">r<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="18.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="18.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="18.8">pu<seg phoneme="i" type="vs" value="1" rule="491" place="9" mp="M">i</seg><pgtc id="10" weight="2" schema="CR">ss<rhyme label="b" id="10" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>ts</rhyme></pgtc></w></l>
							<l n="19" num="1.3" lm="10" met="4+6"><w n="19.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="19.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="19.3"><seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>r</w> ‒<caesura></caesura> <w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="19.5">gu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="19.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="19.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>r<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w> ‒</l>
							<l n="20" num="1.4" lm="10" met="4+6"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="20.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M/mc">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="3" mp="Lc">eau</seg></w>-<w n="20.3">n<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="20.4">l</w>’<w n="20.5" punct="vg:5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="vg">o</seg>r</w>, <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="20.7">m<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>rrh<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.8"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="20.9">l</w>’<w n="20.10" punct="pe:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg><pgtc id="10" weight="2" schema="CR">c<rhyme label="b" id="10" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="10" punct="pe">en</seg>s</rhyme></pgtc></w> !</l>
							<l n="21" num="1.5" lm="10" met="4+6"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="21.2" punct="vg:4">chr<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4" punct="vg" caesura="1">en</seg>s</w>,<caesura></caesura> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="21.4">l</w>’<w n="21.5"><seg phoneme="e" type="vs" value="1" rule="354" place="6" mp="M">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="21.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="21.7" punct="vg:10"><pgtc id="11" weight="2" schema="[CR">M<rhyme label="c" id="11" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
							<l n="22" num="1.6" lm="10" met="4+6"><w n="22.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="22.2">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rn<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="22.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="22.5" punct="pe:10">R<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">em</seg>p<pgtc id="12" weight="2" schema="CR">t<rhyme label="d" id="12" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pe">eu</seg>r</rhyme></pgtc></w> !</l>
							<l n="23" num="1.7" lm="10" met="4+6"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">A</seg>dr<seg phoneme="e" type="vs" value="1" rule="353" place="2" mp="M/mp">e</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="Lp">on</seg>s</w>-<w n="23.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="23.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="5" mp="C">o</seg>s</w> <w n="23.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="d-3" place="7" mp="M">u</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="23.5">h<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg><pgtc id="11" weight="2" schema="CR">mm<rhyme label="c" id="11" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w> <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="24" num="1.8" lm="10" met="4+6"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="24.2" punct="dp:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="dp" caesura="1">on</seg>s</w> :<caesura></caesura> <w n="24.3">Gl<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg></w> <w n="24.5" punct="pe:10">L<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="12" weight="2" schema="CR">t<rhyme label="d" id="12" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pe">eu</seg>r</rhyme></pgtc></w> ! <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
						</lg>
					</div>
					<closer>
						<dateline>
							<date when="1887">Décembre 1887</date>.
						</dateline>
						<note type="footnote" id="7">[Dédié au révérend M.F.-H. Bélanger, curé de St-Roch, Québec.]</note>
					</closer>
				</div></body></text></TEI>