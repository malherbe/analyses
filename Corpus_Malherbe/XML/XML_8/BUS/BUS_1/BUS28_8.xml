<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS DE TOUS LES PAYS</head><div type="poem" key="BUS28" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="5(abba) 6(aa) 1(abab)" er_moy="0.89" er_max="2" er_min="0" er_mode="0(10/18)" er_moy_et="0.99">
					<head type="main">CHANSON CHINOISE</head>
					<opener>
						<salute>A ANTOINE FAUCHERY, MORT A YOKOHAMA.</salute>
					</opener>
					<lg n="1" type="quatrain" rhyme="abba">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="1.3" punct="vg:2">v<seg phoneme="y" type="vs" value="1" rule="450" place="2" punct="vg">u</seg></w>, <w n="1.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="1.5" punct="vg:4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>r</w>, <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="1.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.8" punct="vg:8">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="2.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="2.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="2.4">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r</w> <w n="2.5">br<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="2.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="2.7">l</w>’<w n="2.8" punct="pv:8"><pgtc id="2" weight="0" schema="[R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pv">eau</seg></rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3" punct="vg:4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="3.4">j</w>’<w n="3.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="3.6">tr<seg phoneme="o" type="vs" value="1" rule="433" place="7">o</seg>p</w> <w n="3.7" punct="tc:8">h<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="vg ti">au</seg>t</rhyme></pgtc></w>, —</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3" punct="tc:3">fu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="ti">i</seg>r</w> — <w n="4.4">j</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="4.6">tr<seg phoneme="o" type="vs" value="1" rule="433" place="6">o</seg>p</w> <w n="4.7">d</w>’<w n="4.8" punct="pe:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>d<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="2" type="distique" rhyme="aa">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1" punct="vg:2">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="5.2" punct="vg:4">B<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ddh<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="5.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.4">l</w>’<w n="5.5"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="3" weight="2" schema="CR">bl<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="6.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="6.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.5">F<seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w>-<w n="6.6" punct="pt:8"><pgtc id="3" weight="2" schema="[CR">L<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="quatrain" rhyme="abab">
						<l n="7" num="3.1" lm="8" met="8"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="7.2" punct="vg:3">fl<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>t</w>, <w n="7.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.6" punct="pt:8">l<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="8" num="3.2" lm="8" met="8"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.3">gr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="8.4" punct="pv:8">n<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>ph<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg>r</rhyme></pgtc></w> ;</l>
						<l n="9" num="3.3" lm="8" met="8"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="9.3">f<seg phoneme="ɥi" type="vs" value="1" rule="462" place="3">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="9.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="9.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.6" punct="pt:8">d<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="10" num="3.4" lm="8" met="8"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="10.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="10.6" punct="pe:8">k<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>dj<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>r</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="4" type="distique" rhyme="aa">
						<l n="11" num="4.1" lm="8" met="8"><w n="11.1" punct="vg:2">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="11.2" punct="vg:4">B<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ddh<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="11.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="11.4">l</w>’<w n="11.5"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="6" weight="2" schema="CR">bl<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></rhyme></pgtc></w></l>
						<l n="12" num="4.2" lm="8" met="8"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="12.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="12.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.5">F<seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w>-<w n="12.6" punct="pt:8"><pgtc id="6" weight="2" schema="[CR">L<rhyme label="a" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="5" type="quatrain" rhyme="abba">
						<l n="13" num="5.1" lm="8" met="8"><w n="13.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="13.3">r<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="13.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="13.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="13.6" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg><pgtc id="7" weight="2" schema="CR">ch<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="5.2" lm="8" met="8"><w n="14.1">J</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="14.3">n<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="14.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="14.7" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="8" weight="2" schema="CR">b<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="15" num="5.3" lm="8" met="8"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="15.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="15.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">aî</seg>t</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="15.6" punct="ps:8"><pgtc id="8" weight="2" schema="[CR">b<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></rhyme></pgtc></w>…</l>
						<l n="16" num="5.4" lm="8" met="8"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">fl<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>t</w> <w n="16.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>t</w> <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="16.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.7" punct="pe:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="7" weight="2" schema="CR">ch<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">er</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="6" type="distique" rhyme="aa">
						<l n="17" num="6.1" lm="8" met="8"><w n="17.1" punct="vg:2">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="17.2" punct="vg:4">B<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ddh<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="17.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.4">l</w>’<w n="17.5"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="9" weight="2" schema="CR">bl<rhyme label="a" id="9" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></rhyme></pgtc></w></l>
						<l n="18" num="6.2" lm="8" met="8"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="18.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="18.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.5">F<seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w>-<w n="18.6" punct="pt:8"><pgtc id="9" weight="2" schema="[CR">L<rhyme label="a" id="9" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="7" type="quatrain" rhyme="abba">
						<l n="19" num="7.1" lm="8" met="8"><w n="19.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="19.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="19.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="19.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="19.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="19.7">tr<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="20" num="7.2" lm="8" met="8"><w n="20.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="20.2">j</w>’<w n="20.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="20.4">l</w>’<w n="20.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="20.7" punct="pe:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<pgtc id="11" weight="0" schema="R"><rhyme label="b" id="11" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pe">eau</seg></rhyme></pgtc></w> !</l>
						<l n="21" num="7.3" lm="8" met="8"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">m</w>’<w n="21.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="21.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="21.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="21.6">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="21.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="21.8">l</w>’<w n="21.9" punct="ps:8"><pgtc id="11" weight="0" schema="[R"><rhyme label="b" id="11" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="ps">eau</seg></rhyme></pgtc></w> …</l>
						<l n="22" num="7.4" lm="8" met="8"><w n="22.1">M<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="22.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="22.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="22.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="22.5" punct="pe:8">f<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>g<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="8" type="distique" rhyme="aa">
						<l n="23" num="8.1" lm="8" met="8"><w n="23.1" punct="vg:2">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="23.2" punct="vg:4">B<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ddh<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="23.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="23.4">l</w>’<w n="23.5"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="12" weight="2" schema="CR">bl<rhyme label="a" id="12" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></rhyme></pgtc></w></l>
						<l n="24" num="8.2" lm="8" met="8"><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="24.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="24.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.5">F<seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w>-<w n="24.6" punct="pt:8"><pgtc id="12" weight="2" schema="[CR">L<rhyme label="a" id="12" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="9" type="quatrain" rhyme="abba">
						<l n="25" num="9.1" lm="8" met="8"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="25.2">l</w>’<w n="25.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="25.4"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="25.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="25.6">l<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="25.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="25.8" punct="pt:8">l<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="26" num="9.2" lm="8" met="8"><w n="26.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="26.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="26.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="26.5">s<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</rhyme></pgtc></w></l>
						<l n="27" num="9.3" lm="8" met="8"><w n="27.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="27.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="27.3" punct="vg:4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg></w> <w n="27.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="27.6">v<pgtc id="14" weight="0" schema="R"><rhyme label="b" id="14" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r</rhyme></pgtc></w></l>
						<l n="28" num="9.4" lm="8" met="8"><w n="28.1" punct="vg:3">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>x</w>, <w n="28.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="28.3">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="28.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="28.5" punct="pe:8">r<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="10" type="distique" rhyme="aa">
						<l n="29" num="10.1" lm="8" met="8"><w n="29.1" punct="vg:2">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="29.2" punct="vg:4">B<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ddh<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="29.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="29.4">l</w>’<w n="29.5"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="15" weight="2" schema="CR">bl<rhyme label="a" id="15" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></rhyme></pgtc></w></l>
						<l n="30" num="10.2" lm="8" met="8"><w n="30.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="30.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="30.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="30.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="30.5">F<seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w>-<w n="30.6" punct="pt:8"><pgtc id="15" weight="2" schema="[CR">L<rhyme label="a" id="15" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="11" type="quatrain" rhyme="abba">
						<l n="31" num="11.1" lm="8" met="8"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="31.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="31.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="31.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="31.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<pgtc id="16" weight="0" schema="R"><rhyme label="a" id="16" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</rhyme></pgtc></w></l>
						<l n="32" num="11.2" lm="8" met="8"><w n="32.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="32.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="32.4">gr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="32.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<pgtc id="17" weight="0" schema="R"><rhyme label="b" id="17" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="33" num="11.3" lm="8" met="8"><w n="33.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="33.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="33.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="33.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="33.5">fl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="33.7" punct="vg:8">n<pgtc id="17" weight="0" schema="R"><rhyme label="b" id="17" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="34" num="11.4" lm="8" met="8"><w n="34.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="34.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="34.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="34.4" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<pgtc id="16" weight="0" schema="R"><rhyme label="a" id="16" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pe">ou</seg>r</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="12" type="distique" rhyme="aa">
						<l n="35" num="12.1" lm="8" met="8"><w n="35.1" punct="vg:2">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="35.2" punct="vg:4">B<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ddh<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg></w>, <w n="35.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="35.4">l</w>’<w n="35.5"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="18" weight="2" schema="CR">bl<rhyme label="a" id="18" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></rhyme></pgtc></w></l>
						<l n="36" num="12.2" lm="8" met="8"><w n="36.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="36.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="36.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg></w> <w n="36.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="36.5">F<seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w>-<w n="36.6" punct="pt:8"><pgtc id="18" weight="2" schema="[CR">L<rhyme label="a" id="18" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>