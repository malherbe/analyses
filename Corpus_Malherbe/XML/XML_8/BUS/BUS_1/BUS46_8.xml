<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DOMESTIQUES</head><div type="poem" key="BUS46" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique" schema="3(aabccb)" er_moy="1.11" er_max="6" er_min="0" er_mode="0(6/9)" er_moy_et="1.91">
					<head type="main">BERCEUSE</head>
					<opener>
						<salute>A SYLVIE, A LAURENCE.</salute>
					</opener>
					<lg n="1" type="sizain" rhyme="aabccb">
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="1.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="1.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="1.6" punct="vg:8">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>x</rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">P<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ts</w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="2.4">P<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r</w> <w n="2.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="2.6" punct="vg:8">d<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>x</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="3.1">G<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="3.3">fru<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>ts</w> <w n="3.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="3.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.6" punct="vg:8">br<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1" punct="vg:1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" punct="vg">e</seg></w>, <w n="4.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="4.5">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="4.6" punct="pt:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
						<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">J</w>’<w n="5.2"><seg phoneme="e" type="vs" value="1" rule="353" place="1">e</seg>ssu<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="5.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="5.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="5.6" punct="pt:8">y<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">b<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="6.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="6.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="341" place="9">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.9">s</w>’<w n="6.10"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>p<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					</lg>
					<lg n="2" type="sizain" rhyme="aabccb">
						<l n="7" num="2.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="7.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="7.4">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4">en</seg>s</w> <w n="7.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="7.7" punct="pt:8">d<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
						<l n="8" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="8.4">P<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r</w> <w n="8.5" punct="pt:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
						<l n="9" num="2.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="9.4" punct="pt:8"><pgtc id="5" weight="2" schema="[CR">r<rhyme label="b" id="5" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="10" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">j</w>’<w n="10.3"><seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="10.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="10.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.7" punct="pt:8"><pgtc id="6" weight="2" schema="[CR">pl<rhyme label="c" id="6" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</l>
						<l n="11" num="2.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="11.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg></w> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="11.6" punct="pt:8">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="6" weight="2" schema="CR">l<rhyme label="c" id="6" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>rs</rhyme></pgtc></w>.</l>
						<l n="12" num="2.6" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="12.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="12.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="12.5" punct="vg:6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="12.8">r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="12.9" punct="pe:12">m<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="5" weight="2" schema="CR">r<rhyme label="b" id="5" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" type="sizain" rhyme="aabccb">
						<l n="13" num="3.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="13.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="13.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="13.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.6" punct="pt:8">d<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
						<l n="14" num="3.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="14.3">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="14.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="14.5">j<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="14.6" punct="pt:8">y<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
						<l n="15" num="3.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="15.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="15.4" punct="vg:8">j<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="16" num="3.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="16.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="16.4" punct="vg:5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5" punct="vg">ein</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="16.5"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="16.6">ch<pgtc id="9" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="c" id="9" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></rhyme></pgtc></w></l>
						<l n="17" num="3.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="17.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">qu</w>’<w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="17.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="17.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>t</w> <w n="17.7" punct="pv:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>f<pgtc id="9" weight="6" schema="VCR"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<rhyme label="c" id="9" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pv">er</seg></rhyme></pgtc></w> ;</l>
						<l n="18" num="3.6" lm="12" met="6+6"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="18.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="18.3">n</w>’<w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="18.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="18.7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>x</w><caesura></caesura> <w n="18.8"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="18.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="18.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="18.11" punct="vg:10">c<seg phoneme="œ" type="vs" value="1" rule="249" place="10" punct="vg">œu</seg>r</w>, <w n="18.12" punct="pe:12">cr<seg phoneme="y" type="vs" value="1" rule="454" place="11" mp="M">u</seg><pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg>s</rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>