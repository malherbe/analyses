<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS DE TOUS LES PAYS</head><div type="poem" key="BUS38" modus="cp" lm_max="10" metProfile="6, 5, 3, 8, 4, 4+6" form="suite de strophes" schema="4[aa] 3[abbab] 2[abba] 1[abbaa]" er_moy="0.8" er_max="6" er_min="0" er_mode="0(13/20)" er_moy_et="1.44">
					<head type="main">LES GOUTTES DE PLUIE</head>
					<head type="sub_1">RÊVERIE IMITÉE DE l’ALLEMAND</head>
					<lg n="1" type="regexp" rhyme="aaabba">
						<l n="1" num="1.1" lm="6" met="6"><space unit="char" quantity="8"></space><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="1.2" punct="vg:2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2" punct="vg">oi</seg>r</w>, <w n="1.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="1.5" punct="dp:6">d<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="dp">eu</seg>x</rhyme></pgtc></w> :</l>
						<l n="2" num="1.2" lm="5" met="5"><space unit="char" quantity="10"></space><w n="2.1">B<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg>s</w> <w n="2.2" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="6" met="6"><space unit="char" quantity="8"></space><w n="3.1">V<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="3.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.4" punct="pt:6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="a" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="8"></space><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">l<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="4.4">d</w>’<w n="4.5" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rg<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="m" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
						<l n="5" num="1.5" lm="5" met="5"><space unit="char" quantity="10"></space><w n="5.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="5.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="5.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ge<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="m" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="5">an</seg>t</rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="6" met="6"><space unit="char" quantity="8"></space><w n="6.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>t</w> <w n="6.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="6.3"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="6.4" punct="pt:6">br<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="e" stanza="2"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="regexp" rhyme="babba">
						<l n="7" num="2.1" lm="6" met="6"><space unit="char" quantity="8"></space><w n="7.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="7.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="7.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="7.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="7.5" punct="vg:6">br<seg phoneme="y" type="vs" value="1" rule="445" place="5">û</seg>l<pgtc id="3" weight="0" schema="R"><rhyme label="b" id="3" gender="m" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
						<l n="8" num="2.2" lm="6" met="6"><space unit="char" quantity="8"></space><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="8.3" punct="vg:3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>x</w>, <w n="8.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="8.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="8.6" punct="vg:6">l<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="9" num="2.3" lm="3" met="3"><space unit="char" quantity="14"></space><w n="9.1">Pl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="9.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="9.3" punct="pt:3">ci<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="a" stanza="3"><seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
						<l n="10" num="2.4" lm="6" met="6"><space unit="char" quantity="8"></space><w n="10.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="10.2" punct="pt:6">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="e" stanza="3"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
						<l n="11" num="2.5" lm="6" met="6"><space unit="char" quantity="8"></space><w n="11.1" punct="vg:2">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="11.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="11.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="11.4" punct="pe:6">fi<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" type="regexp" rhyme="aaabbab">
						<l n="12" num="3.1" lm="6" met="6"><space unit="char" quantity="8"></space><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="12.5">m</w>’<w n="12.6" punct="pt:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="a" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="pt">en</seg>d</rhyme></pgtc></w>.</l>
						<l n="13" num="3.2" lm="5" met="5"><space unit="char" quantity="10"></space><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="13.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>sc<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="m" type="e" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d</rhyme></pgtc></w></l>
						<l n="14" num="3.3" lm="6" met="6"><space unit="char" quantity="8"></space><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="14.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="14.5" punct="dp:6">pl<pgtc id="7" weight="1" schema="GR">u<rhyme label="a" id="7" gender="f" type="a" stanza="5"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="15" num="3.4" lm="6" met="6"><space unit="char" quantity="8"></space><w n="15.1">G<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="15.2"><seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="15.3">fl<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>ts</w> <w n="15.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>r<pgtc id="8" weight="2" schema="CR">s<rhyme label="b" id="8" gender="m" type="a" stanza="5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</rhyme></pgtc></w></l>
						<l n="16" num="3.5" lm="5" met="5"><space unit="char" quantity="10"></space><w n="16.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="16.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="16.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="16.4" punct="vg:5">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg><pgtc id="8" weight="2" schema="CR">ss<rhyme label="b" id="8" gender="m" type="e" stanza="5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg">é</seg>s</rhyme></pgtc></w>,</l>
						<l n="17" num="3.6" lm="6" met="6"><space unit="char" quantity="8"></space><w n="17.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="353" place="5">e</seg>ss<pgtc id="7" weight="1" schema="GR">u<rhyme label="a" id="7" gender="f" type="e" stanza="5"><seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
						<l n="18" num="3.7" lm="3" met="3"><space unit="char" quantity="14"></space><w n="18.1" punct="pe:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>pr<seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg><pgtc id="8" weight="2" schema="CR">ss<rhyme label="b" id="8" gender="m" type="a" stanza="5"><seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="pe">é</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="4" type="regexp" rhyme="aaabba">
						<l n="19" num="4.1" lm="10" met="4+6"><w n="19.1">Qu</w>’<w n="19.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="19.3">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="19.5" punct="vg:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg">ou</seg>rs</w>, <w n="19.6" punct="vg:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>rs</w>, <w n="19.7" punct="pe:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>c<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="a" stanza="6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
						<l n="20" num="4.2" lm="6" met="6"><space unit="char" quantity="8"></space><w n="20.1">D<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>x</w> <w n="20.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="20.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="20.4">j</w>’<w n="20.5" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>d<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="f" type="e" stanza="6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="21" num="4.3" lm="8" met="8"><space unit="char" quantity="4"></space><w n="21.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="21.2"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="21.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="21.4" punct="pe:8">fr<pgtc id="10" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<rhyme label="a" id="10" gender="m" type="a" stanza="7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>s</rhyme></pgtc></w> !</l>
						<l n="22" num="4.4" lm="10" met="4+6"><w n="22.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="22.2" punct="vg:4">t<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg" caesura="1">ou</seg>rs</w>,<caesura></caesura> <w n="22.3" punct="vg:6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">ez</seg></w>, <w n="22.4">g<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="22.5" punct="vg:10">pr<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>p<pgtc id="11" weight="0" schema="R"><rhyme label="b" id="11" gender="f" type="a" stanza="7"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="23" num="4.5" lm="6" met="6"><space unit="char" quantity="8"></space><w n="23.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="23.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="23.4" punct="pt:6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>pl<pgtc id="11" weight="0" schema="R"><rhyme label="b" id="11" gender="f" type="e" stanza="7"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="24" num="4.6" lm="8" met="8"><space unit="char" quantity="4"></space><w n="24.1">L</w>’<w n="24.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="24.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="24.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="24.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="24.6" punct="pe:8">bu<pgtc id="10" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ss<rhyme label="a" id="10" gender="m" type="e" stanza="7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="5" type="regexp" rhyme="aaabbab">
						<l n="25" num="5.1" lm="6" met="6"><space unit="char" quantity="8"></space><w n="25.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="25.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="25.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="25.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5">a</seg><pgtc id="12" weight="1" schema="GR">y<rhyme label="a" id="12" gender="m" type="a" stanza="8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></rhyme></pgtc></w></l>
						<l n="26" num="5.2" lm="5" met="5"><space unit="char" quantity="10"></space><w n="26.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="26.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="26.3">pl<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg><pgtc id="12" weight="1" schema="GR">y<rhyme label="a" id="12" gender="m" type="e" stanza="8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></rhyme></pgtc></w></l>
						<l n="27" num="5.3" lm="6" met="6"><space unit="char" quantity="8"></space><w n="27.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="27.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="27.3" punct="pt:6">m<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>t<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="a" stanza="9"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="28" num="5.4" lm="6" met="6"><space unit="char" quantity="8"></space><w n="28.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="28.2">br<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="28.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="28.4" punct="pt:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg><pgtc id="14" weight="2" schema="CR">c<rhyme label="b" id="14" gender="m" type="a" stanza="9"><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pt">é</seg>s</rhyme></pgtc></w>.</l>
						<l n="29" num="5.5" lm="5" met="5"><space unit="char" quantity="10"></space><w n="29.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="29.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rds</w> <w n="29.3" punct="ps:5">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg><pgtc id="14" weight="2" schema="CR">ss<rhyme label="b" id="14" gender="m" type="e" stanza="9"><seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="ps">é</seg>s</rhyme></pgtc></w>…</l>
						<l n="30" num="5.6" lm="6" met="6"><space unit="char" quantity="8"></space><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="30.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="30.3">t</w>’<w n="30.4" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg>s</w>, <w n="30.5" punct="ps:6">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>gn<pgtc id="13" weight="0" schema="R"><rhyme label="a" id="13" gender="f" type="e" stanza="9"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ps">e</seg></rhyme></pgtc></w>…</l>
						<l n="31" num="5.7" lm="3" met="3"><space unit="char" quantity="14"></space><w n="31.1">J<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rs</w> <w n="31.2" punct="pe:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg><pgtc id="14" weight="2" schema="CR">ss<rhyme label="b" id="14" gender="m" type="a" stanza="9"><seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="pe">é</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="6" type="regexp" rhyme="abbaa">
						<l n="32" num="6.1" lm="4" met="4"><space unit="char" quantity="12"></space><w n="32.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="32.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="32.3" punct="pt:4">ci<pgtc id="15" weight="0" schema="R"><rhyme label="a" id="15" gender="m" type="a" stanza="10"><seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
						<l n="33" num="6.2" lm="6" met="6"><space unit="char" quantity="8"></space><w n="33.1" punct="vg:2">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="33.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="33.3" punct="pe:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<pgtc id="16" weight="0" schema="R"><rhyme label="b" id="16" gender="f" type="a" stanza="10"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="34" num="6.3" lm="4" met="4"><space unit="char" quantity="12"></space><w n="34.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="34.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="34.3">l</w>’<w n="34.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<pgtc id="16" weight="0" schema="R"><rhyme label="b" id="16" gender="f" type="e" stanza="10"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></rhyme></pgtc></w></l>
						<l n="35" num="6.4" lm="5" met="5"><space unit="char" quantity="10"></space><w n="35.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="35.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="35.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="35.4" punct="pe:5">h<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<pgtc id="15" weight="0" schema="R"><rhyme label="a" id="15" gender="m" type="e" stanza="10"><seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="pe">eu</seg>x</rhyme></pgtc></w> !</l>
						<l n="36" num="6.5" lm="4" met="4"><space unit="char" quantity="12"></space><w n="36.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="36.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="36.3" punct="pt:4">ci<pgtc id="15" weight="0" schema="R"><rhyme label="a" id="15" gender="m" type="a" stanza="10"><seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>