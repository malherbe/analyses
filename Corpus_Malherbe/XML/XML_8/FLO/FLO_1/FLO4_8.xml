<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><div type="poem" key="FLO4" modus="cp" lm_max="12" metProfile="8, 6+6, (4+6)" form="suite de strophes" schema="2[abab] 1[abaab] 1[aabba]" er_moy="0.0" er_max="0" er_min="0" er_mode="0(10/10)" er_moy_et="0.0">
					<head type="number">FABLE IV</head>
					<head type="main">Les deux Voyageurs</head>
					<lg n="1" type="regexp" rhyme="abababaabababaabba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.3">Th<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg></w> <w n="1.7">L<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="2.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4">e</seg>d</w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="2.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="2.8">v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.9" punct="pt:12">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>ch<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="3.1">Th<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="3.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="3.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></rhyme></pgtc></w></l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.4">l<seg phoneme="u" type="vs" value="1" rule="d-2" place="6">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>s</w> <w n="4.5" punct="pv:8">pl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4" punct="pt:6"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="6" punct="pt" caesura="1">ô</seg>t</w>.<caesura></caesura> <w n="5.5" punct="vg:8">L<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>b<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></w>, <w n="5.6">d</w>’<w n="5.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="5.8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r</w> <w n="5.9" punct="vg:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>t<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="ri_1" place="12" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
						<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="6.2" punct="dp:2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="dp">i</seg>t</w> : <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="6.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.6">b<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7" punct="pe:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>b<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
						<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, <w n="7.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d</w> <w n="7.3">Th<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="7.4" punct="vg:8">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
						<l n="8" num="1.8" lm="10" met="4+6" met_alone="True"><space unit="char" quantity="4"></space><hi rend="ital"><w n="8.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w></hi> <w n="8.3">n</w>’<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="8.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="8.7" punct="vg:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>t</w>,  <w n="8.8">c</w>’<w n="8.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="8.10" punct="pt:10">d<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
						<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">L<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="M">u</seg>b<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="9.4" punct="pv:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pv" caesura="1">u</seg>s</w> ;<caesura></caesura> <w n="9.5" punct="vg:7">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="vg">ai</seg>s</w>, <w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="9.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9" mp="M">i</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="9.9">pl<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="10.2">tr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="10.4">v<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="10.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg></w> <w n="10.7">b<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s</w> <w n="10.8" punct="pt:12">v<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>s<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg></rhyme></pgtc></w>.</l>
						<l n="11" num="1.11" lm="8" met="8"><space unit="char" quantity="8"></space><w n="11.1">Th<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="11.2" punct="vg:4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="11.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="11.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="11.6" punct="vg:8">c<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a" stanza="3"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1" punct="dp:1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="dp">i</seg>t</w> : <w n="12.2">N<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="12.3">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="12.4" punct="pe:6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pe" caesura="1">u</seg>s</w> !<caesura></caesura> <w n="12.5" punct="vg:7">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="vg">on</seg></w>, <w n="12.6">lu<seg phoneme="i" type="vs" value="1" rule="491" place="8" mp="C">i</seg></w> <w n="12.7">r<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>d</w> <w n="12.8" punct="vg:12">L<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>b<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></rhyme></pgtc></w>,</l>
						<l n="13" num="1.13" lm="12" met="6+6"><hi rend="ital"><w n="13.1" punct="pt:7">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w><hi rend="ital"> <w n="13.2" punct="pt:7">n</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="13.6">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="13.7" punct="vg:6">m<seg phoneme="o" type="vs" value="1" rule="438" place="6" punct="vg" caesura="1">o</seg>t</w>,<caesura></caesura> <w n="13.8">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> </hi><w n="13.9">t<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w></hi> <w n="13.10">c</w>’<w n="13.11"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="13.12"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="13.13" punct="pt:12">ch<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e" stanza="3"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="14.2" punct="vg:3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>t</w>, <w n="14.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="C">i</seg>l</w> <w n="14.4">s</w>’<w n="14.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="14.7">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>rs</w> <w n="14.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="14.9" punct="pt:12">t<seg phoneme="a" type="vs" value="1" rule="307" place="11" mp="M">a</seg>ill<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="a" stanza="4"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>s</rhyme></pgtc></w>.</l>
						<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1" mp="M">I</seg>mm<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="15.3" punct="vg:6">p<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="15.4">Th<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="15.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="10" mp="M">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="11">ô</seg>t</w> <w n="15.7" punct="pv:12">pr<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="e" stanza="4"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pv">i</seg>s</rhyme></pgtc></w> ;</l>
						<l n="16" num="1.16" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="16.2">t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="16.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="16.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="16.7" punct="pt:8">d<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="a" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="17" num="1.17" lm="12" met="6+6"><w n="17.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="17.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="17.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="17.4">qu</w>’<w n="17.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="17.6">s<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="17.7">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="17.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="17.9">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.10"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="11">e</seg>st</w> <w n="17.11">b<pgtc id="8" weight="0" schema="R"><rhyme label="b" id="8" gender="f" type="e" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="18" num="1.18" lm="8" met="8"><space unit="char" quantity="8"></space><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="18.4">n</w>’<w n="18.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="18.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>t</w> <w n="18.7">d</w>’<w n="18.8" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="a" stanza="4"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>s</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>