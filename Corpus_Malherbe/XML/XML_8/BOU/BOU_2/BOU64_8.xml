<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU64" modus="cp" lm_max="12" metProfile="6+6, (8)" form="suite périodique" schema="2(abab) 1(abaab)" er_moy="0.29" er_max="2" er_min="0" er_mode="0(6/7)" er_moy_et="0.7">
				<head type="main">VESTIGIA FLAMMÆ</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="1.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>c</w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="3" mp="Lp">e</seg>s</w>-<w n="1.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="1.5" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="482" place="6" punct="vg" caesura="1">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>,<caesura></caesura> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="1.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.8">j<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="1.9" punct="pi:12">f<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="2.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="2.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="2.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rd</w><caesura></caesura> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="2.9" punct="vg:10">v<seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="vg">oi</seg>x</w>, <w n="2.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="2.11" punct="vg:12">j<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="3.3"><seg phoneme="wa" type="vs" value="1" rule="420" place="3" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="3.4">qu</w>’<w n="3.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="3.7">bru<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="3.8">s<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="P">ou</seg>s</w> <w n="3.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="3.10" punct="vg:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rm<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="4.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="4.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="4.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="4.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="4.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="4.10">l</w>’<w n="4.11" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>r</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">An</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="5.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="5.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M/mp">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5" mp="Lp">en</seg>t</w>-<w n="5.4"><seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>l</w><caesura></caesura> <w n="5.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.7">t</w>’<w n="5.8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="9" mp="M">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="306" place="10">ai</seg></w> <w n="5.9">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="P">u</seg>r</w> <w n="5.10" punct="pi:12"><pgtc id="3" weight="2" schema="[CR">t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">j</w>’<w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="6.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="6.5">d<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="6.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="6.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="6.8">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="6.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="6.10" punct="pi:12">t<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pi">oi</seg></rhyme></pgtc></w> ?</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="Lc">o</seg>rsqu</w>’<w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg></w> <w n="7.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="7.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="7.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="7.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="7.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>ds</w> <w n="7.9" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">N</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1" mp="Lp">e</seg>st</w>-<w n="8.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="444" place="6" caesura="1">o</seg></w><caesura></caesura> <w n="8.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.7">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="8.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="8.10" punct="pi:12">m<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pi">oi</seg></rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="3" type="quintil" rhyme="abaab">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="Lp">ai</seg>s</w>-<w n="9.3" punct="vg:3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg></w>, <w n="9.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" mp="M">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="9.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="9.7">su<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>s</w> <w n="9.8">s<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>l</w> <w n="9.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="9.10">l</w>’<w n="9.11" punct="vg:12"><pgtc id="5" weight="0" schema="[R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="10.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>x</w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="10.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>s</w> <w n="10.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="10.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg></rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2" punct="vg:2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" punct="vg">e</seg></w>, <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="11.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="11.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="11.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" caesura="1">ain</seg>s</w><caesura></caesura> <w n="11.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="11.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="11.9">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>t</w> <w n="11.10" punct="vg:12">s<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.3" punct="vg:6">br<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="12.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="12.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="12.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="11" mp="P">an</seg>s</w> <w n="12.8" punct="vg:12">n<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="13" num="3.5" lm="8"><space unit="char" quantity="8"></space><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="13.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="13.5" punct="pi:8">f<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pi">eu</seg></rhyme></pgtc></w> ?</l>
				</lg>
			</div></body></text></TEI>