<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC47" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="5[aa] 5[abba]" er_moy="0.87" er_max="3" er_min="0" er_mode="0(8/15)" er_moy_et="1.02">
				<head type="main">Adriane</head>
				<lg n="1" type="regexp" rhyme="aaa">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.6" punct="vg:8">b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>s</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">C</w>’<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="2.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.5">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="2.6" punct="pt:8">f<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>s</rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="3.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="4">a</seg>y<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="3.4" punct="pt:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>p<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="regexp" rhyme="bba">
					<l n="4" num="2.1" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="y" type="vs" value="1" rule="d-3" place="3">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="4.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="4.5" punct="vg:8">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="7">a</seg><pgtc id="3" weight="3" schema="GR">y<rhyme label="b" id="3" gender="m" type="a" stanza="2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>s</rhyme></pgtc></w>,</l>
					<l n="5" num="2.2" lm="8" met="8"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="5.5">gr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="3" weight="3" schema="GR">ll<rhyme label="b" id="3" gender="m" type="e" stanza="2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</rhyme></pgtc></w></l>
					<l n="6" num="2.3" lm="8" met="8"><w n="6.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.2">j<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="403" place="4">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6">en</seg>t</w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="6.4">l</w>’<w n="6.5" punct="pv:8">h<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
				</lg>
				<lg n="3" type="regexp" rhyme="aaa">
					<l n="7" num="3.1" lm="8" met="8"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="7.3">s<seg phoneme="ɛ" type="vs" value="1" rule="384" place="4">ei</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="7.6" punct="vg:7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></w>, <w n="7.7" punct="pt:8"><pgtc id="4" weight="2" schema="CR">v<rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg>gt</rhyme></pgtc></w>.</l>
					<l n="8" num="3.2" lm="8" met="8"><w n="8.1" punct="vg:1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1" punct="vg">O</seg>r</w>, <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="8.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.4">qu</w>’<w n="8.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="8.6" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<pgtc id="4" weight="2" schema="CR">v<rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pv">in</seg>t</rhyme></pgtc></w> ;</l>
					<l n="9" num="3.3" lm="8" met="8"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">pr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="9.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="9.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="9.5" punct="vg:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>p<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="4" type="regexp" rhyme="bba">
					<l n="10" num="4.1" lm="8" met="8"><w n="10.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="10.6" punct="ps:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>g<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="ps">a</seg>rd</rhyme></pgtc></w>…</l>
					<l n="11" num="4.2" lm="8" met="8"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">sc<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.5">l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>z<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" stanza="4"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rd</rhyme></pgtc></w></l>
					<l n="12" num="4.3" lm="8" met="8"><w n="12.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">ch<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="12.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>il</w> <w n="12.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="12.6">l</w>’<w n="12.7" punct="pt:8">h<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="5" type="regexp" rhyme="aaa">
					<l n="13" num="5.1" lm="8" met="8"><w n="13.1">J</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd</w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.7" punct="vg:8">fl<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="a" stanza="5"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>r</rhyme></pgtc></w>,</l>
					<l n="14" num="5.2" lm="8" met="8"><w n="14.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="14.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rt</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="14.7" punct="pv:8">c<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="m" type="e" stanza="5"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="pv">œu</seg>r</rhyme></pgtc></w> ;</l>
					<l n="15" num="5.3" lm="8" met="8"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">m</w>’<w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="15.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="15.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="15.6">j<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="15.7" punct="pt:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>p<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="f" type="a" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="6" type="regexp" rhyme="bba">
					<l n="16" num="6.1" lm="8" met="8"><w n="16.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="16.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="16.4" punct="vg:4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>p</w>, <w n="16.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="16.6" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="9" weight="2" schema="CR">s<rhyme label="b" id="9" gender="m" type="a" stanza="6"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></pgtc></w>,</l>
					<l n="17" num="6.2" lm="8" met="8"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">pr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="17.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="17.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="17.5">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="17.7" punct="ps:8">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg><pgtc id="9" weight="2" schema="CR">s<rhyme label="b" id="9" gender="m" type="e" stanza="6"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="ps">er</seg></rhyme></pgtc></w>…</l>
					<l n="18" num="6.3" lm="8" met="8"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="18.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="467" place="5">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="18.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="18.6">l</w>’<w n="18.7" punct="pt:8">h<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="f" type="e" stanza="6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="7" type="regexp" rhyme="aaa">
					<l n="19" num="7.1" lm="8" met="8"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="19.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="19.3">d</w>’<w n="19.4"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="19.5">h<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="19.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="19.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<pgtc id="10" weight="1" schema="GR">i<rhyme label="a" id="10" gender="m" type="a" stanza="7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</rhyme></pgtc></w></l>
					<l n="20" num="7.2" lm="8" met="8"><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="20.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="20.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<pgtc id="10" weight="1" schema="GR">i<rhyme label="a" id="10" gender="m" type="e" stanza="7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>s</rhyme></pgtc></w>,</l>
					<l n="21" num="7.3" lm="8" met="8"><w n="21.1" punct="vg:1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1" punct="vg">eu</seg>ls</w>, <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="21.3">tr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="21.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="21.6" punct="pt:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>p<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="a" stanza="8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="8" type="regexp" rhyme="bba">
					<l n="22" num="8.1" lm="8" met="8"><w n="22.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="22.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="22.3">v<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="22.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="22.5">d</w>’<w n="22.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="22.7" punct="ps:8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg><pgtc id="12" weight="2" schema="CR">g<rhyme label="b" id="12" gender="m" type="a" stanza="8"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="ps">er</seg></rhyme></pgtc></w>…</l>
					<l n="23" num="8.2" lm="8" met="8"><w n="23.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="23.2" punct="vg:3"><seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3" punct="vg">eau</seg>x</w>, <w n="23.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="23.4">l</w>’<w n="23.5" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="12" weight="2" schema="CR">g<rhyme label="b" id="12" gender="m" type="e" stanza="8"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></pgtc></w>,</l>
					<l n="24" num="8.3" lm="8" met="8"><w n="24.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">b<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="24.3">g<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">aî</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6">en</seg>t</w> <w n="24.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="24.5">l</w>’<w n="24.6" punct="pt:8">h<pgtc id="11" weight="0" schema="R"><rhyme label="a" id="11" gender="f" type="e" stanza="8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="9" type="regexp" rhyme="aaa">
					<l n="25" num="9.1" lm="8" met="8"><w n="25.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="25.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="25.3">b<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="25.4">s</w>’<w n="25.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="384" place="5">ei</seg>gn<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="25.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="25.7" punct="vg:8">br<pgtc id="13" weight="1" schema="GR">u<rhyme label="a" id="13" gender="m" type="a" stanza="9"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>t</rhyme></pgtc></w>,</l>
					<l n="26" num="9.2" lm="8" met="8"><w n="26.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="26.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="26.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="26.4">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="26.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="26.6">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="26.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="26.8" punct="vg:8">n<pgtc id="13" weight="1" schema="GR">u<rhyme label="a" id="13" gender="m" type="e" stanza="9"><seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>t</rhyme></pgtc></w>,</l>
					<l n="27" num="9.3" lm="8" met="8"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="27.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="27.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="27.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="27.6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="27.7" punct="pe:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>p<pgtc id="14" weight="0" schema="R"><rhyme label="a" id="14" gender="f" type="a" stanza="10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="10" type="regexp" rhyme="bba">
					<l n="28" num="10.1" lm="8" met="8"><w n="28.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="28.2" punct="vg:2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="28.3">n</w>’<w n="28.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="28.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="28.6">m</w>’<w n="28.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg><pgtc id="15" weight="2" schema="CR">s<rhyme label="b" id="15" gender="m" type="a" stanza="10"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></pgtc></w>,</l>
					<l n="29" num="10.2" lm="8" met="8"><w n="29.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="29.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="29.3" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>rd</w>, <w n="29.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="29.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="29.6" punct="vg:8">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg><pgtc id="15" weight="2" schema="CR">s<rhyme label="b" id="15" gender="m" type="e" stanza="10"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></rhyme></pgtc></w>,</l>
					<l n="30" num="10.3" lm="8" met="8"><w n="30.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="30.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="30.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="30.4">c<seg phoneme="œ" type="vs" value="1" rule="345" place="5">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="30.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="30.6">l</w>’<w n="30.7" punct="pe:8">h<pgtc id="14" weight="0" schema="R"><rhyme label="a" id="14" gender="f" type="e" stanza="10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1865">1865</date>
					</dateline>
				</closer>
			</div></body></text></TEI>