<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><head type="sub_part">(1867-1870)</head><div type="poem" key="DUC71" modus="sp" lm_max="7" metProfile="7, 6, 2" form="suite de strophes" schema="5[abaab]" er_moy="0.4" er_max="2" er_min="0" er_mode="0(12/15)" er_moy_et="0.8">
					<head type="main">Il est malade !!!</head>
					<head type="tune">(Sur l’air : — Des Fraises)</head>
					<lg n="1" type="regexp" rhyme="ab">
						<head type="speaker">LES JOURNAUX OFFICIEUX</head>
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1" punct="pe:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="pe">on</seg></w> ! <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">I</seg>l</w> <w n="1.3">n</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="1.7" punct="vg:7">p<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>l</rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="2"></space><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="2.2" punct="vg:3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="2.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="2.4" punct="pt:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>gr<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="regexp" rhyme="a">
						<head type="speaker">LES PRÉFETS, LES GENDARMES, LES SACRISTAINS</head>
						<l n="3" num="2.1" lm="7" met="7"><foreign lang="LAT"><w n="3.1" punct="pe:3">T<seg phoneme="e" type="vs" value="1" rule="160" place="1">e</seg></w> <w n="3.2">D<seg phoneme="e" type="vs" value="1" rule="393" place="2">e</seg><seg phoneme="ɔ" type="vs" value="1" rule="451" place="3" punct="pe">u</seg>m</w></foreign> ! <w n="3.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="3.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>t</w>-<w n="3.5" punct="ps:7"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="ps">i</seg>l</rhyme></pgtc></w> …</l>
					</lg>
					<lg n="3" type="regexp" rhyme="a">
						<head type="speaker">LA FOULE</head>
						<l n="4" num="3.1" lm="7" met="7"><w n="4.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="4.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg></w>, <w n="4.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="5">en</seg>t</w> <w n="4.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w>-<w n="4.5"><pgtc id="1" weight="2" schema="[C[R" part="1">t</pgtc></w>-<w n="4.6" punct="pe:7"><pgtc id="1" weight="2" schema="[C[R" part="2"><rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pe">i</seg>l</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="4" type="regexp" rhyme="b">
						<head type="speaker">LA BOURSE</head>
						<l n="5" num="4.1" lm="2" met="2"><space unit="char" quantity="6"></space><w n="5.1">Ç<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="5.2" punct="pe:2">b<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pe in">e</seg></rhyme></pgtc></w> ! (<del hand="RR" reason="analysis" type="irrelevant">ter</del>) <ref type="noteAnchor" target="(1)">(1)</ref></l>
					</lg>
					<lg n="5" type="regexp" rhyme="ab">
						<head type="speaker">LA FOULE</head>
						<l n="6" num="5.1" lm="7" met="7"><w n="6.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="6.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="6.4"><seg phoneme="i" type="vs" value="1" rule="468" place="6">I</seg>l</w> <w n="6.5" punct="vg:7"><pgtc id="3" weight="0" schema="[R" part="1"><rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7" punct="vg">e</seg>st</rhyme></pgtc></w>,</l>
						<l n="7" num="5.2" lm="6" met="6"><space unit="char" quantity="2"></space><w n="7.1">Qu</w>’<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="7.3" punct="vg:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="7.4">qu</w>’<w n="7.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="7.6" punct="pt:6">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="f" type="a" stanza="2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ps pt">e</seg></rhyme></pgtc></w>….</l>
					</lg>
					<lg n="6" type="regexp" rhyme="aab">
						<head type="speaker">QUELQUES INTIMES</head>
						<l n="8" num="6.1" lm="7" met="7"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="8.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="8.3">qu</w>’<w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="8.5">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="8.6" punct="vg:7">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>scr<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="m" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="7" punct="vg">e</seg>t</rhyme></pgtc></w>,</l>
						<l n="9" num="6.2" lm="7" met="7"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="9.2">c<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="9.4" punct="vg:7">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg><pgtc id="3" weight="2" schema="CR" part="1">cr<rhyme label="a" id="3" gender="m" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="7" punct="vg">e</seg>t</rhyme></pgtc></w>,</l>
						<l n="10" num="6.3" lm="2" met="2"><space unit="char" quantity="6"></space><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2" punct="po:2">S<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="f" type="e" stanza="2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></pgtc></w> (<del hand="RR" reason="analysis" type="irrelevant">ter</del>)</l>
					</lg>
					<lg n="7" type="regexp" rhyme="abaab">
						<head type="speaker">LA VOIX DU MALADE</head>
						<l n="11" num="7.1" lm="7" met="7"><w n="11.1">L</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r</w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="11.4" punct="vg:3">l<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>rd</w>, <w n="11.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="11.7" punct="pe:7">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ls<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7" punct="pe ps">ain</seg></rhyme></pgtc></w> !…</l>
						<l n="12" num="7.2" lm="6" met="6"><space unit="char" quantity="2"></space><w n="12.1">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="12.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4" punct="vg:6">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>st<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="f" type="a" stanza="3"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="13" num="7.3" lm="7" met="7"><w n="13.1">R<seg phoneme="o" type="vs" value="1" rule="415" place="1">ô</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="13.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w> <w n="13.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="13.4" punct="pi:7">tr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rs<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="m" type="e" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="pi ps">in</seg></rhyme></pgtc></w> ?…</l>
						<l n="14" num="7.4" lm="7" met="7"><w n="14.1">Qu</w>’<w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="14.3">m</w>’<w n="14.4" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="14.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.6" punct="ps:7">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="m" type="a" stanza="3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="ps">in</seg></rhyme></pgtc></w>… <ref type="noteAnchor" target="2">(2)</ref></l>
						<l n="15" num="7.5" lm="2" met="2"><space unit="char" quantity="6"></space><w n="15.1">Ç<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="15.2" punct="po:2">p<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="f" type="e" stanza="3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3">e</seg></rhyme></pgtc></w> (<del hand="RR" reason="analysis" type="irrelevant">ter</del>)</l>
					</lg>
					<lg n="8" type="regexp" rhyme="abaababaa">
						<head type="speaker">LE PRINCE JÉRÔME NAPOLÉON</head>
						<l n="16" num="8.1" lm="7" met="7"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="16.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="16.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.4">p<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="m" type="a" stanza="4"><seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="vg">u</seg></rhyme></pgtc></w>,</l>
						<l n="17" num="8.2" lm="6" met="6"><space unit="char" quantity="2"></space><w n="17.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w>-<w n="17.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="17.4" punct="vg:6">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rs<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="f" type="a" stanza="4"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="18" num="8.3" lm="7" met="7"><w n="18.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="18.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="18.3">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="m" type="e" stanza="4"><seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="vg">u</seg></rhyme></pgtc></w>,</l>
						<l n="19" num="8.4" lm="6" met="6"><space unit="char" quantity="2"></space><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="19.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="19.4" punct="ps:6"><pgtc id="7" weight="2" schema="[CR" part="1">d<rhyme label="a" id="7" gender="m" type="a" stanza="4"><seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="ps">u</seg></rhyme></pgtc></w>…</l>
						<l n="20" num="8.5" lm="2" met="2"><space unit="char" quantity="6"></space><w n="20.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="20.2" punct="pe:2">tr<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="f" type="e" stanza="4"><seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pe in">e</seg></rhyme></pgtc></w> ! (<del hand="RR" reason="analysis" type="irrelevant">ter</del>) <ref type="noteAnchor" target="(3)">(3)</ref></l>
						<l n="21" num="8.6" lm="7" met="7"><w n="21.1">J</w>’<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="21.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="21.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="21.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>p</w> <w n="21.7" punct="vg:7">s<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="a" id="8" gender="m" type="a" stanza="5"><seg phoneme="y" type="vs" value="1" rule="445" place="7" punct="vg">û</seg>r</rhyme></pgtc></w>,</l>
						<l n="22" num="8.7" lm="6" met="6"><space unit="char" quantity="2"></space><w n="22.1" punct="vg:2">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="315" place="2" punct="vg">eau</seg>x</w>, <w n="22.2" punct="vg:3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3" punct="vg">œu</seg>rs</w>, <w n="22.3" punct="vg:5">p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="22.4" punct="vg:6">tr<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="b" id="9" gender="f" type="a" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>fl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="23" num="8.8" lm="7" met="7"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="23.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.4">fru<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="23.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="23.6" punct="vg:7">m<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="a" id="8" gender="m" type="e" stanza="5"><seg phoneme="y" type="vs" value="1" rule="445" place="7" punct="vg">û</seg>r</rhyme></pgtc></w>,</l>
						<l n="24" num="8.9" lm="7" met="7"><w n="24.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2">p<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="24.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="24.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="24.5" punct="ps:7">s<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="a" id="8" gender="m" type="a" stanza="5"><seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="ps">u</seg>r</rhyme></pgtc></w>…</l>
					</lg>
					<lg n="9" type="regexp" rhyme="b">
						<head type="speaker">GAVROCHE l’interrompant avec un geste bien connu :</head>
						<l n="25" num="9.1" lm="2" met="2"><space unit="char" quantity="6"></space><w n="25.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="25.2" punct="pe:2">n<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="b" id="9" gender="f" type="e" stanza="5"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>fl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pe in">e</seg>s</rhyme></pgtc></w> ! (<del hand="RR" reason="analysis" type="irrelevant">ter</del>)</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1869">Ier Septembre 1869</date>.
						</dateline>
						<note type="footnote" id="1">
							L’Officiel et les journaux officieux, en parlant de la maladie <lb></lb>
							de Napoléon III, essayaient de rassurer le public. Mais la <lb></lb>
							cote de la Bourse répondait chaque jour, par une baisse rapide, <lb></lb>
							aux affirmations des dits journaux et donnait la mesure de la <lb></lb>
							confiance et de la sécurité que l’on peut avoir et trouver dans <lb></lb>
							un gouvernement personnel, lorsque la fortune publique est à la <lb></lb>
							merci d’une colique royale ou impériale.
						</note>
						<note type="footnote" id="2">
							Ce cousin… C’est-à-dire Jérôme Bonaparte, qui depuis son <lb></lb>
							discours de Chalons, cherchait à renverser Napoléon III.
						</note>
						<note type="footnote" id="3">
							L’Empire tremble sur sa base ; un vent souffle qui menace de <lb></lb>
							le jeter à terre. Mais ce ne sera point pour ton profit, <lb></lb>
							ô Jérôme Bonaparte ! — Plus de trône, ni pour toi, ni pour <lb></lb>
							personne !
						</note>
					</closer>
				</div></body></text></TEI>