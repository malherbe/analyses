<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BOURDEAU DES NEUF PUCELLES</title>
				<title type="medium">Édition électronique</title>
				<author key="FRT">
					<name>
						<forename>Charles-Théophile</forename>
						<surname>FERET</surname>
					</name>
					<date from="1858" to="1928">1858-1928</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1111 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le Bourdeau des neuf pucelles</title>
						<author>Charles-Théophile Feret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/66781</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Bourdeau des neuf pucelles</title>
								<author>Charles-Théophile Feret</author>
								<imprint>
									<pubPlace>CAUDÉRAN-BORDEAUX</pubPlace>
									<publisher>ÉDITIONS DES CAHIERS LITTÉRAIRES</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1912">1912</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et les notes de l’éditeur n’ont pas été reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<p>Les notes de bas de page (numérotation conforme à l’édition imprimée) ont été reportées en fin de poème.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-12-09" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-12-10" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Vers pour <lb></lb>LES SERVANTES</head><div type="poem" key="FRT24" modus="cm" lm_max="12" metProfile="6−6" form="sonnet classique" schema="abba abba ccd cdc" er_moy="2.43" er_max="6" er_min="0" er_mode="2(4/7)" er_moy_et="1.68">
					<head type="main">Servante d’Auberge</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="1.2">b<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>nh<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="1.4">r<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>t</w><caesura></caesura> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="1.6">j<seg phoneme="u" type="vs" value="1" rule="426" place="8">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg></w> <w n="1.8" punct="vg:12">h<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>b<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="1" weight="2" schema="CR">r<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">S</w>’<w n="2.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="2.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="2.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="2.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="2.7">j<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="2.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="2.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="2.10" punct="vg:12">fr<pgtc id="2" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>m<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6">— <w n="3.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="3.3">n<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="3.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="3.5">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" caesura="1">ain</seg>t</w><caesura></caesura> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="3.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ls</w> <w n="3.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="10">um</seg>s</w> <w n="3.9" punct="tc:12">d<pgtc id="2" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>mm<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ti" mp="F">e</seg></rhyme></pgtc></w> —</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="P">a</seg>r</w> <w n="4.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453" place="6" caesura="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="4.8">gr<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="4.9" punct="pt:12">L<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">r<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="pt">o</seg>t</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="5.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="5.6">n<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6" caesura="1">im</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="5.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="5.10">f<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>x</w> <w n="5.11">d<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="3" weight="2" schema="CR">r<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg></rhyme></pgtc></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2" punct="vg:2">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" punct="vg">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg></w> <w n="6.4">Gl<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="6.5">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.7" punct="vg:9">r<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" punct="vg" mp="F">e</seg></w>, <w n="6.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.9" punct="pt:12"><seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
						<l n="7" num="2.3" lm="12" mp6="Pem" met="6−6"><w n="7.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="7.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="7.4">M<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem" caesura="1">e</seg></w><caesura></caesura> <w n="7.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="7.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="7.9" punct="pt:12"><pgtc id="4" weight="2" schema="[CR">M<rhyme label="b" id="4" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="8.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>cs</w><caesura></caesura> <w n="8.6">l<seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</w> <w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="8.8">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="8.9" punct="vg:12">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg><pgtc id="3" weight="2" schema="CR">rr<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="9.2">c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d</w> <w n="9.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="9.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="9.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.8" punct="ps:10">c<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="ps">eu</seg>r</w>… <w n="9.9" punct="pe:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="5" weight="2" schema="CR">ss<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="10.4">L<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>t</w><caesura></caesura> <w n="10.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="10.7" punct="vg:9">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" punct="vg">e</seg>rt</w>, <w n="10.8">h<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>t</w> <w n="10.9" punct="vg:12">tr<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="5" weight="2" schema="CR">ss<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">Ch<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fm">e</seg></w>-<w n="11.2" punct="vg:3">pl<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>t</w>, <w n="11.3">ch<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fm">e</seg></w>-<w n="11.4" punct="vg:6">l<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="11.6">r<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>ge<seg phoneme="o" type="vs" value="1" rule="314" place="9">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.7" punct="pt:12">L<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>s<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" rhyme="cdc">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="12.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="12.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="12.5" punct="vg:6">p<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="12.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="12.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="9">ain</seg></w> <w n="12.9" punct="vg:12">f<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>rv<seg phoneme="wa" type="vs" value="1" rule="440" place="11" mp="M">o</seg><pgtc id="7" weight="3" schema="GR">y<rhyme label="c" id="7" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="13.2">pr<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="4" mp="M">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="13.3">fou<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="13.5">l</w>’<w n="13.6"><seg phoneme="y" type="vs" value="1" rule="453" place="10" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ss<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></rhyme></pgtc></w></l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="14.2">fr<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>ct<seg phoneme="y" type="vs" value="1" rule="d-3" place="5" mp="M">u</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="14.6">f<seg phoneme="a" type="vs" value="1" rule="193" place="9">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="14.7" punct="pt:12">m<seg phoneme="u" type="vs" value="1" rule="428" place="11" mp="M">ou</seg><pgtc id="7" weight="3" schema="GR">ill<rhyme label="c" id="7" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>