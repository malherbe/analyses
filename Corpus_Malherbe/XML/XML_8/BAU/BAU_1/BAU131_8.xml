<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA MORT</head><div type="poem" key="BAU131" modus="cm" lm_max="10" metProfile="5+5" form="sonnet classique" schema="abab abab ccd ede" er_moy="0.86" er_max="2" er_min="0" er_mode="0(3/7)" er_moy_et="0.83">
					<head type="number">CXXVII</head>
					<head type="main">La Mort des amants</head>
					<lg n="1" rhyme="abab">
						<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="M">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="1.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>ts</w><caesura></caesura> <w n="1.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg>s</w> <w n="1.6">d</w>’<w n="1.7"><seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="1.8" punct="vg:10">l<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg><pgtc id="1" weight="2" schema="CR">g<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="2.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="2.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>ds</w><caesura></caesura> <w n="2.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="2.6" punct="vg:10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">om</seg><pgtc id="2" weight="2" schema="CR">b<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="vg">eau</seg>x</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">d</w>’<w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="3.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="P">u</seg>r</w> <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.7" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">g<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>cl<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="4.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>r</w> <w n="4.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>s</w><caesura></caesura> <w n="4.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="P">ou</seg>s</w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="4.6">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="4.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="4.8" punct="pt:10"><pgtc id="2" weight="2" schema="[CR">b<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="pt">eau</seg>x</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="abab">
						<l n="5" num="2.1" lm="10" met="5+5"><w n="5.1"><seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="M">U</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg></w><caesura></caesura> <w n="5.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" mp="C">eu</seg>rs</w> <w n="5.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>rs</w> <w n="5.7" punct="vg:10">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rn<pgtc id="3" weight="1" schema="GR">i<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1" mp="C">o</seg>s</w> <w n="6.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="6.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>rs</w> <w n="6.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>t</w><caesura></caesura> <w n="6.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="6.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="6.7" punct="vg:10">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">am</seg>b<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="vg">eau</seg>x</rhyme></pgtc></w>,</l>
						<l n="7" num="2.3" lm="10" met="5+5"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>fl<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>t</w><caesura></caesura> <w n="7.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" mp="C">eu</seg>rs</w> <w n="7.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="7.5">l<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<pgtc id="3" weight="1" schema="GR">i<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="10" met="5+5"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="8.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="2" mp="C">o</seg>s</w> <w n="8.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="8.4" punct="vg:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg" caesura="1">i</seg>ts</w>,<caesura></caesura> <w n="8.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="8.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>rs</w> <w n="8.7" punct="pt:10">j<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>m<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="pt">eau</seg>x</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" rhyme="ccd">
						<l n="9" num="3.1" lm="10" met="5+5"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="9.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="9.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="9.5">r<seg phoneme="o" type="vs" value="1" rule="444" place="5" caesura="1">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="9.8">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="9.9" punct="vg:10">m<seg phoneme="i" type="vs" value="1" rule="493" place="9" mp="M">y</seg>st<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>s</w><caesura></caesura> <w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r</w> <w n="10.5" punct="vg:10"><seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>n<pgtc id="5" weight="0" schema="R"><rhyme label="c" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="11" num="3.3" lm="10" met="5+5"><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="11.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g</w> <w n="11.4" punct="vg:5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438" place="5" punct="vg" caesura="1">o</seg>t</w>,<caesura></caesura> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="11.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="11.7">d</w>’<w n="11.8" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>d<pgtc id="6" weight="1" schema="GR">i<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pv">eu</seg>x</rhyme></pgtc></w> ;</l>
					</lg>
					<lg n="4" rhyme="ede">
						<l n="12" num="4.1" lm="10" met="5+5"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="12.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="12.5" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg" caesura="1">An</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="Lc">en</seg>tr</w>’<w n="12.7"><seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M/mc">ou</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="12.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="12.9" punct="vg:10">p<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="13" num="4.2" lm="10" met="5+5"><w n="13.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="13.2" punct="vg:5">r<seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="5" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="13.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="13.5" punct="vg:10">j<seg phoneme="wa" type="vs" value="1" rule="440" place="9" mp="M">o</seg><pgtc id="6" weight="1" schema="GR">y<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
						<l n="14" num="4.3" lm="10" met="5+5"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="14.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>rs</w> <w n="14.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>s</w><caesura></caesura> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="14.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="14.6">fl<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="14.7" punct="pt:10">m<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>