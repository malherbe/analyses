<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU67" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="11(aaa)" er_moy="4.95" er_max="14" er_min="0" er_mode="6(10/21)" er_moy_et="3.06">
					<head type="number">LXIII</head>
					<head type="main" lang="LAT">Francicæ meæ laudes</head>
					<lg n="1" type="tercet" rhyme="aaa">
						<l lang="LAT" n="1" num="1.1" lm="8" met="8"><w n="1.1">N<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="1.2">t<seg phoneme="e" type="vs" value="1" rule="BAU67_17" place="3">e</seg></w> <w n="1.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg></w> <w n="1.4" punct="vg:8">ch<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>r<pgtc id="1" weight="2" schema="CR">d<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</rhyme></pgtc></w>,</l>
						<l lang="LAT" n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="2.2">n<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="451" place="5">u</seg>m</w> <w n="2.3">qu<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>d</w> <w n="2.4">l<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg><pgtc id="1" weight="2" schema="CR">d<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</rhyme></pgtc></w></l>
						<l lang="LAT" n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg></w> <w n="3.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.3" punct="pt:8">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>r<pgtc id="1" weight="2" schema="CR">d<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="tercet" rhyme="aaa">
						<l lang="LAT" n="4" num="2.1" lm="8" met="8"><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>st<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg></w> <w n="4.2">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="4.3" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5">im</seg><pgtc id="2" weight="14" schema="CVCVCR">pl<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="a" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></pgtc></w>,</l>
						<l lang="LAT" n="5" num="2.2" lm="8" met="8"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="5.2">f<seg phoneme="e" type="vs" value="1" rule="250" place="2">œ</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg><pgtc id="2" weight="14" schema="CVCVCR">l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="a" id="2" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></rhyme></pgtc></w></l>
						<l lang="LAT" n="6" num="2.3" lm="8" met="8"><w n="6.1">P<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>r</w> <w n="6.2">qu<seg phoneme="a" type="vs" value="1" rule="145" place="2">a</seg>m</w> <w n="6.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>lv<seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="6.4" punct="pe:8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg><pgtc id="2" weight="8" schema="CVCR">cc<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="a" id="2" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="3" type="tercet" rhyme="aaa">
						<l lang="LAT" n="7" num="3.1" lm="8" met="8"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="7.2">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="451" place="6">u</seg>m</w> <w n="7.3" punct="vg:8">L<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg><pgtc id="3" weight="2" schema="CR">th<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="BAU67_1" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l lang="LAT" n="8" num="3.2" lm="8" met="8"><w n="8.1">H<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>r<seg phoneme="i" type="vs" value="1" rule="BAU67_4" place="2">i</seg><seg phoneme="a" type="vs" value="1" rule="145" place="3">a</seg>m</w> <w n="8.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>sc<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.4" punct="vg:8"><pgtc id="3" weight="2" schema="[CR">t<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="BAU67_17" place="8" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l lang="LAT" n="9" num="3.3" lm="8" met="8"><w n="9.1">Qu<seg phoneme="e" type="vs" value="1" rule="272" place="1">æ</seg></w> <w n="9.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2">im</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="5">e</seg>s</w> <w n="9.4" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="BAU67_2" place="7">e</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="BAU67_3" place="8" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="tercet" rhyme="aaa">
						<l lang="LAT" n="10" num="4.1" lm="8" met="8"><w n="10.1">Qu<seg phoneme="ɔ" type="vs" value="1" rule="451" place="1">u</seg>m</w> <w n="10.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="i" type="vs" value="1" rule="BAU67_5" place="3">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="451" place="5">u</seg>m</w> <w n="10.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>s<pgtc id="4" weight="2" schema="CR">t<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</rhyme></pgtc></w></l>
						<l lang="LAT" n="11" num="4.2" lm="8" met="8"><w n="11.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rb<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t</w> <w n="11.2"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="11.3" punct="vg:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
						<l lang="LAT" n="12" num="4.3" lm="8" met="8"><w n="12.1" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="u" type="vs" value="1" rule="BAU67_6" place="3">u</seg><seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg></w>, <w n="12.2" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="BAU67_7" place="6">e</seg><pgtc id="4" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="497" place="7">i</seg>t<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
					</lg>
					<lg n="5" type="tercet" rhyme="aaa">
						<l lang="LAT" n="13" num="5.1" lm="8" met="8"><w n="13.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="13.2">st<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>t<pgtc id="5" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</rhyme></pgtc></w></l>
						<l lang="LAT" n="14" num="5.2" lm="8" met="8"><w n="14.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg></w> <w n="14.2">n<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>fr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="i" type="vs" value="1" rule="497" place="4">i</seg><seg phoneme="i" type="vs" value="1" rule="497" place="5">i</seg>s</w> <w n="14.3" punct="ps:8"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<pgtc id="5" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="ps">i</seg>s</rhyme></pgtc></w>…</l>
						<l lang="LAT" n="15" num="5.3" lm="8" met="8"><w n="15.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="a" type="vs" value="1" rule="145" place="3">a</seg>m</w> <w n="15.2">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="15.3">t<seg phoneme="u" type="vs" value="1" rule="BAU67_16" place="5">u</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="15.4" punct="pe:8"><pgtc id="5" weight="6" schema="[VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="6" type="tercet" rhyme="aaa">
						<l lang="LAT" n="16" num="6.1" lm="8" met="8"><w n="16.1">P<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>sc<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="16.2">pl<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="16.3" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<pgtc id="6" weight="8" schema="CVCR">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</rhyme></pgtc></w>,</l>
						<l lang="LAT" n="17" num="6.2" lm="8" met="8"><w n="17.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>s</w> <w n="17.2"><seg phoneme="e" type="vs" value="1" rule="272" place="2">æ</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rn<seg phoneme="e" type="vs" value="1" rule="272" place="4">æ</seg></w> <w n="17.3" punct="vg:8">j<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg><pgtc id="6" weight="8" schema="CVCR">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t<rhyme label="a" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</rhyme></pgtc></w>,</l>
						<l lang="LAT" n="18" num="6.3" lm="8" met="8"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="18.2">v<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="361" place="4">e</seg>m</w> <w n="18.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>dd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="18.4" punct="pe:8">m<pgtc id="6" weight="6" schema="VCR"><seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="7" type="tercet" rhyme="aaa">
						<l lang="LAT" n="19" num="7.1" lm="8" met="8"><w n="19.1">Qu<seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">o</seg>d</w> <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="BAU67_17" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t</w> <w n="19.3" punct="vg:5">sp<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rc<seg phoneme="ɔ" type="vs" value="1" rule="451" place="5" punct="vg">u</seg>m</w>, <w n="19.4" punct="pv:8">cr<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<pgtc id="7" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>st<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg></rhyme></pgtc></w> ;</l>
						<l lang="LAT" n="20" num="7.2" lm="8" met="8"><w n="20.1">Qu<seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">o</seg>d</w> <w n="20.2" punct="vg:4">r<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>d<seg phoneme="i" type="vs" value="1" rule="BAU67_8" place="3">i</seg><seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg>s</w>, <w n="20.3" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="BAU67_9" place="5">e</seg>x<seg phoneme="ae" type="vs" value="1" rule="BAU67_10" place="6">æ</seg>qu<pgtc id="7" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>st<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg></rhyme></pgtc></w> ;</l>
						<l lang="LAT" n="21" num="7.3" lm="8" met="8"><w n="21.1">Qu<seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">o</seg>d</w> <w n="21.2" punct="vg:4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="21.3" punct="pe:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>rm<pgtc id="7" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>st<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg></rhyme></pgtc></w> !</l>
					</lg>
					<lg n="8" type="tercet" rhyme="aaa">
						<l lang="LAT" n="22" num="8.1" lm="8" met="8"><w n="22.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg></w> <w n="22.2">f<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="22.3">m<seg phoneme="e" type="vs" value="1" rule="BAU67_11" place="4">e</seg><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="22.4" punct="vg:8">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<pgtc id="8" weight="6" schema="VCR"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rn<rhyme label="a" id="8" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></pgtc></w>,</l>
						<l lang="LAT" n="23" num="8.2" lm="8" met="8"><w n="23.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">In</seg></w> <w n="23.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="23.3">m<seg phoneme="e" type="vs" value="1" rule="BAU67_11" place="4">e</seg><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="23.4" punct="vg:8">l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>c<pgtc id="8" weight="6" schema="VCR"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rn<rhyme label="a" id="8" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></pgtc></w>,</l>
						<l lang="LAT" n="24" num="8.3" lm="8" met="8"><w n="24.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="24.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="24.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="24.4" punct="pt:8">g<seg phoneme="y" type="vs" value="1" rule="448" place="6">u</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rn<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="9" type="tercet" rhyme="aaa">
						<l lang="LAT" n="25" num="9.1" lm="8" met="8"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>dd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="25.2">n<seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg>c</w> <w n="25.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="25.4" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<pgtc id="10" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>b<rhyme label="a" id="10" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>s</rhyme></pgtc></w>,</l>
						<l lang="LAT" n="26" num="9.2" lm="8" met="8"><w n="26.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>lc<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="26.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ln<seg phoneme="e" type="vs" value="1" rule="BAU67_12" place="4">e</seg><seg phoneme="u" type="vs" value="1" rule="BAU67_13" place="5">u</seg>m</w> <w n="26.3">su<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<pgtc id="10" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>b<rhyme label="a" id="10" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</rhyme></pgtc></w></l>
						<l lang="LAT" n="27" num="9.3" lm="8" met="8"><w n="27.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg>gu<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="451" place="4">u</seg>m</w> <w n="27.2" punct="pe:8"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<pgtc id="10" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>b<rhyme label="a" id="10" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pe">u</seg>s</rhyme></pgtc></w> !</l>
					</lg>
					<lg n="10" type="tercet" rhyme="aaa">
						<l lang="LAT" n="28" num="10.1" lm="8" met="8"><w n="28.1">M<seg phoneme="e" type="vs" value="1" rule="BAU67_14" place="1">e</seg><seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="28.2">c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>rc<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="28.3">l<seg phoneme="y" type="vs" value="1" rule="463" place="5">u</seg>mb<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="28.4" punct="vg:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<pgtc id="9" weight="0" schema="R"><rhyme label="a" id="9" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></pgtc></w>,</l>
						<l lang="LAT" n="29" num="10.2" lm="8" met="8"><w n="29.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="29.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="29.3" punct="vg:8">l<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<pgtc id="11" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<rhyme label="a" id="11" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></pgtc></w>,</l>
						<l lang="LAT" n="30" num="10.3" lm="8" met="8"><w n="30.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>qu<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="30.2">t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>ct<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="30.3" punct="pv:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ph<pgtc id="11" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<rhyme label="a" id="11" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg></rhyme></pgtc></w> ;</l>
					</lg>
					<lg n="11" type="tercet" rhyme="aaa">
						<l lang="LAT" n="31" num="11.1" lm="8" met="8"><w n="31.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="31.2">g<seg phoneme="ɛ" type="vs" value="1" rule="366" place="4">e</seg>mm<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="31.3" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg><pgtc id="12" weight="2" schema="CR">sc<rhyme label="a" id="12" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></pgtc></w>,</l>
						<l lang="LAT" n="32" num="11.2" lm="8" met="8"><w n="32.1">P<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="32.2" punct="vg:4">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ls<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg>s</w>, <w n="32.3">m<seg phoneme="o" type="vs" value="1" rule="435" place="5">o</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="32.4" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg><pgtc id="12" weight="2" schema="CR">sc<rhyme label="a" id="12" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></rhyme></pgtc></w>,</l>
						<l lang="LAT" n="33" num="11.3" lm="8" met="8"><w n="33.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="451" place="3">u</seg>m</w> <w n="33.2" punct="vg:5">v<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="451" place="5" punct="vg">u</seg>m</w>, <w n="33.3" punct="pe:8">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="12" weight="2" schema="CR">sc<rhyme label="a" id="12" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg></rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>