<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUPPLÉMENT AUX FLEURS DU MAL</head><div type="poem" key="BAU148" modus="sm" lm_max="8" metProfile="8" form="sonnet irrégulier" schema="aaa bbb abaa cacc" er_moy="1.5" er_max="6" er_min="0" er_mode="0(4/8)" er_moy_et="1.94">
					<head type="number">XII</head>
					<head type="main">Bien loin d’ici</head>
					<lg n="1" rhyme="aaa">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="1.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.6">s<pgtc id="1" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>cr<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="2.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="2.5" punct="vg:8">p<pgtc id="1" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="2">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="3.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="3.4" punct="vg:8">pr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					</lg>
					<lg n="2" rhyme="bbb">
						<l n="4" num="2.1" lm="8" met="8"><w n="4.1">D</w>’<w n="4.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="4.6" punct="vg:8"><pgtc id="3" weight="2" schema="[CR">s<rhyme label="b" id="3" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8" punct="vg">ein</seg>s</rhyme></pgtc></w>,</l>
						<l n="5" num="2.2" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="5.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="5.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="5.6" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="3" weight="2" schema="CR">ss<rhyme label="b" id="3" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg>s</rhyme></pgtc></w>,</l>
						<l n="6" num="2.3" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="6.4" punct="dp:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ss<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="dp">in</seg>s</rhyme></pgtc></w> :</l>
					</lg>
					<lg n="3" rhyme="abaa">
						<l n="7" num="3.1" lm="8" met="8"><w n="7.1">C</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.6" punct="pt:8">D<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>th<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
						<l n="8" num="3.2" lm="8" met="8">— <w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="8.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="8.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="8.8">l<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg></rhyme></pgtc></w></l>
						<l n="9" num="3.3" lm="8" met="8"><w n="9.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="9.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>ts</w> <w n="9.5">h<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="10" num="3.4" lm="8" met="8"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="10.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="10.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="10.5" punct="pt:8">g<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg><pgtc id="5" weight="2" schema="CR">t<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" rhyme="cacc">
						<l n="11" num="4.1" lm="8" met="8"><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="11.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="11.4" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</w>, <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="11.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="11.7" punct="vg:8">s<pgtc id="6" weight="0" schema="R"><rhyme label="c" id="6" gender="m" type="a"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="vg">oin</seg></rhyme></pgtc></w>,</l>
						<l n="12" num="4.2" lm="8" met="8"><w n="12.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2">p<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="12.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="12.5">fr<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg><pgtc id="5" weight="2" schema="CR">tt<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="13" num="4.3" lm="8" met="8"><w n="13.1">D</w>’<w n="13.2">hu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.6" punct="pt:8">b<seg phoneme="ɛ̃" type="vs" value="1" rule="238" place="7">en</seg>j<pgtc id="6" weight="0" schema="R"><rhyme label="c" id="6" gender="m" type="e"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="pt">oin</seg></rhyme></pgtc></w>.</l>
						<l n="14" num="4.4" lm="8" met="8">— <w n="14.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="14.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="14.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="14.7" punct="pt:8">c<pgtc id="6" weight="0" schema="R"><rhyme label="c" id="6" gender="m" type="a"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="pt">oin</seg></rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>