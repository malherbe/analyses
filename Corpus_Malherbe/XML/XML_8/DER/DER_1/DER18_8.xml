<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER18" modus="cm" lm_max="12" metProfile="6=6" form="suite de distiques" schema="6((aa))" er_moy="2.33" er_max="8" er_min="0" er_mode="2(3/6)" er_moy_et="2.69">
				<head type="number">XVIII</head>
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="12" mp6="F" met="4+4+4"><w n="1.1">Vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="1">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="1.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" caesura="2">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w><caesura></caesura> <w n="1.6" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>rqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="2.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="2.5">c</w>’<w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="2.8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="9">ain</seg></w> <w n="2.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="2.10">t<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="2.11">t</w>’<w n="2.12" punct="pt:12"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="pe:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="M">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="pe">ou</seg>r</w> ! <w n="3.2">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="3.3">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg>s</w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="3.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rts</w><caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="3.8">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.9">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="11" mp="M">oin</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="12">ain</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">n</w>’<w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="4.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="4.6">c<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="4.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="4.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="4.9"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="4.10">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12">in</seg></rhyme></pgtc></w></l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">f<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>nt</w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="5.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="5.6">l<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>z<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="5.8" punct="vg:12"><pgtc id="3" weight="2" schema="[CR">pl<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="6.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="6.4">r<seg phoneme="u" type="vs" value="1" rule="428" place="6" caesura="1">ou</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="6.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="6.7" punct="pt:12">p<seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg><pgtc id="3" weight="2" schema="CR">pl<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">c<seg phoneme="ø" type="vs" value="1" rule="398" place="2" mp="Lp">eu</seg>x</w>-<w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="7.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="M">eu</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="7.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="8">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="7.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg><pgtc id="4" weight="8" schema="CVCR">cr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>ch<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="8.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="8.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="8.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.8">qu</w>’<w n="8.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="8.10"><pgtc id="4" weight="8" schema="[CVCR">r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>ch<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></rhyme></pgtc></w></l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">Qu</w>’<w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ls</w> <w n="9.3" punct="pe:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="pe" mp="F">e</seg>nt</w> ! <w n="9.4">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="9.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg>s</w><caesura></caesura> <w n="9.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="9.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>cs</w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="9.9" punct="pt:12">t<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="5" weight="2" schema="CR">r<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">Oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="10.3">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="10.5">l</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r</w><caesura></caesura> <w n="10.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="10.8">n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="10.9" punct="pt:12">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>g<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="5" weight="2" schema="CR">r<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">Qu</w>’<w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ls</w> <w n="11.3" punct="pe:3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="pe" mp="F">e</seg>nt</w> ! <w n="11.4">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">Oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="11.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="11.8">n<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rgu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="11.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="11.10">p<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>r<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l</rhyme></pgtc></w></l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="12.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5" mp="C">eu</seg>rs</w> <w n="12.3">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">om</seg>bs</w><caesura></caesura> <w n="12.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="12.6">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10">ain</seg>s</w> <w n="12.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="12.8" punct="pt:12">m<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>l</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>