<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER95" modus="cm" lm_max="12" metProfile="6+6" form="suite de distiques" schema="4((aa))" er_moy="2.0" er_max="2" er_min="2" er_mode="2(4/4)" er_moy_et="0.0">
				<head type="number">XCV</head>
				<opener>
					<salute>A Guillaume Apollinaire.</salute>
				</opener>
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="1.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>rr<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="1.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="1.5">p<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.7">f<seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="M">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="1.9">s<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="2.5">r<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg>s</w><caesura></caesura> <w n="2.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7">l</w>’<w n="2.8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r</w> <w n="2.9">ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.10" punct="pt:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="1" weight="2" schema="CR">l<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="3.2">f<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="3.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>l</w><caesura></caesura> <w n="3.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>c</w> <w n="3.8">d<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="2" weight="2" schema="CR">c<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3" punct="vg:4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="4.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="4.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="4.7" punct="vg:10">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="vg" mp="F">e</seg>s</w>, <w n="4.8">tr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg><pgtc id="2" weight="2" schema="CR">ss<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>r</rhyme></pgtc></w></l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="5.2">r<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="5.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="5.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="5.7">r<seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="5.9" punct="pt:12">t<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="3" weight="2" schema="CR">nn<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="6.5" punct="vg:6">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg></w>,<caesura></caesura> <w n="6.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="6.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>r</w> <w n="6.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">A</seg>p<seg phoneme="o" type="vs" value="1" rule="435" place="10" mp="M">o</seg>ll<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg><pgtc id="3" weight="2" schema="CR">n<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="7.2">b<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="7.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="7.4">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>x</w><caesura></caesura> <w n="7.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6">g<seg phoneme="u" type="vs" value="1" rule="425" place="8">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="7.7">s<seg phoneme="œ" type="vs" value="1" rule="407" place="10" mp="M">eu</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12">en</seg>t</rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">C<seg phoneme="ø" type="vs" value="1" rule="398" place="1" mp="Lp">eu</seg>x</w>-<w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="8.3">qu</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="8.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="8.7"><seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.8" punct="pt:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="12" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>