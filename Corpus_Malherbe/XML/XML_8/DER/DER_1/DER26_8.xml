<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER26" modus="cm" lm_max="12" metProfile="6=6" form="suite de distiques" schema="6((aa))" er_moy="1.67" er_max="6" er_min="0" er_mode="0(3/6)" er_moy_et="2.13">
				<head type="number">XXVI</head>
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="12" mp6="M" met="4+4+4"><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="1.2" punct="dp:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="dp" caesura="1">ai</seg>t</w> :<caesura></caesura> <w n="1.3">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="1.4">b<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="1.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="8" caesura="2">en</seg>t</w><caesura></caesura> <w n="1.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="1.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="1.8">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>t</w> <w n="1.9"><pgtc id="1" weight="2" schema="[C[R" part="1">d</pgtc></w>’<w n="1.10" punct="pt:12"><pgtc id="1" weight="2" schema="[C[R" part="2"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="426" place="12" punct="pt">où</seg></rhyme></pgtc></w>.</l>
					<l n="2" num="1.2" lm="12" mp6="F" met="4+4+4"><w n="2.1">B<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ts</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">br<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" caesura="1">e</seg>t</w><caesura></caesura> <w n="2.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="2.6">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8" caesura="2">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w><caesura></caesura> <w n="2.7">d</w>’<w n="2.8"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="1" weight="2" schema="CR" part="1">d<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="426" place="12">ou</seg></rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">f<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="3.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" mp="M">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="3.7">b<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="445" place="12">û</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="pt:2">R<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" punct="pt" mp="F">e</seg>s</w>. <w n="4.2">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="4.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="4.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="4.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="4.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="4.8">cr<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="C">u</seg></w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="5.4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="5.6">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rd</w><caesura></caesura> <w n="5.7">t<seg phoneme="o" type="vs" value="1" rule="435" place="7" mp="M">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="5.9" punct="pt:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="3" weight="2" schema="CR" part="1">m<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg>s</rhyme></pgtc></w>.</l>
					<l n="6" num="1.6" lm="12" mp7="Fc" met="4+4+4"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="6.4">ch<seg phoneme="o" type="vs" value="1" rule="318" place="4" caesura="1">au</seg>d</w><caesura></caesura> <w n="6.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="Fc">e</seg></w> <w n="6.7">p<seg phoneme="i" type="vs" value="1" rule="468" place="8" caesura="2">i</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg></w> <w n="6.9">cr<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="6.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="6.11"><pgtc id="3" weight="2" schema="[CR" part="1">m<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="12">ain</seg>s</rhyme></pgtc></w></l>
					<l n="7" num="1.7" lm="12" mp6="C" met="6−6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="7.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="7.4">v<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t</w> <w n="7.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C" caesura="1">a</seg></w><caesura></caesura> <w n="7.7">tr<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352" place="8">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.8">f<seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>li<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="8.2">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="8.4">qu</w>’<w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="8.7" punct="pt:12">l<seg phoneme="i" type="vs" value="1" rule="dc-1" place="11" mp="M">i</seg><pgtc id="4" weight="0" schema="R" part="1"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="9.2" punct="pt:3">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pt" mp="F">e</seg></w>. <w n="9.3" punct="vg:4">M<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" punct="vg">e</seg>ts</w>, <w n="9.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="9.5" punct="vg:6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="9.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.7">pi<seg phoneme="e" type="vs" value="1" rule="241" place="8">e</seg>ds</w> <w n="9.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="P">u</seg>r</w> <w n="9.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="9.10" punct="pt:12">ch<pgtc id="5" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="pt">e</seg>ts</rhyme></pgtc></w>.</l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1" punct="pt:2">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="pt" mp="F">e</seg></w>. <w n="10.2">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="10.3">f<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="10.4">j<seg phoneme="a" type="vs" value="1" rule="307" place="5" mp="M">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="10.6">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="10.8" punct="pt:12">g<pgtc id="5" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12" punct="pt">ê</seg>ts</rhyme></pgtc></w>.</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fm">e</seg></w>-<w n="11.2" punct="vg:3">l<seg phoneme="a" type="vs" value="1" rule="342" place="3" punct="vg">à</seg></w>, <w n="11.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353" place="5" mp="M">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="11.4"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.6" punct="pt:12">t<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>l<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="12.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="12.3">s</w>’<w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5" mp="M">ein</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="12.5">s</w>’<w n="12.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="12.7">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="12.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="12.9">t<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="12.10" punct="pt:12">p<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>