<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER32" modus="cp" lm_max="14" metProfile="8, 6+8, (4+6), 6−6, (6)" form="suite de strophes" schema="3[abab]" er_moy="2.0" er_max="6" er_min="0" er_mode="1(2/6)" er_moy_et="1.91">
				<head type="number">XXXII</head>
				<lg n="1" type="regexp" rhyme="abababababab">
					<l n="1" num="1.1" lm="14" met="6+8"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="1.3">d</w>’<w n="1.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>ct<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="1.6">l<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rd</w><caesura></caesura> <w n="1.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="1.9">l<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="1.10" punct="vg:14">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="12" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="13" mp="Mem">e</seg>l<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="y" type="vs" value="1" rule="450" place="14">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="15" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6" met_alone="True"><space quantity="8" unit="char"></space><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">j<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="2.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>s</w><caesura></caesura> <w n="2.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="2.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="2.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="2.7">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="2.8" punct="pv:10">v<pgtc id="2" weight="6" schema="VCR"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rr<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pv">on</seg>t</rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><space quantity="12" unit="char"></space><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">n</w>’<w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="3.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="3.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="3.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><space quantity="12" unit="char"></space><w n="4.1">S</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="4.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="4.6" punct="pt:8">p<pgtc id="2" weight="6" schema="VCR"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="14" met="6+8"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="5.2">n</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" mp="M">aî</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="5.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="5.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7" mp="M">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="5.6">qu</w>’<w n="5.7"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="5.9">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" mp="M">in</seg>t<seg phoneme="a" type="vs" value="1" rule="341" place="13" mp="M">a</seg>n<pgtc id="3" weight="1" schema="GR">i<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="14">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="15" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="8" met="8"><space quantity="12" unit="char"></space><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="6.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="6.6" punct="pv:8">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="7">eu</seg><pgtc id="4" weight="2" schema="CR">r<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg>s</rhyme></pgtc></w> ;</l>
					<l n="7" num="1.7" lm="8" met="8"><space quantity="12" unit="char"></space><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">j<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="7.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="7.6">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<pgtc id="3" weight="1" schema="GR">i<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="12" met="6+6"><space quantity="4" unit="char"></space><w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">s</w>’<w n="8.3"><seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="8.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w><caesura></caesura> <w n="8.6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="M">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="8.9" punct="pt:12"><pgtc id="4" weight="2" schema="[CR">br<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>s</rhyme></pgtc></w>.</l>
					<l n="9" num="1.9" lm="12" met="6+6"><space quantity="4" unit="char"></space><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="9.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.4">br<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="9.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.7">f<seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>l<pgtc id="5" weight="1" schema="GR">i<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="10" num="1.10" lm="8" met="8"><space quantity="12" unit="char"></space><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.4">r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="10.5" punct="pv:8">j<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="6" weight="2" schema="CR">m<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pv">ai</seg>s</rhyme></pgtc></w> ;</l>
					<l n="11" num="1.11" lm="12" mp6="C" met="6−6"><space quantity="4" unit="char"></space><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="11.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C" caesura="1">e</seg></w><caesura></caesura> <w n="11.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>ps</w> <w n="11.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="11.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="11.8" punct="vg:12">p<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>ss<pgtc id="5" weight="1" schema="GR">i<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="1.12" lm="6"><space quantity="20" unit="char"></space><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="12.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="12.5" punct="pt:6">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>r<pgtc id="6" weight="2" schema="CR">m<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="pt">é</seg>s</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>