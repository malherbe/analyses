<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER126" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="7(abab)" er_moy="1.29" er_max="2" er_min="0" er_mode="2(9/14)" er_moy_et="0.96">
				<head type="number">CXXVI</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="1.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="1.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="1.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>x</w> <w n="1.7" punct="vg:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="1" weight="2" schema="CR">v<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="2.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="2">ein</seg>s</w> <w n="2.3">qu</w>’<w n="2.4"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.5">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.6" punct="vg:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="2" weight="2" schema="CR">c<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="3.4" punct="vg:6">m<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="3.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="3.6"><pgtc id="1" weight="2" schema="[CR">v<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="4.4" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg><pgtc id="2" weight="2" schema="CR">c<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="5.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="5.3" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>rd</w>, <w n="5.4">s</w>’<w n="5.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="5.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.7" punct="vg:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>t</rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3" punct="vg:4">r<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="6.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="6.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="6.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>gr<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="7.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rts</w> <w n="7.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="7.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>spr<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="8.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg></w> <w n="8.3">d</w>’<w n="8.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="8.5" punct="pt:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>cr<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">H<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="9.2" punct="vg:4">ch<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="9.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="9.4" punct="vg:8">m<seg phoneme="u" type="vs" value="1" rule="428" place="7">ou</seg><pgtc id="14" weight="2" schema="CR">ill<rhyme label="a" id="14" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg>s</rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Bl<seg phoneme="ø" type="vs" value="1" rule="403" place="1">eu</seg>s</w> <w n="10.2">pl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="10.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="10.6" punct="vg:8"><pgtc id="5" weight="0" schema="[R"><rhyme label="b" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l</w>’<w n="11.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>r</w>, <w n="11.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="11.5">t</w>’<w n="11.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="383" place="7">e</seg><pgtc id="14" weight="2" schema="CR">ill<rhyme label="a" id="14" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>s</rhyme></pgtc></w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="12.2" punct="vg:3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>s</w>, <w n="12.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="12.4">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.6" punct="vg:8">v<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">R<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="13.2" punct="vg:4">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="13.3">f<seg phoneme="œ" type="vs" value="1" rule="406" place="5">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="13.4" punct="vg:8"><pgtc id="6" weight="2" schema="[CR">v<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="vg">e</seg>rts</rhyme></pgtc></w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">T<seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="14.3">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="14.4" punct="vg:8">ph<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg><pgtc id="7" weight="2" schema="CR">s<rhyme label="b" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">L</w>’<w n="15.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="15.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="15.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="6" weight="2" schema="CR">v<rhyme label="a" id="6" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8">e</seg>rs</rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">N</w>’<w n="16.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="16.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="16.6" punct="vg:8">m<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg><pgtc id="7" weight="2" schema="CR">s<rhyme label="b" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="17.3" punct="vg:4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s</w>, <w n="17.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="17.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="17.6" punct="vg:8">v<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>x</rhyme></pgtc></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">T<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r</w> <w n="18.2" punct="vg:4">n<seg phoneme="a" type="vs" value="1" rule="343" place="3">a</seg><seg phoneme="i" type="vs" value="1" rule="477" place="4" punct="vg">ï</seg>f</w>, <w n="18.3">n</w>’<w n="18.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="18.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="18.6"><pgtc id="9" weight="2" schema="CR">c<rhyme label="b" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="19.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="19.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="19.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="19.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="19.6">b<pgtc id="8" weight="0" schema="R"><rhyme label="a" id="8" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>s</rhyme></pgtc></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="20.4">v<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="20.5" punct="pt:8"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="9" weight="2" schema="CR">s<rhyme label="b" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="21.2" punct="vg:3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="21.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="21.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>ps</w> <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="21.6">b<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></rhyme></pgtc></w></l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="22.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">où</seg></w> <w n="22.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="22.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="22.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="22.6" punct="pi:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="11" weight="2" schema="CR">s<rhyme label="b" id="11" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg>s</rhyme></pgtc></w> ?</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="23.2">pr<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="23.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="23.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="23.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>p<pgtc id="10" weight="0" schema="R"><rhyme label="a" id="10" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></rhyme></pgtc></w></l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="24.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="24.3" punct="vg:8">br<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="11" weight="2" schema="CR">s<rhyme label="b" id="11" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
				</lg>
				<lg n="7" type="quatrain" rhyme="abab">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="25.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="25.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="25.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="25.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="25.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="12" weight="2" schema="CR">c<rhyme label="a" id="12" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</rhyme></pgtc></w></l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">l</w>’<w n="26.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366" place="4">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="26.4" punct="vg:8">fr<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="13" weight="2" schema="CR">s<rhyme label="b" id="13" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="27.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="27.3">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="27.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="27.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg><pgtc id="12" weight="2" schema="CR">c<rhyme label="a" id="12" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</rhyme></pgtc></w></l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="28.2">qu</w>’<w n="28.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="28.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="28.5" punct="pt:8">m<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg><pgtc id="13" weight="2" schema="CR">s<rhyme label="b" id="13" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>