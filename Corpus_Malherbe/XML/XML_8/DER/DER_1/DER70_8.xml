<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER70" modus="cm" lm_max="12" metProfile="6=6" form="suite de distiques" schema="10((aa))" er_moy="1.3" er_max="2" er_min="0" er_mode="2(6/10)" er_moy_et="0.9">
				<head type="number">LXX</head>
				<opener>
					<salute>A Henri Martineau.</salute>
				</opener>
				<lg n="1" type="distiques" rhyme="aa…">
					<l n="1" num="1.1" lm="12" mp6="C" met="6−6"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="1.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C" caesura="1">a</seg></w><caesura></caesura> <w n="1.6">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.7">f<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="1.9"><pgtc id="10" weight="2" schema="[C[R" part="1">j</pgtc></w>’<w n="1.10"><pgtc id="10" weight="2" schema="[C[R" part="2"><rhyme label="a" id="10" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="12">ai</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="12" mp7="F" met="4+4+4"><w n="2.1">P<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="2.2">n<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" caesura="1">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="2.4">t<seg phoneme="ɔ" type="vs" value="1" rule="441" place="6">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <subst hand="RR" reason="analysis" type="phonemization"><del>II</del><add rend="hidden"><w n="2.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="8" caesura="2">eu</seg>x</w></add><caesura></caesura></subst> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="2.7">l</w>’<hi rend="ital"><w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">A</seg>br<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="10" weight="2" schema="CR" part="1">g<rhyme label="a" id="10" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></pgtc></w></hi></l>
					<l n="3" num="1.3" lm="12" met="6+6"><hi rend="ital"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w></hi> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="3.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="3.7">gr<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="3.9" punct="pt:12">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="11" mp="M">eu</seg><pgtc id="1" weight="2" schema="CR" part="1">r<rhyme label="b" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190" place="1" mp="C">e</seg>t</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>br<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="4.3">n</w>’<w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="4.8">l<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>br<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg><pgtc id="1" weight="2" schema="CR" part="1">r<rhyme label="b" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="C">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="5.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="5.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="5.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="5.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="5.9">r<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="5.10"><pgtc id="2" weight="2" schema="[CR" part="1">d<rhyme label="a" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">an</seg>s</rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="6.2" punct="pt:3">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="pt" mp="F">e</seg></w>. <w n="6.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="6.4" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="6.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="6.6">j</w>’<w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="6.8">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="6.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="6.10"><pgtc id="2" weight="2" schema="[CR" part="1">d<rhyme label="a" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>ts</rhyme></pgtc></w></l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="7.3">t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="7.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>t</w><caesura></caesura> <w n="7.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.6">j</w>’<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="7.8">f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="7.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="7.10">p<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="b" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">L</w>’<w n="8.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="8.3" punct="pt:3">h<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" punct="pt">e</seg>r</w>. <w n="8.4">L<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="8.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fm">e</seg></w>-<w n="8.7">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="8.9">fr<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="b" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="9.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="9.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="9.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d</w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="9.9">n<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>s</w> <w n="9.10">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="9.11"><pgtc id="4" weight="2" schema="[CR" part="1">d<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="161" place="12">e</seg>s</rhyme></pgtc></w></l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">Fl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="10.2" punct="pt:3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="pt" mp="F">e</seg>s</w>. <w n="10.3">C</w>’<w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="10.6">l</w>’<w n="10.7">h<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="10.9">j</w>’<w n="10.10"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg><pgtc id="4" weight="2" schema="CR" part="1">d<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>s</rhyme></pgtc></w></l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="11.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" mp="M">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="11.6">l</w>’<w n="11.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="5" weight="2" schema="CR" part="1">g<rhyme label="b" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">pu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="12.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" mp="M">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="M">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="12.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="12.7">b<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="5" weight="2" schema="CR" part="1">g<rhyme label="b" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="13.2">f<seg phoneme="œ" type="vs" value="1" rule="406" place="2" mp="M">eu</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="13.3">l</w>’<w n="13.4">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rbi<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="13.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>dr<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="13.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="13.7" punct="pt:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="6" weight="2" schema="CR" part="1">n<rhyme label="a" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>r</rhyme></pgtc></w>.</l>
					<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="14.2">j</w>’<w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ds</w> <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="14.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>x</w><caesura></caesura> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="14.7">l</w>’<w n="14.8"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-27" place="10" mp="F">e</seg></w> <w n="14.9" punct="pe:12">h<seg phoneme="e" type="vs" value="1" rule="169" place="11" mp="M">e</seg><pgtc id="6" weight="2" schema="CR" part="1">nn<rhyme label="a" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pe">i</seg>r</rhyme></pgtc></w> !</l>
					<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="15.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="15.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="15.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</w> <w n="15.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt</w> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="15.8">f<seg phoneme="œ" type="vs" value="1" rule="406" place="10">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="15.9" punct="pt:12">s<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="b" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
					<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="16.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="16.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="16.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="16.5">t<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>ts</w><caesura></caesura> <w n="16.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="16.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="16.8">r<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="16.9" punct="pv:12">fl<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="b" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>s</rhyme></pgtc></w> ;</l>
					<l n="17" num="1.17" lm="12" met="6+6"><w n="17.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="17.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="17.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rds</w><caesura></caesura> <w n="17.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="17.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="17.6">d</w>’<w n="17.7" punct="pv:12"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="M">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>f<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="a" id="8" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pv">oi</seg>s</rhyme></pgtc></w> ;</l>
					<l n="18" num="1.18" lm="12" met="6+6"><w n="18.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1" mp="C">eu</seg>r</w> <w n="18.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="18.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="18.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="18.6">l<seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.7"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="18.8">v<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="a" id="8" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>s</rhyme></pgtc></w></l>
					<l n="19" num="1.19" lm="12" met="6+6"><w n="19.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="19.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="19.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>c<seg phoneme="u" type="vs" value="1" rule="d-2" place="5" mp="M">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="19.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>r</w> <w n="19.5">s<seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="19.6">cr<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>n<pgtc id="9" weight="1" schema="GR" part="1">i<rhyme label="b" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="20" num="1.20" lm="12" met="6+6"><w n="20.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="20.2">qu<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="20.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>cs</w><caesura></caesura> <w n="20.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>br<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w> <w n="20.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="20.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="20.8" punct="pt:12">l<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>m<pgtc id="9" weight="1" schema="GR" part="1">i<rhyme label="b" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>