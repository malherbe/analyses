<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN3" modus="cp" lm_max="15" metProfile="8, 4, 5÷5, 4÷5, 5=8, (6+6), (2), (6), (7), (5), (11), (14)">
		<head type="main">CANTILÈNE DES TRAINS<lb></lb>QU’ON MANQUE</head>
			<lg n="1">
				<l n="1" num="1.1" lm="10" mp5="F" met="10"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="1.4" punct="vg:5">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg" mp="F">e</seg>s</w>, <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="1.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7" mp="M">oin</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="1.7" punct="vg:10">g<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
				<l n="2" num="1.2" lm="9" mp5="F" met="4−5"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w><caesura></caesura> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rs</w> <w n="2.6">tr<seg phoneme="o" type="vs" value="1" rule="433" place="8">o</seg>p</w> <w n="2.7" punct="pt:9">t<seg phoneme="a" type="vs" value="1" rule="340" place="9" punct="pt">a</seg>rd</w>.</l>
			</lg>
			<lg n="2">
				<l n="3" num="2.1" lm="8" met="8"><w n="3.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w>-<w n="3.2" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" punct="vg">an</seg></w>, <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w>-<w n="3.4" punct="vg:8">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8" punct="vg">oi</seg></w>,</l>
				<l n="4" num="2.2" lm="9" met="4+5"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M/mp">Em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M/mp">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="4.2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" caesura="1">oi</seg></w><caesura></caesura> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="4.5" punct="vg:9">f<seg phoneme="wa" type="vs" value="1" rule="420" place="9" punct="vg">oi</seg>s</w>,</l>
				<l n="5" num="2.3" lm="10" met="5+5"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M/mp">em</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M/mp">i</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="Lp">on</seg>s</w>-<w n="5.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="5" caesura="1">ou</seg>s</w><caesura></caesura> <w n="5.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s</w></l>
				<l n="6" num="2.4" lm="8" met="8"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="6.4"><seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mn<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="6.5" punct="pt:8">b<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>s</w>.</l>
			</lg>
			<lg n="3">
				<l n="7" num="3.1" lm="2"><w n="7.1" punct="vg:1"><seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">Ou</seg>f</w>, <w n="7.2" punct="pe:2">br<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="pe">ou</seg>f</w> !</l>
				<l n="8" num="3.2" lm="4" met="4"><w n="8.1" punct="pe:4">W<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rp<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="pe">o</seg>fs</w> !</l>
				<l n="9" num="3.3" lm="6"><w n="9.1">C<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="9.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="9.3" punct="ps:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>plu<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="ps">e</seg>s</w>…</l>
				<l n="10" num="3.4" lm="9" met="4+5"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="10.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>s</w><caesura></caesura> <w n="10.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="10.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="10.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="10.8">j</w>’<w n="10.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="10.10" punct="pt:9">su<seg phoneme="i" type="vs" value="1" rule="491" place="9" punct="pt">i</seg>s</w>.</l>
			</lg>
			<lg n="4">
				<l n="11" num="4.1" lm="9" met="4+5"><w n="11.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="11.2">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="11.4">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="10" mp="F">e</seg></w></l>
				<l n="12" num="4.2" lm="13" mp5="M" met="8+5"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>nt</w> <w n="12.3">b<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" caesura="1">en</seg>t</w><caesura></caesura> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="12.5">f<seg phoneme="y" type="vs" value="1" rule="453" place="10" mp="M">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>t</w> <w n="12.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="12" mp="C">eu</seg>rs</w> <w n="12.7" punct="pt:13">p<seg phoneme="i" type="vs" value="1" rule="468" place="13">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="14" punct="pt" mp="F">e</seg>s</w>.</l>
			</lg>
			<lg n="5">
				<l n="13" num="5.1" lm="7"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2" punct="vg:2">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2" punct="vg">ain</seg></w>, <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4">ain</seg></w> <w n="13.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.6">j</w>’<w n="13.7" punct="pe:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" punct="pe">en</seg>ds</w> !</l>
				<l n="14" num="5.2" lm="9" met="5+4" mp4="Mem"><w n="14.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="14.2">n</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" caesura="1">on</seg>s</w><caesura></caesura> <w n="14.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="6" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="14.6" punct="pt:9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" punct="pt">em</seg>ps</w>.</l>
				<l n="15" num="5.3" lm="4" met="4">(<w n="15.1" punct="pt:4">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" punct="pt">en</seg>t</w>.)</l>
			</lg>
			<lg n="6">
				<l n="16" num="6.1" lm="13" met="5+8">— <w n="16.1" punct="vg:2">M<seg phoneme="œ" type="vs" value="1" rule="151" place="1" mp="M">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="2" punct="vg">eu</seg>r</w>, <w n="16.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="16.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="16.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5" caesura="1">eu</seg>t</w><caesura></caesura> <w n="16.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>str<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="16.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="11" mp="C">o</seg>s</w> <w n="16.8" punct="dp:13">b<seg phoneme="a" type="vs" value="1" rule="340" place="12" mp="M">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="13">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="14" punct="dp" mp="F">e</seg>s</w> :</l>
				<l n="17" num="6.2" lm="5"><w n="17.1">C</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="17.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="3">en</seg>t</w> <w n="17.4" punct="pt:5">d<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>mm<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
			</lg>
			<lg n="7">
				<l n="18" num="7.1" lm="11"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="18.2">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="18.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="18.4" punct="vg:6">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>rt</w>, <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="9">ez</seg></w> <w n="18.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="18.7" punct="dp:11">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="12" punct="dp" mp="F">e</seg></w> :</l>
				<l n="19" num="7.2" lm="14"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="19.2">m<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>c<seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg></w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="19.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="19.5">ch<seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>ff<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="19.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="19.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="19.8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12">œu</seg>r</w> <w n="19.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="13" mp="Pem">e</seg></w> <w n="19.10" punct="pv:14">r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="14">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="15" punct="pv" mp="F">e</seg></w> ;</l>
				<l n="20" num="7.3" lm="15"><w n="20.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rs</w>, <w n="20.2"><seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="20.3">d</w>’<w n="20.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="20.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="Fc">e</seg></w> <w n="20.6">m<seg phoneme="u" type="vs" value="1" rule="425" place="12" mp="M">ou</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420" place="13">oi</seg>r</w> <w n="20.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="14" mp="Pem">e</seg></w> <w n="20.8" punct="pi:15">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="15">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="16" punct="pi" mp="F">e</seg></w> ?</l>
			</lg>
			<lg n="8">
				<l n="21" num="8.1" lm="12" met="6+6" met_alone="True"><w n="21.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="21.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="21.3">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>s</w> <w n="21.4">s</w>’<w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="21.6">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="21.7">r<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="21.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="21.9" punct="dp:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="dp">e</seg>ts</w> :</l>
				<l n="22" num="8.2" lm="9" met="4+5"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="22.2">l</w>’<w n="22.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="22.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="22.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" caesura="1">è</seg>s</w><caesura></caesura> <w n="22.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" mp="M">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="22.7" punct="pt:9"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9" punct="pt">è</seg>s</w>.</l>
			</lg>
	</div></body></text></TEI>