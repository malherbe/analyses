<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN19" modus="cp" lm_max="12" metProfile="5, 4, 5+5, (6+6), (7), (3), (11)">
		<head type="main">BALLADE <lb></lb>DES CANNES ET DES PARAPLUIES</head>
			<div type="section" n="1">
			<head type="number">I</head>
			<lg n="1">
				<l n="1" num="1.1" lm="5" met="5"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">B<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w></l>
				<l n="2" num="1.2" lm="5" met="5"><w n="2.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="2.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="2.3" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>plu<seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
				<l n="3" num="1.3" lm="4" met="4"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>rs</w> <w n="3.3">P<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w></l>
				<l n="4" num="1.4" lm="4" met="4"><w n="4.1">S</w>’<w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="4.4" punct="tc:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="dp ti">e</seg></w> : —</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1" lm="10" met="5+5"><w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="5.3" punct="vg:5">j<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg" caesura="1">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w>,<caesura></caesura> <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="5.6" punct="vg:10">j<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
				<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="6.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="6.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="5" caesura="1">eu</seg>x</w><caesura></caesura> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="6.5">s<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>il</w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="6.7" punct="pt:10">lu<seg phoneme="i" type="vs" value="1" rule="491" place="10" punct="pt">i</seg></w>.</l>
			</lg>
			</div>
			<div type="section" n="2">
			<head type="number">II</head>
			<lg n="1">
				<l n="7" num="1.1" lm="11"><w n="7.1" punct="vg:5">N<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5" punct="vg">en</seg>t</w>, <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="7.3">d<seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.4" punct="vg:11">su<seg phoneme="i" type="vs" value="1" rule="491" place="10" mp="M">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" punct="vg">an</seg>t</w>,</l>
				<l n="8" num="1.2" lm="10" met="5+5"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="8.2">B<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.4" punct="dp:5">d<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="dp" caesura="1">i</seg>t</w> :<caesura></caesura> <w n="8.5">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="6" mp="Lc">i</seg>squ</w>’<w n="8.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="8.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="8.8">b<seg phoneme="o" type="vs" value="1" rule="315" place="9">eau</seg></w> <w n="8.9" punct="vg:10">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" punct="vg">em</seg>ps</w>,</l>
				<l n="9" num="1.3" lm="10" met="5+5"><w n="9.1">Pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="9.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="9.3">c<seg phoneme="a" type="vs" value="1" rule="341" place="5" caesura="1">a</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="9.5">p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.6">d</w>’<w n="9.7" punct="tc:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10" punct="pv ti">en</seg>t</w> ; —</l>
			</lg>
			<lg n="2">
				<l n="10" num="2.1" lm="10" met="5+5"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g</w> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="10.5" punct="vg:5">j<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg" caesura="1">ou</seg>r</w>,<caesura></caesura> <w n="10.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="C">i</seg>l</w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="10.8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="10.9">s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="10.10">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt</w></l>
				<l n="11" num="2.2" lm="12" met="6+6" met_alone="True"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="11.4">n</w>’<w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="11.7">m<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="11.8">M<seg phoneme="œ" type="vs" value="1" rule="151" place="7" mp="M">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="8">eu</seg>r</w> <w n="11.9">C<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rn<seg phoneme="o" type="vs" value="1" rule="438" place="10">o</seg>t</w> <w n="11.10" punct="pt:12">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12" punct="pt">o</seg>rs</w>.</l>
			</lg>
			</div>
			<div type="section" n="3">
			<head type="number">III</head>
			<lg n="1">
				<l n="12" num="1.1" lm="10" met="5+5"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="12.3" punct="vg:5">B<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg" caesura="1">eu</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w>,<caesura></caesura> <w n="12.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rs</w> <w n="12.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="12.6" punct="vg:10">pr<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
				<l n="13" num="1.2" lm="5" met="5"><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="13.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w></l>
				<l n="14" num="1.3" lm="5" met="5"><w n="14.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="14.2" punct="vg:5">f<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>ch<seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg">eu</seg>x</w>,</l>
				<l n="15" num="1.4" lm="10" met="5+5"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="15.2">pr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="15.5">f<seg phoneme="wa" type="vs" value="1" rule="420" place="5" caesura="1">oi</seg>s</w><caesura></caesura> <w n="15.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>plu<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="15.8" punct="vg:10">st<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg>ck</w>,</l>
			</lg>
			<lg n="2">
				<l n="16" num="2.1" lm="7"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="16.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="16.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="16.6">d<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w></l>
				<l n="17" num="2.2" lm="3"><w n="17.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="17.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3" punct="pt:3">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3" punct="pt">ain</seg></w>.</l>
			</lg>
			</div>
	</div></body></text></TEI>