<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU343" modus="cm" lm_max="10" metProfile="5+5" form="sonnet classique" schema="abba abba cdd cee" er_moy="1.43" er_max="4" er_min="0" er_mode="0(3/7)" er_moy_et="1.4">
				<head type="main">SONNET</head>
				<lg n="1" rhyme="abba">
					<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1" mp="M">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="5" caesura="1">oi</seg>s</w><caesura></caesura> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="1.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.6" punct="pv:10">p<seg phoneme="a" type="vs" value="1" rule="343" place="9" mp="M">a</seg>ï<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="366" place="10">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">m</w>’<w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="2.4" punct="vg:5">cr<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg><seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="2.5">f<seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="2.6">d</w>’<w n="2.7" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>qu<seg phoneme="i" type="vs" value="1" rule="491" place="9" mp="M">i</seg><pgtc id="2" weight="2" schema="CR">t<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="3.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c</w> <w n="3.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>l</w><caesura></caesura> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.6">sc<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>l<pgtc id="2" weight="2" schema="CR">pt<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1">D</w>’<w n="4.2">h<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>t<seg phoneme="a" type="vs" value="1" rule="343" place="2" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="477" place="3">ï</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="4.3">gr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" caesura="1">e</seg>cqu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="4.4"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="4.5" punct="pt:10">m<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="dc-1" place="9" mp="M">i</seg><pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="366" place="10">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" rhyme="abba">
					<l n="5" num="2.1" lm="10" met="5+5"><w n="5.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="5.2">j</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" caesura="1">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.4"><seg phoneme="y" type="vs" value="1" rule="453" place="6" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.5" punct="vg:10"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">I</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>l<pgtc id="3" weight="4" schema="VR"><seg phoneme="i" type="vs" value="1" rule="dc-1" place="9" mp="M">i</seg><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="366" place="10">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="10" met="5+5"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="6.2">t<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg></w><caesura></caesura> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="6.5" punct="vg:10">m<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="10" met="5+5"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="7.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="7.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="7.4" punct="vg:5">g<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5" punct="vg" caesura="1">e</seg>ts</w>,<caesura></caesura> <w n="7.5">f<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.7">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d</w> <w n="7.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="7.9" punct="vg:10"><pgtc id="4" weight="2" schema="[CR">th<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.4" lm="10" met="5+5"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">qu</w>’<w n="8.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="8.4">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">An</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" caesura="1">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w><caesura></caesura> <w n="8.6"><seg phoneme="u" type="vs" value="1" rule="426" place="6">ou</seg></w> <w n="8.7" punct="pt:10">P<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>s<pgtc id="3" weight="4" schema="VR"><seg phoneme="i" type="vs" value="1" rule="dc-1" place="9" mp="M">i</seg><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="366" place="10">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" rhyme="cdd">
					<l n="9" num="3.1" lm="10" met="5+5"><w n="9.1">L</w>’<w n="9.2" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="341" place="1" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>r</w>, <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="9.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="9.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="9.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="9.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="9.9" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>s<pgtc id="5" weight="2" schema="CR">t<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="10" punct="vg">e</seg>l</rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="10" met="5+5"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="10.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="10.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>cs</w> <w n="10.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="10.5">pr<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>s</w><caesura></caesura> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="10.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="10.9" punct="vg:10">t<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>rqu<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="10" met="5+5"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="11.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg></w><caesura></caesura> <w n="11.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="11.7" punct="vg:10">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">am</seg>b<pgtc id="6" weight="0" schema="R"><rhyme label="d" id="6" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="4" rhyme="cee">
					<l n="12" num="4.1" lm="10" met="5+5"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="12.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.4">gr<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" caesura="1">e</seg>c</w><caesura></caesura> <w n="12.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="P">an</seg>s</w> <w n="12.6">l</w>’<w n="12.7"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r</w> <w n="12.8">d</w>’<w n="12.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="12.10" punct="vg:10">c<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<pgtc id="5" weight="2" schema="CR">t<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="346" place="10" punct="vg">e</seg>l</rhyme></pgtc></w>,</l>
					<l n="13" num="4.2" lm="10" met="5+5"><w n="13.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg>x</w> <w n="13.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M">o</seg>rtr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" caesura="1">ai</seg>ts</w><caesura></caesura> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="13.5">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="13.8">pl<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="4.3" lm="10" met="5+5"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="14.3">R<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>lb<seg phoneme="a" type="vs" value="1" rule="340" place="5" caesura="1">a</seg></w><caesura></caesura> <w n="14.4">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="14.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="14.6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w> <w n="14.7" punct="pt:10">bl<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="M">eu</seg><pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="10">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">1870</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>