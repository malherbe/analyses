<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU349" modus="cm" lm_max="12" metProfile="6+6" form="suite périodique" schema="4(abba)" er_moy="0.88" er_max="2" er_min="0" er_mode="0(4/8)" er_moy_et="0.93">
				<head type="main">A UNE JEUNE AMIE</head>
				<lg n="1" type="quatrain" rhyme="abba">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="1.4">c<seg phoneme="o" type="vs" value="1" rule="435" place="4" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="1.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="Fc">e</seg></w> <w n="1.7" punct="vg:12">f<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="2.2" punct="vg:3">M<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rb<seg phoneme="œ" type="vs" value="1" rule="249" place="3" punct="vg">œu</seg>f</w>, <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="2.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="2.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="2.7">c<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.8">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="2.9" punct="vg:12">fi<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="GAU349_1" place="12" punct="vg">e</seg>r</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6">(<w n="3.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="3.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="3.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.6">d<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="3.7">d</w>’<w n="3.8" punct="pf:12">h<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>r</rhyme></pgtc></w>),</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="vg:3">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="4.3">n</w>’<w n="4.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="4.5">qu</w>’<w n="4.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="4.7">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="4.8" punct="pt:12">f<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abba">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2" punct="pv:3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="pv">in</seg>s</w> ; <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="5.4">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="5" mp="M">im</seg>pi<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</w> <w n="5.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="P">u</seg>r</w> <w n="5.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="5.8" punct="vg:12">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>x</rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="6.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="œ" type="vs" value="1" rule="286" place="6" caesura="1">œ</seg>il</w><caesura></caesura> <w n="6.6">br<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="6.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="6.8">f<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg></w> <w n="6.9">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="6.10" punct="pv:12">t<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="7.2">c<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="7.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="7.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="7.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg>pr<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="8.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="3" mp="C">o</seg>s</w> <w n="8.3">j<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="8.6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg>s</w> <w n="8.7">br<seg phoneme="y" type="vs" value="1" rule="454" place="8" mp="M">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ts</w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="8.9">pl<seg phoneme="y" type="vs" value="1" rule="450" place="11">u</seg>s</w> <w n="8.10" punct="pt:12">d<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>x</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abba">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="9.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="9.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="9.6"><seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="9.8"><pgtc id="5" weight="2" schema="[CR">pr<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="10.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3">t<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>nn<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="10.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="10.5">qu</w>’<w n="10.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="10.7">n<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="10.8" punct="vg:12">f<seg phoneme="œ" type="vs" value="1" rule="406" place="10" mp="M">eu</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="6" weight="2" schema="CR">t<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></rhyme></pgtc></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="11.5" punct="vg:6">f<seg phoneme="a" type="vs" value="1" rule="193" place="6" punct="vg" caesura="1">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="11.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="11.9">b<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="6" weight="2" schema="CR">t<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg></rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">Tr<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="12.3">s</w>’<w n="12.4"><seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.6">p<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="12.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="12.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="12.9" punct="pt:12"><pgtc id="5" weight="2" schema="[CR">r<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="444" place="12">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abba">
					<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="13.2">d</w>’<w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="13.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="5" mp="M">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="13.6">j</w>’<w n="13.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="13.8">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9" mp="P">e</seg>rs</w> <w n="13.9">d</w>’<w n="13.10"><seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="13.11" punct="ps:12">c<pgtc id="7" weight="1" schema="GR">i<rhyme label="a" id="7" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="ps">eu</seg>x</rhyme></pgtc></w>…</l>
					<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="14.3">qu</w>’<w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="14.5">f<seg phoneme="wa" type="vs" value="1" rule="440" place="5" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="14.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="14.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="C">ou</seg>s</w> <w n="14.8">tr<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w> <w n="14.9" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg><pgtc id="8" weight="2" schema="CR">c<rhyme label="b" id="8" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1" punct="vg:1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="15.2">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rb<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="15.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="15.6">qu</w>’<w n="15.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="15.8">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w> <w n="15.9">b<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rge<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="15.10">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg><pgtc id="8" weight="2" schema="CR">c<rhyme label="b" id="8" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1" punct="vg:1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="16.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="16.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="16.4" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="16.6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg></w> <w n="16.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="10">à</seg></w> <w n="16.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="11">en</seg></w> <w n="16.9" punct="pt:12">v<pgtc id="7" weight="1" schema="GR">i<rhyme label="a" id="7" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>