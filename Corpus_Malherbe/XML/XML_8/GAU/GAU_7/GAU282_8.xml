<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU282" modus="cm" lm_max="12" metProfile="6+6" form="sonnet classique" schema="abba abba cdd cee" er_moy="1.71" er_max="6" er_min="0" er_mode="0(3/7)" er_moy_et="1.98">
					<head type="main">PERSPECTIVE</head>
					<head type="form">SONNET</head>
					<lg n="1" rhyme="abba">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3" punct="vg:6">Gu<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>lqu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>r</w>,<caesura></caesura> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="1.5">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="1.7" punct="vg:12">S<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="œ" type="vs" value="1" rule="286" place="2">œ</seg>il</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="2.5">l</w>’<w n="2.6">h<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="2.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="2.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>c</w> <w n="2.10" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="2" weight="2" schema="CR">gr<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="vg">e</seg>t</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="3.2" punct="vg:3">d<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="3.4">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg>s</w><caesura></caesura> <w n="3.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="3.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="Fc">e</seg></w> <w n="3.8" punct="pv:12">f<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg><pgtc id="2" weight="2" schema="CR">r<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="12" punct="pv">ê</seg>t</rhyme></pgtc></w> ;</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="4.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="4.5">r<seg phoneme="u" type="vs" value="1" rule="426" place="6" caesura="1">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="4.6"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="4.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="4.8"><seg phoneme="y" type="vs" value="1" rule="453" place="10" mp="C">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.9" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>g<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɥi" type="vs" value="1" rule="274" place="12">ui</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" rhyme="abba">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">D</w>’<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd</w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="5.4" punct="vg:6">G<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ld<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg></w>,<caesura></caesura> <w n="5.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="5.6">l</w>’<w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.8">d</w>’<w n="5.9"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r</w> <w n="5.10" punct="vg:12">sc<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11" mp="M">in</seg>t<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">R<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="6.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="6.5" punct="vg:6">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg></w>,<caesura></caesura> <w n="6.6">d<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="6.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="6.8" punct="pv:12">m<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>n<pgtc id="4" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="12" punct="pv">e</seg>t</rhyme></pgtc></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="7.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>th<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="7.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="7.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>r</w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>pp<pgtc id="4" weight="6" schema="VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">aî</seg>t</rhyme></pgtc></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="Lc">a</seg>r</w>-<w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem/mc">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="8.4" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg>s</w>,<caesura></caesura> <w n="8.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.6">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="8.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="8.9" punct="pt:12">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" rhyme="cdd">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="9.2" punct="vg:2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" punct="vg">è</seg>s</w>, <w n="9.3">l</w>’<w n="9.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="9.5">n</w>’<w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rç<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>t</w><caesura></caesura> <w n="9.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="9.9">fr<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>gm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10">en</seg>ts</w> <w n="9.10">d</w>’<w n="9.11"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<pgtc id="5" weight="2" schema="CR">c<rhyme label="c" id="5" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="12">eau</seg>x</rhyme></pgtc></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="10.2">p<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="10.3" punct="vg:6">b<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>sc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rn<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg></w>,<caesura></caesura> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.6">d</w>’<w n="10.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9">un</seg></w> <w n="10.8">m<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r</w> <w n="10.9">m<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg><pgtc id="6" weight="2" schema="CR">ss<rhyme label="d" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="11.3">fl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.4"><seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>vr<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="11.7">r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.8" punct="pt:12">f<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg><pgtc id="6" weight="2" schema="CR">ç<rhyme label="d" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" rhyme="cee">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>ds</w> <w n="12.2" punct="vg:3">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="12.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" mp="M">o</seg>bstr<seg phoneme="y" type="vs" value="1" rule="454" place="5" mp="M">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="12.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>squ<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="12.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="P">a</seg>r</w> <w n="12.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="12.8" punct="vg:12"><pgtc id="5" weight="2" schema="CR">s<rhyme label="c" id="5" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="vg">o</seg>ts</rhyme></pgtc></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="13.3">h<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="13.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="13.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="13.7">t<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>ts</w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="13.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="13.10" punct="vg:12">v<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="14.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg></w> <w n="14.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="3" mp="C">o</seg>s</w> <w n="14.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>ts</w> <w n="14.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="14.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>nt</w> <w n="14.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="14.8">l</w>’<w n="14.9"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>r</w> <w n="14.10" punct="pe:12">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>qu<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="485" place="12">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Sur le Guadalquivir</placeName>,
							<date when="1844">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>