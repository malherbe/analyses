<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU22" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="6(abab)" er_moy="1.08" er_max="2" er_min="0" er_mode="2(6/12)" er_moy_et="0.95">
				<head type="main">L’AVEUGLE</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>gl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="1.4">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg></w> <w n="1.5">d</w>’<w n="1.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="1.7" punct="vg:8">b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">H<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd</w> <w n="2.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="2.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="2.6" punct="vg:8">h<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>b<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="vg">ou</seg></rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="3.3" punct="vg:5">fl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ge<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5" punct="vg">e</seg>t</w>, <w n="3.4">d</w>’<w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r</w> <w n="3.7" punct="vg:8">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">â</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="4.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6" punct="vg:8">tr<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="vg">ou</seg></rhyme></pgtc></w>,</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2">j<seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg></w> <w n="5.5">v<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="3" weight="2" schema="CR">v<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="6.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.4" punct="pv:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>rb<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pv">en</seg>t</rhyme></pgtc></w> ;</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="7.2">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.7" punct="vg:8"><pgtc id="3" weight="2" schema="[CR">v<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="œ" type="vs" value="1" rule="286" place="6">œ</seg>il</w> <w n="8.6" punct="pt:8">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>r<pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="9.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="9.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="9.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="9.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="9.7" punct="pv:8">l<pgtc id="5" weight="1" schema="GR">u<rhyme label="a" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1" punct="vg:1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d</w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>bsc<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r</rhyme></pgtc></w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.3">v<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="11.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="11.5">br<pgtc id="5" weight="1" schema="GR">u<rhyme label="a" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="12.3">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="12.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="12.6" punct="pe:8">m<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pe">u</seg>r</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="4" type="quatrain" rhyme="abab">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="13.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="13.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="13.4">ch<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="13.5">n<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">H<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="14.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="14.4" punct="pe:8">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="8" weight="2" schema="CR">v<rhyme label="b" id="8" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pe">eau</seg></rhyme></pgtc></w> !</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ls</w> <w n="15.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="15.4">gr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="16.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.6" punct="pe:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="8" weight="2" schema="CR">v<rhyme label="b" id="8" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pe">eau</seg></rhyme></pgtc></w> !</l>
				</lg>
				<lg n="5" type="quatrain" rhyme="abab">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="17.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="17.4">pu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>ts</w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="17.6" punct="vg:8">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="9" weight="2" schema="CR">n<rhyme label="a" id="9" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="18.2">pr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nni<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="18.5" punct="vg:8">f<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="vg">ou</seg></rhyme></pgtc></w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="19.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="19.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="19.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="19.5">s</w>’<w n="19.6" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>r<pgtc id="9" weight="2" schema="CR">n<rhyme label="a" id="9" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">Gr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="20.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="20.3">m<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>ts</w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="20.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="20.6" punct="pt:8">cl<pgtc id="10" weight="0" schema="R"><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="pt">ou</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="6" type="quatrain" rhyme="abab">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="21.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w>-<w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="21.5">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="21.6" punct="vg:8">f<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg><pgtc id="11" weight="2" schema="CR">n<rhyme label="a" id="11" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="22.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="22.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="22.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="22.6" punct="vg:8">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg><pgtc id="12" weight="2" schema="CR">b<rhyme label="b" id="12" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1">L</w>’<w n="23.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="23.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="23.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="23.5">t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="11" weight="2" schema="CR">n<rhyme label="a" id="11" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1"><seg phoneme="i" type="vs" value="1" rule="497" place="1">Y</seg></w> <w n="24.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="24.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r</w> <w n="24.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="24.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.6" punct="pe:8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg><pgtc id="12" weight="2" schema="CR">b<rhyme label="b" id="12" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pe">eau</seg></rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>