<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1838-1845</title>
				<title type="sub_2">Tome second</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>625 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1838-1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU233" modus="sm" lm_max="6" metProfile="6" form="suite périodique" schema="5(ababacc)" er_moy="0.3" er_max="2" er_min="0" er_mode="0(17/20)" er_moy_et="0.71">
			<head type="main">LES MATELOTS</head>
			<lg n="1" type="septain" rhyme="ababacc">
				<l n="1" num="1.1" lm="6" met="6"><w n="1.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="1.2">l</w>’<w n="1.3"><seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="1.4">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="1.6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>f<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="2" num="1.2" lm="6" met="6"><w n="2.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="2.3" punct="vg:6">v<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ge<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="6" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
				<l n="3" num="1.3" lm="6" met="6"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.3">m<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="4" num="1.4" lm="6" met="6"><w n="4.1">D</w>’<w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="4.3">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">d</w>’<w n="4.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rg<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="vg">en</seg>t</rhyme></pgtc></w>,</l>
				<l n="5" num="1.5" lm="6" met="6"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">î</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.5" punct="vg:6">S<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				<l n="6" num="1.6" lm="6" met="6"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">In</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="6.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="6.6" punct="vg:6">br<seg phoneme="y" type="vs" value="1" rule="445" place="5">û</seg><pgtc id="3" weight="2" schema="CR">l<rhyme label="c" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg></rhyme></pgtc></w>,</l>
				<l n="7" num="1.7" lm="6" met="6"><w n="7.1">J<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>squ</w>’<w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="7.3">p<seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4" punct="ps:6">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg><pgtc id="3" weight="2" schema="CR">l<rhyme label="c" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="ps">é</seg></rhyme></pgtc></w>…</l>
			</lg>
			<lg n="2" type="septain" rhyme="ababacc">
				<l n="8" num="2.1" lm="6" met="6"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></pgtc></w></l>
				<l n="9" num="2.2" lm="6" met="6"><w n="9.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="9.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>gt</w> <w n="9.5">d</w>’<w n="9.6"><pgtc id="5" weight="0" schema="[R"><rhyme label="b" id="5" gender="m" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r</rhyme></pgtc></w></l>
				<l n="10" num="2.3" lm="6" met="6"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="10.3">c<seg phoneme="o" type="vs" value="1" rule="415" place="3">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="10.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="10.5">v<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg>s</rhyme></pgtc></w></l>
				<l n="11" num="2.4" lm="6" met="6"><w n="11.1">D<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="11.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="11.3">l</w>’<w n="11.4" punct="pv:6"><seg phoneme="e" type="vs" value="1" rule="353" place="5">e</seg>ss<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="pv">o</seg>r</rhyme></pgtc></w> ;</l>
				<l n="12" num="2.5" lm="6" met="6"><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="12.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.5" punct="vg:6">t<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
				<l n="13" num="2.6" lm="6" met="6"><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>cs</w> <w n="13.4" punct="vg:6"><seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg><pgtc id="6" weight="2" schema="CR">s<rhyme label="c" id="6" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="vg">eau</seg>x</rhyme></pgtc></w>,</l>
				<l n="14" num="2.7" lm="6" met="6"><w n="14.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ffl<seg phoneme="ø" type="vs" value="1" rule="405" place="3">eu</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg><pgtc id="6" weight="2" schema="C[R" part="1">s</pgtc></w> <w n="14.4" punct="pt:6"><pgtc id="6" weight="2" schema="C[R" part="2"><rhyme label="c" id="6" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="pt">eau</seg>x</rhyme></pgtc></w>.</l>
			</lg>
			<lg n="3" type="septain" rhyme="ababacc">
				<l n="15" num="3.1" lm="6" met="6"><w n="15.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="15.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="15.5">t<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="16" num="3.2" lm="6" met="6"><w n="16.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="16.3">f<seg phoneme="ɥi" type="vs" value="1" rule="462" place="3">u</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="16.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>j<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</rhyme></pgtc></w></l>
				<l n="17" num="3.3" lm="6" met="6"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="17.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.4" punct="vg:6">m<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				<l n="18" num="3.4" lm="6" met="6"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="18.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="18.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="18.4" punct="pv:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pv">ou</seg>rs</rhyme></pgtc></w> ;</l>
				<l n="19" num="3.5" lm="6" met="6"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="19.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.4">l<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>g<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="20" num="3.6" lm="6" met="6"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="20.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="20.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="20.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>fr<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="c" id="9" gender="m" type="a"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></rhyme></pgtc></w></l>
				<l n="21" num="3.7" lm="6" met="6"><w n="21.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w> <w n="21.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="21.3" punct="pt:6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>gr<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="c" id="9" gender="m" type="e"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="pt">in</seg></rhyme></pgtc></w>.</l>
			</lg>
			<lg n="4" type="septain" rhyme="ababacc">
				<l n="22" num="4.1" lm="6" met="6"><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="22.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ch<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="a" id="10" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="23" num="4.2" lm="6" met="6"><w n="23.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="23.2">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l</w> <w n="23.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="23.5" punct="pv:6">d<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="b" id="11" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="pv">u</seg>r</rhyme></pgtc></w> ;</l>
				<l n="24" num="4.3" lm="6" met="6"><w n="24.1">L</w>’<w n="24.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="24.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="24.4">n<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="a" id="10" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="25" num="4.4" lm="6" met="6"><w n="25.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="25.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="25.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>ps</w> <w n="25.4">d</w>’<w n="25.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>z<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="b" id="11" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg>r</rhyme></pgtc></w>,</l>
				<l n="26" num="4.5" lm="6" met="6"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="26.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="26.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="26.5" punct="vg:6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>du<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="a" id="10" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				<l n="27" num="4.6" lm="6" met="6"><w n="27.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="27.2">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="27.3">n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="27.4" punct="vg:6">tr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="c" id="12" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="307" place="6" punct="vg">a</seg>il</rhyme></pgtc></w>,</l>
				<l n="28" num="4.7" lm="6" met="6"><w n="28.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="28.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="28.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="28.5" punct="pt:6">c<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="c" id="12" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="307" place="6" punct="pt">a</seg>il</rhyme></pgtc></w>.</l>
			</lg>
			<lg n="5" type="septain" rhyme="ababacc">
				<l n="29" num="5.1" lm="6" met="6"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="354" place="1">E</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="29.2" punct="pe:6">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>bl<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></pgtc></w> !</l>
				<l n="30" num="5.2" lm="6" met="6"><w n="30.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="30.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="30.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="30.4" punct="vg:6"><pgtc id="14" weight="2" schema="[CR" part="1">n<rhyme label="b" id="14" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>d</rhyme></pgtc></w>,</l>
				<l n="31" num="5.3" lm="6" met="6"><w n="31.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="31.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="31.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="31.4">l</w>’<w n="31.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>b<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
				<l n="32" num="5.4" lm="6" met="6"><w n="32.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="32.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="2">ein</seg></w> <w n="32.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="32.4">l</w>’<w n="32.5" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg><pgtc id="14" weight="2" schema="CR" part="1">n<rhyme label="b" id="14" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg></rhyme></pgtc></w>,</l>
				<l n="33" num="5.5" lm="6" met="6"><w n="33.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="33.2">fl<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>ts</w> <w n="33.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="33.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="33.5" punct="vg:6">c<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
				<l n="34" num="5.6" lm="6" met="6"><w n="34.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="34.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="34.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="34.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rt</w> <w n="34.5">bl<pgtc id="15" weight="0" schema="R" part="1"><rhyme label="c" id="15" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></rhyme></pgtc></w></l>
				<l n="35" num="5.7" lm="6" met="6"><w n="35.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="35.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="35.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="35.4" punct="pe:6">Di<pgtc id="15" weight="0" schema="R" part="1"><rhyme label="c" id="15" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pe">eu</seg></rhyme></pgtc></w> !</l>
			</lg>
			<closer>
				<dateline>
					<date when="1841">1841</date>.
				</dateline>
			</closer>
		</div></body></text></TEI>