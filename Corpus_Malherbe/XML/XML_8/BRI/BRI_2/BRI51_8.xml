<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><head type="sub_part">EN BRETAGNE</head><div type="poem" key="BRI51" modus="sm" lm_max="8" metProfile="8" form="suite périodique" schema="4(aaa)" er_moy="2.0" er_max="6" er_min="0" er_mode="0(4/8)" er_moy_et="2.45">
					<head type="main">Les trois Voyages</head>
					<lg n="1" type="tercet" rhyme="aaa">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">OU</seg>R</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="1.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g</w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="1.6" punct="vg:8">S<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="2.2" punct="vg:3">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="2.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="2.5">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s</w> <w n="2.6" punct="vg:8"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>t</w> <w n="3.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w>-<w n="3.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="3.5">tr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="3.6" punct="pt:8">v<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="tercet" rhyme="aaa">
						<l n="4" num="2.1" lm="8" met="8"><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="4.6" punct="vg:8">m<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="vg">e</seg>rs</rhyme></pgtc></w>,</l>
						<l n="5" num="2.2" lm="8" met="8"><w n="5.1">S</w>’<w n="5.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">im</seg>pr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="5.4">cl<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ts</w> <w n="5.5" punct="vg:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="vg">e</seg>rs</rhyme></pgtc></w>,</l>
						<l n="6" num="2.3" lm="8" met="8"><w n="6.1">Si<seg phoneme="e" type="vs" value="1" rule="241" place="1">e</seg>d</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="6.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="6.4">fl<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.6" punct="pt:8"><pgtc id="2" weight="2" schema="[CR">v<rhyme label="a" id="2" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="pt">e</seg>rts</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="3" type="tercet" rhyme="aaa">
						<l n="7" num="3.1" lm="8" met="8"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="7.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="7.4" punct="vg:5">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>ls</w>, <w n="7.5">l</w>’<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341" place="6">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="7.7">h<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="3.2" lm="8" met="8"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="8.3"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.4">d<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><pgtc id="3" weight="2" schema="CR">m<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="9" num="3.3" lm="8" met="8"><w n="9.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="9.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.7" punct="pt:8">pr<pgtc id="3" weight="6" schema="VCR"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					</lg>
					<lg n="4" type="tercet" rhyme="aaa">
						<l n="10" num="4.1" lm="8" met="8"><w n="10.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="10.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="10.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="10.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rni<pgtc id="4" weight="6" schema="V[CR" part="1"><seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></pgtc></w> <w n="10.7" punct="pv:8"><pgtc id="4" weight="6" schema="V[CR" part="2">j<rhyme label="a" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pv">ou</seg>r</rhyme></pgtc></w> ;</l>
						<l n="11" num="4.2" lm="8" met="8"><w n="11.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="11.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="11.4" punct="vg:4">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg>x</w>, <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.6">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l</w> <w n="11.7" punct="vg:8">s<pgtc id="4" weight="6" schema="VCR" part="1"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>j<rhyme label="a" id="4" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</rhyme></pgtc></w>,</l>
						<l n="12" num="4.3" lm="8" met="8"><w n="12.1">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.4">r<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.6">l</w>’<w n="12.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">A</seg>m<pgtc id="4" weight="0" schema="R" part="1"><rhyme label="a" id="4" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>