<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="CHA">
					<name>
						<forename>François-René</forename>
						<nameLink>de</nameLink>
						<surname>CHATEAUBRIAND</surname>
					</name>
					<date from="1768" to="1848">1768-1848</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>735 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CHA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies diverses</title>
						<author>François-René de Chateaubriand</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/chateaubriandpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1797" to="1827">1797-1827</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas incluse.</p>
				<p>Les notes de l’auteur ont été intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Des corrections métriques ont été apportées sans autres attestations publiées.</p>
				<p>Les vers de la strophe illisible du poème "SERMENT — LES NOIRS DEVANT LE GIBET DE JOHN BROWN" ont été marqués comme inanalysables.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
 					<p>L’orthographe a été vérifiée avec le correcteur Aspell.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CHA22" modus="cm" lm_max="10" metProfile="4+6" form="suite périodique" schema="2(abab)" er_moy="0.75" er_max="2" er_min="0" er_mode="0(2/4)" er_moy_et="0.83">
				<head type="number">XII</head>
				<head type="main">Pour la fête de Madame de ***</head>
				<opener>
					<dateline>
						<placeName>Verneuil</placeName>,
						<date when="1812">1812</date>.
					</dateline>
				</opener>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="1.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="1.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="1.6">tr<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.7">f<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>d<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="2.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3" mp="M">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="2.4">s</w>’<w n="2.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="2.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="2.8" punct="dp:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>f<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="dp">an</seg>ts</rhyme></pgtc></w> :</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="3.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="3.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">aî</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="3.7">n<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="4.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>r</w><caesura></caesura> <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ls</w> <w n="4.6">t</w>’<w n="4.7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>nt</w> <w n="4.8">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="4.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="4.10" punct="pt:10"><pgtc id="2" weight="0" schema="[R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="pt">an</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="5.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg></w><caesura></caesura> <w n="5.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="5.6">gr<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="5.8" punct="vg:10">pr<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>d<pgtc id="3" weight="1" schema="GR">u<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="6.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="6.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" caesura="1">em</seg>ps</w><caesura></caesura> <w n="6.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>t</w> <w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="6.8" punct="pt:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg>s</rhyme></pgtc></w>.</l>
					<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="7.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="7.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="P">ou</seg>r</w> <w n="7.4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4" caesura="1">oi</seg></w><caesura></caesura> <w n="7.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="C">ou</seg>s</w> <w n="7.6">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>ls</w> <w n="7.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rqu<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>s</w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="7.9" punct="dp:10">f<pgtc id="3" weight="1" schema="GR">u<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
					<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="8.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="8.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="8.6">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="8.7" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg><pgtc id="4" weight="2" schema="CR">m<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg>s</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>