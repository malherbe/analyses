<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="CHA">
					<name>
						<forename>François-René</forename>
						<nameLink>de</nameLink>
						<surname>CHATEAUBRIAND</surname>
					</name>
					<date from="1768" to="1848">1768-1848</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>735 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CHA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies diverses</title>
						<author>François-René de Chateaubriand</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/chateaubriandpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1797" to="1827">1797-1827</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas incluse.</p>
				<p>Les notes de l’auteur ont été intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Des corrections métriques ont été apportées sans autres attestations publiées.</p>
				<p>Les vers de la strophe illisible du poème "SERMENT — LES NOIRS DEVANT LE GIBET DE JOHN BROWN" ont été marqués comme inanalysables.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
 					<p>L’orthographe a été vérifiée avec le correcteur Aspell.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CHA16" modus="sp" lm_max="8" metProfile="8, 2" form="suite périodique" schema="5(aababb)" er_moy="0.3" er_max="6" er_min="0" er_mode="0(19/20)" er_moy_et="1.31">
				<head type="number">VI</head>
				<head type="main">Souvenir du pays de France</head>
				<head type="form">Romance</head>
				<lg n="1" type="sizain" rhyme="aababb">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="1.2">j</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="1.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="2.2">j<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="2.3">li<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="2.6" punct="pe:8">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>ss<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2" punct="vg:2">s<seg phoneme="œ" type="vs" value="1" rule="249" place="2" punct="vg">œu</seg>r</w>, <w n="3.3">qu</w>’<w n="3.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ls</w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="3.6">b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>x</w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="3.8">j<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="2" met="2"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2" punct="pe:2">Fr<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="5.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="5.3" punct="vg:4">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="4" punct="vg">y</seg>s</w>, <w n="5.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="5.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="2" met="2"><w n="6.1" punct="pe:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="pe">ou</seg>rs</rhyme></pgtc></w> !</l>
				</lg>
				<lg n="2" type="sizain" rhyme="aababb">
					<l n="7" num="2.1" lm="8" met="8"><w n="7.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>t</w>-<w n="7.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.6" punct="vg:8">m<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.2" lm="8" met="8"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="8.2">f<seg phoneme="wa" type="vs" value="1" rule="440" place="2">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.5" punct="vg:8">ch<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>mi<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="9" num="2.3" lm="8" met="8"><w n="9.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="9.2">pr<seg phoneme="e" type="vs" value="1" rule="353" place="2">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="9.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="9.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="9.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="9.6" punct="vg:8">j<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</rhyme></pgtc></w>,</l>
					<l n="10" num="2.4" lm="2" met="2"><w n="10.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="10.2" punct="pi:2">ch<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
					<l n="11" num="2.5" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="11.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>si<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="11.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="11.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>cs</w> <w n="11.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</rhyme></pgtc></w></l>
					<l n="12" num="2.6" lm="2" met="2"><w n="12.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="12.2" punct="pt:2">d<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="sizain" rhyme="aababb">
					<l n="13" num="3.1" lm="8" met="8"><w n="13.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="13.2" punct="vg:2">s<seg phoneme="œ" type="vs" value="1" rule="249" place="2" punct="vg">œu</seg>r</w>, <w n="13.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>t</w>-<w n="13.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="3.2" lm="8" met="8"><w n="14.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="14.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="14.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.6" punct="pv:8">D<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="15" num="3.3" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="15.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="15.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<pgtc id="6" weight="6" schema="V[CR" part="1"><seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></pgtc></w> <w n="15.6"><pgtc id="6" weight="6" schema="V[CR" part="2">t<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</rhyme></pgtc></w></l>
					<l n="16" num="3.4" lm="2" met="2"><w n="16.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="16.2" punct="vg:2">M<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="17" num="3.5" lm="8" met="8"><w n="17.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="17.4">s<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="17.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="17.6">r<pgtc id="6" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</rhyme></pgtc></w></l>
					<l n="18" num="3.6" lm="2" met="2"><w n="18.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="18.2" punct="pi:2">j<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="pi">ou</seg>r</rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="4" type="sizain" rhyme="aababb">
					<l n="19" num="4.1" lm="8" met="8"><w n="19.1">T<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>t</w>-<w n="19.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="19.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="19.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>c</w> <w n="19.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>qu<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="485" place="8">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="20" num="4.2" lm="8" met="8"><w n="20.1">Qu</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ffl<seg phoneme="ø" type="vs" value="1" rule="405" place="2">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="20.3">l</w>’<w n="20.4">h<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="21" num="4.3" lm="8" met="8"><w n="21.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="21.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="21.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="21.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rb<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="21.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="21.6">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></rhyme></pgtc></w></l>
					<l n="22" num="4.4" lm="2" met="2"><w n="22.1" punct="vg:2">M<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>b<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="23" num="4.5" lm="8" met="8"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="23.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="23.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="23.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="23.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="23.6">l</w>’<w n="23.7" punct="vg:8"><pgtc id="8" weight="0" schema="[R" part="1"><rhyme label="b" id="8" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></rhyme></pgtc></w>,</l>
					<l n="24" num="4.6" lm="2" met="2"><w n="24.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="24.2" punct="pi:2">b<pgtc id="8" weight="0" schema="R" part="1"><rhyme label="b" id="8" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="315" place="2" punct="pi">eau</seg></rhyme></pgtc></w> ?</l>
				</lg>
				<lg n="5" type="sizain" rhyme="aababb">
					<l n="25" num="5.1" lm="8" met="8"><w n="25.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="25.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="25.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="25.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="25.6" punct="vg:8">H<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>l<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="26" num="5.2" lm="8" met="8"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="26.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="26.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="26.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="26.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="26.7" punct="pi:8">ch<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></rhyme></pgtc></w> ?</l>
					<l n="27" num="5.3" lm="8" met="8"><w n="27.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="27.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="27.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="27.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="27.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="27.6">j<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</rhyme></pgtc></w></l>
					<l n="28" num="5.4" lm="2" met="2"><w n="28.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="28.2" punct="dp:2">p<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="3" punct="dp">e</seg></rhyme></pgtc></w> :</l>
					<l n="29" num="5.5" lm="8" met="8"><w n="29.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="29.2">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="3">y</seg>s</w> <w n="29.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="29.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="29.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="e"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</rhyme></pgtc></w></l>
					<l n="30" num="5.6" lm="2" met="2"><w n="30.1" punct="pe:2">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<pgtc id="10" weight="0" schema="R" part="1"><rhyme label="b" id="10" gender="m" type="a"><seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="pe">ou</seg>rs</rhyme></pgtc></w> !</l>
				</lg>
			</div></body></text></TEI>