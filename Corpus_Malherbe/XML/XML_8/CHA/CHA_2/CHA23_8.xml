<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="CHA">
					<name>
						<forename>François-René</forename>
						<nameLink>de</nameLink>
						<surname>CHATEAUBRIAND</surname>
					</name>
					<date from="1768" to="1848">1768-1848</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>735 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CHA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies diverses</title>
						<author>François-René de Chateaubriand</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/chateaubriandpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1797" to="1827">1797-1827</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas incluse.</p>
				<p>Les notes de l’auteur ont été intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Des corrections métriques ont été apportées sans autres attestations publiées.</p>
				<p>Les vers de la strophe illisible du poème "SERMENT — LES NOIRS DEVANT LE GIBET DE JOHN BROWN" ont été marqués comme inanalysables.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
 					<p>L’orthographe a été vérifiée avec le correcteur Aspell.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CHA23" modus="cp" lm_max="12" metProfile="8, 4+6, 6+6" form="suite périodique" schema="2(abab)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(4/4)" er_moy_et="0.0">
				<head type="number">XIII</head>
				<head type="main">Vers</head>
				<head type="sub">trouvés sur le pont du Rhône</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="1.3" punct="vg:4">m<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg>t</w>, <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="1.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="1.6" punct="pv:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>mm<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="2.2" punct="vg:2">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rs</w>, <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="2.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="2.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.6">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="2.7" punct="pt:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>r</rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>-<w n="3.3" punct="vg:2">j<seg phoneme="ə" type="ee" value="0" rule="e-18">e</seg></w>, <w n="3.4" punct="pe:4">h<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pe" caesura="1">a</seg>s</w> !<caesura></caesura> <w n="3.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5" mp="Lc">eu</seg>t</w>-<w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="3.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="3.8">t<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="3.9" punct="pe:10">v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="382" place="10">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg>s</rhyme></pgtc></w> !</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="4.2" punct="pi:2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="pi pt pt pt">i</seg></w> ?... <w n="4.3">L</w>’<w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" caesura="1">e</seg>r</w><caesura></caesura> <w n="4.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="4.6">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="4.7">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg>s</w> <w n="4.8" punct="pt:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>ffr<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg>r</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="5.2" punct="vg:3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>d</w>, <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>pp<seg phoneme="ɥi" type="vs" value="1" rule="462" place="5" mp="M">u</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="5.5">br<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="5.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="5.8" punct="vg:12">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>qu<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="6.3">tr<seg phoneme="o" type="vs" value="1" rule="433" place="4">o</seg>p</w> <w n="6.4">d</w>’<w n="6.5"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="6.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="6.9" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>p<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a"><seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="vg">o</seg>s</rhyme></pgtc></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="7.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.4" punct="vg:6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="7.7">p<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg></w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="7.9">t<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="6">er</seg></w> <w n="8.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="8.5" punct="pt:8">fl<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e"><seg phoneme="o" type="vs" value="1" rule="438" place="8" punct="pt">o</seg>ts</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>