<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA100" modus="sp" lm_max="8" metProfile="8, 6" form="suite périodique" schema="2(ababcdcd)" er_moy="0.0" er_max="0" er_min="0" er_mode="0(8/8)" er_moy_et="0.0">
					<head type="main">AMICA SILENTIA</head>
					<lg n="1" type="huitain" rhyme="ababcdcd">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="pe:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="pe">en</seg>s</w> ! <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">nu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="1.5" punct="vg:6">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="1.6">l</w>’<w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r</w> <w n="1.8" punct="vg:8">p<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>r</rhyme></pgtc></w>,</l>
						<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="2.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="2.5" punct="pv:6">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">l<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">gl<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="3.5">l</w>’<w n="3.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>z<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>r</rhyme></pgtc></w>,</l>
						<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="4.3">c<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="4.5" punct="pv:6">n<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="5.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="5.3">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rf<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="m" type="a"><seg phoneme="œ̃" type="vs" value="1" rule="268" place="8">um</seg></rhyme></pgtc></w></l>
						<l n="6" num="1.6" lm="6" met="6"><space unit="char" quantity="4"></space><w n="6.1">M<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="6.3" punct="pv:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg>s</rhyme></pgtc></w> ;</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">N<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="7.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>rs</w> <w n="7.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="7.4">n</w>’<w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="7.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="7.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="7.8">qu</w>’<w n="7.9" punct="vg:8"><pgtc id="3" weight="0" schema="[R"><rhyme label="c" id="3" gender="m" type="e"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" punct="vg">un</seg></rhyme></pgtc></w>,</l>
						<l n="8" num="1.8" lm="6" met="6"><space unit="char" quantity="4"></space><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t</w> <w n="8.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="8.4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="8.5" punct="pt:6">p<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>tr<pgtc id="4" weight="0" schema="R"><rhyme label="d" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg>s</rhyme></pgtc></w>.</l>
					</lg>
					<lg n="2" type="huitain" rhyme="ababcdcd">
						<l n="9" num="2.1" lm="8" met="8"><w n="9.1">P<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="9.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="9.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>s</w> <w n="9.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="9.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="9.7" punct="vg:8">br<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
						<l n="10" num="2.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="10.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="10.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="10.5" punct="pv:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="a"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
						<l n="11" num="2.3" lm="8" met="8"><w n="11.1" punct="pe:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="pe">en</seg>s</w> ! <w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="11.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="11.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="11.6" punct="vg:8">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>dr<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</rhyme></pgtc></w>,</l>
						<l n="12" num="2.4" lm="6" met="6"><space unit="char" quantity="4"></space><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3" punct="vg:3">pr<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg>s</w>, <w n="12.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="12.5">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.6" punct="vg:6">s<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="f" type="e"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="13" num="2.5" lm="8" met="8"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="13.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd</w> <w n="13.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="13.4">ru<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="13.5" punct="vg:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="m" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>rd</rhyme></pgtc></w>,</l>
						<l n="14" num="2.6" lm="6" met="6"><space unit="char" quantity="4"></space><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="14.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="14.4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="14.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="14.6" punct="vg:6">tr<pgtc id="8" weight="0" schema="R"><rhyme label="d" id="8" gender="f" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="15" num="2.7" lm="8" met="8"><w n="15.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="15.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="15.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="15.5" punct="vg:8">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s<pgtc id="7" weight="0" schema="R"><rhyme label="c" id="7" gender="m" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>rd</rhyme></pgtc></w>,</l>
						<l n="16" num="2.8" lm="6" met="6"><space unit="char" quantity="4"></space><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="16.2">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="16.3">d</w>’<w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5" punct="pe:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>s<pgtc id="8" weight="0" schema="R"><rhyme label="d" id="8" gender="f" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></rhyme></pgtc></w> !</l>
					</lg>
				</div></body></text></TEI>