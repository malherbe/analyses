<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA90" modus="sm" lm_max="8" metProfile="8" form="sonnet classique, prototype 1" schema="abba abba ccd eed" er_moy="1.71" er_max="6" er_min="0" er_mode="0(3/7)" er_moy_et="1.98">
					<head type="main">SOLEIL COUCHÉ</head>
					<head type="form">SONNET</head>
					<lg n="1" rhyme="abbaabbaccdeed">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="1.3">s</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="1.5">pr<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></rhyme></pgtc></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="2.2">l</w>’<w n="2.3">h<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="2.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6" punct="dp:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>v<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="3.2" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">aî</seg>t</w>, <w n="3.3"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="3.4">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5" punct="dp:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">C</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="4.5">d</w>’<w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="4.7">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="4.8">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="4.9">d</w>’<w n="4.10" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
						<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="4"></space><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.4" punct="vg:8">cl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></rhyme></pgtc></w>,</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="6.2">qu</w>’<w n="6.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>r<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg></w> <w n="7.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="7.3" punct="vg:5">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rv<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>t</w>, <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="7.5">c<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="8.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rds</w> <w n="8.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="8.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="8.5">qu</w>’<w n="8.6"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.8" punct="pt:8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg><pgtc id="3" weight="2" schema="CR">tt<rhyme label="a" id="3" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
						<l n="9" num="1.9" lm="8" met="8"><space unit="char" quantity="4"></space><w n="9.1">T<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="9.6" punct="vg:8">ch<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="5" weight="2" schema="CR">r<rhyme label="c" id="5" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="10" num="1.10" lm="8" met="8"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="10.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt</w> <w n="10.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="10.4"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="5" weight="2" schema="CR">tr<rhyme label="c" id="5" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="469" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
						<l n="11" num="1.11" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">l</w>’<w n="11.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="11.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="11.6" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<pgtc id="6" weight="6" schema="VCR"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<rhyme label="d" id="6" gender="m" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>r</rhyme></pgtc></w>,</l>
						<l n="12" num="1.12" lm="8" met="8"><space unit="char" quantity="4"></space><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.4">m<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="12.5" punct="vg:8">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="a"><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="13" num="1.13" lm="8" met="8"><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="13.3">tr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4" punct="vg:8">l<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<pgtc id="7" weight="0" schema="R"><rhyme label="e" id="7" gender="f" type="e"><seg phoneme="ø" type="vs" value="1" rule="403" place="8">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
						<l n="14" num="1.14" lm="8" met="8"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="14.2">cl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="14.5" punct="pt:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<pgtc id="6" weight="6" schema="VCR"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<rhyme label="d" id="6" gender="m" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>r</rhyme></pgtc></w>.</l>
					</lg>
				</div></body></text></TEI>