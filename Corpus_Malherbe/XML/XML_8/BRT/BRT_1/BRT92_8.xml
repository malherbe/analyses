<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT92" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="5[abab]" er_moy="2.6" er_max="6" er_min="0" er_mode="0(5/10)" er_moy_et="2.84">
				<head type="number">XCII</head>
				<lg n="1" type="regexp" rhyme="abababababababababab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="1.3">d</w>’<w n="1.4"><seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="1.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="1.7"><pgtc id="1" weight="6" schema="[VCR"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rr<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="2.4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6">en</seg>t</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.6" punct="pt:8">l<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="pt">oin</seg></rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="3.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="3.6">l<pgtc id="1" weight="6" schema="V[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></pgtc></w> <w n="3.7" punct="vg:8"><pgtc id="1" weight="6" schema="V[CR" part="2">r<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="4.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.7" punct="pt:8">f<pgtc id="2" weight="0" schema="R" part="1"><rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" punct="pt">oin</seg></rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="5.3">l<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rd</w> <w n="5.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.8">fl<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="y" type="vs" value="1" rule="445" place="8">û</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">P<seg phoneme="ɛ" type="vs" value="1" rule="339" place="1">a</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>c<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>t</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6" punct="pt:8">ch<pgtc id="4" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></rhyme></pgtc></w>.</l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="7.2">l</w>’<w n="7.3" punct="vg:2"><seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>rs</w>, <w n="7.4" punct="vg:4">h<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" punct="vg">e</seg>r</w>, <w n="7.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="7.6"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.7" punct="dp:8">ch<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6">oin</seg>t</w> <w n="8.5" punct="pt:8">d<pgtc id="4" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="pt">ain</seg></rhyme></pgtc></w>.</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2" punct="vg:4">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="9.4">c</w>’<w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="9.6" punct="dp:8">f<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></rhyme></pgtc></w> :</l>
					<l n="10" num="1.10" lm="8" met="8"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>t</w> <w n="10.4" punct="vg:3">t<seg phoneme="o" type="vs" value="1" rule="415" place="3" punct="vg">ô</seg>t</w>, <w n="10.5">l</w>’<w n="10.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="10.7">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.8" punct="pv:8">l<pgtc id="6" weight="6" schema="VCR" part="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>gt<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="pv">em</seg>ps</rhyme></pgtc></w> ;</l>
					<l n="11" num="1.11" lm="8" met="8"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="11.4">s</w>’<w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="11.6" punct="vg:5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="5" punct="vg">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="11.8" punct="vg:8">b<pgtc id="5" weight="0" schema="R" part="1"><rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="1.12" lm="8" met="8"><w n="12.1">B<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="12.2" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="12.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="12.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>rs</w> <w n="12.5" punct="pt:8">c<pgtc id="6" weight="6" schema="VCR" part="1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="pt">en</seg>ts</rhyme></pgtc></w>.</l>
					<l n="13" num="1.13" lm="8" met="8"><w n="13.1">L</w>’<w n="13.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="13.5">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" punct="vg">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>, <w n="13.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.8" punct="vg:8">p<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="a" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="14" num="1.14" lm="8" met="8"><w n="14.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="14.2">m<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="14.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="14.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="14.5">l</w>’<w n="14.6" punct="pv:8">h<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="8" weight="2" schema="CR" part="1">v<rhyme label="b" id="8" gender="m" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pv">e</seg>r</rhyme></pgtc></w> ;</l>
					<l n="15" num="1.15" lm="8" met="8"><w n="15.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="15.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r</w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="15.4">m<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="15.7" punct="vg:8">d<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="e" stanza="4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="16" num="1.16" lm="8" met="8"><w n="16.1">S</w>’<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="16.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="16.5" punct="pt:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="8" weight="2" schema="CR" part="1">v<rhyme label="b" id="8" gender="m" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="pt">e</seg>rt</rhyme></pgtc></w>.</l>
					<l n="17" num="1.17" lm="8" met="8"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="17.2">M<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="17.3">gr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="17.6" punct="pe:8">p<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="a" stanza="5"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ps">e</seg></rhyme></pgtc></w> ! …</l>
					<l n="18" num="1.18" lm="8" met="8"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="18.2">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="18.3">d</w>’<w n="18.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="18.5" punct="vg:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rl<pgtc id="10" weight="6" schema="VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<rhyme label="b" id="10" gender="m" type="a" stanza="5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg></rhyme></pgtc></w>,</l>
					<l n="19" num="1.19" lm="8" met="8"><w n="19.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="19.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="19.3" punct="vg:4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="19.6" punct="vg:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="e" stanza="5"><seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="20" num="1.20" lm="8" met="8"><w n="20.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3" punct="vg:4">b<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>r</w>, <w n="20.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">am</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="20.5" punct="pt:8">b<pgtc id="10" weight="6" schema="VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>tt<rhyme label="b" id="10" gender="m" type="e" stanza="5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Ce qui vient de la flûte retourne au tambour.</note>
					</closer>
			</div></body></text></TEI>