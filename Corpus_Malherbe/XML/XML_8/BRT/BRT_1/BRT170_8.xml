<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉNIGMES</head><head type="sub_1">BOTANIQUE<lb></lb>(<hi rend="smallcap">EMBLÈMES</hi>.)</head><div type="poem" key="BRT170" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="2[abab] 1[aa] 1[abba]" er_moy="1.43" er_max="2" er_min="0" er_mode="2(5/7)" er_moy_et="0.9">
				<head type="number">XX</head>
				<lg n="1" type="regexp" rhyme="ababaaabbaabab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="1.5" punct="vg:8">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="7">eu</seg><pgtc id="1" weight="2" schema="CR">r<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">A</seg>tl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="2.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg></w> <w n="2.6" punct="pv:8">h<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="2" weight="2" schema="CR">l<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pv">eu</seg>x</rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="3.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="3.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="3.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="3.5">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6" punct="vg:8">v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg><pgtc id="1" weight="2" schema="CR">r<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rds</w> <w n="4.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="4.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="4.5" punct="pt:8">p<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg><pgtc id="2" weight="2" schema="CR">l<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="5.5">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="5.6" punct="vg:8"><pgtc id="3" weight="2" schema="[CR">v<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="6.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="3" weight="2" schema="CR">v<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">D</w>’<w n="7.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>xqu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="7.6">fru<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ts</w> <w n="7.7" punct="pt:8">l<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>rds</rhyme></pgtc></w>.</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="8.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="8.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rd</w> <w n="8.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.7" punct="vg:8">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="f" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="9.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="9.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="9.5">s</w>’<w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="9.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>g<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="f" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="10" num="1.10" lm="8" met="8"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="10.2">pl<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="10.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="10.6">bru<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ts</w> <w n="10.7" punct="pt:8">s<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>rds</rhyme></pgtc></w>.</l>
					<l n="11" num="1.11" lm="8" met="8"><w n="11.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" punct="vg">in</seg></w>, <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6">oi</seg></w> <w n="11.5" punct="vg:8">gr<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg><pgtc id="6" weight="2" schema="CR">p<rhyme label="a" id="6" gender="f" type="a" stanza="4"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="12" num="1.12" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>m<seg phoneme="a" type="vs" value="1" rule="307" place="2">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.3">cr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="12.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg><pgtc id="7" weight="2" schema="C[R" part="1">s</pgtc></w> <w n="12.5" punct="vg:8"><pgtc id="7" weight="2" schema="C[R" part="2"><rhyme label="b" id="7" gender="m" type="a" stanza="4"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg>x</rhyme></pgtc></w>,</l>
					<l n="13" num="1.13" lm="8" met="8"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="13.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="13.3">s<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>rs</w> <w n="13.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg><pgtc id="6" weight="2" schema="CR" part="1">p<rhyme label="a" id="6" gender="f" type="e" stanza="4"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="14" num="1.14" lm="8" met="8"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2" punct="vg:3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>s</w>, <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="14.6">d</w>’<w n="14.7" punct="pt:8"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg><pgtc id="7" weight="2" schema="CR" part="1">s<rhyme label="b" id="7" gender="m" type="e" stanza="4"><seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg>x</rhyme></pgtc></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Ténériffe.</note>
					</closer>
			</div></body></text></TEI>