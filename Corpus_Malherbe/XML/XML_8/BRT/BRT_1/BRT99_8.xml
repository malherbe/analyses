<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT99" modus="cp" lm_max="12" metProfile="6+6, (6)" form="suite de strophes" schema="2[aa] 3[abab]" er_moy="2.75" er_max="6" er_min="0" er_mode="0(3/8)" er_moy_et="2.63">
				<head type="number">XCIX</head>
				<lg n="1" type="regexp" rhyme="aaaaabababababab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="1.2" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg></w>, <w n="1.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="1.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="1.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="1.7">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="1.8">v<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="C">ou</seg>s</w> <w n="1.9" punct="dp:12">d<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
					<l n="2" num="1.2" lm="6"><space unit="char" quantity="12"></space><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>rd<seg phoneme="a" type="vs" value="1" rule="365" place="2">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="3">en</seg>t</w> <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></rhyme></pgtc></w></l>
					<l part="I" n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="3.2" punct="pe:4">f<seg phoneme="a" type="vs" value="1" rule="193" place="3">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pe" mp="F">e</seg></w> ! </l>
					<l part="F" n="3" num="1.3" lm="12" met="6+6">— <w n="3.3" punct="pe:6">Br<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="6" punct="pe" caesura="1">o</seg></w> !<caesura></caesura> <w n="3.4" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="vg">ain</seg></w>, <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M/mp">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="10" mp="M/mp">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="11" mp="Lp">ez</seg></w>-<w n="3.6" punct="pt:12">v<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="m" type="a" stanza="2"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>s</rhyme></pgtc></w>.</l>
					<l n="4" num="1.4" lm="12" met="6+6">— <w n="4.1" punct="pv:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="pv">i</seg></w> ; <w n="4.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="4.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="4.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="4.5">t<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10">en</seg>t</w> <w n="4.8" punct="pt:12">j<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<pgtc id="2" weight="0" schema="R"><rhyme label="a" id="2" gender="m" type="e" stanza="2"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>x</rhyme></pgtc></w>.</l>
					<l part="I" n="5" num="1.5" lm="12" met="6+6">— <w n="5.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="5.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="5.4" punct="pt:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pt" caesura="1">a</seg>s</w>.<caesura></caesura> </l>
					<l part="F" n="5" num="1.5" lm="12" met="6+6">— <w n="5.5">C</w>’<w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="5.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="5.9">s<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>l<pgtc id="3" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="a" id="3" gender="f" type="a" stanza="3"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="6.5">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="6.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="6.8">v<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>t</w> <w n="6.9">m</w>’<w n="6.10" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>xpl<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="m" type="a" stanza="3"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg></rhyme></pgtc></w>.</l>
					<l part="I" n="7" num="1.7" lm="12" met="6+6">— <w n="7.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M/mp">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="7.2" punct="vg:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>s</w>, <w n="7.3" punct="pt:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" punct="pt" caesura="1">o</seg>rs</w>.<caesura></caesura> </l>
					<l part="F" n="7" num="1.7" lm="12" met="6+6">— <w n="7.4">J</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="7.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="7.7">d</w>’<w n="7.8"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="7.9" punct="pv:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>b<pgtc id="3" weight="6" schema="VCR"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<rhyme label="a" id="3" gender="f" type="e" stanza="3"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">J</w>’<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="497" place="1">y</seg></w> <w n="8.3" punct="pt:2">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2" punct="pt">en</seg>s</w>. <w n="8.4">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="8.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M/mp">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="5" mp="Lp">ez</seg></w>-<w n="8.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="8.7">qu</w>’<w n="8.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="8.9">v<seg phoneme="œ" type="vs" value="1" rule="406" place="8">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.10">s</w>’<w n="8.11"><seg phoneme="i" type="vs" value="1" rule="497" place="10">y</seg></w> <w n="8.12" punct="pi:12">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="11" mp="M">ê</seg><pgtc id="4" weight="2" schema="CR">t<rhyme label="b" id="4" gender="m" type="e" stanza="3"><seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pi">er</seg></rhyme></pgtc></w> ?</l>
					<l part="I" n="9" num="1.9" lm="12" met="6+6">— <w n="9.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="9.4" punct="pt:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pt" caesura="1">a</seg>s</w>.<caesura></caesura> </l>
					<l part="F" n="9" num="1.9" lm="12" met="6+6">— <w n="9.5">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></w> <w n="9.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="9.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="9.8">l<pgtc id="5" weight="6" schema="V[CR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></pgtc></w> <w n="9.9"><pgtc id="5" weight="6" schema="V[CR" part="2">v<rhyme label="a" id="5" gender="f" type="a" stanza="4"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3" punct="vg:4">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="10.4">pr<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="10.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="10.9" punct="pe:12">s<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="a" stanza="4"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="12" punct="pe">oin</seg></rhyme></pgtc></w> !</l>
					<l part="I" n="11" num="1.11" lm="12" met="6+6">— <w n="11.1" punct="ps:3">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="ps">ez</seg></w>… </l>
					<l part="F" n="11" num="1.11" lm="12" met="6+6">— <w n="11.2">S<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="11.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="11.4" punct="vg:6">f<seg phoneme="a" type="vs" value="1" rule="193" place="6" punct="vg" caesura="1">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="11.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="11.6" punct="vg:8">j<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</w>, <w n="11.7">m</w>’<w n="11.8"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="11.9" punct="pe:12">r<pgtc id="5" weight="6" schema="VCR" part="1"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<rhyme label="a" id="5" gender="f" type="e" stanza="4"><seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="12.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ts</w> <w n="12.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>br<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="12.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="12.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="12.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="12.8" punct="pe:12">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>s<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="e" stanza="4"><seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="12" punct="pe">oin</seg></rhyme></pgtc></w> !</l>
					<l part="I" n="13" num="1.13" lm="12" met="6+6">— <w n="13.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="13.3" punct="ps:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="ps">i</seg></w>… </l>
					<l part="F" n="13" num="1.13" lm="12" met="6+6">— <w n="13.4">J</w>’<w n="13.5" punct="pt:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="pt" caesura="1">en</seg>ds</w>.<caesura></caesura> <w n="13.6">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="13.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="13.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="13.9">f<pgtc id="7" weight="6" schema="VCR" part="1"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rt<rhyme label="a" id="7" gender="f" type="a" stanza="5"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">I</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="14.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="14.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="14.5">pi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="14.6" punct="pi:12">n<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="8" weight="2" schema="CR" part="1">v<rhyme label="b" id="8" gender="m" type="a" stanza="5"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pi">eu</seg></rhyme></pgtc></w> ?</l>
					<l n="15" num="1.15" lm="12" met="6+6">— <w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="15.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="C">ou</seg>s</w> <w n="15.3" punct="pt:6">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pt" caesura="1">er</seg></w>.<caesura></caesura> <w n="15.4">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="15.5">j<seg phoneme="ø" type="vs" value="1" rule="398" place="8" mp="Lp">eu</seg></w>-<w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="15.7">m</w>’<w n="15.8" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="10" mp="M">im</seg>p<pgtc id="7" weight="6" schema="VCR" part="1"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rt<rhyme label="a" id="7" gender="f" type="e" stanza="5"><seg phoneme="y" type="vs" value="1" rule="453" place="12">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="16.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="16.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="16.4" punct="vg:6">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="16.5" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></w>, <w n="16.6">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="16.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="16.8">l</w>’<w n="16.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="16.10" punct="pe:12"><pgtc id="8" weight="2" schema="[CR" part="1">v<rhyme label="b" id="8" gender="m" type="e" stanza="5"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg>t</rhyme></pgtc></w> ! »</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Ce que femme veut, Dieu le veut.</note>
					</closer>
			</div></body></text></TEI>