<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT122" modus="cp" lm_max="10" metProfile="2, 4+6" form="suite périodique" schema="3(abab)" er_moy="1.67" er_max="2" er_min="0" er_mode="2(5/6)" er_moy_et="0.75">
				<head type="number">XXII</head>
				<lg n="1" type="quatrain" rhyme="abab">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="1.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>s</w><caesura></caesura> <w n="1.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="1.6" punct="vg:7">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="vg">on</seg></w>, <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg></w> <w n="1.8" punct="pe:10">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>r<pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="2.2">f<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>l</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="2.4">l</w>’<w n="2.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rp<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg></w> <w n="2.7" punct="pt:10"><pgtc id="2" weight="2" schema="[CR">v<rhyme label="b" id="2" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="3.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="3.3" punct="vg:6">h<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>rd</w>, <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="3.5">l</w>’<w n="3.6" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9" mp="M">en</seg><pgtc id="1" weight="2" schema="CR">t<rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="2" met="2"><space unit="char" quantity="16"></space><w n="4.1" punct="ps:2">R<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg><pgtc id="2" weight="2" schema="CR">v<rhyme label="b" id="2" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="ps">an</seg>t</rhyme></pgtc></w>…</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2" type="quatrain" rhyme="abab">
					<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2">p<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>tr<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg></w><caesura></caesura> <w n="5.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="5.5">c<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="5.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">om</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="f" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="6.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" caesura="1">o</seg>r</w><caesura></caesura> <w n="6.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8">en</seg></w> <w n="6.8">p<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg><pgtc id="4" weight="2" schema="CR">gn<rhyme label="b" id="4" gender="m" type="a"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></rhyme></pgtc></w></l>
					<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="7.2">d</w>’<w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="7.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="7.5">d</w>’<w n="7.6"><seg phoneme="o" type="vs" value="1" rule="315" place="4" caesura="1">eau</seg></w><caesura></caesura> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="C">au</seg></w> <w n="7.9">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="7.10">d</w>’<w n="7.11"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.12" punct="dp:10"><pgtc id="3" weight="2" schema="[CR">t<rhyme label="a" id="3" gender="f" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></rhyme></pgtc></w> :</l>
					<l n="8" num="2.4" lm="2" met="2"><space unit="char" quantity="16"></space><w n="8.1">L</w>’<w n="8.2" punct="pt:2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">O</seg><pgtc id="4" weight="2" schema="CR">gn<rhyme label="b" id="4" gender="m" type="e"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="pt">on</seg></rhyme></pgtc></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3" type="quatrain" rhyme="abab">
					<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="9.4">f<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="9.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>gr<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="3.2" lm="10" met="4+6"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="10.2">s</w>’<w n="10.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="10.4">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="10.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="10.6">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6">e</seg>d</w> <w n="10.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>s</w> <w n="10.8" punct="pt:10">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg><pgtc id="6" weight="2" schema="CR">m<rhyme label="b" id="6" gender="m" type="a"><seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10" punct="pt">en</seg>t</rhyme></pgtc></w>.</l>
					<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="2">en</seg>t</w> <w n="11.2">n<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="11.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5" mp="C">e</seg>t</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>cc<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>t</w> <w n="11.5" punct="pi:10">f<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>n<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
					<l n="12" num="3.4" lm="2" met="2"><space unit="char" quantity="16"></space><w n="12.1" punct="pi:2">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg><pgtc id="6" weight="2" schema="CR">mm<rhyme label="b" id="6" gender="m" type="e"><seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="2" punct="pi">en</seg>t</rhyme></pgtc></w> ?</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Fou-lure.</note>
					</closer>
			</div></body></text></TEI>