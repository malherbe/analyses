<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT123" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique" schema="2(aabb) 1(aabbcc)" er_moy="1.43" er_max="6" er_min="0" er_mode="0(4/7)" er_moy_et="2.06">
				<head type="number">XXIII</head>
				<lg n="1" type="quatrain" rhyme="aabb">
					<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="1.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r</w> <w n="1.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="1.7" punct="vg:8">p<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>r</rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="2.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="2.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="2.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="2.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="2.7">l</w>’<w n="2.8" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>z<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="m" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pv">u</seg>r</rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="3.4">bru<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>ts</w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="3.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="3.7"><seg phoneme="i" type="vs" value="1" rule="497" place="7" mp="C">y</seg></w> <w n="3.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="3.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="P">a</seg>r</w> <w n="3.10" punct="vg:12">b<pgtc id="2" weight="6" schema="VCR"><seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>ff<rhyme label="b" id="2" gender="f" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="4.2">n</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="4.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="4.5">qu</w>’<w n="4.6"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="4.7">l</w>’<w n="4.8"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>t</w><caesura></caesura> <w n="4.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="4.10">cl<seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="4.11" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<pgtc id="2" weight="6" schema="VCR"><seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>ff<rhyme label="b" id="2" gender="f" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</rhyme></pgtc></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2" type="quatrain" rhyme="aabb">
					<l n="5" num="2.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="5.2">l</w>’<w n="5.3" punct="pi:4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="pi">er</seg></w> ? <w n="5.4">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="5.5">f<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="5.6">b<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>nh<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="a"><seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</rhyme></pgtc></w></l>
					<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="6.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.6" punct="pv:8">c<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="m" type="e"><seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="pv">œu</seg>r</rhyme></pgtc></w> ;</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="7.2">h<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="7.3">s</w>’<w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="7.8">l<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>m<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="a"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</rhyme></pgtc></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="8.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="8.5"><seg phoneme="i" type="vs" value="1" rule="497" place="7" mp="C">y</seg></w> <w n="8.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="8.7" punct="pe:12">h<seg phoneme="i" type="vs" value="1" rule="493" place="10" mp="M">y</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>cr<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="f" type="e"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg>s</rhyme></pgtc></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3" type="sizain" rhyme="aabbcc">
					<l n="9" num="3.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="9.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="9.5">f<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="5" weight="2" schema="CR">cl<rhyme label="a" id="5" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pv">ai</seg>r</rhyme></pgtc></w> ;</l>
					<l n="10" num="3.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="10.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="10.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="10.6"><pgtc id="5" weight="2" schema="[C[R" part="1">l</pgtc></w>’<w n="10.7" punct="pv:8"><pgtc id="5" weight="2" schema="[C[R" part="2"><rhyme label="a" id="5" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pv">ai</seg>r</rhyme></pgtc></w> ;</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="11.2">s</w>’<w n="11.3" punct="pe:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="pe ps">a</seg>t</w> ! … <w n="11.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="11.6">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>l</w><caesura></caesura> <w n="11.7">r<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="11.9">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="11.10">p<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="f" type="a"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="12.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="12.4" punct="vg:6">fl<seg phoneme="o" type="vs" value="1" rule="438" place="6" punct="vg" caesura="1">o</seg>t</w>,<caesura></caesura> <w n="12.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="12.7" punct="vg:9">cr<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg></w>, <w n="12.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="12.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="12.10" punct="pe:12">r<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="f" type="e"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></rhyme></pgtc></w> !</l>
					<l n="13" num="3.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="13.1">L</w>’<w n="13.2"><seg phoneme="e" type="vs" value="1" rule="354" place="1">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="13.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="13.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="13.6" punct="pv:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg><pgtc id="7" weight="2" schema="CR" part="1">ff<rhyme label="c" id="7" gender="m" type="a"><seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="pv">e</seg>t</rhyme></pgtc></w> ;</l>
					<l n="14" num="3.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>h</w> <w n="14.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="14.4" punct="pe:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<pgtc id="7" weight="2" schema="CR" part="1">f<rhyme label="c" id="7" gender="m" type="e"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pe">ai</seg>t</rhyme></pgtc></w> !</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Cime-terre.</note>
					</closer>
			</div></body></text></TEI>