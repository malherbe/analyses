<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT93" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite de strophes" schema="1[abbab] 6[abab]" er_moy="1.87" er_max="6" er_min="0" er_mode="0(9/15)" er_moy_et="2.58">
				<head type="number">XCIII</head>
				<lg n="1" type="regexp" rhyme="abbab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="1.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="1.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="1.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rc<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>s</w> <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>p<seg phoneme="e" type="vs" value="1" rule="347" place="11">er</seg></w> <w n="1.8">d</w>’<w n="1.9"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">N</w>’<w n="2.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3" punct="vg:5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="5" punct="vg">en</seg>t</w>, <w n="2.4">n</w>’<w n="2.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="6">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>r<pgtc id="2" weight="0" schema="[R" part="1">t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></pgtc></w> <w n="2.6" punct="pt:8"><pgtc id="2" weight="0" schema="[R" part="2"><rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="u" type="vs" value="1" rule="426" place="8" punct="pt">où</seg></rhyme></pgtc></w>.</l>
					<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="3.2">d</w>’<w n="3.3" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="3.5">bru<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="3.6">c</w>’<w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="3.8" punct="vg:8"><pgtc id="2" weight="0" schema="R" part="1">t<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>t</rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="4.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="4.5">b<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>ss<seg phoneme="y" type="vs" value="1" rule="457" place="6" caesura="1">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="4.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="4.7">qu</w>’<w n="4.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="4.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="4.10">tr<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="4.11" punct="vg:12">b<pgtc id="1" weight="0" schema="R" part="1"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>f<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="5.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="5.4"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="5.5">c<seg phoneme="wa" type="vs" value="1" rule="420" place="8" mp="M">oi</seg>ff<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="5.7" punct="pt:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg><pgtc id="2" weight="2" schema="CR" part="1">t<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="u" type="vs" value="1" rule="426" place="12" punct="pt">ou</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="regexp" rhyme="abababababababababababab">
					<l n="6" num="2.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.3" punct="vg:5">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5" punct="vg">e</seg>il</w>, <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.5">n</w>’<w n="6.6"><pgtc id="3" weight="0" schema="[R" part="1"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="7" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="7.2">l</w>’<w n="7.3" punct="vg:3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="7.4">n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="7.5">l</w>’<w n="7.6" punct="pv:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<pgtc id="4" weight="6" schema="VCR" part="1"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pv">é</seg></rhyme></pgtc></w> ;</l>
					<l n="8" num="2.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="8.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t</w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="8.6">d</w>’<w n="8.7"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>bl<pgtc id="3" weight="0" schema="R" part="1"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="9" num="2.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="9.1">Qu</w>’<w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="9.3">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>t</w> <w n="9.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w> <w n="9.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<pgtc id="4" weight="6" schema="VCR" part="1"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
					<l n="10" num="2.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="10.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>x</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.6">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg><pgtc id="5" weight="2" schema="CR" part="1">mm<rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="11" num="2.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="11.3">d</w>’<w n="11.4" punct="vg:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="11.5">c</w>’<w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="11.7">tr<seg phoneme="o" type="vs" value="1" rule="433" place="7">o</seg>p</w> <w n="11.8" punct="pe:8">p<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg></rhyme></pgtc></w> !</l>
					<l n="12" num="2.7" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="12.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>cl<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg><pgtc id="5" weight="2" schema="CR" part="1">m<rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="13" num="2.8" lm="8" met="8"><space unit="char" quantity="8"></space><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">pl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="13.4">pl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="13.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="13.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="13.8" punct="pt:8">li<pgtc id="6" weight="0" schema="R" part="1"><rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></rhyme></pgtc></w>.</l>
					<l n="14" num="2.9" lm="8" met="8"><space unit="char" quantity="8"></space><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="14.4" punct="vg:6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="14.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>t</w>-<w n="14.6" punct="vg:8"><pgtc id="7" weight="0" schema="[R" part="1"><rhyme label="a" id="7" gender="f" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="15" num="2.10" lm="8" met="8"><space unit="char" quantity="8"></space><w n="15.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="15.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.5" punct="pv:8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>s<pgtc id="8" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="8" gender="m" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pv">ai</seg>t</rhyme></pgtc></w> ;</l>
					<l n="16" num="2.11" lm="8" met="8"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="16.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="16.6" punct="vg:8">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<pgtc id="7" weight="0" schema="R" part="1"><rhyme label="a" id="7" gender="f" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="17" num="2.12" lm="8" met="8"><space unit="char" quantity="8"></space><w n="17.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="17.2" punct="vg:4">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>t</w>, <w n="17.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.4" punct="pt:8">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<pgtc id="8" weight="6" schema="VCR" part="1"><seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<rhyme label="b" id="8" gender="m" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>t</rhyme></pgtc></w>.</l>
					<l n="18" num="2.13" lm="8" met="8"><space unit="char" quantity="8"></space><w n="18.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="18.2">n</w>’<w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="18.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="18.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="18.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="18.7">r<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="a" stanza="5"><seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="19" num="2.14" lm="8" met="8"><space unit="char" quantity="8"></space><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="19.5" punct="pv:8">s<pgtc id="10" weight="6" schema="VCR" part="1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="b" id="10" gender="m" type="a" stanza="5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pv">on</seg></rhyme></pgtc></w> ;</l>
					<l n="20" num="2.15" lm="8" met="8"><space unit="char" quantity="8"></space><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="20.3">d</w>’<w n="20.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="20.5">br<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd</w> <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="20.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<pgtc id="9" weight="0" schema="R" part="1"><rhyme label="a" id="9" gender="f" type="e" stanza="5"><seg phoneme="y" type="vs" value="1" rule="457" place="8">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="21" num="2.16" lm="8" met="8"><space unit="char" quantity="8"></space><w n="21.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="21.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="21.3">s<seg phoneme="œ" type="vs" value="1" rule="406" place="3">eu</seg>il</w> <w n="21.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="21.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="21.6" punct="pv:8">m<pgtc id="10" weight="6" schema="VCR" part="1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="b" id="10" gender="m" type="e" stanza="5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pv">on</seg></rhyme></pgtc></w> ;</l>
					<l n="22" num="2.17" lm="8" met="8"><space unit="char" quantity="8"></space><w n="22.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="22.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="22.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="22.4" punct="dp:5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="5" punct="dp">om</seg></w> : <hi rend="ital"><w n="22.5" punct="vg:8">Cl<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>p<pgtc id="11" weight="0" schema="R" part="1"><rhyme label="a" id="11" gender="f" type="a" stanza="6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w></hi>,</l>
					<l n="23" num="2.18" lm="8" met="8"><space unit="char" quantity="8"></space><w n="23.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="23.2">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="23.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="23.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="23.5">j<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="m" type="a" stanza="6"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</rhyme></pgtc></w></l>
					<l n="24" num="2.19" lm="8" met="8"><space unit="char" quantity="8"></space><w n="24.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="24.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.5" punct="vg:8">th<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><pgtc id="11" weight="0" schema="R" part="1"><rhyme label="a" id="11" gender="f" type="e" stanza="6"><seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="25" num="2.20" lm="8" met="8"><space unit="char" quantity="8"></space><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="25.2">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg></w> <w n="25.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="25.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="25.5">d</w>’<w n="25.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="25.7" punct="pt:8">c<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>f<pgtc id="12" weight="0" schema="R" part="1"><rhyme label="b" id="12" gender="m" type="e" stanza="6"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
					<l n="26" num="2.21" lm="8" met="8"><space unit="char" quantity="8"></space><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="26.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="26.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="26.4" punct="vg:5">j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rn<seg phoneme="o" type="vs" value="1" rule="318" place="5" punct="vg">au</seg>x</w>, <w n="26.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="26.6">m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>m<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="a" stanza="7"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="27" num="2.22" lm="8" met="8"><space unit="char" quantity="8"></space><w n="27.1" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="353" place="1">E</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>t</w>, <w n="27.2">c</w>’<w n="27.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="27.4" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>t<pgtc id="14" weight="6" schema="VCR" part="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d<rhyme label="b" id="14" gender="m" type="a" stanza="7"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg></rhyme></pgtc></w>,</l>
					<l n="28" num="2.23" lm="8" met="8"><space unit="char" quantity="8"></space><w n="28.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="28.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="28.3">h<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<pgtc id="13" weight="0" schema="R" part="1"><rhyme label="a" id="13" gender="f" type="e" stanza="7"><seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</rhyme></pgtc></w></l>
					<l n="29" num="2.24" lm="8" met="8"><space unit="char" quantity="8"></space><w n="29.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="29.2" punct="vg:2">f<seg phoneme="u" type="vs" value="1" rule="426" place="2" punct="vg">ou</seg></w>, <w n="29.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="29.4" punct="vg:5">n<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg">é</seg></w>, <w n="29.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="29.6" punct="pt:8">p<pgtc id="14" weight="6" schema="VCR" part="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d<rhyme label="b" id="14" gender="m" type="e" stanza="7"><seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg></rhyme></pgtc></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Il ne faut pas parler de corde dans la maison d’un pendu.</note>
					</closer>
			</div></body></text></TEI>