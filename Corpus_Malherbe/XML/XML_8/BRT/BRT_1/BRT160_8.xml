<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉNIGMES</head><head type="sub_1">BOTANIQUE<lb></lb>(<hi rend="smallcap">EMBLÈMES</hi>.)</head><div type="poem" key="BRT160" modus="sm" lm_max="8" metProfile="8" form="suite de strophes" schema="2[abab] 1[aa] 1[abba]" er_moy="1.71" er_max="6" er_min="0" er_mode="0(3/7)" er_moy_et="1.98">
				<head type="number">X</head>
				<lg n="1" type="regexp" rhyme="ababaaabbaabab">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">M<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.2" punct="vg:4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" punct="vg">o</seg>rt</w>, <w n="1.3">t<seg phoneme="a" type="vs" value="1" rule="307" place="5">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="1.4" punct="vg:8">fl<seg phoneme="y" type="vs" value="1" rule="454" place="7">u</seg><pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">F<seg phoneme="œ" type="vs" value="1" rule="406" place="1">eu</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ct</w> <w n="2.6" punct="vg:8">lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg><pgtc id="2" weight="2" schema="CR">s<rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="3.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.4">n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="3.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="3.6">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>qu<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="4.2">gr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.3">d</w>’<w n="4.4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="4.5">n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="4.6">fru<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="4.7" punct="pt:8">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="2" weight="2" schema="CR">s<rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="5.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="5.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="5.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sb<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg><pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="6.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>s<pgtc id="3" weight="2" schema="CR">t<rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="7" num="1.7" lm="8" met="8"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="7.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="7.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.5" punct="pv:8">s<pgtc id="4" weight="6" schema="VCR"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="a" id="4" gender="m" type="a" stanza="3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pv">on</seg></rhyme></pgtc></w> ;</l>
					<l n="8" num="1.8" lm="8" met="8"><w n="8.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="8.2">b<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="8.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="8.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="8.7" punct="vg:8">pr<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="f" type="a" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="9" num="1.9" lm="8" met="8"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">pr<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="9.5">d</w>’<w n="9.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>c<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="f" type="e" stanza="3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="1.10" lm="8" met="8"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2" punct="vg:3">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315" place="3" punct="vg">eau</seg></w>, <w n="10.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="10.5" punct="pt:8">m<pgtc id="4" weight="6" schema="VCR"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<rhyme label="a" id="4" gender="m" type="e" stanza="3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></rhyme></pgtc></w>.</l>
					<l n="11" num="1.11" lm="8" met="8"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.4">g<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="11.5" punct="pv:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>pr<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="a" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="12" num="1.12" lm="8" met="8"><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="12.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="12.5">m</w>’<w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="12.7" punct="pv:8">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg><pgtc id="7" weight="2" schema="CR">t<rhyme label="b" id="7" gender="m" type="a" stanza="4"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pv">é</seg></rhyme></pgtc></w> ;</l>
					<l n="13" num="1.13" lm="8" met="8"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <hi rend="ital"><w n="13.3">st<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg><seg phoneme="i" type="vs" value="1" rule="477" place="4">ï</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>sm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w></hi> <w n="13.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w>-<w n="13.5">m<pgtc id="6" weight="0" schema="R"><rhyme label="a" id="6" gender="f" type="e" stanza="4"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="1.14" lm="8" met="8"><w n="14.1">J</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>xpr<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="14.5" punct="pt:8">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rm<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg><pgtc id="7" weight="2" schema="CR">t<rhyme label="b" id="7" gender="m" type="e" stanza="4"><seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></rhyme></pgtc></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Buis.</note>
					</closer>
			</div></body></text></TEI>