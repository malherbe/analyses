<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS EN TRIANGLES</head><div type="poem" key="BRT254" modus="cp" lm_max="12" metProfile="6+6, (6)" form="suite de strophes" schema="1[abba] 1[aa]" er_moy="2.0" er_max="4" er_min="0" er_mode="0(1/3)" er_moy_et="1.63">
				<head type="number">IV</head>
					<div n="1" type="section">
					<head type="number">1.</head>
				<lg n="1" type="regexp" rhyme="a">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.6">s</w>’<w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden"><w n="1.8" punct="pt:12">J<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>n<pgtc id="1" weight="2" schema="CR"><rhyme label="a" id="1" gender="m" type="a" stanza="1"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pt">u</seg>s</rhyme></pgtc></w></add></subst>.</l>
				</lg>
					</div>
					<div n="2" type="section">
					<head type="number">2.</head>
				<lg n="1" type="regexp" rhyme="b">
					<l n="2" num="1.1" lm="12" met="6+6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="2.2">l</w>’<subst hand="ML" reason="analysis" type="completion"><del>....</del><add rend="hidden"><w n="2.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>s</w></add></subst>, <w n="2.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="2.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="2.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="2.7">l<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="2.8">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="2.9" punct="pt:12">bl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="a" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
					</div>
					<div n="3" type="section">
					<head type="number">3.</head>
				<lg n="1" type="regexp" rhyme="b">
					<l n="3" num="1.1" lm="12" met="6+6"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="3.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="3.3" punct="vg:4">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg" mp="F">e</seg>s</w>, <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <subst hand="ML" reason="analysis" type="completion"><del>...</del><add rend="hidden"><w n="3.5">N<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>l</w></add><caesura></caesura></subst> <w n="3.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="9">en</seg></w> <w n="3.8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="3.9" punct="pt:12">br<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="f" type="e" stanza="1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
				</lg>
					</div>
					<div n="4" type="section">
					<head type="number">4.</head>
				<lg n="1" type="regexp" rhyme="a">
					<l n="4" num="1.1" lm="12" met="6+6"><w n="4.1">S</w>’<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="4.3" punct="vg:2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="4.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="4.5" punct="pv:6">b<seg phoneme="a" type="vs" value="1" rule="307" place="4" mp="M">â</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pv" caesura="1">a</seg></w> ;<caesura></caesura> <w n="4.6">c</w>’<w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="4.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="4.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">a<pgtc id="1" weight="2" schema="(c)[R" part="1">n</pgtc></seg>t</w> <w n="4.10"><pgtc id="1" weight="2" schema="(c)[R" part="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></pgtc></w> <subst hand="ML" reason="analysis" type="completion"><del>..</del><add rend="hidden"><w n="4.11" punct="pe:12"><pgtc id="1" weight="2" schema="(c)[R" part="3"><rhyme label="a" id="1" gender="m" type="e" stanza="1"><seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pe">u</seg>s</rhyme></pgtc></w></add></subst> !</l>
				</lg>
					</div>
					<div n="5" type="section">
					<head type="number">5.</head>
				<lg n="1" type="regexp" rhyme="aa">
					<l n="5" num="1.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.4">qu</w>’<w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <subst hand="ML" reason="analysis" type="completion"><del>.</del><add rend="hidden"><w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="6" caesura="1">è</seg>s</w></add><caesura></caesura></subst> <w n="5.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="5.9">m<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>t</w> « <w n="5.10">c<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>r<pgtc id="3" weight="4" schema="VR" part="1"><seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w> »</l>
					<l n="6" num="1.2" lm="6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> « <w n="6.4" punct="pt:6">st<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>d<pgtc id="3" weight="4" schema="VR" part="1"><seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ø" type="vs" value="1" rule="403" place="6">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></rhyme></pgtc></w> ».</l>
				</lg>
					</div>
						<closer>
						<note type="footnote" id="none">▪ Janus<lb></lb>▪ anis<lb></lb>▪ Nil<lb></lb>▪ us<lb></lb>▪ s</note>
					</closer>
			</div></body></text></TEI>