<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT54" modus="cm" lm_max="12" metProfile="6+6" form="suite de strophes" schema="3[abab]" er_moy="1.83" er_max="6" er_min="0" er_mode="0(3/6)" er_moy_et="2.19">
				<head type="number">LIV</head>
				<lg n="1" type="regexp" rhyme="abababababab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="1.4" punct="dp:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="dp" mp="F">e</seg></w> : <w n="1.5">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="1.6">n</w>’<w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg></w><caesura></caesura> <w n="1.8">qu</w>’<w n="1.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="1.10">h<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="1.11">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="1.12" punct="pv:12"><pgtc id="1" weight="3" schema="[CGR">pi<rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="2.5" punct="vg:6">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg" caesura="1">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="2.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="2.9">m<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</w> <w n="2.10" punct="pe:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rt<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>t</rhyme></pgtc></w> !</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="3.5">m<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>rs</w><caesura></caesura> <w n="3.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="3.7">br<seg phoneme="y" type="vs" value="1" rule="445" place="8">û</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="3.9" punct="pv:12">p<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg><pgtc id="1" weight="3" schema="CGR">pi<rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="4.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="4.3" punct="vg:4">cr<seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="4.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="4.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="6" caesura="1">en</seg>s</w><caesura></caesura> <w n="4.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="4.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="4.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="4.9" punct="pe:12">b<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>t</rhyme></pgtc></w> !</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="5.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="5.7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8" mp="M">oi</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="5.9" punct="vg:12">r<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="6.3" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">am</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="6.5">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="6.7" punct="pt:12">j<pgtc id="4" weight="6" schema="VCR"><seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>rn<rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="o" type="vs" value="1" rule="318" place="12" punct="pt">au</seg>x</rhyme></pgtc></w>.</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="7.2">m</w>’<w n="7.3" punct="dp:3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>c<seg phoneme="œ" type="vs" value="1" rule="249" place="3" punct="dp">œu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> : <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="7.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>ct</w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="7.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="7.10">f<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="8.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="Lc">ou</seg>s</w>-<w n="8.6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>ls</w><caesura></caesura> <w n="8.7"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="8.8">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="8.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="8.10" punct="pt:12">f<pgtc id="4" weight="6" schema="VCR"><seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>rn<rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg>x</rhyme></pgtc></w>.</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1">J</w>’<w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="9.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="9.8">n</w>’<w n="9.9"><seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">N<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="10.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="10.3">n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="10.4" punct="vg:6">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>g<seg phoneme="o" type="vs" value="1" rule="438" place="6" punct="vg" caesura="1">o</seg>ts</w>,<caesura></caesura> <w n="10.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="10.6">n</w>’<w n="10.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="10.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="10.9">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="10">oin</seg>s</w> <w n="10.10" punct="pv:12">h<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="M">eu</seg><pgtc id="6" weight="2" schema="CR">r<rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pv">eu</seg>x</rhyme></pgtc></w> ;</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="11.3">m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="11.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>r</w><caesura></caesura> <w n="11.7">s</w>’<w n="11.8"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d</w> <w n="11.9"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="11.10">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="11.11">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>l<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" mp="M">in</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="12.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg></w> <w n="12.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="12.7">v<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>ls</w> <w n="12.8" punct="pt:12"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">om</seg><pgtc id="6" weight="2" schema="CR">br<rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Pierre qui roule n’amasse pas mousse.</note>
					</closer>
			</div></body></text></TEI>