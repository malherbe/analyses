<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS EN TRIANGLES</head><div type="poem" key="BRT272" modus="cm" lm_max="12" metProfile="6+6" form="strophe unique" schema="1(ababcdcd)" er_moy="0.5" er_max="2" er_min="0" er_mode="0(3/4)" er_moy_et="0.87">
				<head type="number">XXII</head>
				<lg n="1" type="huitain" rhyme="ababcdcd">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="1.3" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="421" place="3">oi</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="1.4">dr<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="1.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.7">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <subst hand="ML" type="completion" reason="analysis"><del>....</del><add rend="hidden"><w n="1.9" punct="vg:12">b<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w></add></subst>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="2.2">m<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="2.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="2.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8" mp="M">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg>s</w> <w n="2.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="2.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11">an</seg>d</w> <w n="2.8" punct="pv:12">b<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pv">oi</seg>s</rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="3.4" punct="vg:6">V<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ln<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg></w>,<caesura></caesura> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="3.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="491" place="9" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w> <w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11">un</seg></w> <subst hand="ML" type="completion" reason="analysis"><del>...</del><add rend="hidden"><w n="3.8" punct="vg:12"><pgtc id="1" weight="0" schema="[R"><rhyme label="a" id="1" gender="f" type="e"><seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w></add></subst>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="4.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="4.3">m<seg phoneme="ø" type="vs" value="1" rule="399" place="3">eu</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="4.6">m<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="4.7" punct="pt:12">v<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e"><seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pt">oi</seg>x</rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="5.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="5.7">s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>n<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r</w> <w n="6.4">n<seg phoneme="a" type="vs" value="1" rule="343" place="5" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="477" place="6" caesura="1">ï</seg>f</w><caesura></caesura> <w n="6.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="6.6">d<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="6.7" punct="dp:10">n<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" punct="dp" mp="F">e</seg>s</w> : <hi rend="ital"><w n="6.8" punct="vg:11">d<seg phoneme="o" type="vs" value="1" rule="444" place="11" punct="vg">o</seg></w></hi>, <subst hand="ML" type="completion" reason="analysis"><del>..</del><add rend="hidden"><w n="6.9"><pgtc id="4" weight="2" schema="[CR">r<rhyme label="d" id="4" gender="m" type="a"><seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></rhyme></pgtc></w></add></subst></l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">d</w>’<w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <subst hand="ML" type="completion" reason="analysis"><del>.</del><add rend="hidden"><w n="7.4"><seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w></add></subst> <w n="7.5" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>j<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="7.7"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="7.8">qu</w>’<w n="7.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="7.10"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>gn<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="8.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.3">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="8.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>t</w> <w n="8.7" punct="pt:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg><pgtc id="4" weight="2" schema="CR">r<rhyme label="d" id="4" gender="m" type="e"><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></rhyme></pgtc></w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Bure<lb></lb>▪ ure<lb></lb>▪ ré<lb></lb>▪ e</note>
					</closer>
			</div></body></text></TEI>