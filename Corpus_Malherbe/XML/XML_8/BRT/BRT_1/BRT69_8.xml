<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT69" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite de strophes" schema="3[abab]" er_moy="0.0" er_max="0" er_min="0" er_mode="0(6/6)" er_moy_et="0.0">
				<head type="number">LXIX</head>
				<lg n="1" type="regexp" rhyme="abababababab">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="1.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="1.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="1.5">d</w>’<w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>tt<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="1.9" punct="pv:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>sc<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="2.2">n</w>’<w n="2.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="2.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="2.5" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</w>, <w n="2.6" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="2.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="2.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="P">ou</seg>r</w> <w n="2.9">s<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg></w> <w n="2.10" punct="pv:12">p<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="a" stanza="1"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pv">eu</seg></rhyme></pgtc></w> ;</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="3.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="3.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>t</w> <w n="3.5">d</w>’<w n="3.6" punct="pe:6">h<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="pe" caesura="1">eu</seg>r</w> !<caesura></caesura> <w n="3.7" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pe">on</seg>s</w> ! <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="3.9">l</w>’<w n="3.10" punct="pe:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe ps" mp="F">e</seg></rhyme></pgtc></w> ! …</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="4.4">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="4.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="4.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.7" punct="vg:10">v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="4.8" punct="pe:12">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rbl<pgtc id="2" weight="0" schema="R"><rhyme label="b" id="2" gender="m" type="e" stanza="1"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg></rhyme></pgtc></w> ! »</l>
					<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">D</w>’<w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="5.4" punct="vg:3">l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" punct="vg">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="5.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="5.6">gr<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.8" punct="pv:8">r<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></rhyme></pgtc></w> ;</l>
					<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="6.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="6.5" punct="pv:8">s<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="a" stanza="2"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pv">eu</seg>r</rhyme></pgtc></w> ;</l>
					<l n="7" num="1.7" lm="8" met="8"><space unit="char" quantity="8"></space><w n="7.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="7.2">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="7.4" punct="vg:6">h<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg></w>, <w n="7.5">s</w>’<w n="7.6"><seg phoneme="i" type="vs" value="1" rule="497" place="7">y</seg></w> <w n="7.7" punct="pt:8">c<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></rhyme></pgtc></w>.</l>
					<l n="8" num="1.8" lm="8" met="8"><space unit="char" quantity="8"></space><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="8.3" punct="ps:4">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="ps">ai</seg>t</w>… <w n="8.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6" punct="pe:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lh<pgtc id="4" weight="0" schema="R"><rhyme label="b" id="4" gender="m" type="e" stanza="2"><seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pe ps">eu</seg>r</rhyme></pgtc></w> ! …</l>
					<l n="9" num="1.9" lm="8" met="8"><space unit="char" quantity="8"></space><w n="9.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="9.2">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="2">ein</seg>t</w> <w n="9.3" punct="vg:4">bl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>f<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>rd</w>, <w n="9.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="9.5">d<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="9.7" punct="vg:8">v<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="3"><seg phoneme="u" type="vs" value="1" rule="425" place="8">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="1.10" lm="8" met="8"><space unit="char" quantity="8"></space><w n="10.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="10.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="10.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t</w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.7">j<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="a" stanza="3"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</rhyme></pgtc></w></l>
					<l n="11" num="1.11" lm="8" met="8"><space unit="char" quantity="8"></space><w n="11.1">Lu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="11.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="11.3">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="11.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">qu</w>’<w n="11.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="11.8">c<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" stanza="3"><seg phoneme="u" type="vs" value="1" rule="425" place="8">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></rhyme></pgtc></w></l>
					<l n="12" num="1.12" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="12.2">s</w>’<w n="12.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="12.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="12.6" punct="pt:8">t<pgtc id="6" weight="0" schema="R"><rhyme label="b" id="6" gender="m" type="e" stanza="3"><seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</rhyme></pgtc></w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Il n’y a que le premier pas qui coûte.</note>
					</closer>
			</div></body></text></TEI>