<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC31">
				<head type="main">La Fiancée</head>
				<lg n="1">
					<head type="speaker">PREMIÈRE JEUNE FILLE (<hi rend="ital">à la fiancée</hi>)</head>
					<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="1.2">qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="1.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>-<w n="1.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="1.5">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="2.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> ?</l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.6">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">C</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> !</l>
				</lg>
				<lg n="2">
					<head type="speaker">DEUXIÈME JEUNE FILLE</head>
					<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>, <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="5.5">f<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="7.4">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">B<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.6">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
				</lg>
				<lg n="3">
					<head type="speaker">LA FIANCÉE (<hi rend="ital">à part</hi>)</head>
					<l n="9" num="3.1"><w n="9.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.6">li<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
				</lg>
				<lg n="4">
					<head type="speaker">PREMIÈRE JEUNE FILLE (<hi rend="ital">à la fiancée</hi>)</head>
					<l n="11" num="4.1"><space unit="char" quantity="8"></space><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="11.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="11.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.4">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="4.2"><space unit="char" quantity="8"></space><w n="12.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="12.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="12.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ;</l>
					<l n="13" num="4.3"><space unit="char" quantity="8"></space><w n="13.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="13.2">M<seg phoneme="a" type="vs" value="1" rule="343">a</seg>î<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.3">d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="14" num="4.4"><space unit="char" quantity="8"></space><w n="14.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="14.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="14.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> !</l>
				</lg>
				<lg n="5">
					<head type="speaker">DEUXIÈME JEUNE FILLE</head>
					<l n="15" num="5.1"><space unit="char" quantity="8"></space><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="16" num="5.2"><space unit="char" quantity="8"></space><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="16.3">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="16.7">bru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w>,</l>
					<l n="17" num="5.3"><space unit="char" quantity="8"></space><w n="17.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="17.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">z<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="18" num="5.4"><space unit="char" quantity="8"></space><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="18.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gui<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.7">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> !</l>
				</lg>
				<lg n="6">
					<head type="speaker">LA FIANCÉE (<hi rend="ital">à part</hi>)</head>
					<l n="19" num="6.1"><w n="19.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="19.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="19.4">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="19.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="19.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.7">l</w>’<w n="19.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="19.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="20" num="6.2"><space unit="char" quantity="8"></space><w n="20.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="20.2">pr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> !</l>
				</lg>
				<lg n="7">
					<head type="speaker">PREMIÈRE JEUNE FILLE (<hi rend="ital">à la fiancée</hi>).</head>
					<l n="21" num="7.1"><space unit="char" quantity="8"></space><w n="21.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="21.5">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="22" num="7.2"><space unit="char" quantity="8"></space><w n="22.1">J<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="22.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.5">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ;</l>
					<l n="23" num="7.3"><space unit="char" quantity="8"></space><w n="23.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="23.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="23.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="23.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="23.7">d</w>’<w n="23.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="24" num="7.4"><space unit="char" quantity="8"></space><w n="24.1">D<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="24.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="24.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.4">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="24.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="24.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="24.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> !</l>
				</lg>
				<lg n="8">
					<head type="speaker">DEUXIÈME JEUNE FILLE</head>
					<l n="25" num="8.1"><space unit="char" quantity="8"></space><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="25.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">l</w>’<w n="25.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="25.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="26" num="8.2"><space unit="char" quantity="8"></space><w n="26.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">l</w>’<w n="26.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="26.4">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="26.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>…</l>
					<l n="27" num="8.3"><space unit="char" quantity="8"></space><w n="27.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="27.2">s<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.3">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rph<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="28" num="8.4"><space unit="char" quantity="8"></space><w n="28.1">B<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.3">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, — <w n="28.4">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> !</l>
				</lg>
				<lg n="9">
					<head type="speaker">LA FIANCÉE (<hi rend="ital">à part</hi>)</head>
					<l n="29" num="9.1"><w n="29.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="29.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="29.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.4">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="29.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="29.6">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="29.7">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="29.8">j</w>’<w n="29.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="30" num="9.2"><space unit="char" quantity="8"></space><w n="30.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.2">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="30.3">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="30.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.5">l</w>’<w n="30.6">h<seg phoneme="i" type="vs" value="1" rule="497">y</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="221">en</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
					<date when="1856">1856</date>
					</dateline>
				</closer>
			</div></body></text></TEI>