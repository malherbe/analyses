<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Rome et les Barbares</head><head type="main_subpart">Hortorum Deus</head><div type="poem" key="HER46">
						<head type="number">IV</head>
						<opener>
							<epigraph>
								<cit>
									<quote>Mihi corolla picta vere ponitur.</quote>
									<bibl>
										<name>Catulle</name>.
									</bibl>
								</cit>
							</epigraph>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w>. <w n="1.3">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.4">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>li<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="1.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="1.7">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
							<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.5">n<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="2.9">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.10">gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="3.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>. <w n="3.6">L</w>’<w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="3.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.10">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="4.2">j<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.6">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="4.7">d</w>’<w n="4.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.9">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.10">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.6">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="?" type="va" value="1" rule="162">en</seg>t</w> : <w n="5.8">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ds</w> <w n="5.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
							<l n="6" num="2.2"><w n="6.1">R<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="6.2">m<seg phoneme="y" type="vs" value="1" rule="445">û</seg>rs</w>, <w n="6.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="6.6">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>ni<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="7.6">c<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="7.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.9">l</w>’<w n="7.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="8" num="2.4"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.7">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.10">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">cl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="9.6">m</w>’<w n="9.7">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>. <w n="9.8">J</w>’<w n="9.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.10">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="9.11">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="10" num="3.2"><w n="10.1">J<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.2">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="11.3">n</w>’<w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.5">mi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.6">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="11.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.10">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p</w> <w n="11.11">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="12.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="12.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>, <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.6">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.10">l</w>’<w n="12.11">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="13" num="4.2"><w n="13.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="13.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.6">t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.9">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w></l>
							<l n="14" num="4.3"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ni<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="14.3">d</w>’<w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="14.5">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="14.6">qu</w>’<w n="14.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="14.8">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.10">R<seg phoneme="ɔ" type="vs" value="1" rule="441">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						</lg>
					</div></body></text></TEI>