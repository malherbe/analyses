<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La Nature et le Rêve</head><div type="poem" key="HER116">
					<head type="main">Michel-Ange</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.4">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.5">d</w>’<w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.7">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="2.2">qu</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.5">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg>xt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.7">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.9">R<seg phoneme="ɔ" type="vs" value="1" rule="441">o</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.11">f<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="3" num="1.3"><w n="3.1">S<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.3">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.4">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="i" type="vs" value="1" rule="493">y</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.6">Pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ph<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="4.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.5">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>, <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.8">J<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.4">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.5">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bst<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>,</l>
						<l n="6" num="2.2"><w n="6.1">T<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="6.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="6.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="6.8">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ts</w> <w n="6.9">f<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="7" num="2.3"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.2">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.7">Gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.9">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="7.10">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301">ai</seg>t</w> <w n="8.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.5">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rt</w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.9">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.10">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rds</w> <w n="9.4">G<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.8">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xs<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>scl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.3">qu</w>’<w n="10.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w> <w n="10.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.7">g<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.5">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.6">d</w>’<w n="11.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.9">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.5">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ds</w> <w n="12.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="12.7">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="12.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.9"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lti<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="13" num="4.2"><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="13.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="13.9">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="14" num="4.3"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.2">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">d</w>’<w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.5">Di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="14.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.9">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>