<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Le Moyen âge et la Renaissance</head><div type="poem" key="HER64">
					<head type="main">Le Huchier de Nazareth</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-27">e</seg></w> <w n="1.4">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>chi<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="1.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.6">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="1.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.8">dr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
						<l n="2" num="1.2"><w n="2.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>, <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">r<seg phoneme="a" type="vs" value="1" rule="340">â</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.8">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="5.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>, <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="5.6">t</w>-<w n="5.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="5.8">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="5.9">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="5.10">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.11">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
						<l n="6" num="2.2"><w n="6.1">S</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.3">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="6.5">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> <w n="6.6">l</w>’<w n="6.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.9">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="6.10">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">Vi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.7">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.3">J<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="8.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.7">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.8">s</w>’<w n="8.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257">eoi</seg>r</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.4">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.8">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.9">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.10">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="10.3">J<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ph</w>, <w n="10.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.8">ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="10.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.10">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="11.2">s</w>’<w n="11.3"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="11.7">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="11.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.9">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="12.5">qu</w>’<w n="12.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.7">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="13" num="4.2"><w n="13.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="13.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="13.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.8">l</w>’<w n="13.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>li<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
						<l n="14" num="4.3"><w n="14.1">V<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="14.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="14.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="14.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.9">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.10">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>