<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES ANTIQUES</head><div type="poem" key="CHE20">
					<head type="number">IV</head>
					<head type="main">Chrysé</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w>, <w n="1.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">Chr<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="1.4">t</w>’<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="1.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1">T</w>’<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="2.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w> <w n="2.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.8">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="2.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
						<l n="3" num="1.3"><w n="3.1">Di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ! <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">t</w>’<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="3.5">v<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="3.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.10">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.11">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.3">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ils</w> <w n="4.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.8">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="4.9">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="5" num="1.5"><w n="5.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="5.3">fl<seg phoneme="o" type="vs" value="1" rule="435">o</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.5">l</w>’<w n="5.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.9">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="5.11">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="1.6"><w n="6.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.6"><seg phoneme="i" type="vs" value="1" rule="dc-1">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="dc-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="7" num="1.7"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">N<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="7.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="7.6">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.7">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.9">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
						<l n="8" num="1.8"><w n="8.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="8.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="8.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="8.5">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.7">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.9">d</w>’<w n="8.10"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
						<l n="9" num="1.9"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="9.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.3">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>li<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.4">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="9.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.6">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="9.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="10" num="1.10"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.2">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="10.3">H<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="10.7">gr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.8">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="11" num="1.11"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">j</w>’<w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="11.5">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="11.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.9">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.10">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="11.11"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.12">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="12" num="1.12"><w n="12.1">T<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ph<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s</w> <w n="12.2">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
						<l n="13" num="1.13"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">j</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">v<seg phoneme="ø" type="vs" value="1" rule="248">œu</seg>x</w> <w n="13.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="13.7">di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.9">l</w>’<w n="13.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.11"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="14" num="1.14"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">v<seg phoneme="ø" type="vs" value="1" rule="248">œu</seg>x</w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.5">N<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>pt<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.7">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="14.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.10">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="15" num="1.15"><w n="15.1">Gl<seg phoneme="o" type="vs" value="1" rule="318">au</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="15.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="15.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> ; <w n="15.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="15.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="15.10">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>,</l>
						<l n="16" num="1.16"><w n="16.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="16.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="16.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="16.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="16.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="16.9">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>.</l>
						<l n="17" num="1.17"><w n="17.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="17.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.3">n</w>’<w n="17.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="17.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="18" num="1.18"><w n="18.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">n<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="18.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="18.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.8">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="19" num="1.19"><w n="19.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>, <w n="19.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="19.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>, <w n="19.5">j</w>’<w n="19.6"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="19.7">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.8">f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.10">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w></l>
						<l n="20" num="1.20"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="20.2">d<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ph<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="20.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.4">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="20.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.8">L<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sb<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>