<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES ANTIQUES ‒ ÉTUDES</head><div type="poem" key="CHE10">
					<head type="number">X</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cch<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="1.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.4">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ts</w> <w n="1.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.7">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.8">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="2.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">M<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>, <w n="2.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.7">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
						<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">Ph<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.7">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w> ;</l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="4.3">n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.5">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="5.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.6">d</w>’<w n="5.7"><seg phoneme="e" type="vs" value="1" rule="250">Œ</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="6" num="2.2"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">s</w>’<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="6.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="6.7">d</w>’<w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>r<seg phoneme="i" type="vs" value="1" rule="dc-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">gr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="7.5">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.9">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><space quantity="8" unit="char"></space><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="8.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>d<seg phoneme="i" type="vs" value="1" rule="497">y</seg>m<seg phoneme="i" type="vs" value="1" rule="dc-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="9.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="9.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.4">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rts</w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> ;</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="355">e</seg>x<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="11.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.5">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="11.6">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="12" num="3.4"><w n="12.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="12.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="12.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					</lg>
					<lg n="4">
						<l ana="incomplete" where="F" n="13" num="4.1"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="13.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="13.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.4">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ......................................................</l>
						<l n="14" num="4.2"><w n="14.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.4">l</w>’<w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.7">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.9">l</w>’<w n="14.10"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>.</l>
						<l rhyme="none" n="15" num="4.3"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="15.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="15.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="15.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.8">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="4.4"><space quantity="8" unit="char"></space><w n="16.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>gl<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="16.4">qu</w>’<w n="16.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>