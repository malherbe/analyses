<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU141">
				<head type="main">SOUVENIR</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Deux estions et n’avions qu’ung cœur.
							</quote>
							<bibl>
								<hi rend="ital">Le lay de maistre Ytier Marchant.</hi>
							</bibl>
						</cit>
						<cit>
							<quote>
								Hélas ! il n’étoit pas saison <lb></lb>
								Sitôt de son département.
							</quote>
							<bibl>
								<hi rend="ital">La complainte de Valentin Granson.</hi>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="1.5">t</w>-<w n="1.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="1.8">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> ? <w n="1.9">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.10">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.11">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="2.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="2.3">d</w>’<w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="2.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="2.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.8">c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="3" num="1.3"><w n="3.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">l</w>’<w n="3.5">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg>s</w> <w n="3.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.9">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>,</l>
					<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307">a</seg>il</w> <w n="4.4">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.6">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ; <w n="4.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="4.8">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="4.9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>,</l>
					<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">fr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">l</w>’<w n="5.5">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.8">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.9">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="6" num="1.6"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.8">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="7" num="1.7"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.9">l</w>’<w n="7.10"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="8" num="1.8"><w n="8.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.2">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.7">ru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="9" num="1.9"><w n="9.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">cl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="10" num="1.10"><w n="10.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.6">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="10.7">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.9">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.10">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>