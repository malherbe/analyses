<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU98">
				<head type="main">CHINOISERIE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="1.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="1.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="1.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="1.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.9">j</w>’<w n="1.10"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="2.5">J<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="i" type="vs" value="1" rule="dc-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ; <w n="2.6">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɔ" type="vs" value="1" rule="439">O</seg>ph<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="dc-1">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="3.2">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.3">B<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w>, <w n="3.4">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ɔ" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="4.7">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">j</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.6">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.9">Ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="6.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>,</l>
					<l n="7" num="2.3"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="8.2">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="8.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="9.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="10.2">pi<seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="10.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.5">t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="10.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w> <w n="11.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.4">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="11.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.7">cu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.9">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="12" num="3.4"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.5">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="13.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.3">tr<seg phoneme="e" type="vs" value="1" rule="383">e</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.7">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="4.2"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">l</w>’<w n="14.3">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="14.5">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="14.6">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="14.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ;</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="15.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="15.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="15.6">qu</w>’<w n="15.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.8">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="16" num="4.4"><w n="16.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.6">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.8">p<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
				</lg>
			</div></body></text></TEI>