<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1838-1845</title>
				<title type="sub_2">Tome second</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>625 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1838-1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU237">
			<head type="main">SULTAN MAHMOUD</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.3">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="361">e</seg>m</w> <w n="1.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">gr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="2" num="1.2"><space quantity="4" unit="char"></space><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w></l>
				<l n="3" num="1.3"><w n="3.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.2">d</w>’<w n="3.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="4" num="1.4"><space quantity="4" unit="char"></space><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>,</l>
				<l n="5" num="1.5"><w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="5.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="5.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="6" num="1.6"><space quantity="4" unit="char"></space><w n="6.1">D</w>’<w n="6.2"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>pi<seg phoneme="ɔ" type="vs" value="1" rule="451">u</seg>m</w> <w n="6.3"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
				<l n="7" num="1.7"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="8" num="1.8"><space quantity="4" unit="char"></space><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="8.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="8.3">bl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
			</lg>
			<lg n="2">
				<l n="9" num="2.1"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="9.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
				<l n="10" num="2.2"><space quantity="4" unit="char"></space><w n="10.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>.</l>
				<l n="11" num="2.3"><w n="11.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="11.2">j</w>’<w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="11.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="11.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="11.6">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
				<l n="12" num="2.4"><space quantity="4" unit="char"></space><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
			</lg>
			<lg n="3">
				<l n="13" num="3.1"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.2">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.4">l</w>’<w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="14" num="3.2"><space quantity="4" unit="char"></space><w n="14.1">J</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="14.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="14.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
				<l n="15" num="3.3"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="15.3"><seg phoneme="ø" type="vs" value="1" rule="405">Eu</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="16" num="3.4"><space quantity="4" unit="char"></space><w n="16.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="16.2">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ;</l>
				<l n="17" num="3.5"><w n="17.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w> <w n="17.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>, <w n="17.3">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w> <w n="17.4">d</w>’<w n="17.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="18" num="3.6"><space quantity="4" unit="char"></space><w n="18.1"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="18.2">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="18.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="18.4">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
				<l n="19" num="3.7"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.4">l</w>’<w n="19.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="20" num="3.8"><space quantity="4" unit="char"></space><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="20.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ;</l>
			</lg>
			<lg n="4">
				<l n="21" num="4.1"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="21.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
				<l n="22" num="4.2"><space quantity="4" unit="char"></space><w n="22.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="22.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>…</l>
				<l n="23" num="4.3"><w n="23.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="23.2">j</w>’<w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="23.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="23.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="23.6">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
				<l n="24" num="4.4"><space quantity="4" unit="char"></space><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="24.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="24.3">d</w>’<w n="24.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
			</lg>
			<lg n="5">
				<l n="25" num="5.1"><w n="25.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="25.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.5">Gr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="26" num="5.2"><space quantity="4" unit="char"></space><w n="26.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ;</l>
				<l n="27" num="5.3"><w n="27.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="27.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.4">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="28" num="5.4"><space quantity="4" unit="char"></space><w n="28.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="28.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ;</l>
				<l n="29" num="5.5"><w n="29.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="29.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.4">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="30" num="5.6"><space quantity="4" unit="char"></space><w n="30.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="30.2">l</w>’<w n="30.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="30.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ;</l>
				<l n="31" num="5.7"><w n="31.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="31.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="32" num="5.8"><space quantity="4" unit="char"></space><w n="32.1">N</w>’<w n="32.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="32.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="32.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> !</l>
			</lg>
			<lg n="6">
				<l n="33" num="6.1"><w n="33.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="33.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="33.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="33.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="33.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
				<l n="34" num="6.2"><space quantity="4" unit="char"></space><w n="34.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="34.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="34.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>…</l>
				<l n="35" num="6.3"><w n="35.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="35.2">j</w>’<w n="35.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="35.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="35.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="35.6">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
				<l n="36" num="6.4"><space quantity="4" unit="char"></space><w n="36.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="36.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="36.3">d</w>’<w n="36.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
			</lg>
			<closer>
				<dateline>
					<date when="1845">1845</date>.
				</dateline>
			</closer>
		</div></body></text></TEI>