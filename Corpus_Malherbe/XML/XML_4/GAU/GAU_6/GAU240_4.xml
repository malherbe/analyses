<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1838-1845</title>
				<title type="sub_2">Tome second</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>625 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1838-1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU240">
			<head type="main">LES TACHES JAUNES</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1">S<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w>, <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">pl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="2" num="1.2"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="2.3">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.4">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
				<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="3.3">d</w>’<w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.5">gr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="3.6">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="4" num="1.4"><w n="4.1">Pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">gr<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ;</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1"><w n="5.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="5.2">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>fl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="6" num="2.2"><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="6.4">pi<seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.7">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>,</l>
				<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="7.4">n</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">qu</w>’<w n="7.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="8" num="2.4"><w n="8.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>.</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rtr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
				<l n="10" num="3.2"><w n="10.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w>, <w n="10.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">n</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
				<l n="11" num="3.3"><w n="11.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
				<l n="12" num="3.4"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="12.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="12.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
			</lg>
			<lg n="4">
				<l n="13" num="4.1"><w n="13.1">S</w>’<w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="13.4">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="14" num="4.2"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
				<l n="15" num="4.3"><w n="15.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="15.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="15.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.5">l</w>’<w n="15.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="16" num="4.4"><w n="16.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.2">sc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="16.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="16.6">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ds</w>.</l>
			</lg>
			<lg n="5">
				<l n="17" num="5.1"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="17.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="17.3">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="17.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="17.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
				<l n="18" num="5.2"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="18.2">s</w>’<w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="18.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.6">dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ps</w>,</l>
				<l n="19" num="5.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="19.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="19.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
				<l n="20" num="5.4"><w n="20.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="20.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="20.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="20.5">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
			</lg>
			<lg n="6">
				<l n="21" num="6.1"><w n="21.1">S<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="21.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
				<l n="22" num="6.2"><w n="22.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.3">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="22.4">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
				<l n="23" num="6.3"><w n="23.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="23.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>pi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.4">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
				<l n="24" num="6.4"><w n="24.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="24.5">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>.</l>
			</lg>
			<lg n="7">
				<l n="25" num="7.1">— <w n="25.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="25.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="25.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="26" num="7.2"><w n="26.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rcl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="26.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>il</w>,</l>
				<l n="27" num="7.3"><w n="27.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="27.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="27.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="27.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="27.5">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="28" num="7.4"><w n="28.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="28.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.3">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="28.4">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="28.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.6">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w>,</l>
			</lg>
			<lg n="8">
				<l n="29" num="8.1"><w n="29.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="29.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="29.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="29.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="29.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="29.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.7">f<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="30" num="8.2"><w n="30.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="30.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="30.4">d</w>’<w n="30.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="30.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
				<l n="31" num="8.3"><w n="31.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="31.2">ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="31.3">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="31.5">m<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="32" num="8.4"><w n="32.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="32.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.5">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="32.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> ?</l>
			</lg>
			<lg n="9">
				<l n="33" num="9.1"><w n="33.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="33.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="33.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="33.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="33.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="34" num="9.2"><w n="34.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="34.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="34.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.6">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.7">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
				<l n="35" num="9.3"><w n="35.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="35.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="35.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="35.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="36" num="9.4"><w n="36.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="36.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="36.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="36.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.6">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> ?</l>
			</lg>
			<closer>
				<dateline>
					<date when="1844">1844</date>.
				</dateline>
			</closer>
		</div></body></text></TEI>