<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU250">
					<head type="main">EN ALLANT A LA CHARTREUSE DE MIRAFLORES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ou<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="1.2">c</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="1.7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.9">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="2.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="2.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="2.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="2.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rtr<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">pi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.6">cr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="3.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.9">pi<seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="4.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg>y<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
						<l n="5" num="1.5"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.3">br<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="5.4">d</w>’<w n="5.5">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>, <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.9">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.10">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="6" num="1.6"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="6.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="6.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="6.7">b<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="6.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.9">pi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.10">s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="7" num="1.7"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2">gr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w> <w n="7.4">d</w>’<w n="7.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vi<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="7.6">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
						<l n="8" num="1.8"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="8.2">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ls<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="8.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>-<w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w>-<w n="8.8">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
						<l n="9" num="1.9"><w n="9.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>, <w n="9.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.8">n</w>’<w n="9.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="1.10"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2">r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">gr<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.7">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">cr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="11" num="1.11"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="11.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.9">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.10">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>…</l>
						<l n="12" num="1.12"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="12.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="12.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.6">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>, <w n="12.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="12.8">d</w>’<w n="12.9"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="12.10"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
						<l n="13" num="1.13"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rç<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="13.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="13.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.8">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="13.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.11">pl<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="1.14"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="14.4">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="14.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.6">C<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d</w> <w n="14.7">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="14.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.9">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>ñ<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.10">Ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Cartuja de Miraflores</placeName>,
							<date not-before="1839" not-after="1849">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>