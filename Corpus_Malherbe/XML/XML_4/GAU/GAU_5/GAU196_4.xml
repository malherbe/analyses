<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1833-1838</title>
				<title type="sub_2">Tome premier</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3874 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1833-1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU196">
				<head type="main">LE SPECTRE DE LA ROSE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>pi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">cl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ffl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rg<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">d</w>’<w n="3.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.7">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.4">h<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="4.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="4.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>.</l>
					<l n="5" num="1.5"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="1.6"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="6.3">d</w>’<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">l</w>’<w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">f<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="1.8"><w n="8.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="8.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="9.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="9.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="9.7">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="9.8">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="2.2"><w n="10.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.4">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="11" num="2.3"><w n="11.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="11.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.5">sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="2.4"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="12.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="12.4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					<l n="13" num="2.5"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="13.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>, <w n="13.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.7">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="14" num="2.6"><w n="14.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.2">m<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <hi rend="ital"><w n="14.4">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.5">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="167">un</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></hi> ;</l>
					<l n="15" num="2.7"><w n="15.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w> <w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="16" num="2.8"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">j</w>’<w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="17.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="17.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.5">d</w>’<w n="17.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="18" num="3.2"><w n="18.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="18.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.4">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="18.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="19" num="3.3"><w n="19.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="19.2">d</w>’<w n="19.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="19.5">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="19.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.7">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="20" num="3.4"><w n="20.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="20.2">j</w>’<w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="20.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.5">g<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="20.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="21" num="3.5"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="21.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="21.3">l</w>’<w n="21.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lb<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="21.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="21.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="22" num="3.6"><w n="22.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="22.2">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414">ë</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="22.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="23" num="3.7"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : <w n="23.2">C<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>-<w n="23.3">g<seg phoneme="i" type="vs" value="1" rule="468">î</seg>t</w> <w n="23.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="24" num="3.8"><w n="24.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="24.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.4">r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="24.5">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="24.6">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1837">1837</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>