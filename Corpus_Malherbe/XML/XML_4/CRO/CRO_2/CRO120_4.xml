<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VISIONS</head><div type="poem" key="CRO120">
					<head type="main">Sonnet</head>
					<opener>
						<salute>à Ulysse Rocq, peintre</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="1.2">d</w>’<w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="1.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.7">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.8">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="1.9">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="2.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>, <w n="2.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="2.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="3" num="1.3"><w n="3.1">G<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>. <w n="3.2">V<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="3.3">d</w>’<w n="3.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="3.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="3.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="3.9">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.3">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="4.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="4.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>ps</w>, <w n="5.6">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="5.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.5">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>, <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.7">qu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="7" num="2.3"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>, <w n="7.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.7">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="7.8">br<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="8.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="8.5">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d</w>. <w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="8.7">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.9">tr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.7"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="9.8">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="10" num="3.2"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">m<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="10.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.8">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="11.2">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="11.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.4">l</w>’<w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>, <w n="11.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.7">d</w>’<w n="11.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.9">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="11.10">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="12.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.5">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="12.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="13" num="4.2"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="13.3">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="13.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w>, <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="13.6">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="14" num="4.3"><w n="14.1">B<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="14.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="14.7">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.8">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>