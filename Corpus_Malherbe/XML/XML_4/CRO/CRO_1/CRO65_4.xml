<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DRAMES ET FANTAISIES</head><div type="poem" key="CRO65">
					<head type="main">Trois quatrains</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">pl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.8">s</w>’<w n="1.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">R<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="2.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="2.7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">D</w>’<w n="3.2"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="3.3">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>. <w n="3.4">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.5">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.6">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ds</w>, <w n="3.7">pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ts</w> <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.10">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">D<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="4.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w> <w n="4.4">d</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ci<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.8">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-27">e</seg></w> <w n="4.9">h<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.6">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
						<l n="6" num="2.2"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">qu</w>’<w n="6.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="6.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.6">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>. <w n="6.7">Pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.9">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.11">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="7" num="2.3"><w n="7.1">S</w>’<w n="7.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="7.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>dr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>. <w n="7.5">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.9">m<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.4">n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="8.5">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-7">e</seg>r</w> <w n="8.6">n</w>’<w n="8.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="8.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.10">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">c<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="9.5">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w>.</l>
						<l n="11" num="3.3"><w n="11.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="11.4">qu</w>’<w n="11.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.6">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="11.8">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.9">d<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> :</l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="12.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="12.3">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.6">r<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="12.7">l</w>’<w n="12.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>