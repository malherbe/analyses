<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS PERPÉTUELLES</head><div type="poem" key="CRO5">
					<head type="main">Ronde flamande</head>
					<opener>
						<salute>A Mademoiselle Mauté de Fleurville.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.2">j</w>’<w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.4">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w>,</l>
						<l n="2" num="1.2"><space quantity="4" unit="char"></space><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">m<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">d</w>’<w n="3.3"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> ; <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.5">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="3.6">bl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w></l>
						<l n="4" num="1.4"><space quantity="12" unit="char"></space><w n="4.1">J</w>’<w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.4">tr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="5.2">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="5.3">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="5.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.5">d</w>’<w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w></l>
						<l n="6" num="2.2"><space quantity="4" unit="char"></space><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">pr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.6">d<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="8" num="2.4"><space quantity="12" unit="char"></space><w n="8.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">bi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">bi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="9.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w>.</l>
						<l n="10" num="3.2"><space quantity="4" unit="char"></space><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="11" num="3.3"><w n="11.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.2">qu</w>’<w n="11.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.4">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="11.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="11.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="12" num="3.4"><space quantity="12" unit="char"></space><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.3">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.2">qu</w>’<w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.4">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="13.5">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="13.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="13.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.8">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w></l>
						<l n="14" num="4.2"><space quantity="4" unit="char"></space><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="14.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="15" num="4.3"><w n="15.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.3">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="15.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="15.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="15.7">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="15.8">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="16" num="4.4"><space quantity="12" unit="char"></space><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="16.4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="17.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></l>
						<l n="18" num="5.2"><space quantity="4" unit="char"></space><w n="18.1">J<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="18.3">g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>, <w n="18.4">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rgu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="19" num="5.3"><w n="19.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.2">j</w>’<w n="19.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.4">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="19.5">c</w>’<w n="19.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="19.8">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w></l>
						<l n="20" num="5.4"><space quantity="12" unit="char"></space><w n="20.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">j</w>’<w n="20.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="20.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">J</w>’<w n="21.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.7">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
						<l n="22" num="6.2"><space quantity="4" unit="char"></space><w n="22.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="22.2">l</w>’<w n="22.3"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="22.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.7">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="23" num="6.3"><w n="23.1">M<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t</w> <w n="23.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.3">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="23.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
						<l n="24" num="6.4"><space quantity="12" unit="char"></space><w n="24.1">R<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="24.2">d</w>’<w n="24.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="25.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w>, <w n="25.3">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.5">c<seg phoneme="a" type="vs" value="1" rule="340">â</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="25.6">d</w>’<w n="25.7"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>…</l>
						<l n="26" num="7.2"><space quantity="4" unit="char"></space><w n="26.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.2">fl<seg phoneme="y" type="vs" value="1" rule="445">û</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="26.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="27" num="7.3"><w n="27.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="27.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="27.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="27.6">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w></l>
						<l n="28" num="7.4"><space quantity="12" unit="char"></space><w n="28.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="28.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.3">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">H<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="29.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="29.3">d</w>’<w n="29.4"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="29.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="29.6">d</w>’<w n="29.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>,</l>
						<l n="30" num="8.2"><space quantity="4" unit="char"></space><w n="30.1">D<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="30.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>, <w n="30.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.4">d</w>’<w n="30.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="31" num="8.3"><w n="31.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="31.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="31.5">d<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>…</l>
						<l n="32" num="8.4"><space quantity="12" unit="char"></space><w n="32.1">J</w>’<w n="32.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="32.3">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="32.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="32.5">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.2">n</w>’<w n="33.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="33.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="33.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="33.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.8">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
						<l n="34" num="9.2"><space quantity="4" unit="char"></space><w n="34.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rgu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="35" num="9.3"><w n="35.1">D</w>’<w n="35.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.4">r<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="35.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="36" num="9.4"><space quantity="12" unit="char"></space><w n="36.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.3">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="36.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>