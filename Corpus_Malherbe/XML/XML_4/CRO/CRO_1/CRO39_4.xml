<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PASSÉ</head><div type="poem" key="CRO39">
					<head type="main">Excuse</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="1.4">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="1.7">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>,</l>
						<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>,</l>
						<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="3.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.5">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">m<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="4.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.7">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w>,</l>
						<l n="5" num="1.5"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="5.5">ti<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w></l>
						<l n="6" num="1.6"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="6.2">r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w>, <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.4">qu</w>’<w n="6.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="6.6">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="7.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>, <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="7.7">j<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="8" num="2.2"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">tr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>ph<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ; <w n="8.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="8.4">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="8.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.7">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="9" num="2.3"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.5">fi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="10" num="2.4"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="10.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.3">d</w>’<w n="10.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="10.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.6">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w></l>
						<l n="11" num="2.5"><w n="11.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.3">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.4">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="11.6">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w></l>
						<l n="12" num="2.6"><w n="12.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494">ym</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>th<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tm<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>sph<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="13.3">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="14" num="3.2"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd</w> <w n="14.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
						<l n="15" num="3.3"><w n="15.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.2">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="15.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="15.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="15.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.7">gr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="16" num="3.4"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="16.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="17" num="3.5"><w n="17.1">L</w>’<w n="17.2"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="17.3">gr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="o" type="vs" value="1" rule="435">o</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.5">m<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="18" num="3.6"><w n="18.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.2">m<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="18.3">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.5">l</w>’<w n="18.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.3">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="19.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w></l>
						<l n="20" num="4.2"><w n="20.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.4">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>,</l>
						<l n="21" num="4.3"><w n="21.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd</w> <w n="21.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="21.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.6">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="22" num="4.4"><w n="22.1">Ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="22.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.3">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ls</w>, <w n="22.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="22.5">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.6">v<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-29">e</seg>nt</w> <w n="22.7">qu</w>’<w n="22.8">h<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>,</l>
						<l n="23" num="4.5"><w n="23.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="23.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="23.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.5">fi<seg phoneme="ɛ" type="vs" value="1" rule="CRO39_1">e</seg>r</w>,</l>
						<l n="24" num="4.6"><w n="24.1">R<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="24.2">l</w>’<w n="24.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="24.6">s</w>’<w n="24.7"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="25.2"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>, <w n="25.3">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="25.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
						<l n="26" num="5.2"><w n="26.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="26.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="26.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="26.5">r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
						<l n="27" num="5.3"><w n="27.1">F<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="27.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="27.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="27.4">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="28" num="5.4"><w n="28.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.3">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>h<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="28.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="28.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="28.6">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
						<l n="29" num="5.5"><w n="29.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="29.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="29.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="29.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w>,</l>
						<l n="30" num="5.6"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="30.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="30.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="30.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>