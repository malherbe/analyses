<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODES, ÉPIGRAMMES ET AUTRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>633 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1664" to="1704">1664-1704</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI55">
				<head type="main">AUTRE</head>
				<head type="sub">contre le même</head>
				<opener>
					<dateline>
						<date when="1685">(1685)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="1.2">qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="1.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.5">d</w>’<w n="1.6"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w>, <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="1.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.11">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="2" num="1.2"><w n="2.1">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="2.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.9"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
					<l n="3" num="1.3"><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.5">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.8"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="4" num="1.4"><w n="4.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.7">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w>.</l>
				</lg>
			</div></body></text></TEI>