<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES HORIZONTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Horizontales</title>
						<author>Henri Beauclair</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 — 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/horizont.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Horizontales</title>
								<author>Henri Beauclair</author>
								<edition>2e édition en partie originale</edition>
								<imprint>
									<publisher>Léon Vanier, Paris</publisher>
									<date when="1886">1886</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEA8">
				<head type="number">II</head>
				<head type="main">La Nasse</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Les Turcs ont passé là.</quote>
							<bibl>
								<name>V. HUGO</name>, <hi rend="ital">Orientales</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">K<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>. — <w n="1.5">C</w>’<w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="1.8">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>.</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="2.7">l</w>’<w n="2.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w></l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="3.4">P<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nni<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="4" num="1.4"><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="4.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.4">G<seg phoneme="i" type="vs" value="1" rule="493">y</seg>mn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.8">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="5.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="5.6">K<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g</w> <w n="5.7">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.10">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="5.11">d</w>’<w n="5.12"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gt</w> <w n="6.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>. — <w n="7.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="7.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.10">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>.</l>
					<l n="8" num="2.2"><w n="8.1">S<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="8.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="8.7">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.9">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="8.10">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>,</l>
					<l n="9" num="2.3"><space unit="char" quantity="8"></space><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">K<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g</w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.4">l</w>’<w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="10" num="2.4"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="10.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.7">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.9">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					<l n="11" num="2.5"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="11.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.8">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.9">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="11.10">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.11">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="12" num="2.6"><space unit="char" quantity="8"></space><w n="12.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="12.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="12.4">L<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <hi rend="ital"><w n="12.5">M<seg phoneme="ɔ̃" type="vs" value="1" rule="BEA8_2">un</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></hi>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1">«<w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="13.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="13.3">B<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="13.4">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.5">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="14" num="3.2"><w n="14.1">Fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="14.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="14.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="14.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>sf<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
					<l n="15" num="3.3"><space unit="char" quantity="8"></space><w n="15.1">C<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">l</w>’<w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="15.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="16" num="3.4"><w n="16.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="16.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ! <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.6">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w>, <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.8">l</w>’<w n="16.9"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="17" num="3.5"><w n="17.1">J<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="17.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.4">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sn<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="17.5"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="17.6">L<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> !</l>
					<l n="18" num="3.6"><space unit="char" quantity="8"></space><w n="18.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="18.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="18.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.5">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.6">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">N</w>’<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w>-<w n="19.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="19.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.6">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="19.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.8">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="19.9">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.10">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> ?</l>
					<l n="20" num="4.2"><w n="20.1">N</w>’<w n="20.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>-<w n="20.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.6">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w>-<w n="20.7">Fl<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="20.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="20.9">qu</w>’<w n="20.10"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.11">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mmi<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>,</l>
					<l n="21" num="4.3"><space unit="char" quantity="8"></space><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.3">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.5">n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="22" num="4.4"><w n="22.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>-<w n="22.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="22.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="22.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="22.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="22.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.8">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="22.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.10">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> ?</l>
					<l n="23" num="4.5"><w n="23.1">V<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> ! <w n="23.2">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ? <w n="23.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.4">n</w>’<w n="23.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="23.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="23.8">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="23.9">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.10">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
					<l n="24" num="4.6"><space unit="char" quantity="8"></space><w n="24.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="24.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="25.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.6">l</w>’<w n="25.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.10">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
					<l n="26" num="5.2"><w n="26.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rtr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="26.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.4">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="26.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.6">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lt<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="26.9">R<seg phoneme="wa" type="vs" value="1" rule="BEA8_1">oy</seg></w>,</l>
					<l n="27" num="5.3"><space unit="char" quantity="8"></space><w n="27.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="27.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="27.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="27.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="28" num="5.4"><w n="28.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="28.2">d</w>’<w n="28.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="28.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="28.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="28.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : <w n="28.7">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="28.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="28.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="28.10">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
					<l n="29" num="5.5"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="29.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="29.4">s</w>’<w n="29.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> : <w n="29.6">J</w>’<w n="29.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.8">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="29.9"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="29.10">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
					<l n="30" num="5.6"><space unit="char" quantity="8"></space><w n="30.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="30.2">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="30.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="30.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="30.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="31.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.4">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.5">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ? <w n="31.6"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="31.7">qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> ! <w n="31.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="31.9">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.10">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> :</l>
					<l n="32" num="6.2"><w n="32.1">L</w>’<w n="32.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="32.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="32.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="32.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="32.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="32.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>,</l>
					<l n="33" num="6.3"><space unit="char" quantity="8"></space><w n="33.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="33.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.3">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="33.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.6">c<seg phoneme="ɛ̃" type="vs" value="1" rule="494">ym</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="34" num="6.4"><w n="34.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="34.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="34.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="34.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="34.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="34.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.9"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w> ?</l>
					<l n="35" num="6.5">«-<w n="35.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="35.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="35.3">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="35.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>-<w n="35.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="35.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="35.7">d</w>’<w n="35.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> ?</l>
					<l n="36" num="6.6"><space unit="char" quantity="8"></space><w n="36.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="36.3">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="36.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="36.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !»</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">Juillet 1883</date>
						</dateline>
				</closer>
			</div></body></text></TEI>