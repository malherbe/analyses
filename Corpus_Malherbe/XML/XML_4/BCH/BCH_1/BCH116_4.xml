<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">L’AMOUR DIVIN</head><div type="poem" key="BCH116">
					<head type="number">XVIII</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.2">m</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="1.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="1.8">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.9">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="2.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.6">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>x<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="i" type="vs" value="1" rule="476">ï</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="3.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>lli<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="3.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="3.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="4.4">d</w>’<w n="4.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ssi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> : « <w n="5.4">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="2.2"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.6">s</w>’<w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> ;</l>
						<l n="7" num="2.3"><w n="7.1">V<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>-<w n="7.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.3">m</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.5">d</w>’<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> ?» — <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="7.9">j</w>’<w n="7.10"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="7.11">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.12">ou<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1">Cr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.2">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="8.6">l</w>’<w n="8.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.8">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="9.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ç<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.8">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.9"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
						<l n="10" num="3.2"><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="10.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
						<l n="11" num="3.3"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">cr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.6">m</w>’<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.9">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="12.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="12.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.8">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="12.9">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.10">m<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> ;</l>
						<l n="13" num="4.2"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="13.6">m</w>’<w n="13.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.8">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="13.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.10">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="4.3"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.3">m</w>’<w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>.</l>
					</lg>
				</div></body></text></TEI>