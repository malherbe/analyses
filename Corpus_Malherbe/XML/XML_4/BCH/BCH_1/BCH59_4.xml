<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES POËMES DE L’AMOUR ET DE LA MER</title>
				<title type="medium">Édition électronique</title>
				<author key="BCH">
					<name>
						<forename>Maurice</forename>
						<surname>Bouchor</surname>
					</name>
					<date from="1855" to="1929">1855-1929</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BCH_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
					<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
					<author>Maurice Bouchor</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9736743c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title>LES POËMES DE L’AMOUR ET DE LA MER</title>
							<author>Maurice Bouchor</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>Charpentier et Cie, Libraires-éditeurs</publisher>
								<date when="1876">1876</date>
							</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1876">1876</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p></p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA MORT DE L’AMOUR</head><div type="poem" key="BCH59">
					<head type="number">VIII</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.3">d</w>’<w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="1.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="1.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ?</l>
						<l n="2" num="1.2"><w n="2.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="2.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.4">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="2.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.7">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.8">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="3.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="3.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>ni<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="3.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.7">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>.</l>
						<l n="4" num="1.4"><w n="4.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="4.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.7">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">V<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.5">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="5.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.7">d</w>’<w n="5.8"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
						<l n="6" num="2.2"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="6.3">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w>, <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="6.6">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="7" num="2.3"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="7.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="7.4">pi<seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w>, <w n="7.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="7.6">s</w>’<w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> ? —</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="8.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="8.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.8"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="9.2">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="10" num="3.2"><w n="10.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.3">s</w>’<w n="10.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.6">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.8">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="10.9">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="11" num="3.3"><w n="11.1">B<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="11.2">d</w>’<w n="11.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> ?</l>
						<l n="12" num="3.4"><w n="12.1">B<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="12.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="12.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.8">gr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ff<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="13.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
						<l n="14" num="4.2"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.2">qu</w>’<w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="14.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.6">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.9">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="14.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffl<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.8">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>…</l>
						<l n="16" num="4.4"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="16.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>, <w n="16.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="16.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>