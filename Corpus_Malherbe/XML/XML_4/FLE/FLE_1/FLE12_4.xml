<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Friperies</title>
				<title type="medium">Une édition électronique</title>
				<author key="FLE">
					<name>
						<forename>Fernand</forename>
						<surname>FLEURET</surname>
					</name>
					<date from="1883" to="1945">1883-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>455 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FLE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Friperies</title>
						<author>Fernand Fleuret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/friperies00fleu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Friperies</title>
								<author>Fernand Fleuret</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>CHEZ EUGÈNE REY, LIBRAIRE</publisher>
									<date when="1907">1907</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1907">1907</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-08-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-08-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FLE12">
				<head type="main">APRÈS L’ENLÈVEMENT</head>
				<head type="sub_1">(Gretna-Green)</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">K<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>vi<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="1.4">cr<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">m<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="2.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="2.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.6">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="2.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w>, <w n="2.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> :</l>
					<l n="3" num="1.3"><w n="3.1">C</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="3.9">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="4.3">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="4.5">n</w>’<w n="4.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="4.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.8">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.9">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="o" type="vs" value="1" rule="433">o</seg>ps</w> <w n="4.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.11">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="5.4">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="5.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="5.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.10">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="5.11">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="6" num="2.2"><w n="6.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="6.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="6.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="7.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="7.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.8">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.9">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.7">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="8.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="8.9">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>…</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">K<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="9.2">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="9.3">l</w>’<w n="9.4"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.6">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.8">l</w>’<w n="9.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.2">qu</w>’<w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.4">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="10.7">l</w>’<w n="10.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.9">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.10"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.11">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>-<w n="10.12">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> :</l>
					<l n="11" num="3.3"><w n="11.1">K<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="11.5">qu</w>’<w n="11.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.7">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="11.8">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> <w n="11.9"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="12" num="3.4"><w n="12.1">C<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="12.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>si<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="12.5">Th<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="12.6">Gr<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="12.8">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>…</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">K<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="13.7">qu</w>’<w n="13.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.10">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.11"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.12">c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="14" num="4.2"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="14.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.6">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="14.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="14.8">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="15.2">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="15.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.4">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="15.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="15.6">d</w>’<w n="15.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.8">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="4.4"><w n="16.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="16.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w>…</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">K<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="17.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="17.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="17.5">d</w>’<w n="17.6"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="17.7">qu</w>’<w n="17.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.10">b<seg phoneme="wa" type="vs" value="1" rule="420">oî</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.11">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="18" num="5.2"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="18.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.4">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.8">h<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ;</l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="19.3">d</w>’<w n="19.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.5">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="19.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="20" num="5.4"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.3">bu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="20.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.5">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="20.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.7">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="20.8">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>…</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="21.2">K<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="21.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="21.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.5">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="21.6">t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="21.7">qu</w>’<w n="21.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.9"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.10">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.11">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="22" num="6.2"><w n="22.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="22.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.5">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="22.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="22.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="22.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.9">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="22.10">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w></l>
					<l n="23" num="6.3"><w n="23.1">Qu</w>’<w n="23.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="23.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.4">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="23.6">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="23.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.9">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="23.10">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="24" num="6.4"><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="24.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="24.4">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>gr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="24.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="24.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.8">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="24.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d</w>…</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="25.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.4">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="25.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="25.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.9">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.10">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="26" num="7.2"><w n="26.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="26.2">qu</w>’<w n="26.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="26.4">s<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="26.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="26.6">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="26.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="26.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>.</l>
					<l n="27" num="7.3"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="27.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="27.4">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="27.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.7">bu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.9">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="28" num="7.4"><w n="28.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="28.5">c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="28.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="28.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="28.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.9">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>…</l>
				</lg>
			</div></body></text></TEI>