<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Friperies</title>
				<title type="medium">Une édition électronique</title>
				<author key="FLE">
					<name>
						<forename>Fernand</forename>
						<surname>FLEURET</surname>
					</name>
					<date from="1883" to="1945">1883-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>455 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FLE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Friperies</title>
						<author>Fernand Fleuret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/friperies00fleu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Friperies</title>
								<author>Fernand Fleuret</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>CHEZ EUGÈNE REY, LIBRAIRE</publisher>
									<date when="1907">1907</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1907">1907</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-08-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-08-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FLE5">
				<head type="main">COLLOQUE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								« Si ton cœur n’est pas content.<lb></lb>
								La solitude t’attend<lb></lb>
								Comme un Fantôme à la porte. »
							</quote>
							<bibl>
								<name>Lucie Delarue-Mardrus.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="10"></space>— <w n="1.1">Qu</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="1.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">j</w>’<w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> ?</l>
					<l n="2" num="1.2"><space unit="char" quantity="10"></space><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="3" num="1.3"><space unit="char" quantity="10"></space><w n="3.1">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="4.4">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.7">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
					<l n="5" num="1.5"><space unit="char" quantity="10"></space><w n="5.1">L</w>’<w n="5.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sgr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><space unit="char" quantity="10"></space>— <w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> !</l>
					<l n="7" num="2.2"><space unit="char" quantity="10"></space><w n="7.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.3">qu</w>’<w n="7.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.5">t</w>’<w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="2.3"><space unit="char" quantity="10"></space><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="9" num="2.4"><w n="9.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="9.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> : <w n="9.4">c</w>’<w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="9.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>…</l>
					<l n="10" num="2.5"><space unit="char" quantity="10"></space><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="10.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="10.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><space unit="char" quantity="10"></space>— <w n="11.1">Qu</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="11.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="11.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">j</w>’<w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> ?…</l>
					<l n="12" num="3.2"><space unit="char" quantity="10"></space><w n="12.1">J</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="12.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.6">h<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="13" num="3.3"><space unit="char" quantity="10"></space><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.2">r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="14" num="3.4"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.2">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ils</w>, <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.6">d</w>’<w n="14.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>.</l>
					<l n="15" num="3.5"><space unit="char" quantity="10"></space><w n="15.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.4">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1"><space unit="char" quantity="10"></space>— <w n="16.1">V<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ts</w> <w n="16.2">d</w>’<w n="16.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
					<l n="17" num="4.2"><space unit="char" quantity="10"></space><w n="17.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="18" num="4.3"><space unit="char" quantity="10"></space><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.4">r<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="19" num="4.4"><w n="19.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.2">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nni<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="19.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="19.5">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="472">i</seg>i<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
					<l n="20" num="4.5"><space unit="char" quantity="10"></space><w n="20.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="20.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.5">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1"><space unit="char" quantity="10"></space>— <w n="21.1">Qu</w>’<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="21.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="21.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.5">j</w>’<w n="21.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>,</l>
					<l n="22" num="5.2"><space unit="char" quantity="10"></space><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="22.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="22.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nh<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="23" num="5.3"><space unit="char" quantity="10"></space><w n="23.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.2">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="24" num="5.4"><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="24.2">R<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, — <w n="24.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="24.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="24.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="24.6">l</w>’<w n="24.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> ?</l>
					<l n="25" num="5.5"><space unit="char" quantity="10"></space><w n="25.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.2">str<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ph<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="25.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1"><space unit="char" quantity="10"></space>— <w n="26.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> ! <w n="26.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="26.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="27" num="6.2"><space unit="char" quantity="10"></space><w n="27.1">R<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="27.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="27.3">s</w>’<w n="27.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="28" num="6.3"><space unit="char" quantity="10"></space><w n="28.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.4">pl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="29" num="6.4"><w n="29.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="29.2">f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="29.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="29.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>il</w> <w n="29.5">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="29.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="30" num="6.5"><space unit="char" quantity="10"></space><w n="30.1">Br<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="30.2">qu</w>’<w n="30.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="7">
					<l n="31" num="7.1"><space unit="char" quantity="10"></space>— <w n="31.1">Qu</w>’<w n="31.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="31.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="31.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.5">j</w>’<w n="31.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> ?</l>
					<l n="32" num="7.2"><space unit="char" quantity="10"></space><w n="32.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="32.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="32.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w> ;</l>
					<l n="33" num="7.3"><space unit="char" quantity="10"></space><w n="33.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="33.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.5">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="34" num="7.4"><w n="34.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="34.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="34.4">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="34.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="34.6">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="34.7">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="34.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>.</l>
					<l n="35" num="7.5"><space unit="char" quantity="10"></space><w n="35.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sgr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
			</div></body></text></TEI>