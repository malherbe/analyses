<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Friperies</title>
				<title type="medium">Une édition électronique</title>
				<author key="FLE">
					<name>
						<forename>Fernand</forename>
						<surname>FLEURET</surname>
					</name>
					<date from="1883" to="1945">1883-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>455 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FLE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Friperies</title>
						<author>Fernand Fleuret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/friperies00fleu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Friperies</title>
								<author>Fernand Fleuret</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>CHEZ EUGÈNE REY, LIBRAIRE</publisher>
									<date when="1907">1907</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1907">1907</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-08-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-08-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FLE7">
				<head type="main">COQUILLAGES</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">Gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="2.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w></l>
					<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.3">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">c<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="6.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
					<l n="7" num="2.3"><w n="7.1">D</w>’<w n="7.2"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="7.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.4">s</w>’<w n="7.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="2.4"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>…</l>
					<l n="9" num="2.5">— <w n="9.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="9.2">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="3">
					<l n="10" num="3.1"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="10.2">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4">
					<l n="11" num="4.1"><w n="11.1">Vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.2">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>.</l>
					<l n="12" num="4.2"><w n="12.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
					<l n="13" num="4.3"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="13.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="13.3">s<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="14" num="4.4"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> !</l>
				</lg>
				<lg n="5">
					<l n="15" num="5.1"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.2">M<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="15.3">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="5.2"><w n="16.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>…</l>
					<l n="17" num="5.3"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="18" num="5.4"><w n="18.1">P<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.2">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>,</l>
					<l n="19" num="5.5"><w n="19.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.3">d</w>’<w n="19.4"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
				</lg>
				<lg n="6">
					<l n="20" num="6.1">— <w n="20.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="20.2">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>