<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BOURDEAU DES NEUF PUCELLES</title>
				<title type="medium">Édition électronique</title>
				<author key="FRT">
					<name>
						<forename>Charles-Théophile</forename>
						<surname>FERET</surname>
					</name>
					<date from="1858" to="1928">1858-1928</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1111 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le Bourdeau des neuf pucelles</title>
						<author>Charles-Théophile Feret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/66781</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Bourdeau des neuf pucelles</title>
								<author>Charles-Théophile Feret</author>
								<imprint>
									<pubPlace>CAUDÉRAN-BORDEAUX</pubPlace>
									<publisher>ÉDITIONS DES CAHIERS LITTÉRAIRES</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1912">1912</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et les notes de l’éditeur n’ont pas été reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<p>Les notes de bas de page (numérotation conforme à l’édition imprimée) ont été reportées en fin de poème.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-12-09" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-12-10" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CLIO</head><head type="sub_part">Muse de l’Histoire</head><div type="poem" key="FRT19">
					<head type="main">Mademoiselle de Scudéry</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> ! <w n="1.3">n</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sm<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="1.6">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ph<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sv<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>il</w>, <w n="2.3">z<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r</w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5">r<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="3" num="1.3"><w n="3.1">S</w>’<w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.4">cu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.6">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.7">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>lt</w></l>
						<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="4.4">j<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>scu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="5.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="5.4">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="6" num="2.2"><w n="6.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.2">thr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>sn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="7.3">pi<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.4">c<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w></l>
						<l n="8" num="2.4"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="8.3">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.6">qu<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">V<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="9.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="9.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="9.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w>,</l>
						<l n="10" num="3.2">— <w n="10.1">R<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.4">fi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.5">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>. —</l>
						<l n="11" num="3.3"><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="11.5">S<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w></l>
						<l n="12" num="3.4"><w n="12.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="12.3">d<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s</w> <w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.4">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="13.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>,</l>
						<l n="14" num="4.2"><w n="14.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="14.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ct</w> <w n="14.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>lp<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="15" num="4.3"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
						<l n="16" num="4.4"><w n="16.1">D</w>’<w n="16.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="16.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ci<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>lp<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">L</w>’<w n="17.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="17.4">s</w>’<w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></l>
						<l n="18" num="5.2"><w n="18.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="18.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="18.4">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.6">pi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="18.7">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="19" num="5.3"><w n="19.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.5">l</w>’<w n="19.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c</w>,</l>
						<l n="20" num="5.4"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="20.2">r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.4">phr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.5">d</w>’<w n="20.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">L</w>’<w n="21.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>-<w n="21.3">q<seg phoneme="wa" type="vs" value="1" rule="159">ua</seg>rt<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>, <w n="21.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="21.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
						<l n="22" num="6.2"><w n="22.1">M<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="22.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.4">p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="22.5">su<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.6">l</w>’<w n="22.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="23" num="6.3"><w n="23.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>-<w n="23.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="23.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="23.6">l</w>’<w n="23.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
						<l n="24" num="6.4"><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="24.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="24.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.5">s</w>’<w n="24.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1"><seg phoneme="ɔ" type="vs" value="1" rule="443">O</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="25.2">v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>lx</w>, — <w n="25.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="25.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> —</l>
						<l n="26" num="7.2"><w n="26.1">Vi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="26.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="26.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="26.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="26.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="26.7">l</w>’<w n="26.8">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="27" num="7.3"><w n="27.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="27.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>scl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> — <w n="27.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.4">sç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="27.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="27.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> —</l>
						<l n="28" num="7.4"><w n="28.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.2">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w>, <w n="28.3">l</w>’<w n="28.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.7">ch<seg phoneme="i" type="vs" value="1" rule="493">y</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="29.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="29.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="29.5"><seg phoneme="ɔ" type="vs" value="1" rule="439">O</seg>st<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="29.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w></l>
						<l n="30" num="8.2"><w n="30.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.2">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="30.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.5">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="31" num="8.3"><w n="31.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.2">li<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="31.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.4">l</w>’<w n="31.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> (<w n="31.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>h</w> ! <w n="31.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>)</l>
						<l n="32" num="8.4"><w n="32.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gs<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="32.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="32.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="33.2">Ph<seg phoneme="e" type="vs" value="1" rule="250">œ</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ! <w n="33.3">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="33.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
						<l n="34" num="9.2"><w n="34.1">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="34.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="34.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.6">sc<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="35" num="9.3"><w n="35.1">L</w>’<w n="35.2"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="35.3">d</w>’<w n="35.4">H<seg phoneme="i" type="vs" value="1" rule="468">i</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="35.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="35.7">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ;</l>
						<l n="36" num="9.4"><w n="36.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="36.2">l</w>’<w n="36.3">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="36.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="36.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.6">P<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Qu</w>’<w n="37.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="37.3">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="37.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="37.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="37.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="37.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gr<seg phoneme="ø" type="vs" value="1" rule="390">eû</seg></w></l>
						<l n="38" num="10.2"><w n="38.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="38.4">ru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="38.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.6">m</w>’<w n="38.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ff<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="39" num="10.3"><w n="39.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.2">B<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="39.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="39.4">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="39.5">S<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> ! <w n="39.6">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.7">r<seg phoneme="y" type="vs" value="1" rule="445">û</seg></w></l>
						<l n="40" num="10.4"><w n="40.1">M<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="40.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="40.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="40.4">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ph<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="41.2">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="41.3">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="41.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg></w> <w n="41.5">l</w>’<w n="41.6"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w></l>
						<l n="42" num="11.2"><w n="42.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="42.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="42.3">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="42.4">j</w>’<w n="42.5"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="42.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>br<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="43" num="11.3"><w n="43.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="43.2">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="43.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="43.4">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="43.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="e" type="vs" value="1" rule="353">e</seg>tt<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>…</l>
						<l n="44" num="11.4"><w n="44.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="44.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="44.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="44.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="44.5">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>. <w n="44.6">L<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="44.7">L<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="45.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="45.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="45.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="45.5">qu</w>’<w n="45.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="45.7">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="45.8">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w></l>
						<l n="46" num="12.2"><w n="46.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="46.2">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st</w> <w n="46.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="46.4">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="46.5">S<seg phoneme="i" type="vs" value="1" rule="493">y</seg>lv<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="47" num="12.3"><w n="47.1"><seg phoneme="o" type="vs" value="1" rule="435">O</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="47.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="47.3">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="47.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="47.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w></l>
						<l n="48" num="12.4"><w n="48.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="48.2">l</w>’<w n="48.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="48.4">R<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="48.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="48.6">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1"><seg phoneme="ɔ" type="vs" value="1" rule="443">O</seg>r</w>, <w n="49.2">j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="49.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="49.4">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="49.5">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="49.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="50" num="13.2"><w n="50.1">C<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="50.2">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="50.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="50.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="50.5">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lpr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="51" num="13.3"><w n="51.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="51.2">fu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="51.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="51.4">P<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="51.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="51.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="51.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>cr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="52" num="13.4"><w n="52.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="52.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="52.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="52.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="52.5">M<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>