<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE BOURDEAU DES NEUF PUCELLES</title>
				<title type="medium">Édition électronique</title>
				<author key="FRT">
					<name>
						<forename>Charles-Théophile</forename>
						<surname>FERET</surname>
					</name>
					<date from="1858" to="1928">1858-1928</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1111 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le Bourdeau des neuf pucelles</title>
						<author>Charles-Théophile Feret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/66781</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Bourdeau des neuf pucelles</title>
								<author>Charles-Théophile Feret</author>
								<imprint>
									<pubPlace>CAUDÉRAN-BORDEAUX</pubPlace>
									<publisher>ÉDITIONS DES CAHIERS LITTÉRAIRES</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1912">1912</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et les notes de l’éditeur n’ont pas été reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).
				</p>
				<p>Les notes de bas de page (numérotation conforme à l’édition imprimée) ont été reportées en fin de poème.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-12-09" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-12-10" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Vers pour <lb></lb>LES SERVANTES</head><div type="poem" key="FRT28">
					<head type="main">Jour de marasme</head>
					<head type="sub_1">Du Vieux peintre amant de sa bonne</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>li<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>, <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="2" num="1.2"><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="2.6">f<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="2.7">d</w>’<w n="2.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.9">l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
						<l n="3" num="1.3"><w n="3.1">R<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c</w> <w n="3.4">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="3.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="3.8">cu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
						<l n="4" num="1.4"><w n="4.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="4.3">n<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.5">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.6">d</w>’<w n="4.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.8">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.9"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>, <w n="5.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="5.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="5.7">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rç<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.4">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>squ<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.6">cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.8">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="6.9">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Qu</w>’<w n="7.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>-<w n="7.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="7.4">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ? <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="7.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="7.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.9">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="7.11">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.12">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="8.2">ti<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="8.3">gr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="8.4">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.6">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.8">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">g<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>th<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="9.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="9.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.8">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w></l>
						<l n="10" num="3.2"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lf<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="10.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Fl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.5">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.6">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.9">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.11">l</w>’<w n="11.12"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">P<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="12.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="12.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="12.7">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>bs</w> <w n="12.9">s</w>’<w n="12.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
						<l n="13" num="4.2"><w n="13.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w>, <w n="13.2">d</w>’<w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.5">cr<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="13.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.7">l</w>’<w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.9">bl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg><seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="4.3"><w n="14.1">D</w>’<w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rb<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="14.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.5">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.6">cr<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="14.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.9">b<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="14.10">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					</lg>
				</div></body></text></TEI>