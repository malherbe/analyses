<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><div type="poem" key="AIC31">
					<head type="number">V</head>
					<head type="main">LUMIÈRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="1.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.6">qu</w>’<w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>, <w n="2.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="2.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="2.7">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.8">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">V<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.6">l</w>’<w n="3.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.8">ru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.10">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="5.2">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.3">d</w>’<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.5">l</w>’<w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="5.7">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.9">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="6.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">s</w>’<w n="7.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="7.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.8">br<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-27">e</seg></w> <w n="7.9">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.6">n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="8.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.9">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="9.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="9.6">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="10.2">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.3">t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>br<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.6">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ;</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.8">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="11.9">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.10">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.11"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.12">fu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w></l>
						<l n="12" num="3.4"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="12.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.7">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="13.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>, <w n="13.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">n</w>’<w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="13.6">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="13.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.8">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.10">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
						<l n="14" num="4.2"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>, <w n="14.5">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.8">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="15" num="4.3"><w n="15.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">Di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="15.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="15.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="4.4"><w n="16.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> : <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="16.7">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="16.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.9"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1865">1865</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>