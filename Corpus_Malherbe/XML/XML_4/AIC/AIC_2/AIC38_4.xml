<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><div type="poem" key="AIC38">
					<head type="number">IV</head>
					<head type="main">SAUTS PÉRILLEUX</head>
					<opener>
						<salute><hi rend="ital">À l’Auteur des Misérables.</hi></salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lt<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="2" num="1.2"><w n="2.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.2">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>st<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">d</w>’<w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.5">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="3.6">c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">Sc<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.2">d</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.5">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>g</w> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.7">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="6" num="2.2"><w n="6.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="6.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="7.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>ill<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
						<l n="8" num="2.4"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>pi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="9" num="2.5"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.4"><seg phoneme="y" type="vs" value="1" rule="251">eû</seg>t</w> <w n="9.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="9.6">qu</w>’<w n="9.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="10" num="2.6"><w n="10.1">G<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.6">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">J<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="11.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.6">f<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="12" num="3.2"><w n="12.1">B<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">s</w>’<w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.6">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> ;</l>
						<l n="13" num="3.3"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="13.2">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">dr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="14" num="3.4"><w n="14.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.7">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> !</l>
					</lg>
					<lg n="4">
						<l n="15" num="4.1"><w n="15.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="15.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.6">bl<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="4.2"><w n="16.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">bl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="16.5">qu</w>’<w n="16.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="17" num="4.3"><w n="17.1">S<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="17.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="17.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ;</l>
						<l n="18" num="4.4"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="18.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.5">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="19" num="4.5"><w n="19.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.3">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="20" num="4.6"><w n="20.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="20.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="20.3">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="20.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> !</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> ! <w n="21.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="21.4">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="22" num="5.2"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> ! <w n="22.2">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="22.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="22.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> !</l>
						<l n="23" num="5.3"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="23.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.4">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="23.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="23.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="23.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="24" num="5.4"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="24.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.5">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="24.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> !</l>
					</lg>
					<lg n="6">
						<l n="25" num="6.1"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="25.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="25.3">l</w>’<w n="25.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.5">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="25.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.7">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
						<l n="26" num="6.2"><w n="26.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="26.2">l</w>’<w n="26.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="26.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="26.7">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="27" num="6.3"><w n="27.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="27.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="27.5">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="27.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.7">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> !</l>
						<l n="28" num="6.4"><w n="28.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="28.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="28.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="28.4">pl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="29" num="6.5"><w n="29.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="29.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="29.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="29.5">l</w>’<w n="29.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.7">t</w>’<w n="29.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ccl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="30" num="6.6"><w n="30.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="30.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="30.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.5">t</w>’<w n="30.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>du<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> ?</l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1"><w n="31.1">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h</w> ! <w n="31.2">qu</w>’<w n="31.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="31.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.6">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="32" num="7.2"><w n="32.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="32.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="32.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="32.6">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ;</l>
						<l n="33" num="7.3"><w n="33.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="33.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.3">gr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="34" num="7.4"><w n="34.1">C</w>’<w n="34.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="34.3">dr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="34.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.6">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> !</l>
					</lg>
					<lg n="8">
						<l n="35" num="8.1"><w n="35.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="35.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w>, <w n="35.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="35.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="35.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="35.6">s</w>’<w n="35.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="36" num="8.2"><w n="36.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="36.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="36.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="36.4">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="36.5">n</w>’<w n="36.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="36.7">qu</w>’<w n="36.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="36.9">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="37" num="8.3"><w n="37.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="37.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.3">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="37.4">l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="37.6">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
						<l n="38" num="8.4"><w n="38.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="38.2">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="38.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="38.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="39" num="8.5"><w n="39.1">F<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="39.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="39.3">ch<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="39.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="40" num="8.6"><w n="40.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lt<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="40.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="40.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppl<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> !</l>
					</lg>
					<lg n="9">
						<l n="41" num="9.1"><w n="41.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="41.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="41.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="41.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="41.5">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="41.6">l</w>’<w n="41.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="42" num="9.2"><w n="42.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="42.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="42.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="42.4">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="42.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="42.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> !…</l>
						<l n="43" num="9.3"><w n="43.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="43.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="43.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="43.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="43.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="43.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="43.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="44" num="9.4"><w n="44.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="44.2">l</w>’<w n="44.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="44.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="44.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="44.6">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="44.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Toulon</placeName>,
							<date when="1866">8 juillet 1866</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>