<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La Légende des Sexes</title>
				<title type="sub">Poëmes Hystériques</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https ://fr.wikisource.org/wiki/La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Légende des sexes, poëmes hystériques</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https ://fr.wikisource.org/wiki/Fichier :Haraucourt_-_La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques,_1882.djvu</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Imprimé à Bruxelles pour l’auteur</publisher>
									<date when="1882">1882</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/btv1b8618380c</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>ÉDITION PRIVILE, REVUE PAR L’AUTEUR</publisher>
							<date when="1893">1893</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1882">1882</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-28" who="RR">Une correction introduite à partir de l’édition imprimée de 1893</change>
				<change when="2017-10-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La légende des sexes</head><div type="poem" key="HAR25">
					<head type="main">VŒU</head>
					<head type="form">SONNET</head>
					<opener>
						<salute>À Édouard d’Otémar.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="1.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! <w n="1.3">L</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="1.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.8">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">n</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="2.6">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>t</w> <w n="2.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.2">f<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.10">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="3.11">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w></l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="4.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="4.3">d</w>’<w n="4.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.5">bl<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="4.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="4.9">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="5.4">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.10"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="6.2">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">d</w>’<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="6.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.7">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="7" num="2.3"><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-27">e</seg></w> <w n="7.2">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="7.3">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="7.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w></l>
						<l n="8" num="2.4"><w n="8.1">L</w>’<w n="8.2"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="8.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.6">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.8">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.9">n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> : <w n="9.5">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="9.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.9">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.11">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
						<l n="10" num="3.2"><w n="10.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="10.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="432">o</seg>s</w> <w n="10.4">br<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.7">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>scl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.8">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rtr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
						<l n="11" num="3.3"><w n="11.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.3">sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="11.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="12.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="12.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>,</l>
						<l n="13" num="4.2"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="13.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.4">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="13.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.9">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="4.3"><w n="14.1">T</w>’<w n="14.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.6">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="14.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.9">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> !</l>
					</lg>
				</div></body></text></TEI>