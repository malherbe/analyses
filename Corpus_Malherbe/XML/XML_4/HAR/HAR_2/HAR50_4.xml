<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR50">
						<head type="main">LE CHANT DU DÉPART</head>
						<lg n="1">
							<l n="1" num="1.1"><space unit="char" quantity="10"></space><w n="1.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="1.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						</lg>
						<lg n="2">
							<l n="2" num="2.1"><w n="2.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>. — <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="2.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rb<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="2.4">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.8">bl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> :</l>
							<l n="3" num="2.2"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>-<w n="3.5">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.8">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="4" num="2.3"><w n="4.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.8">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> !</l>
							<l n="5" num="2.4"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.3">l</w>’<w n="5.4">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.9">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="6" num="2.5"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="6.2">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> ! <w n="6.3">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="6.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.6">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>, <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="6.9">s</w>’<w n="6.10"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bst<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
							<l n="7" num="2.6"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">bru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.10">l</w>’<w n="7.11">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>…</l>
						</lg>
						<lg n="3">
							<l n="8" num="3.1"><space unit="char" quantity="10"></space><w n="8.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="8.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						</lg>
						<lg n="4">
							<l n="9" num="4.1"><w n="9.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>. — <w n="9.2">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.3">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="9.4">l</w>’<w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w> <w n="9.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>,</l>
							<l n="10" num="4.2"><w n="10.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="10.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="10.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.6">cr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ; <w n="10.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="10.9">cr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="11" num="4.3"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.5">h<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="11.7">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.8">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="11.9">dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>…</l>
							<l n="12" num="4.4"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="12.2">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rth<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="12.4">S<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! <w n="12.5">N<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.7">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="493">y</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
							<l n="13" num="4.5"><w n="13.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="13.3">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="13.5">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.8">c<seg phoneme="i" type="vs" value="1" rule="493">y</seg>cl<seg phoneme="o" type="vs" value="1" rule="442">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="14" num="4.6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.7">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>…</l>
						</lg>
						<lg n="5">
							<l n="15" num="5.1"><space unit="char" quantity="10"></space><w n="15.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="15.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						</lg>
						<lg n="6">
							<l n="16" num="6.1"><w n="16.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>. — <w n="16.2">L</w>’<w n="16.3"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="16.4">bl<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="16.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="16.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> :</l>
							<l n="17" num="6.2"><w n="17.1">L</w>’<w n="17.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>tl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rg<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.6">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="17.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.8">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="18" num="6.3"><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.7">fl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>x</w> <w n="18.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.9">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> !</l>
							<l n="19" num="6.4"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="19.6">gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="19.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.9">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ll<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="20" num="6.5"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="20.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="20.3">di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="20.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="20.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="20.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="20.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.8">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.9">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="21" num="6.6"><w n="21.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="21.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>il</w> <w n="21.4">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.7">g<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>…</l>
						</lg>
						<lg n="7">
							<l n="22" num="7.1"><space unit="char" quantity="10"></space><w n="22.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="22.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="22.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						</lg>
						<lg n="8">
							<l n="23" num="8.1"><w n="23.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>. — <w n="23.2">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.3">T<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="23.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> : <w n="23.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="23.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="23.8">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d</w>, <w n="23.9"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="23.10">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="23.11">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> ;</l>
							<l n="24" num="8.2"><w n="24.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>lc<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="24.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>ts</w> <w n="24.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="24.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="24.9">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="25" num="8.3"><w n="25.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="25.2">d</w>’<w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>, <w n="25.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="25.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="25.7">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="25.8">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.9">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="25.10">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="25.11">bru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>,</l>
							<l n="26" num="8.4"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="26.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="26.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.7">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="26.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.9">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="27" num="8.5"><w n="27.1">C</w>’<w n="27.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="27.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> : <w n="27.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.6">S<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="27.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="27.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.9">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="28" num="8.6"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="28.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.3">T<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w>, <w n="28.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.7">S<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="28.8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>…</l>
						</lg>
						<lg n="9">
							<l n="29" num="9.1"><space unit="char" quantity="10"></space><w n="29.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="29.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="29.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						</lg>
					</div></body></text></TEI>