<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURE</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><head type="main_subpart">MIDI</head><div type="poem" key="HAR131">
						<head type="main">L’AXE</head>
						<opener>
							<salute>À FÉLICIEN ROPS</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>, <w n="1.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">v<seg phoneme="ø" type="vs" value="1" rule="248">œu</seg>x</w>, <w n="1.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.5">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="2.8">c<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">f<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.4">j<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.7">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="3.8">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> ;</l>
							<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w> <w n="4.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="4.4">d</w>’<w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>-<w n="4.9">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> ;</l>
							<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.4">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="5.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="5.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="6" num="1.6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.3">r<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.4">d</w>’<w n="6.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.8">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="6.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.10">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
							<l n="7" num="1.7"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="7.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.10">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
							<l n="8" num="1.8"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rds</w> <w n="8.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w> <w n="8.4">n<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="8.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.7"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="8.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.9">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
							<l n="9" num="1.9"><w n="9.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">du<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w>, <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4">sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.7">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="10" num="1.10"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3">vi<seg phoneme="e" type="vs" value="1" rule="383">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.5">d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> <w n="10.6">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="10.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.9">pi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="11" num="1.11"><w n="11.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rj<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="11.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pts</w> ; <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493">y</seg>rs</w>, <w n="11.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> ;</l>
							<l n="12" num="1.12"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2">l<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.5">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.6"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="12.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.8">f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="12.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.10">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> ;</l>
							<l n="13" num="1.13"><w n="13.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.2">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="13.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ls</w>, <w n="13.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6">vi<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ls</w>, <w n="13.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="13.9">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
							<l n="14" num="1.14"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">ti<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.3">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs</w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr</w>’<w n="14.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="14.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="15" num="1.15"><w n="15.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="15.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.5">gu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="15.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="15.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> ;</l>
							<l n="16" num="1.16"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="16.7">l</w>’<w n="16.8"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> ; <w n="16.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
							<l n="17" num="1.17"><w n="17.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.2">h<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="17.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="17.5">h<seg phoneme="i" type="vs" value="1" rule="493">y</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.7">pr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="18" num="1.18"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.5">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="18.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="18.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.9">tr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
							<l n="19" num="1.19"><w n="19.1">Pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="19.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.3">gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> : <w n="19.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.5">r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="19.6">r<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="19.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.9">r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
							<l n="20" num="1.20"><w n="20.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.2">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.4">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="20.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>ps</w> <w n="20.7">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="20.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.9">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>fs</w> <w n="20.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.11">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>,</l>
							<l n="21" num="1.21"><w n="21.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.3">ch<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c</w> <w n="21.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.5">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="21.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="21.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.8">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
							<l n="22" num="1.22"><w n="22.1">Pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="22.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.3">di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="22.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="22.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="22.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.8">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="23" num="1.23"><w n="23.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="23.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>, <w n="23.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="23.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>, <w n="23.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="23.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.9">v<seg phoneme="ø" type="vs" value="1" rule="248">œu</seg></w>, <w n="23.10">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="23.11"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.12">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> !</l>
							<l n="24" num="1.24"><w n="24.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="24.2">l</w>’<w n="24.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">O</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : « <w n="24.6">P<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="24.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> : <w n="24.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="24.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="24.11">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w>. »</l>
						</lg>
					</div></body></text></TEI>