<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BUS27">
				<head type="main">UN TABLEAU DE SNYDERS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="1.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.4">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w>-<w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="1.7">d</w>’<w n="1.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.9">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">r<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="2.4">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.5">qu</w>’<w n="2.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.8">B<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
					<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="3.3">hi<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="3.6">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w> <w n="3.7">d</w>’<w n="3.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.10">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.3">m</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, — <w n="4.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="4.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="4.9">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="4.10"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.11">s<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.4"><seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="5.6">Sn<seg phoneme="i" type="vs" value="1" rule="493">y</seg>d<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>, <w n="5.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.8"><seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.10">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="5.11">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="2.2"><w n="6.1">R<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="ɛ̃" type="vs" value="1" rule="221">en</seg>s</w> <w n="6.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppl<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w> <w n="6.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>.</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.7">V<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="7.8"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="7.9">D<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="2.4"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.2">v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="8.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.8">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> :</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="9.2">li<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.5">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="9.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="10" num="3.2"><w n="10.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="10.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3">li<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.5">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ssi<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="10.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">s</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ssi<seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="11.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.5">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="11.7">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="11.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.9">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> !</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="12.2">vi<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="170">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="12.5">d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w>, <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="12.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="13" num="4.2"><w n="13.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.2">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.3">d</w>’<w n="13.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="13.5">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
					<l n="14" num="4.3"><w n="14.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="14.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.5">R<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, — <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.9">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
				</lg>
				<closer>
					<dateline>
						<placeName>Bordeaux</placeName>,
						<date when="1857">1857</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>