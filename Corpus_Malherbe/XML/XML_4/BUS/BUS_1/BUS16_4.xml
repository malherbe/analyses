<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES CHEMINS</head><head type="sub_part">IMPRESSIONS DE VOYAGE</head><head type="sub_part">PORTUGAL-ESPAGNE</head><div type="poem" key="BUS16">
					<head type="number">XV</head>
					<head type="main">GRENADE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">br<seg phoneme="y" type="vs" value="1" rule="dc-3">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">z<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="1.6">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ccr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="2.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="2.6">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>.</l>
						<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="3.5">d</w>’<w n="3.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.9">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.10"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.12">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="4.2">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.5">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.8">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="4.9">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="5.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.7">qu</w>’<w n="5.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.10">v<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="6" num="2.2"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ils</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="6.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="6.6">fr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>fr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>.</l>
						<l n="7" num="2.3"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.3">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.5">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.8">r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.10">s<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.2">fr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="8.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>, <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.7">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">N<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.8">s</w>’<w n="9.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="10" num="3.2"><w n="10.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w></l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.4">d</w>’<w n="11.5">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="11.6">s</w>’<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.9">s</w>’<w n="11.10"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.3">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.6">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="12.7">t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>br<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="13" num="4.2"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="13.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">l</w>’<w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>lb<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="494">yn</seg></w> <w n="13.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="13.9">m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w></l>
						<l n="14" num="4.3"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="14.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.8">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					</lg>
				</div></body></text></TEI>