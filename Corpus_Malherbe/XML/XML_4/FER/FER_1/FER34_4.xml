<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MÉLODIES POÉTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1014 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Mélodies poétiques</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/albertferlandmelodiespoetiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Mélodies poétiques</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://books.google.fr/books?id=TBENAAAAYAAJ</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Pierre J. Bédard, Imprimeur-relieur</publisher>
									<date when="1893">1893</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VOIX INTÉRIEURES</head><div type="poem" key="FER34">
					<head type="main">JÉHOVAH</head>
					<opener>
						<salute>À MON AMI J. MARIE AMÉDÉE DENAULT</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="1.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>, <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.7">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.8">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="1.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.10">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.5">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.9">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.10">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> ;</l>
						<l n="3" num="1.3"><w n="3.1">S</w>’<w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>, <w n="3.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.6">n</w>’<w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="3.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.10">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.11">h<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.12">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.13">n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
						<l n="5" num="1.5"><w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">s</w>’<w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ps<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="5.6">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.9">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="1.6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.3">ci<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="6.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="7" num="1.7"><w n="7.1">Pr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="7.2">qu</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="7.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.6">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.7">c</w>’<w n="7.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="7.10">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">c</w>’<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.4">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.7">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="8.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.10">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.11"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.12">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="9" num="1.9"><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">d</w>’<w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.8">v<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.9">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w> <w n="10.4">qu</w>’<w n="10.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.6">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w>.</l>
						<l n="11" num="1.11"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">R<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="11.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.10">Di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="11.11">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="12" num="1.12"><w n="12.1">C<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.3">s</w>’<w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="12.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="13" num="1.13"><w n="13.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="13.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="13.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="13.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.7">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.9">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>-<w n="13.10">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="14.2">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="14.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="15" num="1.15"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">S<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="15.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.8">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="1.16"><w n="16.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.4">l</w>’<w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="i" type="vs" value="1" rule="487">i</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="16.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.9">l</w>’<w n="16.10"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="17" num="1.17"><w n="17.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">l</w>’<w n="17.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.6">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="17.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="17.8">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="17.9">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.10">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="18" num="1.18"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.6">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="18.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="19" num="1.19"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="19.3">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">l</w>’<w n="19.6"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">Qu</w>’<w n="20.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="20.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.</l>
					</lg>
					<lg n="2">
						<l n="21" num="2.1"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="21.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="21.6">d</w>’<w n="21.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="21.8">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="21.9">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="21.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.11">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.12"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="22" num="2.2"><w n="22.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="22.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.7">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="22.8">l</w>’<w n="22.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ;</l>
						<l n="23" num="2.3"><w n="23.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">si<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.5">n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="23.7">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="23.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.10"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="24" num="2.4"><space unit="char" quantity="8"></space><w n="24.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.2">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="24.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="24.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.5">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
						<l n="25" num="2.5"><w n="25.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="25.2">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="25.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rti<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="25.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.5">l</w>’<w n="25.6">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.7">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="26" num="2.6"><w n="26.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu</w>’<w n="26.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.6">n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="27" num="2.7"><w n="27.1">N</w>’<w n="27.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.3">qu</w>’<w n="27.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="27.6">qu</w>’<w n="27.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="27.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="27.9">d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="27.10"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="27.11">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>.</l>
						<l n="28" num="2.8"><w n="28.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="28.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="28.5">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="28.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="28.7">m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="28.8">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="28.9">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="29" num="2.9"><w n="29.1">M<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="29.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.3">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="29.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="29.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w> <w n="29.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="30" num="2.10"><space unit="char" quantity="8"></space><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="30.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="30.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="30.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="30.6">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>.</l>
					</lg>
				</div></body></text></TEI>