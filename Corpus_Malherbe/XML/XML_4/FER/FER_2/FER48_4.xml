<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FEMMES RÊVÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>234 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Femmes Rêvées</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/12365</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Femmes Rêvées</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://archive.org/details/femmesrves00ferl</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Chez l’auteur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1899">1899</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FEMMES RÊVÉES</head><div type="poem" key="FER48">
					<head type="main">La Chasseresse</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Diane au carquois d’or,déesse bocagère,<lb></lb>
									Qui, la flèche à la main,de sa robe légère,<lb></lb>
									Nouait sur ses genoux les replis ondoyans :
								</quote>
								<bibl>
									<name>DE FONTANES</name>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.4">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.7">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rg<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.5">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="3.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">qu</w>’<w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="3.6">d</w>’<w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.8">s<seg phoneme="i" type="vs" value="1" rule="493">y</seg>lv<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="3.9">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">F<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">n<seg phoneme="ɛ̃" type="vs" value="1" rule="494">ym</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="5.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="5.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w>, <w n="5.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="5.6">d</w>’<w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.9">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="œ" type="vs" value="1" rule="286">Œ</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lti<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="6.5">s</w>’<w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="7.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.4">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="7.7">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="8.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="8.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.9">plu<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>