<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR16">
				<lg n="1">
					<l n="1" num="1.1"><space quantity="12" unit="char"></space><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="1.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>-<w n="1.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.7">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">Fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">j</w>’<w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr</w>’<w n="2.9"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>tr</w>’<w n="3.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="3.3">d</w>’<w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.6">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.9">gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="4.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.4">c</w>’<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.7">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, — <w n="4.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.9">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.10">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.11">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.12">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> !</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="5.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="5.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.5">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, — <w n="5.6">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="5.7">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="6.2">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.6">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="6.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.10">Nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="6.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.12">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.5">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="7.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="1.8"><space quantity="12" unit="char"></space><w n="8.1">L</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">j</w>’<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="8.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.8">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><space quantity="12" unit="char"></space><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">n</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="9.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.5">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.7">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="2.2"><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w>, — <w n="10.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="10.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="10.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.10"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.11">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>,</l>
					<l n="11" num="2.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">s</w>’<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.6">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="11.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="11.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.11">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="2.4"><w n="12.1"><seg phoneme="ɔ" type="vs" value="1" rule="439">O</seg>bst<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="12.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="12.3">cr<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="12.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">n</w>’<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="13.5">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.7">s</w>’<w n="13.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.10">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.11">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.12"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="14" num="3.2"><w n="14.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.2">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="14.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="14.6">p<seg phoneme="y" type="vs" value="1" rule="445">û</seg>t</w> <w n="14.7">n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>,</l>
					<l n="15" num="3.3"><w n="15.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="15.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.4">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="15.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="15.8">qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.9">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="16" num="3.4"><space quantity="12" unit="char"></space><w n="16.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ! <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="16.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! — <w n="16.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.6">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> !</l>
				</lg>
				<lg n="4">
					<l n="17" num="4.1"><space quantity="12" unit="char"></space><w n="17.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="17.2">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.6">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="18" num="4.2"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="18.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="18.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="18.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="18.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.10">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> ;</l>
					<l n="19" num="4.3"><w n="19.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="19.2">l</w>’<w n="19.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="19.5">qu</w>’<w n="19.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="19.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="19.9">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="20" num="4.4"><w n="20.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="20.2">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="20.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.6">l</w>’<w n="20.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ! — <w n="20.8">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="20.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.10">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> !!</l>
					<l ana="incomplete" where="F" n="21" num="4.5"><w n="21.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="21.3">v<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.4">d</w>’<w n="21.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="21.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w>................................</l>
				</lg>
			</div></body></text></TEI>