<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HYMNES, ROMANCES ET CHANSONNETTES</head><div type="poem" key="CAO58">
					<head type="main">BLANCHE, TE SOUVIENT-IL</head>
					<head type="tune">Musique de M. Édouard Vincelette</head>
					<div type="section" n="1">
						<head type="main">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">T<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w>-<w n="1.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ph<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="2.4">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
							<l n="3" num="1.3"><w n="3.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="3.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.3">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>si<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="3.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.6">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="3.9">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="4" num="1.4"><w n="4.1">C<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="4.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="4.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="4.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.10">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> ?</l>
							<l n="5" num="1.5"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="5.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="5.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.4">m</w>’<w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.7">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
							<l n="6" num="1.6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">n<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="6.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="6.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.7">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>.</l>
							<l n="7" num="1.7"><w n="7.1">Pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ‒ <w n="7.4">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ‒</l>
							<l n="8" num="1.8"><w n="8.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.5">pr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>…</l>
							<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w>-<w n="9.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> ?</l>
							<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w>-<w n="10.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> ?</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="main">II</head>
						<lg n="1">
							<l n="11" num="1.1"><w n="11.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.2">t</w>’<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.8">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="12" num="1.2"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="12.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> : « <w n="12.4">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">t</w>’<w n="12.7"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="12.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ;</l>
							<l n="13" num="1.3"><w n="13.1">J</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="13.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.4">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="14" num="1.4"><w n="14.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="14.2">t</w>’<w n="14.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="14.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.6">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="14.7">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="14.8">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! »</l>
							<l n="15" num="1.5"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.5">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="16" num="1.6"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.2">t</w>’<w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="16.7">br<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
							<l n="17" num="1.7"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">pr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="17.3">Di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="17.4">d</w>’<w n="17.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="17.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="18" num="1.8"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="18.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="18.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.6">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="18.7">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
							<l n="19" num="1.9"><space unit="char" quantity="8"></space><w n="19.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w>-<w n="19.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> ?</l>
							<l n="20" num="1.10"><space unit="char" quantity="8"></space><w n="20.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w>-<w n="20.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> ?</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="main">III</head>
						<lg n="1">
							<l n="21" num="1.1"><w n="21.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.2">m</w>’<w n="21.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="21.5">qu</w>’<w n="21.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="21.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.8">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="22" num="1.2"><w n="22.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="22.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="22.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="22.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="22.6">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>…</l>
							<l n="23" num="1.3"><w n="23.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="23.3">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.6"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="23.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="24" num="1.4"><w n="24.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="24.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="24.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="24.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.5">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="24.6">s</w>’<w n="24.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
							<l n="25" num="1.5"><w n="25.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="25.2">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="25.3">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="25.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="25.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.7">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="26" num="1.6"><w n="26.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="26.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="26.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="26.8">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ;</l>
							<l n="27" num="1.7"><w n="27.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="27.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="27.4">t</w>-<w n="27.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="27.6"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="27.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="27.8">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="27.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="28" num="1.8"><w n="28.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="28.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="28.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="28.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="28.6">t</w>-<w n="28.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="28.8">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="28.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.10">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> ?…</l>
							<l n="29" num="1.9"><space unit="char" quantity="8"></space><w n="29.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w>-<w n="29.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> ?</l>
							<l n="30" num="1.10"><space unit="char" quantity="8"></space><w n="30.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w>-<w n="30.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> ?</l>
						</lg>
					</div>
					<closer>
						<dateline>
							<date when="1883">Juin 1883</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>