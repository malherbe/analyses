<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HYMNES, ROMANCES ET CHANSONNETTES</head><div type="poem" key="CAO57">
					<head type="main">CHANT D’ADIEU</head>
					<head type="tune">Musique de M. N. Crépault</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="1.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="1.6">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.2">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.4">l</w>’<w n="2.5"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.7">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> ?</l>
						<l n="3" num="1.3"><w n="3.1">C</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="3.5">d</w>’<w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.7">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.9">l</w>’<w n="3.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.2">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="4.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.6">l</w>’<w n="4.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="4"></space><w n="5.1">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="5.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.6">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="2.2"><space unit="char" quantity="4"></space><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="6.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="6.3">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="6.5">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
						<l n="7" num="2.3"><space unit="char" quantity="4"></space><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><space unit="char" quantity="4"></space><w n="8.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="4"></space><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">d</w>’<w n="9.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.5">d</w>’<w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="4"></space><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="10.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.3">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w>,</l>
						<l n="11" num="3.3"><space unit="char" quantity="4"></space><w n="11.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="11.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="3.4"><space unit="char" quantity="4"></space><w n="12.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t</w> <w n="12.2">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="4"></space><w n="13.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="13.2">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="4.2"><space unit="char" quantity="4"></space><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="14.3">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="14.6">tr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> :</l>
						<l n="15" num="4.3"><space unit="char" quantity="4"></space>« <w n="15.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ssi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="4.4"><space unit="char" quantity="4"></space><w n="16.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">pi<seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="16.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><space unit="char" quantity="4"></space>« <w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.4">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.5">s<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="17.6">cl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="18" num="5.2"><space unit="char" quantity="4"></space><w n="18.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.5">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> :</l>
						<l n="19" num="5.3"><space unit="char" quantity="4"></space><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="19.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.4">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
						<l n="20" num="5.4"><space unit="char" quantity="4"></space><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="20.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">j</w>’<w n="20.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="20.6">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ! »</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><space unit="char" quantity="4"></space><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="21.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="22" num="6.2"><space unit="char" quantity="4"></space><w n="22.1">Lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="22.2">gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="22.4">l</w>’<w n="22.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.7">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> :</l>
						<l n="23" num="6.3"><space unit="char" quantity="4"></space>« <w n="23.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="23.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.4">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="4"></space><w n="24.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.4">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="24.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.6">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><space unit="char" quantity="4"></space>« <w n="25.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.5">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="26" num="7.2"><space unit="char" quantity="4"></space><w n="26.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w> <w n="26.2">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="26.4">fi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
						<l n="27" num="7.3"><space unit="char" quantity="4"></space><w n="27.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.2">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="27.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="28" num="7.4"><space unit="char" quantity="4"></space><w n="28.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ! »</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><space unit="char" quantity="4"></space><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="29.2">chr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w>, <w n="29.3">pr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="29.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="29.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="30" num="8.2"><space unit="char" quantity="4"></space><w n="30.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="30.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="30.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="30.6">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="31" num="8.3"><space unit="char" quantity="4"></space><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="31.2">cr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="31.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.5">pr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="32" num="8.4"><space unit="char" quantity="4"></space><w n="32.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="32.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.5">Di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="33.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="33.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="33.5">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="34" num="9.2"><w n="34.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.2">l</w>’<w n="34.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rgu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="34.6">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="34.7">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="35" num="9.3"><w n="35.1">J<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="35.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="35.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="35.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="35.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="35.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="35.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="36" num="9.4"><w n="36.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="36.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="36.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="36.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="36.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="36.6">ci<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1882">Novembre 1882</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>