<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO28">
					<head type="main">LE JOUR DE L’AN</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.6">l</w>’<w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2">‒ <w n="2.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>-<w n="2.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.4">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="2.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.6">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.7">b<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ?</l>
						<l n="3" num="1.3">‒ <w n="3.1">C</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.3">l</w>’<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="3.5">j<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.7">l</w>’<w n="3.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="3.9">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="4.2">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="4.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.7">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="5.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="5.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rg<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="6.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.6">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ;</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="7.2">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="7.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="7.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="8" num="2.4"><w n="8.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="8.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="8.3">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.6">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="9.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="10.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>…</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ‒ <w n="11.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="355">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ‒</l>
						<l n="12" num="3.4"><w n="12.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="12.2">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.7">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="12.8">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">N</w>’<w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="13.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> ! <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="13.7">l</w>’<w n="13.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="4.2"><w n="14.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="14.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.5">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.7">l</w>’<w n="14.8">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="15.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="15.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.7">h<seg phoneme="i" type="vs" value="1" rule="493">y</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.8">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="4.4"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ccl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.3">l</w>’<w n="16.4">h<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">V<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="17.2">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="17.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="17.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.6">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.7">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="5.2"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.6">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303">aim</seg></w> :</l>
						<l n="19" num="5.3"><w n="19.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="19.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w>, <w n="19.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="5.4"><w n="20.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="20.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.8">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="21.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="21.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rpr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.7">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="22" num="6.2"><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="22.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> :</l>
						<l n="23" num="6.3">« <w n="23.1">B<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="23.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="23.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="23.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="23.5">d</w>’<w n="23.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.7">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="23.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="23.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="24" num="6.4"><w n="24.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="24.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="24.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>, <w n="24.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="24.5">f<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="24.6">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="24.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> ! »</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="25.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="25.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="25.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.6">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="26" num="7.2"><w n="26.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="26.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="26.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.7">Di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ;</l>
						<l n="27" num="7.3"><w n="27.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="27.3">d</w>’<w n="27.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> ‒ <w n="27.5">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="27.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.7">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="27.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ‒</l>
						<l n="28" num="7.4"><w n="28.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.2">l</w>’<w n="28.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="28.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="28.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="28.8">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>…</l>
					</lg>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">D<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="29.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>, <w n="29.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="29.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="29.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="30" num="8.2"><w n="30.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="30.2">l</w>’<w n="30.3"><seg phoneme="e" type="vs" value="1" rule="170">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="30.4">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="30.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
						<l n="31" num="8.3"><w n="31.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="31.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="31.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="31.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="32.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="32.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="32.5">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="32.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1882">1<hi rend="sup">er</hi>de l’an 1882</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>