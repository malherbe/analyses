<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT178">
				<head type="number">III</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">l</w>’<subst reason="analysis" type="completion" hand="ML"><del>.....</del><add rend="hidden"><w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w></add></subst>, <w n="1.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w>, <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.8">gr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.10">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.11">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rb<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> !</l>
					<l n="3" num="1.3"><w n="3.1">F<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ! <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="3.4">L<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>, <w n="3.5">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>, <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>, <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.9">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="5.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.5">n</w>’<w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="5.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <subst reason="analysis" type="completion" hand="ML"><del>.....</del><add rend="hidden"><w n="5.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w></add></subst> <w n="5.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.10">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="6.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.6">mi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.8">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> !</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Encre, ancre.</note>
					</closer>
			</div></body></text></TEI>