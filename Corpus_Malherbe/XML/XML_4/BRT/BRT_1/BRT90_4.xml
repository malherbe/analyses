<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT90">
				<head type="number">XC</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="1.6">p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="2" num="1.2"><space unit="char" quantity="12"></space><w n="2.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="2.2">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.5">cr<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ?</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="3.2">m</w>’<w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="3.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.8">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.9">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.10">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1">C</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="4.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="4.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
					<l n="5" num="1.5"><w n="5.1">V<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> : <w n="5.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">n</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="5.7">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l</w>, <w n="5.8">L<seg phoneme="u" type="vs" value="1" rule="dc-2">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>t<seg phoneme="wa" type="vs" value="1" rule="421">oi</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="6" num="1.6"><space unit="char" quantity="12"></space>— <w n="6.1">J<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="6.2">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="6.3">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> !</l>
					<l n="7" num="1.7">— <w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="7.3">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>-<w n="7.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>sc<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.8">n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="8" num="1.8"><space unit="char" quantity="12"></space>— <w n="8.1">Ou<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="8.2">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>… <w n="8.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>… <w n="8.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>… <w n="8.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>…</l>
					<l n="9" num="1.9">— <w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">t</w>’<w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>. <w n="9.6">D</w>’<w n="9.7"><seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="9.8">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.9">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="10" num="1.10"><space unit="char" quantity="12"></space><w n="10.1">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="10.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="a" type="vs" value="1" rule="365">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
					<l n="11" num="1.11"><w n="11.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">d</w>’<w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="11.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.6">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ge<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="1.12"><space unit="char" quantity="12"></space><w n="12.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="12.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> !</l>
					<l n="13" num="1.13"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">c</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.4">h<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="13.5">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> ! <w n="13.6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.8">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="1.14"><space unit="char" quantity="12"></space><w n="14.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.2">l<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> ! …</l>
					<l n="15" num="1.15"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="15.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.4">r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="15.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ts</w>, <w n="15.9">L<seg phoneme="u" type="vs" value="1" rule="dc-2">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="16" num="1.16"><w n="16.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="16.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w>, <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="16.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="16.7">m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="16.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="16.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.10">si<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ On n’est jamais trahi que par les siens.</note>
					</closer>
			</div></body></text></TEI>