<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT76">
				<head type="number">LXXVI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="1.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.8">l</w>’<w n="1.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="2.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="3.3">t</w>-<w n="3.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ? <w n="3.6">C</w>’<w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.9">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.10">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">D</w>’<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>li<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="4.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rb<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>, <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.6">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> :</l>
					<l n="5" num="1.5"><w n="5.1">Ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> ! <w n="5.2">s</w>’<w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">l</w>’<w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.6">d</w>’<w n="5.7"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="5.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.9">J<seg phoneme="a" type="vs" value="1" rule="310">ea</seg>nn<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>-<w n="5.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="5.11">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="6.4">g<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="6.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="6.6">v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="6.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="6.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> !</l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="ɔ" type="vs" value="1" rule="439">O</seg>rn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.5">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w> <w n="7.8">d</w>’<w n="7.9"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.10"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>g<seg phoneme="ɥi" type="vs" value="1" rule="274">ui</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="8" num="1.8"><w n="8.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="8.4">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="y" type="vs" value="1" rule="450">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="8.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.6">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs</w>, <w n="8.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.9">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> !</l>
					<l n="9" num="1.9">— <w n="9.1">Mi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.2">v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="9.3">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="9.5">J<seg phoneme="ɑ̃" type="vs" value="1" rule="309">ean</seg></w>, <w n="9.6">l</w>’<w n="9.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>g<seg phoneme="ɥi" type="vs" value="1" rule="274">ui</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.10">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="10" num="1.10"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.3">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="10.7">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.8">n</w>’<w n="10.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.10">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="10.11">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> :</l>
					<l n="11" num="1.11"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.3">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="11.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.5">g<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> ! <w n="11.6">S</w>’<w n="11.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.8">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.9">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="11.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.11">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="1.12"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">br<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>, <w n="12.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="12.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.8">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="12.9">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.10">l</w>’<w n="12.11"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! »</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Un bon tiens vaut mieux que deux tu l’auras.</note>
					</closer>
			</div></body></text></TEI>