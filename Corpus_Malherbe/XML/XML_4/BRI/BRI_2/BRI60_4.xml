<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><head type="sub_part">EN BRETAGNE</head><div type="poem" key="BRI60">
					<head type="main">Les Goëlands</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">UN</seg></w> <w n="1.2">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ck</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="383">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.7">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w> <w n="1.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.9">N<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.5">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="2.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="2.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.9"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="3.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.4">r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>, <w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.10">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="4.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.7">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="4.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> :</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="10"></space>« <w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="5.6">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="6" num="2.2"><space unit="char" quantity="10"></space><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.5">Cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c</w></l>
						<l n="7" num="2.3"><space unit="char" quantity="10"></space><w n="7.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="7.2">s</w>’<w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="7.6">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ck</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="10"></space><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.2">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w>, <w n="8.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="8.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.6">c<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="9" num="2.5"><space unit="char" quantity="12"></space><w n="9.1">G<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414">ë</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w>, <w n="9.2">g<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414">ë</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w>,</l>
						<l n="10" num="2.6"><space unit="char" quantity="10"></space><w n="10.1">R<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="10.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> ! »</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">g<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="11.3">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="11.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>lli<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="11.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="12" num="3.2"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.5">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="12.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="12.10">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="13" num="3.3"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>, <w n="13.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>, <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
						<l n="14" num="3.4"><w n="14.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.3">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="15" num="4.1"><space unit="char" quantity="10"></space>« <w n="15.1">G<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414">ë</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w>, <w n="15.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="15.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w> <w n="15.4">d</w>’<w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="4.2"><space unit="char" quantity="10"></space><w n="16.1">Gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="16.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="16.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="16.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>,</l>
						<l n="17" num="4.3"><space unit="char" quantity="10"></space><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="17.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="17.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="17.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="17.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.6">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w></l>
						<l n="18" num="4.4"><space unit="char" quantity="10"></space><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.5">Br<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="19" num="4.5"><space unit="char" quantity="12"></space><w n="19.1">G<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414">ë</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w>, <w n="19.2">g<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414">ë</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w>,</l>
						<l n="20" num="4.6"><space unit="char" quantity="10"></space><w n="20.1">R<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="20.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> ! »</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1"><w n="21.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ck</w> <w n="21.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ! <w n="21.7">l</w>’<w n="21.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="21.10">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="22" num="5.2"><w n="22.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="22.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="22.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rsou<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="22.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="22.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="22.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="23" num="5.3"><w n="23.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.3"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="23.4">l</w>’<w n="23.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="23.6">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ; <w n="23.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="23.8">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
						<l n="24" num="5.4"><w n="24.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.2">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="24.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="24.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="24.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="24.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="24.8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="24.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.10">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					</lg>
					<lg n="6">
						<l n="25" num="6.1"><space unit="char" quantity="10"></space>« <w n="25.1">Su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="25.2">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="25.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="25.4">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="26" num="6.2"><space unit="char" quantity="10"></space><w n="26.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="26.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="26.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="26.5">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> !</l>
						<l n="27" num="6.3"><space unit="char" quantity="10"></space><w n="27.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.2">cr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="27.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="27.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="27.5">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w>,</l>
						<l n="28" num="6.4"><space unit="char" quantity="10"></space><w n="28.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="28.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="28.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="28.5">pl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="29" num="6.5"><space unit="char" quantity="12"></space><w n="29.1">G<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414">ë</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w>, <w n="29.2">g<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414">ë</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w>,</l>
						<l n="30" num="6.6"><space unit="char" quantity="10"></space><w n="30.1">R<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="30.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="30.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="30.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> ! »</l>
					</lg>
				</div></body></text></TEI>