<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><head type="sub_part">AU BORD DE LA MÉDITERRANÉE</head><div type="poem" key="BRI71">
					<head type="main">Consultation</head>
					<opener>
						<salute>AU DOCTEUR P***, DE MARSEILLE</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">H<seg phoneme="e" type="vs" value="1" rule="409">É</seg>L<seg phoneme="a" type="vs" value="1" rule="340">A</seg>S</w> ! <w n="1.2">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="1.3">l</w>’<w n="1.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.6">br<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="2.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="2.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="2.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="2.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="3" num="1.3"><space unit="char" quantity="12"></space><w n="3.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="4.2">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="4.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>. —</l>
						<l n="5" num="1.5"><space unit="char" quantity="12"></space><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="5.3">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>. —</l>
						<l n="6" num="1.6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="6.2">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="6.3">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.4">l</w>’<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="6.6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="7" num="1.7"><space unit="char" quantity="12"></space><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="7.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>. —</l>
						<l n="8" num="1.8"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="8.2">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="8.6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="8.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="8.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="9" num="1.9"><space unit="char" quantity="12"></space><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
						<l n="10" num="1.10"><w n="10.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="10.2">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="10.3">l</w>’<w n="10.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.6">br<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="11" num="1.11"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="11.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="11.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="11.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>