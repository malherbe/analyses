<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><head type="sub_part">A PARIS</head><div type="poem" key="BRI64">
					<head type="main">Tableau d’Intérieur</head>
					<opener>
						<salute>À MADAME MÉLANIE BIXIO</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.4">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="1.7">d</w>’<w n="1.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="1.9">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.10">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="2.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="2.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="2.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="2.7">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>li<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.4">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.8">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="4.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="4.3">ni<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="4.4">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="4.5">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.7">l</w>’<w n="4.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="4.9">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.10">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="5" num="1.5"><w n="5.1">S</w>’<w n="5.2"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="5.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="5.8">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="5.9">d</w>’<w n="5.10"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>g<seg phoneme="ɥi" type="vs" value="1" rule="274">ui</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="1.6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="6.2">l</w>’<w n="6.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="6.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="6.7">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="6.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.10">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.11">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="7" num="1.7"><w n="7.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="7.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="7.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.8">l</w>’<w n="7.9"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="8" num="1.8"><w n="8.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ; <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>h</w> ! <w n="8.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.8">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.9">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="9" num="1.9"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4">si<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="9.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="9.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.9"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="10" num="1.10"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">d</w>’<w n="10.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ct<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="10.8">L<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="10.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.10">L<seg phoneme="ɛ" type="vs" value="1" rule="323">ey</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="11" num="1.11"><w n="11.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="11.3">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w> <w n="11.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rv<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.5">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="13" num="1.13"><space unit="char" quantity="8"></space><w n="13.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.4">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rsi<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="14" num="1.14"><w n="14.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.2">bl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg><seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="14.4">s</w>’<w n="14.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.8">S<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="15" num="1.15"><space unit="char" quantity="8"></space><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="15.2">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="15.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> :</l>
						<l n="16" num="1.16"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="16.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="16.5">l</w>’<w n="16.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="16.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="16.8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="16.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.10">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="16.11">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>.</l>
					</lg>
					<closer>
						<placeName>Île Saint-Louis.</placeName>
					</closer>
				</div></body></text></TEI>