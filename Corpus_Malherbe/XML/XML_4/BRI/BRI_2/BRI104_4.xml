<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE CINQUIÈME</head><head type="sub_part">A ROME</head><div type="poem" key="BRI104">
					<head type="main">À la Maison d’Horace</head>
					<opener>
						<placeName>Tivoli.</placeName>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318">AU</seg>X</w> <w n="1.2">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="1.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.8">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.9"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vi<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="2.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="2.5">s</w>’<w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="2.8">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="3.2">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.4">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.3">r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.4">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="4.5">H<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rv<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="5.4">s<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.7">pi<seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="5.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="6.2">d</w>’<w n="6.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="493">y</seg>x</w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="wa" type="vs" value="1" rule="421">oi</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="7.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.6">ps<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.8">ch<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="8.3">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.6">m<seg phoneme="wa" type="vs" value="1" rule="421">oi</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="9.4">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="9.9">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w>,</l>
						<l n="10" num="3.2"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="10.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="10.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="10.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="10.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="11" num="3.3"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="11.2">c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="11.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="11.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="12.2">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>