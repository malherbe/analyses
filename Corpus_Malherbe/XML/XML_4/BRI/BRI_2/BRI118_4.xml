<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SIXIÈME</head><head type="sub_part">A NAPLE</head><div type="poem" key="BRI118">
					<head type="main">Lettre à Loïc</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318">AU</seg></w> <w n="1.2">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">K<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="1.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="1.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.9">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.10">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ppr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="2.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="2.5">qu</w>’<w n="2.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.7">t</w>’<w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.10">pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">H<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">cl<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc</w>, <w n="3.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="3.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="3.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.8">d</w>’<w n="3.9"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>di<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">l</w>’<w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="BRI118_1">e</seg>n</w> <w n="4.7">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.3">li<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="5.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.8">l</w>’<w n="5.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.10"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="i" type="vs" value="1" rule="493">y</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="6" num="2.2"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="6.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="6.8">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.10">l</w>’<w n="6.11"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.5">bru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.7">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.3">l</w>’<w n="8.4">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="8.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.8">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="8.9">d</w>’<w n="8.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="9" num="2.5"><w n="9.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="9.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="9.3">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ! <w n="9.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="9.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="10" num="2.6"><w n="10.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">fu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="10.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ;</l>
						<l n="11" num="2.7"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c</w> <w n="11.3">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> ; <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="11.8">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="11.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.11">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="2.8"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.5">p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>x</w> <w n="12.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.8"><seg phoneme="ø" type="vs" value="1" rule="247">œu</seg>fs</w> ;</l>
						<l n="13" num="2.9"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">t<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="13.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.4">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.9">b<seg phoneme="ø" type="vs" value="1" rule="247">œu</seg>fs</w></l>
						<l n="14" num="2.10"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="14.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>, <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="14.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="14.8">l</w>’<w n="14.9">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.10"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="15" num="3.1"><space unit="char" quantity="4"></space><w n="15.1">H<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.2">cl<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc</w>, <w n="15.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="15.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="15.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="15.8">d</w>’<w n="15.9"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
						<l n="16" num="3.2"><space unit="char" quantity="4"></space><w n="16.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>di<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">l</w>’<w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="BRI118_1">e</seg>n</w> <w n="16.7">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
						<l n="17" num="3.3"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="17.2">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">K<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="17.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="17.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="17.9">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="17.10">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="3.4"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ppr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="18.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="18.5">qu</w>’<w n="18.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.7">t</w>’<w n="18.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.10">pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<closer>
						<placeName>Près de Cumes</placeName>
					</closer>
				</div></body></text></TEI>