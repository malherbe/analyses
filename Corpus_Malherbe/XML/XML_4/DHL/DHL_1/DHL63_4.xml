<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="DHL63">
				<head type="main">RÉPONSE</head>
				<head type="sub">DE GRISETTE À TATA</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">O</seg>MM<seg phoneme="ɑ̃" type="vs" value="1" rule="369">EN</seg>T</w> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="1.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="2.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.3">c</w>’<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="3.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="4.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.8">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="445">û</seg>t</w> <w n="4.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="5.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> <w n="6.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ! <w n="6.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w></l>
					<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="7.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.5">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">F<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ! <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="8.4">d</w>’<w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.6">t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ;</l>
					<l n="9" num="1.9"><w n="9.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="9.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.3">j</w>’<w n="9.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="9.5">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="9.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.8">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w> <w n="10.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="10.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="11" num="1.11"><space unit="char" quantity="8"></space><w n="11.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="11.2">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="11.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
					<l n="12" num="1.12"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sgr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="12.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>.</l>
					<l n="13" num="1.13"><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="13.3">qu</w>’<w n="13.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.5">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.6">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="13.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="13.8">n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.10"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.11">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="1.14"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w>,</l>
					<l n="15" num="1.15"><w n="15.1">F<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="15.7">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.8"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="16" num="1.16"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="16.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="17" num="1.17"><w n="17.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="17.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w>.</l>
					<l n="18" num="1.18"><w n="18.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="18.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.8">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tti<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="19" num="1.19"><w n="19.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w>, <w n="19.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="19.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="19.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="19.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.7">n</w>’<w n="19.8"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="19.9">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
					<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.5">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.6">fi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="21" num="1.21"><w n="21.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.2">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="21.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="21.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="21.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w>, <w n="21.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="21.7">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.8">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="22" num="1.22"><w n="22.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="22.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="22.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>-<w n="22.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="22.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.8">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="22.9">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="22.10">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="23" num="1.23"><space unit="char" quantity="8"></space><w n="23.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="23.2">j</w>’<w n="23.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="23.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">qu</w>’<w n="23.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="23.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="25" num="1.25"><space unit="char" quantity="8"></space><w n="25.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="25.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="26" num="1.26"><space unit="char" quantity="8"></space><w n="26.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="26.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="26.4">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>,</l>
					<l n="27" num="1.27"><space unit="char" quantity="4"></space><w n="27.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="27.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="27.5">qu</w>’<w n="27.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.8">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="27.9"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="28" num="1.28"><space unit="char" quantity="4"></space><w n="28.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="28.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="28.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="28.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.7">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="29" num="1.29"><space unit="char" quantity="8"></space><w n="29.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="29.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="29.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="29.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w></l>
					<l n="30" num="1.30"><w n="30.1">L</w>’<w n="30.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="30.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="30.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="30.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.8">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="30.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.10">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="30.11">n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="31" num="1.31"><w n="31.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="31.2">m</w>’<w n="31.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="31.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="31.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="31.6">d</w>’<w n="31.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="31.8">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
					<l n="32" num="1.32"><w n="32.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="32.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.3">qu</w>’<w n="32.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.5">m</w>’<w n="32.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="32.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="32.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.9">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="32.10">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="32.11">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> ;</l>
					<l n="33" num="1.33"><space unit="char" quantity="8"></space><w n="33.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="33.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.3">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.4">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="34" num="1.34"><w n="34.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="34.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="34.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>, <w n="34.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="34.5">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="34.6">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="34.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="34.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="34.9">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ;</l>
					<l n="35" num="1.35"><w n="35.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="35.2">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="35.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="35.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="35.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="35.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="35.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="36" num="1.36"><space unit="char" quantity="4"></space><w n="36.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="36.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="36.3">n</w>’<w n="36.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="36.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="36.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="36.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> ;</l>
					<l n="37" num="1.37"><w n="37.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="37.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="37.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="37.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="37.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="37.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="37.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="37.8">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="37.9">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="38" num="1.38"><space unit="char" quantity="8"></space><w n="38.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="38.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.3">qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="38.4">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="38.5">j<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="38.6">m</w>’<w n="38.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ?</l>
					<l n="39" num="1.39"><space unit="char" quantity="8"></space><w n="39.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="39.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="39.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="39.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="39.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="40" num="1.40"><space unit="char" quantity="8"></space><w n="40.1">C</w>’<w n="40.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="40.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="40.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="40.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="40.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="40.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
				</lg>
			</div></body></text></TEI>