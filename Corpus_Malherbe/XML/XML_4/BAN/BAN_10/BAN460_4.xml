<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN460">
				<head type="main">L’Empereur</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>. <w n="1.5">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="1.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2"><seg phoneme="ø" type="vs" value="1" rule="405">Eu</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.6">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rpr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">gl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.7">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.4">r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="5.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="2.2"><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="6.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">J<seg phoneme="ɑ̃" type="vs" value="1" rule="309">ean</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="7.3">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="7.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="7.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.6">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="2.4"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">cu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">Tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>j<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="9.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="9.6">gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="10.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
					<l n="11" num="3.3"><w n="11.1">N</w>’<w n="11.2"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="11.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.5">qu</w>’<w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.7">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="3.4"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="13.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="4.2"><w n="14.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.2">qu</w>’<w n="14.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.5">d</w>’<w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="15.2">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="15.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="4.4"><w n="16.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.5">di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="16.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.7">Rh<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">L</w>’<w n="17.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="17.4">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="18" num="5.2"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="18.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="18.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w>, — <w n="18.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="18.6">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="18.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.8">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> ?</l>
					<l n="19" num="5.3"><w n="19.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="20" num="5.4"><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.2">r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="20.5">C<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> ? <w n="21.3">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> ! <w n="21.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="21.6">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="21.7">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lg<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="22" num="6.2"><w n="22.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="22.4"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="22.5">d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> ! <w n="22.6"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="22.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> !</l>
					<l n="23" num="6.3"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="23.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="24" num="6.4"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="24.2">gr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="24.4">n<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>f</w> <w n="24.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="25.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="25.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="26" num="7.2"><w n="26.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="26.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="26.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
					<l n="27" num="7.3"><w n="27.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="27.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="27.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.5">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="28" num="7.4"><w n="28.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="28.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.5">r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="29.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="29.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="29.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="30" num="8.2"><w n="30.1">Ge<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>li<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="30.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.3">l</w>’<w n="30.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.5">n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ;</l>
					<l n="31" num="8.3"><w n="31.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="31.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.4"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="31.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="31.6">t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="32" num="8.4"><w n="32.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="32.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rh<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="33.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="33.5">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="34" num="9.2"><w n="34.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.4">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="34.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="34.6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="34.7">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
					<l n="35" num="9.3"><w n="35.1">F<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="35.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="35.4">d</w>’<w n="35.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.6">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="36" num="9.4"><w n="36.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="36.2">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="36.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="36.4">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="36.5">d</w>’<w n="36.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="36.7">di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="37.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="37.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="37.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="37.5">l</w>’<w n="37.6"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="38" num="10.2"><w n="38.1">S<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="38.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="38.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.4">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="38.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="38.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.7">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
					<l n="39" num="10.3"><w n="39.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="39.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.5">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="40" num="10.4"><w n="40.1">C</w>’<w n="40.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="40.3">Lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="40.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="40.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="40.6">Lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="40.7">l</w>’<w n="40.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Janvier 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>