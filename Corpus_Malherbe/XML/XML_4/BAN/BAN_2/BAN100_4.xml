<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RONDEAUX</head><div type="poem" key="BAN100">
					<head type="main">MADAME KELLER</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="1.3">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="1.6">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="2.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="2.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="2.7">qu</w>’<w n="2.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.9">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.10"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="3" num="1.3"><w n="3.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="3.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="3.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.5">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w> <w n="3.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="4.2">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="4.3">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="4.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.6">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gt</w> <w n="4.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w></l>
						<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.8">tr<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><w n="6.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="7" num="2.2"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="7.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.4">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.7">l</w>’<w n="7.8">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>pp<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.3"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.3">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="8.4">fi<seg phoneme="ɛ" type="vs" value="1" rule="BAN100_2">e</seg>r</w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.7">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s</w> <w n="8.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>.</l>
						<l n="9" num="2.4"><space quantity="18" unit="char"></space><w n="9.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> !</l>
					</lg>
					<lg n="3">
						<l n="10" num="3.1"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w>, <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>th<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="11" num="3.2"><w n="11.1"><seg phoneme="y" type="vs" value="1" rule="251">Eû</seg>t</w> <w n="11.2">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.5">t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.6">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="3.3"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.3">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="12.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.8">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> ;</l>
						<l n="13" num="3.4"><w n="13.1">J<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="13.2">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="13.3">V<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="13.5">b<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="13.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>,</l>
						<l n="14" num="3.5"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.2">B<seg phoneme="e" type="vs" value="1" rule="353">e</seg>tt<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">c<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.6">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="15" num="3.6"><space quantity="18" unit="char"></space><w n="15.1">K<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="BAN100_1">e</seg>r</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1846">janvier 1846</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>