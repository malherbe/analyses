<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN129">
				<head type="main">MA BIOGRAPHIE <lb></lb>À HENRI D’IDEVILLE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w></l>
					<l n="2" num="1.2"><w n="2.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="2.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.5">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="3" num="1.3"><w n="3.1">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">d</w>’<w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.5">rh<seg phoneme="i" type="vs" value="1" rule="493">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.6">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.4">l<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w></l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="6.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="7.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="7.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="7.6">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w></l>
					<l n="8" num="2.4"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.2">c<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="8.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="8.6">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="9.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="9.5">H<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="10.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.7">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="11.5">m</w>’<w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
					<l n="12" num="3.4"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">ch<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">j<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">J</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="13.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="13.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
					<l n="14" num="4.2"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="14.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.7">m<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="15" num="4.3"><w n="15.1">J</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="15.3">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.5">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
					<l n="16" num="4.4"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.6">l</w>’<w n="16.7"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>, <w n="17.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="17.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="17.4">j</w>’<w n="17.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
					<l n="18" num="5.2"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="18.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.3">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.5">fi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="19" num="5.3"><w n="19.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="19.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w></l>
					<l n="20" num="5.4"><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">C</w>’<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="21.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="21.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="21.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="21.7">d</w>’<w n="21.8">H<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="22" num="6.2"><w n="22.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">j</w>’<w n="22.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="22.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="22.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="22.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="23" num="6.3"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.3">n</w>’<w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="23.5">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="23.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="23.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="24" num="6.4"><w n="24.1">Qu</w>’<w n="24.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.3">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s</w> <w n="24.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="24.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="24.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.7"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="24.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="25.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="25.3">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="25.4">tr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
					<l n="26" num="7.2"><w n="26.1">J</w>’<w n="26.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="26.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="26.5">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="27" num="7.3"><w n="27.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="27.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.4">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="27.5">d</w>’<w n="27.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
					<l n="28" num="7.4"><w n="28.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.2">d<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.5">t<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">J</w>’<w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="29.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="29.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.5">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="29.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w></l>
					<l n="30" num="8.2"><w n="30.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="30.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="30.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="31" num="8.3"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="31.2">j</w>’<w n="31.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="31.4">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="31.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.6">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="31.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w></l>
					<l n="32" num="8.4"><w n="32.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="32.2">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="32.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="32.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>, <w n="33.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="33.4">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="34" num="9.2"><w n="34.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="34.2">j</w>’<w n="34.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="34.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.5">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="35" num="9.3"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="35.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="35.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="35.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="35.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="35.6">di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					<l n="36" num="9.4"><w n="36.1">J</w>’<w n="36.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="36.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="36.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="36.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rpr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="36.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="37.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="37.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="37.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="37.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="38" num="10.2"><w n="38.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="38.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="38.3">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="38.4">m</w>’<w n="38.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="38.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="38.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="38.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="38.9">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="39" num="10.3"><w n="39.1">J</w>’<w n="39.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="39.3">t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="39.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="39.5">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="39.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="39.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w></l>
					<l n="40" num="10.4"><w n="40.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.2">gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="40.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="40.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="40.6">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="41.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="41.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="41.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="41.5">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="41.6">v<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="42" num="11.2"><w n="42.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="42.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="42.3">d</w>’<w n="42.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="42.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="42.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
					<l n="43" num="11.3"><w n="43.1">P<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="43.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="43.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>-<w n="43.4">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="43.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="43.6">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="44" num="11.4"><w n="44.1">Tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="44.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="44.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="44.4">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="44.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="44.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="44.7">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Qu</w>’<w n="45.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="45.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="45.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="45.5">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="45.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="45.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="45.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="46.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="46.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="46.4">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="46.5">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="47" num="12.3"><w n="47.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="47.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="47.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="47.4">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="47.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="47.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
					<l n="48" num="12.4"><w n="48.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="48.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="48.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="48.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="48.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="48.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1858">juin 1858</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>