<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN510">
					<head type="number">XXXVI</head>
					<head type="main">Ballade au lecteur, pour finir</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">G<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="1.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.7">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="2.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="3.3">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.4">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="3.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.9">Di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w></l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="4.2">fl<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.3">d</w>’<w n="4.4"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="4.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="4.9">n</w>’<w n="4.10"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="5" num="1.5"><w n="5.1">F<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="5.4">j</w>’<w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.6">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="1.6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">c</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="6.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.8">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
						<l n="7" num="1.7"><w n="7.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="7.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">n</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.8">d</w>’<w n="7.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.10">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="7.11">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
						<l n="8" num="1.8"><w n="8.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.8">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="9.9">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
						<l n="10" num="1.10"><w n="10.1">J</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="10.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="10.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="10.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><w n="11.1">D</w>’<w n="11.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="11.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.7">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="12" num="2.2"><w n="12.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="12.6">s</w>’<w n="12.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.8">l</w>’<w n="12.9"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="13" num="2.3"><w n="13.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg>t</w> <w n="13.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="13.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.8">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="14" num="2.4"><w n="14.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="14.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="15" num="2.5"><w n="15.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.2">d<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="15.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="16" num="2.6"><w n="16.1">Cl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="16.2">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>, <w n="16.3">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="16.4">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.5">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
						<l n="17" num="2.7"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="17.2">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="17.3">s</w>’<w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="17.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ;</l>
						<l n="18" num="2.8"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="18.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="18.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.6">fi<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="18.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="19" num="2.9"><w n="19.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="19.2">qu</w>’<w n="19.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.4">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="19.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.6">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.8">S<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="i" type="vs" value="1" rule="493">y</seg></w>,</l>
						<l n="20" num="2.10"><w n="20.1">J</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="20.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="20.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="20.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="20.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><w n="21.1">F<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>, <w n="21.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.3">j</w>’<w n="21.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="21.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="21.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="21.7">v<seg phoneme="ø" type="vs" value="1" rule="248">œu</seg></w>,</l>
						<l n="22" num="3.2"><w n="22.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="22.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="22.4">j</w>’<w n="22.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="22.6">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="22.7">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="22.8"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="23" num="3.3"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="23.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.3">m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="23.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="23.8">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="24" num="3.4"><w n="24.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="24.2">l<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w> <w n="24.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="24.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.5">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="24.6">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="25" num="3.5"><w n="25.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="25.2">l</w>’<w n="25.3">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="25.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="25.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.8">l</w>’<w n="25.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="26" num="3.6"><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="26.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="26.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="26.4">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="26.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="26.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.7">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="26.8"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
						<l n="27" num="3.7"><w n="27.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.2">r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="27.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="27.4">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
						<l n="28" num="3.8"><w n="28.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.2">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ck</w> <w n="28.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="28.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.6">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="29" num="3.9"><w n="29.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="29.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="29.3">l</w>’<w n="29.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>, <w n="29.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="29.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="29.8">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
						<l n="30" num="3.10"><w n="30.1">J</w>’<w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="30.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="30.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="30.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="30.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<head type="form">Envoi</head>
						<l n="31" num="4.1"><w n="31.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="31.2">l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="31.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="31.4">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="31.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="31.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="31.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
						<l n="32" num="4.2"><w n="32.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="32.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> : <w n="32.5"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="32.6">n</w>’<w n="32.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="32.9">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="y" type="vs" value="1" rule="450">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
						<l n="33" num="4.3"><w n="33.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="33.3">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="33.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="33.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="33.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="34" num="4.4"><w n="34.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> ; <w n="34.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.5">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.6">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> : <w n="34.7">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
						<l n="35" num="4.5"><w n="35.1">J</w>’<w n="35.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="35.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="35.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="35.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="35.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="35.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1869">Octobre 1869.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>