<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">AMÉTHYSTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>319 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">AMÉTHYSTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleamethystes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1860-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN188">
				<head type="main">Printemps d’Avril</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.5">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="1.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.2">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="2.3">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="3" num="1.3"><w n="3.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="3.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>.</l>
					<l n="4" num="1.4"><w n="4.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="5" num="1.5"><w n="5.1">S</w>’<w n="5.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.4">s<seg phoneme="i" type="vs" value="1" rule="493">y</seg>lph<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="6.5">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
					<l n="7" num="1.7"><w n="7.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="7.4">d</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> !</l>
				</lg>
				<lg n="2">
					<l n="8" num="2.1"><w n="8.1">C</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="8.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.7">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="9" num="2.2"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="9.3">c<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="10" num="2.3"><w n="10.1">Vi<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="10.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="10.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w>,</l>
					<l n="11" num="2.4"><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="11.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="12" num="2.5"><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="12.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="12.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>,</l>
					<l n="13" num="2.6"><w n="13.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.4">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="14" num="2.7"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="14.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>.</l>
				</lg>
				<lg n="3">
					<l n="15" num="3.1"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.4">l</w>’<w n="15.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="3.2"><w n="16.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="16.2">l</w>’<w n="16.3">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="16.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="16.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="16.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="17" num="3.3"><w n="17.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="17.2">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">n<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="17.5">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>.</l>
					<l n="18" num="3.4"><w n="18.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.5">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="19" num="3.5"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="19.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="19.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.4">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="19.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> ;</l>
					<l n="20" num="3.6"><w n="20.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="20.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="20.5">r<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="21" num="3.7"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="21.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1861">Avril 1860.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>