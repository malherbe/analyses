<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN199">
				<head type="number">VII</head>
				<head type="main">Monsieur Alexandre</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.3">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w> <w n="1.6">d</w>’<w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>,</l>
					<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.3">m<seg phoneme="œ" type="vs" value="1" rule="151">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>r</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="355">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="3" num="1.3"><w n="3.1">F<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.3">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.3">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gt</w> <w n="4.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.7">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="5.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="5.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6">l<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w></l>
					<l n="6" num="2.2"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.3">d</w>’<w n="6.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="6.6">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="7" num="2.3"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.4">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> ;</l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="8.2">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="8.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="8.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.6">P<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
					<l n="10" num="3.2"><w n="10.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="10.3">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.6"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="10.7">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.7">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>,</l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.6">s<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.7">fou<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> !</l>
					<l n="14" num="4.2"><w n="14.1">C</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.5">gr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.6">qu</w>’<w n="14.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.8">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.9">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="15" num="4.3"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.4">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="15.5">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="16" num="4.4"><w n="16.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="16.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="16.4">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="16.5">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="16.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="16.7">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">1er décembre 1883.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>