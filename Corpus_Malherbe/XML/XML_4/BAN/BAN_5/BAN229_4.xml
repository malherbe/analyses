<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN229">
				<head type="number">XXXVII</head>
				<head type="main">La Liseuse</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="2.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="3.4">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w></l>
					<l n="4" num="1.4"><space quantity="4" unit="char"></space><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.2">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="6.2">j<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="6.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.3">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.5">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					<l n="8" num="2.4"><space quantity="4" unit="char"></space><w n="8.1">M<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="9.2">c<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.3">d</w>’<w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w></l>
					<l n="12" num="3.4"><space quantity="4" unit="char"></space><w n="12.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.2">d</w>’<w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="13.3">qu</w>’<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.5">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="4.2"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="14.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.5">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="15" num="4.3"><w n="15.1">R<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="15.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w></l>
					<l n="16" num="4.4"><space quantity="4" unit="char"></space><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="16.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="16.3">d<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">l</w>’<w n="17.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="18" num="5.2"><w n="18.1">R<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.3">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.4">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">s</w>’<w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w></l>
					<l n="20" num="5.4"><space quantity="4" unit="char"></space><w n="20.1">V<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="20.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.3">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="21.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="21.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.5">l</w>’<w n="21.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="22" num="6.2"><w n="22.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.2">L<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="23" num="6.3"><w n="23.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="23.3">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w></l>
					<l n="24" num="6.4"><space quantity="4" unit="char"></space><w n="24.1">L</w>’<w n="24.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="24.3">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.2">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="25.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.4">m<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="26" num="7.2"><w n="26.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="26.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">l</w>’<w n="26.4">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="27" num="7.3"><w n="27.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="27.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.4">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w></l>
					<l n="28" num="7.4"><space quantity="4" unit="char"></space><w n="28.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="28.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="28.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>. <w n="28.4">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="29.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="29.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w>, <w n="29.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.5">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="30" num="8.2"><w n="30.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="31" num="8.3"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="31.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="31.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="31.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.5">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="32" num="8.4"><space quantity="4" unit="char"></space><w n="32.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="32.3">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="33.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="33.3">l</w>’<w n="33.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="33.5">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="34" num="9.2"><w n="34.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="34.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="35" num="9.3"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="35.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="35.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>,</l>
					<l n="36" num="9.4"><space quantity="4" unit="char"></space><w n="36.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="36.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="36.4">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> ?</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">C</w>’<w n="37.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="37.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="37.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="37.5">Str<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ph<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="37.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="38" num="10.2"><w n="38.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="38.2">s</w>’<w n="38.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="38.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="38.5">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="39" num="10.3"><w n="39.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="39.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="39.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="39.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w></l>
					<l n="40" num="10.4"><space quantity="4" unit="char"></space><w n="40.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="40.2">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="40.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w>,</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="41.2">s</w>’<w n="41.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="41.4">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="42" num="11.2"><w n="42.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="42.2">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="42.3">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="42.4">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="43" num="11.3"><w n="43.1">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="43.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="43.3">l</w>’<w n="43.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> :</l>
					<l n="44" num="11.4"><space quantity="4" unit="char"></space><w n="44.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> ! <w n="44.2"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="45.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="45.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="45.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="46" num="12.2"><w n="46.1">Tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="46.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="46.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="46.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="47" num="12.3"><w n="47.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="47.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="47.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="47.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w></l>
					<l n="48" num="12.4"><space quantity="4" unit="char"></space><w n="48.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="48.2">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> ;</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="49.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>, <w n="49.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="49.4">d</w>’<w n="49.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="50" num="13.2"><w n="50.1">Tr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="50.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="50.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="50.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="51" num="13.3"><w n="51.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="51.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="51.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="51.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="51.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> ;</l>
					<l n="52" num="13.4"><space quantity="4" unit="char"></space><w n="52.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="52.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="52.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="52.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w></l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">S</w>’<w n="53.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="53.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="53.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="53.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="54" num="14.2"><w n="54.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="54.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="54.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="54.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="55" num="14.3"><w n="55.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="55.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="55.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="55.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="55.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w></l>
					<l n="56" num="14.4"><space quantity="4" unit="char"></space><w n="56.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="56.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="56.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>,</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="57.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="57.3">b<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="58" num="15.2"><w n="58.1">Qu</w>’<w n="58.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w>-<w n="58.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="58.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="58.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="58.6">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="59" num="15.3"><w n="59.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="59.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="59.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="59.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="59.5">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w></l>
					<l n="60" num="15.4"><space quantity="4" unit="char"></space><w n="60.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="60.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="60.3"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">4 janvier 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>