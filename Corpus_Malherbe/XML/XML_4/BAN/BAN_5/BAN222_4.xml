<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN222">
				<head type="number">XXX</head>
				<head type="main">Comédie Française</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">P<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w></l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">s</w>’<w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.6">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rt</w>, <w n="3.2">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="3.4">fi<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="3.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gn<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.5">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">V<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
					<l n="6" num="2.2"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="6.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="6.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="6.6"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="6.7">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.3">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="8.3">j<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.4">C<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="9.3">n</w>’<w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.6">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="9.7">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="10.3">r<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="10.4">fi<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="11.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="11.4">l</w>’<w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="11.6">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w>, <w n="13.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.3">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs<seg phoneme="i" type="vs" value="1" rule="493">y</seg></w></l>
					<l n="14" num="4.2"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>. <w n="14.4">C</w>’<w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.7">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="15.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>, <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.6">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs</w> <w n="15.7"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w></l>
					<l n="16" num="4.4"><w n="16.1">F<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="16.3">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="17.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="17.5">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="17.6">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>,</l>
					<l n="18" num="5.2"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="18.2">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs</w>, <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>n<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="476">ï</seg>s</w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.5">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="19" num="5.3"><w n="19.1">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="19.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.5">d</w>’<w n="19.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="19.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gt</w> <w n="19.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w></l>
					<l n="20" num="5.4"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="20.2">l</w>’<w n="20.3"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="20.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="20.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.7">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.8">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">28 décembre 1883.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>