<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN753">
				<head type="main">Pèlerines</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.3">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="1.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="1.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w></l>
					<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">G<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w></l>
					<l n="5" num="1.5"><space quantity="8" unit="char"></space><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="5.2">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w></l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rb<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.3">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Sv<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.3">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="7.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
					<l n="8" num="2.2"><space quantity="8" unit="char"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
					<l n="9" num="2.3"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="9.5">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="10" num="2.4"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="10.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="10.3">f<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg>y<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="10.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ?</l>
					<l n="11" num="2.5"><space quantity="8" unit="char"></space><w n="11.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="11.2">d</w>’<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="12" num="2.6"><w n="12.1">P<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="12.2">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
					<l n="14" num="3.2"><space quantity="8" unit="char"></space><w n="14.1">V<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
					<l n="15" num="3.3"><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">m<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="16" num="3.4"><w n="16.1">L<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="16.2">sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="16.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="16.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w> <w n="16.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="17" num="3.5"><space quantity="8" unit="char"></space><w n="17.1">C</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.3">W<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w></l>
					<l n="18" num="3.6"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.2">l</w>’<w n="18.3"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="18.5">C<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.2"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="19.3">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="19.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="19.5">d</w>’<w n="19.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="20" num="4.2"><space quantity="8" unit="char"></space><w n="20.1">Z<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>z<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="21" num="4.3"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="21.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="21.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="21.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.5">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="22" num="4.4"><w n="22.1">V<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="22.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="22.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w></l>
					<l n="23" num="4.5"><space quantity="8" unit="char"></space><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="23.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w>,</l>
					<l n="24" num="4.6"><w n="24.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="24.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="24.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rti<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="24.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.5">C<seg phoneme="i" type="vs" value="1" rule="493">y</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.2">n<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w>, <w n="25.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="25.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="25.5">n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
					<l n="26" num="5.2"><space quantity="8" unit="char"></space><w n="26.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
					<l n="27" num="5.3"><w n="27.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="28" num="5.4"><w n="28.1">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="28.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="28.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.4"><seg phoneme="i" type="vs" value="1" rule="468">î</seg>l<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>,</l>
					<l n="29" num="5.5"><space quantity="8" unit="char"></space><w n="29.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="29.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.3">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>,</l>
					<l n="30" num="5.6"><w n="30.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.2">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.3">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pt<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="31.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="31.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> !</l>
					<l n="32" num="6.2"><space quantity="8" unit="char"></space><w n="32.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="32.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>vi<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w></l>
					<l n="33" num="6.3"><w n="33.1">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.2">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="33.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="33.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="34" num="6.4"><w n="34.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="34.2">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="e" type="vs" value="1" rule="353">e</seg>tti<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="34.3">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="34.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
					<l n="35" num="6.5"><space quantity="8" unit="char"></space><w n="35.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="35.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
					<l n="36" num="6.6"><w n="36.1">T<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="36.2">M<seg phoneme="i" type="vs" value="1" rule="493">y</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="36.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="36.4">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg>lv<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="37.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ! <w n="37.3">Su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="37.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
					<l n="38" num="7.2"><space quantity="8" unit="char"></space><w n="38.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="39" num="7.3"><w n="39.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="39.3">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="39.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="40" num="7.4"><w n="40.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="40.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="40.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="40.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w></l>
					<l n="41" num="7.5"><space quantity="8" unit="char"></space><w n="41.1">C</w>’<w n="41.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="41.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="41.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
					<l n="42" num="7.6"><w n="42.1">D</w>’<w n="42.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="42.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="42.4">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="43.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="43.3">s</w>’<w n="43.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="43.5">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
					<l n="44" num="8.2"><space quantity="8" unit="char"></space><w n="44.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="44.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
					<l n="45" num="8.3"><w n="45.1">J<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="45.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="45.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="45.4">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="46" num="8.4"><w n="46.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="46.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="46.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="46.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="46.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="46.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="46.7">pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
					<l n="47" num="8.5"><space quantity="8" unit="char"></space><w n="47.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
					<l n="48" num="8.6"><w n="48.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="48.2">dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="48.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="48.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="48.5">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1"><w n="49.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="49.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="49.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="49.4">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="49.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="49.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
					<l n="50" num="9.2"><space quantity="8" unit="char"></space><w n="50.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="50.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="50.3">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w></l>
					<l n="51" num="9.3"><w n="51.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w> <w n="51.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="51.3">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="52" num="9.4"><w n="52.1">J</w>’<w n="52.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xpl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="52.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="52.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ts</w> <w n="52.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>s</w>,</l>
					<l n="53" num="9.5"><space quantity="8" unit="char"></space><w n="53.1"><seg phoneme="e" type="vs" value="1" rule="354">E</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>s</w>,</l>
					<l n="54" num="9.6"><w n="54.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="54.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="54.3">l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="54.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="54.5">l</w>’<w n="54.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1"><w n="55.1">C</w>’<w n="55.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="55.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="55.4">qu</w>’<w n="55.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="55.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
					<l n="56" num="10.2"><space quantity="8" unit="char"></space><w n="56.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w>,</l>
					<l n="57" num="10.3"><w n="57.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="57.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="57.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="57.4">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="57.5"><seg phoneme="ɛ" type="vs" value="1" rule="410">È</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="58" num="10.4"><w n="58.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="58.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="58.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="58.4">fl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
					<l n="59" num="10.5"><space quantity="8" unit="char"></space><w n="59.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="59.2">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
					<l n="60" num="10.6"><w n="60.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="60.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="60.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="60.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="60.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="11">
					<l n="61" num="11.1"><w n="61.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="61.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="61.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="61.4">dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ps</w>, <w n="61.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="61.6">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="62" num="11.2"><space quantity="8" unit="char"></space><w n="62.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>lt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="63" num="11.3"><w n="63.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="63.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="63.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="63.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="64" num="11.4"><w n="64.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="64.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="64.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="64.4">l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
					<l n="65" num="11.5"><space quantity="8" unit="char"></space><w n="65.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="65.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
					<l n="66" num="11.6"><w n="66.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="66.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="66.3">l</w>’<w n="66.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="66.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="66.6">pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="12">
					<l n="67" num="12.1"><w n="67.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="67.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="67.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="67.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="67.5">d</w>’<w n="67.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="68" num="12.2"><space quantity="8" unit="char"></space><w n="68.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pp<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="69" num="12.3"><w n="69.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="69.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="69.3">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rp<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="70" num="12.4"><w n="70.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="70.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="70.3">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="70.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
					<l n="71" num="12.5"><space quantity="8" unit="char"></space><w n="71.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="71.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>si<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w></l>
					<l n="72" num="12.6"><w n="72.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="72.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="72.3">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="72.4">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1890"> 24 juin 1890.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>