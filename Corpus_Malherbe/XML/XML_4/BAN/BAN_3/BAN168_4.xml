<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN168">
				<head type="main">LES FORGERONS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Rh<seg phoneme="i" type="vs" value="1" rule="493">y</seg>thm<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="1.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.3">j<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
					<l n="3" num="1.3"><w n="3.1">S</w>’<w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="3.5">bru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="3.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="3.7">l</w>’<w n="3.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.2">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-7">e</seg>r</w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="4.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.7">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<head type="main">Jean et Jacques.</head>
					<l n="5" num="2.1"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="6.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="7.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">br<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="2.4"><w n="8.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rb<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<head type="main"></head>
					<l n="9" num="3.1"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">n<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="175">ë</seg>l</w> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="10.4">c</w>’<w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>.</l>
				</lg>
				<lg n="4">
					<head type="main">Jacques.</head>
					<l part="I" n="11" num="4.1"><w n="11.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">J<seg phoneme="ɑ̃" type="vs" value="1" rule="309">ean</seg></w>, </l>
				</lg>
				<lg n="5">
					<head type="main">Jean.</head>
					<l part="F" n="11"><w n="11.4">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.6">J<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cqu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
				</lg>
				<lg n="6">
					<head type="main">Jacques.</head>
					<l part="I" n="12" num="6.1"><w n="12.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ! </l>
				</lg>
				<lg n="7">
					<head type="main">Jean.</head>
					<l part="F" n="12"><w n="12.4">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="12.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> !</l>
				</lg>
				<lg n="8">
					<head type="main">Jacques.</head>
					<l n="13" num="8.1"><w n="13.1">F<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="13.2">gr<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ssi<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="14" num="8.2"><w n="14.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="14.6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="15" num="8.3"><w n="15.1">J<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="15.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="8.4"><w n="16.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.3">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="16.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
				</lg>
				<lg n="9">
					<head type="main">Jean.</head>
					<l n="17" num="9.1"><w n="17.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="17.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="17.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.4">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rph<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="18" num="9.2"><w n="18.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="18.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>, <w n="18.4"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="18.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
					<l n="19" num="9.3"><w n="19.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.3">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="20" num="9.4"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="20.2">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="20.3">d</w>’<w n="20.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.5">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.6">d</w>’<w n="20.7"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> !</l>
				</lg>
				<lg n="10">
					<head type="main">Jacques.</head>
					<l n="21" num="10.1"><w n="21.1">Pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="21.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="21.4">l</w>’<w n="21.5"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="22" num="10.2"><w n="22.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="22.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="22.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="22.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
					<l n="23" num="10.3"><w n="23.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.2">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.3">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="23.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="24" num="10.4"><w n="24.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">ch<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="24.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
				</lg>
				<lg n="11">
					<head type="main">Jean.</head>
					<l n="25" num="11.1"><w n="25.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="25.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="25.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rsi<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="25.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.6">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="26" num="11.2"><w n="26.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rsi<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="26.6">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
					<l n="27" num="11.3"><w n="27.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="27.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="27.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="27.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="28" num="11.4"><w n="28.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rb<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
				</lg>
				<lg n="12">
					<head type="main">Jacques.</head>
					<l n="29" num="12.1"><w n="29.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="29.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="29.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.4">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="29.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="29.6">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="30" num="12.2"><w n="30.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="30.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="30.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.4">s<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.5">m<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r</w>,</l>
					<l n="31" num="12.3"><w n="31.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="31.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="31.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="31.5">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="32" num="12.4"><w n="32.1">L</w>’<w n="32.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="32.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.5">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="32.6">d</w>’<w n="32.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>.</l>
				</lg>
				<lg n="13">
					<head type="main">Jean.</head>
					<l n="33" num="13.1"><w n="33.1">L<seg phoneme="y" type="vs" value="1" rule="453">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="33.2">d</w>’<w n="33.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="o" type="vs" value="1" rule="435">o</seg>pp<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="34" num="13.2"><w n="34.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="34.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="34.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="34.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="34.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> ;</l>
					<l n="35" num="13.3"><w n="35.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="35.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="35.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.4">f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="35.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.6">l</w>’<w n="35.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="36" num="13.4"><w n="36.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="36.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="36.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="36.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>.</l>
				</lg>
				<lg n="14">
					<head type="main">Jacques.</head>
					<l n="37" num="14.1"><w n="37.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="37.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="37.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="37.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="37.5">s</w>’<w n="37.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="38" num="14.2"><w n="38.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="38.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="38.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rg<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="38.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="38.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.6">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="39" num="14.3"><w n="39.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="39.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="39.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="39.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="39.6">gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="40" num="14.4"><w n="40.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="40.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="40.3">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="40.4">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
				</lg>
				<lg n="15">
					<head type="main">Jean.</head>
					<l n="41" num="15.1"><w n="41.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="41.2">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="41.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="41.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="41.5">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="16">
					<head type="main">Jacques.</head>
					<l n="42" num="16.1"><w n="42.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="42.2">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="42.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="42.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="43" num="16.2"><w n="43.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="43.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="43.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="43.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="43.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="43.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="17">
					<head type="main">Jean.</head>
					<l part="I" n="44" num="17.1"><w n="44.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="44.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="44.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> </l>
				</lg>
				<lg n="18">
					<head type="main">Jacques.</head>
					<l part="F" n="44"><w n="44.4"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="44.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1859">octobre 1859</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>