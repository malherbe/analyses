<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN398">
				<head type="main">Élégie</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Gallus et Hesperiis, et Gallus notus Eoïs <lb></lb>
								Et sua cum Gallo nota Lycoris erit.
							</quote>
							<bibl>
								<name>Ovide</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="1.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="1.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
					<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.2">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.4">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="3.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="3.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="3.4">d</w>’<w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.8">r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>,</l>
					<l n="4" num="2.2"><space quantity="8" unit="char"></space><w n="4.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
				</lg>
				<lg n="3">
					<l n="5" num="3.1"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="5.3">d</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="5.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="5.7">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
					<l n="6" num="3.2"><space quantity="8" unit="char"></space><w n="6.1">B<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="6.2">ru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>, <w n="6.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
				</lg>
				<lg n="4">
					<l n="7" num="4.1"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="7.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> ! <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
					<l n="8" num="4.2"><space quantity="8" unit="char"></space><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">m</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="9" num="5.1"><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="9.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> ! <w n="9.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.8">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.9">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="9.10">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w>,</l>
					<l n="10" num="5.2"><space quantity="8" unit="char"></space><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="10.2">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
				</lg>
				<lg n="6">
					<l n="11" num="6.1"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="11.3">s</w>’<w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="11.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.8">d</w>’<w n="11.9"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
					<l n="12" num="6.2"><space quantity="8" unit="char"></space><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="12.2">z<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r</w> <w n="12.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="7">
					<l n="13" num="7.1"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>di<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="13.7">c<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w></l>
					<l n="14" num="7.2"><space quantity="8" unit="char"></space><w n="14.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="14.4">qu</w>’<w n="14.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="14.6">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="8">
					<l n="15" num="8.1"><w n="15.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="15.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.3">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="15.4">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="15.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.6">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="15.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>,</l>
					<l n="16" num="8.2"><space quantity="8" unit="char"></space><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="16.5">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="9">
					<l n="17" num="9.1"><w n="17.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.4">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="17.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="18" num="9.2"><space quantity="8" unit="char"></space><w n="18.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="18.2">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="18.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.4">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="10">
					<l n="19" num="10.1"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w>-<w n="19.4">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="19.5">s</w>’<w n="19.6"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="19.9">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
					<l n="20" num="10.2"><space quantity="8" unit="char"></space><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.2">v<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="20.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.6">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="11">
					<l n="21" num="11.1"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="21.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.3">m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="21.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="21.5">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="21.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.7">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="21.8">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>,</l>
					<l n="22" num="11.2"><space quantity="8" unit="char"></space><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="22.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="22.4">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="12">
					<l n="23" num="12.1"><w n="23.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="23.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="23.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="23.6">r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> !</l>
					<l n="24" num="12.2"><space quantity="8" unit="char"></space><w n="24.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="24.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="24.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="13">
					<l n="25" num="13.1"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="25.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="25.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="25.5">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w>, <w n="25.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="25.7">c<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="25.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="25.9">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s</w>,</l>
					<l n="26" num="13.2"><space quantity="8" unit="char"></space><w n="26.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="26.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="14">
					<l n="27" num="14.1"><w n="27.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.2">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="27.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="27.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="27.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="27.8">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
					<l n="28" num="14.2"><space quantity="8" unit="char"></space><w n="28.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="28.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.4">qu</w>’<w n="28.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="28.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ffl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="15">
					<l n="29" num="15.1"><w n="29.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="29.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.3">d</w>’<w n="29.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="29.5">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="29.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="29.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> !</l>
					<l n="30" num="15.2"><space quantity="8" unit="char"></space><w n="30.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>-<w n="30.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="30.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="30.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
				</lg>
				<lg n="16">
					<l n="31" num="16.1"><w n="31.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="31.2">t</w>’<w n="31.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-42">e</seg></w>, <w n="31.4">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="31.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="31.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
					<l n="32" num="16.2"><space quantity="8" unit="char"></space><w n="32.1">Br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="32.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="32.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="32.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
				</lg>
				<lg n="17">
					<l n="33" num="17.1"><w n="33.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="33.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.3">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="33.4">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="33.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="33.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.7">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="33.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs</w></l>
					<l n="34" num="17.2"><space quantity="8" unit="char"></space><w n="34.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="34.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="34.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="34.4">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="18">
					<l n="35" num="18.1"><w n="35.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="35.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="35.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>,</l>
					<l n="36" num="18.2"><space quantity="8" unit="char"></space><w n="36.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="36.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="36.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="36.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="19">
					<l n="37" num="19.1"><w n="37.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.2">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="37.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="37.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.5">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="37.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.7">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
					<l n="38" num="19.2"><space quantity="8" unit="char"></space><w n="38.1">L</w>’<w n="38.2">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="38.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="38.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.5">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="38.6">d</w>’<w n="38.7"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="38.8">s</w>’<w n="38.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="20">
					<l n="39" num="20.1"><w n="39.1">L</w>’<w n="39.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="39.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="39.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="39.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="39.7">cr<seg phoneme="wa" type="vs" value="1" rule="420">oî</seg>t</w> <w n="39.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.9">gl<seg phoneme="a" type="vs" value="1" rule="343">a</seg>ï<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w>,</l>
					<l n="40" num="20.2"><space quantity="8" unit="char"></space><w n="40.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="40.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="40.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="40.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="40.5">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				</lg>
				<lg n="21">
					<l n="41" num="21.1"><w n="41.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="41.2">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="41.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="41.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="41.5">l</w>’<w n="41.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="41.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="41.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c</w></l>
					<l n="42" num="21.2"><space quantity="8" unit="char"></space><w n="42.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="42.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="42.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="42.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1844">Août 1844.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>