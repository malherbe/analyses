<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VILLANELLES</head><div type="poem" key="BOR28">
							<head type="main">L’Incendie du Bazard</head>
							<opener>
								<epigraph>
									<cit>
										<quote>J’habite la montagne et j’aime à la vallée.</quote>
										<bibl>
											<name>LE VICOMTE D’ARLINCOURT</name>.
										</bibl>
									</cit>
								</epigraph>
							</opener>
							<lg n="1">
								<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="1.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="1.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.4">j</w>’<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.7">l</w>’<w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
								<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.4">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="2.5">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="2.6">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
								<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">L</w>’<w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>-<w n="3.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.6">J<seg phoneme="a" type="vs" value="1" rule="310">ea</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
								<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w>, <w n="4.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="4.6">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ?</l>
							</lg>
							<lg n="2">
								<l n="5" num="2.1"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.6">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="6" num="2.2"><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.4">m</w>’<w n="6.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="6.9">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>,</l>
								<l n="7" num="2.3"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="7.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.3">tr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>. <w n="7.6">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-27">e</seg></w> <w n="7.7">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
								<l n="8" num="2.4"><w n="8.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rç<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.2">l</w>’<w n="8.3">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.4">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="8.5">d</w>’<w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.7"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="8.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="8.9">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="8.10">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>,</l>
								<l n="9" num="2.5"><w n="9.1">Fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="9.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="9.5">d</w>’<w n="9.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="9.8">l</w>’<w n="9.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
								<l n="10" num="2.6"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="10.8">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="10.9">J<seg phoneme="a" type="vs" value="1" rule="310">ea</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
								<l n="11" num="2.7"><space unit="char" quantity="8"></space><w n="11.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w>, <w n="11.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="11.6">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
							</lg>
							<lg n="3">
								<l n="12" num="3.1"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="12.2">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ! <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.4">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ! <w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.6">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ! <w n="12.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.8">Vi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.9"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.10">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="12.11">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
								<l n="13" num="3.2"><w n="13.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w>… <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="13.4">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="13.6">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ! <w n="13.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="13.8">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ! <w n="13.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="13.10">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> !</l>
								<l n="14" num="3.3"><w n="14.1">N</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="14.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="14.5">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="14.6">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>th<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="14.7"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="14.8">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?…</l>
								<l n="15" num="3.4"><w n="15.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="15.2">c</w>’<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="15.7">g<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.8">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>thi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
								<l n="16" num="3.5">— <w n="16.1">Fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.2">d</w>’<w n="16.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="16.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="16.7">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="16.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.10">l<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.11">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="17" num="3.6"><w n="17.1">Fu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> !… <w n="17.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.5">l</w>’<w n="17.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="17.8">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="17.9">J<seg phoneme="a" type="vs" value="1" rule="310">ea</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
								<l n="18" num="3.7"><space unit="char" quantity="8"></space><w n="18.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w>, <w n="18.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="18.6">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ?</l>
							</lg>
							<lg n="4">
								<l n="19" num="4.1"><w n="19.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="19.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="19.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="19.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="19.6">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="19.7">l</w>’<w n="19.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="19.9">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>gl<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
								<l n="20" num="4.2"><w n="20.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="20.3">c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>, <w n="20.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.5">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="20.7">d</w>’<w n="20.8"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
								<l n="21" num="4.3"><w n="21.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="21.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>-<w n="21.4">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pi<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.6">c<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.7">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="22" num="4.4"><w n="22.1">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="22.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pt</w> ! <w n="22.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.8">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="22.9">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
								<l n="23" num="4.5"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="23.2">Bl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="23.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="23.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.5">T<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="23.6">cl<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc</w> <w n="23.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="23.8">l</w>’<w n="23.9"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="23.10"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="24" num="4.6"><w n="24.1">V<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !… <w n="24.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="24.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="24.5">l</w>’<w n="24.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="24.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="24.8">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="24.9">J<seg phoneme="a" type="vs" value="1" rule="310">ea</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
								<l n="25" num="4.7"><space unit="char" quantity="8"></space><w n="25.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w>, <w n="25.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="25.6">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
							</lg>
							<lg n="5">
								<l n="26" num="5.1"><space unit="char" quantity="8"></space><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="26.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="26.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="26.4">j</w>’<w n="26.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="26.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="26.7">l</w>’<w n="26.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
								<l n="27" num="5.2"><space unit="char" quantity="8"></space><w n="27.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="27.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="27.4">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="27.5">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="27.6">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
								<l n="28" num="5.3"><space unit="char" quantity="8"></space><w n="28.1">L</w>’<w n="28.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>-<w n="28.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="28.4">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="28.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.6">j<seg phoneme="a" type="vs" value="1" rule="310">ea</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
								<l n="29" num="5.4"><space unit="char" quantity="8"></space><w n="29.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w>, <w n="29.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="29.6">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> !</l>
							</lg>
						</div></body></text></TEI>