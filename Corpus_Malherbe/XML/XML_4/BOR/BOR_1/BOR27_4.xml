<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VILLANELLES</head><div type="poem" key="BOR27">
							<head type="main">La Soif des Amours</head>
							<opener>
								<epigraph>
									<cit>
										<quote>Hélène ; je vous suis tout vendu.</quote>
										<bibl>
											<name>AUGUSTUS MAC KEAT</name>.
										</bibl>
									</cit>
								</epigraph>
							</opener>
							<lg n="1">
								<l n="1" num="1.1"><space unit="char" quantity="2"></space><w n="1.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="1.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
								<l n="2" num="1.2"><space unit="char" quantity="2"></space><w n="2.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">j</w>’<w n="2.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w></l>
								<l n="3" num="1.3"><space unit="char" quantity="10"></space><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
								<l n="4" num="1.4"><space unit="char" quantity="2"></space><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="5" num="1.5"><space unit="char" quantity="2"></space><w n="5.1">Su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ;</l>
							</lg>
							<lg n="2">
								<l n="6" num="2.1"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.7">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
								<l n="7" num="2.2"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ;</l>
								<l n="8" num="2.3"><space unit="char" quantity="10"></space><w n="8.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="8.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
								<l n="9" num="2.4"><w n="9.1">R<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="9.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="10" num="2.5"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">n</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="10.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>f</w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> !</l>
							</lg>
							<lg n="3">
								<l n="11" num="3.1"><space unit="char" quantity="2"></space><w n="11.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="11.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.4">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
								<l n="12" num="3.2"><space unit="char" quantity="2"></space><w n="12.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="12.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">j</w>’<w n="12.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w></l>
								<l n="13" num="3.3"><space unit="char" quantity="10"></space><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
								<l n="14" num="3.4"><space unit="char" quantity="2"></space><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="14.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="14.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="15" num="3.5"><space unit="char" quantity="2"></space><w n="15.1">Su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
							</lg>
							<lg n="4">
								<l n="16" num="4.1"><w n="16.1">Qu</w>’<w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.3">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="17" num="4.2"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="17.2">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="17.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ;</l>
								<l n="18" num="4.3"><space unit="char" quantity="10"></space><w n="18.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="18.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
								<l n="19" num="4.4"><w n="19.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="19.2">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w> <w n="19.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.4">l</w>’<w n="19.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="20" num="4.5"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">n</w>’<w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="20.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>f</w> <w n="20.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> !</l>
							</lg>
							<lg n="5">
								<l n="21" num="5.1"><space unit="char" quantity="2"></space><w n="21.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="21.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.4">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
								<l n="22" num="5.2"><space unit="char" quantity="2"></space><w n="22.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="22.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">j</w>’<w n="22.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="22.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="22.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w></l>
								<l n="23" num="5.3"><space unit="char" quantity="10"></space><w n="23.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
								<l n="24" num="5.4"><space unit="char" quantity="2"></space><w n="24.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="24.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="24.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="24.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="25" num="5.5"><space unit="char" quantity="2"></space><w n="25.1">Su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="25.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.4">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
							</lg>
							<lg n="6">
								<l n="26" num="6.1"><w n="26.1">Qu</w>’<w n="26.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="26.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="26.6">c<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="27" num="6.2"><w n="27.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="27.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.3">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="27.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ;</l>
								<l n="28" num="6.3"><space unit="char" quantity="10"></space><w n="28.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="28.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
								<l n="29" num="6.4"><w n="29.1">M<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="29.2">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="29.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="29.4">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="30" num="6.5"><w n="30.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.2">n</w>’<w n="30.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="30.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>f</w> <w n="30.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> !</l>
							</lg>
							<lg n="7">
								<l n="31" num="7.1"><space unit="char" quantity="2"></space><w n="31.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="31.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="31.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.4">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
								<l n="32" num="7.2"><space unit="char" quantity="2"></space><w n="32.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="32.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.3">j</w>’<w n="32.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="32.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="32.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w></l>
								<l n="33" num="7.3"><space unit="char" quantity="10"></space><w n="33.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
								<l n="34" num="7.4"><space unit="char" quantity="2"></space><w n="34.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="34.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="34.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="34.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="35" num="7.5"><space unit="char" quantity="2"></space><w n="35.1">Su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="35.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.4">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
							</lg>
							<lg n="8">
								<l n="36" num="8.1"><w n="36.1">Qu</w>’<w n="36.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="36.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="36.4">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="36.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="36.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="36.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
								<l n="37" num="8.2"><w n="37.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="37.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="37.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="37.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="37.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="37.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ;</l>
								<l n="38" num="8.3"><space unit="char" quantity="10"></space><w n="38.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="38.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
								<l n="39" num="8.4"><w n="39.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="39.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="39.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="39.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.5">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="39.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="40" num="8.5"><w n="40.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.2">n</w>’<w n="40.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="40.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="40.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>f</w> <w n="40.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="40.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> !</l>
							</lg>
							<lg n="9">
								<l n="41" num="9.1"><space unit="char" quantity="2"></space><w n="41.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="41.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="41.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="41.4">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
								<l n="42" num="9.2"><space unit="char" quantity="2"></space><w n="42.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="42.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="42.3">j</w>’<w n="42.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="42.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="42.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="42.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w></l>
								<l n="43" num="9.3"><space unit="char" quantity="10"></space><w n="43.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="43.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
								<l n="44" num="9.4"><space unit="char" quantity="2"></space><w n="44.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="44.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="44.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="44.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="44.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="45" num="9.5"><space unit="char" quantity="2"></space><w n="45.1">Su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="45.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="45.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="45.4">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
							</lg>
							<lg n="10">
								<l n="46" num="10.1"><w n="46.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="46.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="46.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="46.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="46.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="46.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
								<l n="47" num="10.2"><w n="47.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="47.2">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="47.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="47.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="47.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="47.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="47.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ;</l>
								<l n="48" num="10.3"><space unit="char" quantity="10"></space><w n="48.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="48.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
								<l n="49" num="10.4"><w n="49.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="49.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="49.3">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="49.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="49.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="49.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
								<l n="50" num="10.5"><w n="50.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="50.2">n</w>’<w n="50.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="50.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="50.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="50.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>f</w> <w n="50.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="50.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> !</l>
							</lg>
							<lg n="11">
								<l n="51" num="11.1"><space unit="char" quantity="2"></space><w n="51.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="51.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="51.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="51.4">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
								<l n="52" num="11.2"><space unit="char" quantity="2"></space><w n="52.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="52.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="52.3">j</w>’<w n="52.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="52.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="52.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="52.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w></l>
								<l n="53" num="11.3"><space unit="char" quantity="10"></space><w n="53.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="53.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
								<l n="54" num="11.4"><space unit="char" quantity="2"></space><w n="54.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="54.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="54.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="54.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="54.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
								<l n="55" num="11.5"><space unit="char" quantity="2"></space><w n="55.1">Su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="55.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="55.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="55.4">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
							</lg>
						</div></body></text></TEI>