<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ARV">
					<name>
						<forename>Félix</forename>
						<surname>ARVERS</surname>
					</name>
					<date from="1806" to="1850">1806-1850</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4560 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ARV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Félix Arvers</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/felixarverspoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies de Félix Arvers: mes heures perdues. Pièces inédites</title>
						<author>Félix Arvers</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">http://www.archive.org/details/posiesdeflixOOarve </idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Félix Arvers</author>
								<imprint>
									<publisher>H. Floury, éditeur</publisher>
									<date when="1900">1900</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PIÈCES INÉDITES</head><div type="poem" key="ARV34">
					<head type="main">Ospitalita</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="1.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="1.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.9">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>pt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.3">d</w>’<w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.5">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="2.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.10">l</w>’<w n="2.11"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">l</w>’<w n="3.5"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.7">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.9">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="4.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="4.7">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="4.8">d</w>’<w n="4.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.10">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> ;</l>
						<l n="5" num="1.5"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="5.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>bi<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="5.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.6">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.9">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="1.6"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">l</w>’<w n="6.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="6.8">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.9">qu</w>’<w n="6.10"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="6.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.12">c<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="7" num="1.7"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.6">l</w>’<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.8">d</w>’<w n="7.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tru<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>…</l>
						<l n="8" num="1.8"><w n="8.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="8.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="8.7">j</w>’<w n="8.8"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="8.9">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.10">mi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.11">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.12">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>.</l>
						<l n="9" num="1.9"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="9.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.7">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="1.10"><w n="10.1">J</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="10.3">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="10.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.8">m<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.10">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="11" num="1.11"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="11.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>-<w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="11.9">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
						<l n="12" num="1.12"><w n="12.1">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.8">sph<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.9">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="12.10">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="13" num="1.13"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.3">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="13.7">j</w>’<w n="13.8"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="13.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.10">h<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="14" num="1.14"><w n="14.1">C</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.3">qu</w>’<w n="14.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.10">j</w>’<w n="14.11"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="14.12">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.13"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>