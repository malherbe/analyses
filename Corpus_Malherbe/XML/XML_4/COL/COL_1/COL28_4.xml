<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL28">
				<head type="main">CHANSON DE PARADE,</head>
				<head type="sub_1">Chantée par Gilles le Niais.</head>
				<head type="tune">Air : Vive les Grecs.</head>
				<head type="tune">Noté, N°. 20.</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468">I</seg></w> <w n="1.2">j</w>’<w n="1.3">sç<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="1.4">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">M<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="2" num="1.2"><space unit="char" quantity="6"></space><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rg<seg phoneme="y" type="vs" value="1" rule="448">u</seg>s</w>,</l>
					<l n="3" num="1.3"><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.2">j</w>’<w n="3.3">sç<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="3.4">d</w>’<w n="3.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.7">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="4" num="1.4"><space unit="char" quantity="6"></space><w n="4.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>’ <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
					<l n="5" num="1.5"><w n="5.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>-<w n="5.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <ref target="1" type="noteAnchor">(1)</ref></l>
					<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1">G<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">N<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> ?</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="468">I</seg></w> <w n="7.2">j</w>’<w n="7.3">sç<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.5">F<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>mm</w>’ <w n="7.6">pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="8" num="2.2"><space unit="char" quantity="6"></space><w n="8.1">J<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w> <w n="8.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.3">bru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> ;</l>
					<l n="9" num="2.3"><w n="9.1">S<seg phoneme="i" type="vs" value="1" rule="468">I</seg></w> <w n="9.2">j</w>’<w n="9.3">sç<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="10" num="2.4"><space unit="char" quantity="6"></space><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>,</l>
					<l n="11" num="2.5"><w n="11.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>-<w n="11.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
					<l n="12" num="2.6"><space unit="char" quantity="4"></space><w n="12.1">G<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">N<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> ?</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.2">j</w>’<w n="13.3">sç<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="13.4">b<seg phoneme="ɛ̃" type="vs" value="1" rule="221">en</seg></w> <w n="13.5">d</w>’<w n="13.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.7">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="14" num="3.2"><space unit="char" quantity="6"></space><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ;</l>
					<l n="15" num="3.3"><w n="15.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.2">j</w>’<w n="15.3">sç<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="15.4">c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.6">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="16" num="3.4"><space unit="char" quantity="6"></space><w n="16.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3">fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ;</l>
					<l n="17" num="3.5"><w n="17.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>-<w n="17.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
					<l n="18" num="3.6"><space unit="char" quantity="4"></space><w n="18.1">G<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">N<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> ?</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.2">j</w>’<w n="19.3">sç<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="19.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.6">F<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="20" num="4.2"><space unit="char" quantity="6"></space><w n="20.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="20.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xpl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ts</w> ;</l>
					<l n="21" num="4.3"><w n="21.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="21.2">j</w>’<w n="21.3">sç<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="21.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="21.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.6">D<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="22" num="4.4"><space unit="char" quantity="6"></space><w n="22.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="22.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="22.4">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w> ;</l>
					<l n="23" num="4.5"><w n="23.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>-<w n="23.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
					<l n="24" num="4.6"><space unit="char" quantity="4"></space><w n="24.1">G<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="24.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.3">N<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> ?</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">T<seg phoneme="u" type="vs" value="1" rule="425">OU</seg>T</w> <w n="25.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="25.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="26" num="5.2"><space unit="char" quantity="6"></space><w n="26.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="26.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.3">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>,</l>
					<l n="27" num="5.3"><w n="27.1">N</w>’<w n="27.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="27.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="27.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="27.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="27.7">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="28" num="5.4"><space unit="char" quantity="6"></space><w n="28.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="28.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w> ;</l>
					<l n="29" num="5.5"><w n="29.1">P<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="29.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.3">m</w>’<w n="29.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="29.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w></l>
					<l n="30" num="5.6"><space unit="char" quantity="4"></space><w n="30.1">G<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="30.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.3">N<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> ?</l>
				</lg>
				<closer>
					<note type="footnote" id="(1)">On remarquera que ce mot est écrit <lb></lb>conformement à l’Orthographe de M. de Voltaire.</note>
				</closer>
			</div></body></text></TEI>