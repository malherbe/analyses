<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL3">
				<head type="main">LE CALCUL,</head>
				<head type="form">PARODIE.</head>
				<head type="tune">Air : Toujours seule ! disoit Nina.</head>
				<head type="tune">Noté, N°. 3.</head>

				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">E</seg>S</w> <w n="1.2">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
					<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="2.2">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="2.4">F<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.5">n<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>f</w> <w n="3.6"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="3.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.5">D<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="5" num="1.5"><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="5.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="5.4">d</w>’<w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="6.2">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.4">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>du<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.6">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
					<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="7.2">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="7.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> :</l>
					<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">sç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu</w>’<w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w></l>
					<l n="9" num="1.9"><space unit="char" quantity="2"></space><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="9.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w></l>
					<l n="10" num="1.10"><space unit="char" quantity="14"></space><w n="10.1">N<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w>.</l>
				</lg>
			</div></body></text></TEI>