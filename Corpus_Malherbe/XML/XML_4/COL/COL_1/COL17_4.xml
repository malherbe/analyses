<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL17">
				<head type="main">LE GALAND SUPPLANTÉ,</head>
				<head type="sub_1">ANECDOTE DE LA COURTILLE.</head>
				<head type="tune">Air : Noté, N°. 15.</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="12"></space><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">UN</seg></w> <w n="1.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="12"></space><w n="2.1">R</w>’<w n="2.2">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.4">P<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="3" num="1.3"><space unit="char" quantity="16"></space><w n="3.1">Qu<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.2">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="10"></space><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="497">Y</seg></w> <w n="4.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="5" num="1.5"><space unit="char" quantity="2"></space><w n="5.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">v</w>’<w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="5.4">t</w>-<w n="5.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="5.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.8">Cr<seg phoneme="o" type="vs" value="1" rule="437">o</seg>cs</w>, <w n="5.9">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.10">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.11">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.12">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.13">F<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="6" num="1.6"><space unit="char" quantity="2"></space><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="497">Y</seg></w> <w n="6.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="6.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="6.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="6.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2">
					<l n="7" num="2.1"><space unit="char" quantity="12"></space><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">mi<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="7.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
					<l n="8" num="2.2"><space unit="char" quantity="12"></space><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.2">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
					<l n="9" num="2.3"><space unit="char" quantity="16"></space><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="9.3">V</w>’<w n="9.4">n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="9.5">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
					<l n="10" num="2.4"><space unit="char" quantity="10"></space><w n="10.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.5">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					<l n="11" num="2.5"><space unit="char" quantity="2"></space><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="11.4">f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="11.5">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">v</w>’<w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="11.8">t</w>-<w n="11.9"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.11">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.12">Dr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="2.6"><space unit="char" quantity="4"></space><w n="12.1">Lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.7">l</w>’<w n="12.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3">
					<l n="13" num="3.1"><space unit="char" quantity="12"></space><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">E</seg></w> <w n="13.2">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="13.3">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="13.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rbl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
					<l n="14" num="3.2"><space unit="char" quantity="12"></space><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="14.4">qu<seg phoneme="ə" type="em" value="1" rule="e-2">e</seg></w></l>
					<l n="15" num="3.3"><space unit="char" quantity="16"></space><w n="15.1">J</w>’<w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="15.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w></l>
					<l n="16" num="3.4"><space unit="char" quantity="12"></space><w n="16.1">Lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="16.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="16.5">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
					<l n="17" num="3.5"><space unit="char" quantity="4"></space><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.2">d</w>’<w n="17.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="17.4">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.6">g<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>, <w n="17.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="17.8">j</w>’<w n="17.9"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="17.10">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="17.11">M<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="18" num="3.6"><space unit="char" quantity="2"></space><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="497">Y</seg></w> <w n="18.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="18.3">m</w>’<w n="18.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.6">m<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="4">
					<l n="19" num="4.1"><space unit="char" quantity="12"></space><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">AI</seg>S</w> <w n="19.2">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="19.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="19.4">s</w>’<w n="19.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
					<l n="20" num="4.2"><space unit="char" quantity="12"></space><w n="20.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : <w n="20.2">M<seg phoneme="œ" type="vs" value="1" rule="151">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>r</w> <w n="20.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="20.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="20.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
					<l n="21" num="4.3"><space unit="char" quantity="16"></space><w n="21.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="21.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">l</w>’<w n="21.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
					<l n="22" num="4.4"><space unit="char" quantity="12"></space><w n="22.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="22.3">mi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="22.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="22.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> ?</l>
					<l n="23" num="4.5"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="23.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="23.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.7">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="24" num="4.6"><space unit="char" quantity="2"></space><w n="24.1"><seg phoneme="i" type="vs" value="1" rule="497">Y</seg></w> <w n="24.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.3">c</w>’<w n="24.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="24.5">b<seg phoneme="ɛ̃" type="vs" value="1" rule="221">en</seg></w> <w n="24.6">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="24.7">z</w>’<w n="24.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.9">vr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="24.10">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.11">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.12">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>