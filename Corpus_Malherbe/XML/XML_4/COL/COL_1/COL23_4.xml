<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL23">
				<head type="main">LES CORNES.</head>
				<head type="tune">Air : Noté, N°. 18.</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">E</seg>S</w> <w n="1.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="2.2">n</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.7">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.8">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
					<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>-<w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.5">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="5" num="1.5"><space unit="char" quantity="2"></space><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.3">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.5">l</w>’<w n="5.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="1.6"><space unit="char" quantity="2"></space><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="6.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.5">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>.</l>
					<l n="7" num="1.7"><space unit="char" quantity="4"></space><w n="7.1">Cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">m</w>’<w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.7">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>.</l>
				</lg>
			</div></body></text></TEI>