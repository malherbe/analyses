<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL53">
				<head type="main">COMPLAINTE D’UNE FEMME A SENTIMENS.</head>
				<head type="tune">Air : De mon Berger volage.</head>
				<head type="tune">Noté, N°. 32.</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">AN</seg>S</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">si<seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="1.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.6">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> !</l>
					<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>, <w n="3.5">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="3.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.7">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>.</l>
					<l n="5" num="1.5"><w n="5.1">T<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="5.2">n</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="5.5">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.5"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
					<l n="7" num="1.7"><w n="7.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="7.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="7.4">qu</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="8.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="8.3">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ?</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">T<seg phoneme="y" type="vs" value="1" rule="450">U</seg></w> <w n="9.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.5">m</w>’<w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="10" num="2.2"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="10.2">c</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.5">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> !</l>
					<l n="11" num="2.3"><w n="11.1">T<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="11.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="11.4">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="12" num="2.4"><w n="12.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="12.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>.</l>
					<l n="13" num="2.5"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="13.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="14" num="2.6"><w n="14.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.2">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="14.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rfl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> ;</l>
					<l n="15" num="2.7"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="305">Ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="15.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="15.3">d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="16" num="2.8"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">O</seg>MM<seg phoneme="ɑ̃" type="vs" value="1" rule="369">EN</seg>T</w> ! <w n="17.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="17.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="18" num="3.2"><w n="18.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="18.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> !</l>
					<l n="19" num="3.3"><w n="19.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">n</w>’<w n="19.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="19.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.6">j</w>’<w n="19.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="20" num="3.4"><w n="20.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="20.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="20.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> ;</l>
					<l n="21" num="3.5"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="21.5">s</w>’<w n="21.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="22" num="3.6"><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="22.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w>, <w n="22.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="22.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w> :</l>
					<l n="23" num="3.7"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> ! <w n="23.2">qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> ? <w n="23.3">M<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="24" num="3.8"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="24.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="24.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w> ?</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">QU<seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>LS</w> <w n="25.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="25.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="25.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.5">n<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
					<l n="26" num="4.2"><w n="26.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="26.2">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>-<w n="26.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="26.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ?</l>
					<l n="27" num="4.3"><w n="27.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="27.2">n</w>’<w n="27.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="27.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="27.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="27.6">d</w>’<w n="27.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="28" num="4.4"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="28.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="28.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.4">m</w>’<w n="28.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="28.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					<l n="29" num="4.5"><w n="29.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="30" num="4.6"><w n="30.1">Ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="30.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.3">l</w>’<w n="30.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="30.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.6">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> :</l>
					<l n="31" num="4.7"><w n="31.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="31.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="31.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="31.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="32" num="4.8"><w n="32.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="32.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="32.4">m</w>’<w n="32.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="32.6">mi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
			</div></body></text></TEI>