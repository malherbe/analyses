<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER132">
				<head type="number">CXXXII</head>
				<opener>
					<salute>A Fagus.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>-<w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">j</w>’<w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> ?</l>
					<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.5">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="3.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">n</w>’<w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="4.7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">H<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="5.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="5.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.4">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>bl<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="6.2">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="6.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="6.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> !</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="7.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="8" num="2.4"><w n="8.1">L<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s</w> <w n="8.2">t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>br<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="9" num="2.5"><w n="9.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.5">ch<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> !</l>
				</lg>
				<lg n="3">
					<l n="10" num="3.1"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.3">l</w>’<w n="10.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="11" num="3.2"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.6">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>m<seg phoneme="a" type="vs" value="1" rule="307">a</seg>il</w> <w n="12.2">f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w></l>
					<l n="13" num="4.2"><w n="13.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="13.2">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="13.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="14" num="4.3"><w n="14.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> ; <w n="14.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>-<w n="14.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="14.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w></l>
					<l n="15" num="4.4"><w n="15.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="15.2">r<seg phoneme="i" type="vs" value="1" rule="480">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>nt</w>-<w n="15.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.6">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
				</lg>
				<lg n="5">
					<l n="16" num="5.1"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="16.2">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.4">l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w></l>
					<l n="17" num="5.2"><w n="17.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="17.2">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="17.3">s<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>thr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
				</lg>
				<lg n="6">
					<l n="18" num="6.1"><w n="18.1">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="18.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="18.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.5">d<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
					<l n="19" num="6.2"><w n="19.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
					<l n="20" num="6.3"><w n="20.1">Gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="20.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg>s</w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.6">d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="7">
					<l n="21" num="7.1"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="21.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="21.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>. <w n="21.4"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="21.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
					<l n="22" num="7.2"><w n="22.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="22.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="22.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="22.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.6">m<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
				</lg>
				<lg n="8">
					<l n="23" num="8.1"><w n="23.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.4">cr<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="24" num="8.2"><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="24.2">j</w>’<w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="24.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>cr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ;</l>
					<l n="25" num="8.3"><w n="25.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.2">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="25.3">br<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="25.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="25.5">r<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="26" num="8.4"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="26.2">qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="26.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.4">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="26.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.6">cr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ?</l>
				</lg>
				<lg n="9">
					<l n="27" num="9.1"><w n="27.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="27.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="27.3">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="27.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="27.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="27.6">s</w>’<w n="27.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>r<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="28" num="9.2"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="28.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="28.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="28.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.6">l</w>’<w n="28.7">h<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w></l>
					<l n="29" num="9.3"><w n="29.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>-<w n="29.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="29.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="29.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.6">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.7">R<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="30" num="9.4"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="30.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="30.4">n<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="30.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.6">tr<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="31" num="9.5"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="31.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="31.5"><seg phoneme="ɛ" type="vs" value="1" rule="384">Ei</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> ?</l>
				</lg>
				<lg n="10">
					<l n="32" num="10.1"><w n="32.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="32.3">n</w>’<w n="32.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="32.5">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="32.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="32.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.8">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.9">n<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="33" num="10.2"><w n="33.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="33.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="33.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="33.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="33.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.6">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
				</lg>
				<lg n="11">
					<l n="34" num="11.1"><w n="34.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="34.2">d</w>’<w n="34.3">h<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="34.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="34.5">d</w>’<w n="34.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="34.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
					<l n="35" num="11.2"><w n="35.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="35.3">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="35.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="35.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.6">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="36" num="11.3"><w n="36.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="36.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="36.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="36.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="36.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
					<l n="37" num="11.4"><w n="37.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.2">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="37.3">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="37.4">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="12">
					<l n="38" num="12.1"><w n="38.1">R<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="38.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="38.3">b<seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="38.4">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="39" num="12.2"><w n="39.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="39.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="39.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="39.6">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.7">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="40" num="12.3"><w n="40.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> ? <w n="40.2"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="40.3">h<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="40.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="41" num="12.4"><w n="41.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="41.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="41.3">s</w>’<w n="41.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="41.5">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="41.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="41.7">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ds</w> <w n="41.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="41.9">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ds</w></l>
					<l n="42" num="12.5"><w n="42.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="42.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="42.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="42.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="42.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="42.6">pl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="13">
					<l n="43" num="13.1"><w n="43.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="43.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="43.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="43.4">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="43.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="44" num="13.2"><w n="44.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="44.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="44.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="44.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="44.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="44.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
				</lg>
			</div></body></text></TEI>