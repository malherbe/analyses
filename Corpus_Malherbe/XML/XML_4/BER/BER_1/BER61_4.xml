<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER61">
				<head type="main">LES PETITS COUPS</head>
				<head type="tune">AIR : Tout ça passe en même temps</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="2"></space><w n="1.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="1.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="2"></space><w n="2.1">R<seg phoneme="e" type="vs" value="1" rule="409">é</seg>gl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="3" num="1.3"><space unit="char" quantity="2"></space><w n="3.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xc<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="3.4">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="3.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="4.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.2">d</w>’<w n="5.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="6.4">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="6.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>,</l>
					<l n="7" num="1.7"><space unit="char" quantity="2"></space><w n="7.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="7.3">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.4">d</w>’<w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.6">h<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="1.8"><space unit="char" quantity="2"></space><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="8.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="8.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.5">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="8.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><space unit="char" quantity="2"></space><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.5">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>,</l>
					<l n="10" num="2.2"><space unit="char" quantity="2"></space><w n="10.1">V<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="10.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.3">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="2.3"><space unit="char" quantity="2"></space><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.4">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.6">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
					<l n="12" num="2.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">qu</w>’<w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="12.6">l</w>’<w n="12.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.8">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.9">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="13" num="2.5"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="13.3">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="13.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>-<w n="13.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="14" num="2.6"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.4">n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="14.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>ts</w> ;</l>
					<l n="15" num="2.7"><space unit="char" quantity="2"></space><w n="15.1">Tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="15.4">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="15.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="360">en</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="16" num="2.8"><space unit="char" quantity="2"></space><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="16.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="16.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.5">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="16.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><space unit="char" quantity="2"></space><w n="17.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="17.2">d</w>’<w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.4">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
					<l n="18" num="3.2"><space unit="char" quantity="2"></space><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="18.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="19" num="3.3"><space unit="char" quantity="2"></space><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="19.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="19.3">d</w>’<w n="19.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.5">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gt</w> <w n="19.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="20" num="3.4"><w n="20.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="20.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="20.3">l</w>’<w n="20.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="21" num="3.5"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="21.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="21.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="21.6">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="21.7">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="22" num="3.6"><w n="22.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="22.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="22.3">j<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w>, <w n="22.4">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="22.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ;</l>
					<l n="23" num="3.7"><space unit="char" quantity="2"></space><w n="23.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="23.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="23.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="23.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="24" num="3.8"><space unit="char" quantity="2"></space><w n="24.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="24.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="24.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="24.5">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="24.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><space unit="char" quantity="2"></space><w n="25.1">Ph<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="25.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="25.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.5"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ?</l>
					<l n="26" num="4.2"><space unit="char" quantity="2"></space><w n="26.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.2">l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w>-<w n="26.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="27" num="4.3"><space unit="char" quantity="2"></space><w n="27.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="27.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w>, <w n="27.4">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
					<l n="28" num="4.4"><w n="28.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="28.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.3">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="28.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="28.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="29" num="4.5"><w n="29.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="29.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="29.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="29.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="29.7">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="30" num="4.6"><w n="30.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="30.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.3">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="30.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w> <w n="30.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.7">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>ts</w>,</l>
					<l n="31" num="4.7"><space unit="char" quantity="2"></space><w n="31.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="31.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ltr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.3">qu</w>’<w n="31.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="31.5">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="32" num="4.8"><space unit="char" quantity="2"></space><w n="32.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="32.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="32.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="32.5">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="32.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w>.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><space unit="char" quantity="2"></space><w n="33.1">Ou<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="33.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="33.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="33.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
					<l n="34" num="5.2"><space unit="char" quantity="2"></space><w n="34.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="34.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="34.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="35" num="5.3"><space unit="char" quantity="2"></space><w n="35.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="35.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="35.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
					<l n="36" num="5.4"><w n="36.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="36.2">s<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="36.3">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="36.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="36.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="37" num="5.5"><w n="37.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="37.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="37.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="37.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.7">n<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="38" num="5.6"><w n="38.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="38.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="38.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="38.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="38.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="38.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> !</l>
					<l n="39" num="5.7"><space unit="char" quantity="2"></space><w n="39.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="39.2">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="39.3">l</w>’<w n="39.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="39.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="39.6">l</w>’<w n="39.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="40" num="5.8"><space unit="char" quantity="2"></space><w n="40.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="40.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="40.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="40.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="40.5">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="40.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w>.</l>
				</lg>
			</div></body></text></TEI>