<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER32">
				<head type="main">MA DERNIÈRE CHANSON PEUT-ÊTRE</head>
				<opener>
					<dateline>
						<date when="1814">Fin de janvier 1814</date>
					</dateline>
				</opener>
				<head type="tune">AIR : Eh quoi ! vous someillez encore (<hi rend="ital">de Fanchon</hi>)</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="y" type="vs" value="1" rule="391">eu</seg>s</w> <w n="1.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.5">d</w>’<w n="1.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="2.6">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>.</l>
					<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="4.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>.</l>
					<l n="5" num="1.5"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="5.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.6">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">d</w>’<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="6.5">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ?</l>
					<l n="7" num="1.7"><w n="7.1">Pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ</w>’<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.4">r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="8.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="e" type="vs" value="1" rule="170">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="9.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="9.3">d</w>’<w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.5">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="9.7">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="2.2"><w n="10.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="10.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ltr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="10.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					<l n="11" num="2.3"><w n="11.1">H<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cch<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.5">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="2.4"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.2">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>qu<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="12.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
					<l n="13" num="2.5"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="13.2">c</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="13.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.7">j</w>’<w n="13.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="14" num="2.6"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="14.2">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
					<l n="15" num="2.7"><w n="15.1">B<seg phoneme="y" type="vs" value="1" rule="450">u</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="15.2">g<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>, <w n="15.3">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="16" num="2.8"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="16.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="16.5">l</w>’<w n="16.6"><seg phoneme="e" type="vs" value="1" rule="170">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.2">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ci<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="17.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="17.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="18" num="3.2"><w n="18.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="18.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="18.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
					<l n="19" num="3.3"><w n="19.1">J</w>’<w n="19.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.3">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.4"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="20" num="3.4"><w n="20.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="20.2">j</w>’<w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="20.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>.</l>
					<l n="21" num="3.5"><w n="21.1">G<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="21.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">l</w>’<w n="21.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="22" num="3.6"><w n="22.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="22.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="22.3"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="22.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="22.5">j</w>’<w n="22.6"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="22.7">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
					<l n="23" num="3.7"><w n="23.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="23.2">m</w>’<w n="23.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="23.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w>, <w n="23.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="23.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="24" num="3.8"><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="24.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="24.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="24.5">l</w>’<w n="24.6"><seg phoneme="e" type="vs" value="1" rule="170">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.2">p<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="26" num="4.2"><w n="26.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="26.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="26.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="26.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>.</l>
					<l n="27" num="4.3"><w n="27.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="27.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="27.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.4">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="27.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="28" num="4.4"><w n="28.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="28.3">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="28.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>.</l>
					<l n="29" num="4.5"><w n="29.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xc<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="29.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.4">l</w>’<w n="29.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="30" num="4.6"><w n="30.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.2">l</w>’<w n="30.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="30.4">qu</w>’<w n="30.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="30.6">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
					<l n="31" num="4.7"><w n="31.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="31.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.3">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="31.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="32" num="4.8"><w n="32.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="32.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="32.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="32.5">l</w>’<w n="32.6"><seg phoneme="e" type="vs" value="1" rule="170">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> !</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="33.2">s</w>’<w n="33.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="33.4">n</w>’<w n="33.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="33.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="33.7">d</w>’<w n="33.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="34" num="5.2"><w n="34.1">J<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="34.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="34.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="34.5">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
					<l n="35" num="5.3"><w n="35.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="35.3">l</w>’<w n="35.4"><seg phoneme="e" type="vs" value="1" rule="170">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="35.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.7">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="36" num="5.4"><w n="36.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="36.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="36.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.4">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="36.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					<l n="37" num="5.5"><w n="37.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="37.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="37.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.4">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="37.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="37.6">qu</w>’<w n="37.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="37.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="38" num="5.6"><w n="38.1">Qu</w>’<w n="38.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="38.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="38.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.5">c<seg phoneme="i" type="vs" value="1" rule="493">y</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.7">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
					<l n="39" num="5.7"><w n="39.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="39.2">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="39.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="39.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="40" num="5.8"><w n="40.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="40.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="40.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="40.5">l</w>’<w n="40.6"><seg phoneme="e" type="vs" value="1" rule="170">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> !</l>
				</lg>
			</div></body></text></TEI>