<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER5">
				<head type="main">LA GAUDRIOLE</head>
				<head type="tune">AIR : La bonne aventure</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">M<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="1.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dj<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>ts</w></l>
					<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="2.3">d</w>’<w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.4">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>ts</w></l>
					<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d</w> <w n="4.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="5" num="1.5"><w n="5.1">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.2">s</w>’<w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="6.2">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">C<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="7" num="1.7"><space unit="char" quantity="4"></space><w n="7.1">C</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l rhyme="none" n="8" num="1.8"><space unit="char" quantity="10"></space><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="8.2">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="9" num="1.9"><space unit="char" quantity="4"></space><w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="10" num="2.1"><w n="10.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="10.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="10.4">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
					<l n="11" num="2.2"><space unit="char" quantity="4"></space><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.3">m</w>’<w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="12" num="2.3"><w n="12.1">M<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
					<l n="13" num="2.4"><space unit="char" quantity="4"></space><w n="13.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="14" num="2.5"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>’<w n="14.4">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="14.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>,</l>
					<l n="15" num="2.6"><w n="15.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>f<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="15.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w></l>
					<l n="16" num="2.7"><space unit="char" quantity="4"></space><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.3">g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l rhyme="none" n="17" num="2.8"><space unit="char" quantity="10"></space><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="17.2">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="18" num="2.9"><space unit="char" quantity="4"></space><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.3">g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="19" num="3.1"><w n="19.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="19.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w></l>
					<l n="20" num="3.2"><space unit="char" quantity="4"></space><w n="20.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">d</w>’<w n="20.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="21" num="3.3"><w n="21.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>li<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="21.3">gr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="21.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
					<l n="22" num="3.4"><space unit="char" quantity="4"></space><w n="22.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.2">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="22.3">l</w>’<w n="22.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="23" num="3.5"><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="23.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.4">l</w>’<w n="23.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xh<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>,</l>
					<l n="24" num="3.6"><w n="24.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="24.2">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w></l>
					<l n="25" num="3.7"><space unit="char" quantity="4"></space><w n="25.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.2">g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l rhyme="none" n="26" num="3.8"><space unit="char" quantity="10"></space><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="26.2">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="27" num="3.9"><space unit="char" quantity="4"></space><w n="27.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.2">g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="28" num="4.1"><w n="28.1">C</w>’<w n="28.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="28.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.4">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> ;</l>
					<l n="29" num="4.2"><space unit="char" quantity="4"></space><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="29.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="29.3">h<seg phoneme="i" type="vs" value="1" rule="493">y</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="30" num="4.3"><w n="30.1">Gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="30.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="30.4">dr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="30.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w>,</l>
					<l n="31" num="4.4"><space unit="char" quantity="4"></space><w n="31.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.2">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="31.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="32" num="4.5"><w n="32.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="32.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.3">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="32.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-4">e</seg>nt</w>,</l>
					<l n="33" num="4.6"><w n="33.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="33.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.3">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="33.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-4">e</seg>nt</w></l>
					<l n="34" num="4.7"><space unit="char" quantity="4"></space><w n="34.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="34.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.3">g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l rhyme="none" n="35" num="4.8"><space unit="char" quantity="10"></space><w n="35.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="35.2">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="36" num="4.9"><space unit="char" quantity="4"></space><w n="36.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="36.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="36.3">g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="37" num="5.1"><w n="37.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="37.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="37.4">gu<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="37.6">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>.</l>
					<l n="38" num="5.2"><space unit="char" quantity="4"></space><w n="38.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="38.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="38.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="38.4">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="39" num="5.3"><w n="39.1">Tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="39.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.3">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="39.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="39.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.6">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> ;</l>
					<l n="40" num="5.4"><space unit="char" quantity="4"></space><w n="40.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="40.3">s</w>’<w n="40.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="41" num="5.5"><w n="41.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="41.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="41.3">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="41.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="42" num="5.6"><w n="42.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="42.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="42.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="42.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="42.5">g<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ?</l>
					<l n="43" num="5.7"><space unit="char" quantity="4"></space><w n="43.1">C</w>’<w n="43.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="43.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="43.4">g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l rhyme="none" n="44" num="5.8"><space unit="char" quantity="10"></space><w n="44.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="44.2">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="45" num="5.9"><space unit="char" quantity="4"></space><w n="45.1">C</w>’<w n="45.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="45.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="45.4">g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="46" num="6.1"><w n="46.1">Pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="46.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="46.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="46.4">cr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="46.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w></l>
					<l n="47" num="6.2"><space unit="char" quantity="4"></space><w n="47.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu</w>’<w n="47.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="47.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="47.4">v<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="48" num="6.3"><w n="48.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="48.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="48.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="48.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="48.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w></l>
					<l n="49" num="6.4"><space unit="char" quantity="4"></space><w n="49.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="49.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="49.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="50" num="6.5"><w n="50.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="50.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="50.3">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="50.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="50.5">r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> :</l>
					<l n="51" num="6.6"><w n="51.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="51.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="51.3">gr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="51.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="51.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="51.6">m<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
					<l n="52" num="6.7"><space unit="char" quantity="4"></space><w n="52.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="52.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="52.3">g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l rhyme="none" n="53" num="6.8"><space unit="char" quantity="10"></space><w n="53.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="53.2">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="54" num="6.9"><space unit="char" quantity="4"></space><w n="54.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="54.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="54.3">g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>