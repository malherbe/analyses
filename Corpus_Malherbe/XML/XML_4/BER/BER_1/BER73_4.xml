<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER73">
				<head type="main">L’OPINION DE CES DEMOISELLES</head>
				<opener>
					<dateline>
						<date when="1815">Mois de mai 1815</date>
					</dateline>
				</opener>
				<head type="tune">AIR : Nom d’un chien, j’veut être épicurien</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> ! <w n="1.2">C</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="1.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="1.6">vr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.7">qu</w>’<w n="1.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">Qu</w>’ <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n</w>’<w n="2.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.5">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="2.7">r</w>’<w n="2.8">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.9">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="2.10">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
					<l n="3" num="1.3"><space unit="char" quantity="10"></space><w n="3.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="3.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="3.3">d</w>’<w n="3.4">ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>.</l>
					<l n="4" num="1.4"><w n="4.1">L</w>’ <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="4.3">r<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>, <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.6">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>’ <w n="4.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">S</w>’<w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>jou<seg phoneme="i" type="vs" value="1" rule="491">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ;</l>
					<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w>.</l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.2">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="7.3">d</w>’ <w n="7.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.7">cr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v</w>’ <w n="8.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="9" num="1.9"><space unit="char" quantity="4"></space><w n="9.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n</w>’<w n="9.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
				</lg>
				<lg n="2">
					<l n="10" num="2.1"><w n="10.1">D</w>’ <w n="10.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="10.3">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.4">j</w>’ <w n="10.5">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="10.6">l</w>’<w n="10.7">s</w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="11" num="2.2"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="11.2">n</w>’<w n="11.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="11.7">chr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w></l>
					<l n="12" num="2.3"><space unit="char" quantity="10"></space><w n="12.1">Qu</w>’ <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ssi<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w>.</l>
					<l n="13" num="2.4"><w n="13.1">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’ <w n="13.2">l</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="13.4">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="13.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.7">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="14" num="2.5"><space unit="char" quantity="8"></space><w n="14.1">F</w>’ <w n="14.2">s<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="14.3">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.4">d</w>’ <w n="14.5">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w></l>
					<l n="15" num="2.6"><space unit="char" quantity="4"></space><w n="15.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>’ <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.4">d</w>’<w n="15.5">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
					<l n="16" num="2.7"><w n="16.1">J</w>’ <w n="16.2">n</w>’<w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>vi<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="16.5">l</w>’ <w n="16.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="16.7">d</w>’<w n="16.8">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.9">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="16.10">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="17" num="2.8"><space unit="char" quantity="8"></space><w n="17.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v</w>’ <w n="17.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="18" num="2.9"><space unit="char" quantity="4"></space><w n="18.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="18.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n</w>’<w n="18.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
				</lg>
				<lg n="3">
					<l n="19" num="3.1"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="19.2">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ</w>’<w n="19.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="19.4">r</w>’<w n="19.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn</w>’<w n="19.6">t</w>, <w n="19.7">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="19.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="20" num="3.2"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">r</w>’<w n="20.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="20.4">B<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>f</w>, <w n="20.5">T<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>f</w>,</l>
					<l n="21" num="3.3"><space unit="char" quantity="10"></space><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="21.2">Pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>f</w> ;</l>
					<l n="22" num="3.4"><w n="22.1">L</w>’ <w n="22.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.3">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>k<seg phoneme="ɛ" type="vs" value="1" rule="BER73_1">e</seg>n</w>, <w n="22.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="22.5">l</w>’ <w n="22.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="22.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.8">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="22.9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="23" num="3.5"><space unit="char" quantity="8"></space><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="23.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>…</l>
					<l n="24" num="3.6"><space unit="char" quantity="4"></space><w n="24.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="24.3">M<seg phoneme="œ" type="vs" value="1" rule="151">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>r</w> <w n="24.4">Bl<seg phoneme="y" type="vs" value="1" rule="445">ü</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> :</l>
					<l n="25" num="3.7"><w n="25.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="25.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.3">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn</w>’<w n="25.4">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="25.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="25.6">c</w>’ <w n="25.7">qu</w>’<w n="25.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="25.9">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="25.10">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="26" num="3.8"><space unit="char" quantity="8"></space><w n="26.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v</w>’ <w n="26.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="27" num="3.9"><space unit="char" quantity="4"></space><w n="27.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="27.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="27.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n</w>’<w n="27.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
				</lg>
				<lg n="4">
					<l n="28" num="4.1"><w n="28.1">Dr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="28.2">qu</w>’ <w n="28.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.4">pl<seg phoneme="ɔ" type="vs" value="1" rule="451">u</seg>m</w>’<w n="28.5">s</w> <w n="28.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.7">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>q</w> <w n="28.8">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="28.9">r</w>’<w n="28.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="29" num="4.2"><w n="29.1">J</w>’ <w n="29.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="29.3">d</w>’ <w n="29.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="29.6">l</w>’ <w n="29.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>’ <w n="29.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
					<l n="30" num="4.3"><space unit="char" quantity="10"></space><w n="30.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>’ <w n="30.2">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>.</l>
					<l n="31" num="4.4"><w n="31.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="31.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="31.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="31.4">j</w>’<w n="31.5">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="31.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="31.7">r</w>’<w n="31.8">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="32" num="4.5"><space unit="char" quantity="8"></space><w n="32.1">Ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b</w>’ <w n="32.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="32.4">l</w>’ <w n="32.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w>,</l>
					<l n="33" num="4.6"><space unit="char" quantity="4"></space><w n="33.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="33.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="33.3">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="33.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="33.5">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="33.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>.</l>
					<l n="34" num="4.7"><w n="34.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="34.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="34.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>’ <w n="34.4">lou<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="34.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w>’ <w n="34.6">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="35" num="4.8"><space unit="char" quantity="8"></space><w n="35.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v</w>’ <w n="35.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="35.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="36" num="4.9"><space unit="char" quantity="4"></space><w n="36.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="36.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="36.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="36.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n</w>’<w n="36.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
				</lg>
				<lg n="5">
					<l n="37" num="5.1"><w n="37.1">J</w>’ <w n="37.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="37.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.4">d</w>’ <w n="37.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>’<w n="37.6">s</w> <w n="37.7">h<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w>’<w n="37.8">s</w> <w n="37.9">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="38" num="5.2"><w n="38.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="38.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="38.3">qu</w>’<w n="38.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="38.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="38.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="38.7">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="39" num="5.3"><space unit="char" quantity="10"></space><w n="39.1">L</w>’<w n="39.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="39.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="40" num="5.4"><w n="40.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="40.2">qu</w>’<w n="40.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="40.4">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu</w>’<w n="40.5">s</w>, <w n="40.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="40.7">d</w>’ <w n="40.8">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="40.9">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll</w>’<w n="40.10">s</w> <w n="40.11">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="41" num="5.5"><space unit="char" quantity="8"></space><w n="41.1">Pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="41.2">l</w>’<w n="41.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="42" num="5.6"><space unit="char" quantity="4"></space><w n="42.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="42.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rg</w> <w n="42.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w>-<w n="42.4">G<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
					<l n="43" num="5.7"><w n="43.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="43.2">l</w>’ <w n="43.3">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="43.4">qu</w>’<w n="43.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="43.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="43.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="43.8">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="43.9">d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="44" num="5.8"><space unit="char" quantity="8"></space><w n="44.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v</w>’ <w n="44.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="44.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="45" num="5.9"><space unit="char" quantity="4"></space><w n="45.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="45.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="45.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="45.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n</w>’<w n="45.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
				</lg>
				<lg n="6">
					<l n="46" num="6.1"><w n="46.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="46.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>’<w n="46.3">s</w> <w n="46.4">s</w>’<w n="46.5">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="46.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="46.7">b<seg phoneme="a" type="vs" value="1" rule="340">â</seg>cl<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="47" num="6.2"><w n="47.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="47.2">j</w>’<w n="47.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="47.4">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="47.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="47.6">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="47.7">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="48" num="6.3"><space unit="char" quantity="10"></space><w n="48.1">D</w>’ <w n="48.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
					<l n="49" num="6.4"><w n="49.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="49.2">y</w> <w n="49.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="49.4">qu<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>qu</w>’<w n="49.5">s</w> <w n="49.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="49.7">d</w>’ <w n="49.8">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="50" num="6.5"><space unit="char" quantity="8"></space><w n="50.1">Qu<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>qu</w>’<w n="50.2">s</w> <w n="50.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="50.4">d</w>’<w n="50.5"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>cc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="51" num="6.6"><space unit="char" quantity="4"></space><w n="51.1">C</w>’<w n="51.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="51.3">l</w>’ <w n="51.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="51.5">d</w>’<w n="51.6">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="51.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
					<l n="52" num="6.7"><w n="52.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="52.2">j</w>’ <w n="52.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="52.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="52.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="52.6">j</w>’ <w n="52.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’<w n="52.8">s</w> <w n="52.9">vi<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="53" num="6.8"><space unit="char" quantity="8"></space><w n="53.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v</w>’ <w n="53.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="53.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="54" num="6.9"><space unit="char" quantity="4"></space><w n="54.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="54.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="54.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="54.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n</w>’<w n="54.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
				</lg>
			</div></body></text></TEI>