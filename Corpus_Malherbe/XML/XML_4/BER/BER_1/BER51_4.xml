<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER51">
				<head type="main">LES PARQUES</head>
				<head type="tune">AIR : Elle aime à rire, elle aime à boire</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.3">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="1.4">gu<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.6">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ppr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="2.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> :</l>
					<l n="3" num="1.3"><w n="3.1">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cch<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="5" num="1.5"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.3">d</w>’<w n="6.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="6.6">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="7" num="1.7"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="7.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="1.8"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="8.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="8.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="170">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="2.2"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>tr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>, <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="10.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="10.4">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="11" num="2.3"><w n="11.1">B<seg phoneme="y" type="vs" value="1" rule="450">u</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w> <w n="11.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="12" num="2.4"><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="13" num="2.5"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.4">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="13.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="13.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
					<l n="14" num="2.6"><w n="14.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="15" num="2.7"><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="15.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="2.8"><w n="16.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="16.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="16.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="17.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="18" num="3.2"><w n="18.1">S</w>’<w n="18.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> : <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>tr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="18.4">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="18.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> !</l>
					<l n="19" num="3.3"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.2">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="19.3">s<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w>, <w n="19.4">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="19.5"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.6">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="19.7">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="20" num="3.4"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="20.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="20.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.7">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.8">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="21" num="3.5"><w n="21.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="21.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="21.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
					<l n="22" num="3.6"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="22.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="22.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="22.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.6">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="23" num="3.7"><w n="23.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="23.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="24" num="3.8"><w n="24.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="24.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="24.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="24.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="25.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.3">qu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="25.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="26" num="4.2"><w n="26.1">Cl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>th<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="26.2">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="26.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : <w n="26.4">ou<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="26.5">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> ;</l>
					<l n="27" num="4.3"><w n="27.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="27.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="27.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
					<l n="28" num="4.4"><w n="28.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="28.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.4">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="28.5">cr<seg phoneme="wa" type="vs" value="1" rule="420">oî</seg>t</w> <w n="28.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.7">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="29" num="4.5"><w n="29.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="29.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
					<l n="30" num="4.6"><w n="30.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="30.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="31" num="4.7"><w n="31.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="31.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="31.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="32" num="4.8"><w n="32.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="32.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="32.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="32.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="32.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> !</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="33.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.3">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="33.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="33.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="34" num="5.2"><w n="34.1">F<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="34.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="34.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="34.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="34.5">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="34.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="35" num="5.3"><w n="35.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="35.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="35.3">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="35.4">g<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="35.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="36" num="5.4"><w n="36.1">Cr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="36.2">qu</w>’<w n="36.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>tr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="36.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.5">s</w>’<w n="36.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="37" num="5.5"><w n="37.1">Qu</w>’<w n="37.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="37.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="37.5">gr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="37.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="37.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
					<l n="38" num="5.6"><w n="38.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="38.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="38.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="38.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="39" num="5.7"><w n="39.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="39.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="39.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="39.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="40" num="5.8"><w n="40.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="40.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="40.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="40.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="40.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="40.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="40.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> !</l>
				</lg>
			</div></body></text></TEI>