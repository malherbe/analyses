<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO36">
					<head type="main">L’INOCULATION</head>
					<head type="form">CONTE</head>
					<lg n="1">
						<l n="1" num="1.1">« <w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>, <w n="1.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>,</l>
						<l n="2" num="1.2">» <w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>, <w n="2.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w>-<w n="2.4">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>, <w n="2.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.7">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="2.9">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, »</l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, « <w n="3.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="3.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.7">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w>, <w n="3.8">c</w>’<w n="3.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.10">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="3.11">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="4" num="1.4">» <w n="4.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="4.4">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rz<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.7">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="4.8">f<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="4.9">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>,</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space>» <w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">d</w>’<w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.4">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="1.6">» <w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="6.4">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="6.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.7">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.9">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> ;</l>
						<l n="7" num="1.7"><space unit="char" quantity="4"></space>» <w n="7.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>… <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space>» ‒ <w n="8.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="8.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>. ‒ <w n="8.6">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="8.7">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space>» <w n="9.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">n</w>’<w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.7">qu</w>’<w n="9.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.9">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ;</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space>» <w n="10.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.4">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="11" num="1.11">» ‒ <w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>. ‒ <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="11.4">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>. ‒ <w n="11.5">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="11.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ‒ <w n="11.7">S<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>, <w n="11.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.9">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>. »</l>
						<l n="12" num="1.12"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="12.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="12.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.7">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="13" num="1.13"><w n="13.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="13.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>, <w n="13.7">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="14" num="1.14"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="14.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="14.3">l</w>’<w n="14.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="14.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.8">Tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ch<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
						<l n="15" num="1.15"><space unit="char" quantity="8"></space><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="15.2">cr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="1.16"><space unit="char" quantity="4"></space><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="16.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w>. <w n="16.4">L</w>’<w n="16.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="17" num="1.17"><space unit="char" quantity="4"></space>‒ « <w n="17.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">n</w>’<w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="17.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, » <w n="17.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="17.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, « <w n="17.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.8">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> ?</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space>» <w n="18.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.2">n</w>’<w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="18.4">qu</w>’<w n="18.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="18.6">m</w>’<w n="18.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="18.8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! »</l>
						<l n="19" num="1.19"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="19.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="19.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.4">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="19.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.6">c<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. ‒ « <w n="19.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.10">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space>» <w n="20.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="20.2">j</w>’<w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>vr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="20.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! »</l>
					</lg>
				</div></body></text></TEI>