<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO66">
					<head type="main">LE MENSONGE ÉVIDENT</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="1.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.5">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w>.</l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.4">d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg>y<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rgn<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="3.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>.</l>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="4.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>, <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.4">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.5">d</w>’<w n="4.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="6.2">si<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="6.3">n<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="6.4">n<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.5">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="7" num="1.7"><space unit="char" quantity="4"></space><w n="7.1">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.4">d</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="7.6">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.7">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="1.8"><space unit="char" quantity="4"></space><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ; <w n="8.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>, <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w></l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="9.4">n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w></l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="10.2">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="11" num="1.11"><w n="11.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.7">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.8">gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.10">bru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>,</l>
						<l n="12" num="1.12"><w n="12.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.4">d</w>’<w n="12.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="12.7">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="13" num="1.13"><space unit="char" quantity="8"></space><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="13.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.4">qu</w>’<w n="13.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.6">l</w>’<w n="13.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space><w n="14.1">S</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="14.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.5">s</w>’<w n="14.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>tr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>du<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>.</l>
						<l n="15" num="1.15"><space unit="char" quantity="8"></space>‒ « <w n="15.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> ! <w n="15.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="15.3">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="15.4">l</w>’<w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xtr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! »</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space><w n="16.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"></space>« <w n="17.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>… <w n="17.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="17.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?…</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space>» ‒ <w n="18.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ? <w n="18.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="18.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="18.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="18.6">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>…</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space>» ‒ <w n="19.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>, <w n="19.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="19.3">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="19.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="19.6"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space>» <w n="20.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w>-<w n="20.3">j<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="20.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="20.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
						<l n="21" num="1.21">» <w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="21.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.3">l</w>’<w n="21.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="21.5">n<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ! <w n="21.6">c</w>’<w n="21.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="21.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="21.9">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="21.10">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="21.11"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>…</l>
						<l n="22" num="1.22"><space unit="char" quantity="8"></space>» ‒ <w n="22.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="22.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="22.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="22.5">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ? »</l>
						<l n="23" num="1.23"><space unit="char" quantity="8"></space><w n="23.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="23.2">L<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="23.3">d</w>’<w n="23.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.6">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ;</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space>« <w n="24.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="24.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> ! <w n="24.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.4">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="24.5">m</w>’<w n="24.6"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="24.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="25" num="1.25"><space unit="char" quantity="8"></space>» <w n="25.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="25.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>. ‒ <w n="25.4">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="25.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="25.6"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> :</l>
						<l n="26" num="1.26"><space unit="char" quantity="8"></space>» <w n="26.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="26.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">n</w>’<w n="26.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="26.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.7">l</w>’<w n="26.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.9">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>. »</l>
					</lg>
				</div></body></text></TEI>