<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO44">
					<head type="main">LE LENDEMAIN DES NOCES</head>
					<head type="form">FOLIE DIALOGUÉE</head>
					<lg n="1">
						<l n="1" num="1.1">« <w n="1.1">H<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="1.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>,</l>
						<l n="2" num="1.2">» <w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="2.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="2.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">f<seg phoneme="i" type="vs" value="1" rule="468">î</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="3" num="1.3">» <w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.3">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="3.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="3.6">m</w>’<w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4">» <w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.4">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.5">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>.</l>
						<l n="5" num="1.5">» <w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="5.4">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="1.6">» <w n="6.1">J</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="6.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="6.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.5">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>…</l>
						<l n="7" num="1.7">» <w n="7.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.4">M<seg phoneme="œ" type="vs" value="1" rule="151">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>r</w> <w n="7.5">Chr<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="1.8">» <w n="8.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">m</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="8.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.7">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>.</l>
						<l n="9" num="1.9">» ‒ <w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="9.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>, <w n="9.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ? ‒ <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="9.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="1.10">» <w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="10.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">m</w>’<w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.8">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
						<l n="11" num="1.11">» <w n="11.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.3">f<seg phoneme="y" type="vs" value="1" rule="445">û</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="11.5">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : <w n="11.6">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="12" num="1.12">» <w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="12.3">t</w>’<w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.6">l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
						<l n="13" num="1.13">» <w n="13.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> ; <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="13.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="1.14">» <w n="14.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="14.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>… <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… ‒ <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="14.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>,</l>
						<l n="15" num="1.15">» <w n="15.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="15.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="15.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.5">Th<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?…</l>
						<l n="16" num="1.16">» <w n="16.1">Qu</w>’<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="16.3">t</w>-<w n="16.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="16.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ? <w n="16.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.7">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.8">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.9">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>.</l>
						<l n="17" num="1.17">» ‒ <w n="17.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.2">m</w>’<w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>vi<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="17.4">qu</w>’<w n="17.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="17.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vi<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="1.18">» <w n="18.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="18.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>…</l>
						<l n="19" num="1.19">» ‒ <w n="19.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="19.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. <w n="19.3"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>-<w n="19.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> ?… ‒ <w n="19.5">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.7">mi<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="1.20">» <w n="20.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="20.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="20.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
						<l n="21" num="1.21">» ‒ <w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="21.2">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> ?… ‒ <w n="21.3"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="21.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.7">l</w>’<w n="21.8"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="22" num="1.22">» <w n="22.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="22.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.3">s</w>’<w n="22.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="22.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
						<l n="23" num="1.23">» ‒ <w n="23.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.2">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> !… <w n="23.4"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ?</l>
						<l n="24" num="1.24">» ‒ <w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="24.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="24.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="24.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.7">v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="25" num="1.25">» <w n="25.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="25.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="25.4">d</w>’<w n="25.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="25.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="25.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> :</l>
						<l n="26" num="1.26">» <w n="26.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.3">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="26.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.5">t</w>’<w n="26.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="27" num="1.27">» <w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.4">pr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="27.6">m</w>’<w n="27.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
						<l n="28" num="1.28">» ‒ <w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="28.2">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> ?… ‒ <w n="28.3">Pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="28.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="28.5">m</w>’<w n="28.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="29" num="1.29">» ‒ <w n="29.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ? ‒ <w n="29.2">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="29.3">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="29.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="29.5">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w>,</l>
						<l n="30" num="1.30">» <w n="30.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="30.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="30.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>, <w n="30.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="31" num="1.31">» <w n="31.1">P<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="31.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="31.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="31.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ?</l>
						<l n="32" num="1.32">» ‒ <w n="32.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="32.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.3">t</w>’<w n="32.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="32.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="32.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.8">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="33" num="1.33">» ‒ <w n="33.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> ! <w n="33.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ; <w n="33.3">c</w>’<w n="33.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="33.5">l</w>’<w n="33.6">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.8">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="33.9">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> :</l>
						<l n="34" num="1.34">» <w n="34.1">M<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="34.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="34.3">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="34.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="34.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="35" num="1.35">» <w n="35.1">Qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> ! <w n="35.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="35.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> ?… <w n="35.4">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="35.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="35.7">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="36" num="1.36">» <w n="36.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.3">n</w>’<w n="36.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="36.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="36.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="36.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w></l>
						<l n="37" num="1.37">» <w n="37.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="37.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="37.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! »</l>
					</lg>
				</div></body></text></TEI>