<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO35">
					<head type="main">LE QUIPROQUO OU COLIN-MAILLARD</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="1.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.3">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.4">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>, <w n="1.5">l</w>’<w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.7">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.9">l</w>’<w n="1.10"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.11">Fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="2.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.3">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="2.6">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="3.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>-<w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="3.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.7">f<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.8">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.9">Cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="4.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.8">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ge<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>.</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space>« ‒ <w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, » <w n="5.3">s</w>’<w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="1.6">« <w n="6.1">D</w>’<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xc<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="?" type="va" value="1" rule="162">en</seg>t</w> <w n="6.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="6.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> :</l>
						<l n="7" num="1.7">» <w n="7.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.2">Fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="7.5">P<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">B<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> ;</l>
						<l n="8" num="1.8">» <w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.7">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="8.8">qu</w>’<w n="8.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.10">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.11">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space>» <w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">j<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.4">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>-<w n="9.5">M<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> ;</l>
						<l n="10" num="1.10">» <w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="10.5">qu</w>’<w n="10.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.9">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>. »</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space><w n="11.1">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w></l>
						<l n="12" num="1.12"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="12.3">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="12.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">cr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.7">gl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="13" num="1.13"><w n="13.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w>, <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.8">g<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w></l>
						<l n="14" num="1.14"><w n="14.1"><seg phoneme="y" type="vs" value="1" rule="391">Eu</seg>t</w> <w n="14.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="14.3">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="14.4">b<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.6">b<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.8">l</w>’<w n="14.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w>.</l>
						<l n="15" num="1.15"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="15.2">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="15.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.8">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="15.9">l</w>’<w n="15.10">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space><w n="16.1">L</w>’<w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="16.4">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="16.5">l</w>’<w n="16.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.7">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"></space><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="17.2">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="17.6">n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="1.18"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">l</w>’<w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.4">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="18.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.10">mi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="19" num="1.19"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="19.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="19.5">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="19.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.7">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.8">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.10">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
						<l n="20" num="1.20"><w n="20.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="20.4">d</w>’<w n="20.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="20.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="20.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.8">l</w>’<w n="20.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="21" num="1.21"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.3">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.5">g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w>, <w n="21.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.9">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="21.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
						<l n="22" num="1.22"><w n="22.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.2">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="22.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="22.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="22.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="22.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="22.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.9">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
						<l n="23" num="1.23"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="23.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="23.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.8">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="23.9">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.10">gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="24" num="1.24"><w n="24.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="24.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="24.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="24.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="24.5">qu</w>’<w n="24.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="24.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="24.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="24.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
						<l n="25" num="1.25"><space unit="char" quantity="8"></space><w n="25.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="25.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="25.3">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="25.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="26" num="1.26"><space unit="char" quantity="8"></space><w n="26.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t</w> <w n="26.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="26.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.6">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"></space><w n="27.1">C</w>’<w n="27.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="27.3">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="27.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="27.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.6">j<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="27.8">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ;</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"></space><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="28.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="28.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="28.5">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="29" num="1.29"><space unit="char" quantity="8"></space><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="29.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="29.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="29.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> :</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space>« <w n="30.1">C</w>’<w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="30.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ! » <w n="30.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="30.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>, « <w n="30.6">R<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="30.7">P<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="31" num="1.31"><space unit="char" quantity="8"></space>» <w n="31.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="31.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>. »</l>
					</lg>
				</div></body></text></TEI>