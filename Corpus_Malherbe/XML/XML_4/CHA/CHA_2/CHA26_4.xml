<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="CHA">
					<name>
						<forename>François-René</forename>
						<nameLink>de</nameLink>
						<surname>CHATEAUBRIAND</surname>
					</name>
					<date from="1768" to="1848">1768-1848</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>735 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CHA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies diverses</title>
						<author>François-René de Chateaubriand</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/chateaubriandpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1797" to="1827">1797-1827</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas incluse.</p>
				<p>Les notes de l’auteur ont été intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Des corrections métriques ont été apportées sans autres attestations publiées.</p>
				<p>Les vers de la strophe illisible du poème "SERMENT — LES NOIRS DEVANT LE GIBET DE JOHN BROWN" ont été marqués comme inanalysables.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
 					<p>L’orthographe a été vérifiée avec le correcteur Aspell.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CHA26">
				<head type="number">XVI</head>
				<head type="main">Charlottembourg</head>
				<head type="sub">Ou le tombeau de la reine de Prusse</head>
				<opener>
					<dateline>
						<placeName>Berlin</placeName>,
						<date when="1821">1821</date>.
					</dateline>
				</opener>
				<lg n="1">
					<head type="main">Le voyageur</head>
					<l n="1" num="1.1"><w n="1.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.3">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ts</w> <w n="1.4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.6">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="1.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="2" num="1.2"><w n="2.1">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rdi<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>, <w n="2.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="2.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="2.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="2.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> ?</l>
				</lg>
				<lg n="2">
					<head type="main">Le gardien</head>
					<l n="3" num="2.1"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="3.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
					<l n="4" num="2.2"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="4.2">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ! <w n="4.3">c</w>’<w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
				</lg>
				<lg n="3">
					<head type="main">Le voyageur</head>
					<l part="I" n="5" num="3.1"><w n="5.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.5">li<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ? </l>
				</lg>
				<lg n="4">
					<head type="main">Le gardien</head>
					<l part="F" n="5"><w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="5.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bj<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="5.8">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="5.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.10">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="5">
					<head type="main">Le voyageur</head>
					<l part="I" n="6" num="5.1"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> ? </l>
				</lg>
				<lg n="6">
					<head type="main">Le gardien</head>
					<l part="F" n="6"><w n="6.4">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.5">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
				<lg n="7">
					<head type="main">Le voyageur</head>
					<l part="I" n="7" num="7.1"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="7.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>. </l>
				</lg>
				<lg n="8">
					<head type="main">Le gardien</head>
					<l part="F" n="7"><w n="7.3">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.5">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="7.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l part="I" n="8" num="8.1"><w n="8.1">N</w>’<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>. </l>
				</lg>
				<lg n="9">
					<head type="main">Le voyageur</head>
					<l part="F" n="8"><w n="8.4">J</w>’<w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="8.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="8.7">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
				<p>Le voyageur et le gardien entrent.</p>
				<lg n="10">
					<head type="main">Le voyageur</head>
					<l n="9" num="10.1"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">Gr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">l</w>’<w n="9.7"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="10" num="10.2"><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.8">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.10">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w>.</l>
					<l n="11" num="10.3"><w n="11.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="11.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="11.3">l</w>’<w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.5">c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.8">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.9">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w> ?</l>
					<l n="12" num="10.4"><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="12.2">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="12.5">C<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
				</lg>
				<lg n="11">
					<head type="main">Le gardien</head>
					<l n="13" num="11.1"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="13.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.4">l</w>’<w n="13.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w></l>
					<l n="14" num="11.2"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="14.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.6">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="12">
					<head type="main">Le voyageur</head>
					<l n="15" num="12.1"><w n="15.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.6">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="15.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.9">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w></l>
					<l n="16" num="12.2"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="16.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.5">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
				</lg>
				<lg n="13">
					<head type="main">Le gardien</head>
					<l n="17" num="13.1"><w n="17.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="17.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="17.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w></l>
					<l n="18" num="13.2"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>-<w n="18.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="18.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="18.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="14">
					<head type="main">Le voyageur</head>
					<l part="I" n="19" num="14.1"><w n="19.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="19.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w>. </l>
				</lg>
				<lg n="15">
					<head type="main">Le gardien</head>
					<l part="F" n="19"><w n="19.3">C</w>’<w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> : <w n="19.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.8">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.9"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.10">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.11">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
					<l n="20" num="15.1"><w n="20.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="20.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.4">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="20.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="20.7">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="16">
					<head type="main">Le voyageur</head>
					<l part="I" n="21" num="16.1"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="21.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="21.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> ? </l>
				</lg>
				<lg n="17">
					<head type="main">Le gardien</head>
					<l part="F" n="21"><w n="21.6">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> : <w n="21.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="21.8">tr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.9">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.10">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="18">
					<head type="main">Le voyageur</head>
					<l n="22" num="18.1"><w n="22.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="22.2">tr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>