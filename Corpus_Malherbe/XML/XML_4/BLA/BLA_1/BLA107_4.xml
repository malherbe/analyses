<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA107">
					<head type="main">LES CORBEAUX</head>
					<head type="form">BALLADE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w>, <w n="1.4">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="1.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.7">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="2" num="2.1"><space unit="char" quantity="6"></space><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.3">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="3" num="2.2"><space unit="char" quantity="6"></space><w n="3.1">G<seg phoneme="i" type="vs" value="1" rule="468">î</seg>t</w> <w n="3.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ri<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="4" num="2.3"><space unit="char" quantity="6"></space><w n="4.1">N<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="4.2">n</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.4">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.6">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>pi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="5" num="2.4"><space unit="char" quantity="6"></space><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="5.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.4">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>, <w n="5.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.6">pr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="6" num="2.5"><space unit="char" quantity="6"></space><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rb<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="6.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="6.5">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xh<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w> <w n="7.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.7">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="8" num="4.1"><space unit="char" quantity="6"></space><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="8.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="8.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="8.5">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> :</l>
						<l n="9" num="4.2"><space unit="char" quantity="6"></space><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> ? <w n="9.4">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="9.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="10" num="4.3"><space unit="char" quantity="6"></space><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ? <w n="10.4">Di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="10.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="10.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="11" num="4.4"><space unit="char" quantity="6"></space><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="11.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="12" num="4.5"><space unit="char" quantity="6"></space><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rb<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="12.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="12.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="12.5">d<seg phoneme="i" type="vs" value="1" rule="467">î</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">n<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.7">n<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="14" num="6.1"><space unit="char" quantity="6"></space><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="15" num="6.2"><space unit="char" quantity="6"></space><w n="15.1">N</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="15.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="16" num="6.3"><space unit="char" quantity="6"></space><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.2">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="16.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="16.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="17" num="6.4"><space unit="char" quantity="6"></space><w n="17.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="17.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="17.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="18" num="6.5"><space unit="char" quantity="6"></space><w n="18.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rb<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="18.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="18.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="18.5">d<seg phoneme="i" type="vs" value="1" rule="467">î</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
					</lg>
				</div></body></text></TEI>