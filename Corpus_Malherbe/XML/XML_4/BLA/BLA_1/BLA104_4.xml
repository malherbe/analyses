<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA104">
					<head type="main">MÉDITATION</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w> <w n="1.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.6">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="1.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="12"></space><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="2.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w>, <w n="2.3">qu</w>’<w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="2.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ?</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="411">Ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>-<w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">qu</w>’<w n="3.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.9">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.10">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ?</l>
						<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="5.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.9"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="5.10">l</w>’<w n="5.11"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.12">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="1.6"><space unit="char" quantity="12"></space><w n="6.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="6.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ?</l>
						<l n="7" num="1.7"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ltr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.4">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="7.7">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.9">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="1.8"><space unit="char" quantity="12"></space><w n="8.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">qu</w>’<w n="8.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="8.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ?</l>
						<l n="9" num="1.9"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="9.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="9.4">v<seg phoneme="ø" type="vs" value="1" rule="248">œu</seg>x</w>, <w n="9.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.7">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="9.8">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="10" num="1.10"><space unit="char" quantity="12"></space><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rç<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ?</l>
						<l n="11" num="1.11"><w n="11.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="11.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="11.4">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.6">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="1.12"><space unit="char" quantity="12"></space><w n="12.1">S</w>’<w n="12.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ?</l>
						<l n="13" num="1.13"><w n="13.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="13.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w>, <w n="13.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="13.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.7">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.8">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="13.9">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="1.14"><space unit="char" quantity="12"></space><w n="14.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ?</l>
						<l n="15" num="1.15"><w n="15.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="15.7">l</w>’<w n="15.8">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="1.16"><space unit="char" quantity="12"></space><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="16.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ?</l>
						<l n="17" num="1.17"><w n="17.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="17.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.3">n</w>’<w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="17.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="17.6">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="17.7">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="17.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.10">plu<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="1.18"><space unit="char" quantity="12"></space><w n="18.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="18.2">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="18.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>,</l>
						<l n="19" num="1.19"><w n="19.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="19.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="19.3">qu</w>’<w n="19.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.5"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="19.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="19.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.9">s</w>’<w n="19.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppu<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="1.20"><space unit="char" quantity="12"></space><w n="20.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="20.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="20.3">c<seg phoneme="i" type="vs" value="1" rule="493">y</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="20.4">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> ?</l>
					</lg>
					<lg n="2">
						<l n="21" num="2.1"><w n="21.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! <w n="21.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="21.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="21.6">J</w>’<w n="21.7"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="21.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="21.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.10">pi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="22" num="2.2"><space unit="char" quantity="12"></space><w n="22.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="22.2">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lcr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.3">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="23" num="2.3"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">j</w>’<w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="23.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="23.5">l</w>’<w n="23.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">î</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.8">j</w>’<w n="23.9"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="23.10">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.11">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.12">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ssi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="24" num="2.4"><space unit="char" quantity="12"></space><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="24.2">j</w>’<w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="24.4">cr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> : — « <w n="24.5">N<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> !</l>
						<l n="25" num="2.5"><w n="25.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ssi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="25.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="25.3">n</w>’<w n="25.4"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="25.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> ! <w n="25.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="25.7">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="25.8">n</w>’<w n="25.9"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="25.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="25.11">l</w>’<w n="25.12"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="26" num="2.6"><space unit="char" quantity="12"></space><w n="26.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="26.4">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> !</l>
						<l n="27" num="2.7"><w n="27.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="27.2">n</w>’<w n="27.3"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="27.4">qu</w>’<w n="27.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.6">v<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="27.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="27.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="27.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.10">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="28" num="2.8"><space unit="char" quantity="12"></space><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="28.2">qu</w>’<w n="28.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="28.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="28.5">fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
						<l n="29" num="2.9"><w n="29.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="29.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="29.4">n</w>’<w n="29.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="29.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="29.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="29.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.9">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="29.10"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="29.11">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="30" num="2.10"><space unit="char" quantity="12"></space><w n="30.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="30.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>.</l>
						<l n="31" num="2.11"><w n="31.1">Qu</w>’<w n="31.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="31.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="31.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.- <w n="31.5">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="31.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="31.7">pi<seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="31.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="32" num="2.12"><space unit="char" quantity="12"></space><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="32.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="32.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> ! »</l>
					</lg>
					<lg n="3">
						<l n="33" num="3.1"><w n="33.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="33.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="33.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="33.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="33.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="33.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="33.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.8">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="34" num="3.2"><space unit="char" quantity="12"></space><w n="34.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="34.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="34.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="34.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> ;</l>
						<l n="35" num="3.3"><w n="35.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="35.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="35.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="35.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="35.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="35.7">c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="36" num="3.4"><space unit="char" quantity="12"></space><w n="36.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.2">c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="36.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="36.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>.</l>
						<l n="37" num="3.5"><w n="37.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="37.2">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="37.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="37.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="37.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="37.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="37.7">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="38" num="3.6"><space unit="char" quantity="12"></space><w n="38.1">L</w>’<w n="38.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="38.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="38.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="38.5">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="38.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> ;</l>
						<l n="39" num="3.7"><w n="39.1">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w>, <w n="39.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="39.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="39.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.5">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="39.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="39.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.8">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="40" num="3.8"><space unit="char" quantity="12"></space><w n="40.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="40.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="40.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="40.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>