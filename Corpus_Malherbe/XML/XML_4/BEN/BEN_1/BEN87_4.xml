<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN87">
					<head type="main">Le Jaloux.</head>
					<head type="form">STANCES.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">E</seg></w> <w n="1.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="1.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>,</l>
						<l n="2" num="1.2"><w n="2.1">M<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.3">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="wa" type="vs" value="1" rule="420">oî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
						<l n="6" num="2.2"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="493">y</seg></w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
						<l n="7" num="2.3"><w n="7.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.2">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.7">g<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.7">pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.2"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w> <w n="9.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="9.6">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ;</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">s</w>’<w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.3">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>x<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
						<l n="12" num="3.4"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.7">G<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="BEN__2">ou</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>r</w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.4">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.5">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w></l>
						<l n="14" num="4.2"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.3">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="14.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w></l>
						<l n="16" num="4.4"><w n="16.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.4">P<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="16.6">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="17.6">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="18" num="5.2"><w n="18.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.6">pr<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="19" num="5.3"><w n="19.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.3">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="19.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="19.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ssi<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="20" num="5.4"><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.5">qu</w>’<w n="20.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">O</seg>rb<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="21.2">l</w>’<w n="21.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="21.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.6">V<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>,</l>
						<l n="22" num="6.2"><w n="22.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="22.4"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="23" num="6.3"><w n="23.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="23.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="23.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.6">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w></l>
						<l n="24" num="6.4"><w n="24.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">l</w>’<w n="24.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="25.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="25.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="25.5">d</w>’<w n="25.6"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="26.2">n</w>’<w n="26.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="26.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.6">Rh<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="27" num="7.3"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.2">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="27.3">n</w>’<w n="27.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="27.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.7">M<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
						<l n="28" num="7.4"><w n="28.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.4">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="28.5">l</w>’<w n="28.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="29.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="29.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="30.4">n</w>’<w n="30.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="30.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.8">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="31" num="8.3"><w n="31.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.2">cr<seg phoneme="wa" type="vs" value="1" rule="424">oy</seg></w> <w n="31.3">qu</w>’<w n="31.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="31.5">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.6">d</w>’<w n="31.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="32" num="8.4"><w n="32.1">Tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="32.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.4">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="32.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.2">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="33.3">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="33.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="33.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="34" num="9.2"><w n="34.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="34.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="34.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.6">d</w>’<w n="34.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="35" num="9.3"><w n="35.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.2">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="35.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="35.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w></l>
						<l n="36" num="9.4"><w n="36.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="36.2">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="36.3">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w> <w n="36.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="36.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="36.6">r<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="37.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="37.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="37.6">Ci<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="38" num="10.2"><w n="38.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="38.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="38.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="38.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="39" num="10.3"><w n="39.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="39.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="39.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="39.4"><seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="40" num="10.4"><w n="40.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="40.2">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="40.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="40.4">l</w>’<w n="40.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="40.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>