<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre puéril</head><head type="main_subpart">IV</head><head type="main_subpart">Le cœur s’inquiète</head><head type="sub_1">Désirs épars… La voix intérieure</head><div type="poem" key="HEU8">
						<opener>
							<salute>Au poète Desprechins.</salute>
						</opener>
						<head type="main">L’AUBE EN FLEURS</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">fl<seg phoneme="o" type="vs" value="1" rule="435">o</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>cl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
							<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="2.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="2.7">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.8">l</w>’<w n="2.9"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
							<l n="3" num="1.3"><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="3.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.4">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
							<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.4"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
							<l n="5" num="1.5"><w n="5.1">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>cts</w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.4">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>il</w> <w n="5.5">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rds</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.9">n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
							<l n="6" num="1.6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="6.2">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pt<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.4">pl<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.6"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="7" num="1.7"><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.3">b<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc</w> <w n="7.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="7.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.9">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> !</l>
							<l n="8" num="1.8"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rg<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.6">bl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
							<l n="9" num="1.9"><w n="9.1">G<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="9.3">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.5">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="10" num="1.10"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.3">pl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="10.5">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="10.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
							<l n="11" num="1.11"><w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="308">Ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="303">aim</seg></w> <w n="11.8">d</w>’<w n="11.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lc<seg phoneme="i" type="vs" value="1" rule="d-4">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> !</l>
							<l n="12" num="1.12"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="12.8">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
							<l n="13" num="1.13"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="13.6">fru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="13.7">d</w>’<w n="13.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.9">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.10">pl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="14" num="1.14"><w n="14.1">G<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">l</w>’<w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="14.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.6">s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.7">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="15" num="1.15"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="15.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">l<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
							<l n="16" num="1.16"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>bl<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="16.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ;</l>
							<l n="17" num="1.17"><w n="17.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="17.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lli<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>, <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.6">gr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="18" num="1.18"><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="18.3">s</w>’<w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="18.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="18.6">s</w>’<w n="18.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="18.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.9">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
							<l n="19" num="1.19"><w n="19.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="19.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="19.8">d</w>’<w n="19.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
							<l n="20" num="1.20"><w n="20.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="20.3">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.4">n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="20.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="20.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="20.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.8">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> !</l>
							<l n="21" num="1.21">— <w n="21.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="21.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.3">d</w>’<w n="21.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="21.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.7">fi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.8">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
							<l n="22" num="1.22"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.3">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="22.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="22.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="22.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.9">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="23" num="1.23">… <w n="23.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rdi<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="23.3">D<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w></l>
							<l n="24" num="1.24"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="24.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>str<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.4">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="24.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr</w>’<w n="24.6"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w> <w n="24.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.8">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.9">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
							<l n="25" num="1.25"><w n="25.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="25.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ct<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.3">d</w>’<w n="25.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="25.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="25.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="26" num="1.26"><w n="26.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.2">M<seg phoneme="ø" type="vs" value="1" rule="399">eu</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xp<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.6">S<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="26.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
						</lg>
					</div></body></text></TEI>