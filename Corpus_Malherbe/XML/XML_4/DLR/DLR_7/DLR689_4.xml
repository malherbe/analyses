<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">A MES SŒURS</head><div type="poem" key="DLR689">
					<head type="main">A MA SŒUR SUZANNE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Mi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="1.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.7">l</w>’<w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="1.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.10">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">T<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">L</w>’<w n="2.9"><seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>f</w>,</l>
						<l n="3" num="1.3"><w n="3.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.6">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="3.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.8">vi<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.9">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="4.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="4.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="4.6">n<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>f</w></l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="5.3">c</w>’<w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.7">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.9">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="6" num="2.2"><w n="6.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="6.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>, <w n="6.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.3">n</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="7.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.8">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="7.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.10">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.11">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.12">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w></l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">N</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="9.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="9.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.6">qu</w>’<w n="9.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="9.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.9">l</w>’<w n="9.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="9.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">em</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.2">qu</w>’<w n="10.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="10.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.9">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>il</w>,</l>
						<l n="11" num="3.3"><w n="11.1">N</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="11.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.7">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="11.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.10">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="11.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.12">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.13">d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="12.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					</lg>
				</div></body></text></TEI>