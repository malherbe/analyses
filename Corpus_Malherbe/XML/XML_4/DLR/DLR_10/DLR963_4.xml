<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">JACQUELINE</head><div type="poem" key="DLR963">
					<head type="number">XI</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">J</w>’<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="1.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.6">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.8">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.9">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>…</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.3">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>. <w n="3.4">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">J</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="4.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.6">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">Pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.3">n</w>’<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="5.6">d</w>’<w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
						<l n="6" num="2.2"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">n</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="6.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="6.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.9">qu</w>’<w n="6.10"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="6.11">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.12">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="6.13">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">T<seg phoneme="ɔ" type="vs" value="1" rule="441">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.4">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="7.5">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.3">n</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="8.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="8.6">d</w>’<w n="8.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"></space><w n="9.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="9.3">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="9.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.6">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="10" num="3.2"><w n="10.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="10.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.8">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.9">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>.</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">N<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="11.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>.</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="12.3">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="12.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.6">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="8"></space><w n="13.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="13.5">n</w>’<w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="13.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
						<l n="14" num="4.2"><w n="14.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="14.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.3">n</w>’<w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="14.5">qu</w>’<w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="14.8">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.10">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.11">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="15.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.3">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>, <w n="15.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.5">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,.</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space>— <w n="16.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="16.5">n</w>’<w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="16.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					</lg>
				</div></body></text></TEI>