<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR893">
				<head type="main">LA ROUTE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">m<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r</w> <w n="1.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>j<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.6">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.8">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></l>
					<l n="3" num="1.3"><w n="3.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.3">l</w>’<w n="3.4">H<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.7">qu</w>’<w n="3.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.7">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">m<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r</w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="5.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.6">l</w>’<w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="5.8">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="5.9">s<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r</w>,</l>
					<l n="6" num="2.2"><w n="6.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="6.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="6.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="6.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>’<w n="6.8">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="7" num="2.3"><w n="7.1">Qu</w>’<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="7.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="7.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="7.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.9">vi<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.10"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="8.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.8">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.9">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="8.10">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.4">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="9.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.8">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.9">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="9.10">qu</w>’<w n="9.11"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.8">t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.10">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1">D</w>’<w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="11.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="11.7">l</w>’<w n="11.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="3.4"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="12.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="12.8">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
				</lg>
			</div></body></text></TEI>