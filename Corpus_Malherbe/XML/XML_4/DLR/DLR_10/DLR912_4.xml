<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR912">
				<head type="main">L’HORREUR</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">j</w>’<w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="12"></space><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.2">mi<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="3.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.6">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
					<l n="4" num="1.4"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.4">vi<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.5">n</w>’<w n="4.6"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="4.7">vi<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="5.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="5.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="5.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>,</l>
					<l n="6" num="2.2"><space unit="char" quantity="12"></space><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.2">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="7.4">b<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.6">j<seg phoneme="ø" type="vs" value="1" rule="390">eû</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.3">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="9.2">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.7">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="3.2"><space unit="char" quantity="12"></space><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w></l>
					<l n="11" num="3.3"><w n="11.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="11.5">s</w>’<w n="11.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.7">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.8">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>,</l>
					<l n="12" num="3.4"><w n="12.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, — <w n="12.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w> <w n="12.4">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> — <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w></l>
					<l n="14" num="4.2"><space unit="char" quantity="12"></space><w n="14.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="14.2">pr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="15" num="4.3"><w n="15.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.5">l</w>’<w n="15.6"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w></l>
					<l n="16" num="4.4"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.2">ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.5">br<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="17.2">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.3">n</w>’<w n="17.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="17.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
					<l n="18" num="5.2"><space unit="char" quantity="12"></space><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">l</w>’<w n="18.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="19" num="5.3"><w n="19.1">N<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bst<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.3">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="19.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="19.5">d</w>’<w n="19.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
					<l n="20" num="5.4"><w n="20.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w>, <w n="20.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>, <w n="20.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="20.4">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="20.5">qu</w>’<w n="20.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.7">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>, <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="21.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w></l>
					<l n="22" num="6.2"><space unit="char" quantity="12"></space><w n="22.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="22.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="23" num="6.3">(<w n="23.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="23.2">b<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>, <w n="23.3"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="23.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w>, <w n="23.5">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg>s</w> <w n="23.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.7">j</w>’<w n="23.8"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !)</l>
					<l n="24" num="6.4"><w n="24.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="24.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="24.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w>.</l>
				</lg>
			</div></body></text></TEI>