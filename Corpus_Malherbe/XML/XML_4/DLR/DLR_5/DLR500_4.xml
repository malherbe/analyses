<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA ROUTE</head><div type="poem" key="DLR500">
					<head type="main">CONSTANTINOPLE</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="1.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.4">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.6">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.8">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> !</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.2">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="3.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.4">L<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.8">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.9">m<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>.</l>
						<l n="5" num="1.5"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">B<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>sph<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="6.2">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.6">g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.7">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><space unit="char" quantity="8"></space><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.2"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="8.2">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
						<l n="9" num="2.3"><space unit="char" quantity="8"></space><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rti<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="9.4">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ts</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
						<l n="10" num="2.4"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="10.4">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.6">l</w>’<w n="10.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="10.8">qu</w>’<w n="10.9"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.10">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="11" num="2.5"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="11.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="11.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="11.6">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>, <w n="11.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="11.9">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>-<w n="11.10">m<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t</w></l>
						<l n="12" num="2.6"><space unit="char" quantity="8"></space><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">V<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><space unit="char" quantity="8"></space><w n="13.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>-<w n="13.2">j<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="13.3">l</w>’<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="13.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
						<l n="14" num="3.2"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="14.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ni<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="15" num="3.3"><space unit="char" quantity="8"></space><w n="15.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>, <w n="15.2">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
						<l n="16" num="3.4"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="16.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rg<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.7">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="17" num="3.5"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="17.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>stru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="17.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="17.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.5">l</w>’<w n="17.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.7">b<seg phoneme="i" type="vs" value="1" rule="493">y</seg>z<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
						<l n="18" num="3.6"><space unit="char" quantity="8"></space><w n="18.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.4">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.5">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="18.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					</lg>
				</div></body></text></TEI>