<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA MER</head><div type="poem" key="DLR491">
					<head type="main">SOIR D’HONFLEUR</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">H<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">ph<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="2.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.4">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="2.5">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="3.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.6">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="4" num="1.4"><w n="4.1">Cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="4.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.3">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.5">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="5.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="6.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="6.3">d</w>’<w n="6.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
						<l n="7" num="2.3"><w n="7.1">C<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="7.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>, <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="7.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.8">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="8.3">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="8.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.5">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>ni<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.6">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="9.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="9.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.5">n<seg phoneme="o" type="vs" value="1" rule="318">au</seg>fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="10" num="3.2"><w n="10.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3">ph<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.5">l</w>’<w n="10.6"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="10.7"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>.</l>
						<l n="11" num="3.3"><w n="11.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">n</w>’<w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.8">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="12.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.7">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="13.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="13.5">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="14" num="4.2"><w n="14.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="14.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="14.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.5">v<seg phoneme="e" type="vs" value="1" rule="383">e</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
						<l n="15" num="4.3"><w n="15.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="15.2">l</w>’<w n="15.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="15.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="15.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="16" num="4.4"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.2">ch<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ch<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="16.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="17.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="17.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.4"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="5.2"><w n="18.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="18.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="18.6">l</w>’<w n="18.7">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="19.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="19.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.4">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="20" num="5.4"><w n="20.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="20.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="20.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="20.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="21.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="21.4">vi<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.5">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="22" num="6.2"><w n="22.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">n<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>fs</w> <w n="22.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
						<l n="23" num="6.3"><w n="23.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="23.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="23.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="23.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="23.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.6">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="24" num="6.4"><w n="24.1">V<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>-<w n="24.2">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="24.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rpr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="25.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="25.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.7">l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="26" num="7.2"><w n="26.1">R<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="26.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="26.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="26.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="26.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>-<w n="26.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> !</l>
						<l n="27" num="7.3"><w n="27.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="27.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="27.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.4">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="27.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>,</l>
						<l n="28" num="7.4"><w n="28.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="28.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="28.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="28.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Ou<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="29.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.4">l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="29.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w></l>
						<l n="30" num="8.2"><w n="30.1">Fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="30.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="30.4">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="30.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="31" num="8.3"><w n="31.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="31.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="31.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="31.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="31.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="31.6">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="32" num="8.4"><w n="32.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="32.2">l</w>’<w n="32.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="32.4">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="32.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>…</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">H<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="33.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="33.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="33.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.6">ph<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="34" num="9.2"><w n="34.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="34.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="34.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="34.4">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="34.5">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
						<l n="35" num="9.3"><w n="35.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="35.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="35.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="35.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="35.6">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="36" num="9.4"><w n="36.1">Cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="36.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="36.3">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="36.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="36.5">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>