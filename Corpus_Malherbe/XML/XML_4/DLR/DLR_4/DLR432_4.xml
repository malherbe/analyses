<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN MARGE</head><div type="poem" key="DLR432">
					<head type="main">FOURNEAU ÉCONOMIQUE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.4">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.7">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w>, <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="1.9">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="1.10">d<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w></l>
						<l n="2" num="1.2"><w n="2.1">H<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="2.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="2.3">d<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="2.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.2">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">qu</w>’<w n="3.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.6">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.8">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="4" num="1.4"><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>, <w n="4.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.5">qu</w>’<w n="4.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.7">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="5.2">m</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="5.4">t</w>-<w n="5.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="5.6">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.9">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gl<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="6" num="2.2"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="6.3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303">aim</seg></w>, <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="6.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>f</w>, <w n="6.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="6.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.10">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="6.11">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ?</l>
						<l n="7" num="2.3"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="7.2">cr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="7.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="7.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="7.6">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="8" num="2.4"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">Chr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st</w>. <w n="8.7">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>-<w n="8.8">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>-<w n="8.9">j<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w>, <w n="8.10">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="8.11">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.12"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">N</w>’<w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="9.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="9.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.10"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="10" num="3.2"><w n="10.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.2">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="i" type="vs" value="1" rule="dc-1">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="10.5">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="10.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.7">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
						<l n="11" num="3.3"><w n="11.1">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w>, <w n="11.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="11.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.8">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="11.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.10">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="3.4"><w n="12.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="12.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="12.8">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>