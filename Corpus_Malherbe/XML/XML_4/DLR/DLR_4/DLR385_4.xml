<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN KROUMIRIE</head><div type="poem" key="DLR385">
					<head type="main">AUTREMENT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ccr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="1.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.8">br<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.5">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.7">b<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.10">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w></l>
						<l n="3" num="1.3"><w n="3.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="3.2">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="3.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.6">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>-<w n="3.7">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.8">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.9">s</w>’<w n="3.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">F<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.2">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.5">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="4.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.8">mi<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w></l>
						<l n="5" num="1.5"><w n="5.1">D</w>’<w n="5.2"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.4">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="1.6"><w n="6.1">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="6.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ph<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w>.</l>
						<l n="7" num="1.7"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.7">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="7.9">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="8" num="1.8"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w>, <w n="8.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.3">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.6">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.8">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.9">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="9" num="1.9"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.9">cr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.11">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s</w>…</l>
					</lg>
					<lg n="2">
						<l n="10" num="2.1"><w n="10.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.3">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="10.4">c</w>’<w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.8">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sti<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="11" num="2.2"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="11.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="11.4">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.8">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="12" num="2.3"><w n="12.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.3">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="12.4">c</w>’<w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.7">n<seg phoneme="ɛ̃" type="vs" value="1" rule="494">ym</seg>ph<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.9">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.10">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.11"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w>,</l>
						<l n="13" num="2.4"><w n="13.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.5">s</w>’<w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>fu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="13.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.9">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.10">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.11">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w>.</l>
					</lg>
				</div></body></text></TEI>