<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">MAI, JOLI MAI !</head><div type="poem" key="DLR804">
					<head type="main">PRIMAVERA</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="1.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="1.4">r<seg phoneme="i" type="vs" value="1" rule="493">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">Pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.6">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.8">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">H<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">n<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="3.5">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="3.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="3.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> : « <w n="3.10">J</w>’<w n="3.11"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! »</l>
						<l n="4" num="1.4"><w n="4.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="4.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="a" type="vs" value="1" rule="365">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="4.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="5.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>, <w n="5.4">pl<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="5.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="5.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.9">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>si<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.3">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ri<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="6.4">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.6">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>x<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.4">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>, <w n="7.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.6">l</w>’<w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.8">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.10">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.11">n<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="8.7">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.10"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="8.11">ci<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.2">l</w>’<w n="9.3">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="10" num="3.2"><w n="10.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.5">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>.</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="11.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.5">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.6">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="11.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.9">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="12.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="12.4">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.5">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="12.8">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="13.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="13.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="13.6">n<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="4.2"><w n="14.1">Fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="14.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="14.3"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>, <w n="14.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>, <w n="14.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="14.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
						<l n="15" num="4.3"><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="15.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="15.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="15.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="15.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="15.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.9">pr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="4.4"><w n="16.1">Cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="16.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="16.5">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="16.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> !</l>
					</lg>
				</div></body></text></TEI>