<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">L’AVENUE</head><div type="poem" key="DLR800">
					<head type="main">HIVER</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.5">l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><space unit="char" quantity="10"></space><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.2">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="3" num="1.3"><w n="3.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>, <w n="3.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="3.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="10"></space><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">l</w>’<w n="4.3">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="5.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.5">n<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="10"></space><w n="6.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="7" num="2.3"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">s</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ccr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="10"></space><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="8.2">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="8.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="9.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="10"></space><w n="10.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">f<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">D</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="10"></space><w n="12.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">L</w>’<w n="13.2">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="13.3">m</w>’<w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : « <w n="13.6">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.8">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="4.2"><space unit="char" quantity="10"></space><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.2">t</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="15" num="4.3"><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>-<w n="15.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="10"></space><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="16.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.3">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> ? »</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">J</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="17.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : « <w n="17.4">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="17.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.8">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.9">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="5.2"><space unit="char" quantity="10"></space><w n="18.1">S<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="18.2">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="19" num="5.3"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.2">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">l</w>’<w n="19.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="10"></space><w n="20.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.2">n</w>’<w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="21.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="21.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="21.7">pl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="dc-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="22" num="6.2"><space unit="char" quantity="10"></space><w n="22.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="22.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="23" num="6.3"><w n="23.1">L</w>’<w n="23.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="23.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="23.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="23.6">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="10"></space><w n="24.1">Cr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> : <hi rend="ital"><w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w></hi>. »</l>
					</lg>
				</div></body></text></TEI>