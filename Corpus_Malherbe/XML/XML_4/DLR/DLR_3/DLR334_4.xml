<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE DÉPART</head><div type="poem" key="DLR334">
					<head type="main">LE DÉPART</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ph<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.7">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="2.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="2.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="3.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.7">ph<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ph<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="4" num="1.4"><w n="4.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="4.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="4.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">d</w>’<w n="4.8"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="4.9">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>q<seg phoneme="wa" type="vs" value="1" rule="256">ua</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.7"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="5.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rct<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="6" num="2.2"><w n="6.1">Br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.3">n<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>th<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="7" num="2.3"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.3">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>. <w n="7.4">J<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="7.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="7.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
						<l n="8" num="2.4"><w n="8.1">Pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="8.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="8.7">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.2">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xc<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
						<l n="10" num="3.2"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="10.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.4">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> <w n="10.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="10.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>.</l>
						<l n="11" num="3.3"><w n="11.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>gr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w></l>
						<l n="12" num="3.4"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>lli<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>str<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.5"><seg phoneme="i" type="vs" value="1" rule="468">î</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="13.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">bl<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">l</w>’<w n="13.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.8">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w></l>
						<l n="14" num="4.2"><w n="14.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.5">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="15" num="4.3"><w n="15.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="15.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="15.4">cl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.6">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="15.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.8">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="4.4"><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="ø" type="vs" value="1" rule="405">Eu</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="16.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="16.6">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="16.7">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="16.8">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w>. <w n="17.2">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w>, <w n="17.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.7">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="17.8">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.9">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w> ;</l>
						<l n="18" num="5.2"><w n="18.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="18.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="18.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.6">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w>.</l>
						<l n="19" num="5.3"><w n="19.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w>. <w n="19.2">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w>. <w n="19.3">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="19.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="19.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="19.6">l</w>’<w n="19.7">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.8">s</w>’<w n="19.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w>,</l>
						<l n="20" num="5.4"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="20.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="20.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="20.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>cts</w> <w n="20.5">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="20.6"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="20.7"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="21.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.5">d</w>’<w n="21.6">h<seg phoneme="i" type="vs" value="1" rule="493">y</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="22" num="6.2"><w n="22.1">P<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="22.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="22.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
						<l n="23" num="6.3"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="23.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="23.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.6">gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="23.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="23.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.10">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="23.11"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="24" num="6.4"><w n="24.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="24.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="24.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="24.4">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="24.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="24.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.7">g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="24.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.9">di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
					</lg>
				</div></body></text></TEI>