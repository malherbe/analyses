<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR104">
					<head type="main">CRÉPUSCULE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="1.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.8">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="1.9">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.8">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.4">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="3.6">l</w>’<w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="3.8">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">R<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.3">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>cs</w> <w n="4.4">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w>, <w n="4.5">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.8">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.9"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>dr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">bru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="5.5">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.7">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
						<l n="6" num="2.2"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">c<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>, <w n="6.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="6.7">m</w>’<w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.9">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.10">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.11">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="7" num="2.3"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="7.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.7">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.8">gr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">Gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="8.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.8">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.9">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="9.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="9.8">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="10" num="3.2"><w n="10.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.2">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>, <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="10.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="10.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.9">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w></l>
						<l n="11" num="3.3"><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.2">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="11.4">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="12.5">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.6">r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.8">qu<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="13.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="13.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>, <w n="13.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.5">l</w>’<w n="13.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="13.8">l</w>’<w n="13.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.10">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="13.11">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w>,</l>
						<l n="14" num="4.2"><w n="14.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="14.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="14.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="14.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-27">e</seg></w> <w n="14.7">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="15.4">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">l</w>’<w n="15.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ff<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="4.4"><w n="16.1">C<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.3">c<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="16.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="16.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.8">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="16.9">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="17.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="17.4">s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="17.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="17.6">l</w>’<w n="17.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.8">j<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="5.2"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>, <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="18.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>,</l>
						<l n="19" num="5.3"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">t</w>’<w n="19.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="19.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="19.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="19.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.8">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="19.9"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w></l>
						<l n="20" num="5.4"><w n="20.1">Tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="20.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w>, <w n="20.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="20.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>