<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ÂME DES RUES</head><div type="poem" key="DLR67">
					<head type="main">PLACES</head>
					<opener>
						<salute>A Mlle Marie Bengesco.</salute>
					</opener>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="1.8">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.10">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
							<l n="2" num="1.2"><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="2.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">bru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="2.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
							<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="3.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.4">s<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.6">br<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="4.3">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="4.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="4.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.8">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.7">ru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.9">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w>,</l>
							<l n="6" num="1.6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.3">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rds</w> <w n="6.4">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.8">d</w>’<w n="6.9"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w>,</l>
							<l n="7" num="1.7"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">fl<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="7.4">s</w>’<w n="7.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.7">l</w>’<w n="7.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="8" num="1.8"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.2">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="8.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.4">si<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w> <w n="8.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.7">r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.8">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="9" num="1.9"><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="9.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="9.6">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
							<l n="10" num="1.10"><w n="10.1">Pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="10.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>fs</w> <w n="10.6">c<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ;</l>
							<l n="11" num="1.11"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">qu</w>’<w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>, <w n="11.4">sc<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="11.8">d</w>’<w n="11.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="12" num="1.12"><w n="12.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.3">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="12.4">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>vr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.5">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="12.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="12.7">m<seg phoneme="wa" type="vs" value="1" rule="192">oe</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="13" num="1.13"><w n="13.1">G<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>, <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="13.3">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>, <w n="13.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="13.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="13.6">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
							<l n="14" num="1.14"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="14.3">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>th<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.5">d</w>’<w n="14.6"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="14.7">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="15" num="1.1"><w n="15.1">L</w>’<w n="15.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rcs</w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">tr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>ph<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>l<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
							<l n="16" num="1.2"><w n="16.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="16.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="16.5">cr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="16.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="16.8">h<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
							<l n="17" num="1.3"><w n="17.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.8">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="18" num="1.4"><w n="18.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="18.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="18.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.7">m<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="18.8">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="18.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.10">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						</lg>
						<lg n="2">
							<l n="19" num="2.1"><w n="19.1">F<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="19.2">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="19.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.5">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>cs</w> <w n="19.6">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="19.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="19.8">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="19.9">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w></l>
							<l n="20" num="2.2"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="20.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="20.4">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="20.5">fl<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="20.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.8">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w>,</l>
							<l n="21" num="2.3"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>tm<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>sph<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.2">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="21.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="21.4">s</w>’<w n="21.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="21.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.7">fl<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="22" num="2.4"><w n="22.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.2">cl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="22.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.5">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="22.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.7">pi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="22.9">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="22.10">br<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						</lg>
						<lg n="3">
							<l n="23" num="3.1"><w n="23.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="23.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.8">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="23.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.10"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w></l>
							<l n="24" num="3.2"><w n="24.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.3">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="24.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="24.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="24.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="24.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.8"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w></l>
							<l n="25" num="3.3"><w n="25.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="25.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="25.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="25.4">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ds</w> <w n="25.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="25.6">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="25.7">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="26" num="3.4"><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="26.2">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="26.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="26.5">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="26.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.8">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						</lg>
						<lg n="4">
							<l n="27" num="4.1"><w n="27.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="27.2">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="27.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.4">ci<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ! <w n="27.5"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="27.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="27.7"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="27.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> !</l>
							<l n="28" num="4.2"><w n="28.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="28.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="28.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="28.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="28.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.8">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>,</l>
							<l n="29" num="4.3"><w n="29.1">M<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="29.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>, <w n="29.4">l</w>’<w n="29.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="29.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="29.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="29.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.9">r<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="30" num="4.4"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="30.2">l</w>’<w n="30.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="30.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="y" type="vs" value="1" rule="448">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.7">d</w>’<w n="30.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						</lg>
					</div>
				</div></body></text></TEI>