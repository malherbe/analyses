<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ENCENSOIR</head><div type="poem" key="DLR93">
					<head type="main">SONNET</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">gr<seg phoneme="o" type="vs" value="1" rule="434">o</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.7">n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="2.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.7">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>,</l>
						<l n="3" num="1.3"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xtr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="3.4">l</w>’<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="3.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="3.9">v<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w></l>
						<l n="4" num="1.4"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="4.4">p<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.7">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w></l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">b<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>il</w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.6">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.8">f<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.5"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="7.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.8">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.10">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="8.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="8.8">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.3">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.8">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.9"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="10.2">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.5">c</w>’<w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="10.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="10.11">h<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="11" num="3.3"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>chi<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">N</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="12.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="12.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.6">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="12.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.9">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="13" num="4.2"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.3">d</w>’<w n="13.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.7">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sc<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="4.3"><w n="14.1"><seg phoneme="wɛ̃" type="vs" value="1" rule="417">Oin</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="14.4">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="14.5">d</w>’<w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.7">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="14.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.9">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.10">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					</lg>
				</div></body></text></TEI>