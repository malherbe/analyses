<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ÂME DES RUES</head><div type="poem" key="DLR68">
					<head type="main">REFLETS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">t</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w>, <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="1.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.10">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">j</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="2.4">v<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="2.7">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="2.8">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>,</l>
						<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="3.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">n</w>’<w n="3.9"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.10">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="3.11">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.12">v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="4.4">l</w>’<w n="4.5">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="4.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.8"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="5.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.6"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w> <w n="5.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.9">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="2.2"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.2">n<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="6.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="6.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.7">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rds</w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w>, <w n="7.2">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg></w> <w n="7.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.6">d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ils</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="9.4">d</w>’<w n="9.5"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.9">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="10.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="10.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
						<l n="11" num="3.3"><w n="11.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="11.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="11.6">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="12" num="3.4"><w n="12.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="12.5">d</w>’<w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.7">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w> ;</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="13.2">qu</w>’<w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.4">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.5">d</w>’<w n="13.6"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="13.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="13.10"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="4.2"><w n="14.1">D<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.3">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>,</l>
						<l n="15" num="4.3"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.4">l<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="15.5">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="15.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.8">br<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="4.4"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="16.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.3">l</w>’<w n="16.4"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="16.5">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.9">m<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w></l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="17.2">qu</w>’<w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.6">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.7">m<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="5.2"><w n="18.1">Gr<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="18.2">d</w>’<w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w> <w n="18.5">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="18.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bm<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Fl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.2">d</w>’<w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="19.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="20" num="5.4"><w n="20.1">Sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">ph<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>sph<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="20.5">r<seg phoneme="i" type="vs" value="1" rule="493">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="21.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="21.3">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.7">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="22" num="6.2"><w n="22.1">Lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="22.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="22.4">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="22.5">cr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="22.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.7"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="23.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="23.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="23.5">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="23.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.8">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="24" num="6.4"><w n="24.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="24.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.3">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.5">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="24.6">s</w>’<w n="24.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="24.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.9">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>.</l>
					</lg>
				</div></body></text></TEI>