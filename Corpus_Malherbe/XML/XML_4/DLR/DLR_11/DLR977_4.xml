<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR977">
				<head type="main">BALLADE DE LA T.S.F.</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="1.4">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg>t</w> <w n="1.7">m<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="363">en</seg></w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">V<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="2.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="2.4">l</w>’<w n="2.5">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="2.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="2.7">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gu<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.9">n<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="383">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ts</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.7">qu</w>’<w n="3.8">h<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="4" num="1.4"><w n="4.1">Gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.2">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="4.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="4.4">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="4.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>… <w n="4.6">Br<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w> !</l>
					<l n="5" num="1.5"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="5.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="5.7">gr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w>,</l>
					<l n="6" num="1.6"><w n="6.1">C</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="6.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.9">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.10">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="6.11">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="7" num="1.7"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">t</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="7.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <subst hand="RR" reason="analysis" type="phonemization"><del>T.S.F.</del><add rend="hidden"><w n="7.7">T<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>-<w n="7.8"><seg phoneme="ɛ" type="vs" value="1" rule="50">è</seg>S</w>-<w n="7.9"><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>f</w>.</add></subst></l>
					<l n="8" num="1.8"><w n="8.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.2">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="445">û</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="8.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="9.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="9.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.7">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="9.8">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.9">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ffr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="10" num="2.2"><w n="10.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.5">s</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.7">d</w>’<w n="10.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.9">vr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="10.10">ch<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w>,</l>
					<l n="11" num="2.3"><w n="11.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="11.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.3">l</w>’<w n="11.4"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rgu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.8">b<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="11.9"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="2.4"><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="12.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="12.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.8">sc<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.10">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>li<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w></l>
					<l n="13" num="2.5">(<w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gr<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch</w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.8">Pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>k<seg phoneme="o" type="vs" value="1" rule="444">o</seg>fi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ff</w>,)</l>
					<l n="14" num="2.6"><w n="14.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="14.5">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="14.6">cl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="14.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.9">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="15" num="2.7"><w n="15.1">L</w>’<w n="15.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.8">n<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w>.</l>
					<l n="16" num="2.8"><w n="16.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.2">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="445">û</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="16.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.2">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="17.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="17.4">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="17.5">fl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.7">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="18" num="3.2"><w n="18.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="18.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ppl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.3">h<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="476">ï</seg>s</w> <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="18.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="18.7">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.8">ch<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w>.</l>
					<l n="19" num="3.3"><w n="19.1">Tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="19.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="19.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="19.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="20" num="3.4"><w n="20.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.5">c<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="20.6">v<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.8">fi<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w>,</l>
					<l n="21" num="3.5"><w n="21.1">Tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="21.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ppr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.7">k<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w>,</l>
					<l n="22" num="3.6"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="22.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.3">n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rfs</w> <w n="22.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.5">n<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="22.6">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="22.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="22.8">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.9">qu</w>’<w n="22.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="22.12">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="23" num="3.7"><w n="23.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="23.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="23.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="23.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="23.8">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="23.9">br<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w> :</l>
					<l n="24" num="3.8">« <w n="24.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="24.2">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="445">û</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="24.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="24.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="24.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1"><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="25.2">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="25.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rb<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="25.4"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="25.5">qu</w>’<w n="25.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.7">r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="25.8"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="25.9">qu</w>’<w n="25.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.11">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="26" num="4.2"><w n="26.1">S</w>’<w n="26.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="26.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="26.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="26.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.7">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w></l>
					<l n="27" num="4.3"><w n="27.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.2">s<seg phoneme="o" type="vs" value="1" rule="435">o</seg>tt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="27.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="27.5">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>, <w n="27.6">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w> !</l>
					<l n="28" num="4.4">— <w n="28.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="28.2">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="445">û</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="28.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="28.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="28.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>