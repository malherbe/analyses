<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR779">
				<head type="main">L’Abeille écolière</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.2">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.4">mi<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="1.5">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="3.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">T<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="5" num="1.5"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="5.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="6.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="7" num="2.2"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="8" num="2.3"><w n="8.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="8.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="8.3">c</w>’<w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
					<l n="9" num="2.4"><space unit="char" quantity="8"></space><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w>,</l>
					<l n="10" num="2.5"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="10.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.6">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><w n="11.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.4">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="11.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>il</w>,</l>
					<l n="12" num="3.2"><space unit="char" quantity="8"></space><w n="12.1">F<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="13" num="3.3"><w n="13.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.3">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="3.4"><space unit="char" quantity="8"></space><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="15" num="3.5"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="15.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="a" type="vs" value="1" rule="307">a</seg>il</w>.</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="16.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.5">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.7">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="17" num="4.2"><space unit="char" quantity="8"></space><w n="17.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>,</l>
					<l n="18" num="4.3"><w n="18.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">s</w>’<w n="18.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>tr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>du<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w></l>
					<l n="19" num="4.4"><space unit="char" quantity="8"></space><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>du<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>,</l>
					<l n="20" num="4.5"><w n="20.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.2">d</w>’<w n="20.3"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="20.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="20.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.6">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.7">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1"><w n="21.1">Qu</w>’<w n="21.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="21.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="21.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>, <w n="21.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.6">g<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="21.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.8">mi<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w></l>
					<l n="22" num="5.2"><space unit="char" quantity="8"></space><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="22.2">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w> <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="23" num="5.3"><w n="23.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="23.2">l</w>’<w n="23.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.4">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="24" num="5.4"><space unit="char" quantity="8"></space><w n="24.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="24.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.4">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="25" num="5.5"><w n="25.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="25.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.3">t<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="25.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>.</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1"><w n="26.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.2">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="26.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="26.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.6">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="27" num="6.2"><space unit="char" quantity="8"></space><w n="27.1">Qu</w>’<w n="27.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="27.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="27.4">f<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					<l n="28" num="6.3"><w n="28.1">L</w>’<w n="28.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="28.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.5">l</w>’<w n="28.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>li<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="29" num="6.4"><space unit="char" quantity="8"></space><w n="29.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="29.2">pl<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="30" num="6.5"><w n="30.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="30.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="7">
					<l n="31" num="7.1">‒ <w n="31.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w>, <w n="31.2">gr<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <hi rend="ital"><w n="31.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="31.4">c<seg phoneme="e" type="vs" value="1" rule="300">ae</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="106">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></hi>…</l>
					<l n="32" num="7.2"><space unit="char" quantity="8"></space><w n="32.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="32.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="33" num="7.3"><w n="33.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="33.3"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>, <w n="33.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="33.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="34" num="7.4"><space unit="char" quantity="8"></space><w n="34.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="34.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="34.3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="34.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="34.5">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.6">gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="35" num="7.5"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="35.2">d</w>’<w n="35.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="35.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="35.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="35.6">mi<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="35.7">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="36" num="8.1"><w n="36.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="36.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="36.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> : « <w n="36.5">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="36.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="36.7">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> »</l>
					<l n="37" num="8.2"><space unit="char" quantity="8"></space><w n="37.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="37.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="37.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> :</l>
					<l n="38" num="8.3">« <w n="38.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="38.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="38.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="38.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="38.7">t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w></l>
					<l n="39" num="8.4"><space unit="char" quantity="8"></space><w n="39.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="39.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="39.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="39.5">g<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="40" num="8.5"><w n="40.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="40.2">mi<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="40.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="40.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="40.5">m</w>’<w n="40.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>d<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="40.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="40.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !»</l>
				</lg>
			</div></body></text></TEI>