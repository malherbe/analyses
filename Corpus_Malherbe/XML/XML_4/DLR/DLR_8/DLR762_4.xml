<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR762">
				<head type="main">Berceuse de la Grande Sœur</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1">D<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>, <w n="1.2">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="1.3">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> !</l>
					<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">J</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="2.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
					<l n="3" num="1.3"><w n="3.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.2">l</w>’<w n="3.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="3.6">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="4.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="4.6">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> ! <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="5.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="5.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.6">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="6" num="1.6"><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="6.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w></l>
					<l n="7" num="1.7"><space unit="char" quantity="4"></space>« <w n="7.1">D<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>, <w n="7.2">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="7.3">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> ! »</l>
				</lg>
				<lg n="2">
					<l n="8" num="2.1"><space unit="char" quantity="4"></space><w n="8.1">D<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>, <w n="8.2">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="8.3">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> !</l>
					<l n="9" num="2.2"><space unit="char" quantity="4"></space><w n="9.1">J</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="9.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
					<l n="10" num="2.3"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>-<w n="10.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ! <w n="10.3">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="10.5">t</w>’<w n="10.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="11" num="2.4"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="11.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="11.3">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="11.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="11.6">l</w>’<w n="11.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
					<l n="12" num="2.5"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="12.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="12.3">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.5">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="12.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="13" num="2.6"><w n="13.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> !… <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3">mi<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="13.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w>,</l>
					<l n="14" num="2.7"><space unit="char" quantity="4"></space><w n="14.1">D<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>, <w n="14.2">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="14.3">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="15" num="3.1"><space unit="char" quantity="4"></space><w n="15.1">D<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>, <w n="15.2">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="15.3">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> !</l>
					<l n="16" num="3.2"><space unit="char" quantity="4"></space><w n="16.1">J</w>’<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="16.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="16.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
					<l n="17" num="3.3"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="17.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="17.3">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="17.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="17.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.7">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="18" num="3.4"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="18.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="18.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.7">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>,</l>
					<l n="19" num="3.5"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="19.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="19.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="20" num="3.6"><w n="20.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> ! <w n="20.2">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="20.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="20.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="20.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
					<l n="21" num="3.7"><space unit="char" quantity="4"></space><w n="21.1">D<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>, <w n="21.2">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="21.3">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> !</l>
				</lg>
			</div></body></text></TEI>