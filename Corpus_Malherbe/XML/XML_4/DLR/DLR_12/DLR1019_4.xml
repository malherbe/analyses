<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Nos Secrètes Amours</title>
				<title type="sub">édition "Les Iles", 1951</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>754 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_12</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher></publisher>
						<idno type="URL">http://romanslesbiens.canalblog.com/archives/2007/11/10/6841446.html</idno>
					</publicationStmt>
					<sourceDesc>
						<p>édition "Les Iles", Paris, Imprimerie Nicolas — Niort, 1951, n° 14 sur 700 exemplaires. 48 pages.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
						<editor>texte établi et annoté par Mirande Lucien</editor>
						<edition>Deuxième tirage revu (première édition : 2008)</edition>
						<imprint>
							<pubPlace>Cassaniouze</pubPlace>
							<publisher>ErosOnyx</publisher>
							<date when="2018">2018</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1902" to="1905">1902-1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition électronique est a priori conforme avec l’édition de Natalie Barney de 1951.</p>
				<p>Une vérification avec un exemplaire imprimé ou numérisé de l’édition de 1951 ne serait pas superflue.</p>
				<p>Des vers ont été retirés pour avoir une édition conforme avec celle de 1951 selon les notes de l’éditeur de l’édition de 2008/2018 (poème : Contradiction).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Des retraits introduits automatiquement ont été supprimés conformément au document de l’édition source et selon l’édition imprimée de 2018.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
				<change when="2020-12-03" who="RR">Vérification du texte avec l’édition de 2018.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR1019">
				<head type="main">PORTRAIT</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="1.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="2.4">r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="2.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.7">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
					<l n="3" num="1.3"><w n="3.1">D</w>’<w n="3.2">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.3">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w>, <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.3">j<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="5.4">d</w>’<w n="5.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="6.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">r<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="7.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.5">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.6">s<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="8" num="2.4"><space unit="char" quantity="12"></space><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="9.2">r<seg phoneme="i" type="vs" value="1" rule="493">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.6">fl<seg phoneme="ɛ" type="vs" value="1" rule="355">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="10.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.5">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.8">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>,</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">Im</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.6">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
					<l n="12" num="3.4"><space unit="char" quantity="12"></space><w n="12.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="13.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="13.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="13.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="13.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.8">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="14" num="4.2"><w n="14.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="14.4">qu</w>’<w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="14.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
					<l n="15" num="4.3"><w n="15.1">V<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.2">d</w>’<w n="15.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="15.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="15.6">d</w>’<w n="15.7"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
					<l n="16" num="4.4"><space unit="char" quantity="12"></space><w n="16.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w> <w n="16.2">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="16.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.3">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="18" num="5.2"><w n="18.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="18.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="18.4">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.5">d</w>’<w n="18.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="19" num="5.3">— <w n="19.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.2">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>, <w n="19.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="19.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.10">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="20" num="5.4"><space unit="char" quantity="12"></space><w n="20.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
				</lg>
			</div></body></text></TEI>