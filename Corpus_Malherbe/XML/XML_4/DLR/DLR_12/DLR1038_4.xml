<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Nos Secrètes Amours</title>
				<title type="sub">édition "Les Iles", 1951</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>754 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_12</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher></publisher>
						<idno type="URL">http://romanslesbiens.canalblog.com/archives/2007/11/10/6841446.html</idno>
					</publicationStmt>
					<sourceDesc>
						<p>édition "Les Iles", Paris, Imprimerie Nicolas — Niort, 1951, n° 14 sur 700 exemplaires. 48 pages.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
						<editor>texte établi et annoté par Mirande Lucien</editor>
						<edition>Deuxième tirage revu (première édition : 2008)</edition>
						<imprint>
							<pubPlace>Cassaniouze</pubPlace>
							<publisher>ErosOnyx</publisher>
							<date when="2018">2018</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1902" to="1905">1902-1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition électronique est a priori conforme avec l’édition de Natalie Barney de 1951.</p>
				<p>Une vérification avec un exemplaire imprimé ou numérisé de l’édition de 1951 ne serait pas superflue.</p>
				<p>Des vers ont été retirés pour avoir une édition conforme avec celle de 1951 selon les notes de l’éditeur de l’édition de 2008/2018 (poème : Contradiction).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Des retraits introduits automatiquement ont été supprimés conformément au document de l’édition source et selon l’édition imprimée de 2018.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
				<change when="2020-12-03" who="RR">Vérification du texte avec l’édition de 2018.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR1038">
				<head type="main">APPEL</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.4">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="2.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="3" num="2.1"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">n</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="3.4">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="3.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="4" num="2.2"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="4.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="5" num="3.1"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="5.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.5">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="5.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.8"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="3.2"><space unit="char" quantity="8"></space><w n="6.1">N</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.4">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="7" num="4.1"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="7.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.5">t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.8"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.11">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w></l>
					<l n="8" num="4.2"><space unit="char" quantity="8"></space><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="8.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.4">cr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> : <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !… <w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
				</lg>
			</div></body></text></TEI>