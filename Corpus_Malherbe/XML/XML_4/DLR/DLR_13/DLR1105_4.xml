<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE IX</head><head type="main_part">Derniers Poèmes</head><head type="sub_part">INÉDITS</head><div type="poem" key="DLR1105">
					<head type="main">Distances</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.5">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.6">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>, <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="3.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.9">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.2">qu</w>’<w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.6">n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="5.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.10">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.3">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>du<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="7.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.5">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.8">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.5">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="9.2">d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">vi<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.9">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="10.4">qu</w>’<w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.5">qu</w>’<w n="11.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="12.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="12.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="13.2">Di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ! <w n="13.3">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="13.7">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="13.8">Pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>,</l>
						<l n="15" num="4.3"><w n="15.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="15.2">qu</w>’<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="15.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="15.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="15.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.9">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> !</l>
					</lg>
				</div></body></text></TEI>