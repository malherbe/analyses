<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE V</head><head type="main_part">Intimité</head><div type="poem" key="DLR1073">
					<head type="main">Ballade du Feu</head>
					<head type="sub_2">TEMPS PRÉSENTS, Paul Mourousy, 1939</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.5">th<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">dr<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.8">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
						<l n="3" num="1.3"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.2">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.5">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">l</w>’<w n="3.8"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="4.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
						<l n="5" num="1.5"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="5.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.7">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">m<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>,</l>
						<l n="6" num="1.6"><w n="6.1">C</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.7">t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="7" num="1.7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="7.3">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.4">c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="a" type="vs" value="1" rule="343">a</seg>ï<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="8" num="1.8"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="9.5">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="10" num="2.2"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="10.3">d</w>’<w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.5">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="10.7">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="11" num="2.3"><w n="11.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="11.2">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="2.4"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="12.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
						<l n="13" num="2.5"><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="13.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="13.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.5">qu</w>’<w n="13.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.7">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> !</l>
						<l n="14" num="2.6"><w n="14.1">D</w>’<w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="14.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.7">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="14.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.9">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w></l>
						<l n="15" num="2.7"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.2">b<seg phoneme="y" type="vs" value="1" rule="445">û</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="15.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="15.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>, <w n="15.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="16" num="2.8"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="16.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="16.3">s</w>’<w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.6">n<seg phoneme="ø" type="vs" value="1" rule="248">œu</seg>d</w></l>
						<l n="17" num="2.9"><w n="17.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">l</w>’<w n="17.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="18" num="3.1"><w n="18.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="19" num="3.2"><w n="19.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.2">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="19.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="20" num="3.3"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="20.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">cr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="20.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="21" num="3.4"><w n="21.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="21.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.4">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="21.5">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>br<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
						<l n="22" num="3.5"><w n="22.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>, <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.3">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ; <w n="22.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="22.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="22.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="22.7">pl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>.</l>
						<l n="23" num="3.6"><w n="23.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="23.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="24" num="3.7"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="24.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>, <w n="24.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="24.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.5">li<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="25" num="3.8"><w n="25.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="25.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="25.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.5">l</w>’<w n="25.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<head type="form">ENVOI</head>
						<l n="26" num="4.1"><w n="26.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> ! <w n="26.2">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="26.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="26.5">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.7">s</w>’<w n="26.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w></l>
						<l n="27" num="4.2"><w n="27.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="27.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.4">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="27.5">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="28" num="4.3"><w n="28.1">C</w>’<w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="28.3">qu</w>’<w n="28.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="28.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="28.7">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="28.8">d</w>’<w n="28.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="29" num="4.4"><w n="29.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="29.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="29.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.5">l</w>’<w n="29.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>