<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE III</head><head type="main_part">Chevauchées</head><div type="poem" key="DLR1063">
					<head type="main">Une Prière à Saint-Georges</head>
					<head type="sub_2">SOUFFLES DE TEMPÊTE, Fasquelle, 1918</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg></w>-<w n="1.2">j<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="1.3">t</w>’<w n="1.4">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="1.6">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="1.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="1.8">Ge<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="2.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="2.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="2.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="2.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.2">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.7">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="3.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.4">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="4.5">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.8">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>-<w n="5.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="5.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.6">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w> <w n="5.7">dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.8">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w></l>
						<l n="6" num="2.2"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.2">d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="6.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>str<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="7" num="2.3"><w n="7.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="7.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.3">l</w>’<w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.6">t<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.8">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="7.9">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.10">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="8" num="2.4"><w n="8.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="8.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="8.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="8.10">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.11">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>-<w n="9.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="9.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ph<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="10" num="3.2"><w n="10.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="10.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.6">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
						<l n="11" num="3.3"><w n="11.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.3">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="11.4">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="12.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="12.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="13.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="13.7">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="13.8">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.9">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="14" num="4.2"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="14.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="a" type="vs" value="1" rule="307">a</seg>il</w> <w n="14.3">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="14.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="14.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="15.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="15.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.8">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="16" num="4.4"><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.2">b<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="16.4">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="16.5">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="16.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.9">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="17.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="17.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="17.7">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>gu<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="17.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.9">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="18" num="5.2"><w n="18.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.3">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.4">fi<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="18.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="18.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.8">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="19.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu</w>’<w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.6">r<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="20" num="5.4"><w n="20.1">M<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="20.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.6">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="20.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.9">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="20.10">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>.</l>
					</lg>
				</div></body></text></TEI>