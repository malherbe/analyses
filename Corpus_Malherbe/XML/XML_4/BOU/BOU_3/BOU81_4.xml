<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU81">
				<head type="number">XIV</head>
				<head type="main">UNE SOIRÉE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w>-<w n="1.2">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> ! ‒ <w n="1.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.5">cr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> ?… <w n="1.6">c</w>’<w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.9">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> !… <w n="1.10">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.11"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.12">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.3">p<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.4">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="2.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.9">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="2.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>cl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301">ai</seg>t</w> <w n="3.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.8">c<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="4.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w> <w n="4.5">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.8">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.9">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.10"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w>.</l>
				</lg>
				<lg n="2">
					<l part="I" n="5" num="2.1"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="5.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.6">d</w>’<w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> ; </l>
					<l part="F" n="5" num="2.1"><w n="5.8"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.9">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1">M<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> " <w n="6.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">m<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ssi<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>rs</w> " <w n="6.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.8">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="6.9">z<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="7.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>br<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ts</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.9">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w>,</l>
					<l n="8" num="2.4"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.3">p<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="8.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w> <w n="8.5">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w>…</l>
					<l n="9" num="2.5"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="9.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rcl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="9.3">l</w>’<w n="9.4"><seg phoneme="œ" type="vs" value="1" rule="381">oe</seg>il</w> <w n="9.5">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="9.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.9">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="2.6"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.5">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="2.7"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>…</l>
					<l n="12" num="2.8"><w n="12.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="12.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.6">c<seg phoneme="œ" type="vs" value="1" rule="389">oeu</seg>r</w>, <w n="12.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.8">n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> !</l>
					<l n="13" num="2.9"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="13.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ‒ <w n="13.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="13.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ‒</l>
					<l n="14" num="2.10"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="14.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="14.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="15" num="2.11"><w n="15.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="15.2">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="15.3">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="15.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>lt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w> <w n="15.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.6">l</w>’<w n="15.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> !</l>
					<l n="16" num="2.12"><w n="16.1">C</w>’<w n="16.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="16.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> ! <w n="16.5">C</w>’<w n="16.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="16.8">vr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> ! <w n="16.9">C</w>’<w n="16.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.11">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="16.12">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> !</l>
					<l n="17" num="2.13"><w n="17.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.3"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="17.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="17.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.6">d</w>’<w n="17.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> ! « <w n="17.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> »</l>
					<l n="18" num="2.14"><w n="18.1">R<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> « <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.6">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="18.7">qu</w>’<w n="18.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.9">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>, »</l>
					<l n="19" num="2.15"><w n="19.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">j</w>’<w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.4">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="19.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.8">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.9">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.10">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>,</l>
					<l n="20" num="2.16"><w n="20.1">G<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="20.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.3">pl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.4">d</w>’<w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="20.7">d<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="20.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.10">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> !…</l>
					<l n="21" num="2.17"><w n="21.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="21.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="21.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="21.4">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> : « <w n="21.5">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !… »</l>
				</lg>
				<lg n="3">
					<l n="22" num="3.1"> ‒ <w n="22.1"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="22.2">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !… <w n="22.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="22.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="22.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.6">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="22.7">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="23" num="3.2"><w n="23.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="23.2">l</w>’<w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="23.4">b<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.6">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w> <w n="23.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.8">m<seg phoneme="œ" type="vs" value="1" rule="151">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>r</w> <w n="23.9">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>,</l>
					<l n="24" num="3.3"><w n="24.1">L</w>’<w n="24.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="24.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301">ai</seg>t</w>, <w n="24.4">l</w>’<w n="24.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="24.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="24.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>.</l>
					<l n="25" num="3.4"><w n="25.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.2">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="25.4">c<seg phoneme="œ" type="vs" value="1" rule="389">oeu</seg>r</w> <w n="25.5">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="25.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="26" num="3.5"><w n="26.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="26.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="26.3">l</w>’<w n="26.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ‒ <w n="26.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="26.6">l</w>’<w n="26.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.9">s<seg phoneme="o" type="vs" value="1" rule="435">o</seg>tt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="27" num="3.6"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="27.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="27.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.7">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
					<l n="28" num="3.7"><w n="28.1">Pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="28.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="28.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w>, <w n="28.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.6">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="28.7">d</w>’<w n="28.8"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>lh<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> !</l>
				</lg>
			</div></body></text></TEI>