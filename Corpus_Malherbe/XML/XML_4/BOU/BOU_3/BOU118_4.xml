<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU118">
				<head type="number">LI</head>
				<head type="main">AMOUR DOUBLE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>-<w n="1.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="2" num="1.2"> ‒ <w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.2">j</w>’<w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="2.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> ‒</l>
					<l n="3" num="1.3">« <w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="3.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="3.6">qu</w>’<w n="3.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.8"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.5">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> ! »</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.2">m</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="5.5">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !… <w n="5.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="6.3">d</w>’<w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.5">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="6.6">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> :</l>
					<l n="7" num="2.3"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="7.3">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.5">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">j</w>’<w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.6">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.5">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="10" num="3.2"><w n="10.1">M<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ;</l>
					<l n="11" num="3.3"><w n="11.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="11.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.3">l</w>’<w n="11.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.8">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="3.4"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="12.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="13.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ? <w n="13.4">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="13.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="14" num="4.2"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
					<l n="15" num="4.3"><w n="15.1">C</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.3">l</w>’<w n="15.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="16" num="4.4"><w n="16.1">C</w>’<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="16.3">l</w>’<w n="16.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.6">j</w>’<w n="16.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.8">mi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="17.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">c<seg phoneme="œ" type="vs" value="1" rule="389">oeu</seg>r</w> <w n="17.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.6">fl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="18" num="5.2"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">l</w>’<w n="18.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="18.5">l</w>’<w n="18.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.7">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="19" num="5.3"><w n="19.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.3">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="19.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="19.5">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="20" num="5.4"><w n="20.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="20.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"> ‒ <w n="21.1"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="21.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="21.5">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="22" num="6.2"><w n="22.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="22.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="22.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ?</l>
					<l n="23" num="6.3"><w n="23.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="23.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="23.3">d</w>’<w n="23.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="23.5"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="23.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="24" num="6.4"><w n="24.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.3">c<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="24.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="24.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.6">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> !…</l>
				</lg>
			</div></body></text></TEI>