<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">FENÊTRES SUR LA VIE</head><div type="poem" key="GUE19" rhyme="none">
					<head type="number">XIX</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="1.3">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="1.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">br<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.7">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.9">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">M<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rt</w> <w n="2.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.4">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="2.8">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.10">l</w>’<w n="2.11"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">l</w>’<w n="3.3">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.5">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d</w> <w n="3.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="3.7">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.9">v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.2">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="4.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="4.6">d</w>’<w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ci<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.8">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
						<l n="5" num="1.5"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="5.6">s</w>’<w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.8">t<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="6" num="1.6"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="6.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w></l>
						<l n="7" num="1.7"><w n="7.1">Fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.5">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.8">d</w>’<w n="7.9"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w></l>
						<l n="8" num="1.8"><w n="8.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt</w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="8.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="8.8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="8.9">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ct<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="9" num="1.9"><w n="9.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.5">bru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.10">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="10" num="1.10"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="10.8">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="11" num="1.11"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="11.5">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w> <w n="11.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="11.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="12" num="1.12"><w n="12.1">R<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="12.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="360">en</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">mi<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>.</l>
						<l n="13" num="1.13"><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="13.2">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="13.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="13.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.9">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w></l>
						<l n="14" num="1.14"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="14.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>,</l>
						<l n="15" num="1.15"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="15.5">fu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.8">l</w>’<w n="15.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="16" num="2.1"><w n="16.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.7">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>, <w n="16.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.9">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.10">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w></l>
						<l n="17" num="2.2"><w n="17.1">L</w>’<w n="17.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="17.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.4">l</w>’<w n="17.5"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.8">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.10">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.11">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! …</l>
						<l n="18" num="2.3"><w n="18.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="18.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="18.3">m</w>’<w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="18.5">d</w>’<w n="18.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.7">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="18.8">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>, <w n="18.9">d</w>’<w n="18.10"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.11">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w></l>
						<l n="19" num="2.4"><w n="19.1">S<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>, <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="19.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.6">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.9">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> :</l>
						<l n="20" num="2.5"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="20.2">F<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="20.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.4">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="20.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="20.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.8">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>