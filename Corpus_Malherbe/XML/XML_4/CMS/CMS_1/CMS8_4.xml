<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES SONNETS DU DOCTEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="CMS">
					<name>
						<forename>Georges</forename>
						<surname>CAMUSET</surname>
					</name>
					<date from="1840" to="1885">1840-1885</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>522 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CMS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/BIUSante_80646</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Sonnets du docteur</title>
								<author>Georges Camuset</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Chez la plupart des libraires</publisher>
									<date when=" 1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
						<idno type="URL">https ://www.google.fr/books/edition/Les_sonnets_du_docteur/bsv60_A0jdUC</idno>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Chez la plupart des libraires</publisher>
							<date when=" 1884">1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poémes absents de l’édition numérique initiale ("Les Préservatifs" et "Du Signe certain de la mort") ont été ajoutés à partir du texte disponible sur Google books.</p>
				<p>Les notes qui suivent les titres dans la table des matières ont été mis en fin de poème.</p>
				<p>La lettre manuscrite de Charles Monselet n’est pas incluse dans la présente édition</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CMS8">
				<head type="main">LE VER SOLITAIRE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">BI<seg phoneme="ɛ̃" type="vs" value="1" rule="375">EN</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">F<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ri<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t</w> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">Ph<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.3">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w>-<w n="2.4">S<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">P<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w> <w n="3.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.6">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="3.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">S<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="dc-1">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="451">u</seg>m</w> <w n="4.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.4">pr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="4.6">ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="5.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>, <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="o" type="vs" value="1" rule="444">o</seg>z<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="d-3">u</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.3">l</w>’<w n="6.4"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="6.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
					<l n="7" num="2.3"><w n="7.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r</w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
					<l n="8" num="2.4"><w n="8.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w>, <w n="8.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="8.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.4">Sc<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>x</w>, <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.7">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="8.8">S<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>, <w n="9.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="9.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.7">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
					<l n="10" num="3.2"><w n="10.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.3">b<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="10.4">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.5">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.7">l</w>’<w n="10.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
					<l n="11" num="3.3"><w n="11.1">L</w>’<w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.4">l</w>’<w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="11.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.8">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.9">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="11.10">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.11">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Ou<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="12.2">c</w>’<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="12.6">sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.8">l</w>’<w n="12.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.10">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="12.11">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="13" num="4.2"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="13.3">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="13.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="14" num="4.3"><w n="14.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="14.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>q<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.5">pi<seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="14.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="14.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.9">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="14.10">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
				</lg>
				<closer>
					<note type="footnote" id="">Le remède africain, le<hi rend="ital">kousso</hi>. d’Abyssinie, pays où le ver solitaire est très commun. —<hi rend="ital">Scolex</hi>, embryon du tænia.</note>
				</closer>
			</div></body></text></TEI>