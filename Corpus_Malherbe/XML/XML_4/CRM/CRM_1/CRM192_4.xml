<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM192">
				<head type="main">INQUIÉTUDE</head>
				<opener>
					<salute>A Jacques De Decker.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="1.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w></l>
					<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="2.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="3" num="1.3"><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w>-<w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.6">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ts</w>.</l>
					<l n="4" num="1.4"><w n="4.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="4.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">m<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w></l>
					<l n="5" num="1.5"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.3">c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.6">f<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="6" num="1.6"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.3">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="7.4">L<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w></l>
					<l n="8" num="2.2"><w n="8.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="9" num="2.3"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="9.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w></l>
					<l n="10" num="2.4"><w n="10.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.5">m<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="11" num="2.5"><w n="11.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="12" num="2.6"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="12.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="12.4">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.2">n</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="13.5">d</w>’<w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.8">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="14" num="3.2"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="14.3">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="14.4">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.5">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="14.6">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
					<l n="15" num="3.3"><w n="15.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.2">n</w>’<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="15.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="15.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="15.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="15.8">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>.</l>
					<l n="16" num="3.4"><w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="16.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w>, <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.7">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="17" num="3.5"><w n="17.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.2">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.4">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="17.5">d</w>’<w n="17.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.7">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="18" num="3.6"><w n="18.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="18.2">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="18.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.4"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="18.5">m<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.2">n</w>’<w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.4">qu</w>’<w n="19.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.6">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="20" num="4.2"><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.4">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="21" num="4.3"><w n="21.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.2">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="21.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="21.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="22" num="4.4"><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="22.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="22.4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="22.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.7">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="23" num="4.5"><w n="23.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.2">r<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="23.3">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="u" type="vs" value="1" rule="CRM192_1">ou</seg>w</w>, <w n="23.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.5">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="24" num="4.6"><w n="24.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="24.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="24.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="24.4">qu</w>’<w n="24.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.6">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="24.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.8">s<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="25.2">n</w>’<w n="25.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="25.4">gu<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="25.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="25.7">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>.</l>
					<l n="26" num="5.2"><w n="26.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="26.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="26.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="26.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="26.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="27" num="5.3"><w n="27.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>gn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="27.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="27.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
					<l n="28" num="5.4"><w n="28.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="28.2">n</w>’<w n="28.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="28.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="28.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="28.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>-<w n="28.7">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="29" num="5.5"><w n="29.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="29.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.4">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rph<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
					<l n="30" num="5.6"><w n="30.1">T<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="30.3">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="30.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="31.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="31.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="31.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="31.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="32" num="6.2"><w n="32.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="32.2">n</w>’<w n="32.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="32.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="32.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="32.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="32.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
					<l n="33" num="6.3"><w n="33.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.4"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.5">d</w>’<w n="33.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
					<l n="34" num="6.4"><w n="34.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="34.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="34.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="34.4">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="34.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="34.6">l</w>’<w n="34.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="35" num="6.5"><w n="35.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="35.2">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="35.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="35.4">L<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w></l>
					<l n="36" num="6.6"><w n="36.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="36.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="36.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="36.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="36.5">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.6">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="36.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>.</l>
				</lg>
			</div></body></text></TEI>