<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM43">
				<head type="main">ET C’EST TOUJOURS LE VENT…</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="1.2">c</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="1.7">j<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.9">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.10">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="2.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="2.8">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="2.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">en</seg>t</w>,</l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="3.3">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="3.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.5">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.10">pl<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.2">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="4.3">tr<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="4.4">d</w>’<w n="4.5"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">Br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">Br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.3">g<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.6">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="6.9">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="6.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.11">ru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
					<l n="7" num="2.3"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">Br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.7">d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.9">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="2.4"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.6">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="8.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.8">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>.</l>
				</lg>
			</div></body></text></TEI>