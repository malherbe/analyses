<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SÉRÉNADE DES SÉRÉNADES</head><div type="poem" key="CRB45">
					<head type="main">HEURES</head>
					<lg n="1">
						<l n="1" num="1.1"><space quantity="8" unit="char"></space><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>m<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>dr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">M<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.2"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.4">l</w>’<w n="2.5"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> !</l>
						<l n="3" num="1.3"><space quantity="8" unit="char"></space><w n="3.1">F<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="3.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.5">sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> !</l>
						<l n="4" num="1.4">— <w n="4.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">n</w>’<w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.9">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! —</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space quantity="8" unit="char"></space><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">f<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">P<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="2.2"><space quantity="8" unit="char"></space><w n="6.1">J</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="6.3">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.8">L<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="7" num="2.3"><space quantity="8" unit="char"></space><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="7.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.4">cr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>…</l>
						<l n="8" num="2.4"><w n="8.1">H<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ! <w n="8.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="8.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space quantity="8" unit="char"></space><w n="9.1">J</w>’<w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="9.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.5">bru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="10" num="3.2"><space quantity="8" unit="char"></space><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="10.5">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.7">m</w>’<w n="10.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="11" num="3.3"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">cr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.5">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="11.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> : <w n="11.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.8">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> … <w n="11.9">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.10">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><space quantity="8" unit="char"></space><w n="12.1">J</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="12.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rz<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="12.7">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>…</l>
						<l n="13" num="4.2"><space quantity="8" unit="char"></space><w n="13.1">L</w>’<w n="13.2">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> — <w n="13.6">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.7">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="14" num="4.3"><w n="14.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> !… <w n="14.3">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="14.5">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> — <w n="14.6">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>