<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SÉRÉNADE DES SÉRÉNADES</head><div type="poem" key="CRB49">
					<head type="main">PIÈCE À CARREAUX</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="1.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.3">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.6">T<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="2.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="a" type="vs" value="1" rule="307">a</seg>il</w> <w n="3.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="3.7">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>cl<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> !…</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="5.2">T<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">j</w>’<w n="5.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.6">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="2.2"><space quantity="8" unit="char"></space><w n="6.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="6.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> ;</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="7.2">T<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><space quantity="8" unit="char"></space><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.2">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="10" num="3.2"><space quantity="8" unit="char"></space><w n="10.1">V<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">cu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>rs</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">C<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="12" num="3.4"><space quantity="8" unit="char"></space><w n="12.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> :</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">— <w n="13.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> — <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="13.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.4">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> : <w n="13.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="4.2"><space quantity="8" unit="char"></space><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="14.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="14.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> !</l>
						<l n="15" num="4.3"><w n="15.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>-<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="15.4">b<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="15.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="4.4"><space quantity="8" unit="char"></space><w n="16.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="16.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="16.3">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="16.4">n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rfs</w> ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">— <w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="17.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> : <w n="17.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="5.2"><space quantity="8" unit="char"></space><w n="18.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="18.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="18.3">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="18.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>…</l>
						<l n="19" num="5.3"><w n="19.1">L</w>’<w n="19.2"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="19.4">gr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>, <w n="19.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="19.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.7">Squ<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="5.4"><space quantity="8" unit="char"></space><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="20.2">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="20.3">d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">— <w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="21.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> : <w n="21.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="21.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="22" num="6.2"><space quantity="8" unit="char"></space><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="22.2">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>bs</w> <w n="22.3">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> !…</l>
						<l n="23" num="6.3"><w n="23.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="23.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="23.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="23.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="23.6">vi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="24" num="6.4"><space quantity="8" unit="char"></space><w n="24.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="24.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> ?</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="25.2">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="25.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="25.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="26" num="7.2"><space quantity="8" unit="char"></space><w n="26.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="26.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !…</l>
						<l n="27" num="7.3"><w n="27.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="27.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="27.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="27.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="27.6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="27.7">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="28" num="7.4"><space quantity="8" unit="char"></space><w n="28.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.2">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="28.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ?</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">— <w n="29.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="29.2">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="29.4">gu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="30" num="8.2"><space quantity="8" unit="char"></space><w n="30.1">C</w>’<w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="30.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> !</l>
						<l n="31" num="8.3">— <w n="31.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="31.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.5">cu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="32" num="8.4"><space quantity="8" unit="char"></space><w n="32.1">C</w>’<w n="32.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="32.3">F<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">… <w n="33.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="33.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="33.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="34" num="9.2"><space quantity="8" unit="char"></space><w n="34.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="34.2">n<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w></l>
						<l n="35" num="9.3"><w n="35.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="35.2">n</w>’<w n="35.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>, <w n="35.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="35.5">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="35.6">Vi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="36" num="9.4"><space quantity="8" unit="char"></space><w n="36.1">Qu</w>’<w n="36.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="36.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="36.4">Z<seg phoneme="e" type="vs" value="1" rule="409">É</seg>R<seg phoneme="o" type="vs" value="1" rule="444">O</seg></w>.</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Cadix</placeName>. — Mai.
						</dateline>
					</closer>
				</div></body></text></TEI>