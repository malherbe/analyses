<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RONDELS POUR APRÈS</head><div type="poem" key="CRB95">
					<head type="main">SONNET POSTHUME</head>
					<lg n="1">
						<l n="1" num="1.1"><hi rend="ital"><w n="1.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> : <w n="1.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>… <w n="1.7">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.8">n</w>’<w n="1.9"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="1.10">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="1.11"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="1.12">n<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</hi></l>
						<l n="2" num="1.2">— <hi rend="ital"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="2.3">d<seg phoneme="i" type="vs" value="1" rule="467">î</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. — <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="2.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="2.7">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="2.9">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="2.10">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.11">f<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>.</hi></l>
						<l n="3" num="1.3"><hi rend="ital"><w n="3.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> : <w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.3">t</w>’<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> — <w n="3.6">L</w>’<w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.8">c</w>’<w n="3.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.10">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></hi> <w n="3.11">l</w>’<w n="3.12"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="4" num="1.4"><hi rend="ital"><w n="4.1">R<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> : <w n="4.2">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.8">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.9">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>…</hi></l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><hi rend="ital"><w n="5.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> : <w n="5.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.3">t</w>’<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="5.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.7">d</w>’<w n="5.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</hi></l>
						<l n="6" num="2.2"><hi rend="ital"><w n="6.1">Ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> !… <w n="6.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="6.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="6.6">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="6.8">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> ;</hi></l>
						<l n="7" num="2.3"><hi rend="ital"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.5">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w>, <w n="7.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="7.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.9">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</hi></l>
						<l n="8" num="2.4">— <hi rend="ital"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> — <w n="8.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.9">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</hi></l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><hi rend="ital"><w n="9.1">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></hi></l>
						<l n="10" num="3.2"><hi rend="ital"><w n="10.1">T</w>’<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> … <w n="10.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> : <w n="10.7">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.9">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.10">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.11">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>.</hi></l>
						<l n="11" num="3.3"><hi rend="ital"><w n="11.1">R<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> : <w n="11.2">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="11.4">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="11.5">t</w>’<w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="11.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.9">p<seg phoneme="wa" type="vs" value="1" rule="158">oê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</hi></l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><hi rend="ital"><w n="12.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="12.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.4">n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="12.5">d</w>’<w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.7">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="12.9">d</w>’<w n="12.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</hi></l>
						<l n="13" num="4.2"><hi rend="ital"><w n="13.1">D<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="13.2">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> !… <w n="13.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">tr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="13.8">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.10">m<seg phoneme="wa" type="vs" value="1" rule="192">oe</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></hi></l>
						<l n="14" num="4.3"><hi rend="ital"><w n="14.1">D</w>’<w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="14.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>-<w n="14.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>, <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="14.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w></hi>.</l>
					</lg>
				</div></body></text></TEI>