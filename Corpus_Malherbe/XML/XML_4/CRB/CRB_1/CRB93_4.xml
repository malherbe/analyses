<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GENS DE MER</head><div type="poem" key="CRB93">
					<head type="main">LE PHARE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ph<seg phoneme="e" type="vs" value="1" rule="250">œ</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.4">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l</w>, <w n="1.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">Dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="2.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>il</w> :</l>
						<l n="3" num="1.3"><w n="3.1">S</w>’<w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="3.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rgn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.6">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">Cl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">l</w>’<w n="4.4"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="5.2">Pr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">d</w>’<w n="5.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>,</l>
						<l n="6" num="2.2"><space quantity="8" unit="char"></space><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="6.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="7" num="2.3"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>…</l>
						<l n="8" num="2.4"><space quantity="8" unit="char"></space> — <w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="8.2">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="8.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.4">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="9.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.8">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><space quantity="8" unit="char"></space><w n="10.1">B<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.3">bl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Fi<seg phoneme="ɛ" type="vs" value="1" rule="va-7">e</seg>r</w> <w n="11.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.5">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="3.4"><space quantity="8" unit="char"></space><w n="12.1">Pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.3">r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">— <w n="13.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="13.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="13.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="13.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="4.2"><space quantity="8" unit="char"></space><w n="14.1">D</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>, <w n="14.3">d</w>’<w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
						<l n="15" num="4.3"><w n="15.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.3">s</w>’<w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.6">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="4.4"><space quantity="8" unit="char"></space><w n="16.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>…</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">— <w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="17.2">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="17.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>pi<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="17.6">n<seg phoneme="o" type="vs" value="1" rule="318">au</seg>fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="18" num="5.2"><space quantity="8" unit="char"></space><w n="18.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="18.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="19" num="5.3"><w n="19.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="19.4">cr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.5">l</w>’<w n="19.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="5.4"><space quantity="8" unit="char"></space><w n="20.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">R<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.6">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="22" num="6.2"><space quantity="8" unit="char"></space> — <w n="22.1">D<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="23" num="6.3"><w n="23.1">D</w>’<w n="23.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> — <w n="23.3"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="23.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="23.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="23.7">qu</w>’<w n="23.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="23.9">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="24" num="6.4"><space quantity="8" unit="char"></space><w n="24.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="24.2">pl<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> — <w n="24.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>. —</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">S<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="25.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="25.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.4">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> : <w n="25.5"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="25.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.7">br<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="26" num="7.2"><space quantity="8" unit="char"></space><w n="26.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="26.3">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
						<l n="27" num="7.3"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.2">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.3">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="27.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.6">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="28" num="7.4"><space quantity="8" unit="char"></space><w n="28.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="28.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="28.4"><seg phoneme="i" type="vs" value="1" rule="468">I</seg></w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">… <w n="29.1">L<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="29.2">g<seg phoneme="i" type="vs" value="1" rule="468">î</seg>t</w> <w n="29.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="29.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="30" num="8.2"><space quantity="8" unit="char"></space> — <w n="30.1">C</w>’<w n="30.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="30.3">l</w>’<w n="30.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> —</l>
						<l n="31" num="8.3"><w n="31.1">Vi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="31.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> (<w n="31.4">s<seg phoneme="ɛ" type="vs" value="1" rule="355">e</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.5">m<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>)</l>
						<l n="32" num="8.4"><space quantity="8" unit="char"></space> — <w n="32.1">C</w>’<w n="32.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="32.3">l</w>’<w n="32.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>. —</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="33.3">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>z<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="33.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="33.5">l</w>’<w n="33.6"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>-<w n="33.7">d<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w>-<w n="33.8">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="34" num="9.2"><space quantity="8" unit="char"></space><w n="34.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="34.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="34.3">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
						<l n="35" num="9.3"><w n="35.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="35.2">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.4">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="36" num="9.4"><space quantity="8" unit="char"></space><w n="36.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="36.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.3">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="37.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="37.3">ph<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ph<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="37.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="37.5">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?…</l>
						<l n="38" num="10.2"><space quantity="8" unit="char"></space> — <w n="38.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="38.2">n</w>’<w n="38.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="38.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="38.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> —</l>
						<l n="39" num="10.3"><w n="39.1">L<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="39.2"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="39.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="39.4">b<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?…</l>
						<l n="40" num="10.4"><space quantity="8" unit="char"></space> — <w n="40.1">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="40.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.3">v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="40.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> —</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="41.2">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="41.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="41.4">s</w>’<w n="41.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="41.6">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w></l>
						<l n="42" num="11.2"><space quantity="8" unit="char"></space><w n="42.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="42.2">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="43" num="11.3">— <w n="43.1">S</w>’<w n="43.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="43.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="43.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="43.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="43.6">qu</w>’<w n="43.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="43.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>…</l>
						<l n="44" num="11.4"><space quantity="8" unit="char"></space><w n="44.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="44.2">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . .</ab>
					<lg n="12">
						<l n="45" num="12.1">— <w n="45.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="45.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="45.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="45.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="45.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="45.6">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="46" num="12.2"><space quantity="8" unit="char"></space><w n="46.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="46.2">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !… — <w n="46.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>-<w n="46.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> ? —</l>
						<l n="47" num="12.3"><w n="47.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="47.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="47.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="47.4"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w>, <w n="47.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="47.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="47.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
						<l n="48" num="12.4"><space quantity="8" unit="char"></space> — <w n="48.1">R<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="48.2">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>. —</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="49.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ri<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="49.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="49.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="49.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="50" num="13.2"><space quantity="8" unit="char"></space><w n="50.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="50.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>…</l>
						<l n="51" num="13.3">— <w n="51.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="51.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> : <w n="51.3">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>-<w n="51.4">g<seg phoneme="i" type="vs" value="1" rule="468">î</seg>t</w>, <w n="51.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="51.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="51.7">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="52" num="13.4"><space quantity="8" unit="char"></space> — <w n="52.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="52.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> —</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="53.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="53.3">b<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="53.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="53.5">l</w>’<w n="53.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="54" num="14.2"><space quantity="8" unit="char"></space><w n="54.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="54.2">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="55" num="14.3"><w n="55.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="55.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <hi rend="ital"><w n="55.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="55.4">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w></hi> — <w n="55.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="55.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> —</l>
						<l n="56" num="14.4"><space quantity="8" unit="char"></space><w n="56.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="56.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">J<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="57.2">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> … <w n="57.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="57.4">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="58" num="15.2"><space quantity="8" unit="char"></space><w n="58.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="58.2">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="58.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="58.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
						<l n="59" num="15.3">— <w n="59.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="59.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> … <w n="59.3">l</w>’<w n="59.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>cti<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="59.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="59.6">ph<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="60" num="15.4"><space quantity="8" unit="char"></space><w n="60.1">N</w>’<w n="60.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="60.3">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="60.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> …</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Les Triagots.</placeName> — Mai.
						</dateline>
					</closer>
				</div></body></text></TEI>