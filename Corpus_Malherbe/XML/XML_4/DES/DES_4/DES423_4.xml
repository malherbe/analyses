<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ENFANTS ET JEUNES FILLES</head><div type="poem" key="DES423">
					<head type="main">LA GRANDE PETITE FILLE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> ! <w n="1.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="2.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="2.4">j</w>’<w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="2.6">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>q</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> !</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="3.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>, <w n="3.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="3.4">j</w>’<w n="3.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.6">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">S</w>’<w n="6.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="6.3">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
						<l n="7" num="2.3"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="7.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="7.3">j</w>’<w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="7.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> : « <w n="8.3">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ! »</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="9.2">c</w>’<w n="9.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="10" num="3.2"><w n="10.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> !</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="11.2">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="11.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="12" num="3.4"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="12.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>li<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.4">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="4.2"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="14.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.3">j</w>’<w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="15" num="4.3"><w n="15.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.2">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="15.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.4">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.7">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="4.4"><w n="16.1">B<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ! <w n="17.3">Qu</w>’<w n="17.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.6">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="5.2"><w n="18.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="18.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
						<l n="19" num="5.3"><w n="19.1">J</w>’<w n="19.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="19.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.5">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="20" num="5.4"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="21.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="21.4">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="22" num="6.2"><w n="22.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="22.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="22.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="22.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
						<l n="23" num="6.3"><w n="23.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ; <w n="23.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="24" num="6.4"><w n="24.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.5">m</w>’<w n="24.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="25.2">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="25.3">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="25.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.5">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="25.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="26" num="7.2"><w n="26.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>.</l>
						<l n="27" num="7.3"><w n="27.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="27.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w>, <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="27.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="28" num="7.4"><w n="28.1">S<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="28.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="28.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="28.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.5">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> ?</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ! <w n="29.2">c</w>’<w n="29.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="29.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="29.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="29.6">qu</w>’<w n="29.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="29.8">s</w>’<w n="29.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppu<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="30" num="8.2"><w n="30.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="30.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.3">pi<seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="30.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="30.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ;</l>
						<l n="31" num="8.3"><w n="31.1">C</w>’<w n="31.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="31.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="31.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="31.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nnu<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="32" num="8.4"><w n="32.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="32.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="32.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : « <w n="32.4">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="32.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.6">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ! »</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="33.2">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="33.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.4">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="34" num="9.2"><w n="34.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="34.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="34.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
						<l n="35" num="9.3"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="35.2">c</w>’<w n="35.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="35.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="35.5">d</w>’<w n="35.6"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="35.7">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.8">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="36" num="9.4"><w n="36.1">Pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="36.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.3">n</w>’<w n="36.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="36.5">qu</w>’<w n="36.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="36.7">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="37.2">j</w>’<w n="37.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="37.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="37.6">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="38" num="10.2"><w n="38.1">C</w>’<w n="38.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="38.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="38.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="38.5">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="38.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.7">cl<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="39" num="10.3"><w n="39.1">V<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>-<w n="39.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="39.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="39.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="40" num="10.4"><w n="40.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="40.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.3">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="40.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="40.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ?</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="41.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="41.3">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="41.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>-<w n="41.5">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="42" num="11.2"><w n="42.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="42.2">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="42.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="42.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="42.5">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="42.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ;</l>
						<l n="43" num="11.3"><w n="43.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="43.2">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="43.3">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="43.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="43.5">m</w>’<w n="43.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="44" num="11.4"><w n="44.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="44.2">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="44.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="44.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="44.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="45.2">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="45.3">l</w>’<w n="45.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="45.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="46" num="12.2"><w n="46.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="46.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="46.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="46.4">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="46.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>.</l>
						<l n="47" num="12.3"><w n="47.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="47.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="47.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="47.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="47.5">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="47.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="48" num="12.4"><w n="48.1">M<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> ! <w n="48.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="48.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="48.4">m</w>’<w n="48.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> !</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="49.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="49.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="49.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="49.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="49.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="49.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="50" num="13.2"><w n="50.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="50.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="50.3">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>, <w n="50.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="50.5">m</w>’<w n="50.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="50.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>.</l>
						<l n="51" num="13.3"><w n="51.1">L<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="51.2">j</w>’<w n="51.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="51.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="51.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="51.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="52" num="13.4"><w n="52.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : « <w n="52.2">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="52.3">m</w>’<w n="52.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ! » <w n="52.5">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="52.6">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> !</l>
					</lg>
				</div></body></text></TEI>