<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES138">
						<head type="main">CONTE <lb></lb>IMITÉ DE L’ARABE</head>
						<lg n="1">
							<l n="1" num="1.1"><space quantity="6" unit="char"></space><w n="1.1">C</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>. <w n="1.4">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="1.7">d</w>’<w n="1.8"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
							<l n="2" num="1.2"><space quantity="2" unit="char"></space><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="2.2">f<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="2.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="2.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="3" num="1.3">(<w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="3.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="3.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>f</w> <w n="3.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="3.8">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="3.9">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="4" num="1.4"><space quantity="6" unit="char"></space><w n="4.1">J</w>’<w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.3">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.4">qu</w>’<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.6">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>.)</l>
							<l n="5" num="1.5"><w n="5.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="5.3">s</w>’<w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="5.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="5.8">l</w>’<w n="5.9"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="6" num="1.6"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>-<w n="6.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="6.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="6.8">n<seg phoneme="o" type="vs" value="1" rule="318">au</seg>fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="7" num="1.7"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.2">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="7.5">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.6">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> ;</l>
							<l n="8" num="1.8"><w n="8.1">Pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.5">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.7">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>.</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="9.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="9.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="9.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.5">n</w>’<w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">l</w>’<w n="9.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="10" num="2.2"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="10.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="10.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="10.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.7">fru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="10.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
							<l n="11" num="2.3"><w n="11.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.4">r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.5">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="11.6">s</w>’<w n="11.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="11.8">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>ls</w> <w n="11.9"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.10">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.11">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
							<l n="12" num="2.4"><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.5">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="12.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="12.8">fu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w> <w n="12.9">l</w>’<w n="12.10"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="13" num="2.5"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.4">d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w>, <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="13.7">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d</w>, <w n="13.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.9">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="13.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
							<l n="14" num="2.6"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="14.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="14.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="14.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.8">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="14.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="15" num="2.7"><w n="15.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.2">cr<seg phoneme="wa" type="vs" value="1" rule="420">oî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.4">cr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.6">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303">aim</seg></w> <w n="15.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.9">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="16" num="2.8"><w n="16.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="16.6">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
							<l n="17" num="2.9"><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="17.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>gr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="17.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.9">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.10">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="18" num="2.10"><w n="18.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="18.3">pi<seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="18.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.5">r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="18.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.9">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> ;</l>
							<l n="19" num="2.11"><w n="19.1">L</w>’<w n="19.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="19.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="19.4">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="19.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="20" num="2.12"><w n="20.1">S</w>’<w n="20.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="20.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="20.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="20.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.7">s</w>’<w n="20.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
							<l n="21" num="2.13"><w n="21.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="21.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="21.5">tr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="21.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="21.7">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.8">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="21.9"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="21.10">s</w>’<w n="21.11"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="22" num="2.14"><w n="22.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="22.2">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="22.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="22.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.5">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="22.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="22.7">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.8">j<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.9"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="22.10">g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>.</l>
							<l n="23" num="2.15"><w n="23.1">D</w>’<w n="23.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="23.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.5">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="23.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.7">Pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="24" num="2.16"><w n="24.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w> <w n="24.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="24.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.5">b<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="24.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="24.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>, <w n="24.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.10">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="24.11">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> !</l>
							<l n="25" num="2.17"><space quantity="6" unit="char"></space><w n="25.1">Pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="25.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="25.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="25.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="25.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="26" num="2.18"><w n="26.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="26.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.3">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="26.4">m<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="26.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="26.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.7">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="26.8">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
							<l n="27" num="2.19"><w n="27.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="27.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="27.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="27.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="27.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="27.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.10">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ds</w>,</l>
							<l n="28" num="2.20"><w n="28.1"><seg phoneme="i" type="vs" value="1" rule="497">Y</seg></w> <w n="28.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="28.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="28.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.5">fru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w>,… <w n="28.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.7">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>,… <w n="28.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.9">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>…</l>
							<l n="29" num="2.21">« <w n="29.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.2">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> ! <w n="29.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="29.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>, <w n="29.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.6">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> ! <w n="29.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="29.8">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="29.9">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="29.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.11">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ! »</l>
							<l n="30" num="2.22"><w n="30.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="30.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.4">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>. <w n="30.5"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="30.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="30.7"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="30.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> !</l>
							<l n="31" num="2.23"><space quantity="2" unit="char"></space>« <w n="31.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="31.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="31.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>, <w n="31.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="31.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="31.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="31.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="32" num="2.24"><space quantity="6" unit="char"></space><w n="32.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="32.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.6">d<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> ! »</l>
						</lg>
					</div></body></text></TEI>