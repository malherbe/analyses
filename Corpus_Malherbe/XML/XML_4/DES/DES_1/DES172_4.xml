<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES172">
						<head type="main">UN BRUIT D’AUTREFOIS</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="1.2">bru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> ! <w n="1.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="1.4">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.5">bru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="1.6">s</w>’<w n="1.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.10">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
							<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="2.2"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="2.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="2.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> !</l>
							<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="3.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="3.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="3.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="4" num="1.4"><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">bru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="4.3">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="4.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.9">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="5.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.8">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.9">s</w>’<w n="5.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="6" num="2.2"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="6.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="6.3">qu</w>’<w n="6.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="6.5">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="6.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="6.8">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.9">t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.10">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.11">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
							<l n="7" num="2.3"><w n="7.1">Qu</w>’<w n="7.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="7.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="7.4">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.5">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="7.6">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="7.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.10">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="7.11">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="8" num="2.4"><w n="8.1">L<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ! <w n="8.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="8.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.6">m</w>’<w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> !</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">bru<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="9.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.5">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.6">l</w>’<w n="9.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.8">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.10">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="10" num="3.2"><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="10.2">s</w>’<w n="10.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bs<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.6">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w>, <w n="10.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.8">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">fu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="10.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.11">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>.</l>
							<l n="11" num="3.3"><w n="11.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.4">s</w>’<w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="11.8">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.9">s</w>’<w n="11.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="12" num="3.4"><w n="12.1">J<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.4">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="12.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.7">l</w>’<w n="12.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="13.5">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="13.7">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="14" num="4.2"><w n="14.1">P<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="14.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.3">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.4">l</w>’<w n="14.5"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>, <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.7">c</w>’<w n="14.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.9">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.11">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> !</l>
							<l n="15" num="4.3"><w n="15.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.2">Di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ! <w n="15.3">s</w>’<w n="15.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="15.5">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="15.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="15.8">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.10">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.11">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.12">t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="16" num="4.4"><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="16.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.5">l</w>’<w n="16.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="16.7">d</w>’<w n="16.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.9">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="17.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="17.3">qu</w>’<w n="17.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="17.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="17.6">s</w>’<w n="17.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.9">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="18" num="5.2"><w n="18.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="18.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="18.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="18.5">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>-<w n="18.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.7">s</w>’<w n="18.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ?</l>
							<l n="19" num="5.3"><w n="19.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">î</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="19.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="19.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
							<l n="20" num="5.4"><w n="20.1">H<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="20.2">l</w>’<w n="20.3"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="20.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> : <w n="20.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="20.7">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="20.8">fu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w> <w n="20.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="21.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> ! <w n="21.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.7">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.8">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.9">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="21.10">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>-<w n="21.11">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
							<l n="22" num="6.2"><w n="22.1">S<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="22.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="22.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>-<w n="22.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="22.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="22.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.8">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.9">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
							<l n="23" num="6.3"><w n="23.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="23.2">l</w>’<w n="23.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="23.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="23.8">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.10">j</w>’<w n="23.11"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
							<l n="24" num="6.4"><w n="24.1">J</w>’<w n="24.2"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="24.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="24.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.7">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="24.8">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.9">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>.</l>
						</lg>
					</div></body></text></TEI>