<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="FLO70">
					<head type="number">FABLE IV</head>
					<head type="main">L’Habit d’Arlequin</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.2">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.5">n<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.8">F<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>, <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.8">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.11">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> :</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="3.5">c</w>’<w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.9">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.10">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="4.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>, <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.7">j</w>’<w n="4.8"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bs<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.9">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.10">m<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w>.</l>
						<l n="5" num="1.5"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="5.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>-<w n="5.5">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.6">j</w>’<w n="5.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.10">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">D</w>’<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="7.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="7.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="7.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="1.8"><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="8.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="8.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="8.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="8.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="8.8">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="9.7">d</w>’<w n="9.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.9">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.10">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="1.10"><w n="10.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.6">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.8">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="11" num="1.11"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppl<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="11.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="11.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="11.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.9">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space><w n="12.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="12.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="12.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="13" num="1.13"><w n="13.1">Tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="13.2"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="13.6">pl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space><w n="14.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="14.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>, <w n="14.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
						<l n="15" num="1.15"><space unit="char" quantity="8"></space><w n="15.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="15.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.3">l</w>’<w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
						<l n="16" num="1.16"><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> : <w n="16.4">J</w>’<w n="16.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="16.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="17" num="1.17"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="17.5">n</w>’<w n="17.6"><seg phoneme="y" type="vs" value="1" rule="391">eu</seg>t</w> <w n="17.7">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> ;</l>
						<l n="18" num="1.18"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="18.3">d</w>’<w n="18.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="18.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> ! <w n="18.8">V<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> ! <w n="18.9">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="18.10">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.11">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> :</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="19.2">n</w>’<w n="19.3"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="19.4">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="19.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="19.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="19.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.8">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">L</w>’<w n="20.2">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> ;</l>
						<l n="21" num="1.21"><space unit="char" quantity="8"></space><w n="21.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="21.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
						<l n="22" num="1.22"><space unit="char" quantity="8"></space><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="22.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="22.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>-<w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="22.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="23" num="1.23"><w n="23.1">R<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="23.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="23.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.5">n</w>’<w n="23.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="23.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="23.8">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="24.2">l</w>’<w n="24.3">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="24.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="24.5">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.6">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
						<l n="25" num="1.25"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="25.2">c</w>’<w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="25.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.5">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="25.6">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="25.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="25.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="25.9">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="25.10">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.11">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="26" num="1.26">— <w n="26.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="26.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>. — <w n="26.4"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="26.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="26.6">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>. — <w n="26.7"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="26.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="26.9">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="26.10">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rbl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> !</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"></space><w n="27.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt</w> <w n="27.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="27.4">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ;</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"></space><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="28.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="28.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.4">tr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="28.5">s</w>’<w n="28.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="29" num="1.29"><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="29.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="29.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="29.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="29.5">cr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="29.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="29.7">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.8">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> ;</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space><w n="30.1">L</w>’<w n="30.2">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="30.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="30.4">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="30.5">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="30.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>.</l>
						<l n="31" num="1.31"><w n="31.1">C<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="31.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rpr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="31.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="31.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="31.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="31.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.8">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="32" num="1.32"><w n="32.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="32.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="32.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="32.6">d</w>’<w n="32.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="32.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="32.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.10">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
						<l n="33" num="1.33"><w n="33.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="33.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="33.3">d</w>’<w n="33.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="33.5">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="33.6">c<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="33.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="33.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="34" num="1.34"><space unit="char" quantity="8"></space><w n="34.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="34.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="34.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.5">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="34.6"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="34.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w></l>
						<l n="35" num="1.35"><space unit="char" quantity="8"></space><w n="35.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="35.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="35.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="35.6">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="35.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>