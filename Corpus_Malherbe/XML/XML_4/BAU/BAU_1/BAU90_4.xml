<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU90">
					<head type="number">LXXXVI</head>
					<head type="main">L’Héautontimorouménos</head>
					<opener><salute>À J. G. F.</salute></opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.5">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.3">h<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="2.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">M<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> !</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.6">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>pi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.4">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">J<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="7" num="2.3"><w n="7.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="7.3">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.4">d</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="8.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="8.5">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.3">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.5">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="10.5">qu</w>’<w n="10.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="10.7">s<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w></l>
						<l n="11" num="3.3"><w n="11.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="11.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="11.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w></l>
						<l n="12" num="3.4"><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.7">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>-<w n="13.3">j<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.6">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w></l>
						<l n="14" num="4.2"><w n="14.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494">ym</seg>ph<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="15" num="4.3"><w n="15.1">Gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.4">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.5"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="4.4"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.6">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>, <w n="17.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.7">cr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="18" num="5.2"><w n="18.1">C</w>’<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="18.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="18.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w>, <w n="18.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.7">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.8">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> !</l>
						<l n="19" num="5.3"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="19.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w></l>
						<l n="20" num="5.4"><w n="20.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="20.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.3">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> !</l>
						<l n="22" num="6.2"><w n="22.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="22.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="22.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.7">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="23" num="6.3"><w n="23.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="23.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.7">r<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="24" num="6.4"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.2">su<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="25.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="25.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="26" num="7.2">— <w n="26.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="26.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="26.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="27" num="7.3"><w n="27.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="27.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="27.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
						<l n="28" num="7.4"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="28.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="28.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.4">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="28.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="28.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>