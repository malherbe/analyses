<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU58">
					<head type="number">LIV</head>
					<head type="main">Le Chat</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.2">qu</w>’<w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
							<l n="3" num="1.3"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="3.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="3.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>, <w n="3.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="3.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.7">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
							<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="4.3">mi<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="4.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.5">l</w>’<w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.8">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.3">t<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> ;</l>
							<l n="6" num="2.2"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="6.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="6.5">s</w>’<w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.7"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="6.8">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="7" num="2.3"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="7.4">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.6">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="8" num="2.4"><w n="8.1">C</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.8">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>, <w n="9.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ltr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="10" num="3.2"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="10.6">t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>br<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
							<l n="11" num="3.3"><w n="11.1">M<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="11.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="11.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
							<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="12.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.6">ph<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ltr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="13.5">cr<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="13.6">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w></l>
							<l n="14" num="4.2"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="14.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
							<l n="15" num="4.3"><w n="15.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="15.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.6">phr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="16" num="4.4"><w n="16.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.2">n</w>’<w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="16.5">b<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="16.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.7">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="17.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="17.3">n</w>’<w n="17.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="17.6">d</w>’<w n="17.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="17.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.9">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="18" num="5.2"><w n="18.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="18.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>str<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>,</l>
							<l n="19" num="5.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="19.4">r<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
							<l n="20" num="5.4"><w n="20.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="20.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="20.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>, <w n="21.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="21.5">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
							<l n="22" num="6.2"><w n="22.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="22.2">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ph<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="22.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="22.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="23" num="6.3"><w n="23.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="23.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>, <w n="23.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="23.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="24" num="6.4"><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="24.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="24.3">qu</w>’<w n="24.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="25" num="1.1"><w n="25.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.3">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.6">br<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="26" num="1.2"><w n="26.1">S<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="26.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w> <w n="26.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="26.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>, <w n="26.6">qu</w>’<w n="26.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.8">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w></l>
							<l n="27" num="1.3"><w n="27.1">J</w>’<w n="27.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="27.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="27.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="27.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="27.6">l</w>’<w n="27.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w></l>
							<l n="28" num="1.4"><w n="28.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="28.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.3">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="28.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="28.5">qu</w>’<w n="28.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="29" num="2.1"><w n="29.1">C</w>’<w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="29.3">l</w>’<w n="29.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="29.5">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>li<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="29.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="29.7">li<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ;</l>
							<l n="30" num="2.2"><w n="30.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="30.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="30.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="30.4">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="30.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="30.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="31" num="2.3"><w n="31.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="31.2">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="31.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="31.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="31.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="32" num="2.4"><w n="32.1">P<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="32.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="32.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="32.5">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="32.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="32.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="32.8">di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
						</lg>
						<lg n="3">
							<l n="33" num="3.1"><w n="33.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="33.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.3">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="33.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="33.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="33.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.8">j</w>’<w n="33.9"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="34" num="3.2"><w n="34.1">T<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="34.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="34.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="34.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
							<l n="35" num="3.3"><w n="35.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="35.3">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
							<l n="36" num="3.4"><w n="36.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="36.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="36.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="36.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>-<w n="36.7">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						</lg>
						<lg n="4">
							<l n="37" num="4.1"><w n="37.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="37.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="37.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
							<l n="38" num="4.2"><w n="38.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.2">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="38.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="38.5">pr<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="38.6">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="39" num="4.3"><w n="39.1">Cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w> <w n="39.2">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>, <w n="39.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="39.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="40" num="4.4"><w n="40.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="40.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="40.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.</l>
						</lg>
					</div>
				</div></body></text></TEI>