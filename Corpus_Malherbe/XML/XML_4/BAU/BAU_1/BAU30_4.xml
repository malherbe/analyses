<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU30">
					<head type="number">XXIX</head>
					<head type="main">Le Serpent qui danse</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">j</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="1.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="2.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="2.2"><space quantity="8" unit="char"></space><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg>s</w>,</l>
						<l n="7" num="2.3"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><space quantity="8" unit="char"></space><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="8.2">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="8.3">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.5">br<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg>s</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.3">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.5">s</w>’<w n="9.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="10" num="3.2"><space quantity="8" unit="char"></space><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="10.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="10.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="3.4"><space quantity="8" unit="char"></space><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="12.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.2">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="13.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="13.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="13.5">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.7">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="4.2"><space quantity="8" unit="char"></space><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="14.3">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>,</l>
						<l n="15" num="4.3"><w n="15.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="15.2">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="15.3">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="15.4">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ds</w> <w n="15.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="15.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="4.4"><space quantity="8" unit="char"></space><w n="16.1">L</w>’<w n="16.2"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="16.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="17.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="17.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="5.2"><space quantity="8" unit="char"></space><w n="18.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.2">d</w>’<w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="19.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="19.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="19.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="5.4"><space quantity="8" unit="char"></space><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="20.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="20.3">d</w>’<w n="20.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.5">b<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="21.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="22" num="6.2"><space quantity="8" unit="char"></space><w n="22.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.2">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.3">d</w>’<w n="22.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="23" num="6.3"><w n="23.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="23.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="24" num="6.4"><space quantity="8" unit="char"></space><w n="24.1">D</w>’<w n="24.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="25.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="25.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.7">s</w>’<w n="25.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="26" num="7.2"><space quantity="8" unit="char"></space><w n="26.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="26.4">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w></l>
						<l n="27" num="7.3"><w n="27.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="27.2">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.3">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="27.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="27.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="27.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="27.7">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="28" num="7.4"><space quantity="8" unit="char"></space><w n="28.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rgu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="28.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.4">l</w>’<w n="28.5"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="29.3">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="29.4">gr<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="29.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="29.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="30" num="8.2"><space quantity="8" unit="char"></space><w n="30.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.2">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ci<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="30.3">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="31.2">l</w>’<w n="31.3"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="31.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="32" num="8.4"><space quantity="8" unit="char"></space><w n="32.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="32.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="32.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>,</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.2">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="33.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="33.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="33.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.7">B<seg phoneme="o" type="vs" value="1" rule="444">o</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="34" num="9.2"><space quantity="8" unit="char"></space><w n="34.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="34.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="34.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
						<l n="35" num="9.3"><w n="35.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="35.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="35.3">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="35.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="36" num="9.4"><space quantity="8" unit="char"></space><w n="36.1">D</w>’<w n="36.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="36.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="36.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> !</l>
					</lg>
				</div></body></text></TEI>