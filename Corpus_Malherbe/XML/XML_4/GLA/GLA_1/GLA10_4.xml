<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Fer rouge</title>
				<title type="medium">Édition électronique</title>
				<author key="GLA">
					<name>
						<forename>Albert</forename>
						<surname>GLATIGNY</surname>
					</name>
					<date from="1839" to="1873">1839-1873</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Fer rouge</title>
						<author>Albert Glatigny</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L928</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Fer rouge</title>
								<author>Albert Glatigny</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54519653</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Poulet-Malassis</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>Les majuscules en début de vers ont été restituées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GLA10">
				<head type="number">X</head>
				<head type="main">DANS CES MURS OÙ L’ÉCHO</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.3">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="1.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="1.7">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.9">h<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w></l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">J<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
					<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.2">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> : « <w n="3.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, » <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">B<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="4.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="4.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.5"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.9">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.4">pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="dc-1">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.7">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ;</l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="6.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xpr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">qu</w>’<w n="6.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.9">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="7.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.4">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="7.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> ; <w n="7.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="7.8">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="8" num="1.8"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>d</w>, <w n="8.2">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.4">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.7">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.9">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="9" num="1.9"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="9.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : « <w n="9.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.4">mi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ! <w n="9.5">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.8">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="9.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>. »</l>
					<l n="10" num="1.10"><w n="10.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="10.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.7">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.8">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>,</l>
					<l n="11" num="1.11"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>. <w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="11.4">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="11.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.9">h<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="1.12"><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w>. <w n="12.4">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="12.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.7">l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="13" num="1.13"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3">bl<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> : « <w n="13.4">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> ! <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.7">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>,</l>
					<l n="14" num="1.14"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="14.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.6">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="14.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.8">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
					<l part="I" n="15" num="1.15"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.4">ch<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="i" type="vs" value="1" rule="468">î</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>, <w n="15.5">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.6">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! » </l>
					<l part="F" n="15" num="1.15"><w n="15.7"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="15.8">d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="1.16"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ! <w n="16.2">C<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="16.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="16.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.5">n</w>’<w n="16.6"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="16.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="16.8">qu</w>’<w n="16.9"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.10">dr<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="17" num="1.17"><w n="17.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.2">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="17.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="17.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="17.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.7">pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="dc-1">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> ?</l>
					<l n="18" num="1.18"><w n="18.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>. <w n="18.5">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.6">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="18.7">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>,</l>
					<l n="19" num="1.19"><w n="19.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="19.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">Tr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pm<seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn</w> : <w n="19.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.7">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="20" num="1.20"><w n="20.1">R<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.3">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="20.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="20.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.7">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="20.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.9">l</w>’<w n="20.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="21" num="1.21"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="21.2">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.3">t</w>’<w n="21.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="21.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="21.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="21.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.8">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
					<l n="22" num="1.22"><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.4">J<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>z<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="22.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="22.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="22.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.8">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg>th<seg phoneme="o" type="vs" value="1" rule="GLA10_1">au</seg>w<seg phoneme="ɛ" type="vs" value="1" rule="GLA10_2">e</seg>rs</w> !</l>
				</lg>
				<closer>
					<dateline> 4 octobre.</dateline>
				</closer>
			</div></body></text></TEI>