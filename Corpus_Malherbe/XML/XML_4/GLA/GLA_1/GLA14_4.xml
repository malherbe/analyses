<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Fer rouge</title>
				<title type="medium">Édition électronique</title>
				<author key="GLA">
					<name>
						<forename>Albert</forename>
						<surname>GLATIGNY</surname>
					</name>
					<date from="1839" to="1873">1839-1873</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Fer rouge</title>
						<author>Albert Glatigny</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L928</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Fer rouge</title>
								<author>Albert Glatigny</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54519653</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Poulet-Malassis</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>Les majuscules en début de vers ont été restituées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GLA14">
				<head type="number">XIV</head>
				<lg n="1">
					<l n="1" num="1.1">« <w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="1.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ! « <w n="1.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>-<w n="1.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w>. <w n="1.6">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
					<l n="2" num="1.2"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.2">qu</w>’<w n="2.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="2.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.5">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="2.6"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.7">l</w>’<w n="2.8">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.10">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="3.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="3.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.4">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>. <w n="3.6">B<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.7">br<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
					<l n="4" num="1.4"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="4.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.6">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="4.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.8">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.9">cr<seg phoneme="y" type="vs" value="1" rule="445">û</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="5" num="1.5"><w n="5.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="5.3">s</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.8">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.</l>
					<l n="6" num="1.6"><w n="6.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.2">d</w>’<w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.4">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.7">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w></l>
					<l n="7" num="1.7"><w n="7.1">P<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="7.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="7.5">cr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.4">c<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="8.5">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="8.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.10">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="9" num="1.9"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
					<l n="10" num="1.10"><w n="10.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.8">l<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
					<l n="11" num="1.11"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="11.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="11.4">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="11.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.8"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="1.12"><w n="12.1">Qu</w>’<w n="12.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.3">pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.6">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="12.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="12.9">m<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="ɛ̃" type="vs" value="1" rule="363">en</seg>s</w> <w n="12.10">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="12.11"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="13" num="1.13"><w n="13.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="13.2">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="13.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ts</w> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.9">f<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ;</l>
					<l n="14" num="1.14"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="14.2">s</w>’<w n="14.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="14.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="14.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pl<seg phoneme="wa" type="vs" value="1" rule="440">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="15" num="1.15"><w n="15.1">L</w>’<w n="15.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="15.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg>y<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.5">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="15.6">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
					<l n="16" num="1.16"><w n="16.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="16.6">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ds</w> <w n="16.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.8">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="17" num="1.17"><w n="17.1">Qu</w>’<w n="17.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="17.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="17.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.5">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="17.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.8">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.9">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>.</l>
					<l n="18" num="1.18"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="18.2">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="18.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>, <w n="18.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>,</l>
					<l n="19" num="1.19"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="19.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.4">cr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pi<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>, <w n="19.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.6">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="19.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.9">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>vi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="20" num="1.20"><w n="20.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.2">pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ss<seg phoneme="i" type="vs" value="1" rule="dc-1">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="20.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="20.4">l</w>’<w n="20.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.7">l</w>’<w n="20.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="21" num="1.21"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="21.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="21.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w>, <w n="21.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.7">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="21.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="21.9">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>-<w n="21.10">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c</w></l>
					<l n="22" num="1.22"><w n="22.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c</w>,</l>
					<l n="23" num="1.23"><w n="23.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="23.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="23.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="23.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="23.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="24" num="1.24"><w n="24.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.2">t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="24.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.4">r<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="24.5">t<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.8">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<closer>
					<dateline> 7 octobre.</dateline>
				</closer>
			</div></body></text></TEI>