<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOUREUSES</title>
				<title type="sub">POEMES ET FANTAISIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DAU">
					<name>
						<forename>Alphonse</forename>
						<surname>DAUDET</surname>
					</name>
					<date from="1840" to="1897">1840-1897</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES AMOUREUSES, POEMES ET FANTAISIES</title>
						<author>Alphonse Daudet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Amoureuses</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title type="main">LES AMOUREUSES, POEMES ET FANTAISIES</title>
								<author>Alphonse Daudet</author>
								<edition>NOUVELLE ÉDITION</edition>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BIBLIOTHÈQUE-CHARPENTIER, Eugène Fasquelle, éditeur</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1858">1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties en prose ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Le recueil a été découpé en parties et sous parties conformément à l’édition de 1882.</p>
				<correction>
					<p>Les majuscules des noms propres ont été restituées</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-12-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOUREUSES</head><div type="poem" key="DAU4">
					<head type="main">TROIS JOURS DE VENDANGES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">l</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">pi<seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="2.7">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
						<l n="3" num="1.3"><w n="3.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">gu<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> :</l>
						<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="4.3">d</w>’<w n="4.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.8">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.9">d</w>’<w n="4.10"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="5.3">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.4">d</w>’<w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="5.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="6.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="6.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>ps</w> <w n="6.7">d</w>’<w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="7" num="2.3"><space unit="char" quantity="10"></space><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="7.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<ab type="dot">⁂</ab>
					<lg n="3">
						<l n="8" num="3.1"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="8.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="9" num="3.2"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="9.8">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ;</l>
						<l n="10" num="3.3"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.5">d</w>’<w n="10.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="10.8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="11" num="3.4"><w n="11.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="11.3">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="11.4">d</w>’<w n="11.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.6">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="13" num="4.2"><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="13.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="13.6">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w>,</l>
						<l n="14" num="4.3"><space unit="char" quantity="10"></space><w n="14.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="14.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<ab type="dot">⁂</ab>
					<lg n="5">
						<l n="15" num="5.1"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">l</w>’<w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="15.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="15.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="5.2"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">j</w>’<w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="16.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.9">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>.</l>
						<ab type="dot">. . . . . . . . . . . . . . . . . . . . .</ab>
						<l n="17" num="5.3"><w n="17.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>il</w> <w n="17.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="17.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.6">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
						<l n="18" num="5.4"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p</w> <w n="18.3">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.7">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="19" num="6.1"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.2">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="19.3">d</w>’<w n="19.4"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.5">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="19.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="19.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>…</l>
						<l n="20" num="6.2"><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="20.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.6">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> ; <w n="20.7">l</w>’<w n="20.8"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
						<l n="21" num="6.3"><space unit="char" quantity="10"></space><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="21.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>