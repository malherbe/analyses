<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOUREUSES</title>
				<title type="sub">POEMES ET FANTAISIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DAU">
					<name>
						<forename>Alphonse</forename>
						<surname>DAUDET</surname>
					</name>
					<date from="1840" to="1897">1840-1897</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES AMOUREUSES, POEMES ET FANTAISIES</title>
						<author>Alphonse Daudet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Amoureuses</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title type="main">LES AMOUREUSES, POEMES ET FANTAISIES</title>
								<author>Alphonse Daudet</author>
								<edition>NOUVELLE ÉDITION</edition>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BIBLIOTHÈQUE-CHARPENTIER, Eugène Fasquelle, éditeur</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1858">1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties en prose ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Le recueil a été découpé en parties et sous parties conformément à l’édition de 1882.</p>
				<correction>
					<p>Les majuscules des noms propres ont été restituées</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-12-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOUREUSES</head><div type="poem" key="DAU18">
					<head type="main">DERNIÈRE AMOUREUSE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="1.2">l</w>’<w n="1.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">d</w>’<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="1.6">l</w>’<w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
						<l n="2" num="1.2"><w n="2.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.2">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="2.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="2.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t</w> <w n="2.7">s</w>’<w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257">eoi</seg>r</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">S</w>’<w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257">eoi</seg>r</w>, <w n="3.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="3.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="4.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">s</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>.</l>
						<l n="5" num="2.2"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.3">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.4">cl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.6">d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></l>
						<l n="6" num="2.3"><w n="6.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="6.3"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="6.4">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.7">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1"><w n="7.1">Pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="7.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">l</w>’<w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.6">l</w>’<w n="7.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="8" num="3.2"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
						<l n="9" num="3.3"><space unit="char" quantity="4"></space><w n="9.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1">« <w n="10.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="10.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ctr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.6">m</w>’<w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="11" num="4.2">« <w n="11.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="11.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="11.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="11.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w></l>
						<l n="12" num="4.3">« <w n="12.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1">« <w n="13.1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> ; <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="13.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>-<w n="13.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
						<l n="14" num="5.2">« <w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="14.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="15" num="5.3">« <w n="15.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.2">dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ps</w> <w n="15.3">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ds</w> <w n="15.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.6">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1">« <w n="16.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.3">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="16.4">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> :</l>
						<l n="17" num="6.2">« <w n="17.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="17.4">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="17.5">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="17.6">d</w>’<w n="17.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="18" num="6.3"><space unit="char" quantity="4"></space>« <w n="18.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.3">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>. »</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">cr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="19.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="19.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
						<l n="20" num="7.2"><w n="20.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="20.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="20.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
						<l n="21" num="7.3"><w n="21.1">D</w>’<w n="21.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="21.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.5">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					</lg>
					<lg n="8">
						<l n="22" num="8.1"><w n="22.1">J</w>’<w n="22.2"><seg phoneme="y" type="vs" value="1" rule="391">eu</seg>s</w> <w n="22.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
						<l n="23" num="8.2"><w n="23.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="23.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="23.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="23.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.7">y<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> :</l>
						<l n="24" num="8.3">« <w n="24.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="24.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="24.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="24.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1">« <w n="25.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> ! <w n="25.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="25.5">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>j<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>.</l>
						<l n="26" num="9.2">« <w n="26.1">F<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="26.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="26.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="26.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="26.5">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d</w>,</l>
						<l n="27" num="9.3"><space unit="char" quantity="4"></space>« <w n="27.1">S<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>-<w n="27.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="27.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="10">
						<l n="28" num="10.1">« <w n="28.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w> <w n="28.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.5">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="29" num="10.2">« <w n="29.1">M<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="29.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.3">m</w>’<w n="29.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="29.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
						<l n="30" num="10.3">« <w n="30.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="30.2">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="30.3">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="11">
						<l n="31" num="11.1">« <w n="31.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="31.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="31.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="31.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
						<l n="32" num="11.2">« <w n="32.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="32.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="32.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.4">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="32.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="33" num="11.3">« <w n="33.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>. »</l>
					</lg>
					<lg n="12">
						<l n="34" num="12.1"><w n="34.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="34.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.4">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="34.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="34.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="34.7">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
						<l n="35" num="12.2"><w n="35.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.2">sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ctr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.4">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="35.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="35.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> :</l>
						<l n="36" num="12.3"><space unit="char" quantity="4"></space>« <w n="36.1">C</w>’<w n="36.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="36.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>… <w n="36.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.5">n</w>’<w n="36.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="36.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="36.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>… »</l>
					</lg>
					<lg n="13">
						<l n="37" num="13.1">« – <w n="37.1">C</w>’<w n="37.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="37.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="37.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="37.5">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> ! <w n="37.6"><seg phoneme="e" type="vs" value="1" rule="133">e</seg>h</w> <w n="37.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> ! <w n="37.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="37.9">mi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
						<l n="38" num="13.2">« <w n="38.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="38.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="38.4">v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ; <w n="38.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="38.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="38.7">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="39" num="13.3">« <w n="39.1">J</w>’<w n="39.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="39.3">b<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="39.4">d</w>’<w n="39.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="39.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="14">
						<l n="40" num="14.1">« <w n="40.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="40.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="40.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="40.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="40.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="40.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
						<l n="41" num="14.2">« <w n="41.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="41.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="41.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ; <w n="41.4">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="41.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
						<l n="42" num="14.3">« <w n="42.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="42.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="42.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="42.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! »</l>
					</lg>
					<lg n="15">
						<l n="43" num="15.1"><w n="43.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="43.2">l</w>’<w n="43.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="43.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="43.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="43.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w>,</l>
						<l n="44" num="15.2"><w n="44.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="44.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="44.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="44.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : « <w n="44.5">Ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="44.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="45" num="15.3"><space unit="char" quantity="4"></space>« <w n="45.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>, <w n="45.2">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="45.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="45.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="45.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
					</lg>
					<lg n="16">
						<l n="46" num="16.1">« <w n="46.1">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="46.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="46.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="46.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="46.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="46.6">m<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ;</l>
						<l n="47" num="16.2">« <w n="47.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="47.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="47.3">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="47.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="47.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="47.6"><seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
						<l n="48" num="16.3">« <w n="48.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="48.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="48.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="48.4">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="17">
						<l n="49" num="17.1">« <w n="49.1"><seg phoneme="a" type="vs" value="1" rule="307">A</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="49.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="49.3">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="49.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="49.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="50" num="17.2">« <w n="50.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="50.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="50.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="50.4">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="50.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="50.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="51" num="17.3">« <w n="51.1">D</w>’<w n="51.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="51.3">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mi<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="51.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="51.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="51.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="51.7">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="18">
						<l n="52" num="18.1">« <w n="52.1">S<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="52.2">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="52.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="52.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="52.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ;</l>
						<l n="53" num="18.2">« <w n="53.1">S<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="53.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="53.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="53.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="53.5">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
						<l n="54" num="18.3"><space unit="char" quantity="8"></space>« <w n="54.1">M<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r</w> <w n="54.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="54.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="54.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! »</l>
					</lg>
				</div></body></text></TEI>