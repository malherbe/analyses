<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA SAINT JEAN D’ÉTÉ</head><head type="main_subpart">SONNETS POUR UNE ANNÉE</head><div type="poem" key="HRV28">
				<head type="main">AVRIL</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">t</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.6">qu</w>’<w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
						<l n="2" num="1.2"><w n="2.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="3" num="1.3"><w n="3.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.2">m</w>’<w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.4">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.7">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="3.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.9">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="3.10">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.11">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="4.2">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="4.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.5">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.6">s</w>’<w n="4.7"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.9">t</w>’<w n="4.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.2">t</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="5.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="5.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>… <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.8">j</w>’<w n="5.9"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.10">cr<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.11">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>…</l>
						<l n="6" num="2.2"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="6.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.8">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="7" num="2.3"><w n="7.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="7.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="7.4">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="7.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="8.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>…</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J</w>’<w n="9.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="9.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="9.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="9.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.9">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="9.10">ci<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="9.11">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.12">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="10" num="3.2"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="10.4">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.7">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w>, <w n="10.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">bu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="10.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.11">P<seg phoneme="a" type="vs" value="1" rule="340">â</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="11" num="3.3"><w n="11.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>, <w n="11.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>, <w n="11.7">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="11.8">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.9">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>…</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="12.4">l</w>’<w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="12.7">qu</w>’<w n="12.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.10">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="12.11"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.12"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="13" num="4.2"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.3">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.7">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="4.3"><w n="14.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="14.2">j</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="14.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="14.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="14.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.8">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="14.9">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>…</l>
					</lg>
				</div></body></text></TEI>