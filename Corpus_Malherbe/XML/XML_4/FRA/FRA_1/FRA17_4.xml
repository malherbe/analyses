<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les poèmes dorés</title>
				<title type="medium">Une édition électronique</title>
				<author key="FRA">
					<name>
						<forename>Anatole</forename>
						<surname>FRANCE</surname>
					</name>
					<date from="1844" to="1924">1844-1924</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>727 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">FRA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Poèmes dorés</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesie-francaise.fr</publisher>
						<idno type="URL">http://www.poesie-francaise.fr/anatole-france-les-poemes-dores/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les poèmes dorés</title>
						<author>Anatole France</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>EDOUARD-JOSEPH, EDITEUR</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Remise en ordre des poèmes conforme à l’édition de référence.</p>
				<p>Les dates de fin de poème ont été ajoutées (édition : EDOUARD-JOSEPH, EDITEUR)</p>
				<p>Les deux sections manquantes du poème "Le Désir" ont été ajoutées (édition : EDOUARD-JOSEPH, EDITEUR)</p>
				<p>Nombreuses corrections de ponctuation</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>

				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRA17">
				<head type="main">La mort</head>
				<head type="form">Sonnet</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="1.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="1.6">j<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.9">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="2.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.8">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="3.2">ti<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="3.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.8">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.10">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w>,</l>
					<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.4">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>-<w n="4.8">m<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="5.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="5.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.9">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="6" num="2.2"><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="6.2">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.3">d</w>’<w n="6.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="6.5">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="6.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.8">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="6.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w>,</l>
					<l n="7" num="2.3"><w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="7.5">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="7.9">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="8" num="2.4"><w n="8.1">V<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.4">j<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="8.5">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c</w> <w n="8.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.9">m<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">b<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="9.4">d</w>’<w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.9">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.10">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w>,</l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="10.2">c</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.6">pl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="10.8">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.10">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> :</l>
					<l n="11" num="3.3"><w n="11.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>g<seg phoneme="ɥi" type="vs" value="1" rule="274">ui</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="11.4">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.9">di<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="11.10">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.11">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="12.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.7">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.8">s</w>’<w n="12.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					<l n="13" num="4.2"><w n="13.1">L<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="13.2">vi<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="13.4">l<seg phoneme="u" type="vs" value="1" rule="d-2">ou</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="13.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="4.3"><w n="14.1">Pu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.3">lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.8">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
				</lg>
			</div></body></text></TEI>