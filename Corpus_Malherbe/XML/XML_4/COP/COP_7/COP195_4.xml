<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE RELIQUAIRE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>421 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2019</date>
				<idno type="local">COP_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE RELIQUAIRE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/francoiscoppeelereliquaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>François Coppée</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie ALEXANDRE HOUSSIAUX</publisher>
									<date when="1885">1885</date>
								</imprint>
								<biblScope unit="tome">Poésie, tome 1</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1866">1866</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-05-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-05-25" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP195">
				<head type="main">LA TRÊVE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="2" num="1.2"><w n="2.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="2.3">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>. <w n="2.4">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					<l n="3" num="1.3"><w n="3.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="3.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">T<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="5.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>. <w n="5.3">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="6.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w></l>
					<l n="7" num="2.3"><w n="7.1">J<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="2.4"><w n="8.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.2">d</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>pi<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="10" num="3.2"><w n="10.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
					<l n="11" num="3.3"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="12" num="3.4"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.4">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="13.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="4.2"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>cc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>,</l>
					<l n="15" num="4.3"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="15.3">l</w>’<w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="4.4"><w n="16.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>