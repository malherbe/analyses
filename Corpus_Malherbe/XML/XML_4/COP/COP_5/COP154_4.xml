<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME PARTIE</head><div type="poem" key="COP154">
					<head type="main">RONDEAU</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">S<seg phoneme="o" type="vs" value="1" rule="COP154_1">o</seg>n</w> <w n="1.2"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="1.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w>-<w n="1.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="1.5">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.6">l</w>’<w n="1.7">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3">hu<seg phoneme="i" type="vs" value="1" rule="491">î</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="3.3">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.6">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="3.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w></l>
						<l n="4" num="1.4"><w n="4.1">Lu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="4.3">mi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w></l>
						<l n="5" num="1.5"><w n="5.1">Qu</w>’<w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.6">c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><w n="6.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="6.4">m</w>’<w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.8">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="7" num="2.2"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="7.4">m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="8" num="2.3"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="8.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="8.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w></l>
						<l n="9" num="2.4"><space quantity="16" unit="char"></space><w n="9.1">S<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="10" num="3.1"><w n="10.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.3">wh<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gt</w> <w n="10.6">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.8">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="11" num="3.2"><w n="11.1">M<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="11.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.4">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="11.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="12" num="3.3"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.5">R<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>thsch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ld</w> <w n="12.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w></l>
						<l n="13" num="3.4"><w n="13.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> ;</l>
						<l n="14" num="3.5"><w n="14.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="14.3">d</w>’<w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.6">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="15" num="3.6"><space quantity="16" unit="char"></space><w n="15.1">S</w>’<w n="15.2">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>