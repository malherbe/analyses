<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME PARTIE</head><div type="poem" key="COP134">
					<head type="main">A MONSIEUR ÉVELART</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="1.2">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="i" type="vs" value="1" rule="dc-1">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="1.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.4">d</w>’<w n="1.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>cts</w> <w n="1.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
						<l n="2" num="1.2"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>n<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="2.6">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="2.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.9">th<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">Fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="3.3">gr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="3.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w>, <w n="3.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.7">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="3.8">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.2">m</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="4.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="4.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>fl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.6">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>q</w> <w n="4.7">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="4.8">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="5.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">l<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="5.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w>,</l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="6.2">hu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.8">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="6.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.10">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="7.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.5">j</w>’<w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="7.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.8">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="7.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.10">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>. <w n="8.5">C</w>’<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.9"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.10">l</w>’<w n="8.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> « <w n="9.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="451">u</seg>m</w> » <w n="9.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w>, <w n="9.5">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="9.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="9.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="9.8">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.5">d</w>’<w n="10.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="10.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.9"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="10.10">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="11" num="3.3"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="11.3">d</w>’<w n="11.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.7">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="11.8">vi<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="12.2">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.6"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="12.7">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>du<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="12.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="12.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.10">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="13" num="4.2"><w n="13.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.2"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="13.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="13.4">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="13.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.6">l<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ttr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="13.7"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="13.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
						<l n="14" num="4.3"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="14.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="14.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.7">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="14.8">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="14.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>