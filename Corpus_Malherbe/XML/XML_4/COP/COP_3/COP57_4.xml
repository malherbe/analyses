<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE CAHIER ROUGE</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1198 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE CAHIER ROUGE</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscopeelecahierrouge.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Œuvres complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie L. Hébert</publisher>
							<date when="1885">1885</date>
						</imprint>
						<biblScope unit="tome">Poésie, tome 2</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP57">
				<head type="main">Sonnet</head>
				<head type="sub_1">Écrit sur un Ronsard</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="1.2">T<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="1.3">c</w>’<w n="1.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ci<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.8">d</w>’<w n="2.9"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="3.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ri<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">V<seg phoneme="e" type="vs" value="1" rule="383">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t</w> <w n="4.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="4.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.4">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="4.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.6">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ge<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.9">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ç<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w>-<w n="5.6">d</w>’<w n="5.7"><seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.8">d</w>’<w n="5.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ci<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="6" num="2.2"><w n="6.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> — <w n="6.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="6.5">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.8">pl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c</w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="7.7">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d</w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.9">l</w>’<w n="7.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>cl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="2.4"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="8.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="9.4">R<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>, <w n="9.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">nu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="9.7">s</w>’<w n="9.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.9"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="10" num="3.2"><w n="10.1">J</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="10.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="10.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="10.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.6">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.8">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.2">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-6">e</seg>r</w> <w n="11.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="11.4">fl<seg phoneme="ɛ" type="vs" value="1" rule="355">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="11.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="12.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="12.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
					<l n="13" num="4.2"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.3">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="13.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.5">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="13.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.8">l</w>’<w n="13.9"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.11">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="14" num="4.3"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">j</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="14.4">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="14.7">gl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="14.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="14.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.10">l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>