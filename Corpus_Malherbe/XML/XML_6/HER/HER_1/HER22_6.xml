<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La Grèce et la Sicile</head><head type="main_subpart">Persée et Andromède</head><div type="poem" key="HER22" modus="cm" lm_max="12" metProfile="6+6">
						<head type="main">Andromède au Monstre</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="1.2">Vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3" punct="vg:6">C<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>ph<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="366" place="6" punct="vg" caesura="1">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>,<caesura></caesura> <w n="1.4" punct="pe:8">h<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>s</w> ! <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>r</w> <w n="1.6" punct="vg:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:2">L<seg phoneme="i" type="vs" value="1" rule="d-1" place="1" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="2.2" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="2.4">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>c</w> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="2.6">n<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>rs</w> <w n="2.7" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">î</seg>l<seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="vg">o</seg>ts</w>,</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="3.4">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="3.7">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10">ain</seg>s</w> <w n="3.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438" place="12">o</seg>ts</w></l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="4.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r</w> <w n="4.3">r<seg phoneme="wa" type="vs" value="1" rule="440" place="3" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="4.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rt</w><caesura></caesura> <w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="4.7">fr<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg></w> <w n="4.8">d</w>’<w n="4.9" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg></w> <w n="5.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>str<seg phoneme="y" type="vs" value="1" rule="454" place="5" mp="M">u</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="5.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="5.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">Cr<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="6.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4">e</seg>ds</w> <w n="6.5">gl<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="6.6">l</w>’<w n="6.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="6.8">b<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="6.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="6.10" punct="vg:12">fl<seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="vg">o</seg>ts</w>,</l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="7.4" punct="vg:6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>t</w>,<caesura></caesura> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="7.6">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>rs</w> <w n="7.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="7.8">c<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>ls</w> <w n="7.9" punct="vg:12">cl<seg phoneme="o" type="vs" value="1" rule="438" place="12" punct="vg">o</seg>s</w>,</l>
							<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">B<seg phoneme="a" type="vs" value="1" rule="307" place="1" mp="M">â</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="8.3">gu<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="8.4" punct="vg:6">gl<seg phoneme="o" type="vs" value="1" rule="318" place="6" punct="vg" caesura="1">au</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="8.5"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="8.7" punct="pt:12">m<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="9.2">qu</w>’<w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="9.6">f<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="9.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="9.9">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="9.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="9.11" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>r</w>,</l>
							<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="10.3" punct="vg:3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>p</w>, <w n="10.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="10.6">h<seg phoneme="e" type="vs" value="1" rule="169" place="8" mp="M">e</seg>nn<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="11">en</seg>t</w> <w n="10.7">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r</w></l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="11.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="11.3">s</w>’<w n="11.4" punct="pt:4"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="pt" mp="F">e</seg>nt</w>. <w n="11.5">L</w>’<w n="11.6">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="11.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="11.8" punct="vg:9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg>t</w>, <w n="11.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="11.10">l</w>’<w n="11.11" punct="pv:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="C">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.4" punct="vg:4">v<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg></w>, <w n="12.5">d</w>’<w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="12.7">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>l</w><caesura></caesura> <w n="12.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="12.9"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="12.10" punct="vg:12">s<seg phoneme="y" type="vs" value="1" rule="445" place="12" punct="vg">û</seg>r</w>,</l>
							<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="13.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>s</w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="13.5">p<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>ds</w><caesura></caesura> <w n="13.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="13.7">f<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ls</w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="13.9" punct="vg:10">Z<seg phoneme="ø" type="vs" value="1" rule="403" place="10" punct="vg">eu</seg>s</w>, <w n="13.10">P<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
							<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="14.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="14.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="14.4">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="14.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="14.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="14.8">d</w>’<w n="14.9" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pt">u</seg>r</w>.</l>
						</lg>
					</div></body></text></TEI>