<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La Grèce et la Sicile</head><div type="poem" key="HER19" modus="cm" lm_max="12" metProfile="6+6">
					<head type="main">La Magicienne</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="1.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="1.3" punct="vg:3">li<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>x</w>, <w n="1.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="1.6">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6" caesura="1">e</seg>d</w><caesura></caesura> <w n="1.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="1.8"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ls</w> <w n="1.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="1.10">j</w> ’<w n="1.11" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="2.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="2.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="2.5">m</w>’<w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.8">m</w>’<w n="2.9"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.10">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="2.11">br<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg>s</w> <w n="2.12" punct="pt:12">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>cs</w>.</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="3.2">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="3.3" punct="vg:6">v<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="3.5">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="3.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="3.8">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>cs</w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">M</w>’<w n="4.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="4.3" punct="vg:3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="4.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w>-<w n="4.5">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="5" mp="Fm">e</seg></w> <w n="4.6">n<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="4.7">d</w>’<w n="4.8"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.9"><seg phoneme="e" type="vs" value="1" rule="354" place="8" mp="M">e</seg>x<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>cr<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="4.10" punct="pi:12">r<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="ø" type="vs" value="1" rule="405" place="1" mp="M">Eu</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>lp<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="5.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>g<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="5.4">n</w>’<w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8">oin</seg>t</w> <w n="5.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="5.8">S<seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>thr<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">S<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>c<seg phoneme="u" type="vs" value="1" rule="d-2" place="2" mp="M">ou</seg><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="6.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" mp="P">e</seg>rs</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="6.4">s<seg phoneme="œ" type="vs" value="1" rule="406" place="6" caesura="1">eu</seg>il</w><caesura></caesura> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="6.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>gs</w> <w n="6.7">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg>x</w> <w n="6.8" punct="vg:12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>ts</w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="7.3" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="7.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="7.5" punct="vg:6">fu<seg phoneme="i" type="vs" value="1" rule="491" place="6" punct="vg" caesura="1">i</seg>s</w>,<caesura></caesura> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>r</w> <w n="7.8" punct="vg:9">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" punct="vg">a</seg>s</w>, <w n="7.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="7.10">pi<seg phoneme="e" type="vs" value="1" rule="241" place="11">e</seg>ds</w> <w n="7.11" punct="pv:12">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="pv">en</seg>ts</w> ;</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">J</w>’<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="8.4">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg>s</w> <w n="8.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="8.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="8.7">h<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="8.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="8.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="8.10" punct="pt:12">tr<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="9.3" punct="vg:4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="4" punct="vg">en</seg>s</w>, <w n="9.4">j</w>’<w n="9.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="9.7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="8" mp="Lc">oi</seg></w>-<w n="9.8">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.9" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>d<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="10.2">n<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>rs</w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>ts</w><caesura></caesura> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="10.6">s<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="10.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="11.2">m</w>’<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="11.6">c<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="11.8" punct="pv:12">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pv">eu</seg>x</w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="12.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ds</w> <w n="12.4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="12.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="12.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="12.7">d</w>’<w n="12.8"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>rr<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="12.9"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="13.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="13.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="4" mp="M">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="13.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="13.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="13.9" punct="vg:12">y<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</w>,</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="14.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="14.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="14.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="14.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg>s</w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="14.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="14.9" punct="pt:12">l<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>