<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Le Moyen âge et la Renaissance</head><head type="main_subpart">Les conquérants</head><div type="poem" key="HER79" modus="cm" lm_max="12" metProfile="6+6">
						<head type="main">Les Conquérants</head>
						<lg n="1">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="1.3">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>l</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="1.5">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rf<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>ts</w><caesura></caesura> <w n="1.6">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rs</w> <w n="1.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="1.8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="1.9" punct="vg:12">n<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>l</w>,</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">F<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>gu<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="2.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="2.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>rs</w> <w n="2.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="2.6" punct="vg:12">h<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="3.2">P<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="3.4" punct="vg:6">M<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" mp="M">o</seg>gu<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="3.5">r<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg>s</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="3.7">c<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="vg:2">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2" punct="vg">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>, <w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="4.3">d</w>’<w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="4.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" caesura="1">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w><caesura></caesura> <w n="4.6">h<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg><seg phoneme="i" type="vs" value="1" rule="477" place="9">ï</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="4.8" punct="pt:12">br<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>l</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="5.5">f<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>l<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="5.6">m<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>l</w></l>
							<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">C<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg></w> <w n="6.3">m<seg phoneme="y" type="vs" value="1" rule="445" place="5" mp="M">û</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="6.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="6.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="6.6">m<seg phoneme="i" type="vs" value="1" rule="467" place="9">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="6.7" punct="vg:12">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="11" mp="M">oin</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="7.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>ts</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>z<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg>s</w><caesura></caesura> <w n="7.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="10" mp="C">eu</seg>rs</w> <w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="366" place="12">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
							<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg>x</w> <w n="8.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rds</w> <w n="8.3">m<seg phoneme="i" type="vs" value="1" rule="493" place="3" mp="M">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="8.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="8.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.6" punct="pt:12"><seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg>cc<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11" mp="M">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>l</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="9.2" punct="vg:3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>r</w>, <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="10">ain</seg>s</w> <w n="9.6" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
							<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="10.3">ph<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>sph<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="10.6">m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>r</w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="10.8">Tr<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">En</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="11.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="C">eu</seg>r</w> <w n="11.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="11.4">d</w>’<w n="11.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="11.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.7" punct="pv:12">d<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pv">é</seg></w> ;</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="12.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="12.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="12.8" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
							<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="13.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="13.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="13.6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="13.7"><seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
							<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="14.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="14.4">l</w>’<w n="14.5"><seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg></w><caesura></caesura> <w n="14.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="14.8" punct="pt:12">n<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
						</lg>
					</div></body></text></TEI>