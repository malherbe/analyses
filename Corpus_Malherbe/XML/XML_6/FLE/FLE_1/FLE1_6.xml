<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Friperies</title>
				<title type="medium">Une édition électronique</title>
				<author key="FLE">
					<name>
						<forename>Fernand</forename>
						<surname>FLEURET</surname>
					</name>
					<date from="1883" to="1945">1883-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>455 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FLE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Friperies</title>
						<author>Fernand Fleuret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/friperies00fleu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Friperies</title>
								<author>Fernand Fleuret</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>CHEZ EUGÈNE REY, LIBRAIRE</publisher>
									<date when="1907">1907</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1907">1907</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-08-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-08-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FLE1" modus="cm" lm_max="12" metProfile="6=6">
				<head type="main">SOMPTUAIRE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6">— <w n="1.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="1.2" punct="vg:3">l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="1.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="C">ou</seg>s</w> <w n="1.4">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.6">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rm<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:1"><seg phoneme="u" type="vs" value="1" rule="426" place="1" punct="vg">Où</seg></w>, <w n="2.2" punct="vg:3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="M">oi</seg>gn<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>x</w>, <w n="2.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="2.4">pl<seg phoneme="i" type="vs" value="1" rule="482" place="5" mp="M">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg></w><caesura></caesura> <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="2.6">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="2.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg></w> <w n="3.2">d</w>’<w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="3.4">pr<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="M">u</seg>sc<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>pt<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="3.7">m<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="4.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="4.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>ps</w><caesura></caesura> <w n="4.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="4.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="10">ai</seg></w> <w n="4.9" punct="pt:12">t<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12" met="6+6">— <w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="5.3" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="5.5">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="5.8" punct="vg:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">O</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2" punct="vg:2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2" punct="vg">e</seg>r</w>, <w n="6.3" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>rn<seg phoneme="wa" type="vs" value="1" rule="420" place="4" mp="M">oi</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="6.4">pr<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="6.6" punct="pv:12">gr<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="7.2" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>s</w>, <w n="7.3">tr<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>rs</w> <w n="7.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="7.6" punct="pt:12">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="8.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>dr<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="8.5">gr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="8.7">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.9" punct="ps:12">M<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="12">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></w>…</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12" met="6+6">— <w n="9.1">Qu</w>’<w n="9.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="1" mp="M">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="9.3">qu</w>’<w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="9.5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="9.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="9.8">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="9.9" punct="pi:12">d<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg>nt</w> ?</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="10.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="10.4" punct="vg:6">v<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="10.5">qu</w>’<w n="10.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="7" mp="M">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="10.7">qu</w>’<w n="10.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="10.9">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="10.10" punct="pi:12">v<seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></w> ?</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="11.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="11.3">gr<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="11.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="11.5" punct="vg:6">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg" caesura="1">ai</seg>t</w>,<caesura></caesura> <w n="11.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="11.7">ps<seg phoneme="i" type="vs" value="1" rule="493" place="8" mp="M">y</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="11.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="11.9" punct="pt:12">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="423" place="12">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					<l n="12" num="3.4" lm="12" mp7="F" met="4+4+4"><w n="12.1">Gl<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="12.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>rn<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="12.4">qu<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="12.5">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="8" caesura="2">oin</seg>s</w><caesura></caesura> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="12.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="12.8" punct="ps:12">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></w>…</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="12" met="6+6">— <w n="13.1">N<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="y" type="vs" value="1" rule="391" place="3">eu</seg>t</w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="13.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="13.5">d</w>’<w n="13.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w> <w n="13.8">v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="9">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.9"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="13.10">tr<seg phoneme="o" type="vs" value="1" rule="433" place="11">o</seg>p</w> <w n="13.11" punct="vg:12">p<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="14.3">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="14.5">v<seg phoneme="u" type="vs" value="1" rule="d-2" place="5" mp="M">ou</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="14.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="14.7">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="14.8" punct="ps:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg>s</w>…</l>
				</lg>
				<lg n="5">
					<l n="15" num="5.1" lm="12" met="6+6">— <w n="15.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="15.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="15.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="15.4">n</w>’<w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="15.6">l</w>’<w n="15.7">h<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="15.8">d</w>’<w n="15.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="15.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438" place="9">o</seg>t</w> <w n="15.11" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>j<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="16" num="5.2" lm="12" met="6+6"><w n="16.1">L</w>’<w n="16.2" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="16.3" punct="vg:6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>s</w>,<caesura></caesura> <w n="16.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="16.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="16.6">c<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="16.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="16.8" punct="pt:12">f<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>nt</w>.</l>
				</lg>
			</div></body></text></TEI>