<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Ïambes et poèmes</title>
				<title type="medium">Édition électronique</title>
				<author key="BRB">
					<name>
						<forename>Auguste</forename>
						<surname>BARBIER</surname>
					</name>
					<date from="1805" to="1882">1805-1882</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4203 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Iambes et poèmes</title>
						<author>Auguste BARBIER</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L947</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Iambes et poèmes</title>
								<author>Auguste BARBIER</author>
								<idno type="URL">https://archive.org/details/iambesetpomes03barbgoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Paul Masgana</publisher>
									<date when="1840">1840</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1831">1831</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IL PIANTO</head><div type="poem" key="BRB24" modus="cm" lm_max="12" metProfile="6+6">
					<head type="main">MICHEL-ANGE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="1.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="1.5">tr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="1.8">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>t</w> <w n="1.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">ai</seg>gr<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="M">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.2">M<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M/mc">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" mp="Lc">e</seg>l</w>-<w n="2.3" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">An</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="2.5">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="2.6">t<seg phoneme="a" type="vs" value="1" rule="307" place="9" mp="M">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="2.8" punct="pe:12">pi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></w> !</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">N<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="3.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>s</w><caesura></caesura> <w n="3.4">n</w>’<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="3.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="3.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="3.8" punct="dp:12">p<seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>pi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></w> :</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="4.2" punct="vg:3">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="4.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="4.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="4.7">n</w>’<w n="4.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="4.9">j<seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>s</w> <w n="4.10" punct="pt:12">r<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="5.2">D</w>’<w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="5.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="5">o</seg>p</w> <w n="5.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rt</w><caesura></caesura> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="5.8">m<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="5.9">t</w>’<w n="5.10"><seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg></w> <w n="5.11" punct="vg:12">n<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt</w> <w n="6.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="6.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="6.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l</w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.8">pr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="6.9">t<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="6.10">v<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.11" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="7.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="7.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="7.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="7.6">tr<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="7.7">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="8.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="8.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="8.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="8.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="8.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="8.7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="9">œu</seg>r</w> <w n="8.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>dr<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">P<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="9.2" punct="pe:6">Bu<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>r<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pe" caesura="1">i</seg></w> !<caesura></caesura> <w n="9.3">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="9.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>l</w> <w n="9.5">b<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg></w> <w n="9.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">F<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>t</w> <w n="10.2">d</w>’<w n="10.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="2" mp="M">im</seg>pr<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="10.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="10.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>r</w> <w n="10.8" punct="vg:12">pr<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="11.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="11.4" punct="vg:6">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg></w>,<caesura></caesura> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="e" type="vs" value="1" rule="353" place="7" mp="M">e</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="339" place="8" mp="M">a</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="11.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="11.8" punct="dp:12">lu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="dp">i</seg></w> :</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="vg:2"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="12.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="12.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rv<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg>s</w><caesura></caesura> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="12.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="12.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w> <w n="12.8" punct="vg:12">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">Vi<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="13.2">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="2" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="13.3" punct="vg:6">f<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>gu<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="13.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="13.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="13.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.7">cr<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>ni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="14.2">m<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="14.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>gu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="14.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7">ein</seg></w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="14.6">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="14.8">d</w>’<w n="14.9" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="11" mp="M">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg></w>.</l>
					</lg>
				</div></body></text></TEI>