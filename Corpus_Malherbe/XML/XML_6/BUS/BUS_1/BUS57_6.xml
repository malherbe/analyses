<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="BUS">
					<name>
						<forename>Alfred</forename>
						<surname>BUSQUET</surname>
					</name>
					<date from="1819" to="1883">1819-1883</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2356 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BUS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES</title>
						<author>Alfred Busquet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URI">https ://archive.org/details/poesiespremieres00alfr/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">POÉSIES</title>
								<author>Alfred Busquet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE HACHETTE ET Cie</publisher>
									<date when="1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface (AU LECTEUR) n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRISTESSES ET JOIES</head><div type="poem" key="BUS57" modus="sm" lm_max="5" metProfile="5">
					<head type="main">RÊVERIE</head>
					<head type="form">Pantoun</head>
					<lg n="1">
						<l n="1" num="1.1" lm="5" met="5"><w n="1.1">G<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="1.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="1.4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w></l>
						<l n="2" num="1.2" lm="5" met="5"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="2.2" punct="pv:5">Ph<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></w> ;</l>
						<l n="3" num="1.3" lm="5" met="5"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>x</w></l>
						<l n="4" num="1.4" lm="5" met="5"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="4.3" punct="pt:5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
						<l n="5" num="1.5" lm="5" met="5">— <w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">Ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="5.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w></l>
						<l n="6" num="1.6" lm="5" met="5"><w n="6.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="6.3" punct="pe:5">gu<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pe">i</seg>r</w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1" lm="5" met="5"><w n="7.1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>l</w> <w n="7.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="7.4" punct="vg:5">n<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
						<l n="8" num="2.2" lm="5" met="5"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>rr<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="8.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="8.4" punct="vg:5">fl<seg phoneme="o" type="vs" value="1" rule="438" place="5" punct="vg">o</seg>ts</w>,</l>
						<l n="9" num="2.3" lm="5" met="5"><w n="9.1">N<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>ts</w></l>
						<l n="10" num="2.4" lm="5" met="5"><w n="10.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="10.2">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="10.3" punct="pt:5">cr<seg phoneme="y" type="vs" value="1" rule="454" place="4">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
						<l n="11" num="2.5" lm="5" met="5">— <w n="11.1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">Ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="11.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w></l>
						<l n="12" num="2.6" lm="5" met="5"><w n="12.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="12.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="12.3" punct="pe:5">m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pe">i</seg>r</w> !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1" lm="5" met="5"><w n="13.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="13.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="13.4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w></l>
						<l n="14" num="3.2" lm="5" met="5"><w n="14.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2">pr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="14.3" punct="pv:5">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></w> ;</l>
						<l n="15" num="3.3" lm="5" met="5"><w n="15.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.2" punct="vg:5"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg">eu</seg>x</w>,</l>
						<l n="16" num="3.4" lm="5" met="5"><w n="16.1"><seg phoneme="u" type="vs" value="1" rule="425" place="1">Ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="16.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r</w> <w n="16.3" punct="pt:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
						<l n="17" num="3.5" lm="5" met="5">— <w n="17.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="17.2">j<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="17.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w></l>
						<l n="18" num="3.6" lm="5" met="5"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="18.3" punct="pe:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="pe">i</seg>r</w> !</l>
					</lg>
				</div></body></text></TEI>