<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><head type="main_subpart">XL</head><head type="main_subpart">Un fantôme</head><div type="poem" key="BAU43" modus="cm" lm_max="10" metProfile="4+6">
						<head type="number">III</head>
						<head type="main">Le Cadre</head>
						<lg n="1">
							<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="1.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="1.4">c<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.8" punct="vg:10">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="9" mp="M">ein</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
							<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg></w> <w n="2.2">qu</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4" caesura="1">oi</seg>t</w><caesura></caesura> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="2.7">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" mp="M">in</seg>c<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="2.8">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>s</w> <w n="2.9" punct="vg:10">v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></w>,</l>
							<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="3.4">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="4" caesura="1">oi</seg></w><caesura></caesura> <w n="3.5">d</w>’<w n="3.6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.8">d</w>’<w n="3.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w></l>
							<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="4.5">l</w>’<w n="4.6"><seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="4.7" punct="vg:10">n<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1" mp="M">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="5.2" punct="vg:4">b<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg" caesura="1">ou</seg>x</w>,<caesura></caesura> <w n="5.3" punct="vg:6">m<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6" punct="vg" mp="F">e</seg>s</w>, <w n="5.4" punct="vg:8">m<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="vg">au</seg>x</w>, <w n="5.5" punct="vg:10">d<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
							<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1">S</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>pt<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="6.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="6.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="6.7" punct="pv:10">b<seg phoneme="o" type="vs" value="1" rule="315" place="9" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pv">é</seg></w> ;</l>
							<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1">en</seg></w> <w n="7.2">n</w>’<w n="7.3"><seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>ff<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="M">u</seg>squ<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="7.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="7.6" punct="vg:10">cl<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></w>,</l>
							<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="8.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="8.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="C">i</seg></w> <w n="8.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r</w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="8.7" punct="pt:10">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="9.3"><seg phoneme="y" type="vs" value="1" rule="251" place="3">eû</seg>t</w> <w n="9.4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="9.6">qu</w>’<w n="9.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.8">cr<seg phoneme="wa" type="vs" value="1" rule="440" place="9" mp="M">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w></l>
							<l n="10" num="3.2" lm="10" met="4+6"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="10.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="10.4">l</w>’<w n="10.5" punct="pv:6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="pv">er</seg></w> ; <w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="10.7">n<seg phoneme="wa" type="vs" value="1" rule="440" place="9" mp="M">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>t</w></l>
							<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="11.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg>s</w><caesura></caesura> <w n="11.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="11.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg></w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="11.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="11.8">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1" lm="10" met="4+6"><w n="12.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="12.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="12.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rps</w> <w n="12.4" punct="vg:4">n<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg" caesura="1">u</seg></w>,<caesura></caesura> <w n="12.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5">ein</seg></w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="12.7" punct="vg:10">fr<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" punct="vg">en</seg>ts</w>,</l>
							<l n="13" num="4.2" lm="10" met="4+6"><w n="13.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="13.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="13.4" punct="vg:4">br<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg" caesura="1">u</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="13.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="13.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="13.8" punct="vg:10">m<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" punct="vg">en</seg>ts</w>,</l>
							<l n="14" num="4.3" lm="10" met="4+6"><w n="14.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="14.3">gr<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" mp="M">an</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="14.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="C">u</seg></w> <w n="14.6" punct="pt:10">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
						</lg>
					</div></body></text></TEI>