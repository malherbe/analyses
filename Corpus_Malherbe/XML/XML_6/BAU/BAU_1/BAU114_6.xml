<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE VIN</head><div type="poem" key="BAU114" modus="cm" lm_max="12" metProfile="6+6">
					<head type="number">CX</head>
					<head type="main">Le Vin du solitaire</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="1.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" mp="M">in</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="5" mp="M">u</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="1.4">d</w>’<w n="1.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.6">f<seg phoneme="a" type="vs" value="1" rule="193" place="9">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.7">g<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">gl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5" mp="P">e</seg>rs</w> <w n="2.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="2.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="2.8">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="10" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg></w> <w n="2.9">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>c</w></l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="3.3">l<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">on</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>l<seg phoneme="ø" type="vs" value="1" rule="403" place="6" caesura="1">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="423" place="8">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg></w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>c</w> <w n="3.8" punct="vg:12">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="4.3"><seg phoneme="i" type="vs" value="1" rule="497" place="3" mp="C">y</seg></w> <w n="4.4">v<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>t</w> <w n="4.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="4.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="4.7">b<seg phoneme="o" type="vs" value="1" rule="315" place="8" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="4.8" punct="pv:12">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="M">on</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="5.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c</w> <w n="5.4">d</w>’<w n="5.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="5.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="5.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="5.8">d<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>gts</w> <w n="5.9">d</w>’<w n="5.10"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10">un</seg></w> <w n="5.11" punct="pv:12">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="11" mp="M">ou</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pv">eu</seg>r</w> ;</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="6.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="6.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" caesura="1">in</seg></w><caesura></caesura> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="6.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>gr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">A</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="7.5">m<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="7.8" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">â</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">em</seg>bl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="8.3">cr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="8.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5" mp="M">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="8.6">l</w>’<w n="8.7">h<seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="M">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305" place="9">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="8.8" punct="vg:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="vg">eu</seg>r</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="9.2">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="9.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="9.4">v<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>t</w> <w n="9.5" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>s</w>,<caesura></caesura> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="9.7">b<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.8" punct="vg:12">pr<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="10.2">b<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="10.3">p<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="10.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="10.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.7">f<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" mp="C">au</seg></w> <w n="11.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>lt<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="11.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="11.6">p<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="9">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="11.7" punct="pv:12">p<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pv">eu</seg>x</w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="12.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2" mp="C">i</seg></w> <w n="12.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="12.4">l</w>’<w n="12.5" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>r</w>,<caesura></caesura> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="12.7">j<seg phoneme="œ" type="vs" value="1" rule="407" place="8" mp="M">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="9">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="12.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="12.10" punct="vg:12">v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="13" num="4.2" lm="12" met="6+6">— <w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="13.2">l</w>’<w n="13.3" punct="vg:3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344" place="3" punct="vg">ue</seg>il</w>, <w n="13.4">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="13.5">tr<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="13.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="13.8" punct="vg:12">gu<seg phoneme="ø" type="vs" value="1" rule="403" place="10" mp="M">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="14.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>d</w> <w n="14.4">tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="14.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">em</seg>bl<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="14.7"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="C">au</seg>x</w> <w n="14.8" punct="pt:12">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</w>.</l>
					</lg>
				</div></body></text></TEI>