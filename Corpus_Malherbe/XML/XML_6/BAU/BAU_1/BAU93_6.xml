<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">TABLEAUX PARISIENS</head><div type="poem" key="BAU93" modus="cm" lm_max="12" metProfile="6+6">
					<head type="number">LXXXIX</head>
					<head type="main">Paysage</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2" punct="vg:2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="1.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="P">ou</seg>r</w> <w n="1.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="M">om</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="1.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9">en</seg>t</w> <w n="1.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="1.7" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>gl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="2.4" punct="vg:6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" punct="vg" caesura="1">e</seg>l</w>,<caesura></caesura> <w n="2.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="2.7" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>str<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="3.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="M">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="3.4" punct="vg:6">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg>s</w>,<caesura></caesura> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="3.7">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="11" mp="M">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</w></l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1" mp="C">eu</seg>rs</w> <w n="4.2">h<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="4.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>l<seg phoneme="a" type="vs" value="1" rule="194" place="5" mp="M">e</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" caesura="1">e</seg>ls</w><caesura></caesura> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg>s</w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="P">a</seg>r</w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="4.7" punct="pt:12">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="pt">en</seg>t</w>.</l>
						<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="5.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="5.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>s</w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg></w> <w n="5.5" punct="vg:6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" mp="M">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg></w>,<caesura></caesura> <w n="5.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="5.7">h<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>t</w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="5.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="5.10" punct="vg:12">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="6.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="6.3">l</w>’<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="6.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="6.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="6.9" punct="pv:12">b<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="7.2" punct="vg:3">t<seg phoneme="ɥi" type="vs" value="1" rule="462" place="2" mp="M">u</seg>y<seg phoneme="o" type="vs" value="1" rule="318" place="3" punct="vg">au</seg>x</w>, <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="7.4" punct="vg:6">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg>s</w>,<caesura></caesura> <w n="7.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="7.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>ts</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="7.9" punct="vg:12">c<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
						<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="8.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ds</w> <w n="8.4">ci<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ls</w> <w n="8.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="8.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="8.7">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7" mp="M">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="8.8">d</w>’<w n="8.9" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10" mp="M">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="9.3" punct="vg:3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>x</w>, <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="9.5">tr<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>rs</w><caesura></caesura> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="9.7" punct="vg:9">br<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" punct="vg" mp="F">e</seg>s</w>, <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="9.9">v<seg phoneme="wa" type="vs" value="1" rule="420" place="11">oi</seg>r</w> <w n="9.10">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="10" num="2.2" lm="12" met="6+6"><w n="10.1">L</w>’<w n="10.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="10.4">l</w>’<w n="10.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>r</w>,<caesura></caesura> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="10.7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">am</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="10.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="10.10" punct="vg:12">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="11" num="2.3" lm="12" met="6+6"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="11.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="11.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rb<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="11.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg></w> <w n="11.7">f<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>rm<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="12">en</seg>t</w></l>
						<l n="12" num="2.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="12.3">l<seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="12.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="12.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="12.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="pt">en</seg>t</w>.</l>
						<l n="13" num="2.5" lm="12" met="6+6"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="13.4" punct="vg:6">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="vg" caesura="1">em</seg>ps</w>,<caesura></caesura> <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="13.6" punct="vg:9"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="9" punct="vg">é</seg>s</w>, <w n="13.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="13.8" punct="pv:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>s</w> ;</l>
						<l n="14" num="2.6" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="14.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3" mp="M">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="14.4">l</w>’<w n="14.5">h<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>r</w><caesura></caesura> <w n="14.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="14.7">n<seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="14.8" punct="vg:12">m<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="15" num="2.7" lm="12" met="6+6"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="15.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rm<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>t</w><caesura></caesura> <w n="15.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>rti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="15.6">v<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="12">e</seg>ts</w></l>
						<l n="16" num="2.8" lm="12" met="6+6"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="16.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">â</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="16.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="16.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="16.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="16.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="16.7">f<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="16.8" punct="pt:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="pt">ai</seg>s</w>.</l>
						<l n="17" num="2.9" lm="12" met="6+6"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="17.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="17.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4" mp="M">ê</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6" caesura="1">ai</seg></w><caesura></caesura> <w n="17.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="17.5">h<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w> <w n="17.6" punct="vg:12">bl<seg phoneme="œ" type="vs" value="1" rule="407" place="11" mp="M">eu</seg><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="18" num="2.10" lm="12" met="6+6"><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="18.2" punct="vg:3">j<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" punct="vg">in</seg>s</w>, <w n="18.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="18.4">j<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>ts</w> <w n="18.5">d</w>’<w n="18.6"><seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="18.7">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="7" mp="M">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="18.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="18.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="18.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>lb<seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="19" num="2.11" lm="12" met="6+6"><w n="19.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="19.2" punct="vg:3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">er</seg>s</w>, <w n="19.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="19.4"><seg phoneme="wa" type="vs" value="1" rule="420" place="5" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="19.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="19.6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>r</w> <w n="19.7"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="19.8" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></w>,</l>
						<l n="20" num="2.12" lm="12" met="6+6"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="20.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="20.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="20.5">l</w>’<w n="20.6"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">I</seg>d<seg phoneme="i" type="vs" value="1" rule="493" place="6" caesura="1">y</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="20.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="20.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="20.9">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="20.10" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="pt">in</seg></w>.</l>
						<l n="21" num="2.13" lm="12" met="6+6"><w n="21.1">L</w>’<w n="21.2" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>m<seg phoneme="ø" type="vs" value="1" rule="399" place="2">eu</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="21.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" mp="M">ê</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="21.4">v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7" mp="M">ai</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="9">en</seg>t</w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="21.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="21.7" punct="vg:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="22" num="2.14" lm="12" met="6+6"><w n="22.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="22.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="22.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="22.4">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="22.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="22.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="22.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="22.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="22.9" punct="pv:12">p<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="23" num="2.15" lm="12" met="6+6"><w n="23.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="23.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="23.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="23.4">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="23.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="23.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="23.7">v<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
						<l n="24" num="2.16" lm="12" met="6+6"><w n="24.1">D</w>’<w n="24.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2" mp="M">o</seg>qu<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="24.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="24.4">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" mp="M">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">em</seg>ps</w><caesura></caesura> <w n="24.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="24.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="24.7" punct="vg:12">v<seg phoneme="o" type="vs" value="1" rule="444" place="10" mp="M">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
						<l n="25" num="2.17" lm="12" met="6+6"><w n="25.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="25.2">t<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="25.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="25.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="25.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="25.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="25.7" punct="vg:9">c<seg phoneme="œ" type="vs" value="1" rule="249" place="9" punct="vg">œu</seg>r</w>, <w n="25.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="25.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="25.10">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="26" num="2.18" lm="12" met="6+6"><w n="26.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="26.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="26.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="26.4">br<seg phoneme="y" type="vs" value="1" rule="445" place="5" mp="M">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="26.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="26.6">ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.7" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>tm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>sph<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>