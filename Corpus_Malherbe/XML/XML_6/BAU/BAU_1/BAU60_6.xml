<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU60" modus="sp" lm_max="7" metProfile="5, 7">
					<head type="number">LVI</head>
					<head type="main">L’Invitation au voyage</head>
					<lg n="1">
						<l n="1" num="1.1" lm="5" met="5"><space quantity="6" unit="char"></space><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="1.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.4" punct="vg:5">s<seg phoneme="œ" type="vs" value="1" rule="249" place="5" punct="vg">œu</seg>r</w>,</l>
						<l n="2" num="1.2" lm="5" met="5"><space quantity="6" unit="char"></space><w n="2.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="2.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w></l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1">D</w>’<w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w>-<w n="3.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="3.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6" punct="pe:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
						<l n="4" num="1.4" lm="5" met="5"><space quantity="6" unit="char"></space><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">Ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="4.3" punct="vg:5">l<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>r</w>,</l>
						<l n="5" num="1.5" lm="5" met="5"><space quantity="6" unit="char"></space><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">Ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="5.3">m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w></l>
						<l n="6" num="1.6" lm="7" met="7"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="6.2">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="3">y</seg>s</w> <w n="6.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="6.4">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5" punct="pe:7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
						<l n="7" num="1.7" lm="5" met="5"><space quantity="6" unit="char"></space><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ils</w> <w n="7.3">m<seg phoneme="u" type="vs" value="1" rule="428" place="4">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w></l>
						<l n="8" num="1.8" lm="5" met="5"><space quantity="6" unit="char"></space><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="8.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ls</w> <w n="8.4">br<seg phoneme="u" type="vs" value="1" rule="427" place="4">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w></l>
						<l n="9" num="1.9" lm="7" met="7"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="9.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="9.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="9.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="9.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
						<l n="10" num="1.10" lm="5" met="5"><space quantity="6" unit="char"></space><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="10.2">m<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w></l>
						<l n="11" num="1.11" lm="5" met="5"><space quantity="6" unit="char"></space><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="11.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="11.4" punct="vg:5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="5" punct="vg">eu</seg>x</w>,</l>
						<l n="12" num="1.12" lm="7" met="7"><w n="12.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="12.3">tr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>rs</w> <w n="12.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="12.5" punct="pt:7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1" lm="7" met="7"><w n="13.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="13.3">n</w>’<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="13.5">qu</w>’<w n="13.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="13.8" punct="vg:7">b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></w>,</l>
						<l n="14" num="2.2" lm="7" met="7"><w n="14.1" punct="vg:2">L<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="14.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="14.4" punct="pt:7">v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="15" num="3.1" lm="5" met="5"><space quantity="6" unit="char"></space><w n="15.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="15.2">m<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="15.3" punct="vg:5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg">an</seg>ts</w>,</l>
						<l n="16" num="3.2" lm="5" met="5"><space quantity="6" unit="char"></space><w n="16.1">P<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="16.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="16.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="16.4" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" punct="vg">an</seg>s</w>,</l>
						<l n="17" num="3.3" lm="7" met="7"><w n="17.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>c<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="17.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.3" punct="pv:7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></w> ;</l>
						<l n="18" num="3.4" lm="5" met="5"><space quantity="6" unit="char"></space><w n="18.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="18.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="18.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="18.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w></l>
						<l n="19" num="3.5" lm="5" met="5"><space quantity="6" unit="char"></space><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="19.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="19.3"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w></l>
						<l n="20" num="3.6" lm="7" met="7"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="20.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="20.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.5">l</w>’<w n="20.6" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="21" num="3.7" lm="5" met="5"><space quantity="6" unit="char"></space><w n="21.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="21.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="21.3" punct="vg:5">pl<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg>ds</w>,</l>
						<l n="22" num="3.8" lm="5" met="5"><space quantity="6" unit="char"></space><w n="22.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="22.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>rs</w> <w n="22.3" punct="vg:5">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg>ds</w>,</l>
						<l n="23" num="3.9" lm="7" met="7"><w n="23.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="23.2">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="23.3" punct="vg:7"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="24" num="3.10" lm="5" met="5"><space quantity="6" unit="char"></space><w n="24.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="24.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="24.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rl<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w></l>
						<l n="25" num="3.11" lm="5" met="5"><space quantity="6" unit="char"></space><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="25.2">l</w>’<w n="25.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="25.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w></l>
						<l n="26" num="3.12" lm="7" met="7"><w n="26.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="26.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="26.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="26.4" punct="pt:7">n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="27" num="4.1" lm="7" met="7"><w n="27.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="27.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="27.3">n</w>’<w n="27.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="27.5">qu</w>’<w n="27.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.7"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="27.8" punct="vg:7">b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></w>,</l>
						<l n="28" num="4.2" lm="7" met="7"><w n="28.1" punct="vg:2">L<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="28.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="28.4" punct="pt:7">v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="29" num="5.1" lm="5" met="5"><space quantity="6" unit="char"></space><w n="29.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w> <w n="29.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="29.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="29.4">c<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>n<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w></l>
						<l n="30" num="5.2" lm="5" met="5"><space quantity="6" unit="char"></space><w n="30.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r</w> <w n="30.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="30.3">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w></l>
						<l n="31" num="5.3" lm="7" met="7"><w n="31.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="31.2">l</w>’<w n="31.3">h<seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="31.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="31.5" punct="pv:7">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></w> ;</l>
						<l n="32" num="5.4" lm="5" met="5"><space quantity="6" unit="char"></space><w n="32.1">C</w>’<w n="32.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="32.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="32.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w></l>
						<l n="33" num="5.5" lm="5" met="5"><space quantity="6" unit="char"></space><w n="33.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="33.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="2">oin</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="33.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w></l>
						<l n="34" num="5.6" lm="7" met="7"><w n="34.1">Qu</w>’<w n="34.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ls</w> <w n="34.3">vi<seg phoneme="ɛ" type="vs" value="1" rule="366" place="2">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="34.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="34.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="34.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="34.7" punct="pt:7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
						<l n="35" num="5.7" lm="5" met="5"><space quantity="6" unit="char"></space>— <w n="35.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="35.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ils</w> <w n="35.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ts</w></l>
						<l n="36" num="5.8" lm="5" met="5"><space quantity="6" unit="char"></space><w n="36.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="36.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="36.3" punct="vg:5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg">am</seg>ps</w>,</l>
						<l n="37" num="5.9" lm="7" met="7"><w n="37.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="37.2" punct="vg:3">c<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="o" type="vs" value="1" rule="318" place="3" punct="vg">au</seg>x</w>, <w n="37.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="37.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.5" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="38" num="5.10" lm="5" met="5"><space quantity="6" unit="char"></space><w n="38.1">D</w>’<w n="38.2">h<seg phoneme="i" type="vs" value="1" rule="d-4" place="1">y</seg><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>th<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="38.4">d</w>’<w n="38.5" punct="pv:5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="pv">o</seg>r</w> ;</l>
						<l n="39" num="5.11" lm="5" met="5"><space quantity="6" unit="char"></space><w n="39.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="39.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="39.3">s</w>’<w n="39.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt</w></l>
						<l n="40" num="5.12" lm="7" met="7"><w n="40.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="40.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="40.3">ch<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="40.4" punct="pt:7">l<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1" lm="7" met="7"><w n="41.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="41.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="41.3">n</w>’<w n="41.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="41.5">qu</w>’<w n="41.6"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="41.7"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="41.8" punct="vg:7">b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></w>,</l>
						<l n="42" num="6.2" lm="7" met="7"><w n="42.1" punct="vg:2">L<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="42.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="42.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="42.4" punct="pt:7">v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pt">é</seg></w>.</l>
					</lg>
				</div></body></text></TEI>