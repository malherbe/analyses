<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">RÉVOLTE</head><div type="poem" key="BAU129" modus="sm" lm_max="8" metProfile="8">
					<head type="number">CXXV</head>
					<head type="main">Abel et Caïn</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">d</w>’<w n="1.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="vg">e</seg>l</w>, <w n="1.4" punct="vg:5">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" punct="vg">o</seg>rs</w>, <w n="1.5">b<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.7" punct="pv:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg></w> <w n="2.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="2.4" punct="pt:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="pt">en</seg>t</w>.</l>
						</lg>
						<lg n="2">
							<l n="3" num="2.1" lm="8" met="8"><w n="3.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.3" punct="vg:5">C<seg phoneme="a" type="vs" value="1" rule="343" place="4">a</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="473" place="5" punct="vg">ïn</seg></w>, <w n="3.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="3.6">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="4" num="2.2" lm="8" met="8"><w n="4.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">am</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="4.3">m<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="4.4" punct="pt:8">m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="8" punct="pt">en</seg>t</w>.</l>
						</lg>
						<lg n="3">
							<l n="5" num="3.1" lm="8" met="8"><w n="5.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">d</w>’<w n="5.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="vg">e</seg>l</w>, <w n="5.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="5.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="6" num="3.2" lm="8" met="8"><w n="6.1">Fl<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.3">n<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="6.5" punct="pe:8">S<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ph<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pe">in</seg></w> !</l>
						</lg>
						<lg n="4">
							<l n="7" num="4.1" lm="8" met="8"><w n="7.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3" punct="vg:5">C<seg phoneme="a" type="vs" value="1" rule="343" place="4">a</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="473" place="5" punct="vg">ïn</seg></w>, <w n="7.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>ppl<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="8" num="4.2" lm="8" met="8"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w>-<w n="8.2">t</w>-<w n="8.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="8.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="8.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="8.6" punct="pi:8">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pi">in</seg></w> ?</l>
						</lg>
						<lg n="5">
							<l n="9" num="5.1" lm="8" met="8"><w n="9.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">d</w>’<w n="9.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="vg">e</seg>l</w>, <w n="9.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="9.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="9.6">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
							<l n="10" num="5.2" lm="8" met="8"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="10.3">b<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>il</w> <w n="10.4">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r</w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="10.6" punct="pv:8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" punct="pv">en</seg></w> ;</l>
						</lg>
						<lg n="6">
							<l n="11" num="6.1" lm="8" met="8"><w n="11.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.3" punct="vg:5">C<seg phoneme="a" type="vs" value="1" rule="343" place="4">a</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="473" place="5" punct="vg">ïn</seg></w>, <w n="11.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>tr<seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
							<l n="12" num="6.2" lm="8" met="8"><w n="12.1">H<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="12.3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4">aim</seg></w> <w n="12.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="12.6">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="12.7" punct="pt:8">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pt">en</seg></w>.</l>
						</lg>
						<lg n="7">
							<l n="13" num="7.1" lm="8" met="8"><w n="13.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.2">d</w>’<w n="13.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="vg">e</seg>l</w>, <w n="13.4">ch<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="13.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="14" num="7.2" lm="8" met="8"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="14.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="14.3">f<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="14.4" punct="pv:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rc<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg>l</w> ;</l>
						</lg>
						<lg n="8">
							<l n="15" num="8.1" lm="8" met="8"><w n="15.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.3" punct="vg:5">C<seg phoneme="a" type="vs" value="1" rule="343" place="4">a</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="473" place="5" punct="vg">ïn</seg></w>, <w n="15.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="15.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="15.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="16" num="8.2" lm="8" met="8"><w n="16.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.3" punct="vg:4">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="vg">oi</seg>d</w>, <w n="16.4">p<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="16.5" punct="pe:8">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>l</w> !</l>
						</lg>
						<lg n="9">
							<l n="17" num="9.1" lm="8" met="8"><w n="17.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2">d</w>’<w n="17.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="vg">e</seg>l</w>, <w n="17.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="17.6" punct="pe:8">p<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>ll<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
							<l n="18" num="9.2" lm="8" met="8"><w n="18.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="18.2"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r</w> <w n="18.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="18.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="18.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="18.6" punct="pt:8">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>ts</w>.</l>
						</lg>
						<lg n="10">
							<l n="19" num="10.1" lm="8" met="8"><w n="19.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="19.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.3" punct="vg:5">C<seg phoneme="a" type="vs" value="1" rule="343" place="4">a</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="473" place="5" punct="vg">ïn</seg></w>, <w n="19.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="19.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="19.6" punct="vg:8">br<seg phoneme="y" type="vs" value="1" rule="445" place="8">û</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="20" num="10.2" lm="8" met="8"><w n="20.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ds</w> <w n="20.2">g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="20.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="20.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ds</w> <w n="20.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>ts</w>.</l>
						</lg>
						<lg n="11">
							<l n="21" num="11.1" lm="8" met="8"><w n="21.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="21.2">d</w>’<w n="21.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="vg">e</seg>l</w>, <w n="21.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="21.5">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s</w> <w n="21.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="21.7">br<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
							<l n="22" num="11.2" lm="8" met="8"><w n="22.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="22.3">p<seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="22.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="22.5" punct="pe:8">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pe">oi</seg>s</w> !</l>
						</lg>
						<lg n="12">
							<l n="23" num="12.1" lm="8" met="8"><w n="23.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="23.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.3" punct="vg:5">C<seg phoneme="a" type="vs" value="1" rule="343" place="4">a</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="473" place="5" punct="vg">ïn</seg></w>, <w n="23.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="23.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="23.6">r<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
							<l n="24" num="12.2" lm="8" met="8"><w n="24.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="24.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="24.3">f<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>x</w> <w n="24.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>b<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>s</w>.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="25" num="1.1" lm="8" met="8"><w n="25.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="25.2">r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="25.3">d</w>’<w n="25.4" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="vg">e</seg>l</w>, <w n="25.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="25.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="26" num="1.2" lm="8" met="8"><w n="26.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="26.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="26.3">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>l</w> <w n="26.4" punct="pe:8">f<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pe">an</seg>t</w> !</l>
						</lg>
						<lg n="2">
							<l n="27" num="2.1" lm="8" met="8"><w n="27.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="27.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="27.3" punct="vg:5">C<seg phoneme="a" type="vs" value="1" rule="343" place="4">a</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="473" place="5" punct="vg">ïn</seg></w>, <w n="27.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="27.5">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="28" num="2.2" lm="8" met="8"><w n="28.1">N</w>’<w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="28.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="28.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="28.5" punct="pv:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8" punct="pv">en</seg>t</w> ;</l>
						</lg>
						<lg n="3">
							<l n="29" num="3.1" lm="8" met="8"><w n="29.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="29.2">d</w>’<w n="29.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="vg">e</seg>l</w>, <w n="29.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg></w> <w n="29.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="29.6" punct="dp:8">h<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
							<l n="30" num="3.2" lm="8" met="8"><w n="30.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="30.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="30.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="30.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="30.6">l</w>’<w n="30.7" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>pi<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg></w> !</l>
						</lg>
						<lg n="4">
							<l n="31" num="4.1" lm="8" met="8"><w n="31.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="31.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="31.3" punct="vg:5">C<seg phoneme="a" type="vs" value="1" rule="343" place="4">a</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="473" place="5" punct="vg">ïn</seg></w>, <w n="31.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="31.5">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>l</w> <w n="31.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
							<l n="32" num="4.2" lm="8" met="8"><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="32.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="32.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="32.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="32.5">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="32.6" punct="pe:8">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pe">eu</seg></w> !</l>
						</lg>
					</div>
				</div></body></text></TEI>