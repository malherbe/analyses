<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM29" modus="cm" lm_max="10" metProfile="4÷6">
				<head type="main">LE JARDINIER</head>
				<lg n="1">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">j<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>ni<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="1.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="1.6" punct="vg:10">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="2.3">fr<seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>tt<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="2.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="2.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg>s</w> <w n="2.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="7">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="2.8" punct="vg:10">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="3.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>s</w> <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="3.6">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="3.7">tr<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</w></l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="4.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3">c<seg phoneme="œ" type="vs" value="1" rule="345" place="3" mp="M">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="4.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="4.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.6" punct="pt:10">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l rhyme="none" n="5" num="2.1" lm="10" met="4+6"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg></w><caesura></caesura> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="5.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="5.7" punct="vg:10">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10" punct="vg">en</seg>ts</w>,</l>
					<l rhyme="none" n="6" num="2.2" lm="10" met="4+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg></w><caesura></caesura> <w n="6.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg></w> <w n="6.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="6.8">l<seg phoneme="y" type="vs" value="1" rule="453" place="9" mp="M">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
					<l rhyme="none" n="7" num="2.3" lm="10" met="4+6"><w n="7.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="7.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3" mp="M">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg></w><caesura></caesura> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="7.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="7.6">m<seg phoneme="u" type="vs" value="1" rule="428" place="9" mp="M">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w></l>
					<l rhyme="none" n="8" num="2.4" lm="10" met="4−6" mp6="M" mp5="F"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="8.2">s<seg phoneme="y" type="vs" value="1" rule="d-3" place="2" mp="M">u</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="8.3">ch<seg phoneme="o" type="vs" value="1" rule="318" place="4" caesura="1">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w><caesura></caesura> <w n="8.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" mp="M">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="8.6" punct="pt:10">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>ssi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="9.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="9.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="9.6">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r</w> <w n="9.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" mp="P">e</seg>rs</w> <w n="9.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="9.9">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>l</w></l>
					<l n="10" num="3.2" lm="10" met="6+4" mp4="C"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="10.2">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="10.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="10.5">t<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="10.6">r<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w><caesura></caesura> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="10.8">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>s</w></l>
					<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">i</seg>l</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="11.4">v<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg></w><caesura></caesura> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>rni<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="11.7">h<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
					<l n="12" num="3.4" lm="10" met="4+6"><w n="12.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="12.3">n<seg phoneme="ø" type="vs" value="1" rule="248" place="4" caesura="1">œu</seg>ds</w><caesura></caesura> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="12.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="12.7" punct="pt:10">p<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pt">on</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="10" met="4+6"><w n="13.1" punct="vg:2">L<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" punct="vg">em</seg>ps</w>, <w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="C">i</seg>l</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg></w><caesura></caesura> <w n="13.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="13.5">l</w>’<w n="13.6" punct="pv:10">h<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pv">on</seg></w> ;</l>
					<l n="14" num="4.2" lm="10" met="4+6"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="14.3">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="14.5">n<seg phoneme="wa" type="vs" value="1" rule="440" place="6" mp="M">o</seg>y<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="14.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="14.8" punct="vg:10">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="15" num="4.3" lm="10" met="4+6"><w n="15.1">S</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="353" place="2" mp="M">e</seg>ss<seg phoneme="ɥi" type="vs" value="1" rule="462" place="3" mp="M">u</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="15.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7">ain</seg></w> <w n="15.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="15.8" punct="vg:10">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9" mp="M">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="vg">on</seg></w>,</l>
					<l n="16" num="4.4" lm="10" met="4+6"><w n="16.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="16.2" punct="vg:4">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="16.3"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="C">i</seg>l</w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="16.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>s</w> <w n="16.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="16.7" punct="pt:10">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>