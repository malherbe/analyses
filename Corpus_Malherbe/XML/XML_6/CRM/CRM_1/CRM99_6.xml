<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM99" modus="sm" lm_max="8" metProfile="8">
				<head type="main">DIS, COMMENT FAISAIS-TU, MA MÈRE…</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="vg">i</seg>s</w>, <w n="1.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="3">en</seg>t</w> <w n="1.3">f<seg phoneme="œ" type="vs" value="1" rule="304" place="4">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w>-<w n="1.4" punct="vg:6">t<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg">u</seg></w>, <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6" punct="vg:8">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="2.2">m</w>’<w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r</w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2" punct="vg:3">s<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.5">br<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.6">cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="4.4">j<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="4.5">d</w>’<w n="4.6"><seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="4.7" punct="pi:8">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ffl<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pi">eu</seg>r</w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="5.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="5.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="5.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6" punct="vg:8">ch<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>c</w> <w n="6.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="6.4">d</w>’<w n="6.5" punct="pe:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></w> !</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="5">um</seg></w> <w n="7.4">d</w>’<w n="7.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.6">r<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="8.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="8.6">l</w>’<w n="8.7" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="9.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="9.3">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="9.5">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="9.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8">ain</seg></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="10.2">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w>-<w n="10.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6">ain</seg></w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.7">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="11.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="12.4">s<seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="12.5" punct="pi:8">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="8" punct="pi">eu</seg>s</w> ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="13.2">m<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>ts</w> <w n="13.3">t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="13.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.6">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="14.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ts</w> <w n="14.4" punct="vg:8">h<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg>x</w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="15.3">c<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>x</w> <w n="15.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="15.5">ru<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="16.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rs</w> <w n="16.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="16.6" punct="pt:8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="17.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w>-<w n="17.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="17.4" punct="vg:6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="6" punct="vg">en</seg>t</w>, <w n="17.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="17.6" punct="vg:8">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="18.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="18.5" punct="vg:8">gr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="e" type="vs" value="1" rule="383" place="7">e</seg>illi<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg>s</w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="19.2">f<seg phoneme="œ" type="vs" value="1" rule="304" place="2">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="19.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="19.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="19.5">l</w>’<w n="19.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w></l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="20.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="20.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="20.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">oû</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.6" punct="pt:8">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>