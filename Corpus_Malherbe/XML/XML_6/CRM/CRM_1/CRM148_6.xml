<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM148" modus="cp" lm_max="12" metProfile="6, 6=6">
				<head type="main">LES ARRACHEURS DE POMMES DE TERRE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="1.2" punct="vg:3"><seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>x</w>, <w n="1.3">l</w>’<w n="1.4">h<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="1.5">r<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="1.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="1.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="1.8" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rp<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>rri<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2" punct="vg:3"><seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg>x</w>, <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="2.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</w> <w n="2.6">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg></w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="2.8" punct="vg:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="vg">e</seg>il</w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="3.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="3.3">l</w>’<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r</w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5" mp="M">o</seg>mn<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="3.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="3.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="y" type="vs" value="1" rule="453" place="10">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-27" place="11" mp="Fc">e</seg></w> <w n="3.9" punct="vg:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rp<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="4" num="1.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="4.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="4.4" punct="pt:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" punct="pt">e</seg>il</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="5.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="5.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>p</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="5.5" punct="vg:6">f<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="5.7">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>l</w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="5.9">t<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rc<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">J<seg phoneme="a" type="vs" value="1" rule="307" place="1" mp="M">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="6.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="6.3">s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l</w> <w n="6.4"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>r</w><caesura></caesura> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>l<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="6.6">d</w>’<w n="6.7" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="318" place="11" mp="M">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="7.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>s</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w><caesura></caesura> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="7.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="7.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="10">i</seg></w> <w n="7.9">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="8" num="2.4" lm="6" met="6"><space unit="char" quantity="12"></space><w n="8.1">J<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="8.3">p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="8.4">d</w>’<w n="8.5" punct="pt:6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="pt">o</seg>r</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="M">Au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="9.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="9.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>t</w><caesura></caesura> <w n="9.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="9.6" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="307" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="10.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="10.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="10.6">s<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="307" place="12">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="11.2">s</w>’<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="11.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>cs</w> <w n="11.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="11.8" punct="pt:12">h<seg phoneme="o" type="vs" value="1" rule="318" place="12" punct="pt">au</seg>ts</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="12.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="12.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="12.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="12.7">m<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="9">en</seg>t</w> <w n="12.8">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-27" place="11" mp="Fc">e</seg></w> <w n="12.9" punct="vg:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="13.2" punct="vg:3">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg" mp="F">e</seg>s</w>, <w n="13.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="13.4" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">in</seg></w>,<caesura></caesura> <w n="13.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="13.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="13.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="13.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="14" num="4.3" lm="12" mp6="F" met="4+4+4"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>s</w><caesura></caesura> <w n="14.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="14.5">t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" mp="M">in</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="8" caesura="2">er</seg></w><caesura></caesura> <w n="14.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="P">ou</seg>r</w> <w n="14.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="14.8" punct="pt:12"><seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg>x</w>.</l>
				</lg>
			</div></body></text></TEI>