<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM117" modus="sm" lm_max="8" metProfile="8">
				<head type="main">A MI-VOIX</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Di<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w>-<w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="2">e</seg></w>-<w n="1.3" punct="vg:3">V<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>l</w>, <w n="1.4" punct="vg:6">G<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>st<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="1.5" punct="vg:8">L<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="2.2">p<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="2.6">d</w>’<w n="2.7" punct="pv:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1" punct="vg:3">Fl<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>l</w>, <w n="3.2" punct="vg:6">Pr<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg">a</seg>rt</w>, <w n="3.3" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="444" place="7">O</seg>h<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="vg">ain</seg></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="4.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="4.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="4.7" punct="pt:8">th<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="8" punct="pt">ym</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="5.2" punct="vg:2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>s</w>, <w n="5.3">L<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="5.5" punct="vg:8">R<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>si<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="6.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="6.5">t<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nn<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>x</w> <w n="6.6" punct="pt:8"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="pt">e</seg>rts</w>.</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="7.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="7.3" punct="vg:5">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>nt</w>, <w n="7.4">Gr<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w>-<w n="7.5" punct="vg:8">D<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>c<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="8.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="8.5" punct="pt:8"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg>x</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">W<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">s</w>’<w n="9.3"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="9.6" punct="dp:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg dp">e</seg></w>, :</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">n</w>’<w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.6">qu</w>’<w n="10.7"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="10.8">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w>-<w n="10.9">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>x</w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="11.2">l<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="11.3"><seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w>§<w n="11.4">t</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.6">p<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>ds</w> <w n="11.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="11.8">ch<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="12.3">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.5" punct="pt:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="341" place="1">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="13.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="13.3" punct="vg:4">m<seg phoneme="o" type="vs" value="1" rule="438" place="4" punct="vg">o</seg>ts</w>, <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="13.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="13.6" punct="vg:8">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="14.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="14.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="14.5" punct="vg:8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="vg">ain</seg></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>s</w> <w n="15.2">d</w>’<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="15.5" punct="vg:8">p<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">S<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>rs</w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">ju<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="16.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="16.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="16.6" punct="pt:8">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8" punct="pt">ain</seg></w>.</l>
				</lg>
			</div></body></text></TEI>