<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM19" modus="cm" lm_max="12" metProfile="6+6">
				<head type="main">AS-TU JAMAIS RÊVE…</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="Lp">A</seg>s</w>-<w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="1.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="1.4" punct="vg:6">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5" mp="M">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="1.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="1.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="1.8" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341" place="4" mp="M">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="2.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="2.6">s<seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="M">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="2.8">cr<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>t</w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="3.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="3.3">pi<seg phoneme="e" type="vs" value="1" rule="241" place="4">e</seg>ds</w> <w n="3.4" punct="vg:6">c<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>ri<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="3.5">d</w>’<w n="3.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.7">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="9">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="3.8" punct="dp:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></w> :</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="4.2" punct="vg:3">l<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>z<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>rd</w>, <w n="4.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="4.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="4.5" punct="vg:6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="4.6" punct="vg:9"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>mm<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.7"><seg phoneme="o" type="vs" value="1" rule="318" place="10" mp="C">au</seg></w> <w n="4.8" punct="pi:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="pi">e</seg>il</w> ?</l>
				</lg>
			</div></body></text></TEI>