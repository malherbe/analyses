<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM169" modus="sm" lm_max="8" metProfile="8">
				<head type="main">LA CLASSE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.6" punct="vg:8">cl<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="2.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8">in</seg></w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2">p<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>mmi<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="3.3">d</w>’<w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="3.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="3.6" punct="pt:8">j<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></w>.</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">M<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="4.2">d</w>’<w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="4.4" punct="pt:8">f<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8" met="8">‒ <w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">pr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>x</w> <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="5.5" punct="vg:5">s<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="vg">e</seg>l</w>, <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.3" punct="vg:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5" punct="vg">ain</seg></w>, <w n="6.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg></w>-<w n="6.6" punct="pi:8">c<seg phoneme="ə" type="ef" value="1" rule="e-1" place="9" punct="pi">e</seg></w> ?</l>
					<l n="7" num="2.3" lm="8" met="8">— <w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="7.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">ç<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="7.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.7" punct="pe:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="8.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6">j</w>’<w n="8.7" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">J</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="9.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="9.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="9.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="9.6">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1" punct="pt:2">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="pt">i</seg>s</w>. <w n="10.2">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="10.3">ch<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="10.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.5">l</w>’<w n="10.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="10.7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="11.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="11.3" punct="vg:4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="3">im</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="11.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="11.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="11.7" punct="pv:8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="pv">œu</seg>r</w> ;</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="12.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="12.3">d</w>’<w n="12.4" punct="vg:3"><seg phoneme="o" type="vs" value="1" rule="315" place="3" punct="vg">eau</seg></w>, <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.6">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="12.8" punct="pt:8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>r</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">L</w>’<w n="13.2" punct="vg:2">h<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="13.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="13.4">l</w>’<w n="13.5">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rl<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.7" punct="vg:8">ch<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="14.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="14.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>ps</w> <w n="14.7" punct="pt:8">fu<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pt">i</seg>t</w>.</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="15.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="411" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="15.5" punct="vg:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">C</w>’<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="16.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="16.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="16.7" punct="pt:8">lu<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pt">i</seg>t</w>.</l>
				</lg>
			</div></body></text></TEI>