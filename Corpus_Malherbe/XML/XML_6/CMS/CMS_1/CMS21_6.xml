<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES SONNETS DU DOCTEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="CMS">
					<name>
						<forename>Georges</forename>
						<surname>CAMUSET</surname>
					</name>
					<date from="1840" to="1885">1840-1885</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>522 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CMS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/BIUSante_80646</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Sonnets du docteur</title>
								<author>Georges Camuset</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Chez la plupart des libraires</publisher>
									<date when=" 1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
						<idno type="URL">https ://www.google.fr/books/edition/Les_sonnets_du_docteur/bsv60_A0jdUC</idno>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Chez la plupart des libraires</publisher>
							<date when=" 1884">1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poémes absents de l’édition numérique initiale ("Les Préservatifs" et "Du Signe certain de la mort") ont été ajoutés à partir du texte disponible sur Google books.</p>
				<p>Les notes qui suivent les titres dans la table des matières ont été mis en fin de poème.</p>
				<p>La lettre manuscrite de Charles Monselet n’est pas incluse dans la présente édition</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CMS21" modus="sm" lm_max="8" metProfile="8">
				<head type="main">LE SPÉCULUM</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:3">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>T<seg phoneme="i" type="vs" value="1" rule="467" place="2">I</seg>N<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" punct="vg">E</seg>TT<seg phoneme="ə" type="ee" value="0" rule="e-23">E</seg></w>, <w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="1.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S</w>’<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="2.5" punct="vg:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="vg">in</seg></w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>lt<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="3.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="3.5" punct="pt:8">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></w>.</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3" punct="pt:3">h<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="pt">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="4.7" punct="pt:8">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>st<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3">d</w>’<w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.5" punct="vg:8"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8">ain</seg></w></l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">Gu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rd</w> <w n="7.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8">ain</seg></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="8.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="2">ein</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="8.5">r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="8.6" punct="pt:8">n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">b<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>b<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg></w> <w n="9.4" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" punct="pt">e</seg>rt</w>.</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="10.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="10.4" punct="vg:4">fl<seg phoneme="a" type="vs" value="1" rule="341" place="4" punct="vg">a</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="10.6">n<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="10.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="10.8" punct="pe:8">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="pe">e</seg>r</w> !</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1" punct="tc:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="ti">ai</seg>s</w> — <w n="11.2"><seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg></w> <w n="11.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="11.6" punct="pe:8">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe ti">e</seg></w> ! —</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="8" met="8"><w n="12.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="12.2">c<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="12.4">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="5">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="12.5" punct="pt:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ffl<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></w>.</l>
					<l n="13" num="4.2" lm="8" met="8"><w n="13.1">L</w>’<w n="13.2" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>r</w>, <w n="13.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rb<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.5" punct="vg:8">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="14" num="4.3" lm="8" met="8"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="14.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.3">qu</w>’<w n="14.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="14.6" punct="pt:8">br<seg phoneme="y" type="vs" value="1" rule="445" place="7">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></w>.</l>
				</lg>
			</div></body></text></TEI>