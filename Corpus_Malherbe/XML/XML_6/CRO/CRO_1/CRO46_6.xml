<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PASSÉ</head><div type="poem" key="CRO46" modus="cm" lm_max="12" metProfile="6+6">
					<head type="main">Sur un éventail</head>
					<head type="form">Sonnet</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="1.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="1.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="1.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>rs</w><caesura></caesura> <w n="1.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="1.7" punct="vg:8">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" punct="vg">e</seg></w>, <w n="1.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="1.9" punct="vg:10">s<seg phoneme="wa" type="vs" value="1" rule="420" place="10" punct="vg">oi</seg>r</w>, <w n="1.10">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="12">an</seg>t</w></l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="2.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="2.4">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="2.5">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="6" caesura="1">eu</seg>s</w><caesura></caesura> <w n="2.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="2.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="2.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="2.9" punct="vg:12">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="11" mp="M">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>ssi<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="3.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="5" mp="C">o</seg>s</w> <w n="3.5" punct="vg:6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="3.6">pl<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>cs</w> <w n="3.8">d</w>’<w n="3.9" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="vg">en</seg>t</w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>rn<seg phoneme="wa" type="vs" value="1" rule="440" place="2" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="4.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="4.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="4.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.5">s</w>’<w n="4.6"><seg phoneme="i" type="vs" value="1" rule="497" place="8">y</seg></w> <w n="4.7">m<seg phoneme="u" type="vs" value="1" rule="428" place="9" mp="M">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="4.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="4.9" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1" mp="Lc">eu</seg>t</w>-<w n="5.2" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="5.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="5.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>s</w><caesura></caesura> <w n="5.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>s</w>, <w n="5.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="5.8" punct="vg:12">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="12" punct="vg">an</seg>t</w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="6.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="6.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="6.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="6.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="6.7">ch<seg phoneme="o" type="vs" value="1" rule="444" place="10">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="6.8" punct="vg:12">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">Qu</w>’<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">à</seg></w> <w n="7.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" mp="M">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="7.6">f<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>s</w> <w n="7.7">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="12">an</seg>t</w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="8.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="8.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="8.5">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="8" mp="M">oi</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rs</w> <w n="8.6" punct="pt:12"><seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="P">u</seg>r</w> <w n="9.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" mp="M">en</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="6" punct="vg" caesura="1">a</seg>il</w>,<caesura></caesura> <w n="9.5">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg></w> <w n="9.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="9">e</seg></w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="P">a</seg>r</w> <w n="9.8">h<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rd</w></l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">In</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="10.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>str<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="10.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="Fc">e</seg></w> <w n="10.6" punct="pv:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pv">a</seg>rd</w> ;</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="11.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="11.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="11.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6" caesura="1">e</seg>rs</w><caesura></caesura> <w n="11.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="11.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="11.8">l</w>’<w n="11.9" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">l</w>’<w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="5">ez</seg></w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="12.6">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="12.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="12.8">v<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>t</w> <w n="12.9">pl<seg phoneme="wa" type="vs" value="1" rule="440" place="11" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
						<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="13.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="13.3">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="C">au</seg>x</w> <w n="13.5">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="9">oin</seg>s</w> <w n="13.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="13.7">f<seg phoneme="wa" type="vs" value="1" rule="440" place="11" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
						<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="14.4">D<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="6" caesura="1">a</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="14.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="14.6">b<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="M">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.7" punct="pe:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>