<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GRAINS DE SEL</head><div type="poem" key="CRO91" modus="sp" lm_max="8" metProfile="8, 5">
					<head type="main">Chanson des Sculpteurs</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">Pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>cl<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="1.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>p</w>’<w n="1.4">s</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.6">l</w>’<w n="1.7" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>rt</w> !</l>
						<l n="2" num="1.2" lm="5" met="5"><space quantity="8" unit="char"></space><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="2.3">l</w>’<w n="2.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d</w>’ <w n="2.5">s</w>’<w n="2.6" punct="pe:5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w>’ <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="3.7" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>rt</w>,</l>
						<l n="4" num="1.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="4.1">Y</w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="4.3">n</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="4.6">d</w>’<w n="4.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="4.8" punct="pt:5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">Pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>cl<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="5.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>p</w>’<w n="5.4">s</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6">l</w>’<w n="5.7" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>rt</w> !</l>
						<l n="6" num="2.2" lm="5" met="5"><space quantity="8" unit="char"></space><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rs<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nn</w>’ <w n="6.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4" punct="pe:5">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rr</w>’ <w n="7.3" punct="vg:3">gl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="in vg">ai</seg>s</w>’, <w n="7.4">c</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="7.6">c<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>mm</w>’ <w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="7.8" punct="pv:8">h<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg>rd</w> ;</l>
						<l n="8" num="2.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="8.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="8.2">c</w>’<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="8.4" punct="vg:3">cu<seg phoneme="i" type="vs" value="1" rule="491" place="3" punct="vg">i</seg>t</w>, <w n="8.5">c</w>’<w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="8.7" punct="pt:5">r<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1">Pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>cl<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="9.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>p</w>’<w n="9.4">s</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.6">l</w>’<w n="9.7" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>rt</w> !</l>
						<l n="10" num="3.2" lm="5" met="5"><space quantity="8" unit="char"></space><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="10.3">l</w>’<w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d</w>’ <w n="10.5">s</w>’<w n="10.6" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">br<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>z</w>’ <w n="11.3" punct="vg:3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="11.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>s</w> <w n="11.6">qu</w>’ <w n="11.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r</w> <w n="11.8" punct="vg:8">h<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>rd</w>,</l>
						<l n="12" num="3.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>ch</w>’<w n="12.4">s</w> <w n="12.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="12.6">n</w>’ <w n="12.7">l</w>’<w n="12.8" punct="pt:5"><seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">Pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>cl<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="13.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>p</w>’<w n="13.4">s</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="13.6">l</w>’<w n="13.7" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>rt</w> !</l>
						<l n="14" num="4.2" lm="5" met="5"><space quantity="8" unit="char"></space><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="14.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="14.3">l</w>’<w n="14.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d</w>’ <w n="14.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.6" punct="pe:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">oû</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">Qu<seg phoneme="wa" type="vs" value="1" rule="281" place="1">oi</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2">l</w>’ <w n="15.3">pl<seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>tr</w>’ <w n="15.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t</w> <w n="15.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="15.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg></w> <w n="15.7" punct="vg:8">bl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>f<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>rd</w>,</l>
						<l n="16" num="4.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="16.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>l</w>’ <w n="16.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="16.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="16.5">l</w>’<w n="16.6" punct="pt:5">m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">Pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>cl<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="17.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>p</w>’<w n="17.4">s</w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="17.6">l</w>’<w n="17.7" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pe">a</seg>rt</w> !</l>
						<l n="18" num="5.2" lm="5" met="5"><space quantity="8" unit="char"></space><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="18.3">l</w>’<w n="18.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d</w>’ <w n="18.5">s</w>’<w n="18.6" punct="pe:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rs</w> <w n="19.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="19.4">f<seg phoneme="ɛ" type="vs" value="1" rule="366" place="5" punct="vg">e</seg>mm</w>’<w n="19.5" punct="vg:5">s</w>, <w n="19.6">c</w>’<w n="19.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="19.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="19.9" punct="vg:8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>rd</w>,</l>
						<l n="20" num="5.4" lm="5" met="5"><space quantity="8" unit="char"></space><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="20.2" punct="vg:2">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" punct="vg">ai</seg>r</w>, <w n="20.3">c</w>’<w n="20.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="20.5">d</w>’ <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="20.7" punct="pt:5">vi<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>