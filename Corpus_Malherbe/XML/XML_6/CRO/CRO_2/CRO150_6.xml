<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DOULEURS ET COLÈRES</head><head type="sub_part">Vers trouvés sur la berge</head><div type="poem" key="CRO150" modus="sp" lm_max="7" metProfile="5, 7">
					<head type="main">Aux imbéciles</head>
					<lg n="1">
						<l n="1" num="1.1" lm="5" met="5"><space quantity="6" unit="char"></space><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="1.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w></l>
						<l n="2" num="1.2" lm="5" met="5"><space quantity="6" unit="char"></space><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="2.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="2.3">h<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w></l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1">D</w>’<w n="3.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.5" punct="vg:7">cu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="5" met="5"><space quantity="6" unit="char"></space><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2">en</seg>s</w> <w n="4.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w></l>
						<l n="5" num="1.5" lm="5" met="5"><space quantity="6" unit="char"></space><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="354" place="1">E</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>pts</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w></l>
						<l n="6" num="1.6" lm="7" met="7"><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">d<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="6.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="6.5" punct="pt:7">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>rsu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1" lm="5" met="5"><space quantity="6" unit="char"></space><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="3">en</seg>t</w> <w n="7.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="7.4" punct="vg:5">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" punct="vg">in</seg></w>,</l>
						<l n="8" num="2.2" lm="5" met="5"><space quantity="6" unit="char"></space><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="8.3">m<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rt</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.5" punct="vg:5">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5" punct="vg">aim</seg></w>,</l>
						<l n="9" num="2.3" lm="7" met="7"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="9.2">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="2">ou</seg><seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.5" punct="vg:7">gu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="10" num="2.4" lm="5" met="5"><space quantity="6" unit="char"></space><w n="10.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="10.2">n</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="10.4" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg">é</seg></w>,</l>
						<l n="11" num="2.5" lm="5" met="5"><space quantity="6" unit="char"></space><w n="11.1">L</w>’<w n="11.2">h<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>r</w> <w n="11.3">n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="11.4">l</w>’<w n="11.5" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5" punct="vg">é</seg></w>,</l>
						<l n="12" num="2.6" lm="7" met="7"><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">tr<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="12.4">d</w>’<w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>c<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="12.6" punct="pt:7">g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1" lm="5" met="5"><space quantity="6" unit="char"></space><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5">e</seg>r</w></l>
						<l n="14" num="3.2" lm="5" met="5"><space quantity="6" unit="char"></space><w n="14.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="14.2">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="3">en</seg>t</w> <w n="14.3">tr<seg phoneme="o" type="vs" value="1" rule="433" place="4">o</seg>p</w> <w n="14.4" punct="pt:5">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="5" punct="pt">e</seg>r</w>.</l>
						<l n="15" num="3.3" lm="7" met="7"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">ste<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="15.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.5">l</w>’<w n="15.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="16" num="3.4" lm="5" met="5"><space quantity="6" unit="char"></space><w n="16.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="16.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="16.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>r</w> <w n="16.4" punct="pv:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5" punct="pv">o</seg>r</w> ;</l>
						<l n="17" num="3.5" lm="5" met="5"><space quantity="6" unit="char"></space><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="17.2">f<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="17.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>p</w> <w n="17.4">d</w>’<w n="17.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w></l>
						<l n="18" num="3.6" lm="7" met="7"><w n="18.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="18.4">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="18.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="18.6" punct="pt:7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1" lm="5" met="5"><space quantity="6" unit="char"></space><w n="19.1" punct="vg:1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg>c</w>, <w n="19.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2">en</seg>s</w> <w n="19.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="19.4" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>s</w>,</l>
						<l n="20" num="4.2" lm="5" met="5"><space quantity="6" unit="char"></space><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="354" place="1">E</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>pts</w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.3" punct="vg:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>s</w>,</l>
						<l n="21" num="4.3" lm="7" met="7"><w n="21.1">M<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>f<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="21.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="21.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="21.4" punct="vg:7">p<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="22" num="4.4" lm="5" met="5"><space quantity="6" unit="char"></space><w n="22.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="22.2" punct="vg:2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>t</w>, <w n="22.3"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="22.4" punct="vg:5">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5" punct="vg">aim</seg></w>,</l>
						<l n="23" num="4.5" lm="5" met="5"><space quantity="6" unit="char"></space><w n="23.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="23.2" punct="vg:2">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" punct="vg">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="23.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="23.5" punct="vg:5">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" punct="vg">in</seg></w>,</l>
						<l n="24" num="4.6" lm="7" met="7"><w n="24.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>s</w> <w n="24.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="24.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="24.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="24.5" punct="pt:7">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>