<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DOULEURS ET COLÈRES</head><head type="sub_part">Vers trouvés sur la berge</head><div type="poem" key="CRO154" modus="sp" lm_max="7" metProfile="7, 5">
					<head type="main">Le Propriétaire</head>
					<lg n="1">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1">N<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg></w> <w n="1.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="1.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.4">tr<seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ls<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7">ain</seg></w></l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1">D</w>’<w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rgn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="2.5" punct="vg:7">L<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="vg">in</seg></w>,</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="3.2">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="3.3">d</w>’<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rd</w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="3.6" punct="pt:7">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
						<l n="4" num="1.4" lm="7" met="7"><w n="4.1" punct="vg:2">H<seg phoneme="œ̃" type="vs" value="1" rule="261" place="1">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="4.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="4.3" punct="vg:5">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg>r</w>, <w n="4.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="4.5" punct="pv:7">b<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="pv">u</seg>t</w> ;</l>
						<l n="5" num="1.5" lm="7" met="7"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.4">m<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="5.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>b<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>t</w></l>
						<l n="6" num="1.6" lm="5" met="5"><space quantity="8" unit="char"></space><w n="6.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="6.2" punct="pt:5">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1" lm="7" met="7"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="7.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="7.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>ps</w> <w n="7.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="7.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="7.7">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg>x</w></l>
						<l n="8" num="2.2" lm="7" met="7"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="8.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>ts</w></l>
						<l n="9" num="2.3" lm="7" met="7"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="9.2">qu<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="9.5">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="10" num="2.4" lm="7" met="7"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="10.2" punct="vg:2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rt</w>, <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="10.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5">ein</seg></w> <w n="10.6">d</w>’<w n="10.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r</w></l>
						<l n="11" num="2.5" lm="7" met="7"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="11.2">n</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="11.5">l</w>’<w n="11.6" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>r</w>, <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="11.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.9" punct="vg:7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="vg">oi</seg>r</w>,</l>
						<l n="12" num="2.6" lm="5" met="5"><space quantity="8" unit="char"></space><w n="12.1">D</w>’<w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="12.3" punct="pt:5">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1" lm="7" met="7"><w n="13.1">D</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd</w> <w n="13.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="13.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="13.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="13.6">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7">ain</seg></w></l>
						<l n="14" num="3.2" lm="7" met="7"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="14.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d</w> <w n="14.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="14.4">p<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg>x</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.6" punct="pt:7">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="pt">in</seg></w>.</l>
						<l n="15" num="3.3" lm="7" met="7"><w n="15.1">Qu<seg phoneme="wa" type="vs" value="1" rule="281" place="1">oi</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.3">c<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lt<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="16" num="3.4" lm="7" met="7"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="16.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">b<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="16.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t</w></l>
						<l n="17" num="3.5" lm="7" met="7"><w n="17.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="17.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="17.4" punct="vg:7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7" punct="vg">en</seg>t</w>,</l>
						<l n="18" num="3.6" lm="5" met="5"><space quantity="8" unit="char"></space><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="18.2" punct="pt:5">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1" lm="7" met="7"><w n="19.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="19.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="19.3">m<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="19.4">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6">o</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w></l>
						<l n="20" num="4.2" lm="7" met="7"><w n="20.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rgn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="20.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="20.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="20.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="20.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="7">œu</seg>r</w></l>
						<l n="21" num="4.3" lm="7" met="7"><w n="21.1">F<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rç<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="21.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.4" punct="pe:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe ps">e</seg></w> !…</l>
						<l n="22" num="4.4" lm="7" met="7"><w n="22.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="22.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="22.4">f<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w></l>
						<l n="23" num="4.5" lm="7" met="7"><w n="23.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="23.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="23.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="23.5" punct="vg:7">p<seg phoneme="y" type="vs" value="1" rule="450" place="7" punct="vg">u</seg>r</w>,</l>
						<l n="24" num="4.6" lm="5" met="5"><space quantity="8" unit="char"></space><w n="24.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="24.2" punct="pt:5">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1" lm="7" met="7"><w n="25.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="25.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>t</w> <w n="25.3">d</w>’<w n="25.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rd</w> <w n="25.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w></l>
						<l n="26" num="5.2" lm="7" met="7"><w n="26.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="26.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="26.3">s</w>’<w n="26.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w></l>
						<l n="27" num="5.3" lm="7" met="7"><w n="27.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="27.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="27.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="27.6" punct="vg:7">m<seg phoneme="i" type="vs" value="1" rule="493" place="6">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="28" num="5.4" lm="7" met="7"><w n="28.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="28.2">d</w>’<w n="28.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rd</w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="28.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="28.6" punct="vg:7">l<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></w>,</l>
						<l n="29" num="5.5" lm="7" met="7"><w n="29.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="29.2"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>r</w> <w n="29.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="29.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="29.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="29.6" punct="vg:7">r<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="vg">oi</seg></w>,</l>
						<l n="30" num="5.6" lm="5" met="5"><space quantity="8" unit="char"></space><w n="30.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="30.2" punct="pt:5">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>