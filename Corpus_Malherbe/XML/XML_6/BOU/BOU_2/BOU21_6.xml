<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU21" modus="cm" lm_max="12" metProfile="6+6">
				<head type="main">L’HALLALI</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="1.3" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="dc-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="vg" caesura="1">on</seg>s</w>,<caesura></caesura> <w n="1.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="1.6">m<seg phoneme="ø" type="vs" value="1" rule="399" place="10">eu</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.7" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11" mp="M">in</seg>f<seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">En</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="2.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="2.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="2.4" punct="vg:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg" caesura="1">œu</seg>r</w>,<caesura></caesura> <w n="2.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="2.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="2.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="P">a</seg>r</w> <w n="2.8" punct="dp:12">m<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>lli<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="dp">er</seg>s</w> :</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">M<seg phoneme="o" type="vs" value="1" rule="444" place="1" mp="M">o</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="F">e</seg>s</w> <w n="3.2" punct="vg:6">h<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>ts</w>,<caesura></caesura> <w n="3.3">d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="œ" type="vs" value="1" rule="286" place="10">œ</seg>il</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="3.8" punct="vg:12">fl<seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="4.2">h<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="4.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>b<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="4.7">tr<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="9">e</seg>rs</w> <w n="4.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="4.9" punct="pv:12">h<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>lli<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pv">er</seg>s</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="5.3">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="5.5" punct="vg:6">r<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="vg" caesura="1">in</seg>s</w>,<caesura></caesura> <w n="5.6" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="189" place="7" punct="vg">e</seg>t</w>, <w n="5.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="5.9">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>rf</w> <w n="5.10">qu<seg phoneme="i" type="vs" value="1" rule="491" place="11">i</seg></w> <w n="5.11" punct="vg:12">br<seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">J</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="6.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="6.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="6.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>g</w><caesura></caesura> <w n="6.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="6.8">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="6.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="6.10" punct="pt:12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11" mp="M">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg>s</w>.</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">L</w>’<w n="7.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="7.3">f<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="7.4">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="C">au</seg></w> <w n="7.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>d</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="7.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="7.9" punct="pe:12"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></w> !</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">J</w>’<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="8.4">bru<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="8.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rs</w><caesura></caesura> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="8.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="8.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="8.11" punct="pe:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>rsi<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pe">er</seg>s</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="9.3">chi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="4">en</seg>s</w> <w n="9.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>gr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="9.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="9.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rcl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="9.9">c<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w> ]</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1" punct="vg:1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>s</w>, <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="10.3">j<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>ts</w> <w n="10.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="10.7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.8" punct="vg:12">t<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="11.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="11.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r</w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="11.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="10" mp="C">un</seg></w> <w n="11.8" punct="ps:12">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="ps">eau</seg></w>…</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="12.3" punct="pe:3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" punct="pe">em</seg>ps</w> ! <w n="12.4"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="C">i</seg>l</w> <w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="12.6" punct="pe:6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" punct="pe ti" caesura="1">em</seg>ps</w> !<caesura></caesura> — <w n="12.7">T<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w> <w n="12.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="12.9">su<seg phoneme="i" type="vs" value="1" rule="491" place="9" mp="M">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</w> <w n="12.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="12.11" punct="vg:12">ch<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1" punct="pe:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="pe">en</seg>s</w> ! <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="13.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="13.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="13.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="13.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="13.7">v<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="13.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="13.9">c<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>p</w> <w n="13.10">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="13.11" punct="pe:12">gr<seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></w> !</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="14.2">f<seg phoneme="a" type="vs" value="1" rule="193" place="2">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="14.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="14.5" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="14.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="14.8" punct="pe:12">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pe">eau</seg></w> !</l>
				</lg>
			</div></body></text></TEI>