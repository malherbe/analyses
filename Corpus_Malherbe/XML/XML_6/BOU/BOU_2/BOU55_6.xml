<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU55" modus="cm" lm_max="12" metProfile="6+6">
				<head type="main">A X.</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Tristes Deos !</quote>
							<bibl>
								<name>Horatius</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="1.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="1.4" punct="vg:6">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="1.5">p<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="8">ë</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg>x</w> <w n="1.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="1.8" punct="vg:12">r<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">V<seg phoneme="wa" type="vs" value="1" rule="440" place="1" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="2.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" mp="P">e</seg>rs</w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="2.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="2.6" punct="vg:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="vg">e</seg>ils</w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="3.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="3.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="3.5">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6" caesura="1">e</seg>ds</w><caesura></caesura> <w n="3.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="3.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>ssi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="3.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="4.3" punct="vg:3">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3" punct="vg">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="4.5" punct="vg:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="4.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="4.7">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="8" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="9">y</seg>s</w> <w n="4.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="4.9" punct="pe:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="pe">e</seg>ils</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">t</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="5.5">v<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="5.6" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="5.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="5.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="5.9">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.10"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="5.11">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="5.12" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1" punct="vg:2">Tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="1" mp="M">aî</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="6.4" punct="vg:6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="vg" caesura="1">eau</seg></w>,<caesura></caesura> <w n="6.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="6.6" punct="vg:12">p<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="7.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="7.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="7.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" caesura="1">ain</seg>s</w><caesura></caesura> <w n="7.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="7.8">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="7.9">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="7.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="7.11" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="8.3" punct="vg:3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3" punct="vg">œu</seg>r</w>, <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="8.5" punct="vg:6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="8.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="8.7" punct="pe:12"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pe">é</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="9.3" punct="vg:3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>s</w>, <w n="9.4" punct="vg:6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" mp="M">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="9.5">qu</w>’<w n="9.6"><seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="8" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="9.9" punct="vg:12">l<seg phoneme="i" type="vs" value="1" rule="493" place="12">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="10.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ds</w> <w n="10.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>s</w><caesura></caesura> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="10.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">O</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="10">ym</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="10.7" punct="vg:12">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg></w>,</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="11.4">t</w>’<w n="11.5" punct="vg:6"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="11.6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w> <w n="11.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg></w> <w n="11.8">t</w>’<w n="11.9"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg></w> <w n="11.10">v<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg></w> <w n="11.11" punct="ps:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="ps" mp="F">e</seg></w>…</l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">J</w>’<w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="12.3" punct="vg:3">cr<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg></w>, <w n="12.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="Lc">u</seg>squ</w>’<w n="12.5" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M/mc">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="12.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="12.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="12.8">n</w>’<w n="12.9"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="12.10">qu</w>’<w n="12.11"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11">un</seg></w> <w n="12.12" punct="pe:12">di<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg></w> !</l>
				</lg>
			</div></body></text></TEI>