<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU52" modus="cm" lm_max="10" metProfile="4+6">
				<head type="main">CHATTERIE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="1.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="1.4" punct="vg:4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg" caesura="1">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="1.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg>s</w> <w n="1.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>gs</w> <w n="1.8" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="2.2">f<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="2.4">l<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" mp="M">ai</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="2.7" punct="vg:10">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg></w>,</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="3.2" punct="vg:4">c<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>rb<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg" caesura="1">ai</seg>t</w>,<caesura></caesura> <w n="3.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="3.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>t</w> <w n="3.6" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="vg">i</seg></w>,</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="4.2">d<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="4.3" punct="vg:4">fr<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>l<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="4.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>s</w> <w n="4.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="4.6">f<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>rr<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="4.7" punct="pt:10">gr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="5.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="5.3" punct="vg:4">m<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="5.5">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="5.6" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="M">am</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="10" punct="vg">u</seg>s</w>,</l>
					<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="6.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="307" place="4" caesura="1">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="6.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="6.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="6.8" punct="pv:10">gr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg>s</w> ;</l>
					<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="7.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>gs</w> <w n="7.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="7.4" punct="vg:4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg" caesura="1">e</seg>rts</w>,<caesura></caesura> <w n="7.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="7.6">d<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="7.7" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>sc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>gr<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" caesura="1">en</seg>t</w><caesura></caesura> <w n="8.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>rs</w> <w n="8.4">c<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>ls</w> <w n="8.5" punct="pt:10"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="10" punct="pt">u</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="9.2" punct="vg:2">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="2" punct="vg">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="9.4">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" caesura="1">o</seg>rd</w><caesura></caesura> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="9.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="9.8" punct="vg:10">f<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="10" num="3.2" lm="10" met="4+6"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="10.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>t</w><caesura></caesura> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="10.5">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ts</w> <w n="10.6">cr<seg phoneme="o" type="vs" value="1" rule="437" place="8">o</seg>cs</w> <w n="10.7">d</w>’<w n="10.8" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307" place="10" punct="vg">a</seg>il</w>,</l>
					<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="11.3">n<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="11.4" punct="vg:4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" punct="vg" caesura="1">in</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="11.6">r<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="11.7" punct="vg:10">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>r<seg phoneme="a" type="vs" value="1" rule="307" place="10" punct="vg">a</seg>il</w>,</l>
					<l n="12" num="3.4" lm="10" met="4+6"><w n="12.1">D</w>’<w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="12.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="12.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>s</w><caesura></caesura> <w n="12.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="12.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="12.7">b<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="12.8" punct="pt:10">f<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="10" met="4+6"><w n="13.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="13.3" punct="po:4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" caesura="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> (<w n="13.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="13.5">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="13.7" punct="pf:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w>)</l>
					<l n="14" num="4.2" lm="10" met="4+6"><w n="14.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">em</seg>bl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="14.2">fr<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>r</w><caesura></caesura> <w n="14.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>s</w> <w n="14.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="14.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="8" mp="F">e</seg>s</w> <w n="14.6" punct="vg:10">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="9" mp="M">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="15" num="4.3" lm="10" met="4+6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">j</w>’<w n="15.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="15.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6" mp="C">un</seg></w> <w n="15.6">bru<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>t</w> <w n="15.7">d</w>’<w n="15.8"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</w></l>
					<l n="16" num="4.4" lm="10" met="4+6"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="16.2">s</w>’<w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">ai</seg>t</w><caesura></caesura> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="16.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="16.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="M">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="16.7" punct="pe:10">m<seg phoneme="wa" type="vs" value="1" rule="420" place="9" mp="M">oi</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="pe ps">é</seg></w> !…</l>
				</lg>
			</div></body></text></TEI>