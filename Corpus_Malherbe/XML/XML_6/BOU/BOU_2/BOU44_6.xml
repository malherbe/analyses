<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU44" modus="sm" lm_max="7" metProfile="7">
				<head type="main">BUCOLIQUE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1" punct="vg:1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" punct="vg">an</seg>d</w>, <w n="1.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="1.4">bl<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="1.5" punct="vg:7">m<seg phoneme="y" type="vs" value="1" rule="445" place="7" punct="vg">û</seg>rs</w>,</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="2.4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="3.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rt</w> <w n="3.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="3.4">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="3.5"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>rs</w></l>
					<l n="4" num="1.4" lm="7" met="7"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="4.3" punct="vg:7">pr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</w>,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="7" met="7"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="5.2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="5.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="5.4">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="5.6" punct="vg:7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="vg">in</seg></w>,</l>
					<l n="6" num="2.2" lm="7" met="7"><w n="6.1">M<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>ss<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>nn<seg phoneme="ø" type="vs" value="1" rule="403" place="3">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="6.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="6.4">p<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="6.5">br<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
					<l n="7" num="2.3" lm="7" met="7"><w n="7.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="7.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="7.3" punct="vg:4">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="7.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="7.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7">ain</seg></w></l>
					<l n="8" num="2.4" lm="7" met="7"><w n="8.1">Ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>t</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="8.6" punct="pv:7">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="7" met="7"><w n="9.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="9.4" punct="vg:7">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>rm<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="7" punct="vg">en</seg>t</w>,</l>
					<l n="10" num="3.2" lm="7" met="7"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2" punct="vg:3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3" punct="vg">au</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="10.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="10.5" punct="vg:7">h<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="7">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="11" num="3.3" lm="7" met="7"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.4">d<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w></l>
					<l n="12" num="3.4" lm="7" met="7"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="12.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="12.6" punct="pt:7">pl<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="7" met="7"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>ps</w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="5">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="13.6" punct="vg:7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" punct="vg">o</seg>rt</w>,</l>
					<l n="14" num="4.2" lm="7" met="7"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="14.2">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="14.4" punct="vg:7">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="15" num="4.3" lm="7" met="7"><w n="15.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="15.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="15.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="15.6" punct="vg:7">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" punct="vg">o</seg>rt</w>,</l>
					<l n="16" num="4.4" lm="7" met="7"><w n="16.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="16.2">s<seg phoneme="y" type="vs" value="1" rule="d-3" place="2">u</seg><seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="16.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="16.5" punct="pv:7">r<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></w> ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="7" met="7"><w n="17.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="17.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="17.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>d</w> <w n="17.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c</w> <w n="17.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="17.6" punct="vg:7">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></w>,</l>
					<l n="18" num="5.2" lm="7" met="7"><w n="18.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="18.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="18.4">j<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
					<l n="19" num="5.3" lm="7" met="7"><w n="19.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="19.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="19.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="19.4">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5">ain</seg>s</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="19.6">bl<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w>^</l>
					<l n="20" num="5.4" lm="7" met="7"><w n="20.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="20.2">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="20.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="20.4" punct="pv:7"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg>s</w> ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1" lm="7" met="7"><w n="21.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="21.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="21.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4">en</seg>t</w> <w n="21.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="21.6" punct="vg:7">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="vg">eu</seg></w>,</l>
					<l n="22" num="6.2" lm="7" met="7"><w n="22.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.2" punct="vg:3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>rt</w>, <w n="22.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4">aî</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="22.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="22.5" punct="vg:7">v<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</w>,</l>
					<l n="23" num="6.3" lm="7" met="7"><w n="23.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="23.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="23.3">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>ni<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="23.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="23.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="23.6">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg></w></l>
					<l n="24" num="6.4" lm="7" met="7"><w n="24.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="24.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="24.3">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="24.4">d</w>’<w n="24.5" punct="pt:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>