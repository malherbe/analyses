<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES. FESTONS ET ASTRAGALES</title>
				<title type="sub"></title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2963 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>POÉSIES. FESTONS ET ASTRAGALES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k9737201m</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES. FESTONS ET ASTRAGALES</title>
								<author>Louis Bouilhet</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE NOUVELLE</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1859">1859</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU47" modus="sm" lm_max="8" metProfile="8">
				<head type="main">LE CRAPAUD</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.3" punct="vg:4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="vg">en</seg>d</w>, <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="1.7" punct="vg:8">br<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="2.3">bru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>ts</w> <w n="2.4">m<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>nt</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.7" punct="pv:8">f<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pv">oi</seg>s</w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1" punct="vg:1">S<seg phoneme="œ" type="vs" value="1" rule="407" place="1" punct="vg">eu</seg>l</w>, <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="3.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="3.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>x<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>s</w> <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="3.7" punct="vg:8">l<seg phoneme="y" type="vs" value="1" rule="453" place="8">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">cr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>d</w> <w n="4.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="4.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rd</w> <w n="4.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="4.7" punct="pt:8">b<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="5.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="5.3">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="5.4">qu</w>’<w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="5.6">li<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.7">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>st<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="6.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w> <w n="6.3" punct="vg:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="3">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="6.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="6.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6">en</seg>t</w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.7" punct="pv:8">s<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pv">oi</seg>r</w> ;</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">fl<seg phoneme="y" type="vs" value="1" rule="445" place="4">û</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.4" punct="vg:8">m<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>x</w> <w n="8.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="7">e</seg>l</w> <w n="8.7" punct="pt:8">n<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="9.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="9.4">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="9.5" punct="pe:8">c<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w>-<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="10.5">l</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="11.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="11.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.5">s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="12.5">nu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>ts</w> <w n="12.6">d</w>’<w n="12.7" punct="pi:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pi">é</seg></w> ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1">Cr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w>-<w n="13.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="13.3">qu</w>’<w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg></w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="13.7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="14.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="14.3">tr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="14.4">d</w>’<w n="14.5" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>p<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg></w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="15.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>g</w> <w n="15.3">d</w>’<w n="15.4"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>mm<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="16.2">J<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>l<seg phoneme="i" type="vs" value="1" rule="dc-1" place="3">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="16.3" punct="pi:8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pi ps">a</seg></w> ?…</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="17.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="17.3">l</w>’<w n="17.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="17.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="17.7" punct="vg:8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="18.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.3" punct="vg:4">l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="18.4">s</w>’<w n="18.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="d-3" place="7">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="19.4">d</w>’<w n="19.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="19.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="19.7" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">R<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg></w> <w n="20.2">s<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="20.4" punct="pt:8">gl<seg phoneme="y" type="vs" value="1" rule="454" place="7">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="21.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="21.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="21.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="21.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="21.6" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="22.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="22.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="22.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="22.5" punct="vg:8">j<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>r</w>,</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1" punct="vg:2">Tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="1">aî</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="23.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="23.3">l</w>’<w n="23.4" punct="vg:5">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="23.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="23.6">gr<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="23.7" punct="vg:8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="8">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1">Pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="1">ein</seg></w> <w n="24.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">p<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="24.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg></w> <w n="24.6">d</w>’<w n="24.7" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</w>.</l>
				</lg>
			</div></body></text></TEI>