<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR22" modus="cp" lm_max="12" metProfile="8, 6+6">
				<opener>
					<salute>À Armance.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1" punct="vg:2">S<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="1.2" punct="vg:4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="1.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="1.4" punct="ps:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="ps" caesura="1">œu</seg>r</w>…<caesura></caesura> <w n="1.5" punct="pe:8">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="pe" mp="F">e</seg></w> ! <w n="1.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="1.7">v<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="1.8" punct="pt:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="2.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g</w> <w n="2.3">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3" mp="M">ein</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6" caesura="1">è</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="2.8">c<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="11">ai</seg></w> <w n="2.9">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</w></l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="3.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="3.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>rpr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="3.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="3.9">pl<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</w> <w n="3.10">j<seg phoneme="wa" type="vs" value="1" rule="440" place="11" mp="M">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</w></l>
					<l n="4" num="1.4" lm="8" met="8"><space quantity="12" unit="char"></space><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="4.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.5" punct="pt:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:2">S<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="5.2" punct="vg:4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="5.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="5.4" punct="vg:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg" caesura="1">œu</seg>r</w>,<caesura></caesura> <w n="5.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="5.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="5.7" punct="pe:12">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="10" mp="M">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="12" punct="pe">en</seg>t</w> !</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ds</w> <w n="6.2" punct="pe:2">g<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="6.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="6.4">t</w>’<w n="6.5" punct="ps:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="ps" caesura="1">ai</seg>t</w>…<caesura></caesura> <w n="6.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="6.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="6.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="6.9">s<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="7.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="7.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="7.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="9">à</seg></w> <w n="7.7">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" mp="M">ai</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="11">a</seg></w> <w n="7.8" punct="vg:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="vg">an</seg>t</w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><space quantity="12" unit="char"></space><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="8.2">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>g</w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.7" punct="pe:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="9.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="9.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="Lc">an</seg>s</w>-<w n="9.5" punct="vg:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg" caesura="1">œu</seg>r</w>,<caesura></caesura> <w n="9.6">p<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="9.7" punct="vg:9">c<seg phoneme="œ" type="vs" value="1" rule="249" place="9" punct="vg">œu</seg>r</w>, <w n="9.8">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="9.9">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="9.10" punct="vg:12">tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="10.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="10.3">fr<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>c</w> <w n="10.4" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg></w>,<caesura></caesura> <w n="10.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="10.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="10.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="10.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="10.9" punct="pt:12">nu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg>t</w>.</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="11.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>x</w> <w n="11.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="11.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>r</w><caesura></caesura> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="11.8">p<seg phoneme="wa" type="vs" value="1" rule="420" place="9" mp="M">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="11.9">h<seg phoneme="y" type="vs" value="1" rule="453" place="11" mp="M">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="12" num="3.4" lm="8" met="8"><space quantity="12" unit="char"></space><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="12.2">tr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="12.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="12.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="12.5" punct="pt:8">bru<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pt">i</seg>t</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="13.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>ts</w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="13.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="13.7">h<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="13.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="13.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="13.10" punct="vg:12">f<seg phoneme="u" type="vs" value="1" rule="425" place="12">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">In</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>ts</w> <w n="14.2">c<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="M">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="14.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t</w> <w n="14.5">cr<seg phoneme="y" type="vs" value="1" rule="454" place="9" mp="M">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346" place="10">e</seg>l</w> <w n="14.6" punct="vg:12">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="12" punct="vg">in</seg></w>,</l>
					<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="15.2">t</w>’<w n="15.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3" mp="M">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="15.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="5">œu</seg>r</w> <w n="15.5" punct="vg:6">fi<seg phoneme="ɛ" type="vs" value="1" rule="va-7" place="6" punct="vg" caesura="1">e</seg>r</w>,<caesura></caesura> <w n="15.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="15.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="15.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="15.10">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="12">ein</seg></w></l>
					<l n="16" num="4.4" lm="8" met="8"><space quantity="12" unit="char"></space><w n="16.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>g<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>g</w> <w n="16.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="16.5">s</w>’<w n="16.6" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="12" met="6+6"><w n="17.1" punct="vg:2">S<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg" mp="F">e</seg></w>, <w n="17.2" punct="vg:4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg" mp="F">e</seg></w>, <w n="17.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="17.4" punct="ps:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="ps" caesura="1">œu</seg>r</w>…<caesura></caesura> <w n="17.5">J</w>’<w n="17.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>ff<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="10">ai</seg></w> <w n="17.7">l</w>’<w n="17.8">h<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="12">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="18" num="5.2" lm="12" met="6+6"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="18.2" punct="vg:3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="vg">ai</seg>t</w>, <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="342" place="4" mp="P">à</seg></w> <w n="18.4">l</w>’<w n="18.5" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="18.6">r<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg></w> <w n="18.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="18.8" punct="pe:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493" place="12" punct="pe">y</seg>r</w> !</l>
					<l n="19" num="5.3" lm="12" met="6+6"><w n="19.1">S<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="19.3" punct="vg:3">m<seg phoneme="œ" type="vs" value="1" rule="407" place="3" punct="vg">eu</seg>rs</w>, <w n="19.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>r</w> <w n="19.5" punct="ps:6">m<seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="ps" caesura="1">i</seg>t</w>…<caesura></caesura> <w n="19.6">c<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r</w> <w n="19.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="19.8">S<seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="12">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="20" num="5.4" lm="8" met="8"><space quantity="12" unit="char"></space><w n="20.1">M<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="20.3">j<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="20.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="20.5">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.6" punct="pe:8">gu<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg>r</w> !</l>
				</lg>
			</div></body></text></TEI>