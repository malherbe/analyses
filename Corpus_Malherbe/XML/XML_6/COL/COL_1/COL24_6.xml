<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL24" modus="sp" lm_max="8" metProfile="4, 8">
				<head type="main">LA PLAINTE ÉQUIVOQUE,</head>
				<head type="form">PARODIE.</head>
				<head type="tune">Air : Noté dans l’Anth. Franç. Tome II, page 10.</head>
				<lg n="1">
					<l n="1" num="1.1" lm="4" met="4"><space unit="char" quantity="8"></space><w n="1.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">U</seg></w> <w n="1.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="1.4" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="2.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="2.3">v<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>s</w> <w n="2.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5">A</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" punct="vg">an</seg>s</w>, <w n="2.5" punct="vg:8">Cl<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="COL24_1" place="8">e</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="3" num="1.3" lm="4" met="4"><space unit="char" quantity="8"></space><w n="3.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="3.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="3.4" punct="dp:4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="dp">a</seg>s</w> :</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="4.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="4.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.5">m</w>’<w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="4.7">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6">ain</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="7">ai</seg></w> <w n="4.8" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>s</w>.</l>
					<l n="5" num="1.5" lm="8" met="8"><w n="5.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="5.2">j</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w> <w n="5.5">qu</w>’<w n="5.6"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.7" punct="vg:8"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>nh<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><w n="6.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="6.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="6.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg">eu</seg>x</w>, <w n="6.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="6.6">m<seg phoneme="ɛ" type="vs" value="1" rule="COL24_1" place="8">e</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="7" num="1.7" lm="4" met="4"><space unit="char" quantity="8"></space><w n="7.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="7.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="7.4" punct="pt:4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="pt">a</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>