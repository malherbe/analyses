<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">APPENDICE</title>
				<title type="sub_2">À L’ÉDITION DES ŒUVRES POÉTIQUES (édition Hachette, 1889)</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>125 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1654" to="1705">1654-1705</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI97" modus="sm" lm_max="7" metProfile="7">
				<head type="number">XII</head>
				<head type="main">Épigramme sur la réconciliation <lb></lb>de l’auteur et de Perrault</head>
				<opener>
					<dateline>
						<date when="1695">(1695)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="7" met="7"><w n="1.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.4" punct="vg:7">p<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="7" met="7"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="2.2">P<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="2.3">s</w>’<w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="2.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="2.6" punct="vg:7">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="vg">er</seg></w>,</l>
					<l n="3" num="1.3" lm="7" met="7"><w n="3.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rr<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>lt</w> <w n="3.2">l</w>’<w n="3.3" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>d<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="7" met="7"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>spr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>x</w> <w n="4.3">l</w>’<w n="4.4">h<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
					<l n="5" num="1.5" lm="7" met="7"><w n="5.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.3">s</w>’<w n="5.4" punct="pt:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="pt">er</seg></w>.</l>
					<l n="6" num="1.6" lm="7" met="7"><w n="6.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>gr<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="6.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="6.5" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="7" num="1.7" lm="7" met="7"><w n="7.1" punct="vg:1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" punct="vg">an</seg>d</w>, <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="7.3">l</w>’<w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="7">en</seg>t</w></l>
					<l n="8" num="1.8" lm="7" met="7"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="8.8">s</w>’<w n="8.9" punct="vg:7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
					<l n="9" num="1.9" lm="7" met="7"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd</w> <w n="9.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="9.5" punct="pt:7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="7" punct="pt">en</seg>t</w>.</l>
					<l n="10" num="1.10" lm="7" met="7"><w n="10.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="10.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="7">en</seg>t</w></l>
					<l n="11" num="1.11" lm="7" met="7"><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="11.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="11.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="11.5">gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
					<l n="12" num="1.12" lm="7" met="7"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">Pr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="12.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="12.5" punct="pt:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>