<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODES, ÉPIGRAMMES ET AUTRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>633 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1664" to="1704">1664-1704</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI83" modus="cp" lm_max="12" metProfile="8, 6+6">
				<head type="main">SUR MON PORTRAIT</head>
				<head type="sub_1">
					M. Le Verrier, mon illustre ami, ayant fait graver mon portrait<lb></lb>
					par Drevet, célèbre graveur, fit mettre au bas de ce portrait<lb></lb>
					quatre vers où l’on me fait ainsi parler :
				</head>
				<opener>
					<dateline>
						<date when="1704">(1704)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="1.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>g</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="C">a</seg></w> <w n="1.5">R<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="1.8" punct="vg:12">R<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="2.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="2.4" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</w> <w n="2.6" punct="vg:12"><seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>g<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="vg">a</seg>l</w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="3.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="3.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" mp="P">an</seg>s</w> <w n="3.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="3.6" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>ts</w>,<caesura></caesura> <w n="3.7" punct="vg:7">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" punct="vg">o</seg>ct<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.8" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>j<seg phoneme="u" type="vs" value="1" rule="d-2" place="9" mp="M">ou</seg><seg phoneme="e" type="vs" value="1" rule="409" place="10" punct="vg">é</seg></w>, <w n="3.9" punct="vg:12">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="4.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="4.4" punct="vg:6">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg" caesura="1">e</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>,<caesura></caesura> <w n="4.5" punct="vg:8">H<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="4.7" punct="pt:12">J<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="M">u</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="12" punct="pt">a</seg>l</w>.</l>
				</lg>
				<p>A quoi j’ai répondu par ces vers :</p>
				<lg n="2">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="5.2">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3" punct="vg:4">V<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rri<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="5.4">c</w>’<w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="342" place="6" caesura="1">à</seg></w><caesura></caesura> <w n="5.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="5.8">f<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="5.9" punct="vg:12">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rtr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12" punct="vg">ai</seg>t</w>,</l>
					<l n="6" num="2.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="6.3" punct="vg:4">gr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>r</w>, <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="6.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.6" punct="vg:8">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="vg">ai</seg>t</w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="7.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="7.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w> <w n="7.4">f<seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="7.5">tr<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="7.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="P">u</seg>r</w> <w n="7.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="7.8">v<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="8.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="8.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="8.4">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="8.6">l</w>’<w n="8.7"><seg phoneme="e" type="vs" value="1" rule="170" place="7" mp="M">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="8.8" punct="pt:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
					<l n="9" num="2.5" lm="12" met="6+6"><w n="9.1" punct="vg:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="vg">ai</seg>s</w>, <w n="9.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="9.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="9.5" punct="vg:6">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">om</seg>p<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="9.6">qu</w>’<w n="9.7"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="9.8">b<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>s</w> <w n="9.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="9.10">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" mp="C">e</seg>t</w> <w n="9.11"><seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>vr<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="10" num="2.6" lm="12" met="6+6"><w n="10.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="10.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="10.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="10.4">pr<seg phoneme="o" type="vs" value="1" rule="444" place="4" mp="M">o</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="M">on</seg>c<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="10.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="10.8" punct="vg:12">fi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
					<l n="11" num="2.7" lm="8" met="8"><space unit="char" quantity="8"></space><w n="11.1">D</w>’<w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="11.6">v<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w></l>
					<l n="12" num="2.8" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w> <w n="12.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="12.4">l</w>’<w n="12.5" punct="pi:8"><seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
				</lg>
			</div></body></text></TEI>