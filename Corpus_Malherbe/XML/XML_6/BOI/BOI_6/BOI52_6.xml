<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODES, ÉPIGRAMMES ET AUTRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>633 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1664" to="1704">1664-1704</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI52" modus="cp" lm_max="12" metProfile="8, 6+6, (4+6)">
				<head type="main">ÉPIGRAMME</head>
				<head type="sub">sur ce qu’on avait lu à l’Académie des vers <lb></lb>contre Homère et contre Virgile</head>
				<opener>
					<dateline>
						<date when="1687">(1687)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Cl<seg phoneme="i" type="vs" value="1" rule="d-1" place="1" mp="M">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg></w> <w n="1.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>t</w> <w n="1.3">l</w>’<w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="1.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="1.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="1.7">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg></w> <w n="1.9">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg></w> <w n="1.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="1.11" punct="vg:12">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="12" punct="vg">e</seg>rs</w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="2.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="3">ain</seg></w> <w n="2.4">li<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="2.6">l</w>’<w n="2.7" punct="vg:8"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="8" punct="vg">e</seg>rs</w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="3.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="3.3">d</w>’<w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="3.5" punct="vg:6">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="vg" caesura="1">oi</seg>ds</w>,<caesura></caesura> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="3.7">p<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="3.8" punct="vg:12">st<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">H<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.5" punct="pt:8">V<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>rg<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6">« <w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="5.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="5.4" punct="pv:6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" punct="pv" caesura="1">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ;<caesura></caesura> <w n="5.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="5.6">s</w>’<w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="8">e</seg>st</w> <w n="5.8">m<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9" mp="M">o</seg>qu<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="5.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="5.10" punct="vg:12">v<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>s</w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>p<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="6.4" punct="pv:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pv">ou</seg>x</w> ;</l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="7.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2" mp="Lp">eu</seg>t</w>-<w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="7.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t</w><caesura></caesura> <w n="7.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="7.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.8" punct="pi:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>f<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></w> ?</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="8.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2" mp="F">e</seg></w> <w n="8.3">ch<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="P">ez</seg></w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="8.5" punct="pe:6">H<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pe" caesura="1">on</seg>s</w> !<caesura></caesura> <w n="8.6">ch<seg phoneme="e" type="vs" value="1" rule="347" place="7" mp="P">ez</seg></w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="8.8" punct="pe:12">T<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>p<seg phoneme="i" type="vs" value="1" rule="467" place="10" mp="M">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">am</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>x</w> !</l>
					<l n="9" num="1.9" lm="12" met="6+6">— <w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="9.4" punct="pt:4">P<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pt ti">i</seg>s</w>. — <w n="9.5">C</w>’<w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="9.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>c</w><caesura></caesura> <w n="9.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="9.9">l</w>’<w n="9.10">h<seg phoneme="o" type="vs" value="1" rule="415" place="8" mp="M">ô</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>l</w> <w n="9.11">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="9.12" punct="pe:12">f<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>s</w> !</l>
					<l n="10" num="1.10" lm="10" met="4+6" met_alone="True"><space unit="char" quantity="4"></space>— <w n="10.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, <w n="10.2">c</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="10.5" punct="vg:4">L<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg" caesura="1">ou</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="10.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="385" place="6">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8" punct="pt:10"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">A</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>m<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>. »</l>
				</lg>
			</div></body></text></TEI>