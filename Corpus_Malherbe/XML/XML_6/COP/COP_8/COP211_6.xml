<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES DIVERS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>626 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2019</date>
				<idno type="local">COP_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes Divers II</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/francoiscopeepoemesdivers2.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>François Coppée</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie ALEXANDRE HOUSSIAUX</publisher>
									<date when="1885">1885</date>
								</imprint>
								<biblScope unit="tome">Poésie, tome 1</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP211" modus="cm" lm_max="12" metProfile="6+6">
				<head type="main">VITRAIL</head>
				<opener>
					<salute>A Paul Verlaine.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="P">u</seg>r</w> <w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="1.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d</w> <w n="1.4">d</w>’<w n="1.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="1.6" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="1.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="1.8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>ts</w> <w n="1.9">r<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="1.10"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="1.11">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s</w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="2.3">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>b</w> <w n="2.4">n<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="2.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="2.7">d<seg phoneme="e" type="vs" value="1" rule="353" place="8" mp="M">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>s</w> <w n="2.8" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="11" mp="M">u</seg>l<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="vg">eu</seg>x</w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Cr<seg phoneme="wa" type="vs" value="1" rule="420" place="1" mp="M">oi</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="3.3" punct="vg:4">br<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</w>, <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="3.6">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</w> <w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="3.8"><seg phoneme="œ" type="vs" value="1" rule="286" place="10">œ</seg>il</w> <w n="3.9" punct="dp:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg></w> :</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="vg:1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="vg">a</seg>rc</w>, <w n="4.2" punct="vg:2">br<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" punct="vg">un</seg></w>, <w n="4.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="4.5" punct="pv:6">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" punct="pv" caesura="1">on</seg></w> ;<caesura></caesura> <w n="4.6" punct="vg:8">M<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>thi<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg></w>, <w n="4.7" punct="vg:9">r<seg phoneme="u" type="vs" value="1" rule="425" place="9" punct="vg">ou</seg>x</w>, <w n="4.8">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>s</w> <w n="4.9">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="4.10">l</w>’<w n="4.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="5.2" punct="vg:2">J<seg phoneme="ɑ̃" type="vs" value="1" rule="309" place="2" punct="vg">ean</seg></w>, <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="5.4" punct="vg:4">r<seg phoneme="o" type="vs" value="1" rule="444" place="4" punct="vg">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6" caesura="1">e</seg>c</w><caesura></caesura> <w n="5.6">l</w>’<w n="5.7"><seg phoneme="wa" type="vs" value="1" rule="420" place="7" mp="M">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w> <w n="5.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="5.9" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">em</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pv">eu</seg>rs</w> ;</l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1" punct="vg:1">L<seg phoneme="y" type="vs" value="1" rule="450" place="1" punct="vg">u</seg>c</w>, <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="6.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="6.4" punct="vg:4">b<seg phoneme="œ" type="vs" value="1" rule="249" place="4" punct="vg">œu</seg>f</w>, <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="6.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="6.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="6.8"><seg phoneme="o" type="vs" value="1" rule="318" place="9" mp="C">au</seg>x</w> <w n="6.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<seg phoneme="œ" type="vs" value="1" rule="407" place="12">eu</seg>rs</w></l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">M<seg phoneme="e" type="vs" value="1" rule="353" place="3" mp="M">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="482" place="4">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="7.5">Ju<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>fs</w><caesura></caesura> <w n="7.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="7.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="7.9" punct="dp:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="dp" mp="F">e</seg>s</w> :</l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="8.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="8.3">d</w>’<w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="8.5">d<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>gt</w><caesura></caesura> <w n="8.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="8.8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="11" mp="M">ym</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ts</w> <w n="9.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" mp="C">un</seg></w> <w n="9.4">f<seg phoneme="œ" type="vs" value="1" rule="406" place="5" mp="M">eu</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6" caesura="1">e</seg>t</w><caesura></caesura> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg></w> <w n="9.7" punct="pv:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pv">é</seg></w> ;</l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="10.2">D<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="10.4" punct="vg:6">Vi<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" punct="vg" caesura="1">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="10.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>t</w> <w n="10.7" punct="vg:12"><seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>mm<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="vg">é</seg></w>,</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1">Pr<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="11.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="11.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="11.4">br<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="11.5" punct="vg:8">J<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>s</w>, <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="11.7">d<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="11">in</seg></w> <w n="11.8" punct="vg:12">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="12.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="12.4">d<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="12.5">d<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>gts</w><caesura></caesura> <w n="12.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="12.7" punct="vg:9">b<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="9" punct="vg">i</seg>r</w>, <w n="12.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="10">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.9"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="11" mp="C">un</seg></w> <w n="12.10" punct="pv:12">pr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="13.3" punct="vg:3">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="vg">eu</seg></w>, <w n="13.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>c</w> <w n="13.5">vi<seg phoneme="e" type="vs" value="1" rule="383" place="5" mp="M">e</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rd</w><caesura></caesura> <w n="13.6">qu</w>’<w n="13.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="13.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="13.9"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="12">u</seg>s</w></l>
					<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1" mp="M">In</seg>cl<seg phoneme="i" type="vs" value="1" rule="467" place="2" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="14.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>s</w> <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="14.4">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>l</w><caesura></caesura> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="14.6">Ch<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>b<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10">in</seg>s</w> <w n="14.7" punct="pv:12">j<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>ffl<seg phoneme="y" type="vs" value="1" rule="450" place="12" punct="pv">u</seg>s</w> ;</l>
					<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="15.3" punct="vg:3">Chr<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>st</w>, <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>br<seg phoneme="ø" type="vs" value="1" rule="405" place="5" mp="M">eu</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="15.6">fi<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>l</w> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="15.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="15.9" punct="vg:12">v<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1">C<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">am</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="16.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="P">u</seg>r</w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="16.4">b<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="16.5">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>r</w><caesura></caesura> <w n="16.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="16.7">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="16.8">j<seg phoneme="o" type="vs" value="1" rule="318" place="10">au</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.9"><seg phoneme="e" type="vs" value="1" rule="189" place="11">e</seg>t</w> <w n="16.10" punct="pt:12">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>