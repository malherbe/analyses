<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES DIVERS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>626 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2019</date>
				<idno type="local">COP_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes Divers II</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/francoiscopeepoemesdivers2.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>François Coppée</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie ALEXANDRE HOUSSIAUX</publisher>
									<date when="1885">1885</date>
								</imprint>
								<biblScope unit="tome">Poésie, tome 1</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COP202" modus="sm" lm_max="8" metProfile="8">
				<head type="main">INNOCENCE</head>
				<opener>
					<salute>A Léopold Horovitz.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="1.2" punct="vg:3">ch<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.3"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="1.4" punct="vg:6">h<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="6" punct="vg">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.5"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L</w>’<w n="2.2"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rph<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="2.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w>-<w n="2.5">cl<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg>s</w></l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Pr<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="3.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="3.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>f<seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L</w>’<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="4.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w> <w n="4.5" punct="pt:8">b<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>cl<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="5.3">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>q</w> <w n="5.4" punct="pv:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" punct="pv">an</seg>s</w> ; <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="6.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="6.5" punct="vg:8">h<seg phoneme="a" type="vs" value="1" rule="307" place="7">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>s</w>,</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="7.2" punct="vg:2">l<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="7.6">ge<seg phoneme="o" type="vs" value="1" rule="415" place="8">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">D</w>’<w n="8.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ts</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="8.4">r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.7" punct="pt:8">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="7">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="9.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.3">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="9.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Sc<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>lpt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="10.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="10.5" punct="pv:8">j<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pv">ou</seg>x</w> ;</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">L</w>’<w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>ci<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="2">en</seg></w> <w n="11.3">cr<seg phoneme="i" type="vs" value="1" rule="467" place="3">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="11.6">j<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="11.7">v<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="12.3">pr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="12.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="12.6" punct="pv:8">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pv">ou</seg>x</w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="13.2">r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="13.4" punct="vg:8">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2">fl<seg phoneme="ø" type="vs" value="1" rule="405" place="2">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="14.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="5">e</seg>d</w> <w n="14.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="14.6" punct="vg:8">g<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="vg">e</seg>t</w>,</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="15.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="15.4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="16.3">qu</w>’<w n="16.4"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="16.5">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="16.6" punct="pt:8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>t</w>.</l>
				</lg>
			</div></body></text></TEI>