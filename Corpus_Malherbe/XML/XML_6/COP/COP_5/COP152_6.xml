<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Sonnets intimes et Poèmes inédits</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1824 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">COP_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Sonnets intimes et Poèmes inédits</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/francoiscoppesonetsintimes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Poésies complètes de François Coppée</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			<change when="2017-11-30" who="RR">Les vers incomplets du refrain du poème "RONDE D’ENFANTS AUX TUILERIES" ("Les lilas passeront, etc.") ont été remplacés par la strophe du refrain complet.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DEUXIÈME PARTIE</head><div type="poem" key="COP152" modus="sm" lm_max="8" metProfile="8">
					<head type="main">A PAUL MUSURUS</head>
					<head type="sub_1">QUI A BIEN TORT DE NE PAS PUBLIER SES SONNETS</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="1.4" punct="vg:4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg>s</w>, <w n="1.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="1.7" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="2" num="1.2" lm="8" met="8"><hi rend="ital"><w n="2.1" punct="pi:8">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg></w> <w n="2.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ci<seg phoneme="a" type="vs" value="1" rule="145" place="6" punct="vg">a</seg>m</w>, <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg></w> <w n="2.5">r<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pi">u</seg>s</w></hi> ?</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="3.3" punct="vg:3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>c</w>, <w n="3.4" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465" place="4">im</seg>pr<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg">é</seg>s</w>, <w n="3.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w>-<w n="3.6">j<seg phoneme="ə" type="ef" value="1" rule="e-1" place="9">e</seg></w></l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="4.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="4.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.5">P<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>l</w> <w n="4.6" punct="pe:8">M<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pe">u</seg>s</w> !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="5" num="1.1" lm="8" met="8"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>xc<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="?" type="va" value="1" rule="162" place="3">en</seg>t</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="5.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="5.5" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
							<l n="6" num="1.2" lm="8" met="8"><w n="6.1">S<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.3">p<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>rs</w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="6.5">b<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>x</w> <w n="6.6" punct="vg:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8" punct="vg">e</seg>ts</w>,</l>
							<l n="7" num="1.3" lm="8" met="8"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="7.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="7.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>rs</w> <w n="7.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="7.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>s</w> <w n="7.7">p<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
							<l n="8" num="1.4" lm="8" met="8"><w n="8.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="8.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="8.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="8.6" punct="pt:8">c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>s</w>.</l>
						</lg>
						<closer>
							<dateline>
								<date when="1902">Mai 1902.</date>
							</dateline>
						</closer>
					</div>
				</div></body></text></TEI>