<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Enluminures</title>
				<title type="medium">Édition électronique</title>
				<author key="ELS">
					<name>
						<forename>Max</forename>
						<surname>ELSKAMP</surname>
					</name>
					<date from="1862" to="1931">1862-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>718 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">ELS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Enluminures</title>
						<author>Max Elskamp</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/maxelskampenluminures.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Enluminures / paysages, heures, vies, chansons, grotesques</title>
						<author>Max Elskamp</author>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k10575703.r=%22max%20elskamp%22?rk=42918;4</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>Paul Lacomblez, Éditeur</publisher>
							<date when="1898">1898</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1898">1898</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS</head><div type="poem" key="ELS21" modus="sp" lm_max="8" metProfile="4, 8">
					<head type="number">III</head>
					<lg n="1">
						<l n="1" num="1.1" lm="4" met="4"><space unit="char" quantity="8"></space><w n="1.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="1.2">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w></l>
						<l n="2" num="1.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="2.1">h<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="2.2">d</w>’<w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="2.4" punct="vg:4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>,</l>
						<l n="3" num="1.3" lm="8" met="8">— <w n="3.1">c</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.5">c<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>hi<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="3.7" punct="tc:8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="ti">er</seg></w> —</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>s</w> <w n="4.4">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="4.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w></l>
						<l n="5" num="1.5" lm="4" met="4"><space unit="char" quantity="8"></space><w n="5.1">qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="5.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t</w> <w n="5.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="5.4">f<seg phoneme="a" type="vs" value="1" rule="193" place="4">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
						<l n="6" num="1.6" lm="4" met="4"><space unit="char" quantity="8"></space><w n="6.1">n<seg phoneme="y" type="vs" value="1" rule="457" place="1">u</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rps</w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="6.4" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1" lm="4" met="4"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="7.2">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w></l>
						<l n="8" num="2.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="8.1">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="8.2">d</w>’<w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="8.4" punct="vg:4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>,</l>
						<l n="9" num="2.3" lm="8" met="8"><w n="9.1">c</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="9.4">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.7" punct="vg:8">plu<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="10" num="2.4" lm="8" met="8"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="1">em</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="4">an</seg>t</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="10.3">di<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="11" num="2.5" lm="4" met="4"><space unit="char" quantity="8"></space><w n="11.1">d</w>’<w n="11.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.3">k<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
						<l n="12" num="2.6" lm="4" met="4"><space unit="char" quantity="8"></space><w n="12.1">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="12.2">cl<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.3"><seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="12.4" punct="pt:4">m<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1" lm="4" met="4"><space unit="char" quantity="8"></space><w n="13.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="13.2">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w></l>
						<l n="14" num="3.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="14.1">tr<seg phoneme="o" type="vs" value="1" rule="433" place="1">o</seg>p</w> <w n="14.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="14.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w></l>
						<l n="15" num="3.3" lm="8" met="8"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>x</w> <w n="15.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="15.4">m<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="15.5" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="16" num="3.4" lm="8" met="8"><w n="16.1">c</w>’<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="16.4">l</w>’<w n="16.5" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>r</w>, <w n="16.6">j<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>squ</w>’<w n="16.7"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="16.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="16.9" punct="vg:8">l<seg phoneme="i" type="vs" value="1" rule="482" place="8">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="17" num="3.5" lm="4" met="4"><space unit="char" quantity="8"></space><w n="17.1">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="17.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="o" type="vs" value="1" rule="438" place="4">o</seg>ts</w></l>
						<l n="18" num="3.6" lm="4" met="4"><space unit="char" quantity="8"></space><w n="18.1">su<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="18.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="18.3" punct="pv:4">l<seg phoneme="o" type="vs" value="1" rule="438" place="4" punct="pv">o</seg>t</w> ;</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1" lm="4" met="4"><space unit="char" quantity="8"></space><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="19.2" punct="vg:4">v<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>s</w>,</l>
						<l n="20" num="4.2" lm="4" met="4"><space unit="char" quantity="8"></space><w n="20.1" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>s</w>,</l>
						<l n="21" num="4.3" lm="8" met="8"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">e</seg>t</w> <w n="21.2">m<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="21.4">l</w>’<w n="21.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w></l>
						<l n="22" num="4.4" lm="8" met="8"><w n="22.1">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="22.2">c<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="22.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="22.4">l</w>’<w n="22.5">h<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="22.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="22.7" punct="vg:8">f<seg phoneme="a" type="vs" value="1" rule="193" place="8">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="23" num="4.5" lm="4" met="4"><space unit="char" quantity="8"></space><w n="23.1">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rs</w> <w n="23.2">c</w>’<w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="23.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w></l>
						<l n="24" num="4.6" lm="4" met="4"><space unit="char" quantity="8"></space><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w> <w n="24.2">f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="24.3">l</w>’<w n="24.4" punct="pt:4"><seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>