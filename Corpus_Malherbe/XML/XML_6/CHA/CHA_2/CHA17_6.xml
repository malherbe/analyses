<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="CHA">
					<name>
						<forename>François-René</forename>
						<nameLink>de</nameLink>
						<surname>CHATEAUBRIAND</surname>
					</name>
					<date from="1768" to="1848">1768-1848</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>735 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CHA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies diverses</title>
						<author>François-René de Chateaubriand</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/chateaubriandpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1797" to="1827">1797-1827</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas incluse.</p>
				<p>Les notes de l’auteur ont été intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Des corrections métriques ont été apportées sans autres attestations publiées.</p>
				<p>Les vers de la strophe illisible du poème "SERMENT — LES NOIRS DEVANT LE GIBET DE JOHN BROWN" ont été marqués comme inanalysables.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
 					<p>L’orthographe a été vérifiée avec le correcteur Aspell.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CHA17" modus="sp" lm_max="5" metProfile="5, 4">
				<head type="number">VII</head>
				<head type="main">Ballade de l’Abencerage</head>
				<lg n="1">
					<l n="1" num="1.1" lm="5" met="5"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">r<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="1.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="1.4">J<seg phoneme="y" type="vs" value="1" rule="d-3" place="4">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg></w></l>
					<l n="2" num="1.2" lm="5" met="5"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="2.2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="2.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w></l>
					<l n="3" num="1.3" lm="5" met="5"><w n="3.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="3.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="3.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
					<l n="4" num="1.4" lm="5" met="5"><w n="4.1">Gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="4.2">d</w>’<w n="4.3" punct="pv:5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">E</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pv">e</seg></w> ;</l>
					<l n="5" num="1.5" lm="5" met="5"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="5.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="5.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="5.4" punct="dp:5">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="5" punct="dp">ain</seg></w> :</l>
					<l n="6" num="1.6" lm="4" met="4"><w n="6.1">C<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="6.2" punct="vg:4">m<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="vg">e</seg></w>,</l>
					<l n="7" num="1.7" lm="4" met="4"><w n="7.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="7.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="7.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="7.4">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
					<l n="8" num="1.8" lm="4" met="4"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>c</w> <w n="8.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.3" punct="pt:4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4" punct="pt">ain</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1" lm="5" met="5"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">t</w>’<w n="9.3" punct="vg:5"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5" punct="vg">ai</seg></w>,</l>
					<l n="10" num="2.2" lm="5" met="5"><w n="10.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w></l>
					<l n="11" num="2.3" lm="5" met="5"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="11.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>s</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="11.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="11.5">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6">e</seg></w></l>
					<l n="12" num="2.4" lm="5" met="5"><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rd<seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="12.3" punct="pt:5">S<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					<l n="13" num="2.5" lm="5" met="5"><w n="13.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rs</w></l>
					<l n="14" num="2.6" lm="4" met="4"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="14.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg>s</w></l>
					<l n="15" num="2.7" lm="4" met="4"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="15.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
					<l n="16" num="2.8" lm="4" met="4"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="16.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>s</w> <w n="16.3" punct="pt:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pt">ou</seg>rs</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1" lm="5" met="5"><w n="17.1">Gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="17.2" punct="dp:5">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="dp">on</seg>d</w> :</l>
					<l n="18" num="3.2" lm="5" met="5"><w n="18.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="18.2">r<seg phoneme="wa" type="vs" value="1" rule="423" place="2">oi</seg></w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="18.4" punct="vg:5">L<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" punct="vg">on</seg></w>,</l>
					<l n="19" num="3.3" lm="5" met="5"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="19.2">M<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="19.3" punct="vg:5">l<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					<l n="20" num="3.4" lm="5" met="5"><w n="20.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="20.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="20.3" punct="pt:5">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					<l n="21" num="3.5" lm="5" met="5"><w n="21.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="21.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="21.3" punct="dp:5">pr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" punct="dp">en</seg>ts</w> :</l>
					<l n="22" num="3.6" lm="4" met="4"><w n="22.1">J</w>’<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="22.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="22.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
					<l n="23" num="3.7" lm="4" met="4"><w n="23.1">R<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="23.2">c<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
					<l n="24" num="3.8" lm="4" met="4"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="24.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>x</w> <w n="24.3" punct="pt:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="pt">an</seg>ts</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1" lm="5" met="5"><w n="25.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="25.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="25.3" punct="pv:5">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="pv">ai</seg>s</w> ;</l>
					<l n="26" num="4.2" lm="5" met="5"><w n="26.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="26.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="26.3" punct="pt:5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5" punct="pt">ai</seg>s</w>.</l>
					<l n="27" num="4.3" lm="5" met="5"><w n="27.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="27.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="27.3" punct="pe:5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>j<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
					<l n="28" num="4.4" lm="5" met="5"><w n="28.1">Gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="3">e</seg>st</w> <w n="28.3" punct="pe:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rj<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg></w> !</l>
					<l n="29" num="4.5" lm="5" met="5"><w n="29.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="29.2">chr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="29.3">m<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w></l>
					<l n="30" num="4.6" lm="4" met="4"><w n="30.1">D</w>’<w n="30.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
					<l n="31" num="4.7" lm="4" met="4"><w n="31.1">Ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>t</w> <w n="31.2">l</w>’<w n="31.3" punct="dp:4">h<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="dp">e</seg></w> :</l>
					<l n="32" num="4.8" lm="4" met="4"><w n="32.1">C</w>’<w n="32.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="32.3" punct="pe:4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pe">i</seg>t</w> !</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1" lm="5" met="5"><w n="33.1">J<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="33.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="33.3">ch<seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w></l>
					<l n="34" num="5.2" lm="5" met="5"><w n="34.1">N</w>’<w n="34.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="34.4" punct="vg:5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315" place="5" punct="vg">eau</seg></w>,</l>
					<l n="35" num="5.3" lm="5" met="5"><w n="35.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="1">è</seg>s</w> <w n="35.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="35.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="35.4" punct="vg:5">p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>sc<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="vg">e</seg></w>,</l>
					<l n="36" num="5.4" lm="5" met="5"><w n="36.1">L</w>’<w n="36.2">h<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>gg<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="36.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="36.4" punct="pt:5">M<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>d<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pt">e</seg></w>.</l>
					<l n="37" num="5.5" lm="5" met="5"><w n="37.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="37.2">chr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="37.3">m<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w></l>
					<l n="38" num="5.6" lm="4" met="4"><w n="38.1">D</w>’<w n="38.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
					<l n="39" num="5.7" lm="4" met="4"><w n="39.1">Ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>t</w> <w n="39.2">l</w>’<w n="39.3" punct="dp:4">h<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="dp">e</seg></w> :</l>
					<l n="40" num="5.8" lm="4" met="4"><w n="40.1">C</w>’<w n="40.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="40.3" punct="pe:4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pe">i</seg>t</w> !</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1" lm="5" met="5"><w n="41.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="41.2">b<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="41.3" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="3">A</seg>lh<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">am</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">a</seg></w> !</l>
					<l n="42" num="6.2" lm="5" met="5"><w n="42.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="42.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="42.3">d</w>’<w n="42.4" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">A</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">a</seg>h</w> !</l>
					<l n="43" num="6.3" lm="5" met="5"><w n="43.1">C<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="43.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="43.3" punct="pe:5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg>s</w> !</l>
					<l n="44" num="6.4" lm="5" met="5"><w n="44.1">Fl<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="44.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>x</w> <w n="44.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="44.4" punct="pe:5">pl<seg phoneme="ɛ" type="vs" value="1" rule="305" place="5">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="6" punct="pe">e</seg>s</w> !</l>
					<l n="45" num="6.5" lm="5" met="5"><w n="45.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="45.2">chr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="45.3">m<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w></l>
					<l n="46" num="6.6" lm="4" met="4"><w n="46.1">D</w>’<w n="46.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5">e</seg></w></l>
					<l n="47" num="6.7" lm="4" met="4"><w n="47.1">Ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1">en</seg>t</w> <w n="47.2">l</w>’<w n="47.3" punct="dp:4">h<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="dp">e</seg></w> :</l>
					<l n="48" num="6.8" lm="4" met="4"><w n="48.1">C</w>’<w n="48.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="48.3" punct="pe:4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pe">i</seg>t</w> !</l>
				</lg>
			</div></body></text></TEI>