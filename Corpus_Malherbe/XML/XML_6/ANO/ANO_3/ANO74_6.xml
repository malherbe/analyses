<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO74" rhyme="none" modus="sp" lm_max="8" metProfile="7, 6, (3), (2), (8)">
					<head type="main">IN-PROMPTU</head>
					<head type="sub">PARODIE D’UN COUPLET DES AMOURS D’ÉTÉ</head>
					<head type="tune">Sur l’air : <hi rend="ital"></hi>En plein, plan</head>
					<lg n="1">
						<l n="1" num="1.1" lm="7" met="7"><space unit="char" quantity="2"></space><w n="1.1">Qu</w>’<w n="1.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.3">v<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="1.5" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="6" met="6"><space unit="char" quantity="4"></space><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">q</w>’<w n="2.3">c</w>’<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="2.5">m<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6" punct="pe:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></w> !</l>
						<l n="3" num="1.3" lm="7" met="7"><space unit="char" quantity="2"></space><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="3.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="3.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="3.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="3.7">s<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>x</w> <w n="3.8" punct="vg:7">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>cs</w>,</l>
						<l n="4" num="1.4" lm="3"><space unit="char" quantity="10"></space><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="4.2" punct="vg:2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="2" punct="vg">ein</seg></w>, <w n="4.3" punct="vg:3">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg></w>,</l>
						<l n="5" num="1.5" lm="6" met="6"><space unit="char" quantity="4"></space><w n="5.1" punct="vg:1">Rl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" punct="vg">an</seg></w>, <w n="5.2" punct="vg:2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" punct="vg">an</seg></w>, <w n="5.3" punct="vg:3">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3" punct="vg">an</seg></w>, <w n="5.4" punct="vg:6">t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="6" num="1.6" lm="2"><space unit="char" quantity="12"></space><w n="6.1" punct="vg:1">L<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" punct="vg">an</seg></w>, <w n="6.2" punct="pt:2">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" punct="pt">an</seg></w>.</l>
						<l n="7" num="1.7" lm="8"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="7.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="7.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="7.6">h<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>t</w>’<w n="7.7">s</w> <w n="7.8" punct="vg:8">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="8" punct="vg">en</seg>s</w>,</l>
						<l n="8" num="1.8" lm="6" met="6"><space unit="char" quantity="4"></space><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">en</seg></w> <w n="8.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="8.4"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="8.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="8.6" punct="pt:6">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>