<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO9" modus="cp" lm_max="12" metProfile="8, 6+6">
					<head type="main">LA BELLE ACCOMMODANTE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><space unit="char" quantity="8"></space><w n="1.1" punct="vg:2">L<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg></w>, <w n="1.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="1.3">d</w>’<w n="1.4">h<seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="1.5" punct="vg:8">f<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><space unit="char" quantity="8"></space><w n="2.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="2.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="2.6">j<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w></l>
						<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2">j<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="3.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="3.4">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="3.5">qu</w>’<w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>lb<seg phoneme="a" type="vs" value="1" rule="340" place="8">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="4" num="1.4" lm="8" met="8"><space unit="char" quantity="8"></space><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2" punct="vg:2">L<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="4.3"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>bj<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="4.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</w>.</l>
						<l n="5" num="1.5" lm="8" met="8"><space unit="char" quantity="8"></space><w n="5.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>t</w> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="5.3">s</w>’<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.7" punct="vg:8">g<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg>t</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="6.3">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.5" punct="pv:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>b<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="7" num="1.7" lm="12" met="6+6">« <w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="7.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="7.4" punct="vg:4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="vg">u</seg>s</w>, » <w n="7.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="Lp">i</seg>t</w>-<w n="7.6" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg in" caesura="1">i</seg>l</w>,<caesura></caesura> « <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.8" punct="pv:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="10" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pv">er</seg></w> ;</l>
						<l n="8" num="1.8" lm="12" met="6+6">» <w n="8.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1" mp="C">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="5" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="8.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="8.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="M">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>t</w> <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="P">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8" punct="pt:12"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg>s</w>.</l>
						<l n="9" num="1.9" lm="8" met="8"><space unit="char" quantity="8"></space>» ‒ <w n="9.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! » <w n="9.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="9.3" punct="vg:3">L<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="vg in">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, « <w n="9.4" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg">i</seg></w>, <w n="9.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="9.6" punct="vg:8">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>,</l>
						<l n="10" num="1.10" lm="12" met="6+6">» <w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M/mp">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="Lp">ez</seg></w>-<w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="10.4" punct="vg:6">d<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg>x</w>,<caesura></caesura> <w n="10.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r</w> <w n="10.6">f<seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="10.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="10" mp="C">eu</seg>rs</w> <w n="10.8" punct="pe:12">qu<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg>s</w> ! »</l>
					</lg>
				</div></body></text></TEI>