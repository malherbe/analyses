<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO45" modus="cm" lm_max="10" metProfile="4+6">
					<head type="main">LE CONFESSEUR EXEMPLAIRE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="1.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="1.4" punct="vg:4">P<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">â</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="1.6">pi<seg phoneme="e" type="vs" value="1" rule="241" place="6">e</seg>ds</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.8">P<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="1.9" punct="vg:10">J<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">on</seg>f<seg phoneme="e" type="vs" value="1" rule="353" place="3" mp="M">e</seg>ss<seg phoneme="wa" type="vs" value="1" rule="420" place="4" caesura="1">oi</seg>t</w><caesura></caesura> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="2.4">j<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="2.5" punct="vg:10">G<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rn<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10" punct="vg">en</seg>t</w>,</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="3.3">p<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg>s</w><caesura></caesura> <w n="3.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="3.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="3.6" punct="vg:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="M">om</seg>br<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10" punct="vg">en</seg>t</w>,</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">C<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="4.3">S<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="441" place="4" caesura="1">o</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w><caesura></caesura> <w n="4.4">h<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444" place="6" mp="M">o</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>t</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="4.6" punct="pt:10">c<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
						<l n="5" num="1.5" lm="10" met="4+6">‒ « <w n="5.1">Qu</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w>-<w n="5.3">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="5.4" punct="pe:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="pe" caesura="1">u</seg></w> !<caesura></caesura> <w n="5.5" punct="pe:5">Ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="pe">e</seg>l</w> ! <w n="5.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="10">en</seg>t</w></l>
						<l n="6" num="1.6" lm="10" met="4+6">» <w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="6.3">p<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg>x</w> <w n="6.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" mp="M">in</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>rn<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="6.6" punct="vg:10">fl<seg phoneme="a" type="vs" value="1" rule="341" place="10">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="7" num="1.7" lm="10" met="4+6">» <w n="7.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="7.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="P">ou</seg>r</w> <w n="7.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" caesura="1">e</seg></w><caesura></caesura> <w n="7.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="7.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="7.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="C">ou</seg>s</w> <w n="7.7" punct="pe:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>cc<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="pe">ez</seg></w> !</l>
						<l n="8" num="1.8" lm="10" met="4+6">» <w n="8.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M/mp">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="Fm">e</seg>s</w>-<w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="8.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</w> <w n="8.6" punct="vg:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9" mp="M">in</seg>f<seg phoneme="a" type="vs" value="1" rule="341" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="9" num="1.9" lm="10" met="4+6">» <w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="9.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="9.3" punct="vg:4">s<seg phoneme="ɛ" type="vs" value="1" rule="355" place="4" punct="vg" caesura="1">e</seg>x<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="9.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="9.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7">e</seg>r</w> <w n="9.7" punct="vg:8">f<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>ls</w>, <w n="9.8">n</w>’<w n="9.9" punct="vg:10"><seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="M">u</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="10" punct="vg">ez</seg></w>,</l>
						<l n="10" num="1.10" lm="10" met="4+6">» <w n="10.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="10.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.3" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="10.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="10.5">v<seg phoneme="wa" type="vs" value="1" rule="440" place="6" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="7">ez</seg></w> <w n="10.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="8">e</seg></w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="10.8" punct="pt:10">f<seg phoneme="a" type="vs" value="1" rule="193" place="10">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg>s</w>. »</l>
					</lg>
				</div></body></text></TEI>