<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO7" modus="cm" lm_max="10" metProfile="4+6">
					<head type="main">LA PLAINTE INJUSTE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="1.3" punct="vg:4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg" caesura="1">a</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5" mp="C">un</seg></w> <w n="1.5">j<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="1.6">qu</w>’<w n="1.7"><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l</w> <w n="1.8">f<seg phoneme="œ" type="vs" value="1" rule="304" place="8" mp="M">ai</seg>s<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>t</w> <w n="1.9" punct="vg:10">b<seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="vg">eau</seg></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">G<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="2.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t</w> <w n="2.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" caesura="1">oi</seg>r</w><caesura></caesura> <w n="2.4">M<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="2.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="2.6" punct="pt:10">M<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">Bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="1">en</seg></w> <w n="3.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="3.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="450" place="4" caesura="1">u</seg></w><caesura></caesura> <w n="3.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" mp="M">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="3.7" punct="dp:10">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="dp">eau</seg></w> :</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3" punct="vg:4">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="4.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="4.5">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t</w> <w n="4.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="4.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="P">a</seg>r</w> <w n="4.8" punct="pv:10"><seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>cu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="C">i</seg>l</w> <w n="5.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="5.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="4" caesura="1">ain</seg>t</w><caesura></caesura> <w n="5.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="5.7">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="5.8">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
						<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="6.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="6.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">er</seg></w><caesura></caesura> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="M">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>s</w> <w n="6.6">d</w>’<w n="6.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="6.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>d</w> <w n="6.9" punct="pt:10">V<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" punct="pt">e</seg>t</w>.</l>
						<l n="7" num="1.7" lm="10" met="4+6"><w n="7.1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">O</seg>r</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="7.4">D<seg phoneme="a" type="vs" value="1" rule="341" place="4" caesura="1">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="7.6">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt</w> <w n="7.7">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="7.8">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>t</w> <w n="7.9" punct="dp:10">G<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10" punct="dp">e</seg>t</w> :</l>
						<l n="8" num="1.8" lm="10" met="4+6"><w n="8.1">Mi<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="8.2">n</w>’<w n="8.3"><seg phoneme="y" type="vs" value="1" rule="251" place="2">eû</seg>t</w> <w n="8.4" punct="vg:4">ch<seg phoneme="wa" type="vs" value="1" rule="420" place="3" mp="M">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="8.5">s<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg></w> <w n="8.6">c</w>’<w n="8.7"><seg phoneme="y" type="vs" value="1" rule="251" place="6">eû</seg>t</w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="8.9">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="P">ou</seg>r</w> <w n="8.10" punct="pt:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>