<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO25" modus="sm" lm_max="8" metProfile="8">
					<head type="main">ÉPIGRAMME</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L</w>’<w n="1.2" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="1.4">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="1.5" punct="vg:8">nu<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>t</w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="2.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.3">f<seg phoneme="a" type="vs" value="1" rule="193" place="5">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="2.4" punct="pt:8">f<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="3" num="1.3" lm="8" met="8">« <w n="3.1">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="3.2" punct="vg:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="vg">oi</seg></w>, » <w n="3.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w>-<w n="3.4" punct="vg:5"><seg phoneme="i" type="vs" value="1" rule="468" place="5" punct="vg in">i</seg>l</w>, « <w n="3.5">s</w>’<w n="3.6"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="3.7">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="3.8" punct="vg:8">cu<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>t</w>,</l>
						<l n="4" num="1.4" lm="8" met="8">» <w n="4.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="4.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="4.3">d<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>gt</w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="4.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="4.6" punct="pt:8">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>. »</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.2"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="5.3" punct="vg:4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4" punct="vg">en</seg>t</w>, <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="5.5">s</w>’<w n="5.6" punct="pv:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="6.3"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="6.4">l</w>’<w n="6.5"><seg phoneme="y" type="vs" value="1" rule="391" place="4">eu</seg>t</w> <w n="6.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="5">en</seg></w> <w n="6.7" punct="pv:8">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>h<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="7" num="1.7" lm="8" met="8">« <w n="7.1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">O</seg>r</w> <w n="7.2" punct="vg:2">ç<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg></w>, » <w n="7.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w>-<w n="7.4" punct="vg:4"><seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg in">i</seg>l</w>, « <w n="7.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="7.6" punct="vg:8">R<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>z<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="8" num="1.8" lm="8" met="8">» <w n="8.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w>-<w n="8.3">j<seg phoneme="ə" type="ef" value="1" rule="e-13" place="3">e</seg></w> <w n="8.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="8.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="8.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="8.7" punct="pi:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pi">i</seg></w> ?</l>
						<l n="9" num="1.9" lm="8" met="8">» ‒ <w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>h</w> <w n="9.2" punct="pe:2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="pe">on</seg>c</w> ! » <w n="9.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="9.4">l</w>’<w n="9.5" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="10" num="1.10" lm="8" met="8">« <w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="10.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rs</w> <w n="10.7" punct="pe:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg></w> ! »</l>
					</lg>
				</div></body></text></TEI>