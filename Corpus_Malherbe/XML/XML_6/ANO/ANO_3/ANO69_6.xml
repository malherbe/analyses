<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO69" modus="sm" lm_max="8" metProfile="8">
					<head type="main">LE PLAISIR SANS REMORDS</head>
					<head type="form">CONTE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="1.3">C<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="1.6" punct="vg:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="2.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="2.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" punct="vg">e</seg></w>, <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="2.6" punct="vg:8">nu<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="vg">i</seg>t</w>,</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="3.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg></w> <w n="3.5" punct="pv:8">bru<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pv">i</seg>t</w> ;</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.5" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1" punct="vg:1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="5.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2">an</seg>s</w> <w n="5.3" punct="vg:4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>r</w>, <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="5.6" punct="vg:8">dr<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>ps</w>,</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg></w> <w n="6.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="6.5">c<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="6.6" punct="pt:8">gu<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="7.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="7.5">M<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w>-<w n="7.6" punct="vg:8">gr<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>s</w>,</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="8.2">s</w>’<w n="8.3" punct="ps:5"><seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="ps">e</seg>nt</w>… <w n="8.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg></w> <w n="8.5" punct="dp:8">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
						<l n="9" num="1.9" lm="8" met="8"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="9.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="9.3">l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="9.4">C<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="9.5" punct="vg:8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>,</l>
						<l n="10" num="1.10" lm="8" met="8"><w n="10.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="1">in</seg>t</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="10.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4" punct="ps:4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4" punct="ps">un</seg></w>… <w n="10.5">c<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="10.6">s</w>’<w n="10.7" punct="ps:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" punct="ps">en</seg>d</w>…</l>
						<l n="11" num="1.11" lm="8" met="8"><w n="11.1" punct="vg:2">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" punct="vg">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rs</w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="11.4">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="11.5" punct="vg:8">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="12" num="1.12" lm="8" met="8"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="12.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">bru<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>t</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="12.6">l</w>’<w n="12.7" punct="vg:8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>,</l>
						<l n="13" num="1.13" lm="8" met="8"><w n="13.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="13.4" punct="vg:4">r<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="13.6">r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="13.7" punct="vg:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>,</l>
						<l n="14" num="1.14" lm="8" met="8"><w n="14.1">Qu</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="14.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="14.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w>-<w n="14.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7" punct="pt:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</w>.</l>
						<l n="15" num="1.15" lm="8" met="8"><w n="15.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="15.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="15.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="15.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="15.6">b<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>nh<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="16" num="1.16" lm="8" met="8"><w n="16.1">R<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>st<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="16.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg>t</w> <w n="16.3">d</w>’<w n="16.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="16.5" punct="dp:8">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="dp">on</seg>d</w> :</l>
						<l n="17" num="1.17" lm="8" met="8"><w n="17.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg>t</w> <w n="17.3">lu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="17.4" punct="pt:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>d</w>.</l>
						<l n="18" num="1.18" lm="8" met="8"><w n="18.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="18.2" punct="vg:4">v<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg>t</w>, <w n="18.3">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="5">oi</seg></w> <w n="18.4">qu</w>’<w n="18.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="18.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="18.7" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">oû</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="19" num="1.19" lm="8" met="8"><w n="19.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="19.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="19.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="19.4">lu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="19.5" punct="pv:8">r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pv">i</seg>r</w> ;</l>
						<l n="20" num="1.20" lm="8" met="8"><w n="20.1" punct="ps:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="ps">ai</seg>s</w>… <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="20.3">s<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>il</w> <w n="20.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.5">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d</w> <w n="20.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="20.7" punct="pt:8">r<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="21" num="1.21" lm="8" met="8"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="21.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="21.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="21.4" punct="vg:6">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg">i</seg>r</w>, <w n="21.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg>s</w> <w n="21.6" punct="vg:8">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="22" num="1.22" lm="8" met="8"><w n="22.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="22.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="22.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="22.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="22.6" punct="pt:8">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>