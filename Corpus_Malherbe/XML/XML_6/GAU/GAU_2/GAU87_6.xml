<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU87" modus="sm" lm_max="8" metProfile="8">
				<head type="main">ABSENCE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1" punct="vg:2">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2" punct="vg">en</seg>s</w>, <w n="1.2" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4" punct="vg">en</seg>s</w>, <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="1.4">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w>-<w n="1.5" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="2.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="2.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg></w> <w n="2.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="2.6" punct="pv:8">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pv">e</seg>il</w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="3.5">v<seg phoneme="i" type="vs" value="1" rule="482" place="5">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="3.7" punct="vg:8">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg></w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="4.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="4.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="4.5" punct="pt:8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pt">e</seg>il</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">n<seg phoneme="o" type="vs" value="1" rule="438" place="3">o</seg>s</w> <w n="5.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4">œu</seg>rs</w> <w n="5.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="5.6" punct="pv:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="6.2">d</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="6.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="6.6" punct="pt:8">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg>s</w>.</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="7.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w> <w n="7.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4" punct="pe">e</seg>r</w> ! <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg></w> <w n="7.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="8.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>ds</w> <w n="8.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>rs</w> <w n="8.4" punct="pe:8"><seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg>s</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">D</w>’<w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w>-<w n="9.4" punct="vg:4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</w>, <w n="9.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.7" punct="vg:8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="10.6" punct="vg:8">h<seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="vg">eau</seg>x</w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6" punct="vg:8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.4">pi<seg phoneme="e" type="vs" value="1" rule="241" place="5">e</seg>d</w> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="12.6" punct="pe:8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318" place="8" punct="pe">au</seg>x</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="13.2">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="2">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="3">y</seg>s</w> <w n="13.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="13.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d</w> <w n="13.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.7" punct="vg:8">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1" punct="pe:2">H<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="pe">a</seg>s</w> ! <w n="14.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="14.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="14.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="14.5" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pv">er</seg></w> ;</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="15.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="15.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rps</w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="15.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="16.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="16.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r</w> <w n="16.5" punct="pe:8">v<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pe">er</seg></w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1" lm="8" met="8"><w n="17.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w>-<w n="17.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="17.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="17.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="17.5" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>ll<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="18" num="5.2" lm="8" met="8"><w n="18.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="18.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="18.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="18.5">d</w>’<w n="18.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="vg">u</seg>r</w>,</l>
					<l n="19" num="5.3" lm="8" met="8"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="19.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>ps</w> <w n="19.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="3">a</seg>y<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="19.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="19.6" punct="vg:8">r<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="20" num="5.4" lm="8" met="8"><w n="20.1">J</w>’<w n="20.2" punct="vg:2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2" punct="vg">ai</seg></w>, <w n="20.3">d</w>’<w n="20.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="20.5">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>l</w> <w n="20.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="20.8" punct="pt:8">s<seg phoneme="y" type="vs" value="1" rule="445" place="8" punct="pt">û</seg>r</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1" lm="8" met="8"><w n="21.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rps</w> <w n="21.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="21.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="21.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="21.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="21.7" punct="pv:8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					<l n="22" num="6.2" lm="8" met="8"><w n="22.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="22.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="22.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="22.4" punct="vg:5"><seg phoneme="a" type="vs" value="1" rule="341" place="4">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="22.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="22.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t</w> <w n="22.7" punct="vg:8">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="vg">oi</seg>t</w>,</l>
					<l n="23" num="6.3" lm="8" met="8"><w n="23.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="23.3">c<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="23.4" punct="vg:8">bl<seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="24" num="6.4" lm="8" met="8"><w n="24.1">T</w>’<w n="24.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="24.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rd</w> <w n="24.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="24.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="24.7" punct="pt:8">t<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>t</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1" lm="8" met="8"><w n="25.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="25.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="25.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="25.4">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="25.5" punct="vg:8">d<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="26" num="7.2" lm="8" met="8"><w n="26.1">Bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="26.3">f<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="26.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="26.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="26.6">l</w>’<w n="26.7" punct="vg:8"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>r</w>,</l>
					<l n="27" num="7.3" lm="8" met="8"><w n="27.1">D<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="27.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="27.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="27.5">d</w>’<w n="27.6" punct="vg:8">h<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467" place="8">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="28" num="7.4" lm="8" met="8"><w n="28.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="28.2" punct="vg:3">g<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="28.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="28.4">r<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l</w> <w n="28.5" punct="pv:8">tr<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pv">o</seg>r</w> ;</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1" lm="8" met="8"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="29.2" punct="vg:2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg>s</w>, <w n="29.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="29.4" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="4" punct="vg">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="29.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="29.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="29.7" punct="vg:8">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="30" num="8.2" lm="8" met="8">«<w n="30.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="30.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="30.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="30.4">qu</w>’<w n="30.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="30.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="30.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="30.8" punct="vg:8">j<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>rs</w>,</l>
					<l n="31" num="8.3" lm="8" met="8"><w n="31.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="31.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="31.3" punct="pe:4">c<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pe">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="31.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="31.5">t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="31.6">d</w>’<w n="31.7" punct="vg:8"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="32" num="8.4" lm="8" met="8"><w n="32.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="32.3">n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>d</w> <w n="32.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="32.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="32.6" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>rs</w>.»</l>
				</lg>
			</div></body></text></TEI>