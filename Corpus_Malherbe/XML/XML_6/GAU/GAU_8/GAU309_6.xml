<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU309" modus="sm" lm_max="8" metProfile="8">
				<head type="main">LES RODEURS DE NUIT</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">M<seg phoneme="i" type="vs" value="1" rule="467" place="1">i</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="1.2">r<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="1.4">b<seg phoneme="e" type="vs" value="1" rule="353" place="6">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w> <w n="1.5" punct="pv:8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1" punct="vg:3">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>b<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg>s</w>, <w n="2.2">v<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="2.4" punct="vg:8">h<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>x</w>,</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">P<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="3.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>f</w> <w n="3.3">qu</w>’<w n="3.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="3.5">l</w>’<w n="3.6" punct="vg:8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">J<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="403" place="2">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="4.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>nt</w> <w n="4.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w> <w n="4.4" punct="pt:8">tr<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="5.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="5.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="7">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="6.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>lsh<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="6.5" punct="pt:8">nu<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pt">i</seg>t</w>.</l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2" punct="vg:3">b<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>s</w>, <w n="7.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="7.4">l<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="7.5" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">S<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2" punct="vg:4">bl<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>tt<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>nt</w>, <w n="8.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="8.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="8.5" punct="pt:8">bru<seg phoneme="i" type="vs" value="1" rule="491" place="8" punct="pt">i</seg>t</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="9.4">du<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ls</w> <w n="9.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="9.7" punct="vg:8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="10.2">cr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>b<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="10.5">qu</w>’<w n="10.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="10.7" punct="vg:8">b<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="vg">a</seg>t</w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="11.2">p<seg phoneme="o" type="vs" value="1" rule="438" place="2">o</seg>ts</w> <w n="11.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>s</w> <w n="11.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="11.6" punct="vg:8">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="12.3" punct="vg:3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>ts</w>, <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="12.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="12.6" punct="pt:8">s<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>bb<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg>t</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1" lm="8" met="8">.... <w n="13.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="13.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3" punct="pt:3">t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="pt ti">ai</seg>t</w>. — <w n="13.4">L<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="13.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>tr<seg phoneme="u" type="vs" value="1" rule="427" place="6">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="13.6" punct="vg:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="14" num="4.2" lm="8" met="8"><w n="14.1">Rh<seg phoneme="i" type="vs" value="1" rule="493" place="1">y</seg>thm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="14.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="14.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="14.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="14.6" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pt">é</seg></w>.</l>
					<l n="15" num="4.3" lm="8" met="8"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">n<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>r</w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="4">aim</seg></w> <w n="15.4">fu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="15.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="15.6">l</w>’<w n="15.7" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt pt pt pt">e</seg></w>....</l>
					<l n="16" num="4.4" lm="8" met="8"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg></w> <w n="16.3">h<seg phoneme="o" type="vs" value="1" rule="435" place="4">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="16.5" punct="pe:8">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="pe">é</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1864">1864</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>