<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU318" modus="cm" lm_max="12" metProfile="6+6">
				<head type="main">A CLAUDIUS POPELIN</head>
				<head type="form">SONNET</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="353" place="3" mp="M">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="1.4">l</w>’<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">A</seg>rt</w><caesura></caesura> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="8">e</seg>c</w> <w n="1.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="1.8">d<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>gt</w> <w n="1.9">tr<seg phoneme="o" type="vs" value="1" rule="433" place="11">o</seg>p</w> <w n="1.10" punct="vg:12">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">om</seg>pt</w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="2.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="2.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="2.8" punct="pt:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2" mp="M">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="3.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="P">ou</seg>s</w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="3.5">cr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" caesura="1">ê</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="3.7">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="8">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="3.9" punct="vg:12">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="4.3">M<seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.4">L<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg></w><caesura></caesura> <w n="4.5">l</w>’<w n="4.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>t</w> <w n="4.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="4.9" punct="pt:12">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg>t</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">n<seg phoneme="o" type="vs" value="1" rule="438" place="3" mp="C">o</seg>s</w> <w n="5.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="5.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="5.6" punct="vg:6">v<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg></w>,<caesura></caesura> <w n="5.7">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w> <w n="5.8">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="5.9">d</w>’<w n="5.10">y<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="5.11">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="5.12" punct="pt:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg>t</w>.</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="6.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="6.4">V<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg></w><caesura></caesura> <w n="6.5">R<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ph<seg phoneme="a" type="vs" value="1" rule="343" place="8" mp="M">a</seg><seg phoneme="ɛ" type="vs" value="1" rule="414" place="9">ë</seg>l</w> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="10">en</seg></w> <w n="6.7" punct="vg:12">r<seg phoneme="y" type="vs" value="1" rule="d-3" place="11" mp="M">u</seg><seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">M<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M/mc">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2" mp="Lc">e</seg>l</w>-<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">An</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="7.3">s</w>’<w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" caesura="1">ein</seg>t</w><caesura></caesura> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="7.6">m<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>rs</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="7.9" punct="vg:12">S<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>xt<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">A</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="8.4">Z<seg phoneme="ø" type="vs" value="1" rule="405" place="5" mp="M">eu</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="8.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>ls</w> <w n="8.6">s</w>’<w n="8.7" punct="pt:12"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>v<seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>n<seg phoneme="u" type="vs" value="1" rule="d-2" place="10" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="M">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg>t</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="9.2" punct="vg:2">t<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="9.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="9.4" punct="vg:6">Cl<seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>d<seg phoneme="i" type="vs" value="1" rule="dc-1" place="5" mp="M">i</seg><seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>s</w>,<caesura></caesura> <w n="9.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="9.6">f<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="9.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="9.8" punct="pv:12">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="10.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="10.3">l</w>’<w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">am</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="y" type="vs" value="1" rule="453" place="4">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="Fc">e</seg></w> <w n="10.6" punct="vg:6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="10.7">l</w>’<w n="10.8"><seg phoneme="i" type="vs" value="1" rule="467" place="7" mp="M">i</seg>mm<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rc<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>sc<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.9"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307" place="12">a</seg>il</w></l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="11.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="5" mp="M">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="450" place="6" caesura="1">u</seg>s</w><caesura></caesura> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="11.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="11.7" punct="pt:12">tr<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307" place="12" punct="pt">a</seg>il</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1" lm="12" met="6+6"><w n="12.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="12.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>ts</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="12.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="12.7"><seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="12.9" punct="vg:12">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="10" mp="M">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>c<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="13" num="4.2" lm="12" met="6+6"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="13.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" mp="M">an</seg>sp<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" caesura="1">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w><caesura></caesura> <w n="13.4"><seg phoneme="i" type="vs" value="1" rule="497" place="7" mp="C">y</seg></w> <w n="13.5">lu<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="13.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="P">u</seg>r</w> <w n="13.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="13.8" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="307" place="11" mp="M">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="vg">on</seg></w>,</l>
					<l n="14" num="4.3" lm="12" met="6+6"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">ch<seg phoneme="e" type="vs" value="1" rule="347" place="2" mp="P">ez</seg></w> <w n="14.3">t<seg phoneme="wa" type="vs" value="1" rule="423" place="3">oi</seg></w> <w n="14.4">l</w>’<w n="14.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">I</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>l</w><caesura></caesura> <w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="14.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rs</w> <w n="14.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="14.9" punct="pt:12">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="11" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="12" punct="pt">on</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1866">1866</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>