<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU117" modus="cm" lm_max="12" metProfile="6+6">
				<head type="main">LE LUXEMBOURG</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Enfant, dans les ébats de l’enfance joueuse.
							</quote>
							<bibl>
								<name>J. Delorme.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="1.2">L<seg phoneme="y" type="vs" value="1" rule="450" place="2" mp="M">u</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">em</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rg</w> <w n="1.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="1.4">l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">G<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>z<seg phoneme="u" type="vs" value="1" rule="428" place="2" mp="M">ou</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="2.3">m<seg phoneme="wa" type="vs" value="1" rule="421" place="5" mp="M">oi</seg>n<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="2.5">j<seg phoneme="wa" type="vs" value="1" rule="440" place="8" mp="M">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="403" place="9">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="2.6" punct="vg:12">v<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">Qu</w>’<w n="3.2"><seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>x</w> <w n="3.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg>s</w> <w n="3.4">d</w>’<w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="3.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="3.7" punct="vg:6">d<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>x</w>,<caesura></caesura> <w n="3.8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="3.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="3.10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="10">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="11" mp="F">e</seg>s</w> <w n="3.11">bl<seg phoneme="ø" type="vs" value="1" rule="403" place="12">eu</seg>s</w></l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">D</w>’<w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="4.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="4.4">ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="4.6" punct="vg:6">r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="4.8"><seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg>s</w> <w n="4.9">fr<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>l<seg phoneme="ø" type="vs" value="1" rule="398" place="12">eu</seg>x</w></l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">H<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="4" mp="C">eu</seg>rs</w> <w n="5.3">r<seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="5.4" punct="vg:9">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="M">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="9" punct="vg">é</seg>s</w>, <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="5.6">qu</w>’<w n="5.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="5.8">g<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="6" num="1.6" lm="12" met="6+6"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="6.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="6.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="6.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>t</w><caesura></caesura> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="6.7">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="435" place="9" mp="M">o</seg>nni<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg>s</w> <w n="6.8">s<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="M">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="7" num="1.7" lm="12" met="6+6"><w n="7.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="7.2">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="7.3" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="C">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="7.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="7.7">b<seg phoneme="o" type="vs" value="1" rule="315" place="11">eau</seg></w> <w n="7.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">em</seg>ps</w></l>
					<l n="8" num="1.8" lm="12" met="6+6"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="8.4">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="5" mp="M">ou</seg><seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.6">f<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">â</seg>tr<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="8.7" punct="vg:12">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="M">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12" punct="vg">em</seg>ps</w>,</l>
					<l n="9" num="1.9" lm="12" met="6+6"><w n="9.1" punct="vg:2">L<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" punct="vg">em</seg>ps</w>, <w n="9.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="C">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="5" mp="M">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="9.6">l</w>’<w n="9.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="9.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="10" mp="C">e</seg>s</w> <w n="9.9">f<seg phoneme="œ" type="vs" value="1" rule="406" place="11" mp="M">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
					<l n="10" num="1.10" lm="12" met="6+6"><w n="10.1">F<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="10.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="10.4">d</w>’<w n="10.5" punct="vg:6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg" caesura="1">o</seg>r</w>,<caesura></caesura> <w n="10.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="10.8" punct="vg:12">c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10" mp="M">o</seg>qu<seg phoneme="i" type="vs" value="1" rule="485" place="11" mp="M">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="11" num="1.11" lm="12" met="6+6"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="11.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="C">u</seg></w> <w n="11.3">j<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>t</w> <w n="11.4">d</w>’<w n="11.5"><seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg></w><caesura></caesura> <w n="11.6">l</w>’<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rc</w> <w n="11.8"><seg phoneme="o" type="vs" value="1" rule="318" place="8" mp="C">au</seg></w> <w n="11.9">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="10">e</seg>t</w> <w n="11.10" punct="vg:12">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311" place="12" punct="vg">an</seg>t</w>,</l>
					<l n="12" num="1.12" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">p<seg phoneme="wa" type="vs" value="1" rule="420" place="3" mp="M">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="12.5" punct="vg:6">p<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>rpr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>,<caesura></caesura> <w n="12.6">h<seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="12.7">d</w>’<w n="12.8"><seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.9"><seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg></w> <w n="12.10">d</w>’<w n="12.11" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="12" punct="pv">en</seg>t</w> ;</l>
					<l n="13" num="1.13" lm="12" met="6+6"><w n="13.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="13.2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2">en</seg></w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4">o</seg>r</w> <w n="13.4" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>r</w>,<caesura></caesura> <w n="13.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="13.7">l<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="13.8" punct="vg:12">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="14" num="1.14" lm="12" met="6+6"><w n="14.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="14.2">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="M">om</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="14.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>rds</w><caesura></caesura> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="14.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="14.7">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.8" punct="vg:12"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" mp="M">in</seg>qu<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="15" num="1.15" lm="12" met="6+6"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="15.2">r<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="15.4">br<seg phoneme="y" type="vs" value="1" rule="453" place="5" mp="M">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="15.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="15.6">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8">ein</seg>t</w> <w n="15.7">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>s</w> <w n="15.8"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="15.9" punct="vg:12">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="vg">e</seg>il</w>,</l>
					<l n="16" num="1.16" lm="12" met="6+6"><w n="16.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="16.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="16.3">j<seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="16.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="6" caesura="1">eu</seg>r</w><caesura></caesura> <w n="16.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="16.7">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="9">er</seg>s</w> <w n="16.8">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="16.9" punct="pe:12">s<seg phoneme="o" type="vs" value="1" rule="444" place="11" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="12" punct="pe">e</seg>il</w> !</l>
				</lg>
			</div></body></text></TEI>