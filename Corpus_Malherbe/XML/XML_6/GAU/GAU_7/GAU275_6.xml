<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU275" modus="sm" lm_max="7" metProfile="7">
					<head type="main">LA LUNE ET LE SOLEIL</head>
					<lg n="1">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="1.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="1.6" punct="dp:7">l<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="dp">e</seg></w> :</l>
						<l n="2" num="1.2" lm="7" met="7">— <w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w>-<w n="2.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="2.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="2.5">l</w>’<w n="2.6" punct="pi:7">h<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pi">on</seg></w> ?</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="3.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="3">en</seg></w> <w n="3.4" punct="vg:4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>rd</w>, <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="3.7" punct="vg:7">br<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="7" met="7"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="4.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="4.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="4.5" punct="pt:7">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="pt">on</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="7" met="7"><w n="5.1">L</w>’<w n="5.2">h<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="5.3" punct="vg:4">f<seg phoneme="a" type="vs" value="1" rule="193" place="4" punct="vg">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="5.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.6" punct="vg:7">h<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="6" num="2.2" lm="7" met="7"><w n="6.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="6.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="6.3" punct="vg:7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="7" punct="vg">e</seg>t</w>,</l>
						<l n="7" num="2.3" lm="7" met="7"><w n="7.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="7.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="7.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg></w> <w n="7.5" punct="vg:7">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="8" num="2.4" lm="7" met="7"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="8.6" punct="pt:7">v<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="7" punct="pt">e</seg>t</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="7" met="7"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="9.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rt</w> <w n="9.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="9.6" punct="pv:7">d<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></w> ;</l>
						<l n="10" num="3.2" lm="7" met="7"><w n="10.1" punct="vg:3">G<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">a</seg>s</w>, <w n="10.2">ch<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w>-<w n="10.3" punct="vg:7">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>s</w>,</l>
						<l n="11" num="3.3" lm="7" met="7"><w n="11.1">R<seg phoneme="o" type="vs" value="1" rule="415" place="1">ô</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="11.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="11.4" punct="pv:7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></w> ;</l>
						<l n="12" num="3.4" lm="7" met="7"><w n="12.1">N<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>rs</w> <w n="12.2"><seg phoneme="u" type="vs" value="1" rule="426" place="2">ou</seg></w> <w n="12.3" punct="vg:3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>cs</w>, <w n="12.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="12.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>ts</w> <w n="12.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="12.7" punct="pt:7">gr<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pt">i</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="7" met="7"><w n="13.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="13.2">pl<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>qu<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
						<l n="14" num="4.2" lm="7" met="7"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="14.4" punct="vg:7">l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="vg">in</seg>s</w>,</l>
						<l n="15" num="4.3" lm="7" met="7"><w n="15.1">Cr<seg phoneme="wa" type="vs" value="1" rule="440" place="1">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="15.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="15.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="15.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="15.5" punct="vg:7">pr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</w>,</l>
						<l n="16" num="4.4" lm="7" met="7"><w n="16.1">Su<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="16.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="16.4" punct="pt:7">cl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="pt">in</seg>s</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="7" met="7"><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="17.2" punct="vg:2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="vg">i</seg>t</w>, <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rs</w> <w n="17.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="17.5">s</w>’<w n="17.6" punct="pt:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>rh<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
						<l n="18" num="5.2" lm="7" met="7"><w n="18.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>s</w>-<w n="18.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="18.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="18.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="18.6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>r</w></l>
						<l n="19" num="5.3" lm="7" met="7"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">br<seg phoneme="u" type="vs" value="1" rule="427" place="2">ou</seg>ill<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd</w> <w n="19.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="19.4">l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>t</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="19.6">pl<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="20" num="5.4" lm="7" met="7"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="20.2">l</w>’<w n="20.3"><seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="20.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="20.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>c</w> <w n="20.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="20.7" punct="pi:7">m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="pi">oi</seg>r</w> ?</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="7" met="7"><w n="21.1">R<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>ds</w>-<w n="21.2" punct="pt:3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="3" punct="pt ti">oi</seg></w>. — <w n="21.3">J</w>’<w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="21.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>t</w> <w n="21.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
						<l n="22" num="6.2" lm="7" met="7"><w n="22.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="22.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="22.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="22.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="22.7" punct="vg:7">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="vg">eu</seg>x</w>,</l>
						<l n="23" num="6.3" lm="7" met="7"><w n="23.1">M<seg phoneme="œ" type="vs" value="1" rule="151" place="1">on</seg>si<seg phoneme="ø" type="vs" value="1" rule="397" place="2">eu</seg>r</w> <w n="23.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="23.3" punct="pv:4">fr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4" punct="pv">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="23.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="23.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>s</w> <w n="23.6"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="7">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
						<l n="24" num="6.4" lm="7" met="7"><w n="24.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="24.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="24.3">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="4">en</seg></w> <w n="24.4" punct="pe:7">c<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pe">eu</seg>x</w> !</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Généralife</placeName>,
							<date when="1844">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>