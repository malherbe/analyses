<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU272" modus="sm" lm_max="8" metProfile="8">
					<head type="main">L’ÉCHELLE D’AMOUR</head>
					<head type="form">SÉRÉNADE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lc<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="1.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="1.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="1.6">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">v<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="2.3" punct="ps:4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="ps">er</seg></w>… <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="353" place="5">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rts</w> <w n="2.5" punct="pe:8">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pe">u</seg>s</w> !</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="3.3">tr<seg phoneme="o" type="vs" value="1" rule="433" place="3">o</seg>p</w> <w n="3.4" punct="vg:4">h<seg phoneme="o" type="vs" value="1" rule="318" place="4" punct="vg">au</seg>t</w>, <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="3.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="3.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>s</w> <w n="3.8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">N</w>’<w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="384" place="2">ei</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="4.5">br<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="4.6" punct="pt:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="5.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>j<seg phoneme="u" type="vs" value="1" rule="d-2" place="3">ou</seg><seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="5.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.4">du<seg phoneme="ɛ" type="vs" value="1" rule="410" place="6">è</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1">J<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="6.3" punct="vg:4">c<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>lli<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="6.5">r<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7">an</seg></w> <w n="6.6">d</w>’<w n="6.7" punct="pv:8"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pv">o</seg>r</w> ;</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="7.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="7.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="7.5">t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="7.6">gu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="1">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3" punct="vg:4"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="vg">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="8.4"><seg phoneme="u" type="vs" value="1" rule="426" place="5">ou</seg></w> <w n="8.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6">en</seg></w> <w n="8.6" punct="ps:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="ps">o</seg>r</w>…</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="1">O</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="9.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="9.3" punct="vg:4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>rs</w>, <w n="9.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="9.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="9.6" punct="vg:8">p<seg phoneme="ɛ" type="vs" value="1" rule="384" place="8">ei</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="10.3">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4">oi</seg></w> <w n="10.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="10.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="10.6" punct="vg:8">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="vg">on</seg>gs</w>,</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">T<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t</w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.3">j<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="11.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6">fl<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>t</w> <w n="11.7">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2">j<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">am</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="12.3">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="12.6" punct="pt:8">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">Ai</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="13.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="13.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">L<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4">en</seg>t</w> <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.3" punct="vg:8">gr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8" punct="vg">ai</seg></w>,</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="15.2">j<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>squ</w>’<w n="15.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="15.4" punct="vg:4">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4" punct="vg">e</seg>l</w>, <w n="15.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="15.6"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="15.8" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="16.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="4">um</seg>s</w> <w n="16.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5" punct="pe:8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="8" punct="pe">ai</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1841">1841</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>