<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES, 1838-1845</title>
				<title type="sub_2">Tome second</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>625 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1838-1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU229" modus="cm" lm_max="12" metProfile="6+6">
			<head type="main">PRIÈRE</head>
			<lg n="1">
				<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="1.4">g<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rdi<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6" caesura="1">en</seg></w><caesura></caesura> <w n="1.5">pr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem/mp">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="8" mp="Lp">ez</seg></w>-<w n="1.6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="9">oi</seg></w> <w n="1.7">s<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="P">ou</seg>s</w> <w n="1.8">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.9" punct="pv:12"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
				<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="vg:2">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1" mp="M">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="2.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5" mp="M">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" mp="M">ai</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="2.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="C">ou</seg>s</w> <w n="2.7" punct="vg:12">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="vg">er</seg></w>,</l>
				<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="3.3">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="3.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" caesura="1">ain</seg></w><caesura></caesura> <w n="3.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="3.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="9">ain</seg></w> <w n="3.7" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="11" mp="M">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358" place="12">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
				<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>r</w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="4.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="4.6">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="4.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="4.8" punct="pe:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pe">er</seg></w> !</l>
			</lg>
			<lg n="2">
				<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="5.2">J<seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="5.4">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>x</w> <w n="5.5" punct="vg:6">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" punct="vg" caesura="1">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg>x</w> <w n="5.7">c<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="5.8" punct="vg:12">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
				<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="M">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="353" place="2" mp="M">e</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="C">au</seg>x</w> <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="6.5">s</w>’<w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="Pem">e</seg></w> <w n="6.8" punct="pv:12">lu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pv">i</seg></w> ;</l>
				<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="7.3">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" mp="M">in</seg>d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="M">u</seg>lg<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="7.5"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="C">i</seg>l</w> <w n="7.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>t</w> <w n="7.7">l<seg phoneme="œ" type="vs" value="1" rule="407" place="10" mp="C">eu</seg>rs</w> <w n="7.8" punct="vg:12">c<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
				<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">j<seg phoneme="u" type="vs" value="1" rule="d-2" place="2" mp="M">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>c</w> <w n="8.4"><seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="8.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="8.6">t<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420" place="9" mp="M">oi</seg>gn<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="8.7">d</w>’<w n="8.8" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="359" place="11" mp="M">en</seg>nu<seg phoneme="i" type="vs" value="1" rule="491" place="12" punct="pt">i</seg></w>.</l>
			</lg>
			<lg n="3">
				<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="9.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="9.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="9.4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">ez</seg></w><caesura></caesura> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="9.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="9.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="315" place="10">eau</seg>x</w> <w n="9.8">d</w>’<w n="9.9"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
				<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="10.4" punct="vg:3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3" punct="vg">oi</seg>t</w>, <w n="10.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="10.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>d</w> <w n="10.7">d</w>’<w n="10.8" punct="vg:6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" punct="vg" caesura="1">o</seg>r</w>,<caesura></caesura> <w n="10.9">l</w>’<w n="10.10"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="8">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="10.11">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg></w></l>
				<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">Pr<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="11.4" punct="vg:6">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="6" punct="vg" caesura="1">aim</seg></w>,<caesura></caesura> <w n="11.5">pr<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="11.8">b<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
				<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="12.2">gr<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="12.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.5">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>d</w><caesura></caesura> <w n="12.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="12.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="12.8">r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="9">o</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.9" punct="pv:12"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pv">é</seg></w> ;</l>
			</lg>
			<lg n="4">
				<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="F">e</seg></w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="13.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg></w><caesura></caesura> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="13.6">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.7" punct="vg:12">d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
				<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="P">a</seg>r</w> <w n="14.2" punct="vg:3">p<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="3" punct="vg">é</seg></w>, <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" mp="M/mp">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="5" mp="Lp">ez</seg></w>-<w n="14.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg></w><caesura></caesura> <w n="14.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="14.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="9" mp="P">u</seg>r</w> <w n="14.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="10" mp="C">o</seg>s</w> <w n="14.8" punct="vg:12">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="vg">ou</seg>x</w>,</l>
				<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="15.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="15.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="15.4" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="15.5" punct="vg:9"><seg phoneme="i" type="vs" value="1" rule="468" place="7" mp="M">i</seg>s<seg phoneme="o" type="vs" value="1" rule="444" place="8" mp="M">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="9" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="15.6" punct="vg:12"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" mp="M">o</seg>rph<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>l<seg phoneme="i" type="vs" value="1" rule="467" place="12">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
				<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="16.2">n</w>’<w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="16.4">d</w>’<w n="16.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="16.6">qu</w>’<w n="16.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="16.8" punct="vg:6">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="vg" caesura="1">eu</seg></w>,<caesura></caesura> <w n="16.9">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="16.10">n</w>’<w n="16.11"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="8">ai</seg></w> <w n="16.12">d</w>’<w n="16.13"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r</w> <w n="16.14">qu</w>’<w n="16.15"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="11">en</seg></w> <w n="16.16" punct="pe:12">v<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pe">ou</seg>s</w> !</l>
			</lg>
		</div></body></text></TEI>