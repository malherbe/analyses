<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉMAUX ET CAMÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2510 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2012</date>
				<idno type="local">GAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Émaux et Camées</title>
								<author>Théophile Gautier</author>
								<imprint>
									<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
									<date when="1895">1895</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Émaux et Camées</title>
						<author>Théophile Gautier</author>
						<imprint>
							<publisher>G. CHARPENTIER et E. FASQUELLE</publisher>
							<date when="1872">1872</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Texte vérifié avec l’édition Charpentier et Cie, 1872</p>
				<p>Les corrections signalées dans la version électronique n’ont pas été intégrées ; erreurs absentes de l’édition de référence</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU20" modus="sm" lm_max="8" metProfile="8">
				<head type="main">FUMÉE</head>
				<lg n="1">
					<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1">à</seg></w>-<w n="1.2" punct="vg:2">b<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>s</w>, <w n="1.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>s</w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="1.6">s</w>’<w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="2" num="1.2" lm="8" met="8"><w n="2.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">ch<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="2.4">d<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="2.5" punct="pv:8">b<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pv">u</seg></w> ;</l>
					<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">t<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="3.3" punct="vg:4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.5">m<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="3.6">s</w>’<w n="3.7" punct="vg:8"><seg phoneme="e" type="vs" value="1" rule="353" place="7">e</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="4" num="1.4" lm="8" met="8"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="4.2">s<seg phoneme="œ" type="vs" value="1" rule="406" place="2">eu</seg>il</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="4.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="4.7" punct="pt:8">m<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1" lm="8" met="8"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="5.2" punct="vg:3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3" punct="vg">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="5.4">v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>t</w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.6" punct="pv:8">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
					<l n="6" num="2.2" lm="8" met="8"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="6.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="6.3" punct="vg:4">t<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>s</w>, <w n="6.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="6.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>ps</w> <w n="6.7">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>d</w></l>
					<l n="7" num="2.3" lm="8" met="8"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="7.2">ti<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="7.3">h<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385" place="4">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.4">d</w>’<w n="7.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.6" punct="vg:8">b<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="8" num="2.4" lm="8" met="8"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="8.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="8.3">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.4" punct="pt:8">v<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>t</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="9.2">t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="9.3">b<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.5" punct="vg:8">f<seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
					<l n="10" num="3.2" lm="8" met="8"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rn<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="10.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="10.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="10.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190" place="7">e</seg>t</w> <w n="10.5" punct="vg:8">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg></w>,</l>
					<l n="11" num="3.3" lm="8" met="8"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="11.5">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="11.6">b<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
					<l n="12" num="3.4" lm="8" met="8"><w n="12.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="12.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="12.5" punct="pt:8">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></w>.</l>
				</lg>
			</div></body></text></TEI>