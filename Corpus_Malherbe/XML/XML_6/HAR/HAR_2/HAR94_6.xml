<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES FORMES</head><div type="poem" key="HAR94" modus="cm" lm_max="12" metProfile="6+6">
						<head type="main">LA MORT DES ROIS</head>
						<opener>
							<salute>À SAINT-JUIRS</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="1.3" punct="vg:4">L<seg phoneme="i" type="vs" value="1" rule="d-1" place="3" mp="M">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg></w>, <w n="1.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="1.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="1.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="1.7">h<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11">ai</seg>t</w> <w n="1.9" punct="vg:12">pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
							<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="2.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="2.6">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>rt</w> <w n="2.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>t</w> <w n="2.8" punct="dp:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="dp">er</seg></w> :</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1" punct="vg:4">P<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="4" punct="vg">en</seg>t</w>, <w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="C">i</seg>l</w> <w n="3.3">s</w>’<w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6" caesura="1">e</seg>st</w><caesura></caesura> <w n="3.5" punct="vg:8">l<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="8" punct="vg">é</seg></w>, <w n="3.6">dr<seg phoneme="wa" type="vs" value="1" rule="420" place="9">oi</seg>t</w> <w n="3.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="3.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="3.9" punct="pt:12">r<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="4" num="2.1" lm="12" met="6+6"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="4.2">fr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="4.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="4.4">d<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="4.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>gr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="C">au</seg></w> <w n="4.7">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>c</w> <w n="4.8">s<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>c</w> <w n="4.9">d<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="C">u</seg></w> <w n="4.10">d<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>tti<seg phoneme="e" type="vs" value="1" rule="347" place="12">er</seg></w></l>
							<l n="5" num="2.2" lm="12" met="6+6"><w n="5.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="5.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="5.3">gr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ff<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="5.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="5.6">qu<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w><caesura></caesura> <w n="5.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="5.8">d<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="5.9">l</w>’<w n="5.10" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
							<l n="6" num="2.3" lm="12" met="6+6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3" punct="vg:4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="4" punct="vg">à</seg></w>, <w n="6.4" punct="vg:6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>f</w>,<caesura></caesura> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="6.6">gr<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>t</w> <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="6.8" punct="pt:12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11" mp="M">en</seg>ti<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg></w>.</l>
						</lg>
						<lg n="3">
							<l n="7" num="3.1" lm="12" met="6+6"><w n="7.1">T<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="7.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="7.3">j<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="190" place="5">e</seg>ts</w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" caesura="1">a</seg>s</w><caesura></caesura> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="7.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10">an</seg>t</w> <w n="7.7">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="7.8" punct="vg:12">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
							<l n="8" num="3.2" lm="12" met="6+6"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="8.2" punct="vg:3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="8.3" punct="vg:4">l<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>rd</w>, <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="8.5" punct="vg:6">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6" punct="vg" caesura="1">en</seg>t</w>,<caesura></caesura> <w n="8.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="8.8" punct="vg:10">d<seg phoneme="y" type="vs" value="1" rule="453" place="9">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" punct="vg" mp="F">e</seg></w>, <w n="8.9">s</w>’<w n="8.10" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257" place="12" punct="vg">eoi</seg>r</w>,</l>
							<l n="9" num="3.3" lm="12" met="6+6"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="9.2">pi<seg phoneme="e" type="vs" value="1" rule="241" place="2">e</seg>ds</w> <w n="9.3" punct="vg:3">j<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3" punct="vg">oin</seg>ts</w>, <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="9.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="9.6">h<seg phoneme="o" type="vs" value="1" rule="318" place="6" caesura="1">au</seg>t</w><caesura></caesura> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="9.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="9.9">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="9">in</seg>s</w> <w n="9.10">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="9.11">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="11" mp="C">e</seg></w> <w n="9.12" punct="pt:12">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="12">o</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
						</lg>
						<lg n="4">
							<l n="10" num="4.1" lm="12" met="6+6"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="10.3">l</w>’<w n="10.4">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.5"><seg phoneme="u" type="vs" value="1" rule="426" place="3">où</seg></w> <w n="10.6">l</w>’<w n="10.7" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">I</seg>sr<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>h<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg></w>,<caesura></caesura> <w n="10.8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>s</w> <w n="10.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="10.10">v<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>p<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>rs</w> <w n="10.11">d<seg phoneme="y" type="vs" value="1" rule="450" place="11" mp="C">u</seg></w> <w n="10.12" punct="vg:12">s<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="vg">oi</seg>r</w>,</l>
							<l n="11" num="4.2" lm="12" met="6+6"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="11.3">br<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>si<seg phoneme="e" type="vs" value="1" rule="347" place="6" caesura="1">er</seg></w><caesura></caesura> <w n="11.4">t<seg phoneme="o" type="vs" value="1" rule="435" place="7" mp="M">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="11.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="11.7" punct="vg:12">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
							<l n="12" num="4.3" lm="12" met="6+6"><w n="12.1">F<seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="12.3">cr<seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="12.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="6" caesura="1">oin</seg></w><caesura></caesura> <w n="12.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="12.8">v<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.9" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>s<seg phoneme="wa" type="vs" value="1" rule="420" place="12" punct="pt">oi</seg>r</w>.</l>
						</lg>
						<lg n="5">
							<l n="13" num="5.1" lm="12" met="6+6"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="13.2">S<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>il</w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="5" mp="M">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6" caesura="1">é</seg></w><caesura></caesura> <w n="13.4" punct="vg:7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" punct="vg">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.5" punct="vg:9"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" punct="vg">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189" place="10">e</seg>t</w> <w n="13.7">s</w>’<w n="13.8" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
							<l n="14" num="5.2" lm="12" met="6+6"><w n="14.1" punct="vg:1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg>s</w>, <w n="14.2">s</w>’<w n="14.3" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="353" place="2" mp="M">e</seg>ff<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" punct="vg">on</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="14.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="P">a</seg>r</w> <w n="14.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="14.7">h<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11">on</seg>s</w> <w n="14.8">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="12">an</seg>s</w></l>
							<l n="15" num="5.3" lm="12" met="6+6"><w n="15.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="15.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="15.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg>s</w> <w n="15.4">r<seg phoneme="ɛ" type="vs" value="1" rule="339" place="5" mp="M">a</seg>y<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="15.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="15.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="15.7">l</w>’<w n="15.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
						</lg>
						<lg n="6">
							<l n="16" num="6.1" lm="12" met="6+6"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>g<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="16.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="16.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rd</w><caesura></caesura> <w n="16.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="16.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="16.8">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="16.9" punct="dp:12">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="dp">an</seg>ts</w> :</l>
							<l n="17" num="6.2" lm="12" met="6+6"><w n="17.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="17.2">v<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="17.3"><seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="17.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="17.6">j<seg phoneme="ɛ" type="vs" value="1" rule="190" place="8">e</seg>ts</w> <w n="17.7"><seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="M">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
							<l n="18" num="6.3" lm="12" met="6+6"><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="18.2">bl<seg phoneme="e" type="vs" value="1" rule="353" place="2" mp="M">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" mp="F">e</seg>s</w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="18.4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="18.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="18.6">s</w>’<w n="18.7"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="18.8"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="18.9">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="18.10" punct="pt:12">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>cs</w>.</l>
						</lg>
						<lg n="7">
							<l n="19" num="7.1" lm="12" met="6+6"><w n="19.1" punct="vg:1"><seg phoneme="e" type="vs" value="1" rule="189" place="1" punct="vg">E</seg>t</w>, <w n="19.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="2" mp="P">an</seg>s</w> <w n="19.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="19.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="19.5">tr<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="6" caesura="1">eau</seg>x</w><caesura></caesura> <w n="19.6">d</w>’<w n="19.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="19.8">m<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg>s</w></l>
							<l n="20" num="7.2" lm="12" met="6+6"><w n="20.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="20.3">y<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="20.4">cl<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="20.5">c<seg phoneme="o" type="vs" value="1" rule="444" place="7" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="20.6"><seg phoneme="a" type="vs" value="1" rule="342" place="10" mp="P">à</seg></w> <w n="20.7">s</w>’<w n="20.8" punct="vg:12"><seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="vg">i</seg>r</w>,</l>
							<l n="21" num="7.3" lm="12" met="6+6"><w n="21.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="21.2" punct="vg:2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg>x</w>, <w n="21.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="Lc">a</seg>r</w>-<w n="21.4">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem/mc">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="21.5">l</w>’<w n="21.6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="6" caesura="1">o</seg>r</w><caesura></caesura> <w n="21.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="21.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="21.9">p<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rpr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="21.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="21.11" punct="vg:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
						</lg>
						<lg n="8">
							<l n="22" num="8.1" lm="12" met="6+6"><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="22.2">F<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="22.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="22.5">S<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" caesura="1">e</seg>il</w><caesura></caesura> <w n="22.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="22.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>nt</w> <w n="22.8" punct="pt:12">m<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>r</w>.</l>
						</lg>
					</div></body></text></TEI>