<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La Légende des Sexes</title>
				<title type="sub">Poëmes Hystériques</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https ://fr.wikisource.org/wiki/La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Légende des sexes, poëmes hystériques</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https ://fr.wikisource.org/wiki/Fichier :Haraucourt_-_La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques,_1882.djvu</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Imprimé à Bruxelles pour l’auteur</publisher>
									<date when="1882">1882</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/btv1b8618380c</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>ÉDITION PRIVILE, REVUE PAR L’AUTEUR</publisher>
							<date when="1893">1893</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1882">1882</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-28" who="RR">Une correction introduite à partir de l’édition imprimée de 1893</change>
				<change when="2017-10-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La légende des sexes</head><div type="poem" key="HAR19" modus="sp" lm_max="7" metProfile="7, 4">
					<head type="main">L’OBSESSION</head>
					<opener>
						<salute>À Charles Morice.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="1.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="1.4" punct="vg:7">v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></w>,</l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">t</w>’<w n="2.3" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="2.4" punct="vg:5">F<seg phoneme="a" type="vs" value="1" rule="193" place="4">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg">e</seg></w>, <w n="2.5" punct="pe:7">B<seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pe">é</seg></w> !</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="3.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="3.4">F<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-27" place="5">e</seg></w> <w n="3.5">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w></l>
						<l n="4" num="1.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3" punct="dp:4">l<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>x<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="dp">e</seg></w> :</l>
						<l n="5" num="1.5" lm="7" met="7"><w n="5.1">Br<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="d-2" place="3">ou</seg><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="5.4" punct="vg:7">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>r</w>,</l>
						<l n="6" num="1.6" lm="7" met="7"><w n="6.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r</w> <w n="6.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>mn<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="6.4">g<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r</w></l>
						<l n="7" num="1.7" lm="7" met="7"><w n="7.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3">m<seg phoneme="ø" type="vs" value="1" rule="402" place="3">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="7.5">d<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r</w></l>
						<l n="8" num="1.8" lm="4" met="4"><space unit="char" quantity="6"></space><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="8.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3" punct="pt:4">pr<seg phoneme="e" type="vs" value="1" rule="353" place="3">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1" lm="7" met="7"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="9.2">r<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t</w> <w n="9.3">f<seg phoneme="u" type="vs" value="1" rule="426" place="3">ou</seg></w> <w n="9.4">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4">en</seg>t</w> <w n="9.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="9.6" punct="dp:7">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7" punct="dp">in</seg></w> :</l>
						<l n="10" num="2.2" lm="7" met="7"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="10.2">j</w>’<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="10.5">f<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg></w></l>
						<l n="11" num="2.3" lm="7" met="7"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="11.2">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="11.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="11.4">j<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>squ</w>’<w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="11.6">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg></w></l>
						<l n="12" num="2.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="12.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="12.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g</w> <w n="12.3" punct="pv:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pv">e</seg></w> ;</l>
						<l n="13" num="2.5" lm="7" met="7"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">su<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>s</w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="13.4">j<seg phoneme="wa" type="vs" value="1" rule="440" place="4">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="13.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493" place="7">y</seg>r</w></l>
						<l n="14" num="2.6" lm="7" met="7"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="14.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="14.3">gr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="14.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r</w></l>
						<l n="15" num="2.7" lm="7" met="7"><w n="15.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="15.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r</w> <w n="15.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="15.4">s</w>’<w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>gl<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r</w></l>
						<l n="16" num="2.8" lm="4" met="4"><space unit="char" quantity="6"></space><w n="16.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="16.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t</w> <w n="16.4" punct="pt:4">r<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1" lm="7" met="7"><w n="17.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="17.2" punct="vg:4">f<seg phoneme="a" type="vs" value="1" rule="193" place="3">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="17.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="17.5" punct="pe:7">v<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pe">eu</seg>x</w> !</l>
						<l n="18" num="3.2" lm="7" met="7"><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="18.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="18.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>squ</w>’<w n="18.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="18.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w></l>
						<l n="19" num="3.3" lm="7" met="7"><w n="19.1">J</w>’<w n="19.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="19.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="19.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="19.5">v<seg phoneme="ø" type="vs" value="1" rule="248" place="7">œu</seg>x</w></l>
						<l n="20" num="3.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="20.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="20.2" punct="dp:4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="457" place="4">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="dp">e</seg>s</w> :</l>
						<l n="21" num="3.5" lm="7" met="7"><w n="21.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="21.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="21.3">j<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>s</w> <w n="21.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s</w></l>
						<l n="22" num="3.6" lm="7" met="7"><w n="22.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="22.2">r<seg phoneme="ɛ" type="vs" value="1" rule="411" place="2">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="22.3"><seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>s</w></l>
						<l n="23" num="3.7" lm="7" met="7"><w n="23.1">Gl<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2">e</seg>nt</w> <w n="23.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="23.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rn<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w> <w n="23.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg>s</w></l>
						<l n="24" num="3.8" lm="4" met="4"><space unit="char" quantity="6"></space><w n="24.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1">e</seg>rs</w> <w n="24.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="24.3">p<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="24.4" punct="pt:4">n<seg phoneme="y" type="vs" value="1" rule="457" place="4">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1" lm="7" met="7"><w n="25.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="25.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>sh<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="25.3">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="25.4" punct="dp:7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7" punct="dp">ein</seg>s</w> :</l>
						<l n="26" num="4.2" lm="7" met="7"><w n="26.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="26.2" punct="vg:4">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="3">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4" punct="vg">e</seg>s</w>, <w n="26.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="26.4" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="353" place="6">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7" punct="vg">aim</seg>s</w>,</l>
						<l n="27" num="4.3" lm="7" met="7"><w n="27.1">S</w>’<w n="27.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="27.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="27.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="27.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>s</w></l>
						<l n="28" num="4.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="28.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="28.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="28.3" punct="pv:4">p<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pv">e</seg>s</w> ;</l>
						<l n="29" num="4.5" lm="7" met="7"><w n="29.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="29.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="29.3">v<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="29.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="29.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="29.6" punct="vg:7">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="vg">an</seg>cs</w>,</l>
						<l n="30" num="4.6" lm="7" met="7"><w n="30.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="30.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="30.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="5">um</seg>s</w> <w n="30.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>ts</w></l>
						<l n="31" num="4.7" lm="7" met="7"><w n="31.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="31.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="31.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="31.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="5">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="31.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>cs</w></l>
						<l n="32" num="4.8" lm="4" met="4"><space unit="char" quantity="6"></space><w n="32.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="32.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="32.3" punct="pt:4">n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1" lm="7" met="7"><w n="33.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="33.2" punct="pt:3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="3" punct="pt">i</seg></w>. <w n="33.3" punct="vg:5">N<seg phoneme="ɛ̃" type="vs" value="1" rule="494" place="4">ym</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" punct="vg">e</seg>s</w>, <w n="33.4">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w></l>
						<l n="34" num="5.2" lm="7" met="7"><w n="34.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="34.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="34.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="34.4">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="34.5"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w></l>
						<l n="35" num="5.3" lm="7" met="7"><w n="35.1">L</w>’<w n="35.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt</w> <w n="35.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="35.4">p<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>c<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="35.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="35.6">p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>rs</w></l>
						<l n="36" num="5.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="36.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="36.2" punct="dp:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="457" place="4">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="dp">e</seg>s</w> :</l>
						<l n="37" num="5.5" lm="7" met="7"><w n="37.1">V<seg phoneme="o" type="vs" value="1" rule="438" place="1">o</seg>s</w> <w n="37.2" punct="vg:3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" punct="vg">e</seg>s</w>, <w n="37.3" punct="vg:7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>bst<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="7" punct="vg">en</seg>t</w>,</l>
						<l n="38" num="5.6" lm="7" met="7"><w n="38.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="38.2">t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>nt</w> <w n="38.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="38.5" punct="pv:7"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="pv">an</seg>t</w> ;</l>
						<l n="39" num="5.7" lm="7" met="7"><w n="39.1">J</w>’<w n="39.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="39.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="39.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>gs</w> <w n="39.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rds</w> <w n="39.6">d</w>’<w n="39.7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w></l>
						<l n="40" num="5.8" lm="4" met="4"><space unit="char" quantity="6"></space><w n="40.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="40.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="40.3" punct="pt:4">st<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="y" type="vs" value="1" rule="457" place="4">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1" lm="7" met="7"><w n="41.1" punct="vg:1">D<seg phoneme="u" type="vs" value="1" rule="425" place="1" punct="vg">ou</seg>x</w>, <w n="41.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="41.3">pr<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="41.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="41.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7">ain</seg></w></l>
						<l n="42" num="6.2" lm="7" met="7"><w n="42.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="42.2">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="42.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="42.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="42.5" punct="vg:7">h<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7" punct="vg">ain</seg></w>,</l>
						<l n="43" num="6.3" lm="7" met="7"><w n="43.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="43.2">j</w>’<w n="43.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="43.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="43.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="43.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg></w></l>
						<l n="44" num="6.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="44.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="44.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="44.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="44.4" punct="pt:4">l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg>s</w>.</l>
						<l n="45" num="6.5" lm="7" met="7"><w n="45.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="45.2">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="45.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="45.4">f<seg phoneme="u" type="vs" value="1" rule="428" place="4">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="45.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="45.6" punct="pv:7">pl<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pv">i</seg>s</w> ;</l>
						<l n="46" num="6.6" lm="7" met="7"><w n="46.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="46.2">s<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>r</w> <w n="46.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="46.4">t<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="46.5" punct="vg:7">p<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>s</w>,</l>
						<l n="47" num="6.7" lm="7" met="7"><w n="47.1">B<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="47.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="47.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>s</w> <w n="47.4" punct="vg:7"><seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>s</w>,</l>
						<l n="48" num="6.8" lm="4" met="4"><space unit="char" quantity="6"></space><w n="48.1">J</w>’<w n="48.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rs</w> <w n="48.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="48.4" punct="pt:4">fi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1" lm="7" met="7">— <w n="49.1" punct="vg:2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="49.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>rs</w> <w n="49.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w></l>
						<l n="50" num="7.2" lm="7" met="7"><w n="50.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="50.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="50.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>fs</w> <w n="50.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="50.5" punct="vg:7">v<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg></w>,</l>
						<l n="51" num="7.3" lm="7" met="7"><w n="51.1">J</w>’<w n="51.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="51.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="51.4">l<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>br<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w></l>
						<l n="52" num="7.4" lm="4" met="4"><space unit="char" quantity="6"></space><w n="52.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="52.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="2">e</seg>rs</w> <w n="52.3"><seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w>-<w n="52.4" punct="pv:4">m<seg phoneme="ɛ" type="vs" value="1" rule="411" place="4">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pv">e</seg>s</w> ;</l>
						<l n="53" num="7.5" lm="7" met="7"><w n="53.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="53.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="53.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="53.4">n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rfs</w> <w n="53.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="53.6" punct="vg:7">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="vg">é</seg>s</w>,</l>
						<l n="54" num="7.6" lm="7" met="7"><w n="54.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="54.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="54.3">b<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="54.4" punct="dp:5">cr<seg phoneme="i" type="vs" value="1" rule="469" place="5" punct="dp">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> : <w n="54.5" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="7" punct="vg">ez</seg></w>,</l>
						<l n="55" num="7.7" lm="7" met="7"><w n="55.1">J</w>’<w n="55.2"><seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>n<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="55.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="55.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg>s</w></l>
						<l n="56" num="7.8" lm="4" met="4"><space unit="char" quantity="6"></space><w n="56.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="56.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="56.3" punct="pe:4">p<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="5" punct="pe">e</seg>s</w> !</l>
					</lg>
				</div></body></text></TEI>