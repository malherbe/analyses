<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES254" modus="sm" lm_max="8" metProfile="8">
					<head type="main">JAMAIS ADIEU</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Et vers le ciel se frayant un chemin <lb></lb>
									Ils sont partis en se donnant la main.
								</quote>
								 <bibl><hi rend="smallcap">de Béranger.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">t</w>’<w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="1.4">v<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="1.5" punct="vg:4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</w>, <w n="1.6">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="1.8" punct="dp:8">r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">L</w>’<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.4" punct="vg:4">v<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg">eu</seg>t</w>, <w n="2.5">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s</w>-<w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="2.7">l</w>’<w n="2.8" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="pt">ou</seg>r</w>.</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="3.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt</w> <w n="3.3">s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7">un</seg></w> <w n="3.6" punct="dp:8">j<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="dp">ou</seg>r</w> :</l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="4.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>s</w> <w n="4.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4" punct="pv:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" punct="pv">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="4.5" punct="pe:5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="pe">a</seg>h</w> ! <w n="4.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="4.7" punct="pe:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="5.3">m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="5.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="5.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="5.6" punct="pv:8">li<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pv">eu</seg></w> ;</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1" punct="dp:1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="dp">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> : <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="6.3" punct="pv:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pv">oi</seg>r</w> ; <w n="6.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="6.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1" lm="8" met="8"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="7.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="7.5">l</w>’<w n="7.6" punct="pe:8"><seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
						<l n="8" num="3.2" lm="8" met="8"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="8.3">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="8.6">l</w>’<w n="8.7" punct="pi:8"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pi">o</seg>r</w> ?</l>
						<l n="9" num="3.3" lm="8" met="8"><w n="9.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="9.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="9.3" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4" punct="vg">en</seg>s</w>, <w n="9.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w>-<w n="9.5">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="9.6" punct="pi:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="pi">o</seg>r</w> ?</l>
						<l n="10" num="3.4" lm="8" met="8"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w>-<w n="10.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="10.5" punct="pi:8">s<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
					</lg>
					<lg n="4">
						<l n="11" num="4.1" lm="8" met="8"><w n="11.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="11.3">m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="11.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="11.6" punct="pv:8">li<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pv">eu</seg></w> ;</l>
						<l n="12" num="4.2" lm="8" met="8"><w n="12.1" punct="dp:1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="dp">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> : <w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="12.3" punct="pv:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pv">oi</seg>r</w> ; <w n="12.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="12.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1" lm="8" met="8"><w n="13.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r</w> <w n="13.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w>-<w n="13.3" punct="dp:4">t<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="dp">u</seg></w> : <w n="13.4">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="13.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="6">i</seg>s</w> <w n="13.6" punct="pe:8">f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
						<l n="14" num="5.2" lm="8" met="8"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>ls</w> <w n="14.2" punct="dp:4">r<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="dp">on</seg>t</w> : <w n="14.3">Cr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="14.4" punct="pv:8">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rfl<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pv">u</seg>s</w> ;</l>
						<l n="15" num="5.3" lm="8" met="8"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="15.2" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444" place="4" punct="vg">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="15.4">n</w>’<w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>d</w> <w n="15.6" punct="pt:8">pl<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pt">u</seg>s</w>.</l>
						<l n="16" num="5.4" lm="8" met="8"><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="16.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="16.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>s</w> <w n="16.5"><seg phoneme="y" type="vs" value="1" rule="391" place="5">eu</seg>t</w> <w n="16.6">p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="16.7">d</w>’<w n="16.8" punct="pe:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg></w> !</l>
					</lg>
					<lg n="6">
						<l n="17" num="6.1" lm="8" met="8"><w n="17.1">V<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="e" type="vs" value="1" rule="189" place="2">e</seg>t</w> <w n="17.3">m<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r</w> <w n="17.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="17.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="6">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="17.6" punct="pv:8">li<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pv">eu</seg></w> ;</l>
						<l n="18" num="6.2" lm="8" met="8"><w n="18.1" punct="dp:1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" punct="dp">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> : <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="18.3" punct="pv:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pv">oi</seg>r</w> ; <w n="18.4">j<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>s</w> <w n="18.5" punct="pt:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg></w>.</l>
					</lg>
				</div></body></text></TEI>