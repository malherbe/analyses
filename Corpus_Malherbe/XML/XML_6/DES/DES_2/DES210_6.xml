<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES210" modus="cm" lm_max="12" metProfile="6+6">
					<head type="main">AMOUR</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Trop faibles que nous sommes ; <lb></lb>
									C’est toujours cet amour qui tourmente les hommes.
								</quote>
								 <bibl><hi rend="smallcap">André Chénier.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="1.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="1.3">j</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="1.7" punct="vg:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg" caesura="1">œu</seg>r</w>,<caesura></caesura> <w n="1.8">br<seg phoneme="y" type="vs" value="1" rule="445" place="7" mp="M">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="1.9">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.10">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="12">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="2.2">j</w>’<w n="2.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.4">t</w>’<w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="2.6" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="2.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="7" mp="M">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="8">en</seg>t</w> <w n="2.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="2.9" punct="pi:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>f<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pi">i</seg>r</w> ?</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w>-<w n="3.2">c<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2" mp="C">un</seg></w> <w n="3.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="3.6">fr<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg></w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="3.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="3.9" punct="pi:12"><seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></w> ?</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1" mp="C">Un</seg></w> <w n="4.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="M">o</seg>rtr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="4.4">n<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="4.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="4.7" punct="pi:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pi">i</seg>r</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1" punct="pe:1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1" punct="pe">oi</seg>s</w> ! <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="5.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="5.5">c</w>’<w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="5.7" punct="vg:6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="5.8">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="5.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="9" mp="P">an</seg>s</w> <w n="5.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="5.11" punct="vg:12"><seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="6.3" punct="pv:4">s<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" punct="pv">e</seg>il</w> ; <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="133" place="5">e</seg>h</w> <w n="6.5" punct="pe:6">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="6" punct="pe" caesura="1">oi</seg></w> !<caesura></caesura> <w n="6.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="7" mp="Lp">eu</seg>t</w>-<w n="6.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="6.8">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="9" mp="M">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="10">er</seg></w> <w n="6.9" punct="pi:12">t<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pi">ou</seg>rs</w> ?</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="7.2">b<seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="7.5">d<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="7.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="7.7" punct="vg:12">pr<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">Tr<seg phoneme="o" type="vs" value="1" rule="433" place="1">o</seg>p</w> <w n="8.2">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>pu<seg phoneme="i" type="vs" value="1" rule="491" place="4" mp="M">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">ai</seg>t</w><caesura></caesura> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="8.5">fl<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="8.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="8.8" punct="pt:12">j<seg phoneme="u" type="vs" value="1" rule="425" place="12" punct="pt">ou</seg>rs</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="9.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5" mp="Lc">eu</seg>t</w>-<w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="6" caesura="1">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="9.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="9.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="9.8">n<seg phoneme="o" type="vs" value="1" rule="438" place="11" mp="C">o</seg>s</w> <w n="9.9" punct="pv:12">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg>s</w> ;</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">P<seg phoneme="ø" type="vs" value="1" rule="398" place="1" mp="Lc">eu</seg>t</w>-<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="Fc">e</seg></w> <w n="10.4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="341" place="6" caesura="1">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="10.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8" mp="M">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="9">é</seg></w> <w n="10.8">d<seg phoneme="ø" type="vs" value="1" rule="398" place="10">eu</seg>x</w> <w n="10.9" punct="pt:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pt">an</seg>ts</w>.</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="11.3">m<seg phoneme="wa" type="vs" value="1" rule="420" place="3" mp="M">oi</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="11.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="342" place="7" mp="P">à</seg></w> <w n="11.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="11.8">j<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rs</w> <w n="11.9" punct="vg:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>ph<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" mp="Fc">e</seg></w> <w n="12.2">b<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t</w> <w n="12.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="12.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="12.5" punct="vg:6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6" punct="vg" caesura="1">ein</seg></w>,<caesura></caesura> <w n="12.6"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="12.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="12.8">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>ts</w> <w n="12.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg>t</w> <w n="12.10" punct="pe:12">v<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="12" punct="pe">an</seg>ts</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="12" met="6+6"><w n="13.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="P">ou</seg>s</w> <w n="13.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="13.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="13.5">f<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg></w><caesura></caesura> <w n="13.6">j</w>’<w n="13.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">em</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="9">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="13.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="13.9" punct="pt:12">v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
						<l n="14" num="4.2" lm="12" met="6+6"><w n="14.1" punct="vg:1">L<seg phoneme="a" type="vs" value="1" rule="342" place="1" punct="vg">à</seg></w>, <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="14.3">t</w>’<w n="14.4" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3" punct="vg">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="14.5" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="467" place="4" mp="M">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444" place="5" mp="M">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="vg" caesura="1">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="14.7">t<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="14.8">n</w>’<w n="14.9"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="9">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="10" mp="F">e</seg>s</w> <w n="14.10">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="11">e</seg></w> <w n="14.11" punct="pt:12">m<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pt">oi</seg></w>.</l>
						<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="15.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="15.3">d</w>’<w n="15.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="15.5">t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>l</w> <w n="15.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438" place="6" caesura="1">o</seg>s</w><caesura></caesura> <w n="15.7">l</w>’<w n="15.8"><seg phoneme="e" type="vs" value="1" rule="354" place="7" mp="M">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468" place="8" mp="M">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.9"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="15.10" punct="vg:12">su<seg phoneme="i" type="vs" value="1" rule="491" place="11" mp="M">i</seg>v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="16.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="16.3">m<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>r</w> <w n="16.4" punct="vg:6">j<seg phoneme="œ" type="vs" value="1" rule="407" place="6" punct="vg" caesura="1">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="16.6">m<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="9">i</seg>r</w> <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="11">e</seg>c</w> <w n="16.8" punct="pe:12">t<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pe">oi</seg></w> !</l>
					</lg>
				</div></body></text></TEI>