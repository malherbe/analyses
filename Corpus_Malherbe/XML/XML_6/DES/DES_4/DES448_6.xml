<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES448" modus="cm" lm_max="10" metProfile="4+6">
					<head type="main">INVITATION À LA VALSE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r</w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="1.4" punct="vg:4">br<seg phoneme="y" type="vs" value="1" rule="445" place="3" mp="M">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="1.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ls<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="1.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="1.9" punct="vg:10">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rcl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="2.3">fu<seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="2.5">s</w>’<w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="Lc">à</seg></w>-<w n="2.8" punct="pv:10">b<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pv">a</seg>s</w> ;</l>
						<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="3.2" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="4" punct="vg" caesura="1">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="3.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="3.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="3.6" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="4.3" punct="dp:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="dp" caesura="1">en</seg>d</w> :<caesura></caesura> <w n="4.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="4.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M/mp">a</seg>ls<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem/mp">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="8" mp="Lp">ez</seg></w>-<w n="4.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s</w> <w n="4.7" punct="pi:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pi">a</seg>s</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="10" met="4+6"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="C">ou</seg>s</w> <w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="6">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>s</w> <w n="5.5" punct="vg:10"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" mp="M">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="6" num="2.2" lm="10" met="4+6"><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="6.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>rds</w><caesura></caesura> <w n="6.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="6.5">b<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="7">oin</seg></w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="6.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="9" mp="C">o</seg>s</w> <w n="6.8" punct="pt:10">y<seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pt">eu</seg>x</w>.</l>
						<l n="7" num="2.3" lm="10" met="4+6"><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="7.4">v<seg phoneme="o" type="vs" value="1" rule="415" place="5">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="7.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="7">ain</seg></w> <w n="7.6" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="8" num="2.4" lm="10" met="4+6"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="8.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3" mp="M">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342" place="4" caesura="1">à</seg></w><caesura></caesura> <w n="8.4">j<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="5">oin</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6" mp="P">à</seg></w> <w n="8.6">l</w>’<w n="8.7"><seg phoneme="e" type="vs" value="1" rule="353" place="7" mp="M">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="8">aim</seg></w> <w n="8.8" punct="pe:10">j<seg phoneme="wa" type="vs" value="1" rule="440" place="9" mp="M">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="10" punct="pe">eu</seg>x</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="10" met="4+6"><w n="9.1">L</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r</w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="9.4" punct="vg:4">br<seg phoneme="y" type="vs" value="1" rule="445" place="3" mp="M">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="9.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ls<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="9.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="9.9" punct="vg:10">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="10" num="3.2" lm="10" met="4+6"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rcl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="10.3">fu<seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.5">s</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="Lc">à</seg></w>-<w n="10.8" punct="pv:10">b<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pv">a</seg>s</w> ;</l>
						<l n="11" num="3.3" lm="10" met="4+6"><w n="11.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="11.2" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="4" punct="vg" caesura="1">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="11.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="11.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="11.6" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="12" num="3.4" lm="10" met="4+6"><w n="12.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="12.3" punct="dp:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="dp" caesura="1">en</seg>d</w> :<caesura></caesura> <w n="12.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="12.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M/mp">a</seg>ls<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem/mp">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="8" mp="Lp">ez</seg></w>-<w n="12.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s</w> <w n="12.7" punct="pi:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pi">a</seg>s</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="10" met="4+6"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="13.2">v<seg phoneme="o" type="vs" value="1" rule="438" place="3" mp="C">o</seg>s</w> <w n="13.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="13.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="13.6">g<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</w> <w n="13.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="13.8" punct="pv:10">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg>s</w> ;</l>
						<l n="14" num="4.2" lm="10" met="4+6"><w n="14.1" punct="vg:1">Fl<seg phoneme="œ" type="vs" value="1" rule="407" place="1" punct="vg">eu</seg>rs</w>, <w n="14.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="14.4" punct="vg:4">f<seg phoneme="ø" type="vs" value="1" rule="398" place="4" punct="vg" caesura="1">eu</seg></w>,<caesura></caesura> <w n="14.5">c</w>’<w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="14.7">tr<seg phoneme="o" type="vs" value="1" rule="433" place="6">o</seg>p</w> <w n="14.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="7" mp="P">ou</seg>r</w> <w n="14.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="14.10">r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10">on</seg></w></l>
						<l n="15" num="4.3" lm="10" met="4+6"><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="15.2">ch<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>ds</w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="4" caesura="1">um</seg>s</w><caesura></caesura> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="15.5">b<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="7">e</seg>ts</w> <w n="15.6" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>ph<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
						<l n="16" num="4.4" lm="10" met="4+6"><w n="16.1">Tr<seg phoneme="o" type="vs" value="1" rule="433" place="1">o</seg>p</w> <w n="16.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="16.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="16.4">c<seg phoneme="œ" type="vs" value="1" rule="249" place="4" caesura="1">œu</seg>r</w><caesura></caesura> <w n="16.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="16.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>nt</w> <w n="16.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="8">en</seg></w> <w n="16.8" punct="pt:10">p<seg phoneme="wa" type="vs" value="1" rule="420" place="9" mp="M">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" punct="pt">on</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="10" met="4+6"><w n="17.1">L</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r</w> <w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="17.4" punct="vg:4">br<seg phoneme="y" type="vs" value="1" rule="445" place="3" mp="M">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="17.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ls<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="17.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="17.9" punct="vg:10">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="18" num="5.2" lm="10" met="4+6"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="18.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rcl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="18.3">fu<seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="18.5">s</w>’<w n="18.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="18.7">l<seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="Lc">à</seg></w>-<w n="18.8" punct="pv:10">b<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pv">a</seg>s</w> ;</l>
						<l n="19" num="5.3" lm="10" met="4+6"><w n="19.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="19.2" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="4" punct="vg" caesura="1">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="19.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="19.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="19.6" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="20" num="5.4" lm="10" met="4+6"><w n="20.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="20.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="20.3" punct="dp:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="dp" caesura="1">en</seg>d</w> :<caesura></caesura> <w n="20.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="20.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M/mp">a</seg>ls<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem/mp">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="8" mp="Lp">ez</seg></w>-<w n="20.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s</w> <w n="20.7" punct="pi:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pi">a</seg>s</w> ?</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="10" met="4+6"><w n="21.1" punct="vg:2">V<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ls<seg phoneme="e" type="vs" value="1" rule="347" place="2" punct="vg">ez</seg></w>, <w n="21.2">pl<seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="4" caesura="1">ez</seg></w><caesura></caesura> <w n="21.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="21.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="21.5">t<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</w></l>
						<l n="22" num="6.2" lm="10" met="4+6"><w n="22.1">Pl<seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>nt</w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="22.3">s<seg phoneme="wa" type="vs" value="1" rule="420" place="4" caesura="1">oi</seg>r</w><caesura></caesura> <w n="22.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="22.5">l</w>’<w n="22.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r</w> <w n="22.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="22.9">d<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>x</w></l>
						<l n="23" num="6.3" lm="10" met="4+6"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="23.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.3"><seg phoneme="e" type="vs" value="1" rule="353" place="3" mp="M">e</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" caesura="1">o</seg>r</w><caesura></caesura> <w n="23.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="23.5">v<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="C">ou</seg>s</w> <w n="23.6">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="23.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="P">ou</seg>r</w> <w n="23.8" punct="pv:10"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg>s</w> ;</l>
						<l n="24" num="6.4" lm="10" met="4+6"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="24.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="C">eu</seg>r</w> <w n="24.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>r</w><caesura></caesura> <w n="24.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="24.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="24.6">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w> <w n="24.7">p<seg phoneme="u" type="vs" value="1" rule="425" place="9" mp="P">ou</seg>r</w> <w n="24.8" punct="pe:10">v<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pe">ou</seg>s</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1" lm="10" met="4+6"><w n="25.1">L</w>’<w n="25.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>r</w> <w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="25.4" punct="vg:4">br<seg phoneme="y" type="vs" value="1" rule="445" place="3" mp="M">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="25.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="25.6">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ls<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="25.7">t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="25.9" punct="vg:10">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="26" num="7.2" lm="10" met="4+6"><w n="26.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="26.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rcl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="26.3">fu<seg phoneme="i" type="vs" value="1" rule="491" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="26.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="26.5">s</w>’<w n="26.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M">a</seg>gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>t</w> <w n="26.7">l<seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="Lc">à</seg></w>-<w n="26.8" punct="pv:10">b<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pv">a</seg>s</w> ;</l>
						<l n="27" num="7.3" lm="10" met="4+6"><w n="27.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" punct="vg">on</seg>s</w>, <w n="27.2" punct="vg:4">m<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341" place="4" punct="vg" caesura="1">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="27.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="27.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="Fc">e</seg></w> <w n="27.6" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
						<l n="28" num="7.4" lm="10" met="4+6"><w n="28.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="28.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="C">ou</seg>s</w> <w n="28.3" punct="dp:4"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" punct="dp" caesura="1">en</seg>d</w> :<caesura></caesura> <w n="28.4">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="28.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="M/mp">a</seg>ls<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem/mp">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="8" mp="Lp">ez</seg></w>-<w n="28.6">v<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>s</w> <w n="28.7" punct="pi:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pi">a</seg>s</w> ?</l>
					</lg>
				</div></body></text></TEI>