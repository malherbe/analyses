<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES449" modus="sm" lm_max="7" metProfile="7">
					<head type="main">L’ANGE ET LA COQUETTE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="7" met="7"><w n="1.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="1.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="1.4">l<seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="2" num="1.2" lm="7" met="7"><w n="2.1">S<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t</w> <w n="2.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="2.5" punct="vg:7">s<seg phoneme="wa" type="vs" value="1" rule="420" place="7" punct="vg">oi</seg>r</w>,</l>
						<l n="3" num="1.3" lm="7" met="7"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="3.2" punct="vg:2">s<seg phoneme="œ" type="vs" value="1" rule="407" place="2" punct="vg">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="3.5" punct="vg:7">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="4" num="1.4" lm="7" met="7"><w n="4.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">f<seg phoneme="a" type="vs" value="1" rule="193" place="3">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="5">en</seg>t</w> <w n="4.4">s</w>’<w n="4.5" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257" place="7" punct="pt">eoi</seg>r</w>.</l>
						<l n="5" num="1.5" lm="7" met="7"><w n="5.1" punct="vg:3">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="5.2">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="4">ein</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="5.4" punct="pt:7">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>p<seg phoneme="ø" type="vs" value="1" rule="403" place="7">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
						<l n="6" num="1.6" lm="7" met="7"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w>-<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>r</w> <w n="6.5" punct="pi:7">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" punct="pi">e</seg>rt</w> ?</l>
						<l n="7" num="1.7" lm="7" met="7"><w n="7.1" punct="pt:1">Ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="1" punct="pt">en</seg></w>. <w n="7.2">C<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="7.3">f<seg phoneme="a" type="vs" value="1" rule="193" place="4">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="7.5" punct="pt:7">h<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403" place="7">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg></w>.</l>
						<l n="8" num="1.8" lm="7" met="7"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.4">p<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="8.6">l</w>’<w n="8.7" punct="pt:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" punct="pt">e</seg>r</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1" lm="7" met="7"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="9.6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="10" num="2.2" lm="7" met="7"><w n="10.1">V<seg phoneme="ɛ" type="vs" value="1" rule="382" place="1">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="10.5" punct="vg:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="vg">on</seg>s</w>,</l>
						<l n="11" num="2.3" lm="7" met="7"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">c</w>’<w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l</w> <w n="11.6">qu</w>’<w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="12" num="2.4" lm="7" met="7"><w n="12.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="12.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="12.6" punct="dp:7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" punct="dp">on</seg>s</w> :</l>
						<l n="13" num="2.5" lm="7" met="7">— « <w n="13.1">N</w>’<w n="13.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="13.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="13.4">v<seg phoneme="o" type="vs" value="1" rule="438" place="5">o</seg>s</w> <w n="13.5" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</w>.</l>
						<l n="14" num="2.6" lm="7" met="7"><w n="14.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w>-<w n="14.2" punct="pt:2"><seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="pt in">i</seg>l</w>. « <w n="14.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="14.4">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4">e</seg>t</w> <w n="14.5"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="14.6" punct="vg:7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" punct="vg">e</seg>rt</w>,</l>
						<l n="15" num="2.7" lm="7" met="7"><w n="15.1">S</w>’<w n="15.2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>l</w> <w n="15.3">n</w>’<w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="15.5">m<seg phoneme="u" type="vs" value="1" rule="428" place="3">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="15.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="6">o</seg>s</w> <w n="15.8" punct="vg:7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</w>,</l>
						<l n="16" num="2.8" lm="7" met="7"><w n="16.1">N<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">s<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="16.5">l</w>’<w n="16.6" punct="pt:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" punct="pt">e</seg>r</w>. »</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1" lm="7" met="7">— « <w n="17.1" punct="vg:1">Qu<seg phoneme="wa" type="vs" value="1" rule="281" place="1" punct="vg">oi</seg></w>, <w n="17.2">n</w>’<w n="17.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w>-<w n="17.4">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="3">e</seg></w> <w n="17.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="17.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="17.7" punct="pi:7">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pi">e</seg></w> ? »</l>
						<l n="18" num="3.2" lm="7" met="7"><w n="18.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w>-<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="18.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="18.5">d</w>’<w n="18.6" punct="pt:7"><seg phoneme="e" type="vs" value="1" rule="353" place="6">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="pt">oi</seg></w>.</l>
						<l n="19" num="3.3" lm="7" met="7">« <w n="19.1" punct="pe:1"><seg phoneme="o" type="vs" value="1" rule="444" place="1" punct="pe">O</seg>h</w> ! <w n="19.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="19.5" punct="vg:7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="20" num="3.4" lm="7" met="7"><w n="20.1">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="20.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="20.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w>-<w n="20.4" punct="pe:7">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="pe">oi</seg></w> !</l>
						<l n="21" num="3.5" lm="7" met="7"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="21.2" punct="vg:2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2" punct="vg">en</seg>s</w>, <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="21.4">nu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>t</w> <w n="21.5"><seg phoneme="u" type="vs" value="1" rule="426" place="5">où</seg></w> <w n="21.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="21.7" punct="vg:7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="22" num="3.6" lm="7" met="7"><w n="22.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="22.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="22.3">c<seg phoneme="œ" type="vs" value="1" rule="249" place="3">œu</seg>r</w> <w n="22.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="22.5">p<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>ds</w> <w n="22.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="22.7" punct="pt:7">f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" punct="pt">e</seg>r</w>. »</l>
						<l n="23" num="3.7" lm="7" met="7">— « <w n="23.1" punct="vg:1">F<seg phoneme="a" type="vs" value="1" rule="193" place="1" punct="vg">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="23.2"><seg phoneme="o" type="vs" value="1" rule="415" place="2">ô</seg></w> <w n="23.3" punct="pe:4">f<seg phoneme="a" type="vs" value="1" rule="193" place="3">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pe">e</seg></w> ! » <w n="23.4">r<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>d</w> <w n="23.5">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="24" num="3.8" lm="7" met="7">« <w n="24.1">C</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="24.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>c</w> <w n="24.4">l<seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="24.5">qu</w>’<w n="24.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="24.7">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.8" punct="pt:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" punct="pt">e</seg>r</w>. »</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1" lm="7" met="7">« <w n="25.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="25.2">pu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>squ</w>’<w n="25.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="25.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="25.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="25.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="25.7">cr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="26" num="4.2" lm="7" met="7"><w n="26.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">n<seg phoneme="u" type="vs" value="1" rule="d-2" place="2">ou</seg><seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="26.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="26.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="26.5" punct="pv:7">n<seg phoneme="ø" type="vs" value="1" rule="248" place="7" punct="pv">œu</seg>ds</w> ;</l>
						<l n="27" num="4.3" lm="7" met="7"><w n="27.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ</w>’<w n="27.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ls</w> <w n="27.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="27.4">d</w>’<w n="27.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="27.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="7">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="28" num="4.4" lm="7" met="7"><w n="28.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="28.2">s</w>’<w n="28.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="384" place="3">ei</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>nt</w> <w n="28.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="28.5">d<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>x</w> <w n="28.6" punct="pt:7">y<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pt">eu</seg>x</w>.</l>
						<l n="29" num="4.5" lm="7" met="7"><w n="29.1">F<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>t</w>-<w n="29.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="29.3" punct="vg:4">h<seg phoneme="a" type="vs" value="1" rule="343" place="3">a</seg><seg phoneme="i" type="vs" value="1" rule="477" place="4" punct="vg">ï</seg>r</w>, <w n="29.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="29.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="29.6" punct="vg:7">pl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="30" num="4.6" lm="7" met="7"><w n="30.1">L</w>’<w n="30.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="30.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg></w> <w n="30.4">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="30.5"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="30.6" punct="pi:7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" punct="pi">e</seg>rt</w> ? »</l>
						<l n="31" num="4.7" lm="7" met="7">— « <w n="31.1" punct="vg:1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" punct="vg">on</seg></w>, » <w n="31.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w> <w n="31.3">l</w>’<w n="31.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="31.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="31.6" punct="vg:7">c<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
						<l n="32" num="4.8" lm="7" met="7">« <w n="32.1">L</w>’<w n="32.2"><seg phoneme="a" type="vs" value="1" rule="341" place="1">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="32.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="32.4">n</w>’<w n="32.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="32.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="32.7">d</w>’<w n="32.8" punct="pt:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" punct="pt">e</seg>r</w>. »</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1" lm="7" met="7">— « <w n="33.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="33.2" punct="vg:2">m<seg phoneme="wa" type="vs" value="1" rule="423" place="2" punct="vg">oi</seg></w>, <w n="33.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>r</w> <w n="33.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="33.5">d</w>’<w n="33.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="5">un</seg></w> <w n="33.7">m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="34" num="5.2" lm="7" met="7"><w n="34.1">J</w>’<w n="34.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="34.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="34.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>s</w> <w n="34.5" punct="pv:7">r<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="7" punct="pv">eau</seg>x</w> ;</l>
						<l n="35" num="5.3" lm="7" met="7"><w n="35.1">M<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="35.2">f<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>t</w> <w n="35.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="35.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="36" num="5.4" lm="7" met="7"><w n="36.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="36.2">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="36.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="36.4" punct="pt:7"><seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="7" punct="pt">eau</seg>x</w>.</l>
						<l n="37" num="5.5" lm="7" met="7"><w n="37.1">M</w>’<w n="37.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="37.3" punct="pi:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="pi ti in">ou</seg>s</w> ? » — « <w n="37.4">P<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="37.5" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>, »</l>
						<l n="38" num="5.6" lm="7" met="7"><w n="38.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>t</w> <w n="38.2">l</w>’<w n="38.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.4"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="38.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="38.6" punct="pv:7">d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" punct="pv">e</seg>rt</w> ;</l>
						<l n="39" num="5.7" lm="7" met="7">« <w n="39.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="39.2">m<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="39.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="39.4">j</w>’<w n="39.5"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
						<l n="40" num="5.8" lm="7" met="7"><w n="40.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="40.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="40.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="40.4">p<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="40.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="40.6">l</w>’<w n="40.7" punct="pt:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" punct="pt">e</seg>r</w>. »</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1" lm="7" met="7">— « <w n="41.1" punct="ps:1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="ps">ai</seg>s</w>… <w n="41.2">j</w>’<w n="41.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="41.4">br<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="41.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="41.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="41.7" punct="vg:7">ch<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</w>,</l>
						<l n="42" num="6.2" lm="7" met="7"><w n="42.1">J</w>’<w n="42.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="42.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="42.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="42.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="42.6" punct="vg:7">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" punct="vg">en</seg>ts</w>,</l>
						<l n="43" num="6.3" lm="7" met="7"><w n="43.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t</w> <w n="43.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="43.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="43.4">f<seg phoneme="a" type="vs" value="1" rule="193" place="4">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="43.5">tr<seg phoneme="o" type="vs" value="1" rule="433" place="6">o</seg>p</w> <w n="43.6">v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg>s</w></l>
						<l n="44" num="6.4" lm="7" met="7"><w n="44.1">V<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="44.2">d</w>’<w n="44.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>x</w> <w n="44.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="44.5">d</w>’<w n="44.6" punct="pe:7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" punct="pe">an</seg>ts</w> !</l>
						<l n="45" num="6.5" lm="7" met="7"><w n="45.1">L<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>rs</w> <w n="45.2">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="45.3">c<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>br<seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="45.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="45.5" punct="vg:7">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</w>,</l>
						<l n="46" num="6.6" lm="7" met="7"><w n="46.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="46.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="46.3">d</w>’<w n="46.4"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>r</w> <w n="46.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="46.6">f<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>t</w> <w n="46.7" punct="pe:7"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" punct="pe ps">e</seg>rt</w> !…»</l>
						<l n="47" num="6.7" lm="7" met="7">— « <w n="47.1" punct="pe:1"><seg phoneme="e" type="vs" value="1" rule="133" place="1" punct="pe">E</seg>h</w> ! <w n="47.2" punct="vg:2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2" punct="vg">en</seg></w>, <w n="47.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="47.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="47.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>rs</w> <w n="47.6" punct="pt:7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pt">e</seg>s</w>.</l>
						<l n="48" num="6.8" lm="7" met="7"><w n="48.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="48.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w> <w n="48.3">p<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="48.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="48.5">l</w>’<w n="48.6" punct="pt:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64" place="7" punct="pt">e</seg>r</w>. »</l>
					</lg>
				</div></body></text></TEI>