<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AMOUR</head><div type="poem" key="DES359" modus="sm" lm_max="8" metProfile="8">
					<head type="main">L’ENTREVUE AU RUISSEAU</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg></w> <w n="1.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="1.4" punct="vg:4">s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="1.6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8">en</seg></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="2.2">l<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="2.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="2.5" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>s</w>, <w n="2.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="2.7">n</w>’<w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="2.9" punct="pt:8">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pt">en</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1" lm="8" met="8"><w n="3.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="3.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="3.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="3.5">c<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="7">ein</seg>t<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="4" num="2.2" lm="8" met="8"><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="5">o</seg>r</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="4.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="4.6" punct="pt:8">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pt">eu</seg>rs</w>.</l>
						<l n="5" num="2.3" lm="8" met="8"><w n="5.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ds</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="4">um</seg>s</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="5.6" punct="vg:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="vg">eu</seg>rs</w>,</l>
						<l n="6" num="2.4" lm="8" met="8"><w n="6.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>ds</w> <w n="6.2" punct="ps:2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="ps">ou</seg>t</w>… <w n="6.3">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="6.4">m</w>’<w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="4">en</seg></w> <w n="6.6">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="6.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="6.8" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1" lm="8" met="8"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg></w> <w n="7.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="7.4" punct="vg:4">s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="7.6" punct="dp:8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" punct="dp">en</seg></w> :</l>
						<l n="8" num="3.2" lm="8" met="8"><w n="8.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="8.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="8.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="8.5" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>s</w>, <w n="8.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="8.7">n</w>’<w n="8.8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="8.9" punct="pt:8">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pt">en</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="9" num="4.1" lm="8" met="8"><w n="9.1">S<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w>-<w n="9.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="9.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="4">oi</seg></w> <w n="9.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.5">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6">en</seg>s</w> <w n="9.6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="7">oi</seg></w>-<w n="9.7">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="10" num="4.2" lm="8" met="8"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="2">er</seg></w> <w n="10.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="10.3">r<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg></w> <w n="10.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="10.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="10.6" punct="pi:8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="8" punct="pi">ein</seg></w> ?</l>
						<l n="11" num="4.3" lm="8" met="8"><w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="11.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="11.6">d</w>’<w n="11.7"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="11.8" punct="pt:8">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rc<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="8" punct="pt">in</seg></w>.</l>
						<l n="12" num="4.4" lm="8" met="8"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="12.4">v<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>t</w> <w n="12.5">d<seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="12.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="12.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="12.8" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="8">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1" lm="8" met="8"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="o" type="vs" value="1" rule="315" place="1">eau</seg></w> <w n="13.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="13.4" punct="vg:4">s<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="13.6" punct="dp:8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" punct="dp">en</seg></w> :</l>
						<l n="14" num="5.2" lm="8" met="8"><w n="14.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="14.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="14.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="14.5" punct="vg:5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" punct="vg">a</seg>s</w>, <w n="14.6">t<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="14.7">n</w>’<w n="14.8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>s</w> <w n="14.9" punct="pt:8">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pt">en</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="15" num="6.1" lm="8" met="8"><w n="15.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="2" punct="vg">eu</seg></w>, <w n="15.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="15.3">r<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="342" place="7">à</seg></w> <w n="15.6" punct="vg:8">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="16" num="6.2" lm="8" met="8"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="16.2">n</w>’<w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="16.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>s</w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="16.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">em</seg>ps</w> <w n="16.7">d</w>’<w n="16.8" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pv">er</seg></w> ;</l>
						<l n="17" num="6.3" lm="8" met="8"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="17.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d</w> <w n="17.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="17.4">n</w>’<w n="17.5"><seg phoneme="o" type="vs" value="1" rule="444" place="4">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="17.6">m</w>’<w n="17.7" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>,</l>
						<l n="18" num="6.4" lm="8" met="8"><w n="18.1">N</w>’<w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w>-<w n="18.3">c<seg phoneme="ə" type="ef" value="1" rule="e-13" place="2">e</seg></w> <w n="18.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>c</w> <w n="18.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="18.6">t<seg phoneme="wa" type="vs" value="1" rule="423" place="5">oi</seg></w> <w n="18.7">qu</w>’<w n="18.8"><seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>l</w> <w n="18.9">f<seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>t</w> <w n="18.10" punct="pi:8">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="8">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pi">e</seg></w> ?</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1" lm="8" met="8"><w n="19.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">j</w>’<w n="19.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="19.5" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>t</w>, <w n="19.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6">en</seg>s</w>-<w n="19.7">l<seg phoneme="ə" type="em" value="1" rule="e-6" place="7">e</seg></w> <w n="19.8" punct="dp:8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" punct="dp">en</seg></w> :</l>
						<l n="20" num="7.2" lm="8" met="8"><w n="20.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="20.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rd</w>’<w n="20.3" punct="vg:4">hu<seg phoneme="i" type="vs" value="1" rule="491" place="4" punct="vg">i</seg></w>, <w n="20.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="20.5">n</w>’<w n="20.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="20.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="20.8" punct="pe:8">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="8" punct="pe">en</seg></w> !</l>
					</lg>
				</div></body></text></TEI>