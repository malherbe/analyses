<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AMOUR</head><div type="poem" key="DES372" modus="sm" lm_max="8" metProfile="8">
					<head type="main">SIMPLE HISTOIRE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="1.2">m</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="1.4">c<seg phoneme="o" type="vs" value="1" rule="435" place="3">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="457" place="4">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg></w> <w n="1.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">em</seg>ps</w> <w n="1.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="1.8" punct="vg:8">r<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="2.3">c<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5">e</seg>s</w> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t</w> <w n="2.5" punct="pv:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>cl<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="3.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rs</w> <w n="3.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="5">ein</seg>s</w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6">s<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8">e</seg>il</w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg>t</w> <w n="4.2">br<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="4.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>r</w> <w n="4.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="4.5">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="6">ein</seg>t</w> <w n="4.6" punct="pt:8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="8" punct="pt">e</seg>il</w>.</l>
						<l n="5" num="1.5" lm="8" met="8"><w n="5.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="2">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="5.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="5.4" punct="pt:8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="6" num="1.6" lm="8" met="8"><w n="6.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="6.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="6.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5" punct="vg:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="8">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="7" num="1.7" lm="8" met="8"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="7.3">t</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="7.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="7.7" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></w>,</l>
						<l n="8" num="1.8" lm="8" met="8"><w n="8.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="8.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="8.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="8.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l</w> <w n="8.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1" lm="8" met="8"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1">À</seg></w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="5">en</seg>t</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="9.5">n<seg phoneme="o" type="vs" value="1" rule="438" place="7">o</seg>s</w> <w n="9.6" punct="vg:8"><seg phoneme="a" type="vs" value="1" rule="341" place="8">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="10" num="2.2" lm="8" met="8"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="10.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="10.7" punct="pv:8">fl<seg phoneme="a" type="vs" value="1" rule="341" place="8">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
						<l n="11" num="2.3" lm="8" met="8"><w n="11.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="11.2" punct="vg:3">f<seg phoneme="œ" type="vs" value="1" rule="406" place="3" punct="vg">eu</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="11.4">bru<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="11.6">r<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg>x</w></l>
						<l n="12" num="2.4" lm="8" met="8"><w n="12.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="12.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="12.4">h<seg phoneme="i" type="vs" value="1" rule="493" place="5">y</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="12.5">d</w>’<w n="12.6" punct="pt:8"><seg phoneme="wa" type="vs" value="1" rule="420" place="7">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="8" punct="pt">eau</seg>x</w>.</l>
						<l n="13" num="2.5" lm="8" met="8"><w n="13.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="13.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="13.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="13.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">em</seg>ps</w> <w n="13.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="13.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="13.7">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="14" num="2.6" lm="8" met="8"><w n="14.1">S<seg phoneme="o" type="vs" value="1" rule="435" place="1">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="14.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="14.4">h<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="y" type="vs" value="1" rule="453" place="6">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7">e</seg></w> <w n="14.6" punct="vg:8">f<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="15" num="2.7" lm="8" met="8"><w n="15.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="15.2">n</w>’<w n="15.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ti<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>s</w> <w n="15.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ls</w> <w n="15.5">qu</w>’<w n="15.6"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="15.7" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></w>,</l>
						<l n="16" num="2.8" lm="8" met="8"><w n="16.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="16.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="16.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="16.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l</w> <w n="16.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg></w> !</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1" lm="8" met="8"><w n="17.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="17.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="17.3">t</w>’<w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="17.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="17.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="17.8" punct="pt:8">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
						<l n="18" num="3.2" lm="8" met="8"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="18.3">v<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="18.4"><seg phoneme="y" type="vs" value="1" rule="391" place="4">eu</seg>t</w> <w n="18.5"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.7" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="8">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="19" num="3.3" lm="8" met="8"><w n="19.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="19.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg></w> <w n="19.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt</w> <w n="19.5" punct="vg:8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="vg">an</seg>t</w>,</l>
						<l n="20" num="3.4" lm="8" met="8"><w n="20.1">L</w>’<w n="20.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="20.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="20.5" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" punct="pt">an</seg>t</w>.</l>
						<l n="21" num="3.5" lm="8" met="8"><w n="21.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="21.2">r<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="21.3">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="21.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="21.5" punct="pv:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="22" num="3.6" lm="8" met="8"><w n="22.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="22.2" punct="vg:3">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="3" punct="vg">à</seg></w>, <w n="22.3">pl<seg phoneme="ø" type="vs" value="1" rule="405" place="4">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="22.4">l</w>’<w n="22.5"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.6" punct="vg:8"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="23" num="3.7" lm="8" met="8"><w n="23.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">n</w>’<w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="2">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="23.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>s</w> <w n="23.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg></w> <w n="23.6">qu</w>’<w n="23.7"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="23.8" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></w>,</l>
						<l n="24" num="3.8" lm="8" met="8"><w n="24.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="24.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="24.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="24.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="24.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l</w> <w n="24.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1" lm="8" met="8"><w n="25.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="25.2" punct="vg:2">j<seg phoneme="u" type="vs" value="1" rule="425" place="2" punct="vg">ou</seg>r</w>, <w n="25.3">l</w>’<w n="25.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.5"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="26" num="4.2" lm="8" met="8"><w n="26.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>ss<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="26.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="26.3">v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>ss<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="26.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="6">e</seg>rs</w> <w n="26.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="26.6" punct="dp:8">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
						<l n="27" num="4.3" lm="8" met="8"><w n="27.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="27.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="27.3">r<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467" place="4">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="27.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r</w> <w n="27.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg></w> <w n="27.6" punct="ps:8">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8" punct="ps">œu</seg>r</w>…</l>
						<l n="28" num="4.4" lm="8" met="8"><w n="28.1" punct="vg:1">J<seg phoneme="œ" type="vs" value="1" rule="407" place="1" punct="vg">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="28.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="28.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="28.4">m<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rt</w> <w n="28.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="28.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="28.7" punct="pe:8">b<seg phoneme="o" type="vs" value="1" rule="444" place="7">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407" place="8" punct="pe">eu</seg>r</w> !</l>
						<l n="29" num="4.5" lm="8" met="8"><w n="29.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="29.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="29.3">gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="29.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="29.6" punct="ps:8"><seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg>s</w>…</l>
						<l n="30" num="4.6" lm="8" met="8"><w n="30.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="30.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="30.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="30.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg>s</w> <w n="30.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="30.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="30.7" punct="pt:8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
						<l n="31" num="4.7" lm="8" met="8"><w n="31.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="31.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="31.3">t</w>’<w n="31.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="31.5">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="31.6">qu</w>’<w n="31.7"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="31.8" punct="vg:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg></w>,</l>
						<l n="32" num="4.8" lm="8" met="8"><w n="32.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="32.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.3" punct="vg:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg></w>, <w n="32.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="32.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l</w> <w n="32.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg></w> !</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1" lm="8" met="8"><w n="33.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="33.2" punct="vg:2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2" punct="vg">a</seg>rd</w>, <w n="33.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="33.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="33.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="33.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="34" num="5.2" lm="8" met="8"><w n="34.1" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg>cc<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="3" punct="vg">u</seg>t</w>, <w n="34.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>t</w> <w n="34.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.4"><seg phoneme="o" type="vs" value="1" rule="318" place="6">au</seg></w> <w n="34.5" punct="vg:8">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="35" num="5.3" lm="8" met="8"><w n="35.1" punct="dp:2">D<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="dp in">an</seg>t</w> : « <w n="35.2">V<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="4">ez</seg></w>-<w n="35.3">v<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="35.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="35.5" punct="pi:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pi">oi</seg>r</w> ?</l>
						<l n="36" num="5.4" lm="8" met="8"><w n="36.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="2">ain</seg></w> <w n="36.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="36.3">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="36.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6">an</seg>s</w> <w n="36.5" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="8" punct="pt">oi</seg>r</w>.</l>
						<l n="37" num="5.5" lm="8" met="8"><w n="37.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="37.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="37.3">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="4">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="5">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="37.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>t</w> <w n="37.5" punct="vg:8">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="38" num="5.6" lm="8" met="8"><w n="38.1">V<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w> <w n="38.2" punct="pv:4">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="pv">e</seg></w> ; <w n="38.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="5">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="38.4">v<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>s</w> <w n="38.5" punct="ps:8"><seg phoneme="ɛ" type="vs" value="1" rule="411" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg>s</w>… »</l>
						<l n="39" num="5.7" lm="8" met="8"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="39.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="39.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4">in</seg>s</w> <w n="39.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="39.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="39.6" punct="pt:8">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg></w>.</l>
						<l n="40" num="5.8" lm="8" met="8"><w n="40.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="40.2">p<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="40.3" punct="pe:4"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="pe">i</seg></w> ! <w n="40.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="40.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>l</w> <w n="40.6" punct="pe:8"><seg phoneme="a" type="vs" value="1" rule="341" place="7">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg></w> !</l>
					</lg>
				</div></body></text></TEI>