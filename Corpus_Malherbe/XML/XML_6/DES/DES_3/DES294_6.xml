<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES294" modus="cm" lm_max="10" metProfile="4+6">
				<head type="main">MOI JE LE SAIS</head>
				<opener>
					<salute>À mademoiselle Louise Grombach.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="1.3" punct="vg:4">s<seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg" caesura="1">ez</seg></w>,<caesura></caesura> <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="1.5">v<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="1.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="10">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</w></l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="2.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4" caesura="1">oin</seg></w><caesura></caesura> <w n="2.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>s</w> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="2.7" punct="dp:10">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="dp">eu</seg>rs</w> :</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="3.2">r<seg phoneme="o" type="vs" value="1" rule="435" place="2" mp="M">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" caesura="1">o</seg>ls</w><caesura></caesura> <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg></w> <w n="3.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7" mp="F">e</seg>nt</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="3.6">l<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="C">eu</seg>rs</w> <w n="3.7" punct="vg:10">c<seg phoneme="i" type="vs" value="1" rule="467" place="10">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="4.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3" mp="Fm">e</seg>nt</w>-<w n="4.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>ls</w><caesura></caesura> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="4.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" mp="M">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="4.8" punct="pi:10">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pi">eu</seg>rs</w> ?</l>
					<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3" punct="vg:4">s<seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg" caesura="1">ez</seg></w>,<caesura></caesura> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="5.5">v<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="5.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467" place="10">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg>s</w></l>
					<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>s</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="6.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4" caesura="1">oin</seg></w><caesura></caesura> <w n="6.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>s</w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="i" type="vs" value="1" rule="467" place="6" mp="M">i</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="M">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>s</w> <w n="6.7" punct="pt:10">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pt">eu</seg>rs</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1" lm="10" met="4+6"><w n="7.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="7.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="3" mp="M">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4" caesura="1">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="7.6">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="7" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="8">y</seg>s</w> <w n="7.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="7.8" punct="pv:10">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg>s</w> ;</l>
					<l n="8" num="2.2" lm="10" met="4+6"><w n="8.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="8.4" punct="dp:4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="dp" caesura="1">ai</seg>s</w> :<caesura></caesura> <w n="8.5">j</w>’<w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="8.7" punct="pt:6">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6" punct="pt">en</seg>s</w>. <w n="8.8">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.9">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.10" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10" punct="vg">o</seg>r</w>,</l>
					<l n="9" num="2.3" lm="10" met="4+6"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="9.3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" caesura="1">an</seg>t</w><caesura></caesura> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="9.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="9.6" punct="vg:7">f<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="vg">eu</seg>x</w>, <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="9.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="9.9" punct="pv:10">ch<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg>s</w> ;</l>
					<l n="10" num="2.4" lm="10" met="4+6"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="10.2">c<seg phoneme="œ" type="vs" value="1" rule="249" place="2">œu</seg>r</w> <w n="10.3">br<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="10.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="8">er</seg></w> <w n="10.7" punct="pe:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10" punct="pe">o</seg>rd</w> !</l>
					<l n="11" num="2.5" lm="10" met="4+6"><w n="11.1" punct="vg:1">Ou<seg phoneme="i" type="vs" value="1" rule="491" place="1" punct="vg">i</seg></w>, <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="11.3">j<seg phoneme="œ" type="vs" value="1" rule="407" place="3" mp="M">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352" place="4" caesura="1">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="11.6">p<seg phoneme="ɛ" type="vs" value="1" rule="339" place="7" mp="M">a</seg><seg phoneme="i" type="vs" value="1" rule="321" place="8">y</seg>s</w> <w n="11.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="9" mp="C">e</seg>s</w> <w n="11.8" punct="pt:10">l<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg>s</w>.</l>
					<l n="12" num="2.6" lm="10" met="4+6"><w n="12.1">M<seg phoneme="wa" type="vs" value="1" rule="423" place="1">oi</seg></w> <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="12.4" punct="dp:4">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="dp" caesura="1">ai</seg>s</w> :<caesura></caesura> <w n="12.5">j</w>’<w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="5">en</seg></w> <w n="12.7" punct="pt:6">vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6" punct="pt">en</seg>s</w>. <w n="12.8">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="12.9">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="8">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.10" punct="pe:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10" punct="pe">o</seg>r</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1" lm="10" met="4+6"><w n="13.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="Lc">o</seg>rsqu</w>’<w n="13.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="13.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="13.4">d</w>’<w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="13.6" punct="vg:7">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="vg">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="13.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="13.8">s</w>’<w n="13.9" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="14" num="3.2" lm="10" met="4+6"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="14.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="14.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="14.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="14.6">v<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>t</w> <w n="14.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="14.9" punct="dp:10">j<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="dp">ou</seg>r</w> :</l>
					<l n="15" num="3.3" lm="10" met="4+6"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>ls</w> <w n="15.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>s</w><caesura></caesura> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="15.5">l</w>’<w n="15.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="15.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="15.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="15.9" punct="pt:10">t<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
					<l n="16" num="3.4" lm="10" met="4+6"><w n="16.1">D</w>’<w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="16.3">t<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="16.4">v<seg phoneme="wa" type="vs" value="1" rule="440" place="3" mp="M">o</seg>y<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="16.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>d</w> <w n="16.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>c</w> <w n="16.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="16.9" punct="pi:10">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pi">ou</seg>r</w> ?</l>
					<l n="17" num="3.5" lm="10" met="4+6"><w n="17.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="Lc">o</seg>rsqu</w>’<w n="17.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="17.3">f<seg phoneme="i" type="vs" value="1" rule="467" place="3" mp="M">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg>t</w><caesura></caesura> <w n="17.4">d</w>’<w n="17.5"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="F">e</seg></w> <w n="17.6" punct="vg:7">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="vg">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="17.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="17.8">s</w>’<w n="17.9" punct="pv:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="10">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="18" num="3.6" lm="10" met="4+6"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="342" place="1" mp="P">À</seg></w> <w n="18.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="Pem">e</seg></w> <w n="18.4">j<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>rs</w><caesura></caesura> <w n="18.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="18.6">v<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>t</w> <w n="18.7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.8"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="9" mp="C">un</seg></w> <w n="18.9" punct="pe:10">j<seg phoneme="u" type="vs" value="1" rule="425" place="10" punct="pe">ou</seg>r</w> !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1" lm="10" met="4+6"><w n="19.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="19.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="19.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="19.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="19.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="19.7" punct="vg:10">bl<seg phoneme="e" type="vs" value="1" rule="353" place="9" mp="M">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="20" num="4.2" lm="10" met="4+6"><w n="20.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="20.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="20.3" punct="dp:4">m<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp" caesura="1">i</seg>r</w> :<caesura></caesura> <w n="20.4" punct="vg:6">v<seg phoneme="wa" type="vs" value="1" rule="440" place="5" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg">ez</seg></w>, <w n="20.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="20.6">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="20.7">m<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rt</w> <w n="20.8" punct="pt:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>s</w>.</l>
					<l n="21" num="4.3" lm="10" met="4+6"><w n="21.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="21.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="21.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="4" caesura="1">en</seg>s</w><caesura></caesura> <w n="21.4">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg></w> <w n="21.5">gu<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="21.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="8" mp="C">e</seg>s</w> <w n="21.7" punct="vg:10">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rs<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="22" num="4.4" lm="10" met="4+6"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="22.3">d<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>ct<seg phoneme="a" type="vs" value="1" rule="341" place="4" caesura="1">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="22.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="22.5">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="6" mp="Mem">e</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="22.6">s<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="P">ou</seg>s</w> <w n="22.7">n<seg phoneme="o" type="vs" value="1" rule="438" place="9" mp="C">o</seg>s</w> <w n="22.8" punct="pt:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pt">a</seg>s</w>.</l>
					<l n="23" num="4.5" lm="10" met="4+6"><w n="23.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="23.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="23.3">b<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>s</w><caesura></caesura> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="Pem">e</seg></w> <w n="23.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6" mp="C">e</seg>s</w> <w n="23.6">m<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="23.7" punct="vg:10">bl<seg phoneme="e" type="vs" value="1" rule="353" place="9" mp="M">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg>s</w>,</l>
					<l n="24" num="4.6" lm="10" met="4+6"><w n="24.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">On</seg></w> <w n="24.2">cr<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="24.3" punct="dp:4">m<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="dp" caesura="1">i</seg>r</w> :<caesura></caesura> <w n="24.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="24.5" punct="vg:6">pl<seg phoneme="i" type="vs" value="1" rule="469" place="6" punct="vg">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="24.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="24.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="24.8">m<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>rt</w> <w n="24.9" punct="pe:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" punct="pe">a</seg>s</w> !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1" lm="10" met="4+6"><w n="25.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem/mp">e</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="25.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="25.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="25.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="25.5">d</w>’<w n="25.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="25.7" punct="vg:10">gl<seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>n<seg phoneme="ø" type="vs" value="1" rule="403" place="10">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="26" num="5.2" lm="10" met="4+6"><w n="26.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="26.2">s</w>’<w n="26.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3" mp="M">ê</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg></w><caesura></caesura> <w n="26.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="26.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="26.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="26.7" punct="pv:10">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10" punct="pv">ain</seg></w> ;</l>
					<l n="27" num="5.3" lm="10" met="4+6"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="27.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="27.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="3" mp="C">u</seg></w> <w n="27.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4" caesura="1">o</seg>rt</w><caesura></caesura> <w n="27.5">l</w>’<w n="27.6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="27.7" punct="vg:10">l<seg phoneme="y" type="vs" value="1" rule="453" place="8" mp="M">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467" place="9" mp="M">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="403" place="10">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="28" num="5.4" lm="10" met="4+6"><w n="28.1">V<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="28.2">m<seg phoneme="y" type="vs" value="1" rule="445" place="2" mp="M">û</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="28.3">mi<seg phoneme="ø" type="vs" value="1" rule="398" place="4" caesura="1">eu</seg>x</w><caesura></caesura> <w n="28.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="28.5"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>s</w> <w n="28.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="28.7" punct="vg:10">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="vg">in</seg></w>,</l>
					<l n="29" num="5.5" lm="10" met="4+6"><w n="29.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M/mp">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem/mp">e</seg>l<seg phoneme="e" type="vs" value="1" rule="347" place="3" mp="Lp">ez</seg></w>-<w n="29.2">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>s</w><caesura></caesura> <w n="29.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="29.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="29.5">d</w>’<w n="29.6"><seg phoneme="y" type="vs" value="1" rule="453" place="7">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="29.7" punct="vg:10">gl<seg phoneme="a" type="vs" value="1" rule="341" place="9" mp="M">a</seg>n<seg phoneme="ø" type="vs" value="1" rule="403" place="10">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="30" num="5.6" lm="10" met="4+6"><w n="30.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="30.2">s</w>’<w n="30.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3" mp="M">ê</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg></w><caesura></caesura> <w n="30.4">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="30.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6" mp="M">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347" place="7">er</seg></w> <w n="30.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="Fc">e</seg></w> <w n="30.7" punct="pe:10">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10" punct="pe">ain</seg></w> !</l>
				</lg>
			</div></body></text></TEI>