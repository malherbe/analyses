<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES327" modus="cm" lm_max="10" metProfile="5+5">
				<head type="main">LA PAROLE D’UN SOLDAT</head>
				<lg n="1">
					<l n="1" num="1.1" lm="10" met="5+5"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="1.2">vi<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="1.3" punct="vg:5">R<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="vg" caesura="1">e</seg>l</w>,<caesura></caesura> <w n="1.4">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="1.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="1.7" punct="vg:10">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="10" met="5+5"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="C">au</seg></w> <w n="2.3">S<seg phoneme="ɛ" type="vs" value="1" rule="384" place="4" mp="M">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="5" caesura="1">eu</seg>r</w><caesura></caesura> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" mp="C">on</seg></w> <w n="2.5">j<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="2.6" punct="vg:10">s<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10" punct="vg">e</seg>il</w>,</l>
					<l n="3" num="1.3" lm="10" met="5+5"><w n="3.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" mp="M">e</seg>rni<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>t</w><caesura></caesura> <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="3.6">gu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7" punct="vg:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="10">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="4" num="1.4" lm="10" met="5+5"><w n="4.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3">cr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="4.5">gl<seg phoneme="wa" type="vs" value="1" rule="420" place="5" caesura="1">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6" mp="M">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>t</w> <w n="4.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="4.8" punct="pt:10">s<seg phoneme="o" type="vs" value="1" rule="444" place="9" mp="M">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="10" punct="pt">e</seg>il</w>.</l>
					<l n="5" num="1.5" lm="10" met="5+5"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3" mp="M">in</seg>c<seg phoneme="e" type="vs" value="1" rule="353" place="4" mp="M">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" caesura="1">an</seg>t</w><caesura></caesura> <w n="5.4">d</w>’<w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="5.6">dr<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="8">eau</seg></w> <w n="5.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="5.8" punct="vg:10">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="6" num="1.6" lm="10" met="5+5"><w n="6.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>t</w> <w n="6.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="6.4">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5" caesura="1">in</seg></w><caesura></caesura> <w n="6.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" mp="P">an</seg>s</w> <w n="6.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="6.7">d<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>gts</w> <w n="6.8" punct="dp:10">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="dp">an</seg>s</w> :</l>
					<l n="7" num="1.7" lm="10" met="5+5">« <w n="7.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="7.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="7.3" punct="vg:5">S<seg phoneme="ɛ" type="vs" value="1" rule="384" place="4" mp="M">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407" place="5" punct="vg" caesura="1">eu</seg>r</w>,<caesura></caesura> <w n="7.4">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6">en</seg>t</w> <w n="7.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w> <w n="7.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="7.7" punct="pv:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="8" num="1.8" lm="10" met="5+5"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="8.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362" place="2">en</seg>s</w> <w n="8.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="8.4">l<seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="8.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="P">ou</seg>s</w> <w n="8.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="8.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="8.8" punct="pe:10">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pe">an</seg>cs</w> ! »</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1" lm="10" met="5+5"><w n="9.1" punct="pe:1">P<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1" punct="pe">ai</seg>x</w> ! <w n="9.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2" mp="M">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="9.3">l</w>’<w n="9.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4" mp="M">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="444" place="5" caesura="1">o</seg></w><caesura></caesura> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="9.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8" mp="Lc">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>-<w n="9.8" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M/mc">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="10" num="2.2" lm="10" met="5+5"><w n="10.1">Pr<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="M">o</seg>cl<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="C">e</seg></w> <w n="10.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="5" caesura="1">om</seg></w><caesura></caesura> <w n="10.4">d</w>’<w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="10.6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t</w> <w n="10.7" punct="pv:10">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9" mp="M">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="pv">eu</seg>r</w> ;</l>
					<l n="11" num="2.3" lm="10" met="5+5"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="11.3">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4" punct="vg:5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="11.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="C">u</seg></w> <w n="11.6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" mp="M">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>t</w> <w n="11.7" punct="vg:10"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="9" mp="M">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="12" num="2.4" lm="10" met="5+5"><w n="12.1">C<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="12.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3" mp="P">e</seg>rs</w> <w n="12.3">R<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" caesura="1">e</seg>l</w><caesura></caesura> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="6">en</seg></w> <w n="12.5">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>t</w> <w n="12.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="12.7" punct="dp:10">c<seg phoneme="œ" type="vs" value="1" rule="249" place="10" punct="dp">œu</seg>r</w> :</l>
					<l n="13" num="2.5" lm="10" met="5+5">« <w n="13.1" punct="vg:3"><seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="M">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="3" punct="vg">ez</seg></w>, <w n="13.2" punct="vg:5">R<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5" punct="vg" caesura="1">e</seg>l</w>,<caesura></caesura> <w n="13.3">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="C">e</seg></w> <w n="13.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>d</w> <w n="13.5">bru<seg phoneme="i" type="vs" value="1" rule="491" place="8">i</seg>t</w> <w n="13.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="13.7" punct="pv:10">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="14" num="2.6" lm="10" met="5+5"><w n="14.1">L<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M/mp">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="2">ez</seg></w>-<w n="14.2">l<seg phoneme="ə" type="ee" value="0" rule="e-7">e</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="4">e</seg>c</w> <w n="14.4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="5" caesura="1">oi</seg></w><caesura></caesura> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6" mp="Pem">e</seg></w> <w n="14.6">v<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="14.7">y<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w> <w n="14.8" punct="dp:10">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="dp">an</seg>s</w> :</l>
					<l n="15" num="2.7" lm="10" met="5+5"><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="15.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="Fc">e</seg></w> <w n="15.3">G<seg phoneme="i" type="vs" value="1" rule="468" place="4" mp="M">i</seg>lb<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" caesura="1">e</seg>rt</w><caesura></caesura> <w n="15.4">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6">en</seg>t</w> <w n="15.5">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="7">en</seg></w> <w n="15.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="15.7" punct="pv:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="16" num="2.8" lm="10" met="5+5"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="C">I</seg>l</w> <w n="16.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="16.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="16.4">l<seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="16.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="P">u</seg>r</w> <w n="16.6">v<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="16.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="16.8" punct="pe:10">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pe">an</seg>cs</w> ! »</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1" lm="10" met="5+5"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1" mp="C">Au</seg></w> <w n="17.2">m<seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>li<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="17.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="17.4">cr<seg phoneme="i" type="vs" value="1" rule="468" place="5" caesura="1">i</seg>s</w><caesura></caesura> <w n="17.5">d</w>’<w n="17.6"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="17.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>p</w> <w n="17.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="17.9" punct="vg:10">b<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="10">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="18" num="3.2" lm="10" met="5+5"><w n="18.1" punct="vg:2">G<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>lb<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2" punct="vg">e</seg>rt</w>, <w n="18.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="18.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="Lp">ou</seg>r</w>-<w n="18.4" punct="vg:5">l<seg phoneme="a" type="vs" value="1" rule="342" place="5" punct="vg" caesura="1">à</seg></w>,<caesura></caesura> <w n="18.5">s<seg phoneme="o" type="vs" value="1" rule="318" place="6" mp="M">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="18.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="18.7" punct="vg:10">dr<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="vg">eau</seg></w>,</l>
					<l n="19" num="3.3" lm="10" met="5+5"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2" mp="M">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="19.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409" place="5" caesura="1">é</seg></w><caesura></caesura> <w n="19.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="P">u</seg>r</w> <w n="19.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="7" mp="C">un</seg></w> <w n="19.6">p<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="19.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="19.8" punct="vg:10">p<seg phoneme="a" type="vs" value="1" rule="307" place="10">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="20" num="3.4" lm="10" met="5+5"><w n="20.1">D<seg phoneme="i" type="vs" value="1" rule="468" place="1" mp="M">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="20.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="20.3" punct="dp:5">m<seg phoneme="u" type="vs" value="1" rule="425" place="4" mp="M">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" punct="dp in" caesura="1">an</seg>t</w> :<caesura></caesura> « <w n="20.4">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="20.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7" mp="C">on</seg></w> <w n="20.6">s<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt</w> <w n="20.7"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="20.8" punct="pe:10">b<seg phoneme="o" type="vs" value="1" rule="315" place="10" punct="pe">eau</seg></w> !</l>
					<l n="21" num="3.5" lm="10" met="5+5"><w n="21.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="21.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2" mp="C">on</seg></w> <w n="21.3" punct="vg:3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="3" punct="vg">om</seg></w>, <w n="21.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="5" caesura="1">e</seg>il</w><caesura></caesura> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="342" place="6" mp="P">à</seg></w> <w n="21.6">l</w>’<w n="21.7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="21.8">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="21.9" punct="vg:10">v<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></w>,</l>
					<l n="22" num="3.6" lm="10" met="5+5"><w n="22.1">S</w>’<w n="22.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>t</w> <w n="22.3">gl<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="4" mp="M">i</seg><seg phoneme="ø" type="vs" value="1" rule="398" place="5" caesura="1">eu</seg>x</w><caesura></caesura> <w n="22.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="P">u</seg>r</w> <w n="22.5">d<seg phoneme="ø" type="vs" value="1" rule="398" place="7">eu</seg>x</w> <w n="22.6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="8">œu</seg>rs</w> <w n="22.7" punct="pt:10">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" punct="pt">an</seg>s</w>.</l>
					<l n="23" num="3.7" lm="10" met="5+5"><w n="23.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="23.2" punct="vg:2">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2" punct="vg">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="3" mp="M/mp">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347" place="4" mp="Lp">ez</seg></w>-<w n="23.4" punct="vg:5">m<seg phoneme="wa" type="vs" value="1" rule="423" place="5" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="23.5">j</w>’<w n="23.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg></w> <w n="23.7">t<seg phoneme="ə" type="em" value="1" rule="e-19" place="7" mp="Mem">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg></w> <w n="23.8" punct="dp:10">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="10">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="dp" mp="F">e</seg></w> :</l>
					<l n="24" num="3.8" lm="10" met="5+5"><w n="24.1">J</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="24.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="24.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3" mp="C">un</seg></w> <w n="24.5">l<seg phoneme="o" type="vs" value="1" rule="318" place="4" mp="M">au</seg>ri<seg phoneme="e" type="vs" value="1" rule="347" place="5" caesura="1">er</seg></w><caesura></caesura> <w n="24.6">s<seg phoneme="y" type="vs" value="1" rule="450" place="6" mp="P">u</seg>r</w> <w n="24.7">v<seg phoneme="o" type="vs" value="1" rule="438" place="7" mp="C">o</seg>s</w> <w n="24.8">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="8" mp="Mem">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398" place="9">eu</seg>x</w> <w n="24.9" punct="pe:10">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="pe">an</seg>cs</w> ! »</l>
				</lg>
			</div></body></text></TEI>