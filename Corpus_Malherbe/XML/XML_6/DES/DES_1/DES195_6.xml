<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>ROMANCES</head><div type="poem" key="DES195" modus="sm" lm_max="7" metProfile="7">
						<head type="main">LE CALVAIRE</head>
						<lg n="1">
							<l n="1" num="1.1" lm="7" met="7"><w n="1.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="1.3" punct="vg:4">v<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</w>, <w n="1.4" punct="vg:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">An</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
							<l n="2" num="1.2" lm="7" met="7"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="2.2">c<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>lv<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="2.4" punct="vg:7">R<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="7" punct="vg">eau</seg>x</w>,</l>
							<l n="3" num="1.3" lm="7" met="7"><w n="3.1">R<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="3.2" punct="vg:4">m<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="3.4" punct="vg:7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
							<l n="4" num="1.4" lm="7" met="7"><w n="4.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="4.2">fr<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="4.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>r</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="4.5" punct="pt:7"><seg phoneme="o" type="vs" value="1" rule="315" place="7" punct="pt">eau</seg>x</w>.</l>
							<l n="5" num="1.5" lm="7" met="7"><w n="5.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="5.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt</w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="5.5">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="5.7" punct="pv:7">h<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg></w> ;</l>
							<l n="6" num="1.6" lm="7" met="7"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="6.2">nu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="6.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="6.4">m</w>’<w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d</w> <w n="6.6" punct="pv:7">g<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pv">i</seg>r</w> ;</l>
							<l n="7" num="1.7" lm="7" met="7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="7.3">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>rs</w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="7.5">vi<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="7.6" punct="vg:7">C<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>lv<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
							<l n="8" num="1.8" lm="7" met="7"><w n="8.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="8.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="8.5" punct="vg:4">d<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>t</w>, <w n="8.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>t</w> <w n="8.7" punct="pt:7">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pt">i</seg>r</w>.</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1" lm="7" met="7"><w n="9.1">P<seg phoneme="o" type="vs" value="1" rule="318" place="1">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2" punct="vg:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">An</seg>g<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="9.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="9.5" punct="vg:7"><seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
							<l n="10" num="2.2" lm="7" met="7"><w n="10.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="10.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt</w> <w n="10.4" punct="vg:4">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4" punct="vg">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="10.6">n<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w>-<w n="10.7" punct="vg:7">pi<seg phoneme="e" type="vs" value="1" rule="241" place="7" punct="vg">e</seg>d</w>,</l>
							<l n="11" num="2.3" lm="7" met="7"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="11.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>g</w> <w n="11.4" punct="vg:7">p<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
							<l n="12" num="2.4" lm="7" met="7"><w n="12.1">N</w>’<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="497" place="1">y</seg></w> <w n="12.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w>-<w n="12.4">t</w>-<w n="12.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="12.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="12.7">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="12.8" punct="pi:7">p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="7" punct="pi ps">é</seg></w> ?…</l>
							<l n="13" num="2.5" lm="7" met="7"><w n="13.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="13.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="13.4" punct="vg:7">br<seg phoneme="y" type="vs" value="1" rule="454" place="6">u</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
							<l n="14" num="2.6" lm="7" met="7"><w n="14.1" punct="vg:3">C<seg phoneme="o" type="vs" value="1" rule="444" place="1">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg">e</seg></w>, <w n="14.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="14.3">v<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="14.4" punct="vg:7">g<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="vg">i</seg>r</w>,</l>
							<l n="15" num="2.7" lm="7" met="7"><w n="15.1"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">O</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="342" place="2">à</seg></w> <w n="15.3">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg></w> <w n="15.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="15.5">pr<seg phoneme="i" type="vs" value="1" rule="d-1" place="6">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410" place="7">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="16" num="2.8" lm="7" met="7"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="16.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="16.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="16.4">pu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="16.5" punct="pt:7">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pt">i</seg>r</w>.</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1" lm="7" met="7"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="17.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="2">e</seg>l</w> <w n="17.3" punct="vg:4">ph<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ltr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="17.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="17.5">br<seg phoneme="ø" type="vs" value="1" rule="405" place="6">eu</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="18" num="3.2" lm="7" met="7"><w n="18.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2" punct="vg">o</seg>rt</w>, <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg></w> <w n="18.3">f<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="18.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="18.5" punct="vg:7"><seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="vg">ai</seg>rs</w>,</l>
							<l n="19" num="3.3" lm="7" met="7"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="19.2">r<seg phoneme="a" type="vs" value="1" rule="341" place="2">a</seg>mi<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="19.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4">an</seg>s</w> <w n="19.4">l</w>’<w n="19.5" punct="vg:7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
							<l n="20" num="3.4" lm="7" met="7"><w n="20.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="20.2">l</w>’<w n="20.3"><seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="20.4">br<seg phoneme="y" type="vs" value="1" rule="445" place="4">û</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="20.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="20.6" punct="pi:7"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="pi">ai</seg>rs</w> ?</l>
							<l n="21" num="3.5" lm="7" met="7"><w n="21.1">D<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="21.3">f<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="21.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="22" num="3.6" lm="7" met="7"><w n="22.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="22.2">l</w>’<w n="22.3"><seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="22.4">n<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="22.5">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="22.6" punct="pv:7">g<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pv">i</seg>r</w> ;</l>
							<l n="23" num="3.7" lm="7" met="7"><w n="23.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="23.2">p<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>t</w>-<w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="23.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>s</w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="23.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="24" num="3.8" lm="7" met="7"><w n="24.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="24.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="24.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="24.4" punct="pe:7">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pe">i</seg>r</w> !</l>
						</lg>
						<lg n="4">
							<l n="25" num="4.1" lm="7" met="7"><w n="25.1" punct="pe:1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" punct="pe">A</seg>h</w> ! <w n="25.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="25.3">j</w>’<w n="25.4" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>s</w>, <w n="25.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="25.6" punct="vg:7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
							<l n="26" num="4.2" lm="7" met="7"><w n="26.1">M<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">d<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>r<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>b<seg phoneme="e" type="vs" value="1" rule="347" place="4">er</seg></w> <w n="26.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="26.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="26.5" punct="vg:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="vg">a</seg>s</w>,</l>
							<l n="27" num="4.3" lm="7" met="7"><w n="27.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="27.2">l</w>’<w n="27.3"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>r</w> <w n="27.4">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>f</w> <w n="27.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="27.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="27.7" punct="vg:7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
							<l n="28" num="4.4" lm="7" met="7"><w n="28.1">J</w>’<w n="28.2" punct="ps:3"><seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="482" place="2">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" punct="ps">ai</seg>s</w>… <w n="28.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rl<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg>s</w> <w n="28.4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="28.5" punct="pe:7">b<seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pe">a</seg>s</w> !</l>
							<l n="29" num="4.5" lm="7" met="7"><w n="29.1" punct="vg:2"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2" punct="vg">i</seg></w>, <w n="29.2">l</w>’<w n="29.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="29.4">m<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rt</w> <w n="29.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="29.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="29.7" punct="vg:7">p<seg phoneme="ɛ" type="vs" value="1" rule="385" place="7">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg>s</w>,</l>
							<l n="30" num="4.6" lm="7" met="7"><w n="30.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="30.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="30.3">n</w>’<w n="30.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="30.5">f<seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg>t</w> <w n="30.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="30.7" punct="pt:7">g<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pt">i</seg>r</w>.</l>
							<l n="31" num="4.7" lm="7" met="7"><w n="31.1" punct="vg:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" punct="vg">an</seg>t</w>, <w n="31.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="31.3">n</w>’<w n="31.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="31.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="31.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="31.7" punct="pv:7">ch<seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pv">e</seg>s</w> ;</l>
							<l n="32" num="4.8" lm="7" met="7"><w n="32.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="32.2" punct="ps:2">fu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="ps">i</seg>s</w>… <w n="32.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="32.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="32.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="32.6" punct="pe:7">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pe">i</seg>r</w> !</l>
						</lg>
						<lg n="5">
							<l n="33" num="5.1" lm="7" met="7"><w n="33.1">Cr<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>s</w>-<w n="33.2">t<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="33.3">qu</w>’<w n="33.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="33.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d</w> <w n="33.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="34" num="5.2" lm="7" met="7"><w n="34.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.2"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="2">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.4"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="34.5" punct="pi:7">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pi">eu</seg></w> ?</l>
							<l n="35" num="5.3" lm="7" met="7"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="133" place="1">E</seg>h</w> <w n="35.2" punct="pe:2">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="2" punct="pe">en</seg></w> ! <w n="35.3">qu</w>’<w n="35.4"><seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l</w> <w n="35.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="35.6">s<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="35.7" punct="vg:7">pr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="vg">e</seg></w>,</l>
							<l n="36" num="5.4" lm="7" met="7"><w n="36.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="36.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="36.3">j<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="3">oin</seg>s</w> <w n="36.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="36.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="36.6" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>di<seg phoneme="ø" type="vs" value="1" rule="398" place="7" punct="pt">eu</seg></w>.</l>
							<l n="37" num="5.5" lm="7" met="7"><w n="37.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg></w> <w n="37.3">C<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>lv<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.4"><seg phoneme="y" type="vs" value="1" rule="453" place="5">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="37.5"><seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="38" num="5.6" lm="7" met="7"><w n="38.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="38.2">ch<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="38.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="38.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="38.5" punct="pv:7">g<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pv">i</seg>r</w> ;</l>
							<l n="39" num="5.7" lm="7" met="7"><w n="39.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="39.2">c</w>’<w n="39.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="39.4" punct="vg:4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" punct="vg">e</seg></w>, <w n="39.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="5">e</seg>l</w> <w n="39.6" punct="pe:7">d<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>mm<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8" punct="pe">e</seg></w> !</l>
							<l n="40" num="5.8" lm="7" met="7"><w n="40.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="40.2">m</w>’<w n="40.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="3">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="40.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="40.5" punct="pe:7">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pe">i</seg>r</w> !</l>
						</lg>
						<lg n="6">
							<l n="41" num="6.1" lm="7" met="7"><w n="41.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="41.2">j<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>tt<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="41.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="41.4">l</w>’<w n="41.5"><seg phoneme="o" type="vs" value="1" rule="315" place="6">eau</seg></w> <w n="41.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="42" num="6.2" lm="7" met="7"><w n="42.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="42.2">n<seg phoneme="ø" type="vs" value="1" rule="248" place="2">œu</seg>d</w> <w n="42.3" punct="vg:4">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" punct="vg">ai</seg>t</w>, <w n="42.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="42.5" punct="vg:7">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="7" punct="vg">eu</seg>r</w>,</l>
							<l n="43" num="6.3" lm="7" met="7"><w n="43.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="43.2">c<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>t</w> <w n="43.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>nn<seg phoneme="o" type="vs" value="1" rule="315" place="4">eau</seg></w> <w n="43.4">d</w>’<w n="43.5">h<seg phoneme="i" type="vs" value="1" rule="d-4" place="5">y</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="7">in</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="44" num="6.4" lm="7" met="7"><w n="44.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="44.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="44.3">c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="44.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="44.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg></w> <w n="44.6" punct="pt:7">c<seg phoneme="œ" type="vs" value="1" rule="249" place="7" punct="pt">œu</seg>r</w>.</l>
							<l n="45" num="6.5" lm="7" met="7"><w n="45.1">V<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w>-<w n="45.2">t</w>’<w n="45.3" punct="pe:2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2" punct="pe">en</seg></w> ! <w n="45.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="45.5">n</w>’<w n="45.6"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg></w> <w n="45.7">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="45.8"><seg phoneme="a" type="vs" value="1" rule="342" place="6">à</seg></w> <w n="45.9">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="46" num="6.6" lm="7" met="7"><w n="46.1">Qu</w>’<w n="46.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="46.3"><seg phoneme="a" type="vs" value="1" rule="341" place="2">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="46.4"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="46.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="46.6" punct="pv:7">s<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pv">i</seg>r</w> ;</l>
							<l n="47" num="6.7" lm="7" met="7"><w n="47.1">B<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="47.2">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>t</w> <w n="47.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="47.4">d<seg phoneme="wa" type="vs" value="1" rule="420" place="5">oi</seg>t</w> <w n="47.5">t</w>’<w n="47.6"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="8">e</seg></w></l>
							<l n="48" num="6.8" lm="7" met="7"><w n="48.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="48.2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg></w> <w n="48.3">d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>gn<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="48.4">l</w>’<w n="48.5" punct="pe:7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="7" punct="pe">i</seg>r</w> !</l>
						</lg>
					</div></body></text></TEI>