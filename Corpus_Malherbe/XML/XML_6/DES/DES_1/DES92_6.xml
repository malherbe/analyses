<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES92" modus="sm" lm_max="8" metProfile="8">
						<head type="main">JE DORMAIS</head>
						<lg n="1">
							<l n="1" num="1.1" lm="8" met="8"><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">On</seg></w> <w n="1.2" punct="pv:2">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="2" punct="pv">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ; <w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="1.4" punct="pv:4">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="4" punct="pv">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ; <w n="1.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="1.6">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="6">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7" punct="pv:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
							<l n="2" num="1.2" lm="8" met="8"><w n="2.1">C</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="2.3" punct="pe:2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2" punct="pe ps">i</seg></w> !… <w n="2.4" punct="pe:3">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="3" punct="pe">eu</seg></w> ! <w n="2.5">qu</w>’<w n="2.6"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="2.7">m</w>’<w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="2.9">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="2.10" punct="pe:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg>r</w> !</l>
							<l n="3" num="1.3" lm="8" met="8"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>l</w> <w n="3.3" punct="vg:4">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="4" punct="vg">en</seg>t</w>, <w n="3.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="3.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="3.6">l</w>’<w n="3.7" punct="dp:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
							<l n="4" num="1.4" lm="8" met="8"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="3">ez</seg></w>-<w n="4.2" punct="vg:4">v<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>s</w>, <w n="4.3">c<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="347" place="6">ez</seg></w> <w n="4.4" punct="pe:8"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pe">i</seg>r</w> !</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="8" met="8"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w>-<w n="5.2" punct="vg:4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="5.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="5.4" punct="vg:8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
							<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="6.2">s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="6.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="4">en</seg>t</w> <w n="6.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>l</w> <w n="6.5">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="6.6" punct="pt:8">tr<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></w>.</l>
							<l n="7" num="2.3" lm="8" met="8"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="7.2">v<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="7.3" punct="ps:4">v<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="ps">i</seg>r</w>… <w n="7.4">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="7.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>g</w> <w n="7.6">s</w>’<w n="7.7" punct="pv:8"><seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412" place="8">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
							<l n="8" num="2.4" lm="8" met="8"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468" place="1">I</seg>l</w> <w n="8.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3" punct="ps:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" punct="ps">o</seg>r</w>… <w n="8.4">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="8.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6">œu</seg>r</w> <w n="8.6">s</w>’<w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="8.8" punct="pt:8">v<seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="8" met="8"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="9.2">n</w>’<w n="9.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="9.4">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="9.5" punct="pt:4">pl<seg phoneme="y" type="vs" value="1" rule="450" place="4" punct="pt">u</seg>s</w>. <w n="9.6">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="9.7">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>l</w> <w n="9.8">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="9.9" punct="pv:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
							<l n="10" num="3.2" lm="8" met="8"><w n="10.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="10.2" punct="vg:4">t<seg phoneme="wa" type="vs" value="1" rule="423" place="4" punct="vg">oi</seg></w>, <w n="10.3">n<seg phoneme="y" type="vs" value="1" rule="d-3" place="5">u</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4" punct="pe:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pe">ai</seg>s</w> !</l>
							<l n="11" num="3.3" lm="8" met="8"><w n="11.1">J</w>’<w n="11.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>ds</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="11.4" punct="vg:4">br<seg phoneme="a" type="vs" value="1" rule="340" place="4" punct="vg">a</seg>s</w>, <w n="11.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="11.6"><seg phoneme="œ" type="vs" value="1" rule="286" place="6">œ</seg>il</w> <w n="11.7">s</w>’<w n="11.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">en</seg>tr</w>’<w n="11.9" punct="ps:8"><seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="ps">e</seg></w>…</l>
							<l n="12" num="3.4" lm="8" met="8"><w n="12.1" punct="pe:1">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="1" punct="pe ps">eu</seg></w> !… <w n="12.2">c</w>’<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="3">un</seg></w> <w n="12.5" punct="vg:4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="vg">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="12.7">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="12.8" punct="pt:8">d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>s</w>.</l>
						</lg>
					</div></body></text></TEI>