<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES67" modus="cp" lm_max="12" metProfile="4+6, 6+6">
						<head type="main">SOUVENIR</head>
						<lg n="1">
							<l n="1" num="1.1" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="1.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>rs</w> <w n="1.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="1.3">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5" mp="C">au</seg></w> <w n="1.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="6">om</seg></w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="Pem">e</seg></w> <w n="1.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="1.8" punct="dp:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" punct="dp">an</seg>t</w> :</l>
							<l n="2" num="1.2" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="2.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1" mp="P">an</seg>s</w> <w n="2.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="2.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3" mp="M">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg></w><caesura></caesura> <w n="2.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="5">en</seg></w> <w n="2.5">n</w>’<w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="6">e</seg>st</w> <w n="2.7">b<seg phoneme="o" type="vs" value="1" rule="315" place="7">eau</seg></w> <w n="2.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" mp="P">an</seg>s</w> <w n="2.9">m<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="C">a</seg></w> <w n="2.10" punct="pt:10">v<seg phoneme="i" type="vs" value="1" rule="482" place="10">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pt" mp="F">e</seg></w>.</l>
							<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="3.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189" place="3">e</seg>t</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="3.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="3.6" punct="vg:6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6" punct="vg" caesura="1">en</seg>s</w>,<caesura></caesura> <w n="3.7">c</w>’<w n="3.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="7">e</seg>st</w> <w n="3.9">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="3.10">s<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>l</w> <w n="3.11">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="10">e</seg></w> <w n="3.12">j</w>’<w n="3.13" punct="vg:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
							<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="4.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>ds</w> <w n="4.6" punct="vg:6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6" punct="vg" caesura="1">u</seg>s</w>,<caesura></caesura> <w n="4.7">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="4.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="8">o</seg>rt</w> <w n="4.9">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="4.10">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="4.11" punct="pt:12">d<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="12" punct="pt">en</seg>d</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="5.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" caesura="1">oi</seg>s</w><caesura></caesura> <w n="5.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="5.6">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r</w> <w n="5.7" punct="pv:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>ph<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="10">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pv" mp="F">e</seg></w> ;</l>
							<l n="6" num="2.2" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="6.1"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="1" mp="C">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4" caesura="1">aî</seg>t</w><caesura></caesura> <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="P">ou</seg>r</w> <w n="6.4">s<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="6.6" punct="pv:10">p<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pv">i</seg>r</w> ;</l>
							<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2" punct="vg:3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" punct="vg" mp="F">e</seg></w>, <w n="7.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="7.4" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>t</w>,<caesura></caesura> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="P">u</seg>r</w> <w n="7.6">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="7.7">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="9">ein</seg></w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="7.9">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="7.10" punct="vg:12">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
							<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="2">è</seg>s</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>r</w> <w n="8.3" punct="vg:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="8.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="8.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">en</seg>ch<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg></w> <w n="8.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="P">ou</seg>r</w> <w n="8.7" punct="pt:12">m<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="12" punct="pt">i</seg>r</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="9.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4" caesura="1">oi</seg>s</w><caesura></caesura> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t</w> <w n="9.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">où</seg></w> <w n="9.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="Pem">e</seg></w> <w n="9.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="9" mp="C">on</seg></w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="341" place="10">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" mp="F">e</seg></w></l>
							<l n="10" num="3.2" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="10.1">S</w>’<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" caesura="1">o</seg>r</w><caesura></caesura> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="10.5">m<seg phoneme="u" type="vs" value="1" rule="425" place="6" mp="M">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="10.6" punct="dp:10">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407" place="10" punct="dp">eu</seg>r</w> :</l>
							<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="11.3">j<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>r</w> <w n="11.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="4" mp="P">u</seg>r</w> <w n="11.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="11.6">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6" caesura="1">eu</seg>x</w><caesura></caesura> <w n="11.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="11.8">r<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>d</w> <w n="11.9">pl<seg phoneme="y" type="vs" value="1" rule="450" place="10">u</seg>s</w> <w n="11.10">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="11.11" punct="vg:12">fl<seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
							<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="12.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="3" mp="Mem">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s</w> <w n="12.4" punct="dp:6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="dp" caesura="1">ou</seg>rs</w> :<caesura></caesura> <w n="12.5">n</w>’<w n="12.6"><seg phoneme="ɛ" type="vs" value="1" rule="50" place="7">e</seg>s</w> <w n="12.7">t</w>-<w n="12.8"><seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>l</w> <w n="12.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="12.10">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="12.11">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="12.12" punct="pi:12">c<seg phoneme="œ" type="vs" value="1" rule="249" place="12" punct="pi">œu</seg>r</w> ?</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="13.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="13.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="13.3" punct="pe:4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="pe" caesura="1">an</seg>t</w> !<caesura></caesura> <w n="13.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="13.5">pl<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>s</w> <w n="13.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="13.7" punct="pe:10">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pe" mp="F">e</seg></w> !</l>
							<l n="14" num="4.2" lm="10" met="4+6"><space quantity="2" unit="char"></space><w n="14.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="14.2"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="341" place="3" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="4" caesura="1">ou</seg>r</w><caesura></caesura> <w n="14.4">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="14.5">ti<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="6" mp="M">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>t</w> <w n="14.6">li<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg></w> <w n="14.7">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="14.8" punct="pi:10">t<seg phoneme="wa" type="vs" value="1" rule="423" place="10" punct="pi">oi</seg></w> ?</l>
							<l n="15" num="4.3" lm="12" met="6+6"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="15.2">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="15.3" punct="vg:4">g<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg></w>, <w n="15.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="15.5" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>ls</w>,<caesura></caesura> <w n="15.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="15.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="15.8">f<seg phoneme="y" type="vs" value="1" rule="450" place="9">u</seg>s</w> <w n="15.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>s</w> <w n="15.10" punct="pv:12">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="11" mp="M">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="12">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
							<l n="16" num="4.4" lm="12" met="6+6"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="16.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="16.3">f<seg phoneme="i" type="vs" value="1" rule="468" place="3" mp="M">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410" place="4">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.4" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="467" place="5" mp="M">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="16.5" punct="pe:7"><seg phoneme="o" type="vs" value="1" rule="444" place="7" punct="pe">o</seg>h</w> ! <w n="16.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="8">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.7"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.8"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="16.9"><seg phoneme="a" type="vs" value="1" rule="342" place="11" mp="P">à</seg></w> <w n="16.10" punct="pe:12">m<seg phoneme="wa" type="vs" value="1" rule="423" place="12" punct="pe">oi</seg></w> !</l>
						</lg>
					</div></body></text></TEI>