<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES82" modus="sp" lm_max="7" metProfile="3, 7">
						<head type="main">DORS, MA MÈRE</head>
						<lg n="1">
							<l n="1" num="1.1" lm="3" met="3"><space quantity="6" unit="char"></space><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="415" place="1">Ô</seg></w> <w n="1.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="1.3" punct="vg:3">v<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
							<l n="2" num="1.2" lm="3" met="3"><space quantity="6" unit="char"></space><w n="2.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="2.2" punct="vg:3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482" place="3">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
							<l n="3" num="1.3" lm="7" met="7"><w n="3.1">J</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="1">ai</seg></w> <w n="3.3">v<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg></w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="3.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="3.7" punct="pv:7">r<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="pv">oi</seg></w> ;</l>
							<l n="4" num="1.4" lm="3" met="3"><space quantity="6" unit="char"></space><w n="4.1">M<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="4.2">ch<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>mi<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></w></l>
							<l n="5" num="1.5" lm="3" met="3"><space quantity="6" unit="char"></space><w n="5.1">M</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="5.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="5.4" punct="vg:3">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
							<l n="6" num="1.6" lm="7" met="7"><w n="6.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>d</w> <w n="6.2">j</w>’<w n="6.3"><seg phoneme="i" type="vs" value="1" rule="497" place="2">y</seg></w> <w n="6.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="3">i</seg>s</w> <w n="6.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346" place="6">e</seg>c</w> <w n="6.7" punct="pe:7">t<seg phoneme="wa" type="vs" value="1" rule="423" place="7" punct="pe">oi</seg></w> !</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1" lm="3" met="3"><space quantity="6" unit="char"></space><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="7.2" punct="vg:3">v<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
							<l n="8" num="2.2" lm="3" met="3"><space quantity="6" unit="char"></space><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="8.2">j<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></w></l>
							<l n="9" num="2.3" lm="7" met="7"><w n="9.1">N</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="9.3">h<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="3">eu</seg>x</w> <w n="9.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r</w> <w n="9.6">l</w>’<w n="9.7" punct="pv:7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="pv">ou</seg>r</w> ;</l>
							<l n="10" num="2.4" lm="3" met="3"><space quantity="6" unit="char"></space><w n="10.1">Fu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="10.3" punct="pv:3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pv">e</seg></w> ;</l>
							<l n="11" num="2.5" lm="3" met="3"><space quantity="6" unit="char"></space><w n="11.1">Tr<seg phoneme="o" type="vs" value="1" rule="433" place="1">o</seg>p</w> <w n="11.2" punct="vg:3">f<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
							<l n="12" num="2.6" lm="7" met="7"><w n="12.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="12.2">m</w>’<w n="12.3"><seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="468" place="3">î</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="342" place="5">à</seg></w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="12.6" punct="pt:7">c<seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="pt">ou</seg>r</w>.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1" lm="3" met="3"><space quantity="6" unit="char"></space><w n="13.1">D</w>’<w n="13.2"><seg phoneme="y" type="vs" value="1" rule="453" place="1">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="13.3">r<seg phoneme="ɛ" type="vs" value="1" rule="385" place="3">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></w></l>
							<l n="14" num="3.2" lm="3" met="3"><space quantity="6" unit="char"></space><w n="14.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="305" place="3">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></w></l>
							<l n="15" num="3.3" lm="7" met="7"><w n="15.1">L</w>’<w n="15.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w>-<w n="15.4">t</w>-<w n="15.5"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="15.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="15.7">d</w>’<w n="15.8" punct="pi:7"><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pi">a</seg>s</w> ?</l>
							<l n="16" num="3.4" lm="3" met="3"><space quantity="6" unit="char"></space><w n="16.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="16.2"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></w></l>
							<l n="17" num="3.5" lm="3" met="3"><space quantity="6" unit="char"></space><w n="17.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="i" type="vs" value="1" rule="467" place="2">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></w></l>
							<l n="18" num="3.6" lm="7" met="7"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">ou</seg></w> <w n="18.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="18.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="18.6" punct="pt:7">p<seg phoneme="a" type="vs" value="1" rule="340" place="7" punct="pt">a</seg>s</w>.</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1" lm="3" met="3"><space quantity="6" unit="char"></space><w n="19.1">R<seg phoneme="ə" type="em" value="1" rule="e-19" place="1">e</seg>vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="2">en</seg>s</w> <w n="19.2" punct="pe:3">v<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pe">e</seg></w> !</l>
							<l n="20" num="4.2" lm="3" met="3"><space quantity="6" unit="char"></space><w n="20.1">T<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>t</w> <w n="20.2">m</w>’<w n="20.3" punct="dp:3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="dp">e</seg></w> :</l>
							<l n="21" num="4.3" lm="7" met="7"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="133" place="1">E</seg>h</w> <w n="21.2" punct="pe:2">qu<seg phoneme="wa" type="vs" value="1" rule="281" place="2" punct="pe">oi</seg></w> ! <w n="21.3">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="21.4">su<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg>s</w> <w n="21.5">s<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <choice reason="analysis" type="false_verse" hand="RR"><sic>encore</sic><corr source="édition_1830"><w n="21.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="7" punct="pe">o</seg>r</w></corr></choice> !</l>
							<l n="22" num="4.4" lm="3" met="3"><space quantity="6" unit="char"></space><w n="22.1" punct="vg:1">Vi<seg phoneme="ɛ̃" type="vs" value="1" rule="373" place="1" punct="vg">en</seg>s</w>, <w n="22.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="22.3" punct="vg:3"><seg phoneme="a" type="vs" value="1" rule="341" place="3">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
							<l n="23" num="4.5" lm="3" met="3"><space quantity="6" unit="char"></space><w n="23.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="23.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="23.3">fl<seg phoneme="a" type="vs" value="1" rule="341" place="3">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></w></l>
							<l n="24" num="4.6" lm="7" met="7"><w n="24.1">P<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="24.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="24.3">d<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>x</w> <w n="24.4" punct="pt:7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="439" place="7" punct="pt">o</seg>rt</w>.</l>
						</lg>
						<lg n="5">
							<l n="25" num="5.1" lm="3" met="3"><space quantity="6" unit="char"></space><w n="25.1">L</w>’<w n="25.2">h<seg phoneme="œ" type="vs" value="1" rule="407" place="1">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="25.3" punct="vg:3">s<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
							<l n="26" num="5.2" lm="3" met="3"><space quantity="6" unit="char"></space><w n="26.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2" punct="pt:3">fr<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="pt">e</seg></w>.</l>
							<l n="27" num="5.3" lm="7" met="7"><w n="27.1">V<seg phoneme="wa" type="vs" value="1" rule="420" place="1">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="27.2">l</w>’<w n="27.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="27.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="27.5" punct="pt:7">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="pt">ou</seg>r</w>.</l>
							<l n="28" num="5.4" lm="3" met="3"><space quantity="6" unit="char"></space><w n="28.1">M<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="1">oin</seg>s</w> <w n="28.2">s<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4">e</seg></w></l>
							<l n="29" num="5.5" lm="3" met="3"><space quantity="6" unit="char"></space><w n="29.1" punct="vg:1">D<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" punct="vg">o</seg>rs</w>, <w n="29.2">m<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="29.3" punct="vg:3">m<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="4" punct="vg">e</seg></w>,</l>
							<l n="30" num="5.6" lm="7" met="7"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="30.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="30.3">v<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="30.4">l</w>’<w n="30.5" punct="pt:7"><seg phoneme="a" type="vs" value="1" rule="341" place="6">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="7" punct="pt">ou</seg>r</w>.</l>
						</lg>
					</div></body></text></TEI>