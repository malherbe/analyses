<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA110" modus="cm" lm_max="12" metProfile="6+6">
					<head type="main">A UN TOIT HOSPITALIER</head>
					<lg n="1">
						<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="1.2">j<seg phoneme="a" type="vs" value="1" rule="341" place="2" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="1.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="1.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>s</w><caesura></caesura> <w n="1.6">l</w>’<w n="1.7"><seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8" mp="M">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346" place="9">e</seg>l</w> <w n="1.8">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="1.9">t</w>’<w n="1.10" punct="vg:12"><seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="o" type="vs" value="1" rule="444" place="2" mp="M">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4" mp="F">e</seg></w> <w n="2.4">t</w>’<w n="2.5" punct="vg:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="vg" caesura="1">a</seg>rgn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="2.7">t<seg phoneme="wa" type="vs" value="1" rule="420" place="8">oi</seg>t</w> <w n="2.8" punct="pe:12">h<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="10" mp="M">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pe">er</seg></w> !</l>
						<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>r</w> <w n="3.2">j</w>’<w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg></w> <w n="3.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="3.6">j<seg phoneme="wa" type="vs" value="1" rule="423" place="6" caesura="1">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w><caesura></caesura> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="3.8">l</w>’<w n="3.9"><seg phoneme="a" type="vs" value="1" rule="341" place="8" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="10">é</seg></w> <w n="3.10" punct="vg:12">ch<seg phoneme="e" type="vs" value="1" rule="409" place="11" mp="M">é</seg>r<seg phoneme="i" type="vs" value="1" rule="482" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
						<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">H<seg phoneme="o" type="vs" value="1" rule="415" place="1">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="4.2" punct="vg:4">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4" punct="vg">an</seg>ts</w>, <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>s</w><caesura></caesura> <w n="4.4"><seg phoneme="o" type="vs" value="1" rule="318" place="7" mp="M">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="Pem">e</seg></w> <w n="4.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="10" mp="C">on</seg></w> <w n="4.7" punct="pt:12">f<seg phoneme="wa" type="vs" value="1" rule="440" place="11" mp="M">o</seg>y<seg phoneme="e" type="vs" value="1" rule="347" place="12" punct="pt">er</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">D</w>’<w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="5.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2" mp="M">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg></w> <w n="5.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4" mp="M">e</seg>rd<seg phoneme="wa" type="vs" value="1" rule="440" place="5" mp="M">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>t</w><caesura></caesura> <w n="5.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="5.7">p<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">ai</seg>x</w> <w n="5.8">t</w>’<w n="5.9" punct="pv:12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="10" mp="M">en</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
						<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="6.2">p<seg phoneme="u" type="vs" value="1" rule="425" place="2" mp="P">ou</seg>r</w> <w n="6.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3" mp="C">e</seg>s</w> <w n="6.4">h<seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="M">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="M">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7" mp="C">e</seg>s</w> <w n="6.6">j<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>rs</w> <w n="6.7">s<seg phoneme="wa" type="vs" value="1" rule="423" place="9">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="6.9" punct="pv:12">d<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pv">eu</seg>rs</w> ;</l>
						<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>r</w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="342" place="5" mp="P">à</seg></w> <w n="7.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="6" caesura="1">ou</seg>r</w><caesura></caesura> <w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22" place="9" mp="F">e</seg>nt</w> <w n="7.8">t<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="7.9">c<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419" place="12">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="Pem">e</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="C">a</seg></w> <w n="8.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>r</w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="8.5">fru<seg phoneme="i" type="vs" value="1" rule="491" place="6" caesura="1">i</seg>ts</w><caesura></caesura> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="8.7">d<seg phoneme="y" type="vs" value="1" rule="450" place="8" mp="C">u</seg></w> <w n="8.8">p<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="10">um</seg></w> <w n="8.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="11" mp="C">e</seg>s</w> <w n="8.10" punct="pe:12">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pe">eu</seg>rs</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1" punct="vg:1">T<seg phoneme="wa" type="vs" value="1" rule="423" place="1" punct="vg">oi</seg></w>, <w n="9.2">n</w>’<w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="2">en</seg></w> <w n="9.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>s</w> <w n="9.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417" place="4">oin</seg>t</w> <w n="9.6" punct="vg:6">j<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="vg" caesura="1">ou</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>,<caesura></caesura> <w n="9.7"><seg phoneme="o" type="vs" value="1" rule="415" place="7">ô</seg></w> <w n="9.8">m<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.9">ch<seg phoneme="ɛ" type="vs" value="1" rule="410" place="9">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="9.10" punct="pe:12">N<seg phoneme="ø" type="vs" value="1" rule="403" place="11" mp="M">eu</seg>str<seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pe" mp="F">e</seg></w> !</l>
						<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="C">on</seg></w> <w n="10.2">d<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>x</w> <w n="10.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200" place="3">om</seg></w> <w n="10.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="10.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5" mp="C">e</seg>s</w> <w n="10.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" caesura="1">an</seg>ts</w><caesura></caesura> <w n="10.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="10.8">f<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>t</w> <w n="10.9">p<seg phoneme="a" type="vs" value="1" rule="340" place="9">a</seg>s</w> <w n="10.10" punct="pt:12"><seg phoneme="u" type="vs" value="1" rule="425" place="10" mp="M">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
						<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="11.2">m</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="11.4">d<seg phoneme="o" type="vs" value="1" rule="435" place="3" mp="M">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5" mp="C">e</seg></w> <w n="11.6" punct="pv:6">j<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pv" caesura="1">ou</seg>r</w> ;<caesura></caesura> <w n="11.7" punct="vg:7">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7" punct="vg">ai</seg>s</w>, <w n="11.8">p<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="P">ou</seg>r</w> <w n="11.9" punct="vg:9">m<seg phoneme="wa" type="vs" value="1" rule="423" place="9" punct="vg">oi</seg></w>, <w n="11.10">l<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="C">a</seg></w> <w n="11.11">p<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469" place="12">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></w></l>
						<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">E</seg>st</w> <w n="12.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>t</w> <w n="12.3"><seg phoneme="u" type="vs" value="1" rule="426" place="4">où</seg></w> <w n="12.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="12.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="12.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7" mp="M">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="12.7">l</w>’<w n="12.8" punct="pt:12"><seg phoneme="a" type="vs" value="1" rule="341" place="10" mp="M">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="12" punct="pt">é</seg></w>.</l>
					</lg>
				</div></body></text></TEI>