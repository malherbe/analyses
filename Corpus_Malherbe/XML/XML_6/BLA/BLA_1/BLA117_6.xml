<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA117" modus="sm" lm_max="8" metProfile="8">
					<head type="main">LE JARDIN DE WANG-WEI</head>
					<lg n="1">
						<l n="1" num="1.1" lm="8" met="8"><w n="1.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="1.2">h<seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>t</w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="1.4">b<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>s</w> <w n="1.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="1.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w></l>
						<l n="2" num="1.2" lm="8" met="8"><w n="2.1">Br<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>t</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307" place="4">a</seg>il</w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="2.5">br<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="7">e</seg>s</w> <w n="2.6" punct="pv:8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg>s</w> ;</l>
						<l n="3" num="1.3" lm="8" met="8"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="3.3">f<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="411" place="5">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="3.4"><seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="4" num="1.4" lm="8" met="8"><w n="4.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="4.3" punct="pt:8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" punct="pt">on</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1" lm="8" met="8"><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="5.3" punct="vg:4">p<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>li<seg phoneme="e" type="vs" value="1" rule="347" place="4" punct="vg">er</seg>s</w>, <w n="5.4">s<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>r</w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="5.6" punct="vg:8">m<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>r<seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg>s</w>,</l>
						<l n="6" num="2.2" lm="8" met="8"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>x</w> <w n="6.2">s<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="6.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="6.4" punct="vg:8">c<seg phoneme="o" type="vs" value="1" rule="435" place="6">o</seg>rr<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443" place="8" punct="vg">o</seg>r</w>,</l>
						<l n="7" num="2.3" lm="8" met="8"><w n="7.1">S</w>’<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.3">m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="7.4">dr<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>g<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="7">on</seg>s</w> <w n="7.5">d</w>’<w n="7.6"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="8">o</seg>r</w></l>
						<l n="8" num="2.4" lm="8" met="8"><w n="8.1">Cu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>s</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22" place="6">e</seg>s</w> <w n="8.4" punct="pt:8"><seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<seg phoneme="a" type="vs" value="1" rule="307" place="8">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1" lm="8" met="8"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">Un</seg></w> <w n="9.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd</w> <w n="9.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="3">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg></w> <w n="9.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>r<seg phoneme="o" type="vs" value="1" rule="435" place="7">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>t</w></l>
						<l n="10" num="3.2" lm="8" met="8"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="10.2">t<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>t</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="10.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="4">o</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305" place="6">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5" punct="pv:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pv">e</seg></w> ;</l>
						<l n="11" num="3.3" lm="8" met="8"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="11.2">g<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="u" type="vs" value="1" rule="d-2" place="3">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="11.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>st<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="12" num="3.4" lm="8" met="8"><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="12.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="12.4">bl<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg></w> <w n="12.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="12.6" punct="pt:8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">on</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8" punct="pt">ai</seg>t</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1" lm="8" met="8"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="13.2">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="13.3">b<seg phoneme="o" type="vs" value="1" rule="315" place="3">eau</seg>x</w> <w n="13.4"><seg phoneme="wa" type="vs" value="1" rule="420" place="4">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>x</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg></w> <w n="13.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="14" num="4.2" lm="8" met="8"><w n="14.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306" place="2">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="14.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="3">an</seg>s</w> <w n="14.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="14.4">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>squ<seg phoneme="ɛ" type="vs" value="1" rule="190" place="6">e</seg>ts</w> <w n="14.5" punct="pv:8"><seg phoneme="ɔ" type="vs" value="1" rule="439" place="7">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450" place="8" punct="pv">u</seg>rs</w> ;</l>
						<l n="15" num="4.3" lm="8" met="8"><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="15.2">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>rs</w> <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>x</w> <w n="15.4">p<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="5">um</seg>s</w> <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="6">e</seg>s</w> <w n="15.6">pl<seg phoneme="y" type="vs" value="1" rule="450" place="7">u</seg>s</w> <w n="15.7">p<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>rs</w></l>
						<l n="16" num="4.4" lm="8" met="8"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="409" place="1">É</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306" place="4">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="16.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="16.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="16.4" punct="pt:8">p<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1" lm="8" met="8"><w n="17.1">S<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>du<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg>t</w> <w n="17.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>r</w> <w n="17.3">l</w>’<w n="17.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t</w> <w n="17.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="17.6" punct="vg:8">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>rg<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>,</l>
						<l n="18" num="5.2" lm="8" met="8"><w n="18.1">J</w>’<w n="18.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="18.3">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="18.4">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407" place="6">eu</seg>r</w> <w n="18.5" punct="vg:8">f<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="19" num="5.3" lm="8" met="8"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="19.2">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="19.4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>s</w> <w n="19.5">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="6">en</seg></w> <w n="19.6"><seg phoneme="o" type="vs" value="1" rule="318" place="7">au</seg></w> <w n="19.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="20" num="5.4" lm="8" met="8"><w n="20.1">Pl<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>s</w> <w n="20.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg></w> <w n="20.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="20.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="20.5">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="20.6">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="20.7" punct="pt:8">p<seg phoneme="ɛ" type="vs" value="1" rule="412" place="7">ê</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1" lm="8" met="8"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="21.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="21.4">c<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>p</w> <w n="21.5">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="21.6">v<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>s</w> <w n="21.7" punct="vg:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="22" num="6.2" lm="8" met="8"><w n="22.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="22.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="22.3">tr<seg phoneme="e" type="vs" value="1" rule="383" place="4">e</seg>ill<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s</w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="22.5" punct="vg:8">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">am</seg>b<seg phoneme="u" type="vs" value="1" rule="425" place="8" punct="vg">ou</seg>s</w>,</l>
						<l n="23" num="6.3" lm="8" met="8"><w n="23.1">D<seg phoneme="ø" type="vs" value="1" rule="398" place="1">eu</seg>x</w> <w n="23.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="23.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="23.4">y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="23.5">br<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>ts</w> <w n="23.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="23.7">d<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>x</w></l>
						<l n="24" num="6.4" lm="8" met="8"><w n="24.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>r</w> <w n="24.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="24.3">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg></w> <w n="24.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rdr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>t</w> <w n="24.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="6">un</seg></w> <w n="24.6" punct="pt:8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="7">em</seg>p<seg phoneme="i" type="vs" value="1" rule="468" place="8">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1" lm="8" met="8"><w n="25.1">L</w>’<w n="25.2"><seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="2">en</seg>t<seg phoneme="a" type="vs" value="1" rule="307" place="3">a</seg>il</w> <w n="25.3">l<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347" place="5">er</seg></w> <w n="25.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="25.5" punct="pv:8">pl<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pv">a</seg></w> ;</l>
						<l n="26" num="7.2" lm="8" met="8"><w n="26.1">J</w>’<w n="26.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>rç<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg>s</w> <w n="26.3">l</w>’<w n="26.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>t</w> <w n="26.5">r<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="26.7" punct="vg:8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="27" num="7.3" lm="8" met="8"><w n="27.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="27.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="27.4">s<seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="27.5">qu<seg phoneme="i" type="vs" value="1" rule="491" place="7">i</seg></w> <w n="27.6" punct="vg:8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="28" num="7.4" lm="8" met="8"><w n="28.1">Fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="28.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419" place="3">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="28.4" punct="pt:8">c<seg phoneme="a" type="vs" value="1" rule="341" place="5">a</seg>m<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>l<seg phoneme="i" type="vs" value="1" rule="d-1" place="7">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="8" punct="pt">a</seg></w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1" lm="8" met="8"><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="29.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="29.3" punct="vg:3">gr<seg phoneme="a" type="vs" value="1" rule="340" place="3" punct="vg">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="29.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="29.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="29.6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="6">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>rd</w> <w n="29.7" punct="vg:8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="vg">e</seg></w>,</l>
						<l n="30" num="8.2" lm="8" met="8"><w n="30.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="30.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="30.3">s<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>ls</w> <w n="30.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>s</w> <w n="30.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="30.6" punct="vg:8">s<seg phoneme="wa" type="vs" value="1" rule="440" place="7">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="vg">eu</seg>x</w>,</l>
						<l n="31" num="8.3" lm="8" met="8"><w n="31.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="31.2">cr<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="31.3">v<seg phoneme="wa" type="vs" value="1" rule="420" place="3">oi</seg>r</w> <w n="31.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="31.5">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="31.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="31.7">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="8">eu</seg>x</w></l>
						<l n="32" num="8.4" lm="8" met="8"><w n="32.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="32.2">p<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="32.3">n<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>s</w> <w n="32.4">d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="32.5" punct="pt:8">d<seg phoneme="ɛ" type="vs" value="1" rule="358" place="7">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pt">e</seg></w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1" lm="8" met="8"><w n="33.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="33.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="33.3"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ct</w> <w n="33.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6">e</seg></w> <w n="33.5" punct="vg:8">s<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="vg">i</seg>t</w>,</l>
						<l n="34" num="9.2" lm="8" met="8"><w n="34.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="34.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="34.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="34.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6">en</seg>ts</w> <w n="34.6">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="34.7" punct="dp:8">j<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="dp">e</seg></w> :</l>
						<l n="35" num="9.3" lm="8" met="8"><w n="35.1">Pu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg>s</w> <w n="35.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="35.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>g<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="35.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="35.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg></w> <w n="35.6">gr<seg phoneme="ə" type="em" value="1" rule="e-19" place="7">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="8">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg></w></l>
						<l n="36" num="9.4" lm="8" met="8"><w n="36.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="36.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="36.3">d<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t</w> <w n="36.4">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="36.5" punct="pt:8">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>t</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1" lm="8" met="8"><w n="37.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg></w> <w n="37.2" punct="pe:3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="2">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" punct="pe">em</seg>ps</w> ! <w n="37.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="37.4">b<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg>s</w> <w n="37.5">n<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9">e</seg>s</w></l>
						<l n="38" num="10.2" lm="8" met="8"><w n="38.1">N</w>’<w n="38.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="38.3">pl<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>s</w> <w n="38.4">d</w>’<w n="38.5"><seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="38.6">p<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>r</w> <w n="38.7">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="6">e</seg></w> <w n="38.8" punct="pv:8">t<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pv">er</seg></w> ;</l>
						<l n="39" num="10.3" lm="8" met="8"><w n="39.1">J</w>’<w n="39.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>s</w> <w n="39.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="39.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="5">eu</seg>rs</w> <w n="39.5">d<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg></w> <w n="39.6" punct="vg:8">p<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="vg">er</seg></w>,</l>
						<l n="40" num="10.4" lm="8" met="8"><w n="40.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="40.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="2">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="3">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t</w> <w n="40.3">j</w>’<w n="40.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="5">ai</seg></w> <w n="40.5">p<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>ti<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="40.6">d</w>’<w n="40.7" punct="pe:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="9" punct="pe">e</seg>s</w> !</l>
					</lg>
				</div></body></text></TEI>