<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA30" modus="sm" lm_max="6" metProfile="6">
					<head type="main">L’OREILLE</head>
					<lg n="1">
						<l n="1" num="1.1" lm="6" met="6"><w n="1.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="1.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="1.3" punct="vg:6">ch<seg phoneme="ə" type="em" value="1" rule="e-19" place="4">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="2" num="1.2" lm="6" met="6"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg>t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>r</w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="2.3">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="2.4" punct="vg:6">f<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="3" num="1.3" lm="6" met="6"><w n="3.1">F<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="3.3">s<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="3.4" punct="vg:6">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="vg">eau</seg></w>,</l>
						<l n="4" num="1.4" lm="6" met="6"><w n="4.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="4.3">br<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="4.4">m<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="5" num="1.5" lm="6" met="6"><w n="5.1" punct="vg:2">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="5.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3">an</seg>t</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="5">e</seg>st</w> <w n="5.5" punct="vg:6">n<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="6" num="1.6" lm="6" met="6"><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>r</w> <w n="6.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2">e</seg>s</w> <w n="6.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22" place="4">e</seg>s</w> <w n="6.4">d</w>’<w n="6.5" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="wa" type="vs" value="1" rule="420" place="6">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="7" num="1.7" lm="6" met="6"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1">e</seg>s</w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="2">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22" place="3">e</seg>s</w> <w n="7.3">d</w>’<w n="7.4"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="4">un</seg></w> <w n="7.5" punct="pt:6">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>rb<seg phoneme="o" type="vs" value="1" rule="315" place="6" punct="pt">eau</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="8" num="2.1" lm="6" met="6"><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="8.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="8.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="8.4">pl<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>s</w> <w n="8.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5">an</seg>s</w> <w n="8.6">l</w>’<w n="8.7" punct="vg:6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="9" num="2.2" lm="6" met="6"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281" place="2">oi</seg></w> <w n="9.2">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="9.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>d<seg phoneme="o" type="vs" value="1" rule="315" place="5">eau</seg></w> <w n="9.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="10" num="2.3" lm="6" met="6"><w n="10.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443" place="2">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w>-<w n="10.2">t</w>-<w n="10.3"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>l</w> <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318" place="5">au</seg>x</w> <w n="10.5">y<seg phoneme="ø" type="vs" value="1" rule="398" place="6">eu</seg>x</w></l>
						<l n="11" num="2.4" lm="6" met="6"><w n="11.1">T<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="11.2">gr<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>c<seg phoneme="i" type="vs" value="1" rule="d-1" place="3">i</seg><seg phoneme="ø" type="vs" value="1" rule="403" place="4">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.3" punct="vg:6"><seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="12" num="2.5" lm="6" met="6"><w n="12.1">D<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="12.2">m<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="13" num="2.6" lm="6" met="6"><w n="13.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="13.2">n</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="13.4">qu</w>’<w n="13.5"><seg phoneme="y" type="vs" value="1" rule="453" place="3">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="13.6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="14" num="2.7" lm="6" met="6"><w n="14.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="14.3">cl<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="14.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="5">e</seg>s</w> <w n="14.5" punct="pi:6">ci<seg phoneme="ø" type="vs" value="1" rule="398" place="6" punct="pi">eu</seg>x</w> ?</l>
					</lg>
					<lg n="3">
						<l n="15" num="3.1" lm="6" met="6"><w n="15.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="15.2"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="15.3" punct="pt:6">p<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
						<l n="16" num="3.2" lm="6" met="6"><w n="16.1">Qu</w>’<w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="1">un</seg></w> <w n="16.3">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd</w> <w n="16.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg></w> <w n="16.5" punct="vg:6">l<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="17" num="3.3" lm="6" met="6"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="17.2">qu<seg phoneme="i" type="vs" value="1" rule="491" place="2">i</seg></w> <w n="17.3">fr<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="17.4" punct="pv:6">s<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="pv">en</seg>t</w> ;</l>
						<l n="18" num="3.4" lm="6" met="6"><w n="18.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="18.3">j<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="19" num="3.5" lm="6" met="6"><w n="19.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="19.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="19.3">t<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="19.5">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="19.6" punct="vg:6">pl<seg phoneme="i" type="vs" value="1" rule="469" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="20" num="3.6" lm="6" met="6"><w n="20.1" punct="vg:2">D<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2" punct="vg">e</seg></w>, <w n="20.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">aî</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="e" type="vs" value="1" rule="189" place="4">e</seg>t</w> <w n="20.4">p<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482" place="6">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="21" num="3.7" lm="6" met="6"><w n="21.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="21.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="21.4" punct="pt:6">v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="pt">an</seg>t</w>.</l>
					</lg>
					<lg n="4">
						<l n="22" num="4.1" lm="6" met="6"><w n="22.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg></w> <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="3">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="22.3" punct="vg:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="23" num="4.2" lm="6" met="6"><w n="23.1">C</w>’<w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="23.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="23.4">n<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="23.5">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467" place="6">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="24" num="4.3" lm="6" met="6"><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="24.2">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="2">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190" place="3">e</seg>t</w> <w n="24.3" punct="vg:6">ch<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="wa" type="vs" value="1" rule="440" place="5">o</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg">an</seg>t</w>,</l>
						<l n="25" num="4.4" lm="6" met="6"><w n="25.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="25.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="25.3">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="e" type="vs" value="1" rule="189" place="5">e</seg>t</w> <w n="25.5">r<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="26" num="4.5" lm="6" met="6"><w n="26.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="26.2">l</w>’<w n="26.3">H<seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>sp<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4">on</seg>t</w> <w n="26.4" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="27" num="4.6" lm="6" met="6"><w n="27.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Où</seg></w> <w n="27.2">C<seg phoneme="i" type="vs" value="1" rule="493" place="2">y</seg>pr<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>s</w> <w n="27.3"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="27.4"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>cl<seg phoneme="o" type="vs" value="1" rule="444" place="6">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="28" num="4.7" lm="6" met="6"><w n="28.1">S<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>s</w> <w n="28.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="28.3">ci<seg phoneme="ɛ" type="vs" value="1" rule="346" place="3">e</seg>l</w> <w n="28.4">d</w>’<w n="28.5" punct="pt:6"><seg phoneme="o" type="vs" value="1" rule="444" place="4">O</seg>r<seg phoneme="i" type="vs" value="1" rule="d-1" place="5">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" punct="pt">en</seg>t</w>.</l>
					</lg>
					<lg n="5">
						<l n="29" num="5.1" lm="6" met="6"><w n="29.1">C</w>’<w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="29.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="29.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="3">eu</seg>r</w> <w n="29.5">fr<seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="30" num="5.2" lm="6" met="6"><w n="30.1">Qu<seg phoneme="i" type="vs" value="1" rule="491" place="1">i</seg></w> <w n="30.2">s</w>’<w n="30.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>tr</w>’<w n="30.4"><seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="30.5">n<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="31" num="5.3" lm="6" met="6"><w n="31.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="31.2">z<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493" place="3">y</seg>r</w> <w n="31.3">d<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="31.4" punct="pv:6">m<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="pv">in</seg></w> ;</l>
						<l n="32" num="5.4" lm="6" met="6"><w n="32.1">C</w>’<w n="32.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="1">e</seg>st</w> <w n="32.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="32.4">s<seg phoneme="wa" type="vs" value="1" rule="440" place="3">o</seg>y<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="32.5">p<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="33" num="5.5" lm="6" met="6"><w n="33.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="33.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="33.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="33.4" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg><seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="34" num="5.6" lm="6" met="6"><w n="34.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1">on</seg>t</w> <w n="34.2">l</w>’<w n="34.3"><seg phoneme="o" type="vs" value="1" rule="318" place="2">au</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="34.4">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="5">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="35" num="5.7" lm="6" met="6"><w n="35.1">Ch<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg>ff<seg phoneme="o" type="vs" value="1" rule="435" place="2">o</seg>nn<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="35.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="35.3" punct="pt:6">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="6" punct="pt">in</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="36" num="6.1" lm="6" met="6"><w n="36.1"><seg phoneme="i" type="vs" value="1" rule="467" place="1">I</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="2">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="36.2" punct="vg:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="37" num="6.2" lm="6" met="6"><w n="37.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">en</seg>d</w> <w n="37.2"><seg phoneme="y" type="vs" value="1" rule="453" place="2">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="37.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5">e</seg></w> <w n="37.4">p<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="38" num="6.3" lm="6" met="6"><w n="38.1"><seg phoneme="a" type="vs" value="1" rule="340" place="1">A</seg></w> <w n="38.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg></w> <w n="38.3">l<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="38.4" punct="pv:6">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" punct="pv">e</seg>il</w> ;</l>
						<l n="39" num="6.4" lm="6" met="6"><w n="39.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="1">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg></w> <w n="39.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="39.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="39.4">r<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="40" num="6.5" lm="6" met="6"><w n="40.1"><seg phoneme="y" type="vs" value="1" rule="453" place="1">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24" place="2">e</seg></w> <w n="40.2">g<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="40.3" punct="vg:6"><seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="41" num="6.6" lm="6" met="6"><w n="41.1"><seg phoneme="o" type="vs" value="1" rule="318" place="1">Au</seg></w> <w n="41.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rd</w> <w n="41.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161" place="3">e</seg>s</w> <w n="41.4">fl<seg phoneme="œ" type="vs" value="1" rule="407" place="4">eu</seg>rs</w> <w n="41.5" punct="vg:6">p<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409" place="6">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="42" num="6.7" lm="6" met="6"><w n="42.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="42.2"><seg phoneme="o" type="vs" value="1" rule="318" place="4">au</seg></w> <w n="42.3" punct="pt:6">s<seg phoneme="o" type="vs" value="1" rule="444" place="5">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382" place="6" punct="pt">e</seg>il</w>.</l>
					</lg>
					<lg n="7">
						<l n="43" num="7.1" lm="6" met="6"><w n="43.1">G<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="1">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="43.2" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="444" place="3">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="4" punct="vg">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="43.3" punct="pe:6"><seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pe">e</seg></w> !</l>
						<l n="44" num="7.2" lm="6" met="6"><w n="44.1">S<seg phoneme="i" type="vs" value="1" rule="468" place="1">i</seg></w> <w n="44.2">t<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="44.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3">on</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="44.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="44.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="44.6">r<seg phoneme="u" type="vs" value="1" rule="425" place="6">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="45" num="7.3" lm="6" met="6"><w n="45.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="45.2">l<seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="45.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="3">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="45.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="45.5" punct="vg:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg">œu</seg>r</w>,</l>
						<l n="46" num="7.4" lm="6" met="6"><w n="46.1"><seg phoneme="o" type="vs" value="1" rule="444" place="1">O</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382" place="2">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="46.2" punct="vg:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="4">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="47" num="7.5" lm="6" met="6"><w n="47.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="190" place="2">e</seg>ts</w> <w n="47.2">qu</w>’<w n="47.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3">à</seg></w> <w n="47.4">t<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="47.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="5">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="48" num="7.6" lm="6" met="6"><w n="48.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="48.2">d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3">e</seg></w> <w n="48.3">m<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="48.4" punct="vg:6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352" place="6">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="49" num="7.7" lm="6" met="6"><w n="49.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419" place="1">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="49.2"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="49.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="49.4"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="49.5">s<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg></w> <w n="49.6" punct="pt:6">s<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="pt">œu</seg>r</w>.</l>
					</lg>
					<lg n="8">
						<l n="50" num="8.1" lm="6" met="6"><w n="50.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="50.2">p<seg phoneme="œ" type="vs" value="1" rule="407" place="2">eu</seg>r</w> <w n="50.3">t<seg phoneme="y" type="vs" value="1" rule="450" place="3">u</seg></w> <w n="50.4">p<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="50.5">m</w>’<w n="50.6" punct="pv:6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pv">e</seg></w> ;</l>
						<l n="51" num="8.2" lm="6" met="6"><w n="51.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="51.2">n</w>’<w n="51.3"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg>s</w> <w n="51.4">ri<seg phoneme="ɛ̃" type="vs" value="1" rule="377" place="3">en</seg></w> <w n="51.5"><seg phoneme="a" type="vs" value="1" rule="342" place="4">à</seg></w> <w n="51.6" punct="pt:6">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="5">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="6">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="pt">e</seg></w>.</l>
						<l n="52" num="8.3" lm="6" met="6"><w n="52.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="52.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="52.3">r<seg phoneme="u" type="vs" value="1" rule="425" place="3">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340" place="5">a</seg>s</w> <w n="52.4" punct="pt:6">p<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pt">a</seg>s</w>.</l>
						<l n="53" num="8.4" lm="6" met="6"><w n="53.1">N<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg>l</w> <w n="53.2">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="53.3">s<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>t</w> <w n="53.4">m<seg phoneme="e" type="vs" value="1" rule="409" place="5">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="54" num="8.5" lm="6" met="6"><w n="54.1">D<seg phoneme="y" type="vs" value="1" rule="450" place="1">u</seg></w> <w n="54.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347" place="3">er</seg></w> <w n="54.3">qu<seg phoneme="i" type="vs" value="1" rule="491" place="4">i</seg></w> <w n="54.4">m</w>’<w n="54.5" punct="vg:6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="5">in</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7" punct="vg">e</seg></w>,</l>
						<l n="55" num="8.6" lm="6" met="6"><w n="55.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="55.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>t</w> <w n="55.3">h<seg phoneme="o" type="vs" value="1" rule="318" place="3">au</seg>t</w> <w n="55.4">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="55.5">p<seg phoneme="ø" type="vs" value="1" rule="398" place="5">eu</seg>x</w> <w n="55.6">d<seg phoneme="i" type="vs" value="1" rule="468" place="6">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="7">e</seg></w></l>
						<l n="56" num="8.7" lm="6" met="6"><w n="56.1">C<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="56.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="2">e</seg></w> <w n="56.3">j</w>’<w n="56.4"><seg phoneme="ɛ" type="vs" value="1" rule="306" place="3">ai</seg></w> <w n="56.5">d<seg phoneme="i" type="vs" value="1" rule="468" place="4">i</seg>t</w> <w n="56.6">t<seg phoneme="u" type="vs" value="1" rule="425" place="5">ou</seg>t</w> <w n="56.7" punct="pt:6">b<seg phoneme="a" type="vs" value="1" rule="340" place="6" punct="pt">a</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>