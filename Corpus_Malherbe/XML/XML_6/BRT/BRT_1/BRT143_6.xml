<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT143" modus="cm" lm_max="12" metProfile="6+6">
				<head type="number">XLIII</head>
				<lg n="1">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358" place="1">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22" place="2" mp="F">e</seg>s</w> <w n="1.2" punct="vg:3">f<seg phoneme="u" type="vs" value="1" rule="425" place="3" punct="vg">ou</seg>s</w>, <w n="1.3">m</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340" place="4" mp="Lp">a</seg></w>-<w n="1.5">t</w>-<w n="1.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5">on</seg></w> <w n="1.7" punct="vg:6">d<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="vg" caesura="1">i</seg>t</w>,<caesura></caesura> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.9">vr<seg phoneme="ɛ" type="vs" value="1" rule="305" place="8" mp="M">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369" place="9">en</seg>t</w> <w n="1.10">c</w>’<w n="1.11"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="10">e</seg>st</w> <w n="1.12" punct="vg:12">p<seg phoneme="o" type="vs" value="1" rule="435" place="11" mp="M">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="1">En</seg></w> <w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="2">on</seg>t</w> <w n="2.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="2.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="4" mp="P">an</seg>s</w> <w n="2.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="2.6" punct="pe:6">l<seg phoneme="y" type="vs" value="1" rule="453" place="6" punct="pe" caesura="1">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> !<caesura></caesura> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="133" place="7">e</seg>h</w> <w n="2.8" punct="vg:8">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="8" punct="vg">en</seg></w>, <w n="2.9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9">an</seg>t</w> <w n="2.10">p<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>s</w> <w n="2.11">p<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="P">ou</seg>r</w> <w n="2.12" punct="pe:12"><seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pe">eu</seg>x</w> !</l>
					<l n="3" num="1.3" lm="12" met="6+6"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="3.2">m<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="3.3" punct="vg:5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="M">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" punct="vg" mp="F">e</seg></w>, <w n="3.4" punct="vg:6">m<seg phoneme="wa" type="vs" value="1" rule="423" place="6" punct="vg" caesura="1">oi</seg></w>,<caesura></caesura> <w n="3.5">d</w>’<w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="3.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="3.9">l<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="3.10" punct="pt:12">c<seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></w>.</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem/mp">e</seg>lu<seg phoneme="i" type="vs" value="1" rule="491" place="2" mp="Lp">i</seg></w>-<w n="4.2">c<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg></w> <w n="4.3">n</w>’<w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="4">e</seg>st</w> <w n="4.5">qu</w>’<w n="4.6" punct="pv:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>dr<seg phoneme="wa" type="vs" value="1" rule="420" place="6" punct="pv" caesura="1">oi</seg>t</w> ;<caesura></caesura> <w n="4.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="7">ai</seg>s</w> <w n="4.8">l</w>’<w n="4.9"><seg phoneme="o" type="vs" value="1" rule="318" place="8">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.10"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="9">e</seg>st</w> <w n="4.11" punct="pt:12">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="10" mp="M">an</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg>r<seg phoneme="ø" type="vs" value="1" rule="398" place="12" punct="pt">eu</seg>x</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="5" num="2.1" lm="12" met="6+6"><w n="5.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2">a</seg></w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="C">a</seg></w> <w n="5.4">si<seg phoneme="ɛ" type="vs" value="1" rule="366" place="4">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="5.5">l<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w><caesura></caesura> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="5.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="8">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="e" type="vs" value="1" rule="189" place="9">e</seg>t</w> <w n="5.9" punct="pv:12">s<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467" place="11" mp="M">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></w> ;</l>
					<l n="6" num="2.2" lm="12" met="6+6"><w n="6.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="6.2" punct="vg:2"><seg phoneme="o" type="vs" value="1" rule="318" place="2" punct="vg">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="342" place="3" mp="P">à</seg></w> <w n="6.4">r<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="6.5">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="6" caesura="1">ain</seg>s</w><caesura></caesura> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="6.7">c<seg phoneme="u" type="vs" value="1" rule="425" place="8" mp="M">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407" place="9">eu</seg>r</w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="Pem">e</seg></w> <w n="6.9" punct="pt:12">c<seg phoneme="ɔ" type="vs" value="1" rule="439" place="11" mp="M">o</seg>rb<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg></w>.</l>
					<l n="7" num="2.3" lm="12" met="6+6"><w n="7.1" punct="vg:3">C<seg phoneme="ə" type="em" value="1" rule="e-19" place="1" mp="Mem">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" punct="vg">an</seg>t</w>, <w n="7.2">qu</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="4">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="7.4">s<seg phoneme="wa" type="vs" value="1" rule="420" place="6" caesura="1">oi</seg>t</w><caesura></caesura> <w n="7.5" punct="vg:8">gl<seg phoneme="a" type="vs" value="1" rule="340" place="7">a</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" punct="vg" mp="F">e</seg></w>, <w n="7.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="308" place="9">aî</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.7"><seg phoneme="u" type="vs" value="1" rule="426" place="10">ou</seg></w> <w n="7.8" punct="vg:12">t<seg phoneme="a" type="vs" value="1" rule="341" place="11" mp="M">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409" place="12">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="8" num="2.4" lm="12" met="6+6"><w n="8.1">Ch<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="2">un</seg></w> <w n="8.2">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="8.3">tr<seg phoneme="u" type="vs" value="1" rule="425" place="4">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="5" mp="F">e</seg></w> <w n="8.4" punct="vg:6">bi<seg phoneme="ɛ̃" type="vs" value="1" rule="375" place="6" punct="vg" caesura="1">en</seg></w>,<caesura></caesura> <w n="8.5">ch<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452" place="8">un</seg></w> <w n="8.6">s<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="8.7">tr<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="8.8" punct="pt:12">b<seg phoneme="o" type="vs" value="1" rule="315" place="12" punct="pt">eau</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="9" num="3.1" lm="12" met="6+6"><w n="9.1">J</w>’<w n="9.2" punct="vg:1"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="1" punct="vg">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="342" place="2" mp="P">à</seg></w> <w n="9.4">tr<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="4">e</seg>rs</w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="C">a</seg></w> <w n="9.6" punct="vg:6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6" punct="vg" caesura="1">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>,<caesura></caesura> <w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="7">en</seg></w> <w n="9.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="9.9">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24" place="11" mp="F">e</seg></w> <w n="9.10" punct="vg:12">m<seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="10" num="3.2" lm="12" met="6+6"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426" place="1">Ou</seg></w> <w n="10.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="2" mp="C">e</seg>s</w> <w n="10.3">f<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340" place="4">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22" place="5" mp="F">e</seg>s</w> <w n="10.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="6" caesura="1">on</seg>ds</w><caesura></caesura> <w n="10.5"><seg phoneme="u" type="vs" value="1" rule="426" place="7">ou</seg></w> <w n="10.6">s<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="10.7">l<seg phoneme="u" type="vs" value="1" rule="425" place="9">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="10.8" punct="ps:12">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11" mp="M">en</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="ps">eu</seg>r</w>…</l>
					<l n="11" num="3.3" lm="12" met="6+6"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12" place="1" mp="C">e</seg></w> <w n="11.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>s</w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="11.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412" place="4">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439" place="6" caesura="1">o</seg>rs</w><caesura></caesura> <w n="11.6" punct="vg:8">L<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="8" punct="vg">an</seg></w>, <w n="11.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="11.8" punct="vg:12">p<seg phoneme="a" type="vs" value="1" rule="340" place="10" mp="M">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="d-1" place="11" mp="M">i</seg><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></w>,</l>
					<l n="12" num="3.4" lm="12" met="6+6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="12.2">J<seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="3">o</seg>b</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="4" mp="M">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5" mp="M">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg></w><caesura></caesura> <w n="12.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="7" mp="P">an</seg>s</w> <w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="452" place="8" mp="C">un</seg></w> <w n="12.6"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420" place="10">oi</seg>r</w> <w n="12.7" punct="pt:12">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="11" mp="M">en</seg>t<seg phoneme="œ" type="vs" value="1" rule="407" place="12" punct="pt">eu</seg>r</w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Trou-peau.</note>
					</closer>
			</div></body></text></TEI>